<html id="TOCCOM">

<head>
<title>SkunkWorks COM Interface</title>
<link rel=stylesheet href="SkunkWorks.css">
</head>

<body class=Normal lang="EN-US">

<h1><a name="_Top"/>COM interface</h1>

<p class=Normal>You don't have to use the <a href="Console.html">SkunkWorks 
console</a> to run your script.  You don't even have to write your macro in 
script.  SkunkWorks exposes a COM interface that you can access directly from 
any COM-capable programming environment such as Visual C++, Visual Basic, 
Delphi, etc.  Any place you can host a COM object&mdash;even a Web 
page&mdash;you can use as a platform for building your macro.</p>

<p class=Normal>Of course by bypassing the console you give up such prefabbed 
console amenities as the <b>Stop</b> button and the in-game miniconsole.  You 
also lose the ability to use script libraries such as ACScriptLib or <a 
href="Libraries.html#_SkunkNav">SkunkNav</a> in your macro.  If you want such 
features, you'll have to build them into your app yourself.  On the plus side, 
you'll have the full power of industrial-strength programming tools at your 
disposal to do it with.</p>

<p class=Normal>All of the SkunkWorks API services are accessed through the 
<b>skapi</b> object, which has the progid <b>SkunkWorks.SkapiCls</b>.  So for 
instance in Visual Basic you would do something like this:</p>

<p class=Codesample2>Dim skapi As SkunkWorks.SkapiCls<br>
Set skapi = New SkunkWorks.SkapiCls</p>

<p class=Normal>or alternatively:</p>

<p class=Codesample2>Dim skapi As Object<br>
Set skapi = CreateObject("SkunkWorks.SkapiCls")</p>

<p class=Normal>the first example being preferable for performance reasons 
(early binding v. late binding).</p>

<p class=Normal>When using the COM interface you have the option of receiving 
game events either by creating and registering event handlers using <a 
href="Skapi.html#_skapi_AddHandler"><b>skapi.AddHandler</b></a>, or by sinking 
COM-style events from <a href="Skapi.html#_skapi_skev"><b>skapi.skev</b></a>.  
So for instance to receive the OnLogon event from <b>skapi.skev</b> (again 
using VB as an example) you would do:</p>

<p class=Codesample2>Dim WithEvents skev As SkunkWorks.SkevCls<br>
Set skev = skapi.skev<br>
<br>
Private Sub skev_OnLogon(ByVal acoChar As SkunkWorks.AcoCls)<br>
' Your code here.<br>
End Sub</p>

<p class=Normal>The details of instantiating COM objects and sinking events from 
them in other languages obviously depend on the language.  I'm going to assume 
you know how to do it (or where to look it up) in the language you're using.</p>

<p class=Normal>For best results you should start your app and instantiate the 
<b>skapi</b> before starting the game, or very soon after.  You should also call 
<a href="Skapi.html#_skapi_WaitEvent"><b>skapi.WaitEvent</b></a> frequently 
(preferably several times a second), starting from the moment you first instantiate 
the <b>skapi</b>, to keep the <b>skapi</b>'s game state up to date and to keep the 
event queue from overflowing.  The SkunkWorks filter will queue events for you up 
to a point, but if you let the queue overflow, you'll lose login and inventory 
information.</p>

<p class=Normal>You'll also lose information if you stop your app and destroy 
your skapi in mid-game.  No game state is stored in the filter; it's all in the 
skapi.  So your skapi must remain continuously in existence while the game is 
running in order to have valid state.  If you must stop and restart your app, 
e.g. for debugging purposes, be sure to log out of the game and back in again 
after restarting in order to refresh the game state.</p>

<p class=Normal>Note also that if the filter finds no <b>skapi</b> running when the 
game starts, it will autolaunch the SkunkWorks console.  Since in the current 
implementation no more than one <b>skapi</b> at a time can connect to the filter, 
this would prevent your app from connecting if you start it later.</p>

<p class=Normal>You can disable this autolaunch feature by creating the following 
registry value:</p>

<p class=Codesample2>[HKEY_CURRENT_USER\Software\VB and VBA Program Settings\SkunkWorks\Settings]<br>
"Console"=""</p>

<p class=Normal>To have the filter autolaunch your app in place of the default 
SkunkWorks console, do something like this instead:</p>

<p class=Codesample2>[HKEY_CURRENT_USER\Software\VB and VBA Program Settings\SkunkWorks\Settings]<br>
"Console"="\"C:\\MyDir\\MyApp.exe\" &lt;args&gt;"</p>

<p class=Normal>with (obviously) the actual path and filename of your app, along with 
any command-line arguments it takes.</p>

<p class=Normal>To restore the default autolaunch behavior, delete the <b>Console</b> 
registry value.</p>

<p class=Highlight17><a name="_GetSkapiCls"/>The foregoing assumes that you're building a standalone COM 
app to run with SkunkWorks.  But that's not the only way to use the COM interface.  
You can also write SkunkWorks-aware ActiveX DLLs (COM components) that can be 
instantiated and used from within a SkunkWorks script.  In that case, you do 
<b>not</b> want to instantiate your own skapi, because there will already be 
one in existence, and you can't create more than one.  What you need in that 
case is a way to get a reference to that pre-existing skapi.  You can do this 
by creating an instance of <b>SkunkWorks.GetSkapiCls</b> and using it to 
retrieve a reference to the skapi.  In Visual Basic this looks like this:</p>

<p class=Codesample2>Dim getskapi As SkunkWorks.GetSkapiCls<br>
Dim skapi As SkunkWorks.SkapiCls<br>
Set getskapi = New SkunkWorks.GetSkapiCls<br>
Set skapi = getskapi.skapi</p>

<p class=Normal>The <b>skapi</b> object gives access to the native <a 
href="Skapi.html">SkunkWorks API</a>.  The native API should provide everything 
you need, but if for some reason you prefer to use the <a 
href="ACScript.html">ACScript API</a>, you can do 
so as follows (again using Visual Basic as an example):</p>

<p class=Codesample2>Dim ac As Object, Inventory As Object, Width As Long, 
Height As Long<br>
Set ac = skapi.ac<br>
Set Inventory = skapi.Inventory<br>
Width = skapi.dxpRes<br>
Height = skapi.dypRes</p>

<p class=Normal>This sets up the familiar <b>ac</b> and <b>Inventory</b> 
objects as well as the <b>Width</b> and <b>Height</b> globals.  (The SkunkWorks 
console does this for you automatically when starting a script.)  References to 
<b>ac.KeyEvent</b>, <b>Inventory.GetItem</b>, and so forth now work as 
expected, with a few exceptions:</p>

<ul>

<li class=Normal>Neither <b>ac.RegisterHandler</b> nor the automatic 
registration of <b>ACMsg_*</b> handlers will work in this mode.  Since this is 
a pure COM interface, you need to package your event handlers up in a COM 
object, and register it explicitly using <a 
href="Skapi.html#_skapi_AddHandler"><b>skapi.AddHandler</b></a>.</li>

<li class=Normal>You should also be aware that for stupid technical reasons, I 
had to change a few names in the ACScript API.  Specifically, <b>ac.Print</b>, 
<b>ac.Debug</b>, and <b>obj.Type</b> are now called <b>ac.Print_</b>, 
<b>ac.Debug_</b>, and <b>obj.Type_</b> (with trailing underscores).  The 
SkunkWorks console handles this translation transparently, but if you're 
bypassing the console to access the COM interface directly, you'll have to do 
the renames yourself.</li>

<li class=Normal>Finally, since you're running without a console, any output 
sent to <b>ac.Print_</b> will land in the bit bucket.  Use <b><a 
href="Skapi.html#_skapi_OutputSz">skapi.OutputSz</a>(..., opmChatWnd)</b> 
instead to direct output to the in-game chat window.</li>

</ul>

<p class=Normal>Note that all of these quirks can be avoided by simply ignoring 
the ACScript API and coding directly to the native SkunkWorks API in the first 
place.</p>

</body>

</html>
