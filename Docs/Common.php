<?php

// <script language=JScript>
// 	
// 	var btnDown = null;
// 	
// 	function OnMouseDown(btn)
// 		{
// 		if (btnDown != null)
// 			btnDown.className = 'Up';
// 		
// 		btn.className = 'Down';
// 		btnDown = btn;
// 		}
// 	
// 	function OnMouseLeave(btn)
// 		{
// 		btn.className = 'Up';
// 		}
// 	
// 	function OnMouseEnter(btn)
// 		{
// 		if (btn = btnDown)
// 			btn.className = 'Down';
// 		}
// 	
// 	function OnMouseUp(btn)
// 		{
// 		if (btn = btnDown)
// 			{
// 			btn.className = 'Up';
// 			btnDown = null;
// 			}
// 		}
// 	
// </script>

function FPrefixMatchSz($sz, $szPrefix)
	{
	return substr($sz, 0, strlen($szPrefix)) == $szPrefix;
	}

function FSuffixMatchSz($sz, $szSuffix)
	{
	return substr($sz, -strlen($szSuffix)) == $szSuffix;
	}

function SzRemoveExt($szFile)
	{
	$ich = strrpos($szFile, '.');
	if ($ich === false)
		return $szFile;
	else
		return substr($szFile, 0, $ich);
	}

$szPathUserWare = '/home/groups/s/sk/skunkworks/htdocs/userware';
$szUrlUserWare = 'http://skunkworks.sourceforge.net/userware';

function UwiFromFile($szXmlFile)
	{
	$hfile = fopen($szXmlFile, "r");
	while (!feof($hfile))
		{
	    $szLine = fgets($hfile, 2000);
		if (FPrefixMatchSz($szLine, "szTitle="))
	    	{
	    	$a = sscanf($szLine, "szTitle=\"%[^\"]\"");
	    	$uwi['szTitle'] = $a[0];
	    	}
	    else if (FPrefixMatchSz($szLine, "szVersion="))
	    	{
	    	$a = sscanf($szLine, "szVersion=\"%[^\"]\"");
	    	$uwi['szVersion'] = $a[0];
	    	}
	    else if (FPrefixMatchSz($szLine, "szAuthor="))
	    	{
	    	$a = sscanf($szLine, "szAuthor=\"%[^\"]\"");
	    	$uwi['szAuthor'] = $a[0];
	    	}
	    else if (FPrefixMatchSz($szLine, "szDate="))
	    	{
	    	$a = sscanf($szLine, "szDate=\"%[^\"]\"");
	    	$uwi['szDate'] = $a[0];
	    	}
	    else if (FPrefixMatchSz($szLine, "szLanguage="))
	    	{
	    	$a = sscanf($szLine, "szLanguage=\"%[^\"]\"");
	    	$uwi['szLanguage'] = $a[0];
	    	}
	    else if (FPrefixMatchSz($szLine, "szFile="))
	    	{
	    	$a = sscanf($szLine, "szFile=\"%[^\"]\"");
	    	$uwi['szFile'] = $a[0];
	    	}
	    else if (FPrefixMatchSz($szLine, "<szDesc>"))
	    	{
			while (!feof($hfile))
				{
			    $szLine = fgets($hfile, 2000);
				if (FPrefixMatchSz($szLine, "</szDesc>")) break;
		    	$uwi['szDesc'] = $uwi['szDesc'] . $szLine;
		    	}
	    	}
		}
	fclose($hfile);
	
	$uwi['szCategory'] = basename(dirname($szXmlFile));
	
	return $uwi;
	}

function SaveUwiToFile($szXmlFile, $uwi)
	{
	$hfile = fopen($szXmlFile, 'w');
	fwrite($hfile, "<UserWare\n");
	fwrite($hfile, "szTitle=\"" . $uwi['szTitle'] . "\"\n");
	fwrite($hfile, "szVersion=\"" . $uwi['szVersion'] . "\"\n");
	fwrite($hfile, "szAuthor=\"" . $uwi['szAuthor'] . "\"\n");
	fwrite($hfile, "szDate=\"" . $uwi['szDate'] . "\"\n");
	fwrite($hfile, "szLanguage=\"" . $uwi['szLanguage'] . "\"\n");
	fwrite($hfile, "szFile=\"" . $uwi['szFile'] . "\"\n");
	fwrite($hfile, ">\n");
	fwrite($hfile, "<szDesc>\n");
	fwrite($hfile, $uwi['szDesc'] . "\n");
	fwrite($hfile, "</szDesc>\n");
	fwrite($hfile, "</UserWare>\n");
	fclose($hfile);
	}

function SzFileFromUwi($uwi)
	{
	global $szPathUserWare;
	return $szPathUserWare . "/" . $uwi['szCategory'] . "/" . $uwi['szFile'];
	}

function SzUrlFromUwi($uwi)
	{
	global $szUrlUserWare;
	return $szUrlUserWare . "/" . $uwi['szCategory'] . "/" . $uwi['szFile'];
	}

function RelUwi($uwi1, $uwi2)
	{
	return strcasecmp($uwi1['szTitle'], $uwi2['szTitle']);
	}

// This doesn't work.  The XSLT module doesn't seem to be available.
function GenItemXslt($szXmlFile)
	{
	// Allocate a new XSLT processor
	$xh = xslt_create();
	
	echo xslt_process($xh, $szXmlFile, 'UserWare.xsl');
	
	xslt_free($xh);
	}

function GenItemUwi($uwi)
	{
	$szUrl = SzUrlFromUwi($uwi);
?>

<table class="UserWare" width="100%" cellspacing="2" valign="middle">
  <col width="100"/>
  <tr>
    <td class="UserWare">
      <p class="Normal" style="text-align: center;">
        <?php
//	          <button class="Up" style="width: 100%; height: 100%;" 
//		        hidefocus="true" tabindex="-1" 
//		        onmousedown="OnMouseDown(this);"
//		        onmouseleave="OnMouseLeave(this);"
//		        onmouseenter="OnMouseEnter(this);"
//		        onmouseup="OnMouseUp(this);"
//		        onlosecapture="OnMouseUp(this);"
//		        onclick="window.open('$szUrl', '_self')"
//		        >
//		  	    <b>Download</b>
//		      </button>
          echo "<a href='$szUrl'><b>Download</b></a>"
	    ?>
	  </p>
    </td>
    <td class="UserWare">
      <p class="Normal">
      <?php
      echo "<span style=\"font-size: 12pt;\">";
      echo "<b>" . $uwi['szTitle'] . "</b>";
      if ($uwi['szVersion'] != "") echo " version <b>" . $uwi['szVersion'] . "</b>";
      echo "</span>";
      echo " submitted";
      if ($uwi['szAuthor'] != "") echo " by <b>" . $uwi['szAuthor'] . "</b>";
      echo " on <b>" . $uwi['szDate'] . "</b>";
      if ($uwi['szLanguage'] != "") echo "<br/>Language: <b>" . $uwi['szLanguage'] . "</b>";
      ?>
      </p>
    </td>
  </tr>
  <tr>
    <td class="UserWare" colspan="2">
      <p class="Normal"><?php echo $uwi['szDesc']; ?></p>
    </td>
  </tr>
</table>
<?php
	}

?>
