<html id="TOCSource">

<head>
<title>SkunkWorks Source</title>
<link rel=stylesheet href="SkunkWorks.css">
</head>

<body class=Normal lang="EN-US">

<h1><a name="_Top"/>SkunkWorks Source</h1>

<p class=Normal>While I don't consider SkunkWorks to 
be "Open Source" in any down-with-copyrights, information-must-be-free 
ideological sense, I've included the source in the distribution package for a 
number of purely practical reasons:</p>

<ul>

<li class=Normal>There's a fair amount of paranoia about since the demise of 
ACScript.  I want users to be able to study the source, verify that it contains 
no password-stealing Trojans or other malicious code, and build themselves a 
clean, safe executable for their own use.</li>

<li class=Normal>If a protocol change breaks something, I don't want to be the 
sole source for fixes.  More people equipped to solve problems and publish the 
solutions means less pressure on me come patch day.</li>

<li class=Normal>To be brutally honest, macro newbies with pirated drain 
scripts are not the intended audience of this tool, and I don't intend to offer 
them a lot of support.  SkunkWorks is meant to be an industrial-strength 
scripting platform for people who know what they're doing and don't mind 
rolling up their sleeves.  Though I've tried to be fairly thorough in the 
<a href="Console.html">console</a> and <a href="Skapi.html">API</a> documentation, 
consider the source the ultimate authority on what the program does and how it does 
it.  You don't have to be a VB wizard to use SkunkWorks, but the support you get 
from me will be based on the assumption that you've at least tried to find answers 
for yourself.</li>

</ul>

<p class=Normal>Source is installed into the SkunkWorks\Source folder, and 
includes four Visual Basic projects, two VB project groups, and one Visual C++ 
project:</p>

<ul>

<li class=Normal><b>SkunkWorks.vbp</b> builds the SkunkWorks API COM component 
(Skapi.dll).  This project gets the coveted "SkunkWorks" name mainly so that its 
progids come out as SkunkWorks.something.</li>

<li class=Normal><b>SWConsole.vbp</b> builds the SkunkWorks console (SWConsole.exe).</li>

<li class=Normal><b>SkunkWorks.vbg</b> is a project group combining the API and 
the console.  Since the console depends on the API, I find it convenient to edit 
and debug them together in one VB environment.</li>

<li class=Normal><b>SWPlugin.vbp</b> builds the SkunkWorks plugin (SWPlugin.dll).</li>

<li class=Normal><b>SWFilter.vbp</b> builds the SkunkWorks netfilter (SWFilter.dll).</li>

<li class=Normal><b>SWPlugin.vbg</b> is a project group combining the plugin 
and the filter.  Again, the intertwined nature of these two components makes it 
easier to work on them together rather than separately.</li>

<li class=Normal><b>SwxScript.dsw</b> is a Visual C++ project that builds a 
component call SwxScript.dll.  When I finally got fed up with the limitations 
of the MS Script Control, I wrote this component to replace it.  Its main 
advantages are better line number reporting on errors, the ability to reliably 
stop a runaway script, and script debugger support.</li>

<p class=Highlight>Note that if you want to build SwxScript.dll, you will need 
the headers and library files contained in Microsoft's Scriptng.exe, which you 
can download from 
<a target="SWOutlink" 
href="http://support.microsoft.com/default.aspx?scid=kb%3ben-us%3b223389">this 
Knowledge Base article</a>.</p>

</ul>

<p class=Normal>If all you want to do is rebuild your own Skapi.dll on <a 
href="Setup.html#_Patch_day">patch day</a>, the installed source should meet your 
needs.  If you're seriously interested in tinkering, however, you may want to enroll 
in the <a target="SWOutlink" href="http://sourceforge.net/projects/skunkworks">SkunkWorks 
project on SourceForge.net</a>.</p>

<h2><a name="_CVS"/>CVS setup</h2>

<p class=Normal>This section describes how to set up anonymous, read-only access to 
the SkunkWorks CVS repository.  This will enable you to customize your version of 
SkunkWorks and automatically maintain your local changes across new releases.  It 
will not enable you to upload changes back into the mainline development source.  
For information on how to become an official SkunkWorks developer with upload 
privileges, contact me by email.</p>

<p class=Normal>I'm going to assume you're already a registered SourceForge user 
and have downloaded and installed WinCVS.  If not, see the <a target="SWOutlink" 
href="http://sourceforge.net/docman/?group_id=1">SourceForge site documentation</a> 
for information on how to do that.</p>

<p class=Normal>Here's my recommended sequence for setting up a CVS-enabled 
SkunkWorks build environment on your disk:</p>

<ul>

<li class=Normal>Download and install the latest SkunkWorks release, if you 
haven't already done so.</li>

<li class=Normal>Create a new folder, e.g. C:\SkunkWorksDev, for SkunkWorks 
development.</li>

<li class=Normal>Copy *.dll from your SkunkWorks installation folder to 
SkunkWorksDev.</li>

<li class=Normal>In SkunkWorksDev, rename Skapi.dll to SkapiCompat.dll, and 
SWFilter.dll to SWFilterCompat.dll.</li>

<li class=Normal>Start WinCVS.  Under <b>Admin</b> <span class=Symbol>4</span> 
<b>Preferences</b>, specify the following:</li>

	<table class=API style="margin: 6pt 0 6pt 24pt;">
	<col style="width: 1.5in;"/>
	<col/>
	<tr><td>Authentication:</td><td>pserver</td></tr>
	<tr><td>Path:</td><td>/cvsroot/skunkworks</td></tr>
	<tr><td>Host Address:</td><td>cvs.sourceforge.net</td></tr>
	<tr><td>User Name:</td><td>anonymous</td></tr>
	<tr><td>CVSROOT:</td><td>anonymous@cvs.sourceforge.net:/cvsroot.skunkworks</td></tr>
	</table>

<li class=Normal>In WinCVS, browse to your SkunkWorksDev folder.  There are four CVS 
modules available for checkout under the SkunkWorks project, as follows:</li>

	<table class=API style="margin: 6pt 0 6pt 24pt;">
	<col style="width: 1.5in;"/>
	<col/>
	<tr>
		<td><p class=Normal>Source</p></td>
		<td><p class=Normal>The main SkunkWorks source.</p></td>
	</tr>
	<tr>
		<td><p class=Normal>Libraries</p></td>
		<td><p class=Normal>The script libraries and samples included in 
		the SkunkWorks install package.</p></td>
	</tr>
	<tr>
		<td><p class=Normal>Docs</p></td>
		<td><p class=Normal>The SkunkWorks documentation.</p></td>
	</tr>
	<tr>
		<td><p class=Normal>Installer</p></td>
		<td><p class=Normal>Files needed for building and packaging a SkunkWorks 
		installer.</p></td>
	</tr>
	</table>
	
<p class=Normal>Use <b>Create</b> <span class=Symbol>4</span> <b>Checkout module</b> in WinCVS to 
check out each module in turn.  Place the checked-out modules under your SkunkWorksDev 
folder.  You needn't check out all of them, only the ones you're interested in.  This 
probably includes Source and perhaps Libraries if you want to customize SkunkNav, for 
instance.  Docs you might want to check out for reference even if you don't plan on 
writing any documentation.  Installer is really of interest only to official SkunkWorks 
release technicians and can safely be ignored by anybody else.</p>

<li class=Normal>If you haven't already done so, download <a target="SWOutlink" 
href="http://support.microsoft.com/default.aspx?scid=kb%3ben-us%3b223389">Scriptng.exe</a> 
and extract it into your Visual C++ Include folder (except for Ad1.lib, which 
goes in you VC++ Lib folder).  You will need these headers and libraries in 
order to build SwxScript.dll.  (If you don't have VC++ installed, you can skip 
this step.)</li>

<li class=Normal>After checking out the modules, it's time to do a build.  Open 
SwxScript.dsw in Visual C++.  (If you don't have VC++ installed, you can skip this 
step.)  On the Build menu, Set Active Configuration to Win32 Release MinDependency, 
then Build SwxScript.dll.  Exit VC++.</li>

<li class=Normal>Open CallPfn.dsw in Visual C++.  On the Build menu, Set Active 
Configuration to Win32 Release, then Build CallPfn.dll.  Exit VC++.</li>

<li class=Normal>Open SkunkWorksDev\Source\SWPlugin.vbg in Visual Basic and Make 
Project Group (on the File Menu).  Make sure "Use Default Build Options" is checked, 
and click Build.  When the build is finished, close VB.  Choose Yes if it asks you 
to save changes.</li>

<li class=Normal>Do the same with SkunkWorksDev\Source\SkunkWorks.vbg.  If you get a 
dialog asking whether you want to Break or Preserve compatibility, choose Preserve.  
When the build is finished, save and close as before.</li>

</ul>

<p class=Normal>You should now have a working SkunkWorks installation in your 
SkunkWorksDev folder.</p>

</body>

</html>
