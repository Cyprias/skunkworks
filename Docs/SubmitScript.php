<html id="TOCUserWare">

<head>
<title>SkunkWorks UserWare</title>
<link rel=stylesheet href="SkunkWorks.css">
</head>

<body class=Normal lang="EN-US">

<h1><a name="_Top"/>UserWare</h1>


<?php
require 'Common.php';

$uwi['szTitle'] = stripslashes($_POST['szTitle']);
$uwi['szVersion'] = stripslashes($_POST['szVersion']);
$uwi['szAuthor'] = stripslashes($_POST['szAuthor']);
$uwi['szDate'] = stripslashes($_POST['szDate']);
if ($uwi['szDate'] == '')
	$uwi['szDate'] = strftime("%b %d, %Y");
$uwi['szCategory'] = stripslashes($_POST['szCategory']);
if ($uwi['szCategory'] == '')
	$uwi['szCategory'] = 'util';
$uwi['szLanguage'] = stripslashes($_POST['szLanguage']);
$uwi['szDesc'] = stripslashes($_POST['szDesc']);
$uwi['szFile'] = strtolower($_FILES['szFile']['name']);

$szSubdir = $uwi['szCategory'];
$szFile = SzFileFromUwi($uwi);

if (move_uploaded_file($_FILES['szFile']['tmp_name'], $szFile))
	{
	$szXmlFile = SzRemoveExt($szFile) . ".xml";
	SaveUwiToFile($szXmlFile, $uwi);
	
	echo "<p>Your UserWare submission has been accepted.</p>";
	
	GenItemUwi($uwi);
	}
else
	{
	echo "<p>Your UserWare submission failed.</p>";
	echo "<p>Make sure your file is not too large (" . sprintf('%u', $_POST['MAX_FILE_SIZE']/1000) . "K max).</p>";
	}

?>

<p class="Normal" style="text-align: center;">
	<?php echo "<a href='UserWare.php?refresh#_$szCategory'>Return to UserWare</a>"; ?>
  <!--
	<button class="Up" 
	  hidefocus="true" tabindex="-1" 
	  onmousedown="OnMouseDown(this);"
	  onmouseleave="OnMouseLeave(this);"
	  onmouseenter="OnMouseEnter(this);"
	  onmouseup="OnMouseUp(this);"
	  onlosecapture="OnMouseUp(this);"
	  onclick=<?php echo "\"window.open('UserWare.php#_$szCategory', '_self');\"" ?>
	  >
		  <b>Return to UserWare</b>
	</button>
  -->
</p>

</body>

</html>
