<html id="TOCUserWare">

<head>
<title>SkunkWorks UserWare</title>
<link rel=stylesheet href="SkunkWorks.css">
</head>

<body class=Normal lang="EN-US">

<h1><a name="_Top"/>UserWare</h1>

<p class=Normal>This section contains scripts, libraries, addons, and utilities 
contributed by SkunkWorks users.  For support on these components, contact the 
individual component authors.</p>


<?php
require 'Common.php';

function GenCategory($szSubdir, $szDesc, $szComment)
	{
	global $szPathUserWare, $szUrlUserWare;
	
	echo "<h2><a name='_$szSubdir'/>$szDesc</h2>\n";
	
	$cuwi = 0;
	$szDir = $szPathUserWare . "/" . $szSubdir;
	if ($hdir = opendir($szDir))
		{
		while (false !== ($szFile = readdir($hdir)))
			{
			if (FSuffixMatchSz($szFile, ".xml"))
				{
				$rguwi[$cuwi] = UwiFromFile($szDir . '/' . $szFile);
				$cuwi++;
				}
			}
		closedir($hdir);
		}
	
	if ($szComment != "")
		{
		echo "<p class=Highlight>" . $szComment . "</p>";
		}
	
	if ($cuwi > 0)
		{
		if ($cuwi == 1)
			echo "<p class=Normal>One script has been submitted in this category.</p>";
		else
			echo "<p class=Normal>" . sprintf("%u", $cuwi) . " scripts have been submitted in this category.</p>";
		
		usort($rguwi, "RelUwi");
		$iuwi = 0;
		while ($iuwi < $cuwi)
			{
			GenItemUwi($rguwi[$iuwi]);
			$iuwi++;
			}
		}
	else
		{
		echo "<p class=Normal>No scripts have been submitted in this category.</p>";
		}
	echo "\n\n";
	}

GenCategory("combat", "Combat scripts, drain macros, and warbots", 
	"Unattended combat macroing is a violation of the Asheron's Call Code of Conduct.  " . 
	"Please do not use these scripts for UCM purposes.");
	
GenCategory("lib", "Function libraries", "");

GenCategory("money", "Moneymaking scripts", 
	"Note that moneymaking exploits tend to be nerfed fairly quickly, so scripts in this " . 
	"category that are more than a month or two old probably won't make money anymore " . 
	"(but may still be useful as coding examples).");
	
GenCategory("sample", "Sample scripts, tutorials, and other learning aids", "");

GenCategory("service", "Buffbots, portalbots, and other service bots", "");

GenCategory("util", "Utilities and miscellaneous addons", "");

?>


<h2><a name="_Submit"/>Submit a script</h2>

<p class=Normal>To submit a script, component, utility, or other item to UserWare, 
please fill out the following form.</p>

<p class=Normal>If you're submitting a new version of an existing item, use the 
same zip file name you used originally to replace the previous version.  Use a 
different zip file name to create a new item and keep the previous version available.</p>

<p class=Highlight>Please <b>do not</b> include spaces or apostrophes in the 
filename that you upload.  Such filenames break the script that generates this 
page and users will not be able to download your file.</p>

<form enctype="multipart/form-data" action="SubmitScript.php" method="post">
<table width="100%" style="table-layout: fixed; margin: 12pt 0 12pt 0;">
  <col width="200">
  <tr>
    <td>Script or package name:</td>
    <td><input type="text" name="szTitle"></td>
  </tr>
  <tr>
    <td>Version:</td>
    <td><input type="text" name="szVersion"></td>
  </tr>
  <tr>
    <td>Author:</td>
    <td><input type="text" name="szAuthor"></td>
  </tr>
<!--
  <tr>
    <td>Date:</td>
    <td><input type="text" name="szDate"></td>
  </tr>
-->
  <tr>
    <td>Category:</td>
    <td>
      <select name="szCategory">
        <option value="">Choose one
        <option value="combat">Combat
        <option value="lib">Libraries
        <option value="money">Moneymakers
        <option value="sample">Samples
        <option value="service">Service bots
        <option value="util">Utilities
      </select>
    </td>
  </tr>
  <tr>
    <td>Script language (if applicable):</td>
    <td>
      <select name="szLanguage">
        <option value="">
        <option value="JScript">JScript
        <option value="VBScript">VBScript
        <option value="Perl">Perl
        <option value="Python">Python
        <option value="CSharp">CSharp
        <option value="COM">COM
        <option value="">Other/NA
      </select>
    </td>
  </tr>
  <tr>
    <td>Script file (zipped):</td>
    <td>
      <input type="hidden" name="MAX_FILE_SIZE" value="600000">
      <input type="file" name="szFile">
    </td>
  </tr>
  <tr>
    <td>Description:</td>
    <td><textarea name="szDesc" style="width: 100%; height: 1in;"></textarea></td>
  </tr>
  <tr>
    <td></td>
    <td><p style="margin-top: 12pt;"><input type="submit" value="Submit"></p></td>
  </tr>
</table>
</form>

</body>

</html>
