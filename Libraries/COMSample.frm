VERSION 5.00
Begin VB.Form formCOMSample 
   Caption         =   "COM Sample"
   ClientHeight    =   1185
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   2130
   LinkTopic       =   "Form1"
   ScaleHeight     =   1185
   ScaleWidth      =   2130
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   1680
      Top             =   120
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Hello"
      Height          =   375
      Left            =   600
      TabIndex        =   0
      Top             =   360
      Width           =   855
   End
End
Attribute VB_Name = "formCOMSample"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' Our handle on the SkunkWorks API object:
Private skapi As SkapiCls

' SkunkWorks Events object for receiving game events:
Private WithEvents skev As SkevCls
Attribute skev.VB_VarHelpID = -1

Private Sub Form_Load()
    
'   Instantiate the skapi.
    Set skapi = New SkapiCls
    
'   Grab a handle to the event source object.
    Set skev = skapi.skev
    
    Timer1.Enabled = True
    
End Sub

Private Sub Timer1_Timer()
    
'   Must call WaitEvent frequently so events can fire
'   and to keep event queue from overflowing.
    Call skapi.WaitEvent(0)
    
End Sub

Private Sub Command1_Click()
    
'   Send some output to the in-game chat window when user clicks a button on our form.
    Call skapi.OutputLine("Hello World!")
    
End Sub


' Handler functions for events raised by skapi.skev.
' No need to register these explicitly; VB does that for us when we say WithEvents.

Private Sub skev_OnLogon(ByVal acoChar As SkunkWorks.AcoCls)
    Call skapi.OutputLine(acoChar.szName + " has logged in.")
End Sub

Private Sub skev_OnEndPortalSelf()
    Call skapi.OutputLine(skapi.acoChar.szName + " has emerged from portal space at " + _
      skapi.acoChar.maploc.sz)
End Sub


