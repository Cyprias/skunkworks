// CommandSample.js
// Demonstrates the use of the hotkey command interface.


var szCmdReceived;

function main()
	{
//	Prepare to handle command events.
//	This causes the Command panel to appear in game.
	var handler = new CommandHandler();
	skapi.AddHandler(evidOnCommand, handler);
	
	skapi.OutputLine("Welcome to the CommandSample demo script.", opmChatWnd);
	skapi.OutputLine("Click the Command button on the Decal bar and enter a JScript expression.", opmChatWnd);
	skapi.OutputLine("Enter 'quit' to exit.", opmChatWnd);
	
//	Process events until the 'quit' command is given.
	var fQuit = false;
	while (!fQuit)
		{
	//	Wait for a command.
		szCmdReceived = "";
		if (skapi.WaitEvent(1000, wemSpecific, evidOnCommand) == evidOnCommand)
			{
		//	Carry out the command.  In a real script this would be where you'd 
		//	summon your portal, make your potions, launch your attack, or whatever 
		//	else the user has just told you to do via szCmdReceived.
			switch (szCmdReceived.toLowerCase())
				{
			case "quit":
				fQuit = true;
				break;
				
			case "help":
			case "?":
				skapi.OutputLine("Enter a JScript expression, or 'quit' to exit.", opmChatWnd);
				break;
				
			default:
			//	Evaluate the command string as a JScript expression.
				var e, szResult;
				try
					{
					var vResult;
					eval("vResult = " + szCmdReceived);
					if (vResult == undefined)
						szResult = "OK";
					else
						{
						szResult = vResult.toString();
						if (szResult == "")
							szResult = "OK";
						}
					}
				catch(e)
					{
					szResult = e.description;
					}
			//	Report the result.
				skapi.OutputLine(szResult, opmChatWnd);
				break;
				}
			}
		}
	
//	Remove the handler.
	skapi.RemoveHandler(evidOnCommand, handler);
	
	skapi.OutputLine("Thanks for trying the CommandSample demo.", opmChatWnd);
	}

// Constructor function for the event handler object.
function CommandHandler()
	{
	this.OnCommand = OnCommand;
	}

// Handler function for the OnCommand event.
function OnCommand(szCmd)
	{
//	Actually carrying out commands inside an event handler is considered bad form.
//	Best practice is to just save away the command string, get out quickly, 
//	and let the main loop do the work.
	szCmdReceived = szCmd;
	}



