// ControlSample.js
// Demonstrates the use of SkunkWorks control panel APIs.


// Decal view schema for the sample control panel.
// See http://www.cs.colostate.edu/~cousinea/vb_decal_docs/controls/controls.htm 
// for documentation.
var szPanelName = "ControlSample";
var szSchema =
	"<view title='" + szPanelName + "' iconlibrary='Inject.dll' icon='128'\n" + 
	"  width='180' height='150'>\n" + 
	"  <control progid='DecalControls.FixedLayout'>\n" + 
	"    <control name='editMsg'  progid='DecalControls.Edit' left='10' top='0'  width='150' height='20' \n" + 
	"      text='Type some text here.'/>\n" + 
	"    <control name='btnEcho'  progid='DecalControls.PushButton' text='Echo' left='10' top='30'  width='50' height='20' />\n" + 
	"    <control name='btnOnOff' progid='DecalControls.PushButton' text='On'   left='10' top='60' width='50' height='20' />\n" + 
	"    <control name='btnQuit'  progid='DecalControls.PushButton' text='Quit' left='10' top='90' width='50' height='20' />\n" + 
	"  </control>\n" + 
	"</view>";

var fQuit = false;

function main()
	{
//	Put up the control panel.
	skapi.ShowControls(szSchema, true);
	
//	Prepare to handle control events.
	var handler = new ControlHandler();
	skapi.AddHandler(evidOnControlEvent, handler);
	
//	Process events until the Quit button is clicked.
	while (!fQuit)
		skapi.WaitEvent(1000);
	
//	Remove the control handler.
	skapi.RemoveHandler(evidOnControlEvent, handler);
	
//	Take down the panel.
	skapi.RemoveControls(szPanelName);
	}

// Constructor function for the event handler object.
function ControlHandler()
	{
	this.OnControlEvent = OnControlEvent;
	}

// Handler function for the OnCommand event.
function OnControlEvent(szPanel, szControl, dictSzValue)
	{
//	Make sure it's our panel that was clicked on.
	if (szPanel == szPanelName)
		{
	//	Figure out which control was clicked.
		switch (szControl)
			{
		case "btnEcho":
		//	Echo the edit text to the chat window.
			var szMsg = dictSzValue("editMsg");
			skapi.OutputLine(szMsg, opmChatWnd);
			break;
			
		case "btnOnOff":
		//	What does the button caption say now?
			var szCaption = dictSzValue("btnOnOff");
		//	Toggle it.
			if (szCaption == "On")
				szCaption = "Off";
			else
				szCaption = "On";
		//	Store it back.
			skapi.SetControlProperty(szPanelName, "btnOnOff", "Text", szCaption);
			break;
			
		case "btnQuit":
		//	Quit the demo.
			fQuit = true;
			break;
			}
		}
	}


