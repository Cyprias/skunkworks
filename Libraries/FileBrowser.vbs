Option Explicit
Class FileBrowserCls

' This module implements an in-game file browser similar to the standard 
' Windows Open File dialog, with a list of filenames in a given folder plus 
' controls for navigating up and down the folder hierarchy.  The only public 
' method is FileBrowser.SzGetFilename, which brings up the browser panel, 
' handles all control events, and returns the chosen filename.  The global 
' FileBrowser object itself is created and initialized automatically on script 
' load.


'-------------------
' Class boilerplate
'-------------------

Private Sub Class_Initialize()
	
	Set fso = CreateObject("Scripting.FileSystemObject")
	
End Sub

Private Sub Class_Terminate()
	
End Sub


'---------
' Methods
'---------

' Bring up the in-game FileBrowser dialog and process control events.  
' szPath specifies the initial folder for browsing.  You can pass in "." or 
' "" (the empty string) to use the current folder.  szFilter specifies which 
' subset of files to display.  It can be a simple wildcard spec such as 
' "*.txt", an arbitrary regular expression such as ".*\.(vbs|js)", or "*" 
' or "" (the empty string) to show all files.  If the user highlights a 
' filename and clicks OK, SzGetFilename returns the full path to the chosen 
' file.  If the user clicks Cancel, SzGetFilename returns the empty string.
Public Function SzGetFilename(szPath, szFilter)
	
'	Show the browser panel.
	szPanelName = "FileBrowser"
	Call skapi.ShowControls( _
	  fso.BuildPath(fso.BuildPath(skapi.szDirSkunkWorks, _
	  "Libraries"), "FileBrowser.xml"), True)
	
'	Fill it with filenames.
    Call skapi.CallPanelFunction("FileBrowser", "Init", _
      fso.GetAbsolutePathName(szPath), szFilter)
	
'	Prepare to handle control events.
	Call skapi.AddHandler(evidOnControlEvent, Me)
	
	SzGetFilename = ""
	
	Do Until skapi.acoChar Is Nothing
	'	Wait for a control to be clicked.
		szControl = ""
		Call skapi.WaitEvent(10000, wemSpecific, evidOnControlEvent)
		Select Case szControl
			
		Case "btnOK"
			SzGetFilename = dictSzValue("textFile")
			Exit Do
			
		Case "btnCancel"
			Exit Do
			
		End Select
	Loop
	
'	Free temporary storage.
	Set dictSzValue = Nothing
	
'	Remove the control handler.
	Call skapi.RemoveHandler(evidOnControlEvent, Me)
	
End Function


'----------------
' Event handlers
'----------------

Public Function OnControlEvent(szPanelA, szControlA, dictSzValueA)
	
	If szPanelA = szPanelName Then
	'	Save away the event params for processing by SzGetFilename.
		szControl = szControlA
		Set dictSzValue = dictSzValueA
	End If
	
End Function


'----------
' Privates
'----------

Private fso						' Scripting.FileSystemObject.
Private szPanelName				' Browser panel title string.
Private szControl, dictSzValue	' Temporary event handler info.


End Class


' Instantiate the global object.
Public FileBrowser
Set FileBrowser = New FileBrowserCls


