Option Explicit


' Display a pop-up input box with a prompt and an edit control.
' If the user hits Enter, return the contents of the edit control.
' If the user hits Cancel, return the empty string.
Public Function SzGetInput(szTitle, szPrompt, szDefault)
	
	Dim szSchema : szSchema = _
		"<view title='' iconlibrary='Inject.dll' icon='128'" + vbCrlf + _
		"  width='270' height='100'>" + vbCrlf + _
		"  <control progid='DecalControls.FixedLayout'>" + vbCrlf + _
		"    <control name='textPrompt' progid='DecalControls.StaticText' " + vbCrlf + _
		"      left='25' top='0'  width='220' height='16' " + vbCrlf + _
		"      text=''/>" + vbCrlf + _
		"    <control name='editInput'  progid='DecalControls.Edit' " + vbCrlf + _
		"      left='13' top='20'  width='244' height='18' " + vbCrlf + _
		"      marginx='12' marginy='2' imageportalsrc='4726' text=''/>" + vbCrlf + _
		"    <control progid='DecalControls.StaticText' " + vbCrlf + _
		"      left='25' top='44'  width='220' height='16' " + vbCrlf + _
		"      text='Press Esc to cancel.'/>" + vbCrlf + _
		"  </control>" + vbCrlf + _
		"</view>"
	
	Dim xmldoc : Set xmldoc = CreateObject("Msxml2.DOMDocument.4.0")
	Call xmldoc.LoadXml(szSchema)
	Call xmldoc.setProperty("SelectionLanguage", "XPath")
	
	Dim elem : Set elem = xmldoc.documentElement
	Call elem.setAttribute("title", szTitle)
	
	Set elem = xmldoc.selectSingleNode("//control[@name='textPrompt']")
	Call elem.setAttribute("text", szPrompt)
	
	Set elem = xmldoc.selectSingleNode("//control[@name='editInput']")
	Call elem.setAttribute("text", szDefault)
	
	Call skapi.ShowControls(xmldoc.xml, True)
	Call skapi.Hotkey("{Esc}", True)
	Call skapi.GetControlProperty(szTitle, "editInput", "Capture")
	
	Dim handler : Set handler = New InputBoxHandlerCls
	handler.szTitle = szTitle
	handler.fQuit = False
	handler.szResult = ""
	
	Call skapi.AddHandler(evidOnControlEvent, handler)
	Call skapi.AddHandler(evidOnHotkey, handler)
	
	Do Until handler.fQuit
		Call skapi.WaitEvent(100)
	Loop
	
	Call skapi.RemoveHandler(evidOnControlEvent, handler)
	Call skapi.RemoveHandler(evidOnHotkey, handler)
	
	Call skapi.Hotkey("{Esc}", False)
	Call skapi.RemoveControls(szTitle)
	
	SzGetInput = handler.szResult
	
End Function

Class InputBoxHandlerCls
	
	Public szTitle
	Public fQuit
	Public szResult
	
	Public Function OnControlEvent(szPanel, szControl, dictSzValue)
		If szPanel = szTitle And szControl = "editInput" Then
			fQuit = True
			szResult = dictSzValue("editInput")
		End If
	End Function
	
	Public Function OnHotkey(cmid, vk)
		
		If cmid = "{Esc}" Then
			fQuit = True
			szResult = ""
		End If
		
	End Function
	
End Class


