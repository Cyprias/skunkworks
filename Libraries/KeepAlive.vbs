Option Explicit

' Here's a simple script for staying logged in indefinitely.  
' Add this file to any VBScript project and it will automatically hit 
' the Reset View (keypad 0) key every ten minutes or so as your toon 
' fidgets.  Note that if you sit down or lie down for more than 20 
' minutes, this might not work.  I chose Reset View as least likely to 
' interfere with the actual operation of your script.  If for some reason 
' you hate third-person view, just change it to cmidFirstPersonView 
' (or any of the other camera commands) and the effect should be the same.

Class KeepaliveCls
	
	Private timer

	Public Function OnAnimationSelf(dwA, dwB, dwC, dwD, dwE)
		
		If timer.cmsec > 600000 Then
			Call skapi.Keys(cmidResetView)
			timer.cmsec = 0
		End If
		
	End Function

	Private Sub Class_Initialize()
		Call skapi.AddHandler(evidOnAnimationSelf, Me)
		Set timer = skapi.TimerNew()
	End Sub
	
End Class

Dim keepalive : Set keepalive = New KeepaliveCls

