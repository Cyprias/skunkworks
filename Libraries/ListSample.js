// ListSample.js

// This sample script demonstrates the use of notebook and list controls, 
// along with the SkunkWorks APIs used to manipulate them.
// 
// It does this by displaying a notebook control with one tab for each pack 
// in your inventory.  Each page of the notebook has a list control for 
// displaying the pack contents.  Each such list has three columns: 
// the item's icon, the item's OID, and a description of the item.
// Clicking on a pack tab in the notebook switches to that page and 
// updates the list with the current pack contents.  Clicking on an item 
// in the list selects that ACO in your inventory.
// 
// By itself this isn't a very useful thing to do.  The main point is to 
// demonstrate the use of the controls and APIs.


var szMyPanel = "ListSample";

function main()
	{
//	Put up our control panel.
	ShowControls();
	
//	Prepare to handle events.
	var h = new Handlers();
	skapi.AddHandler(evidOnControlEvent, h);
	skapi.AddHandler(evidOnControlProperty, h);
	
	while (true)
		{
		skapi.WaitEvent(1000);
		if (h.fRefresh)
			{
		//	User click a pack tab.
		//	Refresh the displayed pack contents.
			RefreshList(h.ipackSelected);
			h.fRefresh = false;
			}
		else if (h.iitemClicked != iitemNil)
			{
		//	User clicked a list item.
		//	Read out the OID of the item by accessing the list's Data property.
		//	Note that we have to encode the column & row arguments to the 
		//	Data property as part of the szProperty string.
		//	The OID is in column 1.
			skapi.GetControlProperty(szMyPanel, "listPack" + h.ipackSelected, 
			  "Data(1, " + h.iitemClicked + ")");
		//	Wait for the result to come back.
			skapi.WaitEvent(2000, wemSpecific, evidOnControlProperty)
		//	Select the clicked-on item.
			skapi.SelectAco(skapi.AcoFromOid(h.oidSelected));
			h.iitemClicked = iitemNil;
			}
		}
	
//	Clean up event handlers.
	skapi.RemoveHandler(evidOnControlEvent, h);
	skapi.RemoveHandler(evidOnControlProperty, h);
	
//	Take down our control panel.
	skapi.RemoveControls(szMyPanel);
	}

function Handlers()
	{
	this.ipackSelected = 0;
	this.iitemClicked = iitemNil;
	this.oidSelected = oidNil;
	this.fRefresh = false;
	
	this.OnControlEvent = OnControlEvent;
	this.OnControlProperty = OnControlProperty;
	}

function OnControlEvent(szPanel, szControl, dictSzValue)
	{
	skapi.OutputLine("OnControlEvent(" + szPanel + ", " + szControl + ")", opmDebugLog);

//	User clicked some control on some panel.  Was it our panel?
	if (szPanel == szMyPanel)
		{
	//	Yes; which control was it?
		if (szControl == "nbkPacks")
			{
		//	User clicked on a pack tab in the notebook control.
		//	The "value" of the notebook control is a string encoding the index 
		//	of the selected tab (i.e. nbkPacks.ActiveTab).
			var szIpack = dictSzValue("nbkPacks");
			skapi.OutputLine("szIpack = " + szIpack, opmDebugLog);
			this.ipackSelected = parseInt(szIpack);
			skapi.OutputLine("this.ipackSelected = " + this.ipackSelected, opmDebugLog);
		//	Make a note to refresh the displayed contents of that pack.
			this.fRefresh = true;
			}
		else if (szControl == "listPack" + this.ipackSelected)
			{
		//	User clicked on a list item in the list control.
		//	The "value" of the list control is a string encoding the coords 
		//	of the item last clicked on.
			var szIcolIrow = dictSzValue("listPack" + this.ipackSelected);
			skapi.OutputLine("szIcolIrow = " + szIcolIrow, opmDebugLog);
		//	Discard the column part and save away the row part.
			this.iitemClicked = parseInt(szIcolIrow.split(",")[1]);
			skapi.OutputLine("this.iitemClicked = " + this.iitemClicked, opmDebugLog);
			}
		}
	}

function OnControlProperty(szPanel, szControl, szProperty, vValue)
	{
	skapi.OutputLine("OnControlProperty(" + szPanel + ", " + szControl + ", " + 
		szProperty + ", " + vValue.toString() + ")", opmDebugLog);

//	Some control on some panel was interrogated by GetControlProperty.
//	Was it our panel?
	if (szPanel == szMyPanel)
		{
	//	Yes; which control was it?
		if (szControl == "listPack" + this.ipackSelected)
			{
		//	This is the response to our list.Data() OID request in main().
		//	Save away the returned OID.
			this.oidSelected = parseInt(vValue, 16);
			skapi.OutputLine("this.oidSelected = " + this.oidSelected.toString(16), opmDebugLog);
			}
		}
	}

function ShowControls()
	{
//	Construct a view schema containing one notebook control.
	var szSchema = 
		"<view title='" + szMyPanel + "' icon='7089' \n" + 
		" left='200' top='25' width='340' height='200'>\n" + 
		" <control progid='DecalControls.FixedLayout'>\n" + 
		"  <control name='nbkPacks' progid='DecalControls.Notebook' " + 
		"   left='0' top='0' width='340' height='160'>\n";
	
//	Add a page to the notebook for each pack in inventory.
	var ipack;
	for (ipack = 0; ipack < skapi.cpack; ipack++)
		{
		var acoPack = skapi.AcoFromIpackIitem(ipack, iitemNil);
		szSchema = szSchema + "    <page label='Pack " + ipack + "'>\n";
		szSchema = szSchema + "      <control progid='DecalControls.FixedLayout'>\n";
		
	//	Each page contains one list control with three columns, for icons, OIDs, 
	//	and descriptive text.
		szSchema = szSchema + "        <control name='listPack" + ipack + "' progid='DecalControls.List' \n" + 
		                      "         left='0' top='0' width='340' height='150'>\n";
		szSchema = szSchema + "          <column progid='DecalControls.IconColumn'/>\n";
		szSchema = szSchema + "          <column progid='DecalControls.TextColumn' fixedwidth='70'/>\n";
		szSchema = szSchema + "          <column progid='DecalControls.TextColumn' fixedwidth='250'/>\n";
		
		if (ipack == 0)
			{
		//	Preload main pack contents via XML.
			var iitem;
			for (iitem = 0; iitem < acoPack.citemContents; iitem++)
				{
				var acoItem = acoPack.coacoContents(iitem);
			//	Append row data containing item's icon, OID, and description.
			//	This row data syntax is a SkunkWorks extension to the pure Decal 
			//	view schema syntax.
				szSchema = szSchema + "          <row>\n";
				szSchema = szSchema + "            <data>" + (0x06000000 + acoItem.icon).toString() + "</data>\n";
				szSchema = szSchema + "            <data>" + SzOidFromAco(acoItem) + "</data>\n";
				szSchema = szSchema + "            <data>" + SzDescFromAco(acoItem) + "</data>\n";
				szSchema = szSchema + "          </row>\n";
				}
			}
		
	//	Close out the list control.
		szSchema = szSchema + "        </control>\n";
		
	//	Close out the page.
		szSchema = szSchema + "      </control>\n";
		szSchema = szSchema + "    </page>\n";
		}
	
//	Close out the notebook and view.
	szSchema = szSchema + "  </control>\n";
	szSchema = szSchema + " </control>\n";
	szSchema = szSchema + "</view>\n";
	
//	skapi.OutputSz(szSchema, opmDebugLog);
	
//	Display the controls.
	skapi.ShowControls(szSchema, true);
	}

function RefreshList(ipack)
	{
	var szControl = "listPack" + ipack;
	
//	Clear the list by calling its Clear method.
//	Use GetControlProperty for this since the syntax is the same as a property call.
	skapi.GetControlProperty(szMyPanel, szControl, "Clear()");
	
//	Add new contents.
	var acoPack = skapi.AcoFromIpackIitem(ipack, iitemNil);
	var iitem;
	for (iitem = 0; iitem < acoPack.citemContents; iitem++)
		{
		var acoItem = acoPack.coacoContents(iitem);
		
	//	Create a new row by calling the AddRow method.
	//	(Note that AddRow returns the index of the new row via OnControlProperty, 
	//	but in this case we don't care since it's the same as iitem.)
		skapi.GetControlProperty(szMyPanel, szControl, "AddRow()");
		
	//	Fill in the row data.
	//	Note that we have to encode the column & row arguments to the 
	//	Data property as part of the szProperty string.
	//	Also Decal requires that Data's third argument be 1 for icons, 
	//	or very bad things happen.
		skapi.SetControlProperty(szMyPanel, szControl, "Data(0, " + iitem + ", 1)", 0x06000000 + acoItem.icon);
	//	For non-icon columns, the third argument isn't needed.
		skapi.SetControlProperty(szMyPanel, szControl, "Data(1, " + iitem + ")", SzOidFromAco(acoItem));
		skapi.SetControlProperty(szMyPanel, szControl, "Data(2, " + iitem + ")", SzDescFromAco(acoItem));
		}
	}

function SzOidFromAco(aco)
	{
	return aco.oid.toString(16).toUpperCase();
	}

function SzDescFromAco(aco)
	{
	if (aco.citemStack == 1)
		return aco.szName;
	else
		return aco.citemStack.toString() + " " + aco.szPlural;
	}


