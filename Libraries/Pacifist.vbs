Option Explicit


' Container class for event handler methods:
Class HandlerCls
'   This method gets called when you enter or exit combat mode.
    Public Function OnCombatMode(fOn)
        If fOn Then
        '   Entering combat mode.
            Call skapi.OutputLine("I'm a pacifist!")
        '   Send the keystroke to toggle us back to peace mode.
            Call skapi.Keys(cmidToggleCombat)
        '   Tell the main loop to exit.
            fQuit = True
        End If
    End Function
End Class

' Create an instance of the handler class.
Dim handler : Set handler = New HandlerCls

' Flag to tell us when to quit the main loop.
Dim fQuit : fQuit = False

' The Main function gets called when you click Go on the console.
Sub Main
    
'   Tell SkunkWorks we want to receive OnCombatMode events.
    Call skapi.AddHandler(evidOnCombatMode, handler)
    
'   Loop until the event handler fires once.
    Do Until fQuit
    '   Give control to SkunkWorks so it can process events.
        Call skapi.WaitEvent(100)
    Loop
    
'   Tell SkunkWorks we don't want to receive OnCombatMode events anymore.
    Call skapi.RemoveHandler(evidOnCombatMode, handler)
    
End Sub


