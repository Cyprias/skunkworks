Option Explicit
'----------------------------------------------------
' RouteMaker.vbs by Greg Kusnick (goric@cognomen.com)
'----------------------------------------------------


'------------------------------------------------------------------------------
' 
' Sample script demonstrating the use of SkunkNav.js.
' This script does two things:
' 
' 1. It lets you browse for route files and try them out, automatically moving 
'    your character from point to point along the route.
' 
' 2. It lets you record a route to a file by manually moving your character 
'    to each waypoint in turn.
' 
' To run the script, make sure Decal is running, then start AC and log in your 
' character manually.  When the miniconsole appears, choose RouteMaker.swx 
' from the dropdown list, click Go, then follow the on-screen instructions.
' 
'------------------------------------------------------------------------------


Const clrBtnEnabled = &H7BF3F7
Const clrBtnDisabled = &H808080

Const clrTextEnabled = &H000000
Const clrTextDisabled = &H808080

Const clrMaplocSel = &HFFFFFF
Const clrMaplocUnsel = &HC0C0C0


Class RouteMakerCls


Private fso
Private szPanelName
Private dictSzRoute
Private fDirty
Private fInPlayback

Private Sub Class_Initialize()
	
	Set fso = CreateObject("Scripting.FileSystemObject")
	
	Set dictSzRoute = Nothing
	fDirty = False
	fInPlayback = False
	
	szFolderCurMbr = ""
	szFileCurMbr = ""
	Set routeCurMbr = Nothing
	imaplocSelMbr = -1
	
End Sub


Public Sub Main()
	
'	Load the panel schema as an XML DOM document.
	Dim xmldoc : Set xmldoc = CreateObject("Msxml2.DOMDocument.4.0")
	xmldoc.async = false
	Call xmldoc.Load("RouteMaker.xml")
	Call xmldoc.setProperty("SelectionLanguage", "XPath")
	szPanelName = xmldoc.documentElement.getAttribute("title")
	
'	Show the panel.
	Call skapi.ShowControls(xmldoc.xml, True)
	
'	Prepare to handle control events.
	Call skapi.AddHandler(evidOnControlEvent, Me)
	
'	Initialize the search path.
	szFolderCur = fso.GetAbsolutePathName(".")
	
	Do Until skapi.acoChar Is Nothing
	'	Wait for a control to be clicked.
		szControl = ""
		Call skapi.WaitEvent(10000, wemSpecific, evidOnControlEvent)
		Select Case szControl
			
		Case "lboxFolder"
			Dim szFolder : szFolder = dictSzValue("lboxFolder")
			If szFolder <> "" Then
				szFolderCur = fso.GetAbsolutePathName(fso.BuildPath(szFolderCur, szFolder))
			End If
			
		Case "lboxFile"
			Dim szFile : szFile = dictSzValue("lboxFile")
			If Left(szFile, 1) = "+" Then
				Call NewRouteFile()
			ElseIf szFile <> "" Then
				szFileCur = fso.GetAbsolutePathName(fso.BuildPath(szFolderCur, szFile))
			End If
			
		Case "lboxRoute"
			Dim szRoute : szRoute = dictSzValue("lboxRoute")
			Call skapi.SetControlProperty(szPanelName, "chkReverse", "Checked", False)
			If Left(szRoute, 1) = "+" Then
				Call NewRoute()
			ElseIf szRoute <> "" Then
				Set routeCur = dictSzRoute(szRoute)
			End If
			
		Case "chkReverse"
			Dim fReverse : fReverse = CBool(dictSzValue("chkReverse"))
			szRoute = dictSzValue("lboxRoute")
			If szRoute <> "" Then
				If fReverse Then
					Set routeCur = dictSzRoute(szRoute).RouteReverse()
				Else
					Set routeCur = dictSzRoute(szRoute)
				End If
			End If
			
		Case "btnGo"
			If routeCur Is Nothing Then
				Call skapi.OutputLine("Please choose a route.")
				Call ac.Beep(1)
			Else
				Call PlayRoute(True)
			End If
			
		Case "btnResume"
			If routeCur Is Nothing Then
				Call skapi.OutputLine("Please choose a route.")
				Call ac.Beep(1)
			ElseIf routeCur.imaploc < routeCur.cmaploc Then
				Call PlayRoute(False)
			End If
			
		Case "listPoints"
			Dim szSel : szSel = dictSzValue("listPoints")
			Dim rex : Set rex = New RegExp
			rex.Pattern = "(\d+),(\d+)"
			szSel = rex.Replace(szSel, "$2")
			imaplocSel = CLng(szSel)
			
		Case "editTimeout"
			Dim szTimeout : szTimeout = dictSzValue("editTimeout")
			If Not (routeCur Is Nothing) Then
				Dim csecTimeout
				If szTimeout = "" Then
					csecTimeout = 0
				ElseIf IsNumeric(szTimeout) Then
					csecTimeout = CLng(szTimeout)
					If csecTimeout < 0 Then csecTimeout = 0
				Else
					csecTimeout = routeCur.csecTimeout
					Call ShowTimeout()
				End If
				If routeCur.csecTimeout <> csecTimeout Then
					routeCur.csecTimeout = csecTimeout
					fDirty = True
				End If
			End If
			
		Case "lboxRecall"
			Dim szRecall : szRecall = dictSzValue("lboxRecall")
			If Not (routeCur Is Nothing) Then
				If routeCur.szRecall <> szRecall Then
					routeCur.szRecall = szRecall
					fDirty = True
				End If
			End If
			
		Case "btnAdd"
			If Not (routeCur Is Nothing) Then
				Call routeCur.AddMaplocCur(imaplocSel)
				Call ShowRouteData(imaplocSel + 1)
				fDirty = True
			End If
			
		Case "btnAddSel"
			If Not (routeCur Is Nothing) Then
				Dim acoSel : Set acoSel = skapi.acoSelected
				If acoSel Is Nothing Then
					Call skapi.OutputLine("Nothing selected.  Please select a door or portal and try again.")
					Call ac.Beep(1)
				ElseIf FAcoIsDoor(acoSel) Then
					Call routeCur.AddDoor(acoSel, imaplocSel)
					Call ShowRouteData(imaplocSel + 1)
					fDirty = True
				ElseIf FAcoIsPortal(acoSel) Then
					Call routeCur.AddPortal(acoSel, imaplocSel)
					Call ShowRouteData(imaplocSel + 1)
					fDirty = True
				Else
					Call skapi.OutputLine("Selected object is not a door or portal.  Please select a door or portal and try again.")
					Call ac.Beep(1)
				End If
			End If
			
		Case "btnDelete"
			If Not (routeCur Is Nothing) Then
				If imaplocSel < routeCur.cmaploc Then
					Call routeCur.DeleteMaploc(imaplocSel)
					Call ShowRouteData(imaplocSel - 1)
					fDirty = True
				End If
			End If
			
		Case "btnSave"
			If Not (dictSzRoute Is Nothing) Then
				Call FSaveRoutes()
			End If
			
		Case "btnQuit"
			If fDirty Then Call FSaveRoutes()
			Exit Do
			
		End Select
	Loop
	
'	Remove the control handler.
	Call skapi.RemoveHandler(evidOnControlEvent, Me)
	
'	Take down the panel.
	Call skapi.RemoveControls(szPanelName)
	
End Sub


Public szControl
Public dictSzValue

Public Function OnControlEvent(szPanelA, szControlA, dictSzValueA)
	If szPanelA = szPanelName Then
		If szControlA = "btnStop" And fInPlayback Then
			Call routeCur.StopRoute()
		End If
	'	Save away the event params for processing by main loop.
		szControl = szControlA
		Set dictSzValue = dictSzValueA
	End If
End Function

Public Function OnLocChangeSelf(maploc)
	
	If fInPlayback Then
		imaplocSel = routeCur.imaploc
		Call ScrollToMaploc(imaplocSel)
	End If
	
End Function


Private szFolderCurMbr

Private Property Get szFolderCur()
	szFolderCur = szFolderCurMbr
End Property

Private Property Let szFolderCur(szNew)
	
	Call skapi.OutputLine("szFolderCur <- " + SzQuote(szNew), opmDebugLog)
	
	If Not FEqSzI(szNew, szFolderCurMbr) Then
		szFolderCurMbr = szNew
		
		Call skapi.OutputLine("Please wait...")
		
		Call skapi.SetControlProperty(szPanelName, "textPath", "Text", szFolderCurMbr)
		
		Call skapi.GetControlProperty(szPanelName, "lboxFolder", "Clear()")
		Call skapi.GetControlProperty(szPanelName, "lboxFile", "Clear()")
		Call EnableText("textFolder", False)
		Call EnableText("textFile", False)
		
		szFileCur = ""
		
		Call BuildFolderList()
		Call BuildFileList("")
		
		Call skapi.OutputLine("Ready.")
	End If
	
End Property


Private szFileCurMbr

Private Property Get szFileCur()
	szFileCur = szFileCurMbr
End Property

Private Property Let szFileCur(szNew)
	
	Call skapi.OutputLine("szFileCur <- " + SzQuote(szNew), opmDebugLog)
	
	If Not FEqSzI(szNew, szFileCurMbr) Then
		If fDirty Then Call FSaveRoutes()
		
		szFileCurMbr = szNew
		fDirty = False
		
		Call skapi.GetControlProperty(szPanelName, "lboxRoute", "Clear()")
		Call EnableText("textRoute", False)
		
		Set routeCur = Nothing
		
		If szFileCurMbr <> "" Then
			Call skapi.OutputLine("Please wait...")
			
			Call skapi.SetControlProperty(szPanelName, "textPath", "Text", szFileCurMbr)
			
			Call BuildRouteList("")
			
			Call skapi.OutputLine("Ready.")
		End If
	End If
	
End Property


Private Property Get irouteCur()
	
	irouteCur = -1
	
	If Not (routeCur Is Nothing) Then
		Dim rgroute : rgroute = dictSzRoute.Items
		Dim iroute
		For iroute = 0 To UBound(rgroute)
			If rgroute(iroute) Is routeCur Then
				irouteCur = iroute
				Exit For
			End If
		Next
	End If
	
End Property

Private Property Let irouteCur(iroute)
	
	Call skapi.OutputLine("irouteCur <- " + CStr(iroute), opmDebugLog)
	
	Call skapi.SetControlProperty(szPanelName, "lboxRoute", "Selected", CLng(iroute))
	
End Property


Private routeCurMbr

Private Property Get routeCur()
	Set routeCur = routeCurMbr
End Property

Private Property Set routeCur(routeNew)

	If Not (routeNew Is routeCurMbr) Then
		If fDirty Then Call FSaveRoutes()
		
		Set routeCurMbr = routeNew
		fDirty = False
		
		Call ShowRouteData(-1)
		Call EnableBtn("btnGo", Not (routeCurMbr Is Nothing))
	End If
	
End Property


Private imaplocSelMbr

Private Property Get imaplocSel()
	imaplocSel = imaplocSelMbr
End Property

Private Property Let imaplocSel(imaploc)
	
	If imaplocSelMbr <> imaploc Then
		If imaplocSelMbr >= 0 Then
			Call skapi.SetControlProperty(szPanelName, "listPoints", _
			  "Color(0, " + CStr(imaplocSelMbr) + ")", clrMaplocUnsel)
		End If
		
		imaplocSelMbr = imaploc
		
		If imaplocSelMbr >= 0 Then
			Call skapi.SetControlProperty(szPanelName, "listPoints", _
			  "Color(0, " + CStr(imaplocSelMbr) + ")", clrMaplocSel)
			Call EnableBtn("btnAdd", True)
			Call EnableBtn("btnAddSel", True)
			Call EnableBtn("btnDelete", imaplocSelMbr < routeCur.cmaploc)
		Else
			Call EnableBtn("btnAdd", False)
			Call EnableBtn("btnAddSel", False)
			Call EnableBtn("btnDelete", False)
		End If
	End If
	
End Property


Private Function BuildFolderList()
	
	Call skapi.GetControlProperty(szPanelName, "lboxFolder", "Clear()")
	Call EnableText("textFolder", False)
	
	Call skapi.GetControlProperty(szPanelName, "lboxFolder", _
	  "AddChoice(" + SzQuote("..") + ")")
	
	Dim cofldr : Set cofldr = fso.GetFolder(szFolderCur).SubFolders
	Dim fldr
	For Each fldr In cofldr
		Dim szFolder : szFolder = fldr.Name
	'	Add the folder to the listbox.
		Call skapi.GetControlProperty(szPanelName, "lboxFolder", _
		  "AddChoice(" + SzQuote(szFolder) + ")")
	Next
	
	Call EnableText("textFolder", True)
	
End Function

Private Function BuildFileList(szFileSel)
	
	Call skapi.GetControlProperty(szPanelName, "lboxFile", "Clear()")
	Call EnableText("textFile", False)
	
	Dim ifileSel : ifileSel = -1
	Dim ifile : ifile = 0
	Dim cofil : Set cofil = fso.GetFolder(szFolderCur).Files
	Dim fil
	For Each fil In cofil
		Dim szFile : szFile = fil.Name
		If FEqSzI(Right(szFile, 6), ".route") Then
		'	Add the filename to the listbox.
			Call skapi.GetControlProperty(szPanelName, "lboxFile", _
			  "AddChoice(" + SzQuote(szFile) + ")")
			If FEqSzI(szFile, szFileSel) Then
				ifileSel = ifile
			End If
			ifile = ifile + 1
		End If
	Next
	
	Call skapi.GetControlProperty(szPanelName, "lboxFile", _
	  "AddChoice(" + SzQuote("+ New route file") + ")")
	
	Call EnableText("textFile", True)
	
	If ifileSel <> -1 Then
		Call skapi.SetControlProperty(szPanelName, "lboxFile", _
		  "Selected", CLng(ifileSel))
	End If
	
End Function

Private Function BuildRouteList(szRouteSel)
	
	Call skapi.OutputLine("Reading " + szFileCur + "...")
	Set routeCur = Nothing
	
'	Create a dictionary of routes in the given file.
	Set dictSzRoute = CreateObject("Scripting.Dictionary")
	dictSzRoute.CompareMode = vbTextCompare
	fDirty = False
	
'	Clear the route listbox.
	Call skapi.GetControlProperty(szPanelName, "lboxRoute", "Clear()")
	Call skapi.SetControlProperty(szPanelName, "chkReverse", "Checked", False)
	Call EnableBtn("btnGo", False)

	On Error Resume Next
	
	Dim ts : Set ts = fso.OpenTextFile(szFileCur, ForReading)
	
	If Err.Number <> 0 Then
		Call skapi.OutputLine("Error opening file " + szFileCur + ": " + Err.Description)
		Exit Function
	End If
	On Error Goto 0
	
	Dim irouteSel : irouteSel = -1
	Dim iroute : iroute = 0
	Do Until ts.AtEndOfStream
		Dim route : Set route = RouteNew()
		
		On Error Resume Next
		
		Call route.Read(ts)
		
		If Err.Number <> 0 Then
			Call skapi.OutputLine("Error reading file " + szFileCur + ": " + Err.Description)
			Exit Function
		End If
		On Error Goto 0
		
		If route.cmaploc > 0 Or route.szName <> "" Then
		'	Add the route to the dictionary.
			Dim szRoute : szRoute = route.szName
			If szRoute = "" Then
				If iroute = 0 Then
					szRoute = "Default route"
				Else
					szRoute = "Route #" + CStr(iroute + 1)
				End If
			End If
			Set dictSzRoute(szRoute) = route
			
		'	Add the route to the listbox.
			Call skapi.GetControlProperty(szPanelName, "lboxRoute", _
			  "AddChoice(" + SzQuote(szRoute) + ")")
			
			If szRouteSel <> "" Then
				If FEqSzI(szRoute, szRouteSel) Then irouteSel = iroute
			Else
				If irouteSel = -1 Then irouteSel = iroute
			End If
			
			iroute = iroute + 1
		End If
	Loop
	
	Call ts.Close()
	
	Call skapi.GetControlProperty(szPanelName, "lboxRoute", _
	  "AddChoice(" + SzQuote("+ New route") + ")")
	
	Call EnableText("textRoute", True)
	
	If irouteSel <> -1 Then
		irouteCur = irouteSel
	End If
	
End Function


Private Function NewRouteFile()
	
	Dim szFile : szFile = SzGetInput("Input", "Type a filename and press Enter:", "")
	Call skapi.ShowControls(szPanelName, True)
	If szFile = "" Then
		Call skapi.SetControlProperty(szPanelName, "lboxFile", "Selected", CLng(-1))
		Exit Function
	End If
	
	If Not FEqSzI(Right(szFile, 6), ".route") Then
		szFile = szFile + ".route"
	End If
	
	On Error Resume Next
	
	Dim szFull : szFull = fso.BuildPath(szFolderCur, szFile)
	Call fso.CreateTextFile(szFull, False).Close()
	
	If Err.Number <> 0 Then
		Call skapi.OutputLine("Error creating file " + szFull + ": " + Err.Description)
		Call skapi.SetControlProperty(szPanelName, "lboxFile", "Selected", CLng(-1))
		Exit Function
	End If
	
	Call BuildFileList(szFile)
	
End Function

Private Function NewRoute()
	
	Dim szRoute : szRoute = SzGetInput("Input", "Type a route name and press Enter:", "")
	Call skapi.ShowControls(szPanelName, True)
	If szRoute = "" Then
		Call skapi.SetControlProperty(szPanelName, "lboxRoute", "Selected", CLng(-1))
		Exit Function
	End If
	
	Dim route : Set route = RouteNew()
	route.szName = szRoute
	
	Dim ts : Set ts = fso.OpenTextFile(szFileCur, ForAppending)
	Call route.Write(ts)
	Call ts.Close()
	
	Call BuildRouteList(szRoute)
	
End Function

Private Function ShowRouteData(imaplocSelNew)
	
	imaplocSel = -1
	
	Call ShowTimeout()
	
	Dim isel
	If routeCur Is Nothing Then
		isel = -1
	Else
		Select Case routeCur.szRecall
		Case "@house recall"
			isel = 1
		Case "@lifestone"
			isel = 2
		Case Else
			isel = 0
		End Select
	End If
	Call skapi.SetControlProperty(szPanelName, "lboxRecall", "Selected", CLng(isel))
	
	Call skapi.GetControlProperty(szPanelName, "listPoints", "Clear")
	
	If Not (routeCur Is Nothing) Then
		Dim imaploc, maploc
		For imaploc = 0 To routeCur.cmaploc - 1
			Set maploc = routeCur.MaplocGet(imaploc)
			Dim sz : sz = maploc.sz(3)
			If maploc.landblock = landblockAco Then
				sz = sz + " ; " + maploc.szAco
			End If
			Call skapi.GetControlProperty(szPanelName, "listPoints", "AddRow")
			Call skapi.SetControlProperty(szPanelName, "listPoints", _
			  "Color(0, " + CStr(imaploc) + ")", clrMaplocUnsel)
			Call skapi.SetControlProperty(szPanelName, "listPoints", _
			  "Data(0, " + CStr(imaploc) + ")", sz)
		Next
		
		Call skapi.GetControlProperty(szPanelName, "listPoints", "AddRow")
		Call skapi.SetControlProperty(szPanelName, "listPoints", _
		  "Color(0, " + CStr(imaploc) + ")", clrMaplocUnsel)
		Call skapi.SetControlProperty(szPanelName, "listPoints", _
		  "Data(0, " + CStr(imaploc) + ")", "Stop")
		
		If imaplocSelNew >= 0 And imaplocSelNew <= routeCur.cmaploc Then
			imaplocSel = imaplocSelNew
			Call ScrollToMaploc(imaplocSel)
		Else
			imaplocSel = routeCur.cmaploc
		End If
	End If
	
End Function

Private Function ShowTimeout()

	Dim sz
	If routeCur Is Nothing Then
		sz = ""
	ElseIf routeCur.csecTimeout = 0 Then
		sz = ""
	Else
		sz = CStr(routeCur.csecTimeout)
	End If
	Call skapi.SetControlProperty(szPanelName, "editTimeout", "Text", sz)
	
End Function


Private Function ScrollToMaploc(imaploc)
	
	Dim imaplocTop
	If imaploc < 2 Then
		imaplocTop = 0
	Else
		imaplocTop = imaploc - 2
	End If
	Call skapi.GetControlProperty(szPanelName, "listPoints", _
	  "JumpToPosition(" + CStr(imaplocTop) + ")")
	
End Function


Private Function FSaveRoutes()
	
	Dim szFile : szFile = SzGetInput("Save", "Press Enter to save route file:", _
	  fso.GetFileName(szFileCur))
	Call skapi.ShowControls(szPanelName, True)
	If szFile = "" Then
		FSaveRoutes = False
		Exit Function
	End If
	
	If fDirty And Not (routeCur Is Nothing) Then
		If Not IsNull(routeCur.routeRev) Then
			Set routeCur.routeRev.routeRev = routeCur
		End If
	End If
	
	Dim szFull : szFull = fso.BuildPath(szFolderCur, szFile)
	Dim ts : Set ts = fso.CreateTextFile(szFull, True)
	
	If dictSzRoute.Count > 0 Then
		Dim rgroute : rgroute = dictSzRoute.Items
		Dim iroute, route
		For iroute = 0 To UBound(rgroute)
			Set route = rgroute(iroute)
			Call route.Write(ts)
			Call ts.WriteLine("")
		Next
	End If
	
	Call ts.Close()
	
	Call skapi.OutputLine(szFull + " has been saved.")
	fDirty = False
	
	If Not FEqSzI(szFull, szFileCur) Then
		Call BuildFileList(szFile)
	End If
	
	FSaveRoutes = True
	
End Function


Private Function PlayRoute(fGo)
	
    Call EnableBtn("btnGo", False)
    Call EnableBtn("btnResume", False)
	
	Call EnableBtn("btnStop", True)
	
	Call skapi.AddHandler(evidOnLocChangeSelf, Me)
	
	fInPlayback = True
	
	If fGo Then
		Call routeCur.Go()
	Else
		Call routeCur.ResumeRoute()
	End If
	
	fInPlayback = False
	
	Call skapi.RemoveHandler(evidOnLocChangeSelf, Me)
	
	Call EnableBtn("btnStop", False)
	
    Call EnableBtn("btnGo", True)
	
	imaplocSel = routeCur.imaploc
	
    If routeCur.imaploc = routeCur.cmaploc Then
		Call skapi.OutputLine("Done.")
	Else
		Call skapi.OutputLine("Route was interrupted.")
	    Call EnableBtn("btnResume", True)
	End If
	
End Function


Private Function EnableBtn(szBtn, fEnable)
	
	If fEnable Then
	    Call skapi.SetControlProperty(szPanelName, szBtn, "TextColor", clrBtnEnabled)
	Else
	    Call skapi.SetControlProperty(szPanelName, szBtn, "TextColor", clrBtnDisabled)
	End If
	
End Function

Private Function EnableText(szText, fEnable)
	
	If fEnable Then
	    Call skapi.SetControlProperty(szPanelName, szText, "TextColor", clrTextEnabled)
	Else
	    Call skapi.SetControlProperty(szPanelName, szText, "TextColor", clrTextDisabled)
	End If
	
End Function


End Class


Sub Main()
	
	Dim routemaker : Set routemaker = New RouteMakerCls
	Call routemaker.Main
	
End Sub

' Return True iff sz1 matches sz2 using case-insensitive comparison.
Public Function FEqSzI(sz1, sz2)
	
	FEqSzI = (StrComp(sz1, sz2, vbTextCompare) = 0)
	
End Function


