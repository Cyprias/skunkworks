
// -------------------------------------------------------------------
// !!!!!!!!!!!!!!!!!!!!!!   R E A D   T H I S   !!!!!!!!!!!!!!!!!!!!!!
// -------------------------------------------------------------------
// !!!  This file is generated automatically by SkapiDefsCs.xsl.   !!!
// !!!    Any edits you make to it by hand WILL be overwritten.    !!!
// !!!         Make your changes to SkapiDefs.xml instead.         !!!
// -------------------------------------------------------------------

namespace SkapiDefs
{

// arc: Action result codes
class arc
{
public const int arcSuccess                     =                           0;	// Your spellcasting attempt (or other action) succeeded.
public const int arcCombatMode                  =                           2;	// One of the parties entered combat mode.
public const int arcChargedTooFar               =                          28;	// You charged too far!
public const int arcTooBusy                     =                          29;	// You're too busy!
public const int arcCantMoveToObject            =                          57;	// Unable to move to object!
public const int arcCanceled                    =                          81;	// One of the parties canceled or moved out of range.
public const int arcFatigued                    =                        1015;	// You are too fatigued to attack!
public const int arcOutOfAmmo                   =                        1016;	// You are out of ammunition!
public const int arcMissleMisfire               =                        1017;	// Your missile attack misfired!
public const int arcImpossibleSpell             =                        1018;	// You've attempted an impossible spell path!
public const int arcDontKnowSpell               =                        1022;	// You don't know that spell!
public const int arcIncorrectTarget             =                        1023;	// Incorrect target type.
public const int arcOutOfComps                  =                        1024;	// You don't have all the components for this spell.
public const int arcNotEnoughMana               =                        1025;	// You don't have enough Mana to cast this spell.
public const int arcFizzle                      =                        1026;	// Your spell fizzled.
public const int arcNoTarget                    =                        1027;	// Your spell's target is missing!
public const int arcSpellMisfire                =                        1028;	// Your projectile spell mislaunched!
public const int arcSummonFailure               =                        1188;	// You fail to summon the portal!
public const int arcNotAvailable                =                        1323;	// That person is not available.
};

// attr: Attribute IDs
class attr
{
public const int attrNil                        =                           0;	// No attribute.
public const int attrStrength                   =                           1;
public const int attrEndurance                  =                           2;
public const int attrQuickness                  =                           3;
public const int attrCoordination               =                           4;
public const int attrFocus                      =                           5;
public const int attrSelf                       =                           6;
public const int attrMax                        =                           7;
};

// bpart: Body part IDs
class bpart
{
public const int bpartHead                      =                           0;
public const int bpartChest                     =                           1;
public const int bpartAbdomen                   =                           2;
public const int bpartUpperArm                  =                           3;
public const int bpartLowerArm                  =                           4;
public const int bpartHand                      =                           5;
public const int bpartUpperLeg                  =                           6;
public const int bpartLowerLeg                  =                           7;
public const int bpartFoot                      =                           8;
};

// chop: Character option masks
class chop
{
public const int chopRepeatAttacks              =                  0x00000002;	// Automatically Repeat Attacks
public const int chopIgnoreAllegiance           =                  0x00000004;	// Ignore Allegiance Requests
public const int chopIgnoreFellowship           =                  0x00000008;	// Ignore Fellowship Requests
public const int chopAcceptGifts                =                  0x00000040;	// Let Other Players Give You Items
public const int chopKeepTargetInView           =                  0x00000080;	// Keep Combat Targets in View
public const int chopTooltips                   =                  0x00000100;	// Display 3D Tooltips
public const int chopDeceive                    =                  0x00000200;	// Attempt to Deceive Other Players
public const int chopRunAsDefault               =                  0x00000400;	// Run as Default Movement
public const int chopStayInChat                 =                  0x00000800;	// Stay in Chat Mode After Sending a Message
public const int chopAdvancedCombat             =                  0x00001000;	// Advanced Combat Interface
public const int chopAutoTarget                 =                  0x00002000;	// Auto Target
public const int chopVividTargeting             =                  0x00008000;	// Vivid Targeting Indicator
public const int chopIgnoreTrade                =                  0x00020000;	// Ignore All Trade Requests
public const int chopShareXP                    =                  0x00040000;	// Share Fellowship Experience
public const int chopAcceptCorpsePerm           =                  0x00080000;	// Accept Corpse-Looting Permissions
public const int chopShareLoot                  =                  0x00100000;	// Share Fellowship Loot
public const int chopStretchUI                  =                  0x00200000;	// Stretch UI
public const int chopShowCoords                 =                  0x00400000;	// Show Coordinates By the Radar
public const int chopSpellDurations             =                  0x00800000;	// Display Spell Durations
public const int chopDisableHouseFX             =                  0x02000000;	// Disable House Restriction Effects
public const int chopDragStartsTrade            =                  0x04000000;	// Drag Item to Player Opens Trade
public const int chopShowAllegianceLogons       =                  0x08000000;	// Show Allegiance Logons
public const int chopUseChargeAttack            =                  0x10000000;	// Use Charge Attack
public const int chopAutoFellow                 =                  0x20000000;	// Automatically Accept Fellowship Requests
public const int chopAllegianceChat             =                  0x40000000;	// Listen to Allegiance Chat
public const int chopCraftingDialog             =                  0x80000000;	// Use Crafting Chance of Success Dialog
public const int chopAcceptAllegiance           =                  0x00000004;	// (Old name for compatibility)
public const int chopAcceptFellowship           =                  0x00000008;	// (Old name for compatibility)
};

// chop2: Character option masks (extended)
class chop2
{
public const int chop2AlwaysDaylight            =                  0x00000001;	// Always Daylight Outdoors
public const int chop2ShowDateOfBirth           =                  0x00000002;	// Allow Others to See Your Date of Birth
public const int chop2ShowChessRank             =                  0x00000004;	// Allow Others to See Your Chess Rank
public const int chop2ShowFishingSkill          =                  0x00000008;	// Allow Others to See Your Fishing Skill
public const int chop2ShowDeaths                =                  0x00000010;	// Allow Others to See Your Number of Deaths
public const int chop2ShowAge                   =                  0x00000020;	// Allow Others to See Your Age
public const int chop2Timestamps                =                  0x00000040;	// Display Timestamps
public const int chop2SalvageMultiple           =                  0x00000080;	// Salvage Multiple Materials at Once
};

// cmc: Chat message colors
class cmc
{
public const int cmcGreen                       =                           0;
public const int cmcWhite                       =                           2;	// Local broadcast chat.
public const int cmcYellow                      =                           3;
public const int cmcDarkYellow                  =                           4;
public const int cmcMagenta                     =                           5;
public const int cmcRed                         =                           6;
public const int cmcLightBlue                   =                           7;
public const int cmcRose                        =                           8;
public const int cmcPaleRose                    =                           9;
public const int cmcDimWhite                    =                          12;
public const int cmcCyan                        =                          13;
public const int cmcPaleBlue                    =                          14;	// SkunkWorks default color.
public const int cmcBlue                        =                          17;	// Spellcasting.
public const int cmcCoral                       =                          22;
};

// chrm: Chat recipient masks
class chrm
{
public const int chrmLocal                      =                  0x00000001;	// Local chat broadcast
public const int chrmEmote                      =                  0x00000002;	// Local emote broadcast (@e)
public const int chrmFellowship                 =                  0x00000800;	// Fellowship broadcast (@f)
public const int chrmVassals                    =                  0x00001000;	// Patron to vassal (@v)
public const int chrmPatron                     =                  0x00002000;	// Vassal to patron (@p)
public const int chrmMonarch                    =                  0x00004000;	// Follower to monarch (@m)
public const int chrmCovassals                  =                  0x01000000;	// Covassal broadcast (@c)
public const int chrmAllegiance                 =                  0x02000000;	// Allegiance broadcast by monarch or speaker (@a)
};

// cmid: Command IDs
class cmid
{
// cmid definitions appear here in the same order they appear on AC's Keyboard Configuration screen.  Where the name of a cmid differs from the descriptive text on that screen, the text appears in the Description column below.
// cmid values may be concatenated using the + operator to specify a sequence of commands.
public const string cmidMovementJump            =            "{MovementJump}";
public const string cmidMovementForward         =         "{MovementForward}";
public const string cmidMovementBackup          =          "{MovementBackup}";
public const string cmidMovementTurnLeft        =        "{MovementTurnLeft}";
public const string cmidMovementTurnRight       =       "{MovementTurnRight}";
public const string cmidMovementWalkMode        =        "{MovementWalkMode}";
public const string cmidMovementStop            =            "{MovementStop}";
public const string cmidMovementStrafeRight     =     "{MovementStrafeRight}";
public const string cmidMovementStrafeLeft      =      "{MovementStrafeLeft}";
public const string cmidMovementRunLock         =         "{MovementRunLock}";
public const string cmidReady                   =                   "{Ready}";
public const string cmidCrouch                  =                  "{Crouch}";
public const string cmidSitting                 =                 "{Sitting}";
public const string cmidSleeping                =                "{Sleeping}";
public const string cmidSelectionSelf           =           "{SelectionSelf}";
public const string cmidSelectionPreviousCompassItem="{SelectionPreviousCompassItem}";
public const string cmidSelectionNextCompassItem="{SelectionNextCompassItem}";
public const string cmidSelectionUseNextUnopenedCorpse="{SelectionUseNextUnopenedCorpse}";
public const string cmidSelectionGive           =           "{SelectionGive}";
public const string cmidSelectionDrop           =           "{SelectionDrop}";
public const string cmidSelectionPickUp         =         "{SelectionPickUp}";
public const string cmidSelectionSplitStack     =     "{SelectionSplitStack}";
public const string cmidSelectionUseClosestUnopenedCorpse="{SelectionUseClosestUnopenedCorpse}";
public const string cmidSelectionClosestCompassItem="{SelectionClosestCompassItem}";
public const string cmidSelectionLastAttacker   =   "{SelectionLastAttacker}";
public const string cmidSelectionClosestMonster = "{SelectionClosestMonster}";
public const string cmidSelectionPreviousItem   =   "{SelectionPreviousItem}";
public const string cmidSelectionNextItem       =       "{SelectionNextItem}";
public const string cmidSelectionPreviousSelection="{SelectionPreviousSelection}";
public const string cmidSelectionClosestItem    =    "{SelectionClosestItem}";
public const string cmidSelectionPreviousMonster="{SelectionPreviousMonster}";
public const string cmidSelectionNextMonster    =    "{SelectionNextMonster}";
public const string cmidSelectionClosestPlayer  =  "{SelectionClosestPlayer}";
public const string cmidSelectionPreviousPlayer = "{SelectionPreviousPlayer}";
public const string cmidSelectionNextPlayer     =     "{SelectionNextPlayer}";
public const string cmidSelectionPreviousFellow = "{SelectionPreviousFellow}";
public const string cmidSelectionNextFellow     =     "{SelectionNextFellow}";
public const string cmidSelectionExamine        =        "{SelectionExamine}";
public const string cmidUSE                     =                     "{USE}";
public const string cmidToggleAbusePanel        =        "{ToggleAbusePanel}";
public const string cmidToggleCharacterInfoPanel="{ToggleCharacterInfoPanel}";
public const string cmidTogglePositiveEffectsPanel="{TogglePositiveEffectsPanel}";
public const string cmidToggleNegativeEffectsPanel="{ToggleNegativeEffectsPanel}";
public const string cmidToggleLinkStatusPanel   =   "{ToggleLinkStatusPanel}";
public const string cmidToggleUrgentAssistancePanel="{ToggleUrgentAssistancePanel}";
public const string cmidToggleVitaePanel        =        "{ToggleVitaePanel}";
public const string cmidToggleSocialPanel       =       "{ToggleSocialPanel}";
public const string cmidToggleSpellManagementPanel="{ToggleSpellManagementPanel}";
public const string cmidToggleSkillManagementPanel="{ToggleSkillManagementPanel}";
public const string cmidToggleMapPanel          =          "{ToggleMapPanel}";
public const string cmidToggleHousePanel        =        "{ToggleHousePanel}";
public const string cmidToggleGameplayOptionsPanel="{ToggleGameplayOptionsPanel}";
public const string cmidToggleCharacterOptionsPanel="{ToggleCharacterOptionsPanel}";
public const string cmidToggleConfigOptionsPanel="{ToggleConfigOptionsPanel}";
public const string cmidToggleRadarPanel        =        "{ToggleRadarPanel}";
public const string cmidToggleKeyboardPanel     =     "{ToggleKeyboardPanel}";
public const string cmidCaptureScreenshot       =       "{CaptureScreenshot}";
public const string cmidToggleHelp              =              "{ToggleHelp}";
public const string cmidTogglePluginManager     =     "{TogglePluginManager}";
public const string cmidToggleAllegiancePanel   =   "{ToggleAllegiancePanel}";
public const string cmidToggleFellowshipPanel   =   "{ToggleFellowshipPanel}";
public const string cmidToggleSpellbookPanel    =    "{ToggleSpellbookPanel}";
public const string cmidToggleSpellComponentsPanel="{ToggleSpellComponentsPanel}";
public const string cmidToggleAttributesPanel   =   "{ToggleAttributesPanel}";
public const string cmidToggleSkillsPanel       =       "{ToggleSkillsPanel}";
public const string cmidToggleWorldPanel        =        "{ToggleWorldPanel}";
public const string cmidToggleOptionsPanel      =      "{ToggleOptionsPanel}";
public const string cmidToggleInventoryPanel    =    "{ToggleInventoryPanel}";
public const string cmidLOGOUT                  =                  "{LOGOUT}";
public const string cmidSelectQuickSlot_6       =       "{SelectQuickSlot_6}";
public const string cmidSelectQuickSlot_7       =       "{SelectQuickSlot_7}";
public const string cmidSelectQuickSlot_8       =       "{SelectQuickSlot_8}";
public const string cmidSelectQuickSlot_9       =       "{SelectQuickSlot_9}";
public const string cmidSelectQuickSlot_1       =       "{SelectQuickSlot_1}";
public const string cmidSelectQuickSlot_2       =       "{SelectQuickSlot_2}";
public const string cmidSelectQuickSlot_3       =       "{SelectQuickSlot_3}";
public const string cmidSelectQuickSlot_4       =       "{SelectQuickSlot_4}";
public const string cmidSelectQuickSlot_5       =       "{SelectQuickSlot_5}";
public const string cmidCreateShortcut          =          "{CreateShortcut}";
public const string cmidUseQuickSlot_1          =          "{UseQuickSlot_1}";
public const string cmidUseQuickSlot_2          =          "{UseQuickSlot_2}";
public const string cmidUseQuickSlot_3          =          "{UseQuickSlot_3}";
public const string cmidUseQuickSlot_4          =          "{UseQuickSlot_4}";
public const string cmidUseQuickSlot_5          =          "{UseQuickSlot_5}";
public const string cmidUseQuickSlot_6          =          "{UseQuickSlot_6}";
public const string cmidUseQuickSlot_7          =          "{UseQuickSlot_7}";
public const string cmidUseQuickSlot_8          =          "{UseQuickSlot_8}";
public const string cmidUseQuickSlot_9          =          "{UseQuickSlot_9}";
public const string cmidToggleChatEntry         =         "{ToggleChatEntry}";
public const string cmidSTART_COMMAND           =           "{START_COMMAND}";
public const string cmidMonarchReply            =            "{MonarchReply}";
public const string cmidPatronReply             =             "{PatronReply}";
public const string cmidReply                   =                   "{Reply}";
public const string cmidEnterChatMode           =           "{EnterChatMode}";
public const string cmidCombatToggleCombat      =      "{CombatToggleCombat}";
public const string cmidCombatDecreaseAttackPower="{CombatDecreaseAttackPower}";
public const string cmidCombatIncreaseAttackPower="{CombatIncreaseAttackPower}";
public const string cmidCombatLowAttack         =         "{CombatLowAttack}";
public const string cmidCombatMediumAttack      =      "{CombatMediumAttack}";
public const string cmidCombatHighAttack        =        "{CombatHighAttack}";
public const string cmidCombatDecreaseMissileAccuracy="{CombatDecreaseMissileAccuracy}";
public const string cmidCombatIncreaseMissileAccuracy="{CombatIncreaseMissileAccuracy}";
public const string cmidCombatAimLow            =            "{CombatAimLow}";
public const string cmidCombatAimMedium         =         "{CombatAimMedium}";
public const string cmidCombatAimHigh           =           "{CombatAimHigh}";
public const string cmidUseSpellSlot_10         =         "{UseSpellSlot_10}";
public const string cmidUseSpellSlot_1          =          "{UseSpellSlot_1}";
public const string cmidUseSpellSlot_2          =          "{UseSpellSlot_2}";
public const string cmidUseSpellSlot_3          =          "{UseSpellSlot_3}";
public const string cmidUseSpellSlot_4          =          "{UseSpellSlot_4}";
public const string cmidUseSpellSlot_11         =         "{UseSpellSlot_11}";
public const string cmidUseSpellSlot_12         =         "{UseSpellSlot_12}";
public const string cmidCombatPrevSpellTab      =      "{CombatPrevSpellTab}";
public const string cmidCombatNextSpellTab      =      "{CombatNextSpellTab}";
public const string cmidCombatPrevSpell         =         "{CombatPrevSpell}";
public const string cmidCombatCastCurrentSpell  =  "{CombatCastCurrentSpell}";
public const string cmidCombatNextSpell         =         "{CombatNextSpell}";
public const string cmidCombatFirstSpellTab     =     "{CombatFirstSpellTab}";
public const string cmidCombatLastSpellTab      =      "{CombatLastSpellTab}";
public const string cmidCombatFirstSpell        =        "{CombatFirstSpell}";
public const string cmidCombatLastSpell         =         "{CombatLastSpell}";
public const string cmidUseSpellSlot_5          =          "{UseSpellSlot_5}";
public const string cmidUseSpellSlot_6          =          "{UseSpellSlot_6}";
public const string cmidUseSpellSlot_7          =          "{UseSpellSlot_7}";
public const string cmidUseSpellSlot_8          =          "{UseSpellSlot_8}";
public const string cmidUseSpellSlot_9          =          "{UseSpellSlot_9}";
public const string cmidAFKState                =                "{AFKState}";
public const string cmidAtEaseState             =             "{AtEaseState}";
public const string cmidATOYOT                  =                  "{ATOYOT}";
public const string cmidAkimbo                  =                  "{Akimbo}";
public const string cmidAkimboState             =             "{AkimboState}";
public const string cmidBeckon                  =                  "{Beckon}";
public const string cmidBeSeeingYou             =             "{BeSeeingYou}";
public const string cmidBlowKiss                =                "{BlowKiss}";
public const string cmidBowDeep                 =                 "{BowDeep}";
public const string cmidBowDeepState            =            "{BowDeepState}";
public const string cmidCheer                   =                   "{Cheer}";
public const string cmidClapHands               =               "{ClapHands}";
public const string cmidClapHandsState          =          "{ClapHandsState}";
public const string cmidCrossArmsState          =          "{CrossArmsState}";
public const string cmidCringe                  =                  "{Cringe}";
public const string cmidCry                     =                     "{Cry}";
public const string cmidCurtseyState            =            "{CurtseyState}";
public const string cmidDrudgeDance             =             "{DrudgeDance}";
public const string cmidDrudgeDanceState        =        "{DrudgeDanceState}";
public const string cmidHaveASeat               =               "{HaveASeat}";
public const string cmidHaveASeatState          =          "{HaveASeatState}";
public const string cmidHeartyLaugh             =             "{HeartyLaugh}";
public const string cmidHelper                  =                  "{Helper}";
public const string cmidKneel                   =                   "{Kneel}";
public const string cmidKneelState              =              "{KneelState}";
public const string cmidKnock                   =                   "{Knock}";
public const string cmidLaugh                   =                   "{Laugh}";
public const string cmidLeanState               =               "{LeanState}";
public const string cmidMeditateState           =           "{MeditateState}";
public const string cmidMimeDrink               =               "{MimeDrink}";
public const string cmidMimeEat                 =                 "{MimeEat}";
public const string cmidMock                    =                    "{Mock}";
public const string cmidNod                     =                     "{Nod}";
public const string cmidNudgeLeft               =               "{NudgeLeft}";
public const string cmidNudgeRight              =              "{NudgeRight}";
public const string cmidPlead                   =                   "{Plead}";
public const string cmidPleadState              =              "{PleadState}";
public const string cmidPoint                   =                   "{Point}";
public const string cmidPointState              =              "{PointState}";
public const string cmidPointDown               =               "{PointDown}";
public const string cmidPointDownState          =          "{PointDownState}";
public const string cmidPointLeft               =               "{PointLeft}";
public const string cmidPointLeftState          =          "{PointLeftState}";
public const string cmidPointRight              =              "{PointRight}";
public const string cmidPointRightState         =         "{PointRightState}";
public const string cmidPossumState             =             "{PossumState}";
public const string cmidPray                    =                    "{Pray}";
public const string cmidPrayState               =               "{PrayState}";
public const string cmidReadState               =               "{ReadState}";
public const string cmidSalute                  =                  "{Salute}";
public const string cmidSaluteState             =             "{SaluteState}";
public const string cmidScanHorizon             =             "{ScanHorizon}";
public const string cmidScratchHead             =             "{ScratchHead}";
public const string cmidScratchHeadState        =        "{ScratchHeadState}";
public const string cmidShakeHead               =               "{ShakeHead}";
public const string cmidShakeFist               =               "{ShakeFist}";
public const string cmidShakeFistState          =          "{ShakeFistState}";
public const string cmidShiver                  =                  "{Shiver}";
public const string cmidShiverState             =             "{ShiverState}";
public const string cmidShoo                    =                    "{Shoo}";
public const string cmidShrug                   =                   "{Shrug}";
public const string cmidSitState                =                "{SitState}";
public const string cmidSitBackState            =            "{SitBackState}";
public const string cmidSitCrossleggedState     =     "{SitCrossleggedState}";
public const string cmidSlouch                  =                  "{Slouch}";
public const string cmidSlouchState             =             "{SlouchState}";
public const string cmidSmackHead               =               "{SmackHead}";
public const string cmidSnowAngelState          =          "{SnowAngelState}";
public const string cmidSpit                    =                    "{Spit}";
public const string cmidSurrender               =               "{Surrender}";
public const string cmidSurrenderState          =          "{SurrenderState}";
public const string cmidTalktotheHandState      =      "{TalktotheHandState}";
public const string cmidTapFoot                 =                 "{TapFoot}";
public const string cmidTapFootState            =            "{TapFootState}";
public const string cmidTeapot                  =                  "{Teapot}";
public const string cmidThinkerState            =            "{ThinkerState}";
public const string cmidWarmHands               =               "{WarmHands}";
public const string cmidWave                    =                    "{Wave}";
public const string cmidWaveState               =               "{WaveState}";
public const string cmidWaveHigh                =                "{WaveHigh}";
public const string cmidWaveLow                 =                 "{WaveLow}";
public const string cmidWoah                    =                    "{Woah}";
public const string cmidWoahState               =               "{WoahState}";
public const string cmidWinded                  =                  "{Winded}";
public const string cmidWindedState             =             "{WindedState}";
public const string cmidYawnStretch             =             "{YawnStretch}";
public const string cmidYMCA                    =                    "{YMCA}";
public const string cmidCameraMoveToward        =        "{CameraMoveToward}";
public const string cmidCameraMoveAway          =          "{CameraMoveAway}";
public const string cmidCameraActivateAlternateMode="{CameraActivateAlternateMode}";
public const string cmidCameraInstantMouseLook  =  "{CameraInstantMouseLook}";
public const string cmidCameraRotateLeft        =        "{CameraRotateLeft}";
public const string cmidCameraRotateRight       =       "{CameraRotateRight}";
public const string cmidCameraRotateUp          =          "{CameraRotateUp}";
public const string cmidCameraRotateDown        =        "{CameraRotateDown}";
public const string cmidCameraViewDefault       =       "{CameraViewDefault}";
public const string cmidCameraViewFirstPerson   =   "{CameraViewFirstPerson}";
public const string cmidCameraViewLookDown      =      "{CameraViewLookDown}";
public const string cmidCameraViewMapMode       =       "{CameraViewMapMode}";
public const string cmidAltEnter                =                "{AltEnter}";
public const string cmidAltTab                  =                  "{AltTab}";
public const string cmidAltF4                   =                   "{AltF4}";
public const string cmidCtrlShiftEsc            =            "{CtrlShiftEsc}";
public const string cmidPointerX                =                "{PointerX}";
public const string cmidPointerY                =                "{PointerY}";
public const string cmidSelectLeft              =              "{SelectLeft}";
public const string cmidSelectRight             =             "{SelectRight}";
public const string cmidSelectMid               =               "{SelectMid}";
public const string cmidSelectDblLeft           =           "{SelectDblLeft}";
public const string cmidSelectDblRight          =          "{SelectDblRight}";
public const string cmidSelectDblMid            =            "{SelectDblMid}";
public const string cmidScrollUp                =                "{ScrollUp}";
public const string cmidScrollDown              =              "{ScrollDown}";
public const string cmidCursorCharLeft          =          "{CursorCharLeft}";
public const string cmidCursorCharRight         =         "{CursorCharRight}";
public const string cmidCursorPreviousLine      =      "{CursorPreviousLine}";
public const string cmidCursorNextLine          =          "{CursorNextLine}";
public const string cmidCursorPreviousPage      =      "{CursorPreviousPage}";
public const string cmidCursorNextPage          =          "{CursorNextPage}";
public const string cmidCursorWordLeft          =          "{CursorWordLeft}";
public const string cmidCursorWordRight         =         "{CursorWordRight}";
public const string cmidCursorStartOfLine       =       "{CursorStartOfLine}";
public const string cmidCursorStartOfDocument   =   "{CursorStartOfDocument}";
public const string cmidCursorEndOfLine         =         "{CursorEndOfLine}";
public const string cmidCursorEndOfDocument     =     "{CursorEndOfDocument}";
public const string cmidEscapeKey               =               "{EscapeKey}";
public const string cmidAcceptInput             =             "{AcceptInput}";
public const string cmidDeleteKey               =               "{DeleteKey}";
public const string cmidBackspaceKey            =            "{BackspaceKey}";
public const string cmidCopyText                =                "{CopyText}";
public const string cmidCutText                 =                 "{CutText}";
public const string cmidPasteText               =               "{PasteText}";
public const string cmidPlayerOption_ViewCombatTarget="{PlayerOption_ViewCombatTarget}";
public const string cmidPlayerOption_AdvancedCombatUI="{PlayerOption_AdvancedCombatUI}";
public const string cmidPlayerOption_AcceptLootPermits="{PlayerOption_AcceptLootPermits}";
public const string cmidPlayerOption_DisplayAllegianceLogonNotifications="{PlayerOption_DisplayAllegianceLogonNotifications}";
public const string cmidPlayerOption_UseChargeAttack="{PlayerOption_UseChargeAttack}";
public const string cmidPlayerOption_UseCraftSuccessDialog="{PlayerOption_UseCraftSuccessDialog}";
public const string cmidPlayerOption_HearAllegianceChat="{PlayerOption_HearAllegianceChat}";
public const string cmidPlayerOption_DisplayDateOfBirth="{PlayerOption_DisplayDateOfBirth}";
public const string cmidPlayerOption_DisplayAge = "{PlayerOption_DisplayAge}";
public const string cmidPlayerOption_DisplayChessRank="{PlayerOption_DisplayChessRank}";
public const string cmidPlayerOption_DisplayFishingSkill="{PlayerOption_DisplayFishingSkill}";
public const string cmidPlayerOption_DisplayNumberDeaths="{PlayerOption_DisplayNumberDeaths}";
public const string cmidPlayerOption_DisplayTimeStamps="{PlayerOption_DisplayTimeStamps}";
public const string cmidPlayerOption_SalvageMultiple="{PlayerOption_SalvageMultiple}";
public const string cmidPlayerOption_AutoRepeatAttack="{PlayerOption_AutoRepeatAttack}";
public const string cmidPlayerOption_IgnoreAllegianceRequests="{PlayerOption_IgnoreAllegianceRequests}";
public const string cmidPlayerOption_IgnoreFellowshipRequests="{PlayerOption_IgnoreFellowshipRequests}";
public const string cmidPlayerOption_IgnoreTradeRequests="{PlayerOption_IgnoreTradeRequests}";
public const string cmidPlayerOption_PersistentAtDay="{PlayerOption_PersistentAtDay}";
public const string cmidPlayerOption_AllowGive  =  "{PlayerOption_AllowGive}";
public const string cmidPlayerOption_ShowTooltips="{PlayerOption_ShowTooltips}";
public const string cmidPlayerOption_UseDeception="{PlayerOption_UseDeception}";
public const string cmidPlayerOption_ToggleRun  =  "{PlayerOption_ToggleRun}";
public const string cmidPlayerOption_StayInChatMode="{PlayerOption_StayInChatMode}";
public const string cmidPlayerOption_AutoTarget = "{PlayerOption_AutoTarget}";
public const string cmidPlayerOption_VividTargetingIndicator="{PlayerOption_VividTargetingIndicator}";
public const string cmidPlayerOption_FellowshipShareXP="{PlayerOption_FellowshipShareXP}";
public const string cmidPlayerOption_FellowshipShareLoot="{PlayerOption_FellowshipShareLoot}";
public const string cmidPlayerOption_FellowshipAutoAcceptRequests="{PlayerOption_FellowshipAutoAcceptRequests}";
public const string cmidPlayerOption_StretchUI  =  "{PlayerOption_StretchUI}";
public const string cmidPlayerOption_CoordinatesOnRadar="{PlayerOption_CoordinatesOnRadar}";
public const string cmidPlayerOption_SpellDuration="{PlayerOption_SpellDuration}";
public const string cmidPlayerOption_DisableHouseRestrictionEffects="{PlayerOption_DisableHouseRestrictionEffects}";
public const string cmidPlayerOption_DragItemOnPlayerOpensSecureTrade="{PlayerOption_DragItemOnPlayerOpensSecureTrade}";
public const string cmidCameraCloser            =            "{CameraCloser}";
public const string cmidCameraFarther           =           "{CameraFarther}";
public const string cmidCameraLeftRotate        =        "{CameraLeftRotate}";
public const string cmidCameraLower             =             "{CameraLower}";
public const string cmidCameraRaise             =             "{CameraRaise}";
public const string cmidCameraRightRotate       =       "{CameraRightRotate}";
public const string cmidFirstPersonView         =         "{FirstPersonView}";
public const string cmidFloorView               =               "{FloorView}";
public const string cmidMapView                 =                 "{MapView}";
public const string cmidResetView               =               "{ResetView}";
public const string cmidShiftView               =               "{ShiftView}";
public const string cmidAutoRun                 =                 "{AutoRun}";
public const string cmidHoldRun                 =                 "{HoldRun}";
public const string cmidHoldSidestep            =            "{HoldSidestep}";
public const string cmidJump                    =                    "{Jump}";
public const string cmidSideStepLeft            =            "{SideStepLeft}";
public const string cmidSideStepRight           =           "{SideStepRight}";
public const string cmidTurnLeft                =                "{TurnLeft}";
public const string cmidTurnRight               =               "{TurnRight}";
public const string cmidWalkBackwards           =           "{WalkBackwards}";
public const string cmidWalkForward             =             "{WalkForward}";
public const string cmidHighAttack              =              "{HighAttack}";
public const string cmidLowAttack               =               "{LowAttack}";
public const string cmidMediumAttack            =            "{MediumAttack}";
public const string cmidToggleCombat            =            "{ToggleCombat}";
public const string cmidDecreasePowerSetting    =    "{DecreasePowerSetting}";
public const string cmidIncreasePowerSetting    =    "{IncreasePowerSetting}";
public const string cmidClosestCompassItem      =      "{ClosestCompassItem}";
public const string cmidClosestItem             =             "{ClosestItem}";
public const string cmidClosestMonster          =          "{ClosestMonster}";
public const string cmidClosestPlayer           =           "{ClosestPlayer}";
public const string cmidLastAttacker            =            "{LastAttacker}";
public const string cmidNextCompassItem         =         "{NextCompassItem}";
public const string cmidNextFellow              =              "{NextFellow}";
public const string cmidNextItem                =                "{NextItem}";
public const string cmidNextMonster             =             "{NextMonster}";
public const string cmidNextPlayer              =              "{NextPlayer}";
public const string cmidPreviousCompassItem     =     "{PreviousCompassItem}";
public const string cmidPreviousFellow          =          "{PreviousFellow}";
public const string cmidPreviousItem            =            "{PreviousItem}";
public const string cmidPreviousMonster         =         "{PreviousMonster}";
public const string cmidPreviousPlayer          =          "{PreviousPlayer}";
public const string cmidPreviousSelection       =       "{PreviousSelection}";
public const string cmidDropSelected            =            "{DropSelected}";
public const string cmidExamineSelected         =         "{ExamineSelected}";
public const string cmidGiveSelected            =            "{GiveSelected}";
public const string cmidAutosortSelected        =        "{AutosortSelected}";
public const string cmidSelectSelf              =              "{SelectSelf}";
public const string cmidSplitSelected           =           "{SplitSelected}";
public const string cmidUseSelected             =             "{UseSelected}";
public const string cmidAllegiancePanel         =         "{AllegiancePanel}";
public const string cmidAttributesPanel         =         "{AttributesPanel}";
public const string cmidCharacterInformationPanel="{CharacterInformationPanel}";
public const string cmidCharacterOptionsPanel   =   "{CharacterOptionsPanel}";
public const string cmidFellowshipPanel         =         "{FellowshipPanel}";
public const string cmidHarmfulSpellsPanel      =      "{HarmfulSpellsPanel}";
public const string cmidHelpfulSpellsPanel      =      "{HelpfulSpellsPanel}";
public const string cmidHousePanel              =              "{HousePanel}";
public const string cmidInventoryPanel          =          "{InventoryPanel}";
public const string cmidLinkStatusPanel         =         "{LinkStatusPanel}";
public const string cmidMapPanel                =                "{MapPanel}";
public const string cmidOptionsPanel            =            "{OptionsPanel}";
public const string cmidSkillsPanel             =             "{SkillsPanel}";
public const string cmidSoundAndGraphicsPanel   =   "{SoundAndGraphicsPanel}";
public const string cmidSpellComponentsPanel    =    "{SpellComponentsPanel}";
public const string cmidSpellbookPanel          =          "{SpellbookPanel}";
public const string cmidTradePanel              =              "{TradePanel}";
public const string cmidVitaePanel              =              "{VitaePanel}";
public const string cmidAcceptCorpseLooting     =     "{AcceptCorpseLooting}";
public const string cmidAdvancedCombatInterface = "{AdvancedCombatInterface}";
public const string cmidAttemptToDeceivePlayers = "{AttemptToDeceivePlayers}";
public const string cmidAutoCreateShortcuts     =     "{AutoCreateShortcuts}";
public const string cmidAutoRepeatAttacks       =       "{AutoRepeatAttacks}";
public const string cmidAutoTarget              =              "{AutoTarget}";
public const string cmidAutoTrackCombatTargets  =  "{AutoTrackCombatTargets}";
public const string cmidDisableHouseEffect      =      "{DisableHouseEffect}";
public const string cmidDisableWeather          =          "{DisableWeather}";
public const string cmidDisplayTooltips         =         "{DisplayTooltips}";
public const string cmidIgnoreAllegianceRequests="{IgnoreAllegianceRequests}";
public const string cmidIgnoreFellowshipRequests="{IgnoreFellowshipRequests}";
public const string cmidIgnoreTradeRequests     =     "{IgnoreTradeRequests}";
public const string cmidInvertMouseLook         =         "{InvertMouseLook}";
public const string cmidLetPlayersGiveYouItems  =  "{LetPlayersGiveYouItems}";
public const string cmidMuteOnLosingFocus       =       "{MuteOnLosingFocus}";
public const string cmidRightClickToMouseLook   =   "{RightClickToMouseLook}";
public const string cmidRunAsDefaultMovement    =    "{RunAsDefaultMovement}";
public const string cmidShareFellowshipLoot     =     "{ShareFellowshipLoot}";
public const string cmidShareFellowshipXP       =       "{ShareFellowshipXP}";
public const string cmidShowRadarCoordinates    =    "{ShowRadarCoordinates}";
public const string cmidShowSpellDurations      =      "{ShowSpellDurations}";
public const string cmidStayInChatModeAfterSend = "{StayInChatModeAfterSend}";
public const string cmidStretchUI               =               "{StretchUI}";
public const string cmidVividTargetIndicator    =    "{VividTargetIndicator}";
public const string cmidCaptureScreenshotToFile = "{CaptureScreenshotToFile}";
};

// dmty: Damage type masks
class dmty
{
// dmty values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to indicate more than one type of damage.
public const int dmtySlashing                   =                      0x0001;
public const int dmtyPiercing                   =                      0x0002;
public const int dmtyBludgeoning                =                      0x0004;
public const int dmtyCold                       =                      0x0008;
public const int dmtyFire                       =                      0x0010;
public const int dmtyAcid                       =                      0x0020;
public const int dmtyElectrical                 =                      0x0040;
};

// eqm: Equipment type masks
class eqm
{
// eqm values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to specify multiple equipment slots.
public const int eqmNil                         =                           0;	// No slot.
public const int eqmHead                        =                  0x00000001;
public const int eqmChestUnder                  =                  0x00000002;
public const int eqmAbdomenUnder                =                  0x00000004;
public const int eqmUpperArmsUnder              =                  0x00000008;
public const int eqmLowerArmsUnder              =                  0x00000010;
public const int eqmHands                       =                  0x00000020;
public const int eqmUpperLegsUnder              =                  0x00000040;
public const int eqmLowerLegsUnder              =                  0x00000080;
public const int eqmFeet                        =                  0x00000100;
public const int eqmChestOuter                  =                  0x00000200;
public const int eqmAbdomenOuter                =                  0x00000400;
public const int eqmUpperArmsOuter              =                  0x00000800;
public const int eqmLowerArmsOuter              =                  0x00001000;
public const int eqmUpperLegsOuter              =                  0x00002000;
public const int eqmLowerLegsOuter              =                  0x00004000;
public const int eqmNecklace                    =                  0x00008000;
public const int eqmRBracelet                   =                  0x00010000;
public const int eqmLBracelet                   =                  0x00020000;
public const int eqmBracelets                   =                  0x00030000;	// Both bracelet slots.
public const int eqmRRing                       =                  0x00040000;
public const int eqmLRing                       =                  0x00080000;
public const int eqmRings                       =                  0x000C0000;	// Both ring slots.
public const int eqmWeapon                      =                  0x00100000;	// Melee weapon slot.
public const int eqmShield                      =                  0x00200000;
public const int eqmRangedWeapon                =                  0x00400000;	// Bow or crossbow slot.
public const int eqmAmmo                        =                  0x00800000;
public const int eqmFocusWeapon                 =                  0x01000000;	// Wand, staff, or orb slot.
public const int eqmWeapons                     =                  0x01500000;	// All weapons of any kind.
public const int eqmAll                         =                  0x01FFFFFF;	// All equipment slots.
};

// evid: Event IDs
class evid
{
public const int evidNil                        =                           0;	// No event was received.
public const int evidOnVitals                   =                           1;
public const int evidOnLocChangeSelf            =                           2;
public const int evidOnLocChangeOther           =                           3;
public const int evidOnStatTotalBurden          =                           4;
public const int evidOnStatTotalExp             =                           5;
public const int evidOnStatUnspentExp           =                           6;
public const int evidOnObjectCreatePlayer       =                           7;
public const int evidOnStatSkillExp             =                           8;
public const int evidOnAdjustStack              =                           9;
public const int evidOnChatLocal                =                          10;
public const int evidOnChatSpell                =                          11;
public const int evidOnDeathSelf                =                          12;
public const int evidOnMyPlayerKill             =                          13;
public const int evidOnDeathOther               =                          14;
public const int evidOnEnd3D                    =                          15;
public const int evidOnStart3D                  =                          16;
public const int evidOnStartPortalSelf          =                          17;
public const int evidOnEndPortalSelf            =                          18;
public const int evidOnEndPortalOther           =                          19;
public const int evidOnCombatMode               =                          20;
public const int evidOnAnimationSelf            =                          21;
public const int evidOnTargetHealth             =                          22;
public const int evidOnAddToInventory           =                          23;
public const int evidOnRemoveFromInventory      =                          24;
public const int evidOnAssessCreature           =                          25;
public const int evidOnAssessItem               =                          26;
public const int evidOnSpellFailSelf            =                          27;
public const int evidOnMeleeEvadeSelf           =                          28;
public const int evidOnMeleeEvadeOther          =                          29;
public const int evidOnMeleeDamageSelf          =                          30;
public const int evidOnMeleeDamageOther         =                          31;
public const int evidOnLogon                    =                          32;
public const int evidOnTellServer               =                          33;
public const int evidOnTell                     =                          34;
public const int evidOnTellMelee                =                          35;
public const int evidOnTellMisc                 =                          36;
public const int evidOnTellFellowship           =                          37;
public const int evidOnTellPatron               =                          38;
public const int evidOnTellVassal               =                          39;
public const int evidOnTellFollower             =                          40;
public const int evidOnDeathMessage             =                          41;
public const int evidOnSpellCastSelf            =                          42;
public const int evidOnTradeStart               =                          43;
public const int evidOnTradeEnd                 =                          44;
public const int evidOnTradeAdd                 =                          45;
public const int evidOnTradeReset               =                          46;
public const int evidOnSetFlagSelf              =                          47;
public const int evidOnSetFlagOther             =                          48;
public const int evidOnApproachVendor           =                          49;
public const int evidOnChatBroadcast            =                          50;
public const int evidOnTradeAccept              =                          51;
public const int evidOnObjectCreate             =                          52;
public const int evidOnChatServer               =                          53;
public const int evidOnSpellExpire              =                          54;
public const int evidOnSpellExpireSilent        =                          55;
public const int evidOnStatTotalPyreals         =                          56;
public const int evidOnStatLevel                =                          57;
public const int evidOnChatEmoteStandard        =                          58;
public const int evidOnChatEmoteCustom          =                          59;
public const int evidOnOpenContainer            =                          60;
public const int evidOnPortalStormWarning       =                          61;
public const int evidOnPortalStormed            =                          62;
public const int evidOnCommand                  =                          63;
public const int evidOnControlEvent             =                          64;
public const int evidOnItemManaBar              =                          65;
public const int evidOnActionComplete           =                          66;
public const int evidOnObjectDestroy            =                          67;
public const int evidOnLocChangeCreature        =                          68;
public const int evidOnMoveItem                 =                          69;
public const int evidOnSpellAdd                 =                          70;
public const int evidOnFellowCreate             =                          71;
public const int evidOnFellowDisband            =                          72;
public const int evidOnFellowDismiss            =                          73;
public const int evidOnFellowQuit               =                          74;
public const int evidOnFellowRecruit            =                          75;
public const int evidOnMeleeLastAttacker        =                          76;
public const int evidOnLeaveVendor              =                          77;
public const int evidOnTimer                    =                          78;
public const int evidOnControlProperty          =                          79;
public const int evidOnPluginMsg                =                          80;
public const int evidOnTellAllegiance           =                          81;
public const int evidOnTellCovassal             =                          82;
public const int evidOnHotkey                   =                          83;
public const int evidOnFellowInvite             =                          84;
public const int evidOnNavStop                  =                          85;
public const int evidOnFellowUpdate             =                          86;
public const int evidOnDisconnect               =                          87;
public const int evidOnCraftConfirm             =                          88;
public const int evidOnChatBoxMessage           =                          89;
public const int evidOnTipMessage               =                          90;
public const int evidOnOpenContainerPanel       =                          91;
public const int evidOnCloseContainer           =                          92;
public const int evidOnChatBoxClick             =                          93;
public const int evidMax                        =                          94;
};

// ioc: Icon outline color masks
class ioc
{
// ioc values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript).
public const int iocNil                         =                           0;
public const int iocEnchanted                   =                  0x00000001;	// Has spells.
public const int iocUnknown2                    =                  0x00000002;	// (No known meaning.)
public const int iocHealing                     =                  0x00000004;	// Restores health.
public const int iocMana                        =                  0x00000008;	// Restores mana.
public const int iocHearty                      =                  0x00000010;	// Restores stamina.
public const int iocFire                        =                  0x00000020;
public const int iocLightning                   =                  0x00000040;
public const int iocFrost                       =                  0x00000080;
public const int iocAcid                        =                  0x00000100;
};

// kmo: Key motions
class kmo
{
public const int kmoDown                        =                        0x01;	// Press the key.
public const int kmoUp                          =                        0x02;	// Release the key.
public const int kmoClick                       =                        0x03;	// Press and release.
};

// material: Material codes
class material
{
public const int materialCeramic                =                           1;
public const int materialPorcelain              =                           2;
public const int materialCloth                  =                           3;
public const int materialLinen                  =                           4;
public const int materialSatin                  =                           5;
public const int materialSilk                   =                           6;
public const int materialVelvet                 =                           7;
public const int materialWool                   =                           8;
public const int materialGem                    =                           9;
public const int materialAgate                  =                          10;
public const int materialAmber                  =                          11;
public const int materialAmethyst               =                          12;
public const int materialAquamarine             =                          13;
public const int materialAzurite                =                          14;
public const int materialBlackGarnet            =                          15;
public const int materialBlackOpal              =                          16;
public const int materialBloodstone             =                          17;
public const int materialCarnelian              =                          18;
public const int materialCitrine                =                          19;
public const int materialDiamond                =                          20;
public const int materialEmerald                =                          21;
public const int materialFireOpal               =                          22;
public const int materialGreenGarnet            =                          23;
public const int materialGreenJade              =                          24;
public const int materialHematite               =                          25;
public const int materialImperialTopaz          =                          26;
public const int materialJet                    =                          27;
public const int materialLapisLazuli            =                          28;
public const int materialLavenderJade           =                          29;
public const int materialMalachite              =                          30;
public const int materialMoonstone              =                          31;
public const int materialOnyx                   =                          32;
public const int materialOpal                   =                          33;
public const int materialPeridot                =                          34;
public const int materialRedGarnet              =                          35;
public const int materialRedJade                =                          36;
public const int materialRoseQuartz             =                          37;
public const int materialRuby                   =                          38;
public const int materialSapphire               =                          39;
public const int materialSmokeyQuartz           =                          40;
public const int materialSunstone               =                          41;
public const int materialTigerEye               =                          42;
public const int materialTourmaline             =                          43;
public const int materialTurquoise              =                          44;
public const int materialWhiteJade              =                          45;
public const int materialWhiteQuartz            =                          46;
public const int materialWhiteSapphire          =                          47;
public const int materialYellowGarnet           =                          48;
public const int materialYellowTopaz            =                          49;
public const int materialZircon                 =                          50;
public const int materialIvory                  =                          51;
public const int materialLeather                =                          52;
public const int materialDilloHide              =                          53;
public const int materialGromnieHide            =                          54;
public const int materialReedsharkHide          =                          55;
public const int materialMetal                  =                          56;
public const int materialBrass                  =                          57;
public const int materialBronze                 =                          58;
public const int materialCopper                 =                          59;
public const int materialGold                   =                          60;
public const int materialIron                   =                          61;
public const int materialPyreal                 =                          62;
public const int materialSilver                 =                          63;
public const int materialSteel                  =                          64;
public const int materialStone                  =                          65;
public const int materialAlabaster              =                          66;
public const int materialGranite                =                          67;
public const int materialMarble                 =                          68;
public const int materialObsidian               =                          69;
public const int materialSandstone              =                          70;
public const int materialSerpentine             =                          71;
public const int materialWood                   =                          72;
public const int materialEbony                  =                          73;
public const int materialMahogany               =                          74;
public const int materialOak                    =                          75;
public const int materialPine                   =                          76;
public const int materialTeak                   =                          77;
};

// mcm: Merchant category masks
class mcm
{
public const int mcmNil                         =                  0x00000000;	// No category; not saleable.
public const int mcmWeaponsMelee                =                  0x00000001;
public const int mcmArmor                       =                  0x00000002;
public const int mcmClothing                    =                  0x00000004;
public const int mcmJewelry                     =                  0x00000008;
public const int mcmCreature                    =                  0x00000010;
public const int mcmFood                        =                  0x00000020;
public const int mcmPyreal                      =                  0x00000040;
public const int mcmMisc                        =                  0x00000080;
public const int mcmWeaponsMissile              =                  0x00000100;
public const int mcmContainers                  =                  0x00000200;
public const int mcmMiscFletching               =                  0x00000400;
public const int mcmGems                        =                  0x00000800;
public const int mcmSpellComponents             =                  0x00001000;
public const int mcmBooksPaper                  =                  0x00002000;
public const int mcmKeysTools                   =                  0x00004000;
public const int mcmMagicItems                  =                  0x00008000;
public const int mcmPortal                      =                  0x00010000;
public const int mcmLockable                    =                  0x00020000;	// Chests have this is addition to mcmContainers.
public const int mcmTradeNotes                  =                  0x00040000;
public const int mcmManaStones                  =                  0x00080000;
public const int mcmServices                    =                  0x00100000;
public const int mcmPlants                      =                  0x00200000;
public const int mcmCookingItems1               =                  0x00400000;
public const int mcmAlchemicalItems1            =                  0x00800000;
public const int mcmFletchingItems1             =                  0x01000000;
public const int mcmCookingItems2               =                  0x02000000;
public const int mcmAlchemicalItems2            =                  0x04000000;
public const int mcmFletchingItems2             =                  0x08000000;
public const int mcmLifestone                   =                  0x10000000;
public const int mcmTinkeringTools              =                  0x20000000;	// The Ust is in this category.
public const int mcmSalvageBag                  =                  0x40000000;
public const int mcmChessboard                  =                  0x80000000;	// There may be other items in this category as well.
};

// Miscellaneous constants
class misc
{
public const int oidNil                         =                           0;	// Signifies no object.
public const int ipackMain                      =                           0;	// Index of the main pack.
public const int iitemNil                       =                          -1;	// Signifies the pack itself.
public const int clrNil                         =                  0x7FFFFFFF;	// VBScript has a problem with FFFFFFFF (turns it into 0000FFFF).
public const int spellidNil                     =                           0;	// Signifies no spell.
};

// ibutton: Mouse button IDs
class ibutton
{
public const int ibuttonLeft                    =                           0;	// Left button.
public const int ibuttonRight                   =                           1;	// Right button.
public const int ibuttonMiddle                  =                           2;	// Middle button.
};

// nopt: Navigation option masks
class nopt
{
public const int noptWalk                       =                      0x0001;
public const int noptStopOnArrival              =                      0x0002;
public const int noptZigzag                     =                      0x0004;
};

// nsc: Navigation stop codes for OnNavStop
class nsc
{
public const int nscArrived                     =                           0;	// You arrived at the target location.
public const int nscTimeout                     =                           1;	// You did not arrive before the timeout expired.
public const int nscCanceled                    =                           2;	// skapi.CancelNav was called.
};

// ocm: Object category masks
class ocm
{
// ocm values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to specify multiple categories.
public const int ocmNil                         =                      0x0000;	// Include no objects.
public const int ocmPlayer                      =                      0x0001;
public const int ocmMonster                     =                      0x0002;
public const int ocmPlayerCorpse                =                      0x0004;
public const int ocmMonsterCorpse               =                      0x0008;
public const int ocmLifestone                   =                      0x0010;
public const int ocmPortal                      =                      0x0020;
public const int ocmMerchant                    =                      0x0040;
public const int ocmEquipment                   =                      0x0080;
public const int ocmPK                          =                      0x0100;
public const int ocmNonPK                       =                      0x0200;
public const int ocmNPC                         =                      0x0400;
public const int ocmHook                        =                      0x0800;
public const int ocmAll                         =                          -1;	// Include all nearby objects of any kind.
};

// olc: Object location categories
class olc
{
// olc values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to specify multiple categories.
public const int olcNil                         =                      0x0000;	// Include no objects.
public const int olcInventory                   =                      0x0001;	// Include objects in your inventory.
public const int olcContained                   =                      0x0002;	// Include objects contained in another object (e.g. a chest or corpse).
public const int olcEquipped                    =                      0x0004;	// Include objects being worn or wielded (by you or someone else).
public const int olcOnGround                    =                      0x0008;	// Include objects out on their own in the game world.
public const int olcAll                         =                          -1;	// Include all object regardless of location.
};

// oty: Object type masks
class oty
{
public const int otyContainer                   =                  0x00000001;
public const int otyInscribable                 =                  0x00000002;
public const int otyNoPickup                    =                  0x00000004;
public const int otyPlayer                      =                  0x00000008;
public const int otySelectable                  =                  0x00000010;
public const int otyPK                          =                  0x00000020;
public const int otyReadable                    =                  0x00000100;
public const int otyMerchant                    =                  0x00000200;
public const int otyDoor                        =                  0x00001000;
public const int otyCorpse                      =                  0x00002000;
public const int otyLifestone                   =                  0x00004000;
public const int otyFood                        =                  0x00008000;
public const int otyHealingKit                  =                  0x00010000;
public const int otyLockpick                    =                  0x00020000;
public const int otyPortal                      =                  0x00040000;
public const int otyFoci                        =                  0x00800000;	// The new magic foci, I think.
public const int otyPKL                         =                  0x02000000;
};

// opm: Output modes
class opm
{
// opm values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to send output to multiple destinations.
public const int opmDebugLog                    =                      0x0001;	// Sends the output to the debug terminal (using the Win32 OutputDebugString API).
public const int opmConsole                     =                      0x0002;	// Sends the output to the SkunkWorks console window.
public const int opmChatWnd                     =                      0x0010;	// Sends the output to the primary in-game chat window.
public const int opmChatWnd1                    =                      0x0020;	// Sends the output to floating chat window #1.
public const int opmChatWnd2                    =                      0x0040;	// Sends the output to floating chat window #2.
public const int opmChatWnd3                    =                      0x0080;	// Sends the output to floating chat window #3.
public const int opmChatWnd4                    =                      0x0100;	// Sends the output to floating chat window #4.
public const int opmChatWndAll                  =                      0x01F0;	// Sends the output to all in-game chat windows.
};

// plig: Player in game codes
class plig
{
public const int pligNotRunning                 =                           0;	// Game is not running.
public const int pligAtLogin                    =                           1;	// Game is at login screen.
public const int pligInPortal                   =                           2;	// Player is in portal space.
public const int pligInWorld                    =                           3;	// Player is in the game world.
};

// psw: Portal storm warning codes
class psw
{
public const int pswStormEnded                  =                           0;	// The portal storm is over.
public const int pswMildStorm                   =                           1;	// Mild portal storm.
public const int pswHeavyStorm                  =                           2;	// Heavy portal storm.
};

// skid: Skill IDs
class skid
{
public const int skidNil                        =                           0;	// No skill.
public const int skidAxe                        =                           1;
public const int skidBow                        =                           2;
public const int skidCrossbow                   =                           3;
public const int skidDagger                     =                           4;
public const int skidMace                       =                           5;
public const int skidMeleeDefense               =                           6;
public const int skidMissileDefense             =                           7;
public const int skidSpear                      =                           9;
public const int skidStaff                      =                          10;
public const int skidSword                      =                          11;
public const int skidThrownWeapons              =                          12;
public const int skidUnarmedCombat              =                          13;
public const int skidArcaneLore                 =                          14;
public const int skidMagicDefense               =                          15;
public const int skidManaConversion             =                          16;
public const int skidItemTinkering              =                          18;
public const int skidAssessPerson               =                          19;
public const int skidDeception                  =                          20;
public const int skidHealing                    =                          21;
public const int skidJump                       =                          22;
public const int skidLockpick                   =                          23;
public const int skidRun                        =                          24;
public const int skidAssessCreature             =                          27;
public const int skidWeaponTinkering            =                          28;
public const int skidArmorTinkering             =                          29;
public const int skidMagicItemTinkering         =                          30;
public const int skidCreatureEnchantment        =                          31;
public const int skidItemEnchantment            =                          32;
public const int skidLifeMagic                  =                          33;
public const int skidWarMagic                   =                          34;
public const int skidLeadership                 =                          35;
public const int skidLoyalty                    =                          36;
public const int skidFletching                  =                          37;
public const int skidAlchemy                    =                          38;
public const int skidCooking                    =                          39;
public const int skidSalvaging                  =                          40;
public const int skidMax                        =                          41;
};

// skts: Skill training status codes
class skts
{
public const int sktsUnusable                   =                           0;
public const int sktsUntrained                  =                           1;
public const int sktsTrained                    =                           2;
public const int sktsSpecialized                =                           3;
};

// Special key names
class specialkeys
{
// {alt}
// {back}                                                                      // Backspace
// {break}                                                                     // Ctrl+Break
// {capslock}
// {control}
// {del}
// {down}                                                                      // Down arrow
// {end}
// {esc}
// {f1}
// {f2}
// {f3}
// {f4}
// {f5}
// {f6}
// {f7}
// {f8}
// {f9}
// {f10}
// {f11}
// {f12}
// {home}
// {ins}
// {keypad 0}
// {keypad 1}
// {keypad 2}
// {keypad 3}
// {keypad 4}
// {keypad 5}
// {keypad 6}
// {keypad 7}
// {keypad 8}
// {keypad 9}
// {keypad *}
// {keypad +}
// {keypad -}
// {keypad /}
// {keypad .}
// {keypad enter}                                                              // Keypad Enter
// {left}                                                                      // Left arrow
// {numlock}
// {pause}
// {pgdn}
// {pgup}
// {prtsc}                                                                     // Print Screen
// {return}                                                                    // Enter (the regular one, above RShift)
// {right}                                                                     // Right arrow
// {scroll}                                                                    // Scroll Lock
// {shift}
// {space}                                                                     // Spacebar
// {tab}
// {up}                                                                        // Up arrow
};

// species: Species codes
class species
{
public const int speciesInvalid                 =                           0;
public const int speciesOlthoi                  =                           1;
public const int speciesBanderling              =                           2;
public const int speciesDrudge                  =                           3;
public const int speciesMosswart                =                           4;
public const int speciesLugian                  =                           5;
public const int speciesTumerok                 =                           6;
public const int speciesMite                    =                           7;
public const int speciesTusker                  =                           8;
public const int speciesPhyntosWasp             =                           9;
public const int speciesRat                     =                          10;
public const int speciesAuroch                  =                          11;
public const int speciesCow                     =                          12;
public const int speciesGolem                   =                          13;
public const int speciesUndead                  =                          14;
public const int speciesGromnie                 =                          15;
public const int speciesReedshark               =                          16;
public const int speciesArmoredillo             =                          17;
public const int speciesFae                     =                          18;
public const int speciesVirindi                 =                          19;
public const int speciesWisp                    =                          20;
public const int speciesKnathtead               =                          21;
public const int speciesShadow                  =                          22;
public const int speciesMattekar                =                          23;
public const int speciesMumiyah                 =                          24;
public const int speciesRabbit                  =                          25;
public const int speciesSclavus                 =                          26;
public const int speciesShallowsShark           =                          27;
public const int speciesMonouga                 =                          28;
public const int speciesZefir                   =                          29;
public const int speciesSkeleton                =                          30;
public const int speciesHuman                   =                          31;
public const int speciesShreth                  =                          32;
public const int speciesChittick                =                          33;
public const int speciesMoarsman                =                          34;
public const int speciesOlthoiLarvae            =                          35;
public const int speciesSlithis                 =                          36;
public const int speciesDeru                    =                          37;
public const int speciesFireElemental           =                          38;
public const int speciesSnowman                 =                          39;
public const int speciesUnknown                 =                          40;
public const int speciesBunny                   =                          41;
public const int speciesLightningElemental      =                          42;
public const int speciesRockslide               =                          43;
public const int speciesGrievver                =                          44;
public const int speciesNiffis                  =                          45;
public const int speciesUrsuin                  =                          46;
public const int speciesCrystal                 =                          47;
public const int speciesHollowMinion            =                          48;
public const int speciesScarecrow               =                          49;
public const int speciesIdol                    =                          50;
public const int speciesEmpyrean                =                          51;
public const int speciesHopeslayer              =                          52;
public const int speciesDoll                    =                          53;
public const int speciesMarionette              =                          54;
public const int speciesCarenzi                 =                          55;
public const int speciesSiraluun                =                          56;
public const int speciesAunTumerok              =                          57;
public const int speciesHeaTumerok              =                          58;
public const int speciesSimulacrum              =                          59;
public const int speciesAcidElemental           =                          60;
public const int speciesFrostElemental          =                          61;
public const int speciesElemental               =                          62;
public const int speciesStatue                  =                          63;
public const int speciesWall                    =                          64;
public const int speciesAlteredHuman            =                          65;
public const int speciesDevice                  =                          66;
public const int speciesHarbinger               =                          67;
public const int speciesDarkSarcophagus         =                          68;
public const int speciesChicken                 =                          69;
public const int speciesGotrokLugian            =                          70;
public const int speciesMargul                  =                          71;
public const int speciesBleachedRabbit          =                          72;
public const int speciesNastyRabbit             =                          73;
public const int speciesGrimacingRabbit         =                          74;
public const int speciesBurun                   =                          75;
public const int speciesTarget                  =                          76;
public const int speciesGhost                   =                          77;
public const int speciesFiun                    =                          78;
public const int speciesEater                   =                          79;
public const int speciesPenguin                 =                          80;
public const int speciesRuschk                  =                          81;
public const int speciesThrungus                =                          82;
public const int speciesViamontianKnight        =                          83;
public const int speciesRemoran                 =                          84;
public const int speciesSwarm                   =                          85;
public const int speciesMoar                    =                          86;
public const int speciesEnchantedArms           =                          87;
public const int speciesSleech                  =                          88;
public const int speciesMukkir                  =                          89;
};

// vital: Vital stat codes
class vital
{
public const int vitalNil                       =                           0;
public const int vitalHealthMax                 =                           1;
public const int vitalHealthCur                 =                           2;	// Cannot be used with skapi.SkinfoFromVital.
public const int vitalStaminaMax                =                           3;
public const int vitalStaminaCur                =                           4;	// Cannot be used with skapi.SkinfoFromVital.
public const int vitalManaMax                   =                           5;
public const int vitalManaCur                   =                           6;	// Cannot be used with skapi.SkinfoFromVital.
public const int vitalMax                       =                           7;
};

// wem: WaitEvent modes
class wem
{
public const int wemNormal                      =                           0;	// Returns when timeout elapsed or the queue is empty.
public const int wemSingle                      =                           1;	// Waits for a single event of any type.
public const int wemSpecific                    =                           2;	// Waits for a specific event type.
public const int wemFullTimeout                 =                           3;	// Always waits the full timeout interval.
};

}


