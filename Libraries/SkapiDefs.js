
// -------------------------------------------------------------------
// !!!!!!!!!!!!!!!!!!!!!!   R E A D   T H I S   !!!!!!!!!!!!!!!!!!!!!!
// -------------------------------------------------------------------
// !!!  This file is generated automatically by SkapiDefsJs.xsl.   !!!
// !!!    Any edits you make to it by hand WILL be overwritten.    !!!
// !!!         Make your changes to SkapiDefs.xml instead.         !!!
// -------------------------------------------------------------------

// arc: Action result codes
var arcSuccess                  =                           0;	// Your spellcasting attempt (or other action) succeeded.
var arcCombatMode               =                           2;	// One of the parties entered combat mode.
var arcChargedTooFar            =                          28;	// You charged too far!
var arcTooBusy                  =                          29;	// You're too busy!
var arcCantMoveToObject         =                          57;	// Unable to move to object!
var arcCanceled                 =                          81;	// One of the parties canceled or moved out of range.
var arcFatigued                 =                        1015;	// You are too fatigued to attack!
var arcOutOfAmmo                =                        1016;	// You are out of ammunition!
var arcMissleMisfire            =                        1017;	// Your missile attack misfired!
var arcImpossibleSpell          =                        1018;	// You've attempted an impossible spell path!
var arcDontKnowSpell            =                        1022;	// You don't know that spell!
var arcIncorrectTarget          =                        1023;	// Incorrect target type.
var arcOutOfComps               =                        1024;	// You don't have all the components for this spell.
var arcNotEnoughMana            =                        1025;	// You don't have enough Mana to cast this spell.
var arcFizzle                   =                        1026;	// Your spell fizzled.
var arcNoTarget                 =                        1027;	// Your spell's target is missing!
var arcSpellMisfire             =                        1028;	// Your projectile spell mislaunched!
var arcSummonFailure            =                        1188;	// You fail to summon the portal!
var arcNotAvailable             =                        1323;	// That person is not available.

// attr: Attribute IDs
var attrNil                     =                           0;	// No attribute.
var attrStrength                =                           1;
var attrEndurance               =                           2;
var attrQuickness               =                           3;
var attrCoordination            =                           4;
var attrFocus                   =                           5;
var attrSelf                    =                           6;
var attrMax                     =                           7;

// bpart: Body part IDs
var bpartHead                   =                           0;
var bpartChest                  =                           1;
var bpartAbdomen                =                           2;
var bpartUpperArm               =                           3;
var bpartLowerArm               =                           4;
var bpartHand                   =                           5;
var bpartUpperLeg               =                           6;
var bpartLowerLeg               =                           7;
var bpartFoot                   =                           8;

// chop: Character option masks
var chopRepeatAttacks           =                  0x00000002;	// Automatically Repeat Attacks
var chopIgnoreAllegiance        =                  0x00000004;	// Ignore Allegiance Requests
var chopIgnoreFellowship        =                  0x00000008;	// Ignore Fellowship Requests
var chopAcceptGifts             =                  0x00000040;	// Let Other Players Give You Items
var chopKeepTargetInView        =                  0x00000080;	// Keep Combat Targets in View
var chopTooltips                =                  0x00000100;	// Display 3D Tooltips
var chopDeceive                 =                  0x00000200;	// Attempt to Deceive Other Players
var chopRunAsDefault            =                  0x00000400;	// Run as Default Movement
var chopStayInChat              =                  0x00000800;	// Stay in Chat Mode After Sending a Message
var chopAdvancedCombat          =                  0x00001000;	// Advanced Combat Interface
var chopAutoTarget              =                  0x00002000;	// Auto Target
var chopVividTargeting          =                  0x00008000;	// Vivid Targeting Indicator
var chopIgnoreTrade             =                  0x00020000;	// Ignore All Trade Requests
var chopShareXP                 =                  0x00040000;	// Share Fellowship Experience
var chopAcceptCorpsePerm        =                  0x00080000;	// Accept Corpse-Looting Permissions
var chopShareLoot               =                  0x00100000;	// Share Fellowship Loot
var chopStretchUI               =                  0x00200000;	// Stretch UI
var chopShowCoords              =                  0x00400000;	// Show Coordinates By the Radar
var chopSpellDurations          =                  0x00800000;	// Display Spell Durations
var chopDisableHouseFX          =                  0x02000000;	// Disable House Restriction Effects
var chopDragStartsTrade         =                  0x04000000;	// Drag Item to Player Opens Trade
var chopShowAllegianceLogons    =                  0x08000000;	// Show Allegiance Logons
var chopUseChargeAttack         =                  0x10000000;	// Use Charge Attack
var chopAutoFellow              =                  0x20000000;	// Automatically Accept Fellowship Requests
var chopAllegianceChat          =                  0x40000000;	// Listen to Allegiance Chat
var chopCraftingDialog          =                  0x80000000;	// Use Crafting Chance of Success Dialog
var chopAcceptAllegiance        =                  0x00000004;	// (Old name for compatibility)
var chopAcceptFellowship        =                  0x00000008;	// (Old name for compatibility)

// chop2: Character option masks (extended)
var chop2AlwaysDaylight         =                  0x00000001;	// Always Daylight Outdoors
var chop2ShowDateOfBirth        =                  0x00000002;	// Allow Others to See Your Date of Birth
var chop2ShowChessRank          =                  0x00000004;	// Allow Others to See Your Chess Rank
var chop2ShowFishingSkill       =                  0x00000008;	// Allow Others to See Your Fishing Skill
var chop2ShowDeaths             =                  0x00000010;	// Allow Others to See Your Number of Deaths
var chop2ShowAge                =                  0x00000020;	// Allow Others to See Your Age
var chop2Timestamps             =                  0x00000040;	// Display Timestamps
var chop2SalvageMultiple        =                  0x00000080;	// Salvage Multiple Materials at Once

// cmc: Chat message colors
var cmcGreen                    =                           0;
var cmcWhite                    =                           2;	// Local broadcast chat.
var cmcYellow                   =                           3;
var cmcDarkYellow               =                           4;
var cmcMagenta                  =                           5;
var cmcRed                      =                           6;
var cmcLightBlue                =                           7;
var cmcRose                     =                           8;
var cmcPaleRose                 =                           9;
var cmcDimWhite                 =                          12;
var cmcCyan                     =                          13;
var cmcPaleBlue                 =                          14;	// SkunkWorks default color.
var cmcBlue                     =                          17;	// Spellcasting.
var cmcCoral                    =                          22;

// chrm: Chat recipient masks
var chrmLocal                   =                  0x00000001;	// Local chat broadcast
var chrmEmote                   =                  0x00000002;	// Local emote broadcast (@e)
var chrmFellowship              =                  0x00000800;	// Fellowship broadcast (@f)
var chrmVassals                 =                  0x00001000;	// Patron to vassal (@v)
var chrmPatron                  =                  0x00002000;	// Vassal to patron (@p)
var chrmMonarch                 =                  0x00004000;	// Follower to monarch (@m)
var chrmCovassals               =                  0x01000000;	// Covassal broadcast (@c)
var chrmAllegiance              =                  0x02000000;	// Allegiance broadcast by monarch or speaker (@a)

// cmid: Command IDs
// cmid definitions appear here in the same order they appear on AC's Keyboard Configuration screen.  Where the name of a cmid differs from the descriptive text on that screen, the text appears in the Description column below.
// cmid values may be concatenated using the + operator to specify a sequence of commands.
var cmidMovementJump            =            "{MovementJump}";
var cmidMovementForward         =         "{MovementForward}";
var cmidMovementBackup          =          "{MovementBackup}";
var cmidMovementTurnLeft        =        "{MovementTurnLeft}";
var cmidMovementTurnRight       =       "{MovementTurnRight}";
var cmidMovementWalkMode        =        "{MovementWalkMode}";
var cmidMovementStop            =            "{MovementStop}";
var cmidMovementStrafeRight     =     "{MovementStrafeRight}";
var cmidMovementStrafeLeft      =      "{MovementStrafeLeft}";
var cmidMovementRunLock         =         "{MovementRunLock}";
var cmidReady                   =                   "{Ready}";
var cmidCrouch                  =                  "{Crouch}";
var cmidSitting                 =                 "{Sitting}";
var cmidSleeping                =                "{Sleeping}";
var cmidSelectionSelf           =           "{SelectionSelf}";
var cmidSelectionPreviousCompassItem="{SelectionPreviousCompassItem}";
var cmidSelectionNextCompassItem="{SelectionNextCompassItem}";
var cmidSelectionUseNextUnopenedCorpse="{SelectionUseNextUnopenedCorpse}";
var cmidSelectionGive           =           "{SelectionGive}";
var cmidSelectionDrop           =           "{SelectionDrop}";
var cmidSelectionPickUp         =         "{SelectionPickUp}";
var cmidSelectionSplitStack     =     "{SelectionSplitStack}";
var cmidSelectionUseClosestUnopenedCorpse="{SelectionUseClosestUnopenedCorpse}";
var cmidSelectionClosestCompassItem="{SelectionClosestCompassItem}";
var cmidSelectionLastAttacker   =   "{SelectionLastAttacker}";
var cmidSelectionClosestMonster = "{SelectionClosestMonster}";
var cmidSelectionPreviousItem   =   "{SelectionPreviousItem}";
var cmidSelectionNextItem       =       "{SelectionNextItem}";
var cmidSelectionPreviousSelection="{SelectionPreviousSelection}";
var cmidSelectionClosestItem    =    "{SelectionClosestItem}";
var cmidSelectionPreviousMonster="{SelectionPreviousMonster}";
var cmidSelectionNextMonster    =    "{SelectionNextMonster}";
var cmidSelectionClosestPlayer  =  "{SelectionClosestPlayer}";
var cmidSelectionPreviousPlayer = "{SelectionPreviousPlayer}";
var cmidSelectionNextPlayer     =     "{SelectionNextPlayer}";
var cmidSelectionPreviousFellow = "{SelectionPreviousFellow}";
var cmidSelectionNextFellow     =     "{SelectionNextFellow}";
var cmidSelectionExamine        =        "{SelectionExamine}";
var cmidUSE                     =                     "{USE}";
var cmidToggleAbusePanel        =        "{ToggleAbusePanel}";
var cmidToggleCharacterInfoPanel="{ToggleCharacterInfoPanel}";
var cmidTogglePositiveEffectsPanel="{TogglePositiveEffectsPanel}";
var cmidToggleNegativeEffectsPanel="{ToggleNegativeEffectsPanel}";
var cmidToggleLinkStatusPanel   =   "{ToggleLinkStatusPanel}";
var cmidToggleUrgentAssistancePanel="{ToggleUrgentAssistancePanel}";
var cmidToggleVitaePanel        =        "{ToggleVitaePanel}";
var cmidToggleSocialPanel       =       "{ToggleSocialPanel}";
var cmidToggleSpellManagementPanel="{ToggleSpellManagementPanel}";
var cmidToggleSkillManagementPanel="{ToggleSkillManagementPanel}";
var cmidToggleMapPanel          =          "{ToggleMapPanel}";
var cmidToggleHousePanel        =        "{ToggleHousePanel}";
var cmidToggleGameplayOptionsPanel="{ToggleGameplayOptionsPanel}";
var cmidToggleCharacterOptionsPanel="{ToggleCharacterOptionsPanel}";
var cmidToggleConfigOptionsPanel="{ToggleConfigOptionsPanel}";
var cmidToggleRadarPanel        =        "{ToggleRadarPanel}";
var cmidToggleKeyboardPanel     =     "{ToggleKeyboardPanel}";
var cmidCaptureScreenshot       =       "{CaptureScreenshot}";
var cmidToggleHelp              =              "{ToggleHelp}";
var cmidTogglePluginManager     =     "{TogglePluginManager}";
var cmidToggleAllegiancePanel   =   "{ToggleAllegiancePanel}";
var cmidToggleFellowshipPanel   =   "{ToggleFellowshipPanel}";
var cmidToggleSpellbookPanel    =    "{ToggleSpellbookPanel}";
var cmidToggleSpellComponentsPanel="{ToggleSpellComponentsPanel}";
var cmidToggleAttributesPanel   =   "{ToggleAttributesPanel}";
var cmidToggleSkillsPanel       =       "{ToggleSkillsPanel}";
var cmidToggleWorldPanel        =        "{ToggleWorldPanel}";
var cmidToggleOptionsPanel      =      "{ToggleOptionsPanel}";
var cmidToggleInventoryPanel    =    "{ToggleInventoryPanel}";
var cmidLOGOUT                  =                  "{LOGOUT}";
var cmidSelectQuickSlot_6       =       "{SelectQuickSlot_6}";
var cmidSelectQuickSlot_7       =       "{SelectQuickSlot_7}";
var cmidSelectQuickSlot_8       =       "{SelectQuickSlot_8}";
var cmidSelectQuickSlot_9       =       "{SelectQuickSlot_9}";
var cmidSelectQuickSlot_1       =       "{SelectQuickSlot_1}";
var cmidSelectQuickSlot_2       =       "{SelectQuickSlot_2}";
var cmidSelectQuickSlot_3       =       "{SelectQuickSlot_3}";
var cmidSelectQuickSlot_4       =       "{SelectQuickSlot_4}";
var cmidSelectQuickSlot_5       =       "{SelectQuickSlot_5}";
var cmidCreateShortcut          =          "{CreateShortcut}";
var cmidUseQuickSlot_1          =          "{UseQuickSlot_1}";
var cmidUseQuickSlot_2          =          "{UseQuickSlot_2}";
var cmidUseQuickSlot_3          =          "{UseQuickSlot_3}";
var cmidUseQuickSlot_4          =          "{UseQuickSlot_4}";
var cmidUseQuickSlot_5          =          "{UseQuickSlot_5}";
var cmidUseQuickSlot_6          =          "{UseQuickSlot_6}";
var cmidUseQuickSlot_7          =          "{UseQuickSlot_7}";
var cmidUseQuickSlot_8          =          "{UseQuickSlot_8}";
var cmidUseQuickSlot_9          =          "{UseQuickSlot_9}";
var cmidToggleChatEntry         =         "{ToggleChatEntry}";
var cmidSTART_COMMAND           =           "{START_COMMAND}";
var cmidMonarchReply            =            "{MonarchReply}";
var cmidPatronReply             =             "{PatronReply}";
var cmidReply                   =                   "{Reply}";
var cmidEnterChatMode           =           "{EnterChatMode}";
var cmidCombatToggleCombat      =      "{CombatToggleCombat}";
var cmidCombatDecreaseAttackPower="{CombatDecreaseAttackPower}";
var cmidCombatIncreaseAttackPower="{CombatIncreaseAttackPower}";
var cmidCombatLowAttack         =         "{CombatLowAttack}";
var cmidCombatMediumAttack      =      "{CombatMediumAttack}";
var cmidCombatHighAttack        =        "{CombatHighAttack}";
var cmidCombatDecreaseMissileAccuracy="{CombatDecreaseMissileAccuracy}";
var cmidCombatIncreaseMissileAccuracy="{CombatIncreaseMissileAccuracy}";
var cmidCombatAimLow            =            "{CombatAimLow}";
var cmidCombatAimMedium         =         "{CombatAimMedium}";
var cmidCombatAimHigh           =           "{CombatAimHigh}";
var cmidUseSpellSlot_10         =         "{UseSpellSlot_10}";
var cmidUseSpellSlot_1          =          "{UseSpellSlot_1}";
var cmidUseSpellSlot_2          =          "{UseSpellSlot_2}";
var cmidUseSpellSlot_3          =          "{UseSpellSlot_3}";
var cmidUseSpellSlot_4          =          "{UseSpellSlot_4}";
var cmidUseSpellSlot_11         =         "{UseSpellSlot_11}";
var cmidUseSpellSlot_12         =         "{UseSpellSlot_12}";
var cmidCombatPrevSpellTab      =      "{CombatPrevSpellTab}";
var cmidCombatNextSpellTab      =      "{CombatNextSpellTab}";
var cmidCombatPrevSpell         =         "{CombatPrevSpell}";
var cmidCombatCastCurrentSpell  =  "{CombatCastCurrentSpell}";
var cmidCombatNextSpell         =         "{CombatNextSpell}";
var cmidCombatFirstSpellTab     =     "{CombatFirstSpellTab}";
var cmidCombatLastSpellTab      =      "{CombatLastSpellTab}";
var cmidCombatFirstSpell        =        "{CombatFirstSpell}";
var cmidCombatLastSpell         =         "{CombatLastSpell}";
var cmidUseSpellSlot_5          =          "{UseSpellSlot_5}";
var cmidUseSpellSlot_6          =          "{UseSpellSlot_6}";
var cmidUseSpellSlot_7          =          "{UseSpellSlot_7}";
var cmidUseSpellSlot_8          =          "{UseSpellSlot_8}";
var cmidUseSpellSlot_9          =          "{UseSpellSlot_9}";
var cmidAFKState                =                "{AFKState}";
var cmidAtEaseState             =             "{AtEaseState}";
var cmidATOYOT                  =                  "{ATOYOT}";
var cmidAkimbo                  =                  "{Akimbo}";
var cmidAkimboState             =             "{AkimboState}";
var cmidBeckon                  =                  "{Beckon}";
var cmidBeSeeingYou             =             "{BeSeeingYou}";
var cmidBlowKiss                =                "{BlowKiss}";
var cmidBowDeep                 =                 "{BowDeep}";
var cmidBowDeepState            =            "{BowDeepState}";
var cmidCheer                   =                   "{Cheer}";
var cmidClapHands               =               "{ClapHands}";
var cmidClapHandsState          =          "{ClapHandsState}";
var cmidCrossArmsState          =          "{CrossArmsState}";
var cmidCringe                  =                  "{Cringe}";
var cmidCry                     =                     "{Cry}";
var cmidCurtseyState            =            "{CurtseyState}";
var cmidDrudgeDance             =             "{DrudgeDance}";
var cmidDrudgeDanceState        =        "{DrudgeDanceState}";
var cmidHaveASeat               =               "{HaveASeat}";
var cmidHaveASeatState          =          "{HaveASeatState}";
var cmidHeartyLaugh             =             "{HeartyLaugh}";
var cmidHelper                  =                  "{Helper}";
var cmidKneel                   =                   "{Kneel}";
var cmidKneelState              =              "{KneelState}";
var cmidKnock                   =                   "{Knock}";
var cmidLaugh                   =                   "{Laugh}";
var cmidLeanState               =               "{LeanState}";
var cmidMeditateState           =           "{MeditateState}";
var cmidMimeDrink               =               "{MimeDrink}";
var cmidMimeEat                 =                 "{MimeEat}";
var cmidMock                    =                    "{Mock}";
var cmidNod                     =                     "{Nod}";
var cmidNudgeLeft               =               "{NudgeLeft}";
var cmidNudgeRight              =              "{NudgeRight}";
var cmidPlead                   =                   "{Plead}";
var cmidPleadState              =              "{PleadState}";
var cmidPoint                   =                   "{Point}";
var cmidPointState              =              "{PointState}";
var cmidPointDown               =               "{PointDown}";
var cmidPointDownState          =          "{PointDownState}";
var cmidPointLeft               =               "{PointLeft}";
var cmidPointLeftState          =          "{PointLeftState}";
var cmidPointRight              =              "{PointRight}";
var cmidPointRightState         =         "{PointRightState}";
var cmidPossumState             =             "{PossumState}";
var cmidPray                    =                    "{Pray}";
var cmidPrayState               =               "{PrayState}";
var cmidReadState               =               "{ReadState}";
var cmidSalute                  =                  "{Salute}";
var cmidSaluteState             =             "{SaluteState}";
var cmidScanHorizon             =             "{ScanHorizon}";
var cmidScratchHead             =             "{ScratchHead}";
var cmidScratchHeadState        =        "{ScratchHeadState}";
var cmidShakeHead               =               "{ShakeHead}";
var cmidShakeFist               =               "{ShakeFist}";
var cmidShakeFistState          =          "{ShakeFistState}";
var cmidShiver                  =                  "{Shiver}";
var cmidShiverState             =             "{ShiverState}";
var cmidShoo                    =                    "{Shoo}";
var cmidShrug                   =                   "{Shrug}";
var cmidSitState                =                "{SitState}";
var cmidSitBackState            =            "{SitBackState}";
var cmidSitCrossleggedState     =     "{SitCrossleggedState}";
var cmidSlouch                  =                  "{Slouch}";
var cmidSlouchState             =             "{SlouchState}";
var cmidSmackHead               =               "{SmackHead}";
var cmidSnowAngelState          =          "{SnowAngelState}";
var cmidSpit                    =                    "{Spit}";
var cmidSurrender               =               "{Surrender}";
var cmidSurrenderState          =          "{SurrenderState}";
var cmidTalktotheHandState      =      "{TalktotheHandState}";
var cmidTapFoot                 =                 "{TapFoot}";
var cmidTapFootState            =            "{TapFootState}";
var cmidTeapot                  =                  "{Teapot}";
var cmidThinkerState            =            "{ThinkerState}";
var cmidWarmHands               =               "{WarmHands}";
var cmidWave                    =                    "{Wave}";
var cmidWaveState               =               "{WaveState}";
var cmidWaveHigh                =                "{WaveHigh}";
var cmidWaveLow                 =                 "{WaveLow}";
var cmidWoah                    =                    "{Woah}";
var cmidWoahState               =               "{WoahState}";
var cmidWinded                  =                  "{Winded}";
var cmidWindedState             =             "{WindedState}";
var cmidYawnStretch             =             "{YawnStretch}";
var cmidYMCA                    =                    "{YMCA}";
var cmidCameraMoveToward        =        "{CameraMoveToward}";
var cmidCameraMoveAway          =          "{CameraMoveAway}";
var cmidCameraActivateAlternateMode="{CameraActivateAlternateMode}";
var cmidCameraInstantMouseLook  =  "{CameraInstantMouseLook}";
var cmidCameraRotateLeft        =        "{CameraRotateLeft}";
var cmidCameraRotateRight       =       "{CameraRotateRight}";
var cmidCameraRotateUp          =          "{CameraRotateUp}";
var cmidCameraRotateDown        =        "{CameraRotateDown}";
var cmidCameraViewDefault       =       "{CameraViewDefault}";
var cmidCameraViewFirstPerson   =   "{CameraViewFirstPerson}";
var cmidCameraViewLookDown      =      "{CameraViewLookDown}";
var cmidCameraViewMapMode       =       "{CameraViewMapMode}";
var cmidAltEnter                =                "{AltEnter}";
var cmidAltTab                  =                  "{AltTab}";
var cmidAltF4                   =                   "{AltF4}";
var cmidCtrlShiftEsc            =            "{CtrlShiftEsc}";
var cmidPointerX                =                "{PointerX}";
var cmidPointerY                =                "{PointerY}";
var cmidSelectLeft              =              "{SelectLeft}";
var cmidSelectRight             =             "{SelectRight}";
var cmidSelectMid               =               "{SelectMid}";
var cmidSelectDblLeft           =           "{SelectDblLeft}";
var cmidSelectDblRight          =          "{SelectDblRight}";
var cmidSelectDblMid            =            "{SelectDblMid}";
var cmidScrollUp                =                "{ScrollUp}";
var cmidScrollDown              =              "{ScrollDown}";
var cmidCursorCharLeft          =          "{CursorCharLeft}";
var cmidCursorCharRight         =         "{CursorCharRight}";
var cmidCursorPreviousLine      =      "{CursorPreviousLine}";
var cmidCursorNextLine          =          "{CursorNextLine}";
var cmidCursorPreviousPage      =      "{CursorPreviousPage}";
var cmidCursorNextPage          =          "{CursorNextPage}";
var cmidCursorWordLeft          =          "{CursorWordLeft}";
var cmidCursorWordRight         =         "{CursorWordRight}";
var cmidCursorStartOfLine       =       "{CursorStartOfLine}";
var cmidCursorStartOfDocument   =   "{CursorStartOfDocument}";
var cmidCursorEndOfLine         =         "{CursorEndOfLine}";
var cmidCursorEndOfDocument     =     "{CursorEndOfDocument}";
var cmidEscapeKey               =               "{EscapeKey}";
var cmidAcceptInput             =             "{AcceptInput}";
var cmidDeleteKey               =               "{DeleteKey}";
var cmidBackspaceKey            =            "{BackspaceKey}";
var cmidCopyText                =                "{CopyText}";
var cmidCutText                 =                 "{CutText}";
var cmidPasteText               =               "{PasteText}";
var cmidPlayerOption_ViewCombatTarget="{PlayerOption_ViewCombatTarget}";
var cmidPlayerOption_AdvancedCombatUI="{PlayerOption_AdvancedCombatUI}";
var cmidPlayerOption_AcceptLootPermits="{PlayerOption_AcceptLootPermits}";
var cmidPlayerOption_DisplayAllegianceLogonNotifications="{PlayerOption_DisplayAllegianceLogonNotifications}";
var cmidPlayerOption_UseChargeAttack="{PlayerOption_UseChargeAttack}";
var cmidPlayerOption_UseCraftSuccessDialog="{PlayerOption_UseCraftSuccessDialog}";
var cmidPlayerOption_HearAllegianceChat="{PlayerOption_HearAllegianceChat}";
var cmidPlayerOption_DisplayDateOfBirth="{PlayerOption_DisplayDateOfBirth}";
var cmidPlayerOption_DisplayAge = "{PlayerOption_DisplayAge}";
var cmidPlayerOption_DisplayChessRank="{PlayerOption_DisplayChessRank}";
var cmidPlayerOption_DisplayFishingSkill="{PlayerOption_DisplayFishingSkill}";
var cmidPlayerOption_DisplayNumberDeaths="{PlayerOption_DisplayNumberDeaths}";
var cmidPlayerOption_DisplayTimeStamps="{PlayerOption_DisplayTimeStamps}";
var cmidPlayerOption_SalvageMultiple="{PlayerOption_SalvageMultiple}";
var cmidPlayerOption_AutoRepeatAttack="{PlayerOption_AutoRepeatAttack}";
var cmidPlayerOption_IgnoreAllegianceRequests="{PlayerOption_IgnoreAllegianceRequests}";
var cmidPlayerOption_IgnoreFellowshipRequests="{PlayerOption_IgnoreFellowshipRequests}";
var cmidPlayerOption_IgnoreTradeRequests="{PlayerOption_IgnoreTradeRequests}";
var cmidPlayerOption_PersistentAtDay="{PlayerOption_PersistentAtDay}";
var cmidPlayerOption_AllowGive  =  "{PlayerOption_AllowGive}";
var cmidPlayerOption_ShowTooltips="{PlayerOption_ShowTooltips}";
var cmidPlayerOption_UseDeception="{PlayerOption_UseDeception}";
var cmidPlayerOption_ToggleRun  =  "{PlayerOption_ToggleRun}";
var cmidPlayerOption_StayInChatMode="{PlayerOption_StayInChatMode}";
var cmidPlayerOption_AutoTarget = "{PlayerOption_AutoTarget}";
var cmidPlayerOption_VividTargetingIndicator="{PlayerOption_VividTargetingIndicator}";
var cmidPlayerOption_FellowshipShareXP="{PlayerOption_FellowshipShareXP}";
var cmidPlayerOption_FellowshipShareLoot="{PlayerOption_FellowshipShareLoot}";
var cmidPlayerOption_FellowshipAutoAcceptRequests="{PlayerOption_FellowshipAutoAcceptRequests}";
var cmidPlayerOption_StretchUI  =  "{PlayerOption_StretchUI}";
var cmidPlayerOption_CoordinatesOnRadar="{PlayerOption_CoordinatesOnRadar}";
var cmidPlayerOption_SpellDuration="{PlayerOption_SpellDuration}";
var cmidPlayerOption_DisableHouseRestrictionEffects="{PlayerOption_DisableHouseRestrictionEffects}";
var cmidPlayerOption_DragItemOnPlayerOpensSecureTrade="{PlayerOption_DragItemOnPlayerOpensSecureTrade}";
var cmidCameraCloser            =            "{CameraCloser}";
var cmidCameraFarther           =           "{CameraFarther}";
var cmidCameraLeftRotate        =        "{CameraLeftRotate}";
var cmidCameraLower             =             "{CameraLower}";
var cmidCameraRaise             =             "{CameraRaise}";
var cmidCameraRightRotate       =       "{CameraRightRotate}";
var cmidFirstPersonView         =         "{FirstPersonView}";
var cmidFloorView               =               "{FloorView}";
var cmidMapView                 =                 "{MapView}";
var cmidResetView               =               "{ResetView}";
var cmidShiftView               =               "{ShiftView}";
var cmidAutoRun                 =                 "{AutoRun}";
var cmidHoldRun                 =                 "{HoldRun}";
var cmidHoldSidestep            =            "{HoldSidestep}";
var cmidJump                    =                    "{Jump}";
var cmidSideStepLeft            =            "{SideStepLeft}";
var cmidSideStepRight           =           "{SideStepRight}";
var cmidTurnLeft                =                "{TurnLeft}";
var cmidTurnRight               =               "{TurnRight}";
var cmidWalkBackwards           =           "{WalkBackwards}";
var cmidWalkForward             =             "{WalkForward}";
var cmidHighAttack              =              "{HighAttack}";
var cmidLowAttack               =               "{LowAttack}";
var cmidMediumAttack            =            "{MediumAttack}";
var cmidToggleCombat            =            "{ToggleCombat}";
var cmidDecreasePowerSetting    =    "{DecreasePowerSetting}";
var cmidIncreasePowerSetting    =    "{IncreasePowerSetting}";
var cmidClosestCompassItem      =      "{ClosestCompassItem}";
var cmidClosestItem             =             "{ClosestItem}";
var cmidClosestMonster          =          "{ClosestMonster}";
var cmidClosestPlayer           =           "{ClosestPlayer}";
var cmidLastAttacker            =            "{LastAttacker}";
var cmidNextCompassItem         =         "{NextCompassItem}";
var cmidNextFellow              =              "{NextFellow}";
var cmidNextItem                =                "{NextItem}";
var cmidNextMonster             =             "{NextMonster}";
var cmidNextPlayer              =              "{NextPlayer}";
var cmidPreviousCompassItem     =     "{PreviousCompassItem}";
var cmidPreviousFellow          =          "{PreviousFellow}";
var cmidPreviousItem            =            "{PreviousItem}";
var cmidPreviousMonster         =         "{PreviousMonster}";
var cmidPreviousPlayer          =          "{PreviousPlayer}";
var cmidPreviousSelection       =       "{PreviousSelection}";
var cmidDropSelected            =            "{DropSelected}";
var cmidExamineSelected         =         "{ExamineSelected}";
var cmidGiveSelected            =            "{GiveSelected}";
var cmidAutosortSelected        =        "{AutosortSelected}";
var cmidSelectSelf              =              "{SelectSelf}";
var cmidSplitSelected           =           "{SplitSelected}";
var cmidUseSelected             =             "{UseSelected}";
var cmidAllegiancePanel         =         "{AllegiancePanel}";
var cmidAttributesPanel         =         "{AttributesPanel}";
var cmidCharacterInformationPanel="{CharacterInformationPanel}";
var cmidCharacterOptionsPanel   =   "{CharacterOptionsPanel}";
var cmidFellowshipPanel         =         "{FellowshipPanel}";
var cmidHarmfulSpellsPanel      =      "{HarmfulSpellsPanel}";
var cmidHelpfulSpellsPanel      =      "{HelpfulSpellsPanel}";
var cmidHousePanel              =              "{HousePanel}";
var cmidInventoryPanel          =          "{InventoryPanel}";
var cmidLinkStatusPanel         =         "{LinkStatusPanel}";
var cmidMapPanel                =                "{MapPanel}";
var cmidOptionsPanel            =            "{OptionsPanel}";
var cmidSkillsPanel             =             "{SkillsPanel}";
var cmidSoundAndGraphicsPanel   =   "{SoundAndGraphicsPanel}";
var cmidSpellComponentsPanel    =    "{SpellComponentsPanel}";
var cmidSpellbookPanel          =          "{SpellbookPanel}";
var cmidTradePanel              =              "{TradePanel}";
var cmidVitaePanel              =              "{VitaePanel}";
var cmidAcceptCorpseLooting     =     "{AcceptCorpseLooting}";
var cmidAdvancedCombatInterface = "{AdvancedCombatInterface}";
var cmidAttemptToDeceivePlayers = "{AttemptToDeceivePlayers}";
var cmidAutoCreateShortcuts     =     "{AutoCreateShortcuts}";
var cmidAutoRepeatAttacks       =       "{AutoRepeatAttacks}";
var cmidAutoTarget              =              "{AutoTarget}";
var cmidAutoTrackCombatTargets  =  "{AutoTrackCombatTargets}";
var cmidDisableHouseEffect      =      "{DisableHouseEffect}";
var cmidDisableWeather          =          "{DisableWeather}";
var cmidDisplayTooltips         =         "{DisplayTooltips}";
var cmidIgnoreAllegianceRequests="{IgnoreAllegianceRequests}";
var cmidIgnoreFellowshipRequests="{IgnoreFellowshipRequests}";
var cmidIgnoreTradeRequests     =     "{IgnoreTradeRequests}";
var cmidInvertMouseLook         =         "{InvertMouseLook}";
var cmidLetPlayersGiveYouItems  =  "{LetPlayersGiveYouItems}";
var cmidMuteOnLosingFocus       =       "{MuteOnLosingFocus}";
var cmidRightClickToMouseLook   =   "{RightClickToMouseLook}";
var cmidRunAsDefaultMovement    =    "{RunAsDefaultMovement}";
var cmidShareFellowshipLoot     =     "{ShareFellowshipLoot}";
var cmidShareFellowshipXP       =       "{ShareFellowshipXP}";
var cmidShowRadarCoordinates    =    "{ShowRadarCoordinates}";
var cmidShowSpellDurations      =      "{ShowSpellDurations}";
var cmidStayInChatModeAfterSend = "{StayInChatModeAfterSend}";
var cmidStretchUI               =               "{StretchUI}";
var cmidVividTargetIndicator    =    "{VividTargetIndicator}";
var cmidCaptureScreenshotToFile = "{CaptureScreenshotToFile}";

// dmty: Damage type masks
// dmty values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to indicate more than one type of damage.
var dmtySlashing                =                      0x0001;
var dmtyPiercing                =                      0x0002;
var dmtyBludgeoning             =                      0x0004;
var dmtyCold                    =                      0x0008;
var dmtyFire                    =                      0x0010;
var dmtyAcid                    =                      0x0020;
var dmtyElectrical              =                      0x0040;

// eqm: Equipment type masks
// eqm values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to specify multiple equipment slots.
var eqmNil                      =                           0;	// No slot.
var eqmHead                     =                  0x00000001;
var eqmChestUnder               =                  0x00000002;
var eqmAbdomenUnder             =                  0x00000004;
var eqmUpperArmsUnder           =                  0x00000008;
var eqmLowerArmsUnder           =                  0x00000010;
var eqmHands                    =                  0x00000020;
var eqmUpperLegsUnder           =                  0x00000040;
var eqmLowerLegsUnder           =                  0x00000080;
var eqmFeet                     =                  0x00000100;
var eqmChestOuter               =                  0x00000200;
var eqmAbdomenOuter             =                  0x00000400;
var eqmUpperArmsOuter           =                  0x00000800;
var eqmLowerArmsOuter           =                  0x00001000;
var eqmUpperLegsOuter           =                  0x00002000;
var eqmLowerLegsOuter           =                  0x00004000;
var eqmNecklace                 =                  0x00008000;
var eqmRBracelet                =                  0x00010000;
var eqmLBracelet                =                  0x00020000;
var eqmBracelets                =                  0x00030000;	// Both bracelet slots.
var eqmRRing                    =                  0x00040000;
var eqmLRing                    =                  0x00080000;
var eqmRings                    =                  0x000C0000;	// Both ring slots.
var eqmWeapon                   =                  0x00100000;	// Melee weapon slot.
var eqmShield                   =                  0x00200000;
var eqmRangedWeapon             =                  0x00400000;	// Bow or crossbow slot.
var eqmAmmo                     =                  0x00800000;
var eqmFocusWeapon              =                  0x01000000;	// Wand, staff, or orb slot.
var eqmWeapons                  =                  0x01500000;	// All weapons of any kind.
var eqmAll                      =                  0x01FFFFFF;	// All equipment slots.

// evid: Event IDs
var evidNil                     =                           0;	// No event was received.
var evidOnVitals                =                           1;
var evidOnLocChangeSelf         =                           2;
var evidOnLocChangeOther        =                           3;
var evidOnStatTotalBurden       =                           4;
var evidOnStatTotalExp          =                           5;
var evidOnStatUnspentExp        =                           6;
var evidOnObjectCreatePlayer    =                           7;
var evidOnStatSkillExp          =                           8;
var evidOnAdjustStack           =                           9;
var evidOnChatLocal             =                          10;
var evidOnChatSpell             =                          11;
var evidOnDeathSelf             =                          12;
var evidOnMyPlayerKill          =                          13;
var evidOnDeathOther            =                          14;
var evidOnEnd3D                 =                          15;
var evidOnStart3D               =                          16;
var evidOnStartPortalSelf       =                          17;
var evidOnEndPortalSelf         =                          18;
var evidOnEndPortalOther        =                          19;
var evidOnCombatMode            =                          20;
var evidOnAnimationSelf         =                          21;
var evidOnTargetHealth          =                          22;
var evidOnAddToInventory        =                          23;
var evidOnRemoveFromInventory   =                          24;
var evidOnAssessCreature        =                          25;
var evidOnAssessItem            =                          26;
var evidOnSpellFailSelf         =                          27;
var evidOnMeleeEvadeSelf        =                          28;
var evidOnMeleeEvadeOther       =                          29;
var evidOnMeleeDamageSelf       =                          30;
var evidOnMeleeDamageOther      =                          31;
var evidOnLogon                 =                          32;
var evidOnTellServer            =                          33;
var evidOnTell                  =                          34;
var evidOnTellMelee             =                          35;
var evidOnTellMisc              =                          36;
var evidOnTellFellowship        =                          37;
var evidOnTellPatron            =                          38;
var evidOnTellVassal            =                          39;
var evidOnTellFollower          =                          40;
var evidOnDeathMessage          =                          41;
var evidOnSpellCastSelf         =                          42;
var evidOnTradeStart            =                          43;
var evidOnTradeEnd              =                          44;
var evidOnTradeAdd              =                          45;
var evidOnTradeReset            =                          46;
var evidOnSetFlagSelf           =                          47;
var evidOnSetFlagOther          =                          48;
var evidOnApproachVendor        =                          49;
var evidOnChatBroadcast         =                          50;
var evidOnTradeAccept           =                          51;
var evidOnObjectCreate          =                          52;
var evidOnChatServer            =                          53;
var evidOnSpellExpire           =                          54;
var evidOnSpellExpireSilent     =                          55;
var evidOnStatTotalPyreals      =                          56;
var evidOnStatLevel             =                          57;
var evidOnChatEmoteStandard     =                          58;
var evidOnChatEmoteCustom       =                          59;
var evidOnOpenContainer         =                          60;
var evidOnPortalStormWarning    =                          61;
var evidOnPortalStormed         =                          62;
var evidOnCommand               =                          63;
var evidOnControlEvent          =                          64;
var evidOnItemManaBar           =                          65;
var evidOnActionComplete        =                          66;
var evidOnObjectDestroy         =                          67;
var evidOnLocChangeCreature     =                          68;
var evidOnMoveItem              =                          69;
var evidOnSpellAdd              =                          70;
var evidOnFellowCreate          =                          71;
var evidOnFellowDisband         =                          72;
var evidOnFellowDismiss         =                          73;
var evidOnFellowQuit            =                          74;
var evidOnFellowRecruit         =                          75;
var evidOnMeleeLastAttacker     =                          76;
var evidOnLeaveVendor           =                          77;
var evidOnTimer                 =                          78;
var evidOnControlProperty       =                          79;
var evidOnPluginMsg             =                          80;
var evidOnTellAllegiance        =                          81;
var evidOnTellCovassal          =                          82;
var evidOnHotkey                =                          83;
var evidOnFellowInvite          =                          84;
var evidOnNavStop               =                          85;
var evidOnFellowUpdate          =                          86;
var evidOnDisconnect            =                          87;
var evidOnCraftConfirm          =                          88;
var evidOnChatBoxMessage        =                          89;
var evidOnTipMessage            =                          90;
var evidOnOpenContainerPanel    =                          91;
var evidOnCloseContainer        =                          92;
var evidOnChatBoxClick          =                          93;
var evidMax                     =                          94;

// ioc: Icon outline color masks
// ioc values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript).
var iocNil                      =                           0;
var iocEnchanted                =                  0x00000001;	// Has spells.
var iocUnknown2                 =                  0x00000002;	// (No known meaning.)
var iocHealing                  =                  0x00000004;	// Restores health.
var iocMana                     =                  0x00000008;	// Restores mana.
var iocHearty                   =                  0x00000010;	// Restores stamina.
var iocFire                     =                  0x00000020;
var iocLightning                =                  0x00000040;
var iocFrost                    =                  0x00000080;
var iocAcid                     =                  0x00000100;

// kmo: Key motions
var kmoDown                     =                        0x01;	// Press the key.
var kmoUp                       =                        0x02;	// Release the key.
var kmoClick                    =                        0x03;	// Press and release.

// material: Material codes
var materialCeramic             =                           1;
var materialPorcelain           =                           2;
var materialCloth               =                           3;
var materialLinen               =                           4;
var materialSatin               =                           5;
var materialSilk                =                           6;
var materialVelvet              =                           7;
var materialWool                =                           8;
var materialGem                 =                           9;
var materialAgate               =                          10;
var materialAmber               =                          11;
var materialAmethyst            =                          12;
var materialAquamarine          =                          13;
var materialAzurite             =                          14;
var materialBlackGarnet         =                          15;
var materialBlackOpal           =                          16;
var materialBloodstone          =                          17;
var materialCarnelian           =                          18;
var materialCitrine             =                          19;
var materialDiamond             =                          20;
var materialEmerald             =                          21;
var materialFireOpal            =                          22;
var materialGreenGarnet         =                          23;
var materialGreenJade           =                          24;
var materialHematite            =                          25;
var materialImperialTopaz       =                          26;
var materialJet                 =                          27;
var materialLapisLazuli         =                          28;
var materialLavenderJade        =                          29;
var materialMalachite           =                          30;
var materialMoonstone           =                          31;
var materialOnyx                =                          32;
var materialOpal                =                          33;
var materialPeridot             =                          34;
var materialRedGarnet           =                          35;
var materialRedJade             =                          36;
var materialRoseQuartz          =                          37;
var materialRuby                =                          38;
var materialSapphire            =                          39;
var materialSmokeyQuartz        =                          40;
var materialSunstone            =                          41;
var materialTigerEye            =                          42;
var materialTourmaline          =                          43;
var materialTurquoise           =                          44;
var materialWhiteJade           =                          45;
var materialWhiteQuartz         =                          46;
var materialWhiteSapphire       =                          47;
var materialYellowGarnet        =                          48;
var materialYellowTopaz         =                          49;
var materialZircon              =                          50;
var materialIvory               =                          51;
var materialLeather             =                          52;
var materialDilloHide           =                          53;
var materialGromnieHide         =                          54;
var materialReedsharkHide       =                          55;
var materialMetal               =                          56;
var materialBrass               =                          57;
var materialBronze              =                          58;
var materialCopper              =                          59;
var materialGold                =                          60;
var materialIron                =                          61;
var materialPyreal              =                          62;
var materialSilver              =                          63;
var materialSteel               =                          64;
var materialStone               =                          65;
var materialAlabaster           =                          66;
var materialGranite             =                          67;
var materialMarble              =                          68;
var materialObsidian            =                          69;
var materialSandstone           =                          70;
var materialSerpentine          =                          71;
var materialWood                =                          72;
var materialEbony               =                          73;
var materialMahogany            =                          74;
var materialOak                 =                          75;
var materialPine                =                          76;
var materialTeak                =                          77;

// mcm: Merchant category masks
var mcmNil                      =                  0x00000000;	// No category; not saleable.
var mcmWeaponsMelee             =                  0x00000001;
var mcmArmor                    =                  0x00000002;
var mcmClothing                 =                  0x00000004;
var mcmJewelry                  =                  0x00000008;
var mcmCreature                 =                  0x00000010;
var mcmFood                     =                  0x00000020;
var mcmPyreal                   =                  0x00000040;
var mcmMisc                     =                  0x00000080;
var mcmWeaponsMissile           =                  0x00000100;
var mcmContainers               =                  0x00000200;
var mcmMiscFletching            =                  0x00000400;
var mcmGems                     =                  0x00000800;
var mcmSpellComponents          =                  0x00001000;
var mcmBooksPaper               =                  0x00002000;
var mcmKeysTools                =                  0x00004000;
var mcmMagicItems               =                  0x00008000;
var mcmPortal                   =                  0x00010000;
var mcmLockable                 =                  0x00020000;	// Chests have this is addition to mcmContainers.
var mcmTradeNotes               =                  0x00040000;
var mcmManaStones               =                  0x00080000;
var mcmServices                 =                  0x00100000;
var mcmPlants                   =                  0x00200000;
var mcmCookingItems1            =                  0x00400000;
var mcmAlchemicalItems1         =                  0x00800000;
var mcmFletchingItems1          =                  0x01000000;
var mcmCookingItems2            =                  0x02000000;
var mcmAlchemicalItems2         =                  0x04000000;
var mcmFletchingItems2          =                  0x08000000;
var mcmLifestone                =                  0x10000000;
var mcmTinkeringTools           =                  0x20000000;	// The Ust is in this category.
var mcmSalvageBag               =                  0x40000000;
var mcmChessboard               =                  0x80000000;	// There may be other items in this category as well.

// Miscellaneous constants
var oidNil                      =                           0;	// Signifies no object.
var ipackMain                   =                           0;	// Index of the main pack.
var iitemNil                    =                          -1;	// Signifies the pack itself.
var clrNil                      =                  0x7FFFFFFF;	// VBScript has a problem with FFFFFFFF (turns it into 0000FFFF).
var spellidNil                  =                           0;	// Signifies no spell.

// ibutton: Mouse button IDs
var ibuttonLeft                 =                           0;	// Left button.
var ibuttonRight                =                           1;	// Right button.
var ibuttonMiddle               =                           2;	// Middle button.

// nopt: Navigation option masks
var noptWalk                    =                      0x0001;
var noptStopOnArrival           =                      0x0002;
var noptZigzag                  =                      0x0004;

// nsc: Navigation stop codes for OnNavStop
var nscArrived                  =                           0;	// You arrived at the target location.
var nscTimeout                  =                           1;	// You did not arrive before the timeout expired.
var nscCanceled                 =                           2;	// skapi.CancelNav was called.

// ocm: Object category masks
// ocm values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to specify multiple categories.
var ocmNil                      =                      0x0000;	// Include no objects.
var ocmPlayer                   =                      0x0001;
var ocmMonster                  =                      0x0002;
var ocmPlayerCorpse             =                      0x0004;
var ocmMonsterCorpse            =                      0x0008;
var ocmLifestone                =                      0x0010;
var ocmPortal                   =                      0x0020;
var ocmMerchant                 =                      0x0040;
var ocmEquipment                =                      0x0080;
var ocmPK                       =                      0x0100;
var ocmNonPK                    =                      0x0200;
var ocmNPC                      =                      0x0400;
var ocmHook                     =                      0x0800;
var ocmAll                      =                          -1;	// Include all nearby objects of any kind.

// olc: Object location categories
// olc values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to specify multiple categories.
var olcNil                      =                      0x0000;	// Include no objects.
var olcInventory                =                      0x0001;	// Include objects in your inventory.
var olcContained                =                      0x0002;	// Include objects contained in another object (e.g. a chest or corpse).
var olcEquipped                 =                      0x0004;	// Include objects being worn or wielded (by you or someone else).
var olcOnGround                 =                      0x0008;	// Include objects out on their own in the game world.
var olcAll                      =                          -1;	// Include all object regardless of location.

// oty: Object type masks
var otyContainer                =                  0x00000001;
var otyInscribable              =                  0x00000002;
var otyNoPickup                 =                  0x00000004;
var otyPlayer                   =                  0x00000008;
var otySelectable               =                  0x00000010;
var otyPK                       =                  0x00000020;
var otyReadable                 =                  0x00000100;
var otyMerchant                 =                  0x00000200;
var otyDoor                     =                  0x00001000;
var otyCorpse                   =                  0x00002000;
var otyLifestone                =                  0x00004000;
var otyFood                     =                  0x00008000;
var otyHealingKit               =                  0x00010000;
var otyLockpick                 =                  0x00020000;
var otyPortal                   =                  0x00040000;
var otyFoci                     =                  0x00800000;	// The new magic foci, I think.
var otyPKL                      =                  0x02000000;

// opm: Output modes
// opm values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to send output to multiple destinations.
var opmDebugLog                 =                      0x0001;	// Sends the output to the debug terminal (using the Win32 OutputDebugString API).
var opmConsole                  =                      0x0002;	// Sends the output to the SkunkWorks console window.
var opmChatWnd                  =                      0x0010;	// Sends the output to the primary in-game chat window.
var opmChatWnd1                 =                      0x0020;	// Sends the output to floating chat window #1.
var opmChatWnd2                 =                      0x0040;	// Sends the output to floating chat window #2.
var opmChatWnd3                 =                      0x0080;	// Sends the output to floating chat window #3.
var opmChatWnd4                 =                      0x0100;	// Sends the output to floating chat window #4.
var opmChatWndAll               =                      0x01F0;	// Sends the output to all in-game chat windows.

// plig: Player in game codes
var pligNotRunning              =                           0;	// Game is not running.
var pligAtLogin                 =                           1;	// Game is at login screen.
var pligInPortal                =                           2;	// Player is in portal space.
var pligInWorld                 =                           3;	// Player is in the game world.

// psw: Portal storm warning codes
var pswStormEnded               =                           0;	// The portal storm is over.
var pswMildStorm                =                           1;	// Mild portal storm.
var pswHeavyStorm               =                           2;	// Heavy portal storm.

// skid: Skill IDs
var skidNil                     =                           0;	// No skill.
var skidAxe                     =                           1;
var skidBow                     =                           2;
var skidCrossbow                =                           3;
var skidDagger                  =                           4;
var skidMace                    =                           5;
var skidMeleeDefense            =                           6;
var skidMissileDefense          =                           7;
var skidSpear                   =                           9;
var skidStaff                   =                          10;
var skidSword                   =                          11;
var skidThrownWeapons           =                          12;
var skidUnarmedCombat           =                          13;
var skidArcaneLore              =                          14;
var skidMagicDefense            =                          15;
var skidManaConversion          =                          16;
var skidItemTinkering           =                          18;
var skidAssessPerson            =                          19;
var skidDeception               =                          20;
var skidHealing                 =                          21;
var skidJump                    =                          22;
var skidLockpick                =                          23;
var skidRun                     =                          24;
var skidAssessCreature          =                          27;
var skidWeaponTinkering         =                          28;
var skidArmorTinkering          =                          29;
var skidMagicItemTinkering      =                          30;
var skidCreatureEnchantment     =                          31;
var skidItemEnchantment         =                          32;
var skidLifeMagic               =                          33;
var skidWarMagic                =                          34;
var skidLeadership              =                          35;
var skidLoyalty                 =                          36;
var skidFletching               =                          37;
var skidAlchemy                 =                          38;
var skidCooking                 =                          39;
var skidSalvaging               =                          40;
var skidMax                     =                          41;

// skts: Skill training status codes
var sktsUnusable                =                           0;
var sktsUntrained               =                           1;
var sktsTrained                 =                           2;
var sktsSpecialized             =                           3;

// Special key names
// {alt}
// {back}                                                        // Backspace
// {break}                                                       // Ctrl+Break
// {capslock}
// {control}
// {del}
// {down}                                                        // Down arrow
// {end}
// {esc}
// {f1}
// {f2}
// {f3}
// {f4}
// {f5}
// {f6}
// {f7}
// {f8}
// {f9}
// {f10}
// {f11}
// {f12}
// {home}
// {ins}
// {keypad 0}
// {keypad 1}
// {keypad 2}
// {keypad 3}
// {keypad 4}
// {keypad 5}
// {keypad 6}
// {keypad 7}
// {keypad 8}
// {keypad 9}
// {keypad *}
// {keypad +}
// {keypad -}
// {keypad /}
// {keypad .}
// {keypad enter}                                                // Keypad Enter
// {left}                                                        // Left arrow
// {numlock}
// {pause}
// {pgdn}
// {pgup}
// {prtsc}                                                       // Print Screen
// {return}                                                      // Enter (the regular one, above RShift)
// {right}                                                       // Right arrow
// {scroll}                                                      // Scroll Lock
// {shift}
// {space}                                                       // Spacebar
// {tab}
// {up}                                                          // Up arrow

// species: Species codes
var speciesInvalid              =                           0;
var speciesOlthoi               =                           1;
var speciesBanderling           =                           2;
var speciesDrudge               =                           3;
var speciesMosswart             =                           4;
var speciesLugian               =                           5;
var speciesTumerok              =                           6;
var speciesMite                 =                           7;
var speciesTusker               =                           8;
var speciesPhyntosWasp          =                           9;
var speciesRat                  =                          10;
var speciesAuroch               =                          11;
var speciesCow                  =                          12;
var speciesGolem                =                          13;
var speciesUndead               =                          14;
var speciesGromnie              =                          15;
var speciesReedshark            =                          16;
var speciesArmoredillo          =                          17;
var speciesFae                  =                          18;
var speciesVirindi              =                          19;
var speciesWisp                 =                          20;
var speciesKnathtead            =                          21;
var speciesShadow               =                          22;
var speciesMattekar             =                          23;
var speciesMumiyah              =                          24;
var speciesRabbit               =                          25;
var speciesSclavus              =                          26;
var speciesShallowsShark        =                          27;
var speciesMonouga              =                          28;
var speciesZefir                =                          29;
var speciesSkeleton             =                          30;
var speciesHuman                =                          31;
var speciesShreth               =                          32;
var speciesChittick             =                          33;
var speciesMoarsman             =                          34;
var speciesOlthoiLarvae         =                          35;
var speciesSlithis              =                          36;
var speciesDeru                 =                          37;
var speciesFireElemental        =                          38;
var speciesSnowman              =                          39;
var speciesUnknown              =                          40;
var speciesBunny                =                          41;
var speciesLightningElemental   =                          42;
var speciesRockslide            =                          43;
var speciesGrievver             =                          44;
var speciesNiffis               =                          45;
var speciesUrsuin               =                          46;
var speciesCrystal              =                          47;
var speciesHollowMinion         =                          48;
var speciesScarecrow            =                          49;
var speciesIdol                 =                          50;
var speciesEmpyrean             =                          51;
var speciesHopeslayer           =                          52;
var speciesDoll                 =                          53;
var speciesMarionette           =                          54;
var speciesCarenzi              =                          55;
var speciesSiraluun             =                          56;
var speciesAunTumerok           =                          57;
var speciesHeaTumerok           =                          58;
var speciesSimulacrum           =                          59;
var speciesAcidElemental        =                          60;
var speciesFrostElemental       =                          61;
var speciesElemental            =                          62;
var speciesStatue               =                          63;
var speciesWall                 =                          64;
var speciesAlteredHuman         =                          65;
var speciesDevice               =                          66;
var speciesHarbinger            =                          67;
var speciesDarkSarcophagus      =                          68;
var speciesChicken              =                          69;
var speciesGotrokLugian         =                          70;
var speciesMargul               =                          71;
var speciesBleachedRabbit       =                          72;
var speciesNastyRabbit          =                          73;
var speciesGrimacingRabbit      =                          74;
var speciesBurun                =                          75;
var speciesTarget               =                          76;
var speciesGhost                =                          77;
var speciesFiun                 =                          78;
var speciesEater                =                          79;
var speciesPenguin              =                          80;
var speciesRuschk               =                          81;
var speciesThrungus             =                          82;
var speciesViamontianKnight     =                          83;
var speciesRemoran              =                          84;
var speciesSwarm                =                          85;
var speciesMoar                 =                          86;
var speciesEnchantedArms        =                          87;
var speciesSleech               =                          88;
var speciesMukkir               =                          89;

// vital: Vital stat codes
var vitalNil                    =                           0;
var vitalHealthMax              =                           1;
var vitalHealthCur              =                           2;	// Cannot be used with skapi.SkinfoFromVital.
var vitalStaminaMax             =                           3;
var vitalStaminaCur             =                           4;	// Cannot be used with skapi.SkinfoFromVital.
var vitalManaMax                =                           5;
var vitalManaCur                =                           6;	// Cannot be used with skapi.SkinfoFromVital.
var vitalMax                    =                           7;

// wem: WaitEvent modes
var wemNormal                   =                           0;	// Returns when timeout elapsed or the queue is empty.
var wemSingle                   =                           1;	// Waits for a single event of any type.
var wemSpecific                 =                           2;	// Waits for a specific event type.
var wemFullTimeout              =                           3;	// Always waits the full timeout interval.


