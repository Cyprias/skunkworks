Option Explicit

' String-manipulation subroutines.


Public Const iNil = -1
Public Const ichNil = -1

' The decimal separator character (period in the US, comma in some European locales).
Public szDecimal
szDecimal = SzMid(FormatNumber(0, 1), 1, 1)

' Not really string-related, but this is a convenient place to stick these.
Public Const ForReading = 1
Public Const ForWriting = 2
Public Const ForAppending = 8
Public fso
Set fso = CreateObject("Scripting.FileSystemObject")


'------------------
' Public functions
'------------------

' Return True if the given sz matches the given regular expression pattern.
Public Function FMatchSzRex(sz, szPattern, fIgnoreCase)
	
	Dim rex : Set rex = New RegExp
	rex.Pattern = szPattern
	rex.IgnoreCase = fIgnoreCase
	FMatchSzRex = rex.Test(sz)
	
End Function

' Return a new string in which instances of szPattern in sz are replaced by szNew.
Public Function SzReplace(sz, szPattern, szNew)
	
	Dim rex : Set rex = New RegExp
	rex.Pattern = szPattern
	rex.IgnoreCase = True
	rex.Global = True
	SzReplace = rex.Replace(sz, szNew)
	
End Function


' Return True iff the given string contains only alphabetic characters.
Public Function FAlphaSz(sz)
	
	Dim ich
	For ich = 0 To Len(sz) - 1
		Dim szCh : szCh = UCase(SzMid(sz, ich, 1))
		If Not (szCh >= "A" And szCh <= "Z") Then
			FAlphaSz = False
			Exit Function
		End If
	Next
	
	FAlphaSz = True
End Function

' Return True iff the given string contains only digits.
Public Function FNumericSz(sz)
	
	Dim ich
	For ich = 0 To Len(sz) - 1
		Dim szCh : szCh = SzMid(sz, ich, 1)
		If Not (szCh >= "0" And szCh <= "9") Then
			FNumericSz = False
			Exit Function
		End If
	Next
	
	FNumericSz = True
End Function

' Return True iff sz1 contains sz2 using case-insensitive comparison.
Public Function FContainsSz(sz1, sz2)
	
	FContainsSz = (IchInStr(0, sz1, sz2) <> ichNil)
	
End Function

' Return True iff sz1 matches sz2 using case-insensitive comparison.
Public Function FEqSzI(sz1, sz2)
	
	FEqSzI = (StrComp(sz1, sz2, vbTextCompare) = 0)
	
End Function

' Return True iff szPrefix matches the beginning (or all) of sz.
' The comparison is case-insensitive.
Public Function FPrefixMatchSz(sz, szPrefix)
	
	Dim cchPrefix: cchPrefix = Len(szPrefix)
	If cchPrefix <= Len(sz) Then
		FPrefixMatchSz = FEqSzI(Left(sz, cchPrefix), szPrefix)
	Else
		FPrefixMatchSz = False
	End If
	
End Function

' Return True iff szSuffix matches the end (or all) of sz.
' The comparison is case-insensitive.
Public Function FSuffixMatchSz(sz, szSuffix)
	
	Dim cchSuffix: cchSuffix = Len(szSuffix)
	If cchSuffix <= Len(sz) Then
		FSuffixMatchSz = FEqSzI(Right(sz, cchSuffix), szSuffix)
	Else
		FSuffixMatchSz = False
	End If
	
End Function

' Return True if sz looks like it might be a filename, False otherwise.
Public Function FLooksLikeFileName(sz)
	
	If FContainsSz(sz, "\") Or FContainsSz(sz, ".") Then
		FLooksLikeFileName = True
	Else
		FLooksLikeFileName = False
	End If
	
End Function

' Return True if szPath looks like a relative file path, False otherwise.
Public Function FRelativePathSz(szPath)
    
    FRelativePathSz = Not (Left(szPath, 2) = "\\" Or SzMid(szPath, 1, 1) = ":")
    
End Function

' Zero-origin version of InStr.
' Return the zero-origin position of the first instance (if any) of sz2 within 
' sz1, starting at position ichFirst or later.  If not found, return -1. 
Public Function IchInStr(ichFirst, sz1, sz2)
	
	IchInStr = InStr(1 + ichFirst, sz1, sz2, vbTextCompare) - 1
	
End Function

' Zero-origin version of InStrRev.
' Return the zero-origin position of the last instance (if any) of sz2 within 
' sz1.  If not found, return -1. 
Public Function IchInStrRev(sz1 , sz2)

	IchInStrRev = InStrRev(sz1, sz2, -1, vbTextCompare) - 1
	
End Function

' Extract the ith comma-delimited field from szLine (counting from zero) and 
' return it as a Long.
Public Function LExtractField(szLine, ifield)
	
	LExtractField = LFromSzEmpty(SzExtractField(szLine, ifield))
	
End Function

' Interpret the given string as a number and return its value as a Long.  If 
' the string is empty, return zero.
Public Function LFromSzEmpty(sz)
	
	If sz <> "" Then
		LFromSzEmpty = CLng(sz)
	Else
		LFromSzEmpty = 0
	End If
	
End Function

' Interpret the given string as a C/C++/JScript-format hexadecimal number and 
' return its value as a Long.
Private Function LFromSzHex(sz)
	
	If FPrefixMatchSz(sz, "0x") Then
		LFromSzHex = CLng("&H" + Right(sz, Len(sz) - 2))
	Else
		LFromSzHex = CLng(sz)
	End If
	
End Function

' Interpret the given Variant as a number and return its value as a Long.  If 
' v is Null, return zero.
Public Function LFromV(v)
	
	If IsNull(v) Then
		LFromV = 0
	Else
		LFromV = CLng(v)
	End If
	
End Function

' Search sz for the first occurrence of szSep; copy the portion of sz prior to 
' szSep into szHead, and the portion following szSep into szTail.  If szSep is 
' not found, copy all of sz to szHead, and the empty string to szTail.
Public Function SplitSz(szHead, szTail, sz, szSep)
	
	Dim ich, szHeadT
	ich = IchInStr(0, sz, szSep)
	If ich >= 0 Then
		szHeadT = Left(sz, ich)
		szTail = Right(sz, Len(sz) - (ich + Len(szSep)))
		If szSep = " " Then szTail = SzTrimWhiteSpace(szTail)
	Else
		szHeadT = sz
		szTail = ""
	End If
	szHead = szHeadT
	
End Function

' Search sz for the last occurrence of szSep; copy the portion of sz prior to 
' szSep into szHead, and the portion following szSep into szTail.  If szSep is 
' not found, copy the empty string to szHead, and all of sz to szTail.
Public Function SplitSzRev(szHead, szTail, sz, szSep)
	
	Dim ich, szHeadT
	ich = IchInStrRev(sz, szSep)
	If ich >= 0 Then
		szHeadT = Left(sz, ich)
		szTail = Right(sz, Len(sz) - (ich + Len(szSep)))
		If szSep = " " Then szHeadT = SzTrimWhiteSpace(szHeadT)
	Else
		szHeadT = ""
		szTail = sz
	End If
	szHead = szHeadT
	
End Function

' Extract the (zero-origin) ifield'th field from a line of comma-separated fields.
Public Function SzExtractField(szLine, ifield)
	
	SzExtractField = SzExtractFieldEx(szLine, ifield, ",")
	
End Function

' Extract the (zero-origin) ifield'th field from a line of fields.
Public Function SzExtractFieldEx(szLine, ifield, szSep)
	
	Dim ifieldT, ichFirst, ichLim
	
	ifieldT = 0
	ichFirst = 0
	Do
		ichLim = IchInStr(ichFirst, szLine, szSep)
		If ichLim = -1 Then
			If ifieldT < ifield Then
				SzExtractFieldEx = ""
				Exit Function
			End If
			ichLim = Len(szLine)
		End If
		If ifieldT = ifield Then Exit Do
		ifieldT = ifieldT + 1
		ichFirst = ichLim + 1
	Loop
	
	SzExtractFieldEx = SzTrimWhiteSpace(SzMid(szLine, ichFirst, ichLim - ichFirst))
	
End Function

' Return a decimal representation of the long integer l, at least cdigit 
' digits in length.  Pad with leading zeroes as necessary.
Public Function SzFromL(l, cdigit)
	
	SzFromL = SzPadLeft(CStr(l), cdigit, "0")
	
End Function

' Return a decimal representation of the long integer l, at least cch 
' characters in length.  Pad with leading spaces as necessary.
Public Function SzFromLSpace(l, cch)
	
	SzFromLSpace = SzPadLeft(CStr(l), cch, " ")
	
End Function

' Return a string representation of the time value t, to millisecond prescision.
Public Function SzFromT(t)
	
	Dim l, cmsec, csec, cmin, chour
	l = Round(t * 86400 * 1000)
	cmsec = l Mod 1000
	l = l \ 1000
	csec = l Mod 60
	l = l \ 60
	cmin = l Mod 60
	l = l \ 60
	chour = l Mod 24
	
	SzFromT = SzFromL(chour, 2) + ":" + _
			  SzFromL(cmin, 2) + ":" + _
			  SzFromL(csec, 2) + "." + _
			  SzFromL(cmsec, 3)
	
End Function

' Return a C/C++/JScript-format hexadecimal representation of the long integer 
' l, at least cdigit digits in length.  Pad with leading zeroes as necessary, 
' and prefix with "0x".
Public Function SzHex(l, cdigit)
	
	SzHex = "0x" + SzPadLeft(Hex(l), cdigit, "0")
	
End Function

' Return a hexadecimal representation of the long integer l, at least 8 digits 
' in length.  Pad with leading zeroes as necessary, and prefix with "0x".
Public Function SzHex8(l)
	
	SzHex8 = SzHex(l, 8)
	
End Function

' Given a count of seconds, return a string formatted as hh:mm:ss.
Public Function SzHourMinSec(csec)
	
	Dim chour : chour = csec \ 3600
	Dim cmin : cmin = (csec Mod 3600) \ 60
	SzHourMinSec = CStr(chour) + ":" + SzFromL(cmin, 2) + ":" + SzFromL(csec Mod 60, 2)
	
End Function

' Zero-origin version of Mid.
' Return a substring of length cch starting at ich in sz.
Public Function SzMid(sz, ich, cch)
	
	SzMid = Mid(sz, ich + 1, cch)
	
End Function

' Return a string of the form "1 thing" or "N things" depending on the actual 
' value of citem.
Public Function SzNItems(citem, szItem)
	
	SzNItems = CStr(citem) + " " + SzPluralEx(szItem, citem)
	
End Function

' Pad the given sz out to cch characters by prefixing with ch.
Public Function SzPadLeft(sz, cch, ch)
	
	If Len(sz) < cch Then
		SzPadLeft = String(cch - Len(sz), ch) + sz
	Else
		SzPadLeft = sz
	End If
	
End Function

' Pad the given sz out to cch characters by suffixing with ch.
Public Function SzPadRight(sz, cch, ch)
	
	If Len(sz) < cch Then
		SzPadRight = sz + String(cch - Len(sz), ch)
	Else
		SzPadRight = sz
	End If
	
End Function

' Return a string representing the plural of the given sz.
Public Function SzPlural(sz)
	
	Dim szT : szT = sz
	If FPrefixMatchSz(szT, "A ") Then
		szT = Right(szT, Len(szT) - 2)
	ElseIf FPrefixMatchSz(szT, "An ") Then
		szT = Right(szT, Len(szT) - 3)
	End If
	
	Dim szHead, szTail
	Call SplitSz(szHead, szTail, szT, " of ")
	If Not FSuffixMatchSz(szHead, "s") Then
		szHead = szHead + "s"
	End If
	
	If szTail <> "" Then
		SzPlural = szHead + " of " + szTail
	Else
		SzPlural = szHead
	End If
	
End Function

' If citem is one, return sz unmodified.  Otherwise return the plural of sz.
Public Function SzPluralEx(sz, citem)
	
	If citem = 1 Then
		SzPluralEx = sz
	Else
		SzPluralEx = SzPlural(sz)
	End If
	
End Function

' Return the given string wrapped in double-quotes.
Public Function SzQuote(sz)
	
	SzQuote = """" + SzReplace(sz, """", """""") + """"
	
End Function

' If the given sz is wrapped in brackets specified by szLeft and szRight, 
' strip them away and return the unbracketed contents.  szLeft and szRight 
' may be the same as in the case of double-quotes, for instance.
Public Function SzStripBrackets(sz, szLeft, szRight)
	
	Dim szT : szT = sz
	
	If FPrefixMatchSz(szT, szLeft) Then
		szT = Right(szT, Len(szT) - Len(szLeft))
	End If
	
	If FSuffixMatchSz(szT, szRight) Then
		szT = Left(szT, Len(szT) - Len(szRight))
	End If
	
	SzStripBrackets = SzTrimWhiteSpace(szT)
	
End Function

' Return a copy of sz with leading and trailing white space removed.
Public Function SzTrimWhiteSpace(sz)
	
	Dim rex : Set rex = New RegExp
	rex.Pattern = "^\s*(\S+(\s+\S+)*)?\s*$"
	rex.IgnoreCase = True
	rex.Global = False
	
	SzTrimWhiteSpace = rex.Replace(sz, "$1")
	
End Function


