// Welcome to t.js for SKUNKWORKS (tm) by Greg Kusnick
// 
// t.js is provided by Therkyn of Morningthaw, who just couldnt let go of
// it from ACScript.  These messages are intended to be summary in nature.
// So if sometimes you'd like to see more info, go ahead and change the text
// to see what you'd like to see.
// 
// This is a VERY noisy script.  That can be solved by commenting out the
// output lines in the event that's causing the noise.
// 
// Some have been commented out by default.
// 
// Good Luck!
// therkyn
// therkyn@hotmail.com

// -------------------------------------------------------------------
// !!!!!!!!!!!!!!!!!!!!!!   R E A D   T H I S   !!!!!!!!!!!!!!!!!!!!!!
// -------------------------------------------------------------------
// !!!     This file is generated automatically by Events.xsl.     !!!
// !!!    Any edits you make to it by hand WILL be overwritten.    !!!
// !!!          Make your changes to Events.xml instead.           !!!
// -------------------------------------------------------------------

// Set up your handlers variable as a global variable
var h = new myHandlers();

// Shortcut function for outputting to Screen "sSay = Screen Say"
function sSay(text)
    {
    skapi.OutputLine(text,opmChatWnd);
    }

function MyHandlers_OnActionComplete()
    {
    skapi.OutputLine("OnActionComplete(): Previous action complete", opmChatWnd);
    }

function MyHandlers_OnAddToInventory(aco)
    {
    skapi.OutputLine("OnAddToInventory(aco): An item (" + aco.szName + ") with oid (" + aco.oid + ") was added to your inventory.", opmChatWnd);
    skapi.OutputLine("OnAddToInventory(aco): It is in pack (" + aco.acoContainer.iitem + ") in slot (" + aco.iitem + ").  Its equipment", opmChatWnd);
    skapi.OutputLine("OnAddToInventory(aco): mask is (" + aco.eqm + ") and is worth (" + aco.cpyValue + "). ", opmChatWnd);
    }

function MyHandlers_OnAdjustStack(aco, citemPrev)
    {
    skapi.OutputLine("OnAdjustStack(aco, citemPrev): An item (" + aco.szName + ") with oid (" + aco.oid + ") stack was adjusted to (" + aco.citemStack, opmChatWnd);
    }

function MyHandlers_OnAnimationSelf(dwA, dwB, dwC, dwD, dwE)
    {
//  Commented out, every time you move, this fires.
//  skapi.OutputLine("OnAnimationSelf(dwA, dwB, dwC, dwD, dwE): " + dwA + ", " + dwB + ", "+ dwC + ", " + dwD + ", " + dwE, opmChatWnd);
    }

function MyHandlers_OnApproachVendor(acoMerchant, mcmBuy, cpyMaxBuy, fractBuy, fractSell, coaco)
    {
    skapi.OutputLine("OnApproachVendor(acoMerchant, mcmBuy, cpyMaxBuy, fractBuy, fractSell, coaco): Merchant: " + acoMerchant.szName + ",+ will buy in category mask: " + mcmBuy, opmChatWnd);
    skapi.OutputLine("OAV: and will not buy items over " + cpyMaxBuy + ", Buys at:" + fractBuy + ", Sells at:"+ fractSell + " and there's a collection of objects for sale.", opmChatWnd);
    }

function MyHandlers_OnAssessCreature(aco, fSuccess, cbi, cai, chi)
    {
    skapi.OutputLine("OnAssessCreature(aco, fSuccess, cbi, cai, chi): Item OID= " + aco.oid + ", Success = " + fSuccess, opmChatWnd);
    if (cbi != null)
        {	var dncbi = new Array(cbi.lvl, cbi.healthCur, cbi.healthMax);
        var text = "";
        for (var x=0;x<dncbi.length;x++)
            {text = text + dncbi[x] +", "}
        skapi.OutputLine("OAC (CAI): " + text, opmChatWnd);
    //  {skapi.OutputLine("OAC (CAI): " + dncbi[x], opmChatWnd)}
        }
    if (cai)
        {	var dncai = new Array(
        cai.strength, cai.endurance, cai.quickness, cai.coordination,
        cai.focus, cai.self, cai.staminaCur, cai.manaCur, cai.manaMax);
        var text = "";
        for (var x=0;x<dncai.length;x++)
            {text = text + dncai[x] +", "}
        skapi.OutputLine("OAC (CAI): " + text, opmChatWnd);
        }
    if (chi)
        {	var dnchi = new Array(
        chi.rank, chi.cfollower, chi.leadership, chi.fPK, chi.szGender,
        chi.szRace, chi.szProfession, chi.szFellowship, chi.szMonarch, chi.szPatron);
        var text = "";
        for (var x=0;x<dnchi.length;x++)
            {text = text + dnchi[x] +", "}
        skapi.OutputLine("OAC (CHI): " + text, opmChatWnd);
        }
    }

function MyHandlers_OnAssessItem(aco, fSuccess, ibi, iai, iwi, iei, ipi)
    {
    skapi.OutputLine("OnAssessItem(aco, fSuccess, ibi, iai, iwi, iei, ipi): Item OID= " + aco.oid + ", Success = " + fSuccess, opmChatWnd);
    if (ibi)
        {	var dnibi = new Array(
        ibi.oty, ibi.cpyValue, ibi.burden, ibi.fOpen, ibi.cpageTotal, ibi.cpageUsed,
        ibi.szInscription, ibi.szInscriber, ibi.szUnknown, ibi.szDescDetailed,
        ibi.szDescSimple, ibi.szComment, ibi.fractEfficiency, ibi.manaCur);
        var text = "";
        for (var x=0;x<dnibi.length;x++)
            {text = text + dnibi[x] +", "}
        skapi.OutputLine("OAI (IBI): " + text, opmChatWnd);
        }
    if (iai)
        {	var dniai = new Array(
        iai.protSlashing, iai.protPiercing, iai.protBludgeoning, iai.protCold,
        iai.protFire, iai.protAcid, iai.protElectrical, iai.al);
        var text = "";
        for (var x=0;x<dniai.length;x++)
            {text = text + dniai[x] +", "}
        skapi.OutputLine("OAI (IAI): " + text, opmChatWnd);
        }
    if (iwi)
        {	var dniwi = new Array(
        iwi.dmty, iwi.speed, iwi.skid, iwi.dhealth, iwi.scaleDamageRange,
        iwi.scaleDamageBonus, iwi.fractUnknown, iwi.scaleDefenseBonus,
        iwi.scaleAttackBonus, iwi.dwHighlights);
        var text = "";
        for (var x=0;x<dniwi.length;x++)
            {text = text + dniwi[x] +", "}
        skapi.OutputLine("OAI (IWI): " + text, opmChatWnd);
        }
    if (iei)
        {	var dniei = new Array(
        iei.difficulty, iei.spellcraft, iei.rankReq, iei.manaCur, iei.manaMax,
        iei.dwUnknown, iei.csecPerMana, iei.szRaceReq, iei.sklvlReq, iei.skidReq,
        iei.szSpellNames, iei.szSpellDesc, iei.fractEfficiency);
        var text = "";
        for (var x=0;x<dniei.length;x++)
            {text = text + dniei[x] +", "}
        skapi.OutputLine("OAI (IEI): " + text, opmChatWnd);
        }
    if (ipi)
        {	var dnipi = new Array(ipi.lvlMin, ipi.lvlMax, ipi.szDest);
        skapi.OutputLine("OAI (IPI): " + dnipi[0] + ", " + dnipi[1] + ", " + dnipi[2], opmChatWnd);
        }
    }

function MyHandlers_OnChatBoxClick(aco)
    {
    skapi.OutputLine("OnChatBoxClick(aco): clicked on " + aco.szName + " (" + aco.oid.toString(16) + ")", opmChatWnd);
    }

function MyHandlers_OnChatBoxMessage(szMsg, cmc)
    {
    skapi.OutputLine("OnChatBoxMessage(szMsg, cmc): (" + szMsg + ") in colorcode: " + cmc, opmChatWnd);
    }

function MyHandlers_OnChatBroadcast(acoSender, szMsg, cmc)
    {
    skapi.OutputLine("OnChatBroadcast(acoSender, szMsg, cmc): Msg from " + acoSender.szName + ", who says (" + szMsg + ") in colorcode: " + cmc, opmChatWnd);
    }

function MyHandlers_OnChatEmoteCustom(acoSender, szMsg)
    {
    skapi.OutputLine("OnChatEmoteCustom(acoSender, szMsg): Msg from " + acoSender.szName + ", who says (" + szMsg + ").", opmChatWnd);
    }

function MyHandlers_OnChatEmoteStandard(acoSender, szMsg)
    {
    skapi.OutputLine("OnChatEmoteStandard(acoSender, szMsg): Msg from " + acoSender.szName + ", who says (" + szMsg + ").", opmChatWnd);
    }

function MyHandlers_OnChatLocal(acoSender, szMsg)
    {
    skapi.OutputLine("OnChatLocal(acoSender, szMsg): Msg from " + acoSender.szName + ", who says (" + szMsg + ").", opmChatWnd);
    }

function MyHandlers_OnChatServer(szMsg, cmc)
    {
    skapi.OutputLine("OnChatServer(szMsg, cmc): Server Message: (" + szMsg + "), in color code " + cmc, opmChatWnd);
    }

function MyHandlers_OnChatSpell(acoSender, szMsg)
    {
    skapi.OutputLine("OnChatSpell(acoSender, szMsg): Msg from " + acoSender.szName + ", who says (" + szMsg + ").", opmChatWnd);
    }

function MyHandlers_OnCloseContainer(acoContainer)
    {
    skapi.OutputLine("OnCloseContainer(acoContainer): Container: " + acoContainer.szName, opmChatWnd);
    }

function MyHandlers_OnCombatMode(fOn)
    {
    skapi.OutputLine("OnCombatMode(fOn): " + fOn, opmChatWnd);
    }

function MyHandlers_OnCommand(szCmd)
    {
    skapi.OutputLine("OnCommand(szCmd): Someone has entered this command in the command box: " + szCmd, opmChatWnd);
    }

function MyHandlers_OnControlEvent(szPanel, szControl, dictSzValue)
    {
    skapi.OutputLine("OnControlEvent(szPanel, szControl, dictSzValue): " + szPanel + ", " + szControl + ", " + dictSzValue, opmChatWnd);
    }

function MyHandlers_OnControlProperty(szPanel, szControl, szProperty, vValue)
    {
    skapi.OutputLine("OnControlProperty(" + szPanel + ", " + szControl + ", " + szProperty + "): " + vValue, opmChatWnd);
    }

function MyHandlers_OnCraftConfirm(szOdds)
    {
    skapi.OutputLine("OnCraftConfirm(): " + szOdds + "% chance of success.", opmChatWnd);
    }

function MyHandlers_OnDeathMessage(szMsg)
    {
    skapi.OutputLine("OnDeathMessage(szMsg): " + szMsg, opmChatWnd);
    }

function MyHandlers_OnDeathOther(acoVictim, acoKiller, szMsg)
    {
    skapi.OutputLine("OnDeathOther(acoVictim, acoKiller, szMsg): " + acoVictim + ", " + acoKiller + ", " + szMsg, opmChatWnd);
    }

function MyHandlers_OnDeathSelf(acoKiller, szMsg)
    {
    skapi.OutputLine("OnDeathSelf(acoKiller, szMsg): " + acoKiller + ", " + szMsg, opmChatWnd);
    }

function MyHandlers_OnDisconnect()
    {
    skapi.OutputLine("OnDisconnect(): Server connection lost", opmChatWnd);
    }

function MyHandlers_OnEnd3D()
    {
    skapi.OutputLine("OnEnd3D(): 3D Ended I guess", opmChatWnd);
    }

function MyHandlers_OnEndPortalOther(aco)
    {
    skapi.OutputLine("OnEndPortalOther(aco): Portal has ended for " + aco.szName, opmChatWnd);
    }

function MyHandlers_OnEndPortalSelf()
    {
    skapi.OutputLine("OnEndPortalSelf(): Portal has ended", opmChatWnd);
    }

function MyHandlers_OnFellowCreate(szName, fShareExp, oidLeader, cofellow)
    {
    skapi.OutputLine("OnFellowCreate(): Fellowship " + szName + " has been created.", opmChatWnd);
    }

function MyHandlers_OnFellowDisband()
    {
    skapi.OutputLine("OnFellowDisband(): Fellowship has been disbanded.", opmChatWnd);
    }

function MyHandlers_OnFellowDismiss(oid)
    {
    skapi.OutputLine("OnFellowDismiss(): Player " + oid.toString(16) + " has been dismissed.", opmChatWnd);
    }

function MyHandlers_OnFellowInvite(szSender)
    {
    skapi.OutputLine("OnFellowInvite(): " + szSender + " asks you to join.", opmChatWnd);
    }

function MyHandlers_OnFellowQuit(oid)
    {
    skapi.OutputLine("OnFellowQuit(): Player " + oid.toString(16) + " has quit.", opmChatWnd);
    }

function MyHandlers_OnFellowRecruit(fellow)
    {
    skapi.OutputLine("OnFellowRecruit(): " + fellow.szName + " has been recruited.", opmChatWnd);
    }

function MyHandlers_OnFellowUpdate(fellow)
    {
    skapi.OutputLine("OnFellowUpdate(): New stats for " + fellow.szName + ".", opmChatWnd);
    }

function MyHandlers_OnHotkey(cmid, vk)
    {
    skapi.OutputLine("OnHotkey(\"" + cmid + "\", " + vk.toString() + ")", opmChatWnd);
    }

function MyHandlers_OnItemManaBar(aco, fractMana)
    {
    skapi.OutputLine("OnItemManaBar(aco, fractMana): ManaBar for " + aco.szName + " has been updated to " + fractMana, opmChatWnd);
    }

function MyHandlers_OnLeaveVendor(acoMerchant)
    {
    skapi.OutputLine("OnLeaveVendor(acoMerchant): Merchant: " + acoMerchant.szName, opmChatWnd);
    }

function MyHandlers_OnLocChangeCreature(aco)
    {
//  Commented out for your own good!  Way too many things move...
//  skapi.OutputLine("OnLocChangeCreature(aco): " + aco.szName + " has moved.", opmChatWnd);
    }

function MyHandlers_OnLocChangeOther(aco)
    {
//  Commented out for your own good!  Way too many things move...
//  skapi.OutputLine("OnLocChangeOther(aco): " + aco.szName + " has moved.", opmChatWnd);
    }

function MyHandlers_OnLocChangeSelf(maploc)
    {
//  Every time you move...
//  sSay("OnLocChangeSelf: Land Block: " + maploc.landblock + ", x:" + maploc.x + ", y:" + maploc.y + ", z:" + maploc.z + ", heading:" + maploc.head);
    }

function MyHandlers_OnLogon(acoChar)
    {
    sSay("OnLogon: OID:" + acoChar.oid + ", Name:" + acoChar.szName);
    }

function MyHandlers_OnMeleeDamageOther(szName, dhealth, dmty)
    {
    sSay("OnMeleeDamageOther: Attacked:" + szName + ", Damage Done:" + dhealth + ", Damage Type:" + dmty);
    }

function MyHandlers_OnMeleeDamageSelf(szName, dhealth, dmty, bpart)
    {
    sSay("OnMeleeDamageSelf: Attacker:" + szName + ", Damage Done:" + dhealth + ", Damage Type:" + dmty + ", Body Part:" + bpart);
    }

function MyHandlers_OnMeleeEvadeOther(szName)
    {
    sSay("OnMeleeEvadeOther: " + szName + " evaded you.");
    }

function MyHandlers_OnMeleeEvadeSelf(szName)
    {
    sSay("OnMeleeEvadeSelf: You evaded " + szName);
    }

function MyHandlers_OnMeleeLastAttacker(aco)
    {
    sSay("OnMeleeLastAttacker: You were last attacked by " + aco.szName);
    }

function MyHandlers_OnMoveItem(aco, acoContainer, iitem)
    {
    sSay("OnMoveItem: " + aco.szName + " (" + aco.oid + ") was just moved around in your inventory.");
    }

function MyHandlers_OnMyPlayerKill(aco, szMsg)
    {
    sSay("OnMyPlayerKill: oid:" + aco.oid + ", Death Message:" + szMsg);
    }

function MyHandlers_OnNavStop(nsc)
    {
    sSay("OnNavStop: nsc: " + nsc);
    }

function MyHandlers_OnObjectCreate(aco)
    {
    sSay("OnObjectCreate: " + aco.szName + " with OID " + aco.oid + " just appeared with the following bit-mask: " + aco.oty);
    }

function MyHandlers_OnObjectCreatePlayer(aco)
    {
    sSay("OnObjectCreatePlayer: " + aco.szName + " with OID " + aco.oid + " just appeared.");
    }

function MyHandlers_OnObjectDestroy(aco)
    {
    sSay("OnObjectDestroy: " + aco.szName + " with OID " + aco.oid + " and bit-mask " + aco.oty + " is being destroyed.");
    }

function MyHandlers_OnOpenContainer(aco)
    {
    sSay("OnOpenContainer: You just opened a " + aco.szName + " with OID " + aco.oid + " that has " + aco.citemContents + " inside.");
    }

function MyHandlers_OnOpenContainerPanel(acoContainer)
    {
    skapi.OutputLine("OnOpenContainerPanel(acoContainer): Container: " + acoContainer.szName, opmChatWnd);
    }

function MyHandlers_OnPluginMsg(szMsg)
    {
    sSay("OnPluginMsg: \"" + szMsg + "\"");
    }

function MyHandlers_OnPortalStormed()
    {
    sSay("OnPortalStormed: You've just been portal stormed.");
    }

function MyHandlers_OnPortalStormWarning(severity)
    {
    sSay("OnPortalStormWarning: You've just received a lvl " + severity + " warning.");
    }

function MyHandlers_OnRemoveFromInventory(aco, acoContainer, iitem)
    {
    sSay("OnRemoveFromInventory: " + aco.szName + " (" + aco.oid + ") was just removed from your inventory.");
    }

function MyHandlers_OnSetFlagOther(aco, iflag, fOn)
    {
    sSay("OnSetFlagOther: " + aco.szName + " just went PK (" + fOn + ").");
    }

function MyHandlers_OnSetFlagSelf(iflag, fOn)
    {
    sSay("OnSetFlagSelf: You just went PK (" + fOn + ").");
    }

function MyHandlers_OnSpellAdd(spell)
    {
    sSay("OnSpellAdd: " + spell.acoCaster + " just cast spell " + spell.spellid + ".");
    }

function MyHandlers_OnSpellCastSelf(spellid)
    {
    sSay("OnSpellCastSelf: You just cast spell " + spellid + ".");
    }

function MyHandlers_OnSpellExpire(spellid, layer)
    {
    sSay("OnSpellExpire: Spell " + spellid + " in layer " + layer + " just expired.");
    }

function MyHandlers_OnSpellExpireSilent(spellid, layer)
    {
    sSay("OnSpellExpireSilent: Spell " + spellid + " in layer " + layer + " just expired silently.");
    }

function MyHandlers_OnSpellFailSelf(arc)
    {
    sSay("OnSpellFailSelf: You failed to cast for reason # " + arc);
    }

function MyHandlers_OnStart3D()
    {
    sSay("OnStart3D: 3D started");
    }

function MyHandlers_OnStartPortalSelf()
    {
    sSay("OnStartPortalSelf: Portal Started");
    }

function MyHandlers_OnStatLevel(lvl)
    {
    sSay("OnStatLevel: You've leveled!  Your new level is level " + lvl);
    }

function MyHandlers_OnStatSkillExp(skid, szSkill, diff, expTotal, dsklvl)
    {
    sSay("OnStatSkillExp: " + szSkill + " (" + skid + ") just went up on a " + diff + " difficulty task.  You're up " + dsklvl + " levels in that skill.");
    }

function MyHandlers_OnStatTotalBurden(burden)
    {
    sSay("OnStatTotalBurden: Your new burden is " + burden);
    }

function MyHandlers_OnStatTotalExp(exp)
    {
    sSay("OnStatTotalExp: Your new xp total is " + exp);
    }

function MyHandlers_OnStatTotalPyreals(cpy)
    {
    sSay("OnStatTotalPyreals: Your new Pyreal inventory is " + cpy);
    }

function MyHandlers_OnStatUnspentExp(exp)
    {
    sSay("OnStatUnspentExp: Your new unspent xp total is " + exp);
    }

function MyHandlers_OnTargetHealth(aco, fractHealth)
    {
//  Gives your health if nothing targeted...
//  sSay("OnTargetHealth: " + aco.szName + "(" + aco.oid + ") has health of " + fractHealth);
    }

function MyHandlers_OnTell(acoSender, acoReceiver, szMsg)
    {
    sSay("OnTell: " + acoSender.szName + " told " + acoReceiver.szName + ": " + szMsg);
    }

function MyHandlers_OnTellAllegiance(szSender, szMsg)
    {
    sSay("OnTellAllegiance: " + szSender + " says " + szMsg);
    }

function MyHandlers_OnTellCovassal(szSender, szMsg)
    {
    sSay("OnTellCovassal: " + szSender + " says " + szMsg);
    }

function MyHandlers_OnTellFellowship(szSender, szMsg)
    {
    sSay("OnTellFellowship: " + szSender + " says " + szMsg);
    }

function MyHandlers_OnTellFollower(szSender, szMsg)
    {
    sSay("OnTellFollower: " + szSender + " says " + szMsg);
    }

function MyHandlers_OnTellMelee(acoSender, acoReceiver, szMsg)
    {
    sSay("OnTellMelee: " + acoSender.szName + " told " + acoReceiver.szName + ": " + szMsg);
    }

function MyHandlers_OnTellMisc(acoSender, acoReceiver, szMsg, cmc)
    {
    sSay("OnTellMisc: " + acoSender.szName + " told " + acoReceiver.szName + ": " + szMsg);
    }

function MyHandlers_OnTellPatron(szSender, szMsg)
    {
    sSay("OnTellPatron: " + szSender + " says " + szMsg);
    }

function MyHandlers_OnTellServer(acoSender, acoReceiver, szMsg)
    {
    sSay("OnTellServer: " + acoSender.szName + " told " + acoReceiver.szName + ": " + szMsg);
    }

function MyHandlers_OnTellVassal(szSender, szMsg)
    {
    sSay("OnTellVassal: " + szSender + " says " + szMsg);
    }

function MyHandlers_OnTimer(timer)
    {
    sSay("OnTimer");
    }

function MyHandlers_OnTipMessage(szMsg)
    {
    skapi.OutputLine("OnTipMessage(szMsg): (" + szMsg + ")", opmChatWnd);
    }

function MyHandlers_OnTradeAccept(aco)
    {
    sSay("OnTradeAccept: " + aco.szName + " has accepted the trade.");
    }

function MyHandlers_OnTradeAdd(aco, side)
    {
    sSay("OnTradeAdd: " + aco.szName + " has been added to the " + side + " side of the trade window.");
    }

function MyHandlers_OnTradeEnd(arc)
    {
    sSay("OnTradeEnd: The trade has ended with reason code " + arc);
    }

function MyHandlers_OnTradeReset(aco)
    {
    sSay("OnTradeReset: " + aco.szName + " has reset the trade window.");
    }

function MyHandlers_OnTradeStart(acoSender, acoReceiver)
    {
    sSay("OnTradeStart: A trade window has been opened between " + acoSender.szName + " and " + aco.Receiver.szName);
    }

function MyHandlers_OnVitals(health, stamina, mana)
    {
    sSay("OnVitals: Health:" + health + ", Stamina:" + stamina + ", Mana:" + mana);
    }

function myHandlers()
    {
    this.OnActionComplete = MyHandlers_OnActionComplete;
    this.OnAddToInventory = MyHandlers_OnAddToInventory;
    this.OnAdjustStack = MyHandlers_OnAdjustStack;
    this.OnAnimationSelf = MyHandlers_OnAnimationSelf;
    this.OnApproachVendor = MyHandlers_OnApproachVendor;
    this.OnAssessCreature = MyHandlers_OnAssessCreature;
    this.OnAssessItem = MyHandlers_OnAssessItem;
    this.OnChatBoxClick = MyHandlers_OnChatBoxClick;
    this.OnChatBoxMessage = MyHandlers_OnChatBoxMessage;
    this.OnChatBroadcast = MyHandlers_OnChatBroadcast;
    this.OnChatEmoteCustom = MyHandlers_OnChatEmoteCustom;
    this.OnChatEmoteStandard = MyHandlers_OnChatEmoteStandard;
    this.OnChatLocal = MyHandlers_OnChatLocal;
    this.OnChatServer = MyHandlers_OnChatServer;
    this.OnChatSpell = MyHandlers_OnChatSpell;
    this.OnCloseContainer = MyHandlers_OnCloseContainer;
    this.OnCombatMode = MyHandlers_OnCombatMode;
    this.OnCommand = MyHandlers_OnCommand;
    this.OnControlEvent = MyHandlers_OnControlEvent;
    this.OnControlProperty = MyHandlers_OnControlProperty;
    this.OnCraftConfirm = MyHandlers_OnCraftConfirm;
    this.OnDeathMessage = MyHandlers_OnDeathMessage;
    this.OnDeathOther = MyHandlers_OnDeathOther;
    this.OnDeathSelf = MyHandlers_OnDeathSelf;
    this.OnDisconnect = MyHandlers_OnDisconnect;
    this.OnEnd3D = MyHandlers_OnEnd3D;
    this.OnEndPortalOther = MyHandlers_OnEndPortalOther;
    this.OnEndPortalSelf = MyHandlers_OnEndPortalSelf;
    this.OnFellowCreate = MyHandlers_OnFellowCreate;
    this.OnFellowDisband = MyHandlers_OnFellowDisband;
    this.OnFellowDismiss = MyHandlers_OnFellowDismiss;
    this.OnFellowInvite = MyHandlers_OnFellowInvite;
    this.OnFellowQuit = MyHandlers_OnFellowQuit;
    this.OnFellowRecruit = MyHandlers_OnFellowRecruit;
    this.OnFellowUpdate = MyHandlers_OnFellowUpdate;
    this.OnHotkey = MyHandlers_OnHotkey;
    this.OnItemManaBar = MyHandlers_OnItemManaBar;
    this.OnLeaveVendor = MyHandlers_OnLeaveVendor;
    this.OnLocChangeCreature = MyHandlers_OnLocChangeCreature;
    this.OnLocChangeOther = MyHandlers_OnLocChangeOther;
    this.OnLocChangeSelf = MyHandlers_OnLocChangeSelf;
    this.OnLogon = MyHandlers_OnLogon;
    this.OnMeleeDamageOther = MyHandlers_OnMeleeDamageOther;
    this.OnMeleeDamageSelf = MyHandlers_OnMeleeDamageSelf;
    this.OnMeleeEvadeOther = MyHandlers_OnMeleeEvadeOther;
    this.OnMeleeEvadeSelf = MyHandlers_OnMeleeEvadeSelf;
    this.OnMeleeLastAttacker = MyHandlers_OnMeleeLastAttacker;
    this.OnMoveItem = MyHandlers_OnMoveItem;
    this.OnMyPlayerKill = MyHandlers_OnMyPlayerKill;
    this.OnNavStop = MyHandlers_OnNavStop;
    this.OnObjectCreate = MyHandlers_OnObjectCreate;
    this.OnObjectCreatePlayer = MyHandlers_OnObjectCreatePlayer;
    this.OnObjectDestroy = MyHandlers_OnObjectDestroy;
    this.OnOpenContainer = MyHandlers_OnOpenContainer;
    this.OnOpenContainerPanel = MyHandlers_OnOpenContainerPanel;
    this.OnPluginMsg = MyHandlers_OnPluginMsg;
    this.OnPortalStormed = MyHandlers_OnPortalStormed;
    this.OnPortalStormWarning = MyHandlers_OnPortalStormWarning;
    this.OnRemoveFromInventory = MyHandlers_OnRemoveFromInventory;
    this.OnSetFlagOther = MyHandlers_OnSetFlagOther;
    this.OnSetFlagSelf = MyHandlers_OnSetFlagSelf;
    this.OnSpellAdd = MyHandlers_OnSpellAdd;
    this.OnSpellCastSelf = MyHandlers_OnSpellCastSelf;
    this.OnSpellExpire = MyHandlers_OnSpellExpire;
    this.OnSpellExpireSilent = MyHandlers_OnSpellExpireSilent;
    this.OnSpellFailSelf = MyHandlers_OnSpellFailSelf;
    this.OnStart3D = MyHandlers_OnStart3D;
    this.OnStartPortalSelf = MyHandlers_OnStartPortalSelf;
    this.OnStatLevel = MyHandlers_OnStatLevel;
    this.OnStatSkillExp = MyHandlers_OnStatSkillExp;
    this.OnStatTotalBurden = MyHandlers_OnStatTotalBurden;
    this.OnStatTotalExp = MyHandlers_OnStatTotalExp;
    this.OnStatTotalPyreals = MyHandlers_OnStatTotalPyreals;
    this.OnStatUnspentExp = MyHandlers_OnStatUnspentExp;
    this.OnTargetHealth = MyHandlers_OnTargetHealth;
    this.OnTell = MyHandlers_OnTell;
    this.OnTellAllegiance = MyHandlers_OnTellAllegiance;
    this.OnTellCovassal = MyHandlers_OnTellCovassal;
    this.OnTellFellowship = MyHandlers_OnTellFellowship;
    this.OnTellFollower = MyHandlers_OnTellFollower;
    this.OnTellMelee = MyHandlers_OnTellMelee;
    this.OnTellMisc = MyHandlers_OnTellMisc;
    this.OnTellPatron = MyHandlers_OnTellPatron;
    this.OnTellServer = MyHandlers_OnTellServer;
    this.OnTellVassal = MyHandlers_OnTellVassal;
    this.OnTimer = MyHandlers_OnTimer;
    this.OnTipMessage = MyHandlers_OnTipMessage;
    this.OnTradeAccept = MyHandlers_OnTradeAccept;
    this.OnTradeAdd = MyHandlers_OnTradeAdd;
    this.OnTradeEnd = MyHandlers_OnTradeEnd;
    this.OnTradeReset = MyHandlers_OnTradeReset;
    this.OnTradeStart = MyHandlers_OnTradeStart;
    this.OnVitals = MyHandlers_OnVitals;
    }

function fAddHandlers()
    {
    skapi.AddHandler(evidOnActionComplete, h);
    skapi.AddHandler(evidOnAddToInventory, h);
    skapi.AddHandler(evidOnAdjustStack, h);
    skapi.AddHandler(evidOnAnimationSelf, h);
    skapi.AddHandler(evidOnApproachVendor, h);
    skapi.AddHandler(evidOnAssessCreature, h);
    skapi.AddHandler(evidOnAssessItem, h);
    skapi.AddHandler(evidOnChatBoxClick, h);
    skapi.AddHandler(evidOnChatBoxMessage, h);
    skapi.AddHandler(evidOnChatBroadcast, h);
    skapi.AddHandler(evidOnChatEmoteCustom, h);
    skapi.AddHandler(evidOnChatEmoteStandard, h);
    skapi.AddHandler(evidOnChatLocal, h);
    skapi.AddHandler(evidOnChatServer, h);
    skapi.AddHandler(evidOnChatSpell, h);
    skapi.AddHandler(evidOnCloseContainer, h);
    skapi.AddHandler(evidOnCombatMode, h);
    skapi.AddHandler(evidOnCommand, h);
    skapi.AddHandler(evidOnControlEvent, h);
    skapi.AddHandler(evidOnControlProperty, h);
    skapi.AddHandler(evidOnCraftConfirm, h);
    skapi.AddHandler(evidOnDeathMessage, h);
    skapi.AddHandler(evidOnDeathOther, h);
    skapi.AddHandler(evidOnDeathSelf, h);
    skapi.AddHandler(evidOnDisconnect, h);
    skapi.AddHandler(evidOnEnd3D, h);
    skapi.AddHandler(evidOnEndPortalOther, h);
    skapi.AddHandler(evidOnEndPortalSelf, h);
    skapi.AddHandler(evidOnFellowCreate, h);
    skapi.AddHandler(evidOnFellowDisband, h);
    skapi.AddHandler(evidOnFellowDismiss, h);
    skapi.AddHandler(evidOnFellowInvite, h);
    skapi.AddHandler(evidOnFellowQuit, h);
    skapi.AddHandler(evidOnFellowRecruit, h);
    skapi.AddHandler(evidOnFellowUpdate, h);
    skapi.AddHandler(evidOnHotkey, h);
    skapi.AddHandler(evidOnItemManaBar, h);
    skapi.AddHandler(evidOnLeaveVendor, h);
    skapi.AddHandler(evidOnLocChangeCreature, h);
    skapi.AddHandler(evidOnLocChangeOther, h);
    skapi.AddHandler(evidOnLocChangeSelf, h);
    skapi.AddHandler(evidOnLogon, h);
    skapi.AddHandler(evidOnMeleeDamageOther, h);
    skapi.AddHandler(evidOnMeleeDamageSelf, h);
    skapi.AddHandler(evidOnMeleeEvadeOther, h);
    skapi.AddHandler(evidOnMeleeEvadeSelf, h);
    skapi.AddHandler(evidOnMeleeLastAttacker, h);
    skapi.AddHandler(evidOnMoveItem, h);
    skapi.AddHandler(evidOnMyPlayerKill, h);
    skapi.AddHandler(evidOnNavStop, h);
    skapi.AddHandler(evidOnObjectCreate, h);
    skapi.AddHandler(evidOnObjectCreatePlayer, h);
    skapi.AddHandler(evidOnObjectDestroy, h);
    skapi.AddHandler(evidOnOpenContainer, h);
    skapi.AddHandler(evidOnOpenContainerPanel, h);
    skapi.AddHandler(evidOnPluginMsg, h);
    skapi.AddHandler(evidOnPortalStormed, h);
    skapi.AddHandler(evidOnPortalStormWarning, h);
    skapi.AddHandler(evidOnRemoveFromInventory, h);
    skapi.AddHandler(evidOnSetFlagOther, h);
    skapi.AddHandler(evidOnSetFlagSelf, h);
    skapi.AddHandler(evidOnSpellAdd, h);
    skapi.AddHandler(evidOnSpellCastSelf, h);
    skapi.AddHandler(evidOnSpellExpire, h);
    skapi.AddHandler(evidOnSpellExpireSilent, h);
    skapi.AddHandler(evidOnSpellFailSelf, h);
    skapi.AddHandler(evidOnStart3D, h);
    skapi.AddHandler(evidOnStartPortalSelf, h);
    skapi.AddHandler(evidOnStatLevel, h);
    skapi.AddHandler(evidOnStatSkillExp, h);
    skapi.AddHandler(evidOnStatTotalBurden, h);
    skapi.AddHandler(evidOnStatTotalExp, h);
    skapi.AddHandler(evidOnStatTotalPyreals, h);
    skapi.AddHandler(evidOnStatUnspentExp, h);
    skapi.AddHandler(evidOnTargetHealth, h);
    skapi.AddHandler(evidOnTell, h);
    skapi.AddHandler(evidOnTellAllegiance, h);
    skapi.AddHandler(evidOnTellCovassal, h);
    skapi.AddHandler(evidOnTellFellowship, h);
    skapi.AddHandler(evidOnTellFollower, h);
    skapi.AddHandler(evidOnTellMelee, h);
    skapi.AddHandler(evidOnTellMisc, h);
    skapi.AddHandler(evidOnTellPatron, h);
    skapi.AddHandler(evidOnTellServer, h);
    skapi.AddHandler(evidOnTellVassal, h);
    skapi.AddHandler(evidOnTimer, h);
    skapi.AddHandler(evidOnTipMessage, h);
    skapi.AddHandler(evidOnTradeAccept, h);
    skapi.AddHandler(evidOnTradeAdd, h);
    skapi.AddHandler(evidOnTradeEnd, h);
    skapi.AddHandler(evidOnTradeReset, h);
    skapi.AddHandler(evidOnTradeStart, h);
    skapi.AddHandler(evidOnVitals, h);
    }

function fRemoveHandlers()
    {
    skapi.RemoveHandler(evidOnActionComplete, h);
    skapi.RemoveHandler(evidOnAddToInventory, h);
    skapi.RemoveHandler(evidOnAdjustStack, h);
    skapi.RemoveHandler(evidOnAnimationSelf, h);
    skapi.RemoveHandler(evidOnApproachVendor, h);
    skapi.RemoveHandler(evidOnAssessCreature, h);
    skapi.RemoveHandler(evidOnAssessItem, h);
    skapi.RemoveHandler(evidOnChatBoxClick, h);
    skapi.RemoveHandler(evidOnChatBoxMessage, h);
    skapi.RemoveHandler(evidOnChatBroadcast, h);
    skapi.RemoveHandler(evidOnChatEmoteCustom, h);
    skapi.RemoveHandler(evidOnChatEmoteStandard, h);
    skapi.RemoveHandler(evidOnChatLocal, h);
    skapi.RemoveHandler(evidOnChatServer, h);
    skapi.RemoveHandler(evidOnChatSpell, h);
    skapi.RemoveHandler(evidOnCloseContainer, h);
    skapi.RemoveHandler(evidOnCombatMode, h);
    skapi.RemoveHandler(evidOnCommand, h);
    skapi.RemoveHandler(evidOnControlEvent, h);
    skapi.RemoveHandler(evidOnControlProperty, h);
    skapi.RemoveHandler(evidOnCraftConfirm, h);
    skapi.RemoveHandler(evidOnDeathMessage, h);
    skapi.RemoveHandler(evidOnDeathOther, h);
    skapi.RemoveHandler(evidOnDeathSelf, h);
    skapi.RemoveHandler(evidOnDisconnect, h);
    skapi.RemoveHandler(evidOnEnd3D, h);
    skapi.RemoveHandler(evidOnEndPortalOther, h);
    skapi.RemoveHandler(evidOnEndPortalSelf, h);
    skapi.RemoveHandler(evidOnFellowCreate, h);
    skapi.RemoveHandler(evidOnFellowDisband, h);
    skapi.RemoveHandler(evidOnFellowDismiss, h);
    skapi.RemoveHandler(evidOnFellowInvite, h);
    skapi.RemoveHandler(evidOnFellowQuit, h);
    skapi.RemoveHandler(evidOnFellowRecruit, h);
    skapi.RemoveHandler(evidOnFellowUpdate, h);
    skapi.RemoveHandler(evidOnHotkey, h);
    skapi.RemoveHandler(evidOnItemManaBar, h);
    skapi.RemoveHandler(evidOnLeaveVendor, h);
    skapi.RemoveHandler(evidOnLocChangeCreature, h);
    skapi.RemoveHandler(evidOnLocChangeOther, h);
    skapi.RemoveHandler(evidOnLocChangeSelf, h);
    skapi.RemoveHandler(evidOnLogon, h);
    skapi.RemoveHandler(evidOnMeleeDamageOther, h);
    skapi.RemoveHandler(evidOnMeleeDamageSelf, h);
    skapi.RemoveHandler(evidOnMeleeEvadeOther, h);
    skapi.RemoveHandler(evidOnMeleeEvadeSelf, h);
    skapi.RemoveHandler(evidOnMeleeLastAttacker, h);
    skapi.RemoveHandler(evidOnMoveItem, h);
    skapi.RemoveHandler(evidOnMyPlayerKill, h);
    skapi.RemoveHandler(evidOnNavStop, h);
    skapi.RemoveHandler(evidOnObjectCreate, h);
    skapi.RemoveHandler(evidOnObjectCreatePlayer, h);
    skapi.RemoveHandler(evidOnObjectDestroy, h);
    skapi.RemoveHandler(evidOnOpenContainer, h);
    skapi.RemoveHandler(evidOnOpenContainerPanel, h);
    skapi.RemoveHandler(evidOnPluginMsg, h);
    skapi.RemoveHandler(evidOnPortalStormed, h);
    skapi.RemoveHandler(evidOnPortalStormWarning, h);
    skapi.RemoveHandler(evidOnRemoveFromInventory, h);
    skapi.RemoveHandler(evidOnSetFlagOther, h);
    skapi.RemoveHandler(evidOnSetFlagSelf, h);
    skapi.RemoveHandler(evidOnSpellAdd, h);
    skapi.RemoveHandler(evidOnSpellCastSelf, h);
    skapi.RemoveHandler(evidOnSpellExpire, h);
    skapi.RemoveHandler(evidOnSpellExpireSilent, h);
    skapi.RemoveHandler(evidOnSpellFailSelf, h);
    skapi.RemoveHandler(evidOnStart3D, h);
    skapi.RemoveHandler(evidOnStartPortalSelf, h);
    skapi.RemoveHandler(evidOnStatLevel, h);
    skapi.RemoveHandler(evidOnStatSkillExp, h);
    skapi.RemoveHandler(evidOnStatTotalBurden, h);
    skapi.RemoveHandler(evidOnStatTotalExp, h);
    skapi.RemoveHandler(evidOnStatTotalPyreals, h);
    skapi.RemoveHandler(evidOnStatUnspentExp, h);
    skapi.RemoveHandler(evidOnTargetHealth, h);
    skapi.RemoveHandler(evidOnTell, h);
    skapi.RemoveHandler(evidOnTellAllegiance, h);
    skapi.RemoveHandler(evidOnTellCovassal, h);
    skapi.RemoveHandler(evidOnTellFellowship, h);
    skapi.RemoveHandler(evidOnTellFollower, h);
    skapi.RemoveHandler(evidOnTellMelee, h);
    skapi.RemoveHandler(evidOnTellMisc, h);
    skapi.RemoveHandler(evidOnTellPatron, h);
    skapi.RemoveHandler(evidOnTellServer, h);
    skapi.RemoveHandler(evidOnTellVassal, h);
    skapi.RemoveHandler(evidOnTimer, h);
    skapi.RemoveHandler(evidOnTipMessage, h);
    skapi.RemoveHandler(evidOnTradeAccept, h);
    skapi.RemoveHandler(evidOnTradeAdd, h);
    skapi.RemoveHandler(evidOnTradeEnd, h);
    skapi.RemoveHandler(evidOnTradeReset, h);
    skapi.RemoveHandler(evidOnTradeStart, h);
    skapi.RemoveHandler(evidOnVitals, h);
    }

function main()
    {
    fAddHandlers();
    
    skapi.OutputLine("Welcome to t.js, starting now...",opmChatWnd);
    
    while (1)
        {skapi.WaitEvent(10)}
    
    fRemoveHandlers();
    }


