VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ACAppCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' This class provides access to information about the user's AC installation and
' about the running instance (if any) of the game.


'--------------
' Private data
'--------------

' Window class and caption strings of the running game.
Private Const szClassAC As String = "Turbine Device Class"
Private Const szCaptionAC As String = "Asheron's Call"

Private Const szRegPathACTOD As String = _
  "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\{CF455208-C302-4FB3-B21D-F7CBB03DDE5A}"

Private hwndMbr As Long
Private hprocessMbr As Long

Private szInstallDir As String
Private szUserDir As String
Private szVersionMbr As String
Private dxpResMbr As Long, dypResMbr As Long


'------------
' Properties
'------------

' Return the full path of the AC installation directory.
Public Property Get szDir() As String
    szDir = szInstallDir
End Property

' Return the full path of the AC client executable.
Public Property Get szClientExe() As String
    szClientExe = SzFileInInstallDir("ACClient.exe")
End Property

' Return the full path of Cell.dat.
Public Property Get szCellDat() As String
    szCellDat = SzFileInInstallDir("client_cell_1.dat")
End Property

' Return the full path of Portal.dat.
Public Property Get szPortalDat() As String
    szPortalDat = SzFileInInstallDir("client_portal.dat")
End Property

Public Property Get szUserPrefIni() As String
    szUserPrefIni = SzFileInUserDir("UserPreferences.ini")
End Property

' Return the build version of the AC executable (ACClient.exe).
Public Property Get szVersion() As String
    
    If szVersionMbr = "" Then
        szVersionMbr = SzExeVerFromFile(szClientExe)
        If szVersionMbr = "" Then
            szVersionMbr = "??.??.??.????"
        End If
    End If
    
    szVersion = szVersionMbr
    
End Property

' Return the window handle of the running game, or hwndNil if it's not running.
Public Property Get hwnd() As Long
    
    Call ValidateHwnd
    
    hwnd = hwndMbr
    
End Property

' Return the process handle of the running game, or hNil if it's not running.
Public Property Get hprocess() As Long
    
    Call ValidateHwnd
    
    If hprocessMbr = hNil And hwndMbr <> hwndNil Then
        Dim threadid As Long, pid As Long
        threadid = GetWindowThreadProcessId(hwnd, pid)
        hprocessMbr = OpenProcess(PROCESS_VM_READ + _
          PROCESS_QUERY_INFORMATION, False, pid)
    End If
    
    hprocess = hprocessMbr
    
End Property


' Return the width in pixels of the AC window's client area, as configured in the
' game's registry settings.
Public Property Get dxpRes() As Long
    
    If dxpResMbr = 0 Then Call GetRes
    
    dxpRes = dxpResMbr
    
End Property

' Return the height in pixels of the AC window's client area, as configured in the
' game's registry settings.
Public Property Get dypRes() As Long
    
    If dypResMbr = 0 Then Call GetRes
    
    dypRes = dypResMbr
    
End Property


' Return the width in pixels of the AC window's client area, as derived from
' the actual window.
Public Property Get dxpWindow() As Long
    
    Call ValidateHwnd
    
    If hwndMbr <> hwndNil Then
        Dim r As RECT
        Call GetClientRect(hwndMbr, r)
        dxpWindow = r.Right
    Else
        dxpWindow = dxpRes
    End If
    
End Property

' Return the height in pixels of the AC window's client area, as derived from
' the actual window..
Public Property Get dypWindow() As Long
    
    Call ValidateHwnd
    
    If hwndMbr <> hwndNil Then
        Dim r As RECT
        Call GetClientRect(hwndMbr, r)
        dypWindow = r.Bottom
    Else
        dypWindow = dypRes
    End If
    
End Property


' Return a dictionary of command names to virtual key codes, according to the
' user's configured key mappings.
Public Property Get dictSzCmidVk() As Dictionary
    
    Dim dict As Dictionary: Set dict = New Dictionary
    dict.CompareMode = TextCompare
    
'   Read out name of keymap file.
    Dim szKeyMap As String
    szKeyMap = SzGetIni(szUserPrefIni, "Input", "KeymapFile", "Default.keymap")
    
'   Open the keymap file.
    Dim ts As TextStream
    Set ts = fso.OpenTextFile(SzFileInUserDir(szKeyMap), ForReading)
    
'    Dim tsOut As TextStream
'    Set tsOut = fso.CreateTextFile("C:\Projects\SkunkWorks\CmidNames.txt")
    
'   Search for the Bindings section.
    Dim szLine As String
    Do
        szLine = SzTrimWhiteSpace(ts.ReadLine())
    Loop Until FEqSzI(szLine, "Bindings")
    
    szLine = SzTrimWhiteSpace(ts.ReadLine())
    Call Assert(szLine = "[", "Error parsing keymap file.")
    
'   Read and parse sections.
    Do
        szLine = SzTrimWhiteSpace(ts.ReadLine())
        If szLine = "]" Then Exit Do
        
        If szLine <> "" Then
            Dim szSection As String
            szSection = szLine
            
'            Call tsOut.WriteLine(szSection + ":")
            
            szLine = SzTrimWhiteSpace(ts.ReadLine())
            Call Assert(szLine = "[", "Error parsing keymap file.")
            
            Do
                szLine = SzTrimWhiteSpace(ts.ReadLine())
                If szLine = "]" Then Exit Do
                
                Dim szCmid As String, szDevice As String, szKey As String
                Dim szMeta As String, dwMeta As Long
                Call SplitSz(szCmid, szLine, szLine, " [ """" [ ")
                Call SplitSz(szDevice, szLine, szLine, " ")
                Call SplitSz(szKey, szLine, szLine, " ] ")
                If szLine <> "]" Then
                    Call SplitSz(szMeta, szLine, szLine, " ")
                    If FPrefixMatchSzI(szMeta, "0x") Then
                        szMeta = Right(szMeta, Len(szMeta) - 2)
                    End If
                    dwMeta = CLng("&H" + szMeta)
                Else
                    szMeta = ""
                    dwMeta = 0
                End If
                
'                Call tsOut.WriteLine(szCmid)
                
                If FPrefixMatchSzI(szKey, "DIK_") And szDevice = "0" Then
                    Dim vk As Long
                    vk = VkFromSzDIK(szKey)
                    If (dwMeta And 1) <> 0 Then
                        vk = vk Or maskVkShift
                    End If
                    If (dwMeta And 2) <> 0 Then
                        vk = vk Or maskVkCtrl
                    End If
                    If (dwMeta And 4) <> 0 Then
                        vk = vk Or maskVkAlt
                    End If
                '   In case of multiple keys for the same command, prefer the first.
                    If Not dict.Exists(szCmid) Then
                        dict.Item(szCmid) = vk
                    End If
'                    Call DebugLine(szCmid + ": " + SzHex(vk, 4))
                    szCmid = SzCmidOldFromSz(szCmid)
                    If szCmid <> "" Then
                        If Not dict.Exists(szCmid) Then
                            dict.Item(szCmid) = vk
                        End If
'                        Call DebugLine(szCmid + ": " + SzHex(vk, 4))
                    End If
                End If
            Loop
            
'            Call tsOut.WriteLine("")
        End If
    Loop
    
'    Call tsOut.Close
    
    Call ts.Close
    
    Set dictSzCmidVk = dict
    
End Property


'---------
' Methods
'---------

Public Function SzFileInInstallDir(ByVal SzLeaf As String) As String
    
    SzFileInInstallDir = fso.BuildPath(szInstallDir, SzLeaf)
    
End Function

Public Function SzFileInUserDir(ByVal SzLeaf As String) As String
    
    SzFileInUserDir = fso.BuildPath(szUserDir, SzLeaf)
    
End Function

Public Sub Launch(Optional ByVal szAccount As String = "", _
  Optional ByVal szPassword As String = "", _
  Optional ByVal szWorld As String = "")
    
    Dim szParams As String
    szParams = ""
    If szAccount <> "" Then
        szParams = szParams + " -Username " + szAccount
        If szPassword <> "" Then
            szParams = szParams + " -Password " + szPassword + " -SkipLogin"
        End If
    End If
    If szWorld <> "" Then
        szParams = szParams + " -World " + szWorld + " -SkipSelect"
    End If
    
    Call ShellExecute(hwndNil, "open", fso.BuildPath(szDir, "ACLauncher.exe"), _
      szParams, vbNullString, SW_SHOW)
    
End Sub

' Bring the AC window to the foreground.
Public Sub Activate()
    
    Call ValidateHwnd
    
    If hwndMbr <> hwndNil Then
        Call ShowWindow(hwndMbr, SW_RESTORE)
        Call SetForegroundWindow(hwndMbr)
    End If
    
End Sub


' Read a DWORD from the memloc specified by pdw.
Public Function DwReadMemloc(ByVal pdw As Long) As Long
    
'   Make sure the game is running.
    Call ValidateHwnd
    If hprocessMbr = hNil Then
        Call Err.Raise(errnoACNotRunning, "DwReadMemloc", "AC is not running.")
    End If
    
'   Try to read from its memory space.
    If Not FReadDw(DwReadMemloc, pdw) Then
        Call Err.Raise(errnoBadMemloc, "DwReadMemloc", "Invalid memloc.")
    End If
    
End Function


' Return the specified pixel's color as a long in standard Windows RGB format.
' xp and yp may be positive or negative.  Positive values measure down from
' upper left corner of the window; negative values measure up from the lower
' right.  For resolution independence, specify pixels near the right or bottom
' edges using negative xp or yp.
Public Function ClrOfPixel(ByVal xp As Long, ByVal yp As Long) As Long
    
    ClrOfPixel = clrNil
    
'   Make sure the game is running and has a window.
    Call ValidateHwnd
    If hwndMbr = hwndNil Then Exit Function
    
'   Convert negative coords to positive using window height and width.
    Call RectifyXyp(xp, yp)
    
'   Get a handle to the window's device context.
    Dim hdc As Long
    hdc = GetDC(hwndMbr)
    If hdc = hNil Then Exit Function
    
'   Read out the pixel data.
    ClrOfPixel = GetPixel(hdc, xp, yp)
    If ClrOfPixel < 0 Then ClrOfPixel = clrNil
    
'   Clean up.
    Call ReleaseDC(hwndMbr, hdc)
    
End Function

' Convert right- or bottom-relative coords to left- or top-relative.
Public Sub RectifyXyp(ByRef xp As Long, ByRef yp As Long)
    
    If xp < 0 Or yp < 0 Then
        Dim r As RECT
        Call ValidateHwnd
        Call GetClientRect(hwndMbr, r)
        If xp < 0 Then xp = xp + r.Right
        If yp < 0 Then yp = yp + r.Bottom
    End If
    
End Sub


' Make sure our AC process handle is valid, and return it.
'Public Function HprocessValidate() As Long
'
'    Dim itry As Long
'    For itry = 0 To 1
'        If hprocess = hNil Then Exit For
'
'    '   Make sure we can read the process memory space.
'        Dim mbi As MEMORY_BASIC_INFORMATION, cbMbi As Long
'        cbMbi = VirtualQueryEx(hprocess, 0, mbi, Len(mbi))
'        If cbMbi = Len(mbi) Then Exit For
'
'    '   Read failed; invalidate and retry.
'        Call InvalHwnd
'    Next itry
'
'    HprocessValidate = hprocess
'
'End Function

' Forget our cached screen resolution (in case user has run TweakAC or something).
Public Function InvalRes()
    
    dxpResMbr = 0
    dypResMbr = 0
    
End Function


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    hwndMbr = hwndNil
    hprocessMbr = hNil
    
'   Find the AC registry key.
    Dim hkey As Long
    hkey = HkeyOpen(HKEY_LOCAL_MACHINE, szRegPathACTOD, KEY_QUERY_VALUE)
    If hkey <> hNil Then
    '   Read out path of install dir.
        Call FGetSzValue(szInstallDir, hkey, "InstallLocation")
        Call DebugLine("AC install dir: " + szInstallDir)
    '   Clean up.
        Call RegCloseKey(hkey)
    End If
    
    szUserDir = fso.BuildPath(SzShellFolder("Personal"), "Asheron's Call")
    
'   Read out preferred screen resolution.
    dxpResMbr = 0
    dypResMbr = 0
    Call GetRes
    
    szVersionMbr = ""
    
End Sub

Private Sub Class_Terminate()
    
    Call InvalHwnd
    
End Sub


' Read the user's preferred screen resolution from the registry.
Private Sub GetRes()
    
    Dim szRes As String, szResH As String, szResV As String
    szRes = SzGetIni(szUserPrefIni, "Display", "Resolution", "800x600")
    Call SplitSz(szResH, szResV, szRes, "x")
    dxpResMbr = CLng(szResH)
    dypResMbr = CLng(szResV)
    
End Sub


' Forget our cached window and process handles, in preparation for
' recalculating them from scratch.
Private Function InvalHwnd()
    
    hwndMbr = hwndNil
    
    If hprocessMbr <> hNil Then
        Call CloseHandle(hprocessMbr)
        hprocessMbr = hNil
    End If
    
End Function

Private Sub ValidateHwnd()
    
    If IsWindow(hwndMbr) = False Then
        Call InvalHwnd
    ElseIf SzClassFromHwnd(hwndMbr) <> szClassAC Then
        Call InvalHwnd
'    ElseIf SzCaptionFromHwnd(hwndMbr) <> szCaptionAC Then
'        Call InvalHwnd
    End If
    
    If hwndMbr = hwndNil Then
        hwndMbr = FindWindow(szClassAC, szCaptionAC)
    End If
    
End Sub

' Attempt to read a DWORD from the memloc specified by pdw.  Return True if
' successful, and store the resulting value in dwOut.  Return False if
' unsuccessful.
Private Function FReadDw(ByRef dwOut As Long, ByVal pdw As Long) As Boolean
    
    FReadDw = False
    
    Dim cbRead As Long
    If ReadProcessMemory(hprocessMbr, pdw, dwOut, Len(dwOut), _
      cbRead) = False Then Exit Function
    If cbRead <> Len(dwOut) Then Exit Function
    
    FReadDw = True
    
End Function


Private Function VkFromSzDIK(ByVal szDIK As String) As Long
    
    Select Case szDIK
        Case "DIK_0": VkFromSzDIK = VK_0
        Case "DIK_1": VkFromSzDIK = VK_1
        Case "DIK_2": VkFromSzDIK = VK_2
        Case "DIK_3": VkFromSzDIK = VK_3
        Case "DIK_4": VkFromSzDIK = VK_4
        Case "DIK_5": VkFromSzDIK = VK_5
        Case "DIK_6": VkFromSzDIK = VK_6
        Case "DIK_7": VkFromSzDIK = VK_7
        Case "DIK_8": VkFromSzDIK = VK_8
        Case "DIK_9": VkFromSzDIK = VK_9
        Case "DIK_A": VkFromSzDIK = VK_A
'       Case "DIK_ABNT_C1": VkFromSzDIK = VK_ABNT_C1
'       Case "DIK_ABNT_C2": VkFromSzDIK = VK_ABNT_C2
        Case "DIK_ADD": VkFromSzDIK = VK_ADD
        Case "DIK_APOSTROPHE": VkFromSzDIK = VK_OEM_7
        Case "DIK_APPS": VkFromSzDIK = VK_APPS
'       Case "DIK_AT": VkFromSzDIK = VK_AT
        Case "DIK_AX": VkFromSzDIK = VK_OEM_AX
        Case "DIK_B": VkFromSzDIK = VK_B
        Case "DIK_BACK": VkFromSzDIK = VK_BACK
        Case "DIK_BACKSLASH": VkFromSzDIK = VK_OEM_5
        Case "DIK_C": VkFromSzDIK = VK_C
'       Case "DIK_CALCULATOR": VkFromSzDIK = VK_CALCULATOR
        Case "DIK_CAPITAL": VkFromSzDIK = VK_CAPITAL
'       Case "DIK_COLON": VkFromSzDIK = VK_COLON
        Case "DIK_COMMA": VkFromSzDIK = VK_OEM_COMMA
        Case "DIK_CONVERT": VkFromSzDIK = VK_CONVERT
        Case "DIK_D": VkFromSzDIK = VK_D
        Case "DIK_DECIMAL": VkFromSzDIK = VK_DECIMAL
        Case "DIK_DELETE": VkFromSzDIK = VK_DELETE
        Case "DIK_DIVIDE": VkFromSzDIK = VK_DIVIDE
        Case "DIK_DOWN": VkFromSzDIK = VK_DOWN
        Case "DIK_DOWNARROW": VkFromSzDIK = VK_DOWN
        Case "DIK_E": VkFromSzDIK = VK_E
        Case "DIK_END": VkFromSzDIK = VK_END
        Case "DIK_EQUALS": VkFromSzDIK = VK_OEM_PLUS
        Case "DIK_ESCAPE": VkFromSzDIK = VK_ESCAPE
        Case "DIK_F": VkFromSzDIK = VK_F
        Case "DIK_F1": VkFromSzDIK = VK_F1
        Case "DIK_F10": VkFromSzDIK = VK_F10
        Case "DIK_F11": VkFromSzDIK = VK_F11
        Case "DIK_F12": VkFromSzDIK = VK_F12
        Case "DIK_F13": VkFromSzDIK = VK_F13
        Case "DIK_F14": VkFromSzDIK = VK_F14
        Case "DIK_F15": VkFromSzDIK = VK_F15
        Case "DIK_F2": VkFromSzDIK = VK_F2
        Case "DIK_F3": VkFromSzDIK = VK_F3
        Case "DIK_F4": VkFromSzDIK = VK_F4
        Case "DIK_F5": VkFromSzDIK = VK_F5
        Case "DIK_F6": VkFromSzDIK = VK_F6
        Case "DIK_F7": VkFromSzDIK = VK_F7
        Case "DIK_F8": VkFromSzDIK = VK_F8
        Case "DIK_F9": VkFromSzDIK = VK_F9
        Case "DIK_G": VkFromSzDIK = VK_G
        Case "DIK_GRAVE": VkFromSzDIK = VK_OEM_3
        Case "DIK_H": VkFromSzDIK = VK_H
        Case "DIK_HOME": VkFromSzDIK = VK_HOME
        Case "DIK_I": VkFromSzDIK = VK_I
        Case "DIK_INSERT": VkFromSzDIK = VK_INSERT
        Case "DIK_J": VkFromSzDIK = VK_J
        Case "DIK_K": VkFromSzDIK = VK_K
        Case "DIK_KANA": VkFromSzDIK = VK_KANA
        Case "DIK_KANJI": VkFromSzDIK = VK_KANJI
        Case "DIK_L": VkFromSzDIK = VK_L
        Case "DIK_LBRACKET": VkFromSzDIK = VK_OEM_4
        Case "DIK_LCONTROL": VkFromSzDIK = VK_LCONTROL
        Case "DIK_LEFT": VkFromSzDIK = VK_LEFT
        Case "DIK_LEFTARROW": VkFromSzDIK = VK_LEFT
        Case "DIK_LMENU": VkFromSzDIK = VK_LMENU
        Case "DIK_LSHIFT": VkFromSzDIK = VK_LSHIFT
        Case "DIK_LWIN": VkFromSzDIK = VK_LWIN
        Case "DIK_M": VkFromSzDIK = VK_M
'       Case "DIK_MAIL": VkFromSzDIK = VK_MAIL
'       Case "DIK_MEDIASELECT": VkFromSzDIK = VK_MEDIASELECT
'       Case "DIK_MEDIASTOP": VkFromSzDIK = VK_MEDIASTOP
        Case "DIK_MINUS": VkFromSzDIK = VK_OEM_MINUS
        Case "DIK_MULTIPLY": VkFromSzDIK = VK_MULTIPLY
'       Case "DIK_MUTE": VkFromSzDIK = VK_MUTE
'       Case "DIK_MYCOMPUTER": VkFromSzDIK = VK_MYCOMPUTER
        Case "DIK_N": VkFromSzDIK = VK_N
        Case "DIK_NEXT": VkFromSzDIK = VK_NEXT
'       Case "DIK_NEXTTRACK": VkFromSzDIK = VK_NEXTTRACK
'       Case "DIK_NOCONVERT": VkFromSzDIK = VK_NOCONVERT
        Case "DIK_NUMLOCK": VkFromSzDIK = VK_NUMLOCK
        Case "DIK_NUMPAD0": VkFromSzDIK = VK_NUMPAD0
        Case "DIK_NUMPAD1": VkFromSzDIK = VK_NUMPAD1
        Case "DIK_NUMPAD2": VkFromSzDIK = VK_NUMPAD2
        Case "DIK_NUMPAD3": VkFromSzDIK = VK_NUMPAD3
        Case "DIK_NUMPAD4": VkFromSzDIK = VK_NUMPAD4
        Case "DIK_NUMPAD5": VkFromSzDIK = VK_NUMPAD5
        Case "DIK_NUMPAD6": VkFromSzDIK = VK_NUMPAD6
        Case "DIK_NUMPAD7": VkFromSzDIK = VK_NUMPAD7
        Case "DIK_NUMPAD8": VkFromSzDIK = VK_NUMPAD8
        Case "DIK_NUMPAD9": VkFromSzDIK = VK_NUMPAD9
'       Case "DIK_NUMPADCOMMA": VkFromSzDIK = VK_NUMPADCOMMA
        Case "DIK_NUMPADENTER": VkFromSzDIK = VK_EXECUTE
'       Case "DIK_NUMPADEQUALS": VkFromSzDIK = VK_NUMPADEQUALS
        Case "DIK_NUMPADMINUS": VkFromSzDIK = VK_SUBTRACT
        Case "DIK_NUMPADPLUS": VkFromSzDIK = VK_ADD
        Case "DIK_NUMPADSLASH": VkFromSzDIK = VK_DIVIDE
        Case "DIK_NUMPADSTAR": VkFromSzDIK = VK_MULTIPLY
        Case "DIK_O": VkFromSzDIK = VK_O
        Case "DIK_OEM_102": VkFromSzDIK = VK_OEM_102
        Case "DIK_P": VkFromSzDIK = VK_P
        Case "DIK_PAUSE": VkFromSzDIK = VK_PAUSE
        Case "DIK_PERIOD": VkFromSzDIK = VK_OEM_PERIOD
        Case "DIK_PGDN": VkFromSzDIK = VK_NEXT
        Case "DIK_PGUP": VkFromSzDIK = VK_PRIOR
'       Case "DIK_PLAYPAUSE": VkFromSzDIK = VK_PLAYPAUSE
'       Case "DIK_POWER": VkFromSzDIK = VK_POWER
'       Case "DIK_PREVTRACK": VkFromSzDIK = VK_PREVTRACK
        Case "DIK_PRIOR": VkFromSzDIK = VK_PRIOR
        Case "DIK_Q": VkFromSzDIK = VK_Q
        Case "DIK_R": VkFromSzDIK = VK_R
        Case "DIK_RBRACKET": VkFromSzDIK = VK_OEM_6
        Case "DIK_RCONTROL": VkFromSzDIK = VK_RCONTROL
        Case "DIK_RETURN": VkFromSzDIK = VK_RETURN
        Case "DIK_RIGHT": VkFromSzDIK = VK_RIGHT
        Case "DIK_RIGHTARROW": VkFromSzDIK = VK_RIGHT
        Case "DIK_RMENU": VkFromSzDIK = VK_RMENU
        Case "DIK_RSHIFT": VkFromSzDIK = VK_RSHIFT
        Case "DIK_RWIN": VkFromSzDIK = VK_RWIN
        Case "DIK_S": VkFromSzDIK = VK_S
        Case "DIK_SCROLL": VkFromSzDIK = VK_SCROLL
        Case "DIK_SEMICOLON": VkFromSzDIK = VK_OEM_1
        Case "DIK_SLASH": VkFromSzDIK = VK_OEM_2
        Case "DIK_SLEEP": VkFromSzDIK = VK_SLEEP
        Case "DIK_SPACE": VkFromSzDIK = VK_SPACE
'       Case "DIK_STOP": VkFromSzDIK = VK_STOP
        Case "DIK_SUBTRACT": VkFromSzDIK = VK_SUBTRACT
'       Case "DIK_SYSRQ": VkFromSzDIK = VK_SYSRQ
        Case "DIK_T": VkFromSzDIK = VK_T
        Case "DIK_TAB": VkFromSzDIK = VK_TAB
        Case "DIK_U": VkFromSzDIK = VK_U
'       Case "DIK_UNDERLINE": VkFromSzDIK = VK_UNDERLINE
'       Case "DIK_UNLABELED": VkFromSzDIK = VK_UNLABELED
        Case "DIK_UP": VkFromSzDIK = VK_UP
        Case "DIK_UPARROW": VkFromSzDIK = VK_UP
        Case "DIK_V": VkFromSzDIK = VK_V
'       Case "DIK_VOLUMEDOWN": VkFromSzDIK = VK_VOLUMEDOWN
'       Case "DIK_VOLUMEUP": VkFromSzDIK = VK_VOLUMEUP
        Case "DIK_W": VkFromSzDIK = VK_W
'       Case "DIK_WAKE": VkFromSzDIK = VK_WAKE
'       Case "DIK_WEBBACK": VkFromSzDIK = VK_WEBBACK
'       Case "DIK_WEBFAVORITES": VkFromSzDIK = VK_WEBFAVORITES
'       Case "DIK_WEBFORWARD": VkFromSzDIK = VK_WEBFORWARD
'       Case "DIK_WEBHOME": VkFromSzDIK = VK_WEBHOME
'       Case "DIK_WEBREFRESH": VkFromSzDIK = VK_WEBREFRESH
'       Case "DIK_WEBSEARCH": VkFromSzDIK = VK_WEBSEARCH
'       Case "DIK_WEBSTOP": VkFromSzDIK = VK_WEBSTOP
        Case "DIK_X": VkFromSzDIK = VK_X
        Case "DIK_Y": VkFromSzDIK = VK_Y
'       Case "DIK_YEN": VkFromSzDIK = VK_YEN
        Case "DIK_Z": VkFromSzDIK = VK_Z
    Case Else: VkFromSzDIK = 0
    End Select
    
End Function

Private Function SzCmidOldFromSz(ByVal sz As String) As String
    
    Select Case sz
    Case "CameraMoveToward": SzCmidOldFromSz = "CameraCloser"
    Case "CameraMoveAway": SzCmidOldFromSz = "CameraFarther"
    Case "CameraRotateLeft": SzCmidOldFromSz = "CameraLeftRotate"
    Case "CameraRotateRight": SzCmidOldFromSz = "CameraLower"
    Case "CameraRotateUp": SzCmidOldFromSz = "CameraRaise"
    Case "CameraRotateDown": SzCmidOldFromSz = "CameraRightRotate"
    Case "CameraViewFirstPerson": SzCmidOldFromSz = "FirstPersonView"
    Case "CameraViewLookDown": SzCmidOldFromSz = "FloorView"
    Case "CameraViewMapMode": SzCmidOldFromSz = "MapView"
    Case "CameraViewDefault": SzCmidOldFromSz = "ResetView"
    Case "CameraActivateAlternateMode": SzCmidOldFromSz = "ShiftView"
    Case "MovementRunLock": SzCmidOldFromSz = "AutoRun"
    Case "MovementWalkMode": SzCmidOldFromSz = "HoldRun"
'    Case ???: SzCmidOldFromSz = "HoldSidestep"
    Case "MovementJump": SzCmidOldFromSz = "Jump"
    Case "MovementStrafeLeft": SzCmidOldFromSz = "SideStepLeft"
    Case "MovementStrafeRight": SzCmidOldFromSz = "SideStepRight"
    Case "MovementTurnLeft": SzCmidOldFromSz = "TurnLeft"
    Case "MovementTurnRight": SzCmidOldFromSz = "TurnRight"
    Case "MovementBackup": SzCmidOldFromSz = "WalkBackwards"
    Case "MovementForward": SzCmidOldFromSz = "WalkForward"
    Case "CombatHighAttack": SzCmidOldFromSz = "HighAttack"
    Case "CombatLowAttack": SzCmidOldFromSz = "LowAttack"
    Case "CombatMediumAttack": SzCmidOldFromSz = "MediumAttack"
    Case "CombatToggleCombat": SzCmidOldFromSz = "ToggleCombat"
    Case "CombatDecreaseAttackPower": SzCmidOldFromSz = "DecreasePowerSetting"
    Case "CombatIncreaseAttackPower": SzCmidOldFromSz = "IncreasePowerSetting"
    Case "SelectionClosestCompassItem": SzCmidOldFromSz = "ClosestCompassItem"
    Case "SelectionClosestItem": SzCmidOldFromSz = "ClosestItem"
    Case "SelectionClosestMonster": SzCmidOldFromSz = "ClosestMonster"
    Case "SelectionClosestPlayer": SzCmidOldFromSz = "ClosestPlayer"
    Case "SelectionLastAttacker": SzCmidOldFromSz = "LastAttacker"
    Case "SelectionNextCompassItem": SzCmidOldFromSz = "NextCompassItem"
    Case "SelectionNextFellow": SzCmidOldFromSz = "NextFellow"
    Case "SelectionNextItem": SzCmidOldFromSz = "NextItem"
    Case "SelectionNextMonster": SzCmidOldFromSz = "NextMonster"
    Case "SelectionNextPlayer": SzCmidOldFromSz = "NextPlayer"
    Case "SelectionPreviousCompassItem": SzCmidOldFromSz = "PreviousCompassItem"
    Case "SelectionPreviousFellow": SzCmidOldFromSz = "PreviousFellow"
    Case "SelectionPreviousItem": SzCmidOldFromSz = "PreviousItem"
    Case "SelectionPreviousMonster": SzCmidOldFromSz = "PreviousMonster"
    Case "SelectionPreviousPlayer": SzCmidOldFromSz = "PreviousPlayer"
    Case "SelectionPreviousSelection": SzCmidOldFromSz = "PreviousSelection"
    Case "SelectionDrop": SzCmidOldFromSz = "DropSelected"
    Case "SelectionExamine": SzCmidOldFromSz = "ExamineSelected"
    Case "SelectionGive": SzCmidOldFromSz = "GiveSelected"
    Case "SelectionPickUp": SzCmidOldFromSz = "AutosortSelected"
    Case "SelectionSelf": SzCmidOldFromSz = "SelectSelf"
    Case "SelectionSplitStack": SzCmidOldFromSz = "SplitSelected"
    Case "USE": SzCmidOldFromSz = "UseSelected"
    Case "ToggleAllegiancePanel": SzCmidOldFromSz = "AllegiancePanel"
    Case "ToggleAttributesPanel": SzCmidOldFromSz = "AttributesPanel"
    Case "ToggleCharacterInfoPanel": SzCmidOldFromSz = "CharacterInformationPanel"
    Case "ToggleCharacterOptionsPanel": SzCmidOldFromSz = "CharacterOptionsPanel"
    Case "ToggleFellowshipPanel": SzCmidOldFromSz = "FellowshipPanel"
    Case "ToggleNegativeEffectsPanel": SzCmidOldFromSz = "HarmfulSpellsPanel"
    Case "TogglePositiveEffectsPanel": SzCmidOldFromSz = "HelpfulSpellsPanel"
    Case "ToggleHousePanel": SzCmidOldFromSz = "HousePanel"
    Case "ToggleInventoryPanel": SzCmidOldFromSz = "InventoryPanel"
    Case "ToggleLinkStatusPanel": SzCmidOldFromSz = "LinkStatusPanel"
    Case "ToggleMapPanel": SzCmidOldFromSz = "MapPanel"
    Case "ToggleOptionsPanel": SzCmidOldFromSz = "OptionsPanel"
    Case "ToggleSkillsPanel": SzCmidOldFromSz = "SkillsPanel"
    Case "ToggleConfigOptionsPanel": SzCmidOldFromSz = "SoundAndGraphicsPanel"
    Case "ToggleSpellComponentsPanel": SzCmidOldFromSz = "SpellComponentsPanel"
    Case "ToggleSpellbookPanel": SzCmidOldFromSz = "SpellbookPanel"
    Case "ToggleSocialPanel": SzCmidOldFromSz = "TradePanel"
    Case "ToggleVitaePanel": SzCmidOldFromSz = "VitaePanel"
    Case "PlayerOption_AcceptLootPermits": SzCmidOldFromSz = "AcceptCorpseLooting"
    Case "PlayerOption_AdvancedCombatUI": SzCmidOldFromSz = "AdvancedCombatInterface"
    Case "PlayerOption_UseDeception": SzCmidOldFromSz = "AttemptToDeceivePlayers"
'    Case ???: SzCmidOldFromSz = "AutoCreateShortcuts"
    Case "PlayerOption_AutoRepeatAttack": SzCmidOldFromSz = "AutoRepeatAttacks"
    Case "PlayerOption_AutoTarget": SzCmidOldFromSz = "AutoTarget"
    Case "PlayerOption_ViewCombatTarget": SzCmidOldFromSz = "AutoTrackCombatTargets"
    Case "PlayerOption_DisableHouseRestrictionEffects": SzCmidOldFromSz = "DisableHouseEffect"
'    Case ???: SzCmidOldFromSz = "DisableWeather"
    Case "PlayerOption_ShowTooltips": SzCmidOldFromSz = "DisplayTooltips"
    Case "PlayerOption_IgnoreAllegianceRequests": SzCmidOldFromSz = "IgnoreAllegianceRequests"
    Case "PlayerOption_IgnoreFellowshipRequests": SzCmidOldFromSz = "IgnoreFellowshipRequests"
    Case "PlayerOption_IgnoreTradeRequests": SzCmidOldFromSz = "IgnoreTradeRequests"
'    Case ???: SzCmidOldFromSz = "InvertMouseLook"
    Case "PlayerOption_AllowGive": SzCmidOldFromSz = "LetPlayersGiveYouItems"
'    Case ???: SzCmidOldFromSz = "MuteOnLosingFocus"
'    Case ???: SzCmidOldFromSz = "RightClickToMouseLook"
    Case "PlayerOption_ToggleRun": SzCmidOldFromSz = "RunAsDefaultMovement"
    Case "PlayerOption_FellowshipShareLoot": SzCmidOldFromSz = "ShareFellowshipLoot"
    Case "PlayerOption_FellowshipShareXP": SzCmidOldFromSz = "ShareFellowshipXP"
    Case "PlayerOption_CoordinatesOnRadar": SzCmidOldFromSz = "ShowRadarCoordinates"
    Case "PlayerOption_SpellDuration": SzCmidOldFromSz = "ShowSpellDurations"
    Case "PlayerOption_StayInChatMode": SzCmidOldFromSz = "StayInChatModeAfterSend"
    Case "PlayerOption_StretchUI": SzCmidOldFromSz = "StretchUI"
    Case "PlayerOption_VividTargetingIndicator": SzCmidOldFromSz = "VividTargetIndicator"
    Case "CaptureScreenshot": SzCmidOldFromSz = "CaptureScreenshotToFile"
    Case Else: SzCmidOldFromSz = ""
    End Select
    
End Function















