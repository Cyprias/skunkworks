Attribute VB_Name = "ACDefs"
Option Explicit
' AC network message definitions.


' AC message types:
Public Const mtyDestroyObject = &H240000
Public Const mtyAdjustStackSize = &H1970000
Public Const mtyPlayerKill = &H19E0000
Public Const mtyIndirectText = &H1E00000
Public Const mtyEmoteText = &H1E20000
Public Const mtyCreatureMessage = &H2BB0000
Public Const mtyCreatureMessageRanged = &H2BC0000
Public Const mtySetCharacterDWORD = &H2CD0000
Public Const mtySetObjectDWORD = &H2CE0000
Public Const mtySetCharacterQWORD = &H2CF0000
Public Const mtySetObjectBoolean = &H2D20000
Public Const mtySetObjectString = &H2D60000
Public Const mtySetObjectResource = &H2D80000
Public Const mtySetCharacterLink = &H2D90000
Public Const mtySetObjectLink = &H2DA0000
Public Const mtySetCharacterPosition = &H2DB0000
Public Const mtySetCharacterSkillLevel = &H2DD0000
Public Const mtySetCharacterSkillState = &H2E10000
Public Const mtySetCharacterAttribute = &H2E30000
Public Const mtySetCharacterMaximumVital = &H2E70000
Public Const mtySetCharacterCurrentVital = &H2E90000
Public Const mtyLifestoneMaterialize = &HF6190000
Public Const mtyChangeModel = &HF6250000
Public Const mtyCharCreationInitilisation = &HF6430000
Public Const mtyEnd3DMode = &HF6530000
Public Const mtyCharDeletion = &HF6550000
Public Const mtyRequestLogin = &HF6570000
Public Const mtyCharacterList = &HF6580000
Public Const mtyCharacterLoginFailure = &HF6590000
Public Const mtyCreateObject = &HF7450000
'Public Const mtyLoginCharacter = &HF7460000&
Public Const mtyRemoveItem = &HF7470000
Public Const mtySetPositionAndMotion = &HF7480000
Public Const mtyWieldObject = &HF7490000
Public Const mtyMoveObjectIntoInventory = &HF74A0000
Public Const mtyToggleObjectVisibility = &HF74B0000
Public Const mtyAnimation = &HF74C0000
Public Const mtyJumping = &HF74E0000
Public Const mtyApplySoundEffect = &HF7500000
Public Const mtyEnterPortalMode = &HF7510000
Public Const mtyApplyVisualSoundEffect = &HF7550000
Public Const mtyGameEvent = &HF7B00000
Public Const mtyUpdateObject = &HF7DB0000
Public Const mtyTurbineChat = &HF7DE0000
Public Const mtyStart3DMode = &HF7DF0000
Public Const mtyServerMessage = &HF7E00000
Public Const mtyServerName = &HF7E10000
Public Const mtyUpdateResource = &HF7E20000
Public Const mtyDatFilePatchList = &HF7E70000

' Message subtypes for mtypeGameEvent:
Public Const mtyMessageBox = &HF7B00004
Public Const mtyLoginCharacter = &HF7B00013
Public Const mtyAllegianceInfo = &HF7B00020
Public Const mtyInsertInventoryItem = &HF7B00022
Public Const mtyWearItem = &HF7B00023
Public Const mtyCloseContainer = &HF7B00052
Public Const mtyApproachVendor = &HF7B00062
Public Const mtyFailureToGiveItem = &HF7B000A0
Public Const mtyRemoveFellowshipMember = &HF7B000A3
Public Const mtyDismissFellowshipMember = &HF7B000A4
Public Const mtyReadTableOfContents = &HF7B000B4
Public Const mtyReadPage = &HF7B000B8
Public Const mtyIdentifyObject = &HF7B000C9
Public Const mtyGroupChat = &HF7B00147
Public Const mtySetPackContents = &HF7B00196
Public Const mtyDropFromInventory = &HF7B0019A
Public Const mtyAttackCompleted = &HF7B001A7
Public Const mtyDeleteSpellFromSpellbook = &HF7B001A8
Public Const mtyYourDeath = &HF7B001AC
Public Const mtyKillDeathMessage = &HF7B001AD
Public Const mtyInflictMeleeDamage = &HF7B001B1
Public Const mtyReceiveMeleeDamage = &HF7B001B2
Public Const mtyOtherMeleeEvade = &HF7B001B3
Public Const mtySelfMeleeEvade = &HF7B001B4
Public Const mtyStartMeleeAttack = &HF7B001B8
Public Const mtyUpdateHealth = &HF7B001C0
Public Const mtyAgeCommandResult = &HF7B001C3
Public Const mtyReadyPreviousActionComplete = &HF7B001C7
Public Const mtyPingReply = &HF7B001EA
Public Const mtySquelchedUsersList = &HF7B001F4
Public Const mtyEnterTrade = &HF7B001FD
Public Const mtyEndTrade = &HF7B001FF
Public Const mtyAddTradeItem = &HF7B00200
Public Const mtyAcceptTrade = &HF7B00202
Public Const mtyUnAcceptTrade = &HF7B00203
Public Const mtyResetTrade = &HF7B00205
Public Const mtyFailureToTradeAnItem = &HF7B00207
Public Const mtyHousePurchaseMaintainence = &HF7B0021D
Public Const mtyHouseGuestList = &HF7B00257
Public Const mtyUpdateItemManaBar = &HF7B00264
Public Const mtyHousesAvailable = &HF7B00271
Public Const mtyConfirmationPanel = &HF7B00274
Public Const mtyConfirmationPanelClosed = &HF7B00276
Public Const mtyAllegianceMemberLoginOut = &HF7B0027A
Public Const mtyActionFailure = &HF7B0028A
Public Const mtyErrorWithParameter = &HF7B0028B
Public Const mtyTell = &HF7B002BD
Public Const mtyCreateFellowship = &HF7B002BE
Public Const mtyDisbandFellowship = &HF7B002BF
Public Const mtyAddFellowshipMember = &HF7B002C0
Public Const mtyAddSpellToSpellbook = &HF7B002C1
Public Const mtyAddCharacterEnchantment = &HF7B002C2
Public Const mtyRemoveCharacterEnchantment = &HF7B002C3
Public Const mtyRemoveMultipleCharacterEnchantments = &HF7B002C5
Public Const mtyRemoveAllCharacterEnchantmentsSilent = &HF7B002C6
Public Const mtyRemoveCharacterEnchantmentSilent = &HF7B002C7
Public Const mtyRemoveMultipleCharacterEnchantmentsSilent = &HF7B002C8
Public Const mtyStatusMessage = &HF7B002EB

' REVIEW these messages:
Public Const mtyClientEvent = &HF7B10000
Public Const mtyCTSUpdateSingleOption = &HF7B10005
Public Const mtyCTSUseItem = &HF7B10036
Public Const mtyCTSUpdateOptions = &HF7B101A1


' Weapon types:
Public Const wtyNil = 0
Public Const wtyMeleeWeapon = 1
Public Const wtyMissileWeapon = 2
Public Const wtyAmmo = 3
Public Const wtyShield = 4

' Wielding slots (which, annoyingly, are not quite the same as weapon type):
Public Const slotNil = 0
Public Const slotMeleeWeapon = 1
Public Const slotMissileWeapon = 2
Public Const slotShield = 3

' Stance codes for mtyAnimation:
Public Const stanceUnarmed = &H3C&
Public Const stanceStanding = &H3D&
Public Const stanceMelee = &H3E&
Public Const stanceBow = &H3F&
Public Const stanceShield = &H40&
Public Const stanceSpellcasting = &H49&

' DWORD property keys:
Public Const lkeySpecies = &H2
Public Const lkeyBurden = &H5
Public Const lkeyEquippedSlots = &HA
Public Const lkeyRareID = &H11
Public Const lkeyIoc = &H12                      ' REVIEW: Test & report to Ken.
Public Const lkeyValue = &H13
Public Const lkeyTotalPyreals = &H14
Public Const lkeySkillCreditsAvailable = &H18
Public Const lkeyCreatureLevel = &H19
Public Const lkeyToDOnly = &H1A
Public Const lkeyArmorLevel = &H1C
Public Const lkeyRank = &H1E
Public Const lkeyBonded = &H21
Public Const lkeyFollowers = &H23
Public Const lkeyEnchantability = &H24
Public Const lkeyLockDiff = &H26                ' Lockpick difficulty of a door or chest.
Public Const lkeyDeaths = &H2B
Public Const lkeyDmtyWand = &H2D                ' Wand damage type.
Public Const lkeyLvlMin = &H56
Public Const lkeyLvlMax = &H57
Public Const lkeyDlvlLockpick = &H58            ' Not implemented.
Public Const lkeyVitalRestored = &H59
Public Const lkeyDlvlRestored = &H5A            ' Also used for healing kit skill bonus.
Public Const lkeyCuseMax = &H5B
Public Const lkeyCuseLeft = &H5C
Public Const lkeyDateOfBirth = &H62
Public Const lkeyWorkmanship = &H69
Public Const lkeySpellcraft = &H6A
Public Const lkeyManaCur = &H6B
Public Const lkeyManaMax = &H6C
Public Const lkeyLoreReq = &H6D
Public Const lkeyRankReq = &H6E
Public Const lkeyPortalFlags = &H6F
Public Const lkeyGender = &H71
Public Const lkeyAttuned = &H72
Public Const lkeySklvlReq = &H73
Public Const lkeyManaCost = &H75
Public Const lkeyAge = &H7D
Public Const lkeyDexpVPPoint = &H81
Public Const lkeyMaterial = &H83
Public Const lkeyCharType = &H86
Public Const lkeyWieldReqKind = &H9E
Public Const lkeyWieldReqID = &H9F
Public Const lkeyWieldReqLvl = &HA0
Public Const lkeySalvageCount = &HAA
Public Const lkeyTinkerCount = &HAB
Public Const lkeyDescriptionFormat = &HAC
Public Const lkeyLockUnknown = &HAD             ' Something related to locks and lockpicks; seems to be always 100 for picks.
Public Const lkeyCpageUsed = &HAE
Public Const lkeyCpageTotal = &HAF
Public Const lkeySkidReq = &HB0
Public Const lkeySetWithCount = &HB1
Public Const lkeySetWithMaterial = &HB2
Public Const lkeyImbues = &HB3
Public Const lkeyRaceReq = &HBC
Public Const lkeyFishingSkill = &HC0
Public Const lkeyCkeyOnRing = &HC1
Public Const lkeyElemDmgBonus = &HCC            ' Elemental damage bonus
'Public Const lkeyRetained = &HFF               ' REVIEW

' QWORD property keys:
Public Const qkeyExpTotal = &H1
Public Const qkeyExpUnassigned = &H2

' Boolean property keys:
Public Const bkeyOpen = &H2
Public Const bkeyLocked = &H3
Public Const bkeyHookVisibility = &H18
Public Const bkeyUnlimitedUses = &H3F
Public Const bkeySellable = &H45
Public Const bkeyRetained = &H5B
Public Const bkeyIvoryable = &H63
Public Const bkeyDyeable = &H64
Public Const bkeyDropOnDeath = &H6C
Public Const bkeyAFK = &H6E

' Double property keys:
Public Const dkeyCsecPerMana = &H5
Public Const dkeyMeleeDBonus = &H1D
Public Const dkeyEfficiency = &H57
Public Const dkeyRestorationBonus = &H64
Public Const dkeyProbDestroy = &H89             ' Chance of destruction for mana stones.
Public Const dkeyManaConvMod = &H90
Public Const dkeyMissileDBonus = &H95
Public Const dkeyMagicDBonus = &H96
Public Const dkeyPvMElemBonus = &H98

' String property keys:
Public Const skeyName = &H1
Public Const skeyTitle = &H5
Public Const skeyInscription = &H7
Public Const skeyInscriber = &H8
Public Const skeyWielderReq = &H9
Public Const skeyFellowship = &HA
Public Const skeyUseInstructions = &HE
Public Const skeySimpleDesc = &HF
Public Const skeyDetailedDesc = &H10
Public Const skeyRaceReq = &H13
Public Const skeyMonarch = &H15
Public Const skeyCreator = &H19
Public Const skeyPatron = &H23
Public Const skeyPortalDest = &H26
Public Const skeyLastTinkerer = &H27
Public Const skeyImbuer = &H28
Public Const skeyDateOfBirth = &H2B

' Resource property keys:
Public Const rkeyIcon = &H8

' Link property keys:
Public Const akeyContainer = &H2
Public Const akeyEquippedBy = &H3
Public Const akeyLastAttacker = &HB
Public Const akeyAllegianceObject = &H18
Public Const akeyPatron = &H19
Public Const akeyMonarch = &H1A
Public Const akeyOwnedBy = &H20

' Position property keys:
Public Const pkeyLastCorpseLocation = &HE

' Chat message types (seems to be the same as CMC):
Public Const tychBroadcast = &H0
Public Const tychPublicChat = &H2
Public Const tychPrivateTell = &H3
Public Const tychOutgoingTell = &H4
Public Const tychMagicSpellResults = &H7
Public Const tychNPCChat = &HC
Public Const tychPlayerSpellcasting = &H11
Public Const tychCreatureChat = &H12
Public Const tychRecall = &H17


Public Function SzFromMcm(ByVal mcm As Long) As String
    
    Static dictMcmSz As Dictionary
    If dictMcmSz Is Nothing Then
        Set dictMcmSz = New Dictionary
        dictMcmSz.Item(mcmWeaponsMelee) = "Weapons"
        dictMcmSz.Item(mcmArmor) = "Armor"
        dictMcmSz.Item(mcmClothing) = "Clothing"
        dictMcmSz.Item(mcmJewelry) = "Jewelry"
        dictMcmSz.Item(mcmCreature) = "Creatures"
        dictMcmSz.Item(mcmFood) = "Food"
        dictMcmSz.Item(mcmPyreal) = "Pyreals"
        dictMcmSz.Item(mcmMisc) = "Miscellaneous"
        dictMcmSz.Item(mcmWeaponsMissile) = "Weapons"
        dictMcmSz.Item(mcmContainers) = "Containers"
        dictMcmSz.Item(mcmMiscFletching) = "Miscellaneous"
        dictMcmSz.Item(mcmGems) = "Gems"
        dictMcmSz.Item(mcmSpellComponents) = "Spell Components"
        dictMcmSz.Item(mcmBooksPaper) = "Books, Paper"
        dictMcmSz.Item(mcmKeysTools) = "Keys, Tools"
        dictMcmSz.Item(mcmMagicItems) = "Magic Items"
        dictMcmSz.Item(mcmPortal) = "Portals"
        dictMcmSz.Item(mcmTradeNotes) = "Trade Notes"
        dictMcmSz.Item(mcmManaStones) = "Mana Stones"
        dictMcmSz.Item(mcmServices) = "Services"
        dictMcmSz.Item(mcmPlants) = "Plants"
        dictMcmSz.Item(mcmCookingItems1) = "Cooking Items"
        dictMcmSz.Item(mcmAlchemicalItems1) = "Alchemical Items"
        dictMcmSz.Item(mcmFletchingItems1) = "Fletching Items"
        dictMcmSz.Item(mcmCookingItems2) = "Cooking Items"
        dictMcmSz.Item(mcmAlchemicalItems2) = "Alchemical Items"
        dictMcmSz.Item(mcmFletchingItems2) = "Fletching Items"
        dictMcmSz.Item(mcmTinkeringTools) = "Keys, Tools"
    End If
    
    SzFromMcm = dictMcmSz.Item(mcm)
    
End Function

Public Function SzFromSkid(ByVal skid As Long) As String
    
    Static mpskidsz() As String
    Static fInited As Boolean
    If Not fInited Then
        ReDim mpskidsz(skidMax - 1)
        mpskidsz(skidAlchemy) = "Alchemy"
        mpskidsz(skidArmorTinkering) = "Armor Tinkering"
        mpskidsz(skidItemTinkering) = "Item Tinkering"
        mpskidsz(skidMagicItemTinkering) = "Magic Item Tinkering"
        mpskidsz(skidWeaponTinkering) = "Weapon Tinkering"
        mpskidsz(skidArcaneLore) = "Arcane Lore"
        mpskidsz(skidAssessCreature) = "Assess Creature"
        mpskidsz(skidAssessPerson) = "Assess Person"
        mpskidsz(skidAxe) = "Axe"
        mpskidsz(skidBow) = "Bow"
        mpskidsz(skidCooking) = "Cooking"
        mpskidsz(skidCreatureEnchantment) = "Creature Enchantment"
        mpskidsz(skidCrossbow) = "Crossbow"
        mpskidsz(skidDagger) = "Dagger"
        mpskidsz(skidDeception) = "Deception"
        mpskidsz(skidFletching) = "Fletching"
        mpskidsz(skidHealing) = "Healing"
        mpskidsz(skidItemEnchantment) = "Item Enchantment"
        mpskidsz(skidJump) = "Jump"
        mpskidsz(skidLeadership) = "Leadership"
        mpskidsz(skidLifeMagic) = "Life Magic"
        mpskidsz(skidLockpick) = "Lockpick"
        mpskidsz(skidLoyalty) = "Loyalty"
        mpskidsz(skidMace) = "Mace"
        mpskidsz(skidMagicDefense) = "Magic Defense"
        mpskidsz(skidManaConversion) = "Mana Conversion"
        mpskidsz(skidMeleeDefense) = "Melee Defense"
        mpskidsz(skidMissileDefense) = "Missile Defense"
        mpskidsz(skidRun) = "Run"
        mpskidsz(skidSalvaging) = "Salvaging"
        mpskidsz(skidSpear) = "Spear"
        mpskidsz(skidStaff) = "Staff"
        mpskidsz(skidSword) = "Sword"
        mpskidsz(skidThrownWeapons) = "Thrown Weapons"
        mpskidsz(skidUnarmedCombat) = "Unarmed Combat"
        mpskidsz(skidWarMagic) = "War Magic"
        fInited = True
    End If
    
    SzFromSkid = mpskidsz(skid)
    
End Function

Public Function SzFromGender(ByVal gender As Long) As String
    
    Select Case gender
    Case 1
       SzFromGender = "Male"
    Case 2
       SzFromGender = "Female"
    End Select
    
End Function

Public Function SzFromRace(ByVal race As Long) As String
    
    Select Case race
    Case 1
       SzFromRace = "Aluvian"
    Case 2
       SzFromRace = "Gharu'ndim"
    Case 3
       SzFromRace = "Sho"
    Case 4
       SzFromRace = "Viamontian"
    End Select
    
End Function









