VERSION 5.00
Begin VB.Form formAbout 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "About SkunkWorks"
   ClientHeight    =   3030
   ClientLeft      =   2340
   ClientTop       =   1935
   ClientWidth     =   5775
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "About.frx":0000
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3030
   ScaleWidth      =   5775
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picIcon 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      ClipControls    =   0   'False
      Height          =   1395
      Left            =   240
      Picture         =   "About.frx":030A
      ScaleHeight     =   936.7
      ScaleMode       =   0  'User
      ScaleWidth      =   905.126
      TabIndex        =   1
      Top             =   240
      Width           =   1350
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Default         =   -1  'True
      Height          =   345
      Left            =   4365
      TabIndex        =   0
      Top             =   2280
      Width           =   1020
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00808080&
      BorderStyle     =   6  'Inside Solid
      Index           =   1
      X1              =   120
      X2              =   5640
      Y1              =   1800
      Y2              =   1800
   End
   Begin VB.Label lblDescription 
      Caption         =   "Scripting platform for Asheron's Call"
      ForeColor       =   &H00000000&
      Height          =   450
      Left            =   1860
      TabIndex        =   2
      Top             =   1125
      Width           =   3075
   End
   Begin VB.Label lblTitle 
      Caption         =   "SkunkWorks"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   480
      Left            =   1860
      TabIndex        =   4
      Top             =   240
      Width           =   3075
   End
   Begin VB.Line Line1 
      BorderColor     =   &H00FFFFFF&
      BorderWidth     =   2
      Index           =   0
      X1              =   120
      X2              =   5640
      Y1              =   1815
      Y2              =   1815
   End
   Begin VB.Label lblVersion 
      Caption         =   "Version"
      Height          =   225
      Left            =   1860
      TabIndex        =   5
      Top             =   780
      Width           =   3075
   End
   Begin VB.Label lblDisclaimer 
      Caption         =   "Warning: ..."
      ForeColor       =   &H00000000&
      Height          =   825
      Left            =   255
      TabIndex        =   3
      Top             =   2025
      Width           =   3870
   End
End
Attribute VB_Name = "formAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub Form_Load()
    
    Caption = "About " + swoptions.szAppName
    lblTitle.Caption = swoptions.szAppName
    lblVersion.Caption = "Version " + skapi.szVersion + " by Greg Kusnick"
    
    lblDisclaimer.Caption = _
        "Use this program at your own risk.  The author makes no warranty " + _
        "regarding its performance or suitability, and is not " + _
        "responsible for any damage or loss resulting from its use."
    
'   Call DrawIcon(picIcon.hdc, 0, 0, LoadStdIcon(hNil, IDI_APPLICATION))
    
End Sub

Private Sub cmdClose_Click()
    
    Call Unload(Me)
    
End Sub












