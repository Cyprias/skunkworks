VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ACCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' This class implements the ac object visible to the script.  Note that
' the properties and methods here are simply wrappers for native APIs
' implemented by the skapi object.


'--------------
' Private data
'--------------

Private opmDebug As Long

Private mpevidszHandler(evidMaxACS - 1) As String
Private serialNext As Long


'------------
' Properties
'------------

Public Property Get Arguments() As Object
    
    Dim cosz As CoszCls: Set cosz = New CoszCls
    
    Dim szArgs As String
    szArgs = skapi.szArgs
    Do While szArgs <> ""
        Dim szArg As String
        Call SplitSz(szArg, szArgs, szArgs, ";")
        Call cosz.Add(szArg)
    Loop
    
    Set Arguments = cosz
    
End Property

Public Property Get burden() As Long
    burden = self.burdenCur
End Property

Public Property Get ConnectedToAC() As Long
    
    If skapi.plig = pligNotRunning Then
        ConnectedToAC = 0
        
'   ACScript compatibility hack:
'   If script started after login, don't reveal full ctac until
'   we've fired a phony OnLogon event.
    ElseIf (SkGlobals.ctac And ctacCachedLogin) <> 0 And _
      (SkGlobals.ctac And ctacInventoryFromCache) = 0 Then
        ConnectedToAC = ctacConnected
        
    Else
        ConnectedToAC = SkGlobals.ctac
        
    End If
    
End Property

Public Property Get GetSelectedObjectID() As Long
    Dim aco As AcoCls
    Set aco = skapi.acoSelected
    If aco Is Nothing Then
        GetSelectedObjectID = oidNil
    Else
        GetSelectedObjectID = aco.oid
    End If
End Property

Public Property Get health() As Long
    health = self.healthCur
End Property

Public Property Get InCombatMode() As Boolean
    InCombatMode = skapi.fCombatMode
End Property

Public Property Get Location() As Variant
    If skapi.acoChar Is Nothing Then
    '   This appears to be the way ACScript does it:
        Dim maploc As MaplocCls: Set maploc = New MaplocCls
        Call maploc.FromLandblockXY(0, 0, 0, 0, 0)
        Location = maploc.rgv
    Else
        Location = skapi.acoChar.maploc.rgv
    End If
End Property

Public Property Get mana() As Long
    mana = self.manaCur
End Property

Public Property Get MyGender() As String
    MyGender = self.szGender
End Property

Public Property Get MyLevel() As Long
    MyLevel = self.lvl
End Property

Public Property Get MyName() As String
    MyName = self.szName
End Property

Public Property Get MyProfession() As String
    MyProfession = self.szProfession
End Property

Public Property Get MyRace() As String
    MyRace = self.szRace
End Property

Public Property Get ObjectFromName(ByVal szName As String) As Long
    Dim aco As AcoCls
    Set aco = skapi.AcoFromSz(szName)
    If aco Is Nothing Then
        ObjectFromName = oidNil
    Else
        ObjectFromName = aco.oid
    End If
End Property

Public Property Get Pyreals() As Long
    Pyreals = self.cpyCash
End Property

Public Property Get SpellCastResult() As Long
    SpellCastResult = skapi.arc
End Property

Public Property Get SpellNumOnTab(ByVal itab As Long, ByVal ispell As Long) As Long
    SpellNumOnTab = self.rgspellid(itab, ispell)
End Property

Public Property Get stamina() As Long
    stamina = self.staminaCur
End Property

Public Property Get Version() As String
'   Prefix version string with the word "SkunkWorks" so scripts can tell they're
'   not running under ACScript.
    Version = "SkunkWorks " + skapi.szVersion
End Property

Public Property Get World() As String
    World = skapi.szWorld
End Property


'---------
' Methods
'---------

Public Sub Beep(ByVal n As Long)
    
    Dim i As Long
    For i = 0 To n - 1
        Call PlaySound("Default Beep", hNil, SND_ALIAS Or SND_SYNC)
    Next i
    
End Sub

' Can't call this Debug because that's a VB reserved word.
Public Sub Debug_(ByVal sz As String)
    
    Call skapi.OutputSz(sz, opmDebug)
    
End Sub

Public Function GetObject(ByVal oid As Variant) As Object
    Select Case VarType(oid)
    Case vbString
    '   Undocumented feature: you can pass in a string instead of an OID!
    '   (I never knew that before.)
        Set GetObject = skapi.AcoFromSz(oid)
    Case Else
        Set GetObject = skapi.AcoFromOid(oid)
    End Select
    If GetObject Is Nothing Then
        Set GetObject = New AcoEmptyCls
    End If
End Function

Public Function GetObjectCollection(Optional ByVal ocm As Variant = ocmAll) As Object
    Set GetObjectCollection = skapi.CoacoFromOcm(ocm)
End Function

Public Function ItemProperty(ByVal oid As Variant, ByVal szProperty As String) As Variant
    Dim aco As AcoCls
    Set aco = skapi.AcoFromOid(oid)
    If aco Is Nothing Then
        If FEqSzI(szProperty, "exists") Then
            ItemProperty = False
        Else
            ItemProperty = Empty
        End If
    Else
        Select Case LCase(szProperty)
        Case "name"
            ItemProperty = aco.Name
        Case "name2"
            ItemProperty = aco.Name2
        Case "holder"
            ItemProperty = aco.Holder
        Case "holderslot"
            ItemProperty = aco.HolderSlot
        Case "value"
            ItemProperty = aco.cpyValue
        Case "location"
            ItemProperty = aco.Location
        Case "totalvalue"
            ItemProperty = 0 ' aco.TotalValue
        Case "stackcount"
            ItemProperty = aco.StackCount
        Case "stackmax"
            ItemProperty = aco.StackMax
        Case "container"
            ItemProperty = aco.Container
        Case "owner"
            ItemProperty = aco.Owner
        Case "model"
            ItemProperty = aco.model
        Case "icon"
            ItemProperty = aco.icon
        Case "type"
            ItemProperty = aco.Type_
        Case "usesleft"
            ItemProperty = aco.UsesLeft
        Case "usesmax"
            ItemProperty = aco.UsesMax
        Case "exists"
            ItemProperty = True
        End Select
    End If
End Function

Public Sub KeyEvent(ByVal sz As String)
    Call skapi.Keys(sz, kmoClick)
End Sub

Public Sub KeyPaste(ByVal sz As String)
    Call skapi.PasteSz(sz)
End Sub

Public Sub KeyPress(ByVal sz As String)
    Call skapi.Keys(sz, kmoDown)
End Sub

Public Sub KeyRelease(ByVal sz As String)
    Call skapi.Keys(sz, kmoUp)
End Sub

Public Sub MouseClick(ByVal ibutton As Long)
    Call skapi.MouseButton(ibutton, kmoClick)
End Sub

Public Sub MouseButtonPress(ByVal ibutton As Long)
    Call skapi.MouseButton(ibutton, kmoDown)
End Sub

Public Sub MouseButtonRelease(ByVal ibutton As Long)
    Call skapi.MouseButton(ibutton, kmoUp)
End Sub

Public Sub MouseMove(ByVal xp As Long, ByVal yp As Long)
    Call skapi.MouseMove(xp, yp)
End Sub

' How is this different from MouseMove?
Public Sub MouseMoveAbs(ByVal xp As Long, ByVal yp As Long)
    Call skapi.MouseMove(xp, yp)
End Sub

' Can't call this Print because that's a VB reserved word.
Public Sub Print_(ByVal sz As String)
    
    Call skapi.OutputSz(sz, opmConsole)
    
End Sub


' Register standard ACMsg_* event handlers before script begins execution.
Public Sub RegisterStandardHandlers()
    Call TraceEnter("ac.RegisterStandardHandlers")
    
    Dim evid As Long
    For evid = 1 To evidMaxACS - 1
        Call RegisterHandler(evid, mpevidszHandler(evid), True)
    Next evid
    
    Call TraceExit("ac.RegisterStandardHandlers")
End Sub

Public Function RegisterHandler(ByVal evid As Long, ByVal szFunc As String, _
  Optional ByVal fStandard As Boolean = False) As Long
'   Call TraceEnter("ac.RegisterHandler", CStr(evid) + ", " + SzQuote(szFunc))
    
    Call Assert(evid > 0 And evid < evidMax, "evid out of range")
    
'   Create a skapi-compatible wrapper object and register it.
    Dim acsh As AcshCls: Set acsh = New AcshCls
    acsh.szHandler = szFunc
    acsh.fStandard = fStandard
    Call skapi.AddHandler(evid, acsh)
    
'   Recover a reference to the created handler.
    Dim llobj As LlobjCls
    Set llobj = mpevidllobj(evid)
    
'   Assign it a serial number.
    llobj.serial = serialNext
    serialNext = serialNext + 1
    
'   Return the serial number as the "cookie" for unregistering.  Note that
'   if we could use the handler itself as the cookie, we wouldn't need the
'   serial number.  But that's awkward in VBScript because you have to say
'   "Set" on object assignments, and existing scripts don't say it on their
'   cookie assignments.  So we're stuck with scalar cookies for VBS
'   compatibility.
    RegisterHandler = llobj.serial
    
'   Call TraceExit("ac.RegisterHandler", CStr(RegisterHandler))
End Function

Public Sub UnregisterHandler(ByVal evid As Long, ByVal serial As Long)
'   Call TraceEnter("ac.UnregisterHandler", CStr(evid) + ", " + CStr(serial))
    
'   Use the cookie to find the handler object.  (See note above.)
    Dim llobj As LlobjCls
    Set llobj = mpevidllobj(evid)
    Do Until llobj Is Nothing
        If llobj.serial = serial Then
        '   Unregister the handler object with skapi.
            Call skapi.RemoveHandler(evid, llobj.obj)
            Exit Do
        End If
    Loop
    
'   Call TraceExit("ac.UnregisterHandler")
End Sub


Public Sub SetDebug(ByVal flags As Long)
    
    opmDebug = 0
    If (flags And &H1&) Then opmDebug = opmDebug Or opmDebugLog
    If (flags And &H2&) Then opmDebug = opmDebug Or opmConsole
    
End Sub

Public Sub SetPreviousSelectedObject(ByVal oid As Variant)
    Call skapi.SetOidSelPrev(LFromV(oid))
End Sub

Public Sub Sleep(ByVal cmsec As Long)
    Call skapi.Sleep(cmsec)
End Sub

' Undocumented API used by StressBot.js:
Public Sub Test(ByVal v As Variant)
'   Not sure what the parameter is for but I'm just going to print it out.
    Call skapi.OutputLine("ac.Test(" + CStr(v) + ")", opmDebugLog)
End Sub

Public Function WaitEvent(Optional ByVal cmsec As Long = 0, _
  Optional ByVal mode As Long = 0, Optional ByVal evid As Long = evidNil) As Long
    WaitEvent = skapi.WaitEvent(cmsec, mode, evid)
End Function


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    opmDebug = opmConsole Or opmDebugLog
    
    Call InitMpevidszHandler
    serialNext = 0
    
End Sub

Private Sub InitMpevidszHandler()
    
'   This table maps event IDS into their standard ACMsg_* names.
    mpevidszHandler(evidOnVitals) = "ACMsg_OnVitals"
    mpevidszHandler(evidOnLocChangeSelf) = "ACMsg_OnLocChangeSelf"
    mpevidszHandler(evidOnLocChangeOther) = "ACMsg_OnLocChangeOther"
    mpevidszHandler(evidOnStatTotalBurden) = "ACMsg_OnStatTotalBurden"
    mpevidszHandler(evidOnStatTotalExp) = "ACMsg_OnStatTotalExp"
    mpevidszHandler(evidOnStatUnspentExp) = "ACMsg_OnStatUnspentExp"
    mpevidszHandler(evidOnObjectCreatePlayer) = "ACMsg_OnObjectCreatePlayer"
    mpevidszHandler(evidOnStatSkillExp) = "ACMsg_OnStatSkillExp"
    mpevidszHandler(evidOnAdjustStack) = "ACMsg_OnAdjustStack"
    mpevidszHandler(evidOnChatLocal) = "ACMsg_OnChatLocal"
    mpevidszHandler(evidOnChatSpell) = "ACMsg_OnChatSpell"
    mpevidszHandler(evidOnDeathSelf) = "ACMsg_OnDeathSelf"
    mpevidszHandler(evidOnMyPlayerKill) = "ACMsg_OnMyPlayerKill"
    mpevidszHandler(evidOnDeathOther) = "ACMsg_OnDeathOther"
    mpevidszHandler(evidOnEnd3D) = "ACMsg_OnEnd3D"
    mpevidszHandler(evidOnStart3D) = "ACMsg_OnStart3D"
    mpevidszHandler(evidOnStartPortalSelf) = "ACMsg_OnStartPortalSelf"
    mpevidszHandler(evidOnEndPortalSelf) = "ACMsg_OnEndPortalSelf"
    mpevidszHandler(evidOnEndPortalOther) = "ACMsg_OnEndPortalOther"
    mpevidszHandler(evidOnCombatMode) = "ACMsg_OnCombatMode"
    mpevidszHandler(evidOnAnimationSelf) = "ACMsg_OnAnimationSelf"
    mpevidszHandler(evidOnTargetHealth) = "ACMsg_OnTargetHealth"
    mpevidszHandler(evidOnAddToInventory) = "ACMsg_OnAddToPack"
    mpevidszHandler(evidOnRemoveFromInventory) = "ACMsg_OnRemoveFromInventory"
    mpevidszHandler(evidOnAssessCreature) = "ACMsg_OnAssessCreature"
    mpevidszHandler(evidOnAssessItem) = "ACMsg_OnAssessItem"
    mpevidszHandler(evidOnSpellFailSelf) = "ACMsg_OnSpellFailSelf"
    mpevidszHandler(evidOnMeleeEvadeSelf) = "ACMsg_OnMeleeEvadeSelf"
    mpevidszHandler(evidOnMeleeEvadeOther) = "ACMsg_OnMeleeEvadeOther"
    mpevidszHandler(evidOnMeleeDamageSelf) = "ACMsg_OnMeleeDamageSelf"
    mpevidszHandler(evidOnMeleeDamageOther) = "ACMsg_OnMeleeDamageOther"
    mpevidszHandler(evidOnLogon) = "ACMsg_OnLogon"
    mpevidszHandler(evidOnTellServer) = "ACMsg_OnTellServer"
    mpevidszHandler(evidOnTell) = "ACMsg_OnTell"
    mpevidszHandler(evidOnTellMelee) = "ACMsg_OnTellMelee"
    mpevidszHandler(evidOnTellMisc) = "ACMsg_OnTellMisc"
    mpevidszHandler(evidOnTellFellowship) = "ACMsg_OnTellFellowship"
    mpevidszHandler(evidOnTellPatron) = "ACMsg_OnTellPatron"
    mpevidszHandler(evidOnTellVassal) = "ACMsg_OnTellVassal"
    mpevidszHandler(evidOnTellFollower) = "ACMsg_OnTellFollower"
    mpevidszHandler(evidOnDeathMessage) = "ACMsg_OnDeathMessage"
    mpevidszHandler(evidOnSpellCastSelf) = "ACMsg_OnSpellCastSelf"
    mpevidszHandler(evidOnTradeStart) = "ACMsg_OnTradeStart"
    mpevidszHandler(evidOnTradeEnd) = "ACMsg_OnTradeEnd"
    mpevidszHandler(evidOnTradeAdd) = "ACMsg_OnTradeAdd"
    mpevidszHandler(evidOnTradeReset) = "ACMsg_OnTradeReset"
    mpevidszHandler(evidOnSetFlagSelf) = "ACMsg_OnSetFlagSelf"
    mpevidszHandler(evidOnSetFlagOther) = "ACMsg_OnSetFlagOther"
    mpevidszHandler(evidOnApproachVendor) = "ACMsg_OnApproachVendor"
    mpevidszHandler(evidOnChatBroadcast) = "ACMsg_OnChatBroadcast"
    mpevidszHandler(evidOnTradeAccept) = "ACMsg_OnTradeAccept"
    mpevidszHandler(evidOnObjectCreate) = "ACMsg_OnObjectCreate"
    mpevidszHandler(evidOnChatServer) = "ACMsg_OnChatServer"
    mpevidszHandler(evidOnSpellExpire) = "ACMsg_OnSpellExpire"
    mpevidszHandler(evidOnSpellExpireSilent) = "ACMsg_OnSpellExpireSilent"
    mpevidszHandler(evidOnStatTotalPyreals) = "ACMsg_OnStatTotalPyreals"
    mpevidszHandler(evidOnStatLevel) = "ACMsg_OnStatLevel"
    mpevidszHandler(evidOnChatEmoteStandard) = "ACMsg_OnChatEmoteStandard"
    mpevidszHandler(evidOnChatEmoteCustom) = "ACMsg_OnChatEmoteCustom"
    mpevidszHandler(evidOnOpenContainer) = "ACMsg_OnOpenContainer"
    mpevidszHandler(evidOnPortalStormWarning) = "ACMsg_OnPortalStormWarning"
    mpevidszHandler(evidOnPortalStormed) = "ACMsg_OnPortalStormed"
    
End Sub







































