VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AcfCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Option Compare Text
' ACF: ACO Collection Filter


'--------------
' Private data
'--------------

Private maplocMbr As MaplocCls
Private acoWearerMbr As AcoCls

Private maplocFrom As MaplocCls


'------------
' Properties
'------------

Public szName As String
Public eqm As Long
Public mcm As Long
Public ocm As Long
Public olc As Long
Public oty As Long
Public material As Long
Public workmanshipMin As Single, workmanshipMax As Single
Public burdenMin As Long, burdenMax As Long
Public cpyMin As Long, cpyMax As Long
Public citemStackMin As Long, citemStackMax As Long
Public cuseLeftMin As Long, cuseLeftMax As Long
Public distMin As Single, distMax As Single
Public oidMonarch As Long

' Obsolete, but must be defined for binary compatibility.
Public oidNoteSeller As Long


Public Property Get acoWearer() As AcoCls
    Set acoWearer = acoWearerMbr
End Property

Public Property Set acoWearer(ByVal acoNew As AcoCls)
    Set acoWearerMbr = acoNew
End Property


Public Property Get maploc() As MaplocCls
    Set maploc = maplocMbr
End Property

Public Property Set maploc(ByVal maplocNew As MaplocCls)
    Set maplocMbr = maplocNew
End Property


'---------
' Methods
'---------

Public Sub Reset()
    
'   Default settings include everything.
    szName = ""
    eqm = &HFFFFFFFF
    mcm = &HFFFFFFFF
    ocm = &HFFFFFFFF
    olc = &HFFFFFFFF
    oty = &HFFFFFFFF
    material = 0
    workmanshipMin = 0
    workmanshipMax = 10
    burdenMin = 0
    burdenMax = &H7FFFFFFF
    Set acoWearerMbr = Nothing
    cpyMin = 0
    cpyMax = &H7FFFFFFF
    citemStackMin = 0
    citemStackMax = &H7FFFFFFF
    cuseLeftMin = 0
    cuseLeftMax = &H7FFFFFFF
    distMin = 0
    distMax = 1000
    oidMonarch = -1
    oidNoteSeller = -1
    
End Sub

' Return a collection of ACOs matching the filter criteria.
Public Function CoacoGet(Optional ByVal coaco As CoacoCls = Nothing) As CoacoCls
    
    Set CoacoGet = CoacoGetSorted(Nothing, coaco)
    
End Function

' Return an arbitrary ACO matching the filter criteria, or Nothing if there
' are no such ACOs.
Public Function AcoGet(Optional ByVal coaco As CoacoCls = Nothing) As AcoCls
    
    Set AcoGet = AcoGetSorted(Nothing, coaco)
    
End Function

' Return a sorted collection of ACOs matching the filter criteria.
Public Function CoacoGetSorted(ByVal sortby As Object, _
  Optional ByVal coaco As CoacoCls = Nothing) As CoacoCls
    
    If Not (maploc Is Nothing) Then
        If TypeName(sortby) = "SortbyDistCls" Then
            Set sortby.maplocFrom = maploc
        End If
    End If
    
    Dim rgaco() As AcoCls, caco As Long
    Call RgacoGet(rgaco, caco, coaco, False, Nothing)
    
    Set CoacoGetSorted = New CoacoCls
    Call CoacoGetSorted.SetMutable
    Call CoacoGetSorted.FromRgaco(rgaco, caco, sortby)
    
End Function

' Return the least ACO matching the sort/filter criteria, or Nothing if there
' are no such ACOs.
Public Function AcoGetSorted(ByVal sortby As Object, _
  Optional ByVal coaco As CoacoCls = Nothing) As AcoCls
    
    If Not (maploc Is Nothing) Then
        If TypeName(sortby) = "SortbyDistCls" Then
            Set sortby.maplocFrom = maploc
        End If
    End If
    
    Dim rgaco(0) As AcoCls, caco As Long
    Call RgacoGet(rgaco, caco, coaco, True, sortby)
    
    Set AcoGetSorted = rgaco(0)
    
End Function

Public Function FMatchAco(ByVal aco As AcoCls) As Boolean
    
    Call SetUp
    FMatchAco = FIncludeAco(aco)
    Call CleanUp
    
End Function


'------------------------------------------
' Dummy methods for backward compatibility
'------------------------------------------

' This was meant to be Private all along, but somehow got tagged Public,
' so now we're stuck with it.
Public Function CoacoGetEx(ByVal coaco As CoacoCls, ByVal fSingle As Boolean) As CoacoCls
    
    Call NotImplemented
    
End Function


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    Call Reset
    
End Sub


Private Sub RgacoGet(rgaco() As AcoCls, ByRef caco As Long, _
  ByVal coaco As CoacoCls, ByVal fSingle As Boolean, ByVal sortby As Object)
    On Error GoTo LExit
    
    caco = 0
    
    Call SetUp
    
    Dim aco As AcoCls
    If Not (coaco Is Nothing) Then
    '   Filter the passed-in coaco.
        If Not fSingle Then ReDim rgaco(coaco.count)
        For Each aco In coaco
            
            If FIncludeAco(aco) Then
                If FAddAco(rgaco, caco, aco, fSingle, sortby) Then GoTo LExit
            End If
            
        Next aco
    ElseIf olc = olcInventory Then
    '   Filter just the inventory.
        If Not fSingle Then ReDim rgaco(102 + 7 * 24)
        Dim ipack As Long
        For ipack = 0 To otable.cpack - 1
            For Each aco In otable.AcoFromIpack(ipack).coacoContents
                
                If FIncludeAco(aco) Then
                    If FAddAco(rgaco, caco, aco, fSingle, sortby) Then GoTo LExit
                End If
                
            Next aco
        Next ipack
    Else
    '   Filter the entire object table.
        If Not fSingle Then ReDim rgaco(otable.caco)
        Dim rgacoOtable As Variant, iaco As Long
        rgacoOtable = otable.rgaco
        For iaco = 0 To UBound(rgacoOtable)
            Set aco = rgacoOtable(iaco)
            
            If FIncludeAco(aco) Then
                If FAddAco(rgaco, caco, aco, fSingle, sortby) Then GoTo LExit
            End If
            
        Next iaco
    End If
    
LExit:
    
    If Err.Number <> 0 Then
        Call LogError(Err.Source, Err.Description, Err.Number)
        Call Err.Clear
    End If
    
    Call CleanUp
    
End Sub

Private Function FAddAco(rgaco() As AcoCls, ByRef caco As Long, _
  ByVal aco As AcoCls, ByVal fSingle As Boolean, ByVal sortby As Object) As Boolean
    
    If fSingle And Not (sortby Is Nothing) And caco > 0 Then
    '   If new ACO sorts out earlier than previous best, keep the new one.
        If sortby.CompareAcos(aco, rgaco(0)) < 0 Then
            Set rgaco(0) = aco
        End If
    Else
        Set rgaco(caco) = aco
        caco = caco + 1
    End If
    
    FAddAco = fSingle And sortby Is Nothing
    
End Function

Private Sub SetUp()
    
    If distMin = 0 And distMax = 1000 Then
        Set maplocFrom = Nothing
    ElseIf maplocMbr Is Nothing Then
        Set maplocFrom = skapi.maplocCur
    Else
        Set maplocFrom = maplocMbr
    End If
    
End Sub

Private Function FIncludeAco(ByVal aco As AcoCls) As Boolean
    FIncludeAco = False
    
'   Do the quickest tests first, slowest last.
    
    If eqm <> &HFFFFFFFF Then
    '   Exclude items that don't match the specified equipment types.
        If (aco.eqm And eqm) = 0 Then Exit Function
    End If
    If mcm <> &HFFFFFFFF Then
    '   Exclude items that don't match the specified merchant categories.
        If (aco.mcm And mcm) = 0 Then Exit Function
    End If
    If ocm <> &HFFFFFFFF Then
    '   Exclude items that don't match the specified object categories.
        If (aco.ocm And ocm) = 0 Then Exit Function
    End If
    If olc <> &HFFFFFFFF Then
    '   Exclude items that don't match the specified location categories.
        If (aco.olc And olc) = 0 Then Exit Function
    End If
    If oty <> &HFFFFFFFF Then
    '   Exclude items that don't match the specified object flags.
        If (aco.oty And oty) = 0 Then Exit Function
    End If
    If Not (acoWearerMbr Is Nothing) Then
    '   Exclude items not being worn by the specified player.
        If Not (aco.acoWearer Is acoWearerMbr) Then Exit Function
    End If
    If material <> 0 Then
    '   Exclude items that aren't nade of the specified material.
        If aco.material <> material Then Exit Function
    End If
    If workmanshipMin > 0 Or workmanshipMax < 10 Then
    '   Exclude items that fall outside the specified workmanship range.
        If aco.workmanship < workmanshipMin Or aco.workmanship > workmanshipMax Then Exit Function
    End If
    If burdenMin > 0 Or burdenMax < &H7FFFFFFF Then
    '   Exclude items that fall outside the specified burden range.
        If aco.burden < burdenMin Or aco.burden > burdenMax Then Exit Function
    End If
    If oidMonarch <> -1 Then
    '   Exclude players who aren't members of the specified monarchy.
        If aco.oidMonarch <> oidMonarch Then Exit Function
    End If
    If cpyMin > 0 Or cpyMax < &H7FFFFFFF Then
    '   Exclude items that fall outside the specified value range.
        If aco.cpyValue < cpyMin Or aco.cpyValue > cpyMax Then Exit Function
    End If
    If citemStackMin > 0 Or citemStackMax < &H7FFFFFFF Then
    '   Exclude items that fall outside the specified stack size range.
        If aco.citemStack < citemStackMin Or aco.citemStack > citemStackMax Then Exit Function
    End If
    If cuseLeftMin > 0 Or cuseLeftMax < &H7FFFFFFF Then
    '   Exclude items that fall outside the specified use count range.
        If aco.cuseLeft < cuseLeftMin Or aco.cuseLeft > cuseLeftMax Then Exit Function
    End If
    If szName <> "" Then
    '   Exclude items whose name doesn't match the specified pattern.
        If Not (aco.szName Like szName) Then Exit Function
    End If
    If Not (maplocFrom Is Nothing) Then
    '   Exclude items that fall outside the specified distance range.
        Dim dist As Single
        dist = maplocFrom.Dist3DToMaploc(aco.MaplocTrans)
        If dist < distMin Or dist > distMax Then Exit Function
    End If
    
    FIncludeAco = True
End Function

Private Sub CleanUp()
    
    Set maplocFrom = Nothing
    
End Sub










