VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AcoCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' This class implements an AC Object as returned by ac.GetObject().  It exposes
' both the traditional properties defined by ACScript and some new, native
' SkunkWorks methods and properties.


'--------------
' Private data
'--------------

Private oidMbr As Long
Private szNameMbr As String, szPluralMbr As String
Private mcmMbr As Long
Private otyMbr As Long
Private eqmMbr As Long
Private modelMbr As Long
Private iconMbr As Long
Private iconOverlayMbr As Long
Private iconUnderlayMbr As Long
Private iocMbr As Long
Private cpyValueMbr As Long
Private materialMbr As Long
Private workmanshipMbr As Single
Private citemStackMbr As Long, citemMaxStackMbr As Long
Private cuseLeftMbr As Long, cuseMaxMbr As Long
Private maplocMbr As MaplocCls
Private burdenMbr As Long
Private acoContainerMbr As AcoCls, iitemMbr As Long
Private acoWearerMbr As AcoCls, eqmWearerMbr As Long
Private oidMonarchMbr As Long
Private spellidMbr As Long
Private citemAvailMbr As Long
Private distApproachMbr As Single
Private fOpenMbr As Boolean

'' Meaningless number provided solely for ACScript compatibility.
'Private dwTotalValueMbr As Long

Private citemMaxMbr As Long, cpackMaxMbr As Long
Private coacoContentsMbr As CoacoCls

Private oaiMbr As OaiCls

Private fTradeItemMbr As Boolean

Private mbufPendingMbr As MbufCls

Private fPendingMbr As Boolean, fPendingInsertMbr As Boolean
Private fOrphanedMbr As Boolean

Private Const iconHook = 8384


'------------
' Properties
'------------

' Return the object ID of this object.
Public Property Get oid() As Long
    oid = oidMbr
End Property

' Return True if this ACO is still in the SkunkWorks object table, False if it
' has gone out of range and been purged.
Public Property Get FExists() As Boolean
    FExists = (otable.AcoFromOid(oidMbr) Is Me)
End Property

' Return the name of this object.
Public Property Get szName() As String
Attribute szName.VB_UserMemId = 0
Attribute szName.VB_MemberFlags = "200"
    szName = szNameMbr
End Property

' Return the plural form of this object's name (e.g. "Bundles of Arrowshafts").
Public Property Get szPlural() As String
    If szPluralMbr <> "" Then
        szPlural = szPluralMbr
    Else
        szPlural = szName + "s"
    End If
End Property

' Return the equipment coverage mask of this object.
Public Property Get eqm() As Long
    eqm = eqmMbr
End Property

' Return the merchant category of this object as a bit mask.
Public Property Get mcm() As Long
    mcm = mcmMbr
End Property

' Return the string name of this object's merchant category.
Public Property Get szMcm() As String
    szMcm = SzFromMcm(mcmMbr)
End Property

' Return the object category of this object as a bit mask.
Public Property Get ocm() As Long
    
    If mcmMbr = mcmCreature Then
        If (otyMbr And otyPlayer) <> 0 Then
            ocm = ocmPlayer
            If (otyMbr And otyPK) <> 0 Then
                ocm = ocm Or ocmPK
            Else
                ocm = ocm Or ocmNonPK
            End If
        ElseIf (otyMbr And otyMerchant) <> 0 Then
            ocm = ocmNPC Or ocmMerchant
        ElseIf (otyMbr And otySelectable) <> 0 Then
            ocm = ocmMonster
        Else
            ocm = ocmNPC
        End If
        
    ElseIf (otyMbr And otyCorpse) <> 0 Then
    '   REVIEW: How do you tell the difference between player and monster corpses?
        ocm = ocmPlayerCorpse Or ocmMonsterCorpse
        
    ElseIf (otyMbr And otyLifestone) <> 0 Then
        ocm = ocmLifestone
        
    ElseIf (otyMbr And otyPortal) <> 0 Then
        ocm = ocmPortal
        
    ElseIf eqmMbr <> eqmNil Then
        ocm = ocmEquipment
        
    ElseIf iconMbr = iconHook Then
        ocm = ocmHook
        
    Else
        ocm = ocmNil
    End If
    
End Property

' Return the object location category of this object.
Public Property Get olc() As Long
    
    If fInInventory Then
        olc = olcInventory
            
    ElseIf Not (acoContainerMbr Is Nothing) Then
        olc = olcContained
        
    ElseIf Not (acoWearerMbr Is Nothing) Then
        olc = olcEquipped
        
    ElseIf Not (maplocMbr Is Nothing) Then
        olc = olcOnGround
        
    Else
        olc = olcNil
    End If
    
End Property

' Return the object type of this object.
Public Property Get oty() As Long
    oty = otyMbr
End Property

' Return the value of this object.  If the object is a stack, this is
' the total value of the whole stack.
Public Property Get cpyValue() As Long
    cpyValue = cpyValueMbr
End Property

' Return the material code of the stuff this object is made of.
Public Property Get material() As Long
    material = materialMbr
End Property

' Return the workmanship quality of this item.
Public Property Get workmanship() As Single
    workmanship = workmanshipMbr
End Property

' Return the number of items in this stack.  If the object is not stackable,
' this will be one.
Public Property Get citemStack() As Long
    citemStack = citemStackMbr
End Property

' Return the maximum number of items that can be stacked.  If the object is
' not stackable, this will be one.
Public Property Get citemMaxStack() As Long
    citemMaxStack = citemMaxStackMbr
End Property

' Return the number of uses remaining for this item (such as a key or heal kit).
Public Property Get cuseLeft() As Long
    cuseLeft = cuseLeftMbr
End Property

' Return the maximum number of uses for an item of this type.
Public Property Get cuseMax() As Long
    cuseMax = cuseMaxMbr
End Property

' Return the map location of this object.  For inventory items, equipped
' items, and other objects that have no location, this will be Nothing.
Public Property Get maploc() As MaplocCls
    Set maploc = maplocMbr
End Property

' The weight of this item (including contents, if any) in burden units.
Public Property Get burden() As Long
    burden = burdenMbr
End Property


' Return True if this item is in the user's inventory, i.e. if it is
' contained in one of the user's packs or is itself one of the user's
' packs (but not the main pack, which is the user himself).
Public Property Get fInInventory() As Boolean
    
    Dim acoPack As AcoCls
    If fContainer Then
        Set acoPack = Me
    Else
        Set acoPack = acoContainerMbr
    End If
    
    If acoPack Is Nothing Then
        fInInventory = False
    Else
        fInInventory = (acoPack.acoContainer Is otable.acoChar)
    End If
    
End Property


' Return the object (such as a chest or pack) that contains this object,
' or Nothing if the object has no container.
Public Property Get acoContainer() As AcoCls
    Set acoContainer = acoContainerMbr
End Property

' Return the (zero-origin) item number of this object within its container,
' or iNil (-1) if the object has no container.
Public Property Get iitem() As Long
    iitem = iitemMbr
End Property


' Return the object that is wearing or wielding this object, or Nothing if the
' object is not being worn or wielded.
Public Property Get acoWearer() As AcoCls
    Set acoWearer = acoWearerMbr
End Property

' Return the equipment slot(s) in which this object is being worn or wielding,
' or eqmNil (zero) if it's not being worn or wielded.
Public Property Get eqmWearer() As Long
    eqmWearer = eqmWearerMbr
End Property


' Obsolete, but must be defined for binary compatibility.
Public Property Get oidNoteSeller() As Long
    oidNoteSeller = oidNil
End Property

' If this object is a player who is a member of an allegiance group, return the
' object ID of the player's monarch, or oidNil otherwise.
Public Property Get oidMonarch() As Long
    oidMonarch = oidMonarchMbr
End Property

' If this object has an associated spell, return the spellid of that spell.
Public Property Get spellid() As Long
    spellid = spellidMbr
End Property

' If this object is part of a merchant's catalog, return the number of such items
' available from that merchant, or -1 if the merchant has an inexhaustible supply.
Public Property Get citemAvail() As Long
    citemAvail = citemAvailMbr
End Property

' The approximate distance in MU that your character will approach this object
' in order to use it.
Public Property Get distApproach() As Single
    distApproach = distApproachMbr
End Property

' Return True if this door or chest is open, False otherwise.
Public Property Get fOpen() As Boolean
    fOpen = fOpenMbr
End Property


' Return True if this object is a container, False otherwise.
Public Property Get fContainer() As Boolean
    fContainer = Not (coacoContentsMbr Is Nothing)
End Property

' Return the maximum number of items that can be contained in this object.
Public Property Get citemMax() As Long
    citemMax = citemMaxMbr
End Property

' Return the number of items actually contained in this object.
Public Property Get citemContents() As Long
    
    If fContainer Then
        citemContents = coacoContentsMbr.count
    Else
        citemContents = 0
    End If
    
End Property

' Return a collection of items contained in this object.
Public Property Get coacoContents() As CoacoCls
    Set coacoContents = coacoContentsMbr
End Property

' Return the maximum number of packs or other containers that can be contained
' in this object.  This will be 7 for players, 0 for everything else.
Public Property Get cpackMax() As Long
    cpackMax = cpackMaxMbr
End Property


Public Property Get oai() As OaiCls
    Set oai = oaiMbr
End Property


'---------
' Methods
'---------

' Return the object contained at position iitem within this object, or Nothing
' if there is no item at that position.
Public Function AcoFromIitem(ByVal iitem As Long) As AcoCls
    
    If iitem < citemContents Then
        Set AcoFromIitem = coacoContentsMbr.Item(iitem)
    Else
        Set AcoFromIitem = Nothing
    End If
    
End Function


' Use this item (which is assumed to be usable on its own, not on another item).
Public Sub Use()
    
    Call skapi.UseAco(Me)
    
End Sub

' Use this item on the item specified by acoTarget.  If omitted, acoTarget
' defaults to the currently selected object.
Public Sub UseOnAco(Optional ByVal acoTarget As AcoCls = Nothing)
    
    Dim acoT As AcoCls
    If IsNull(acoTarget) Then
        Set acoT = Nothing
    Else
        Set acoT = acoTarget
    End If
    
    Call skapi.UseAcoOnAco(Me, acoT)
    
End Sub


' Move this item to the pack specified by ipack, at the position specified by
' iitem.  If fAllowStacking is true, and the item is stackable, then it may
' be merged into an existing stack at that location.  Otherwise no stacking
' takes place.
Public Sub MoveToPack(ByVal ipack As Long, _
  Optional ByVal iitem As Long = 0, Optional ByVal fAllowStacking As Boolean = True)
    
    Call skapi.MoveAcoToPack(Me, otable.AcoFromIpack(ipack), iitem, fAllowStacking)
    
End Sub

' Move this item to the container specified by acoContainer (which need not
' be a pack, but can be a chest or corpse or whatnot), at the position specified by
' iitem.  If fAllowStacking is true, and the item is stackable, then it may
' be merged into an existing stack at that location.  Otherwise no stacking
' takes place.
Public Sub MoveToContainer(ByVal acoContainer As AcoCls, _
  Optional ByVal iitem As Long = 0, Optional ByVal fAllowStacking As Boolean = True)
    
    Call skapi.MoveAcoToPack(Me, acoContainer, iitem, fAllowStacking)
    
End Sub

' Merge this item onto the stack of items specified by acoStack, which must
' be of the same kind as this item.
Public Sub MoveToStack(ByVal acoStack As AcoCls)
    
    Call skapi.MoveAcoEx(Me, acoStack, 0)
    
End Sub

' Add this item to the merchant Buy list (which must already be open).
Public Sub AddToBuyList(Optional ByVal citem As Long = 1)
    
    Call skapi.VendorBuyListAdd(Me, citem)
    
End Sub

' Add this item to the merchant Sell list (which must already be open).
Public Sub AddToSellList()
    
    Call skapi.VendorSellListAdd(Me)
    
End Sub

' Add this item to the Secure Trade window (which must already be open).
Public Sub AddToTrade()
    
    Call skapi.AddAcoToTrade(Me)
    
End Sub

' Add this item to the Ust window (which must already be open).
Public Sub AddToUst()
    
    Call skapi.AddAcoToUst(Me)
    
End Sub

' Drop this item on the ground.
Public Sub Drop()
    
'    Call skapi.MoveAcoEx(Me, Nothing, 1)
    Call skapi.DropAco(Me)
    
End Sub

' Give this item to the item specified by acoTarget.  If omitted, acoTarget
' defaults to the currently selected object.
Public Sub GiveToAco(Optional ByVal acoTarget As AcoCls = Nothing)
    
    If IsNull(acoTarget) Then
        Set acoTarget = skapi.acoSelected
    ElseIf acoTarget Is Nothing Then
        Set acoTarget = skapi.acoSelected
    End If
    
    If Not acoTarget Is Nothing Then
        Call skapi.GiveAcoToAco(Me, acoTarget)
    End If
    
End Sub


Public Sub Tell(ByVal szMsg As String)
    
    Call skapi.ChatEx(0, szNameMbr, szMsg)
    
End Sub


'-----------------------------------
' ACScript compatibility properties
'-----------------------------------

Public Property Get Name() As String
    Name = szName
End Property

Public Property Get Name2() As String
    Name2 = szPlural
End Property

Public Property Get LongName() As String
    LongName = szPlural
End Property

Public Property Get Location() As Variant
    If maplocMbr Is Nothing Then
        Location = Empty
    Else
        Location = maplocMbr.rgv
    End If
End Property

' Can't call this Type because that's a VB reserved word.
Public Property Get Type_() As Long
'   ACScript overloads obj.Type to mean two different things.  For regular
'   inventory objects, it's oty.  For objects in the collection passed to
'   OnApproachVendor, it's mcm.
    If fMerchantItem Then
        Type_ = mcm
    Else
        Type_ = oty
    End If
End Property

Public Property Get TypeName() As String
    TypeName = szMcm
End Property

Public Property Get Holder() As Long
    
    Select Case eqmWearer
    Case eqmWeapon, eqmFocusWeapon, eqmRangedWeapon, eqmShield
        Holder = acoWearer.oid
    Case Else
        Holder = oidNil
    End Select
    
End Property

Public Property Get HolderSlot() As Long
    
    Select Case eqmWearer
    Case eqmWeapon, eqmFocusWeapon
        HolderSlot = slotMeleeWeapon
    Case eqmRangedWeapon
        HolderSlot = slotMissileWeapon
    Case eqmShield
        HolderSlot = slotShield
    Case Else
        HolderSlot = slotNil
    End Select
    
End Property

Public Property Get Value() As Long
    Value = cpyValue
End Property

Public Property Get TotalValue() As Long
'   Not sure what ACScript does here, but I don't think it has anything to do with value.
    TotalValue = 0 ' dwTotalValueMbr
End Property

Public Property Get StackCount() As Long
'   ACScript returns 0, not 1, for unstackable items.
    If citemMaxStack = 1 Then
        StackCount = 0
    Else
        StackCount = citemStack
    End If
End Property

Public Property Get StackMax() As Long
'   ACScript returns 0, not 1, for unstackable items.
    If citemMaxStack = 1 Then
        StackMax = 0
    Else
        StackMax = citemMaxStack
    End If
End Property

Public Property Get UsesLeft() As Long
    UsesLeft = cuseLeft
End Property

Public Property Get UsesMax() As Long
    UsesMax = cuseMax
End Property

Public Property Get Container() As Long
    If acoContainer Is Nothing Then
        Container = oidNil
    Else
        Container = acoContainer.oid
    End If
End Property

Public Property Get ObjectID() As Long
    ObjectID = oid
End Property

Public Property Get Owner() As Long
    If acoWearer Is Nothing Then
        Owner = oidNil
    Else
        Owner = acoWearer.oid
    End If
End Property

Public Property Get model() As Long
    model = modelMbr
End Property

Public Property Get icon() As Long
    icon = iconMbr
End Property

Public Property Get iconOverlay() As Long
    iconOverlay = iconOverlayMbr
End Property

Public Property Get iconUnderlay() As Long
    iconUnderlay = iconUnderlayMbr
End Property

Public Property Get ioc() As Long
    ioc = iocMbr
End Property

Public Property Get Exists() As Boolean
    Exists = FExists
End Property


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------

Friend Sub Init(ByVal oid As Long)
    oidMbr = oid
End Sub

Friend Sub SetDwordProp(ByVal lkey As Long, ByVal dwVal As Long, _
  Optional ByVal fFireEvent As Boolean = False)
    Call TraceEnter("Aco.SetDwordProp", _
      SzHex(lkey, 2) + ", " + CStr(dwVal) + ", " + CStr(fFireEvent))
    
    Select Case lkey
    Case lkeyIoc
        iocMbr = dwVal
    Case lkeyValue
        cpyValueMbr = dwVal
    Case lkeyCuseLeft
        If cuseMaxMbr > 0 Then
        '   REVIEW: Is this covered by the previous case?
            cpyValueMbr = cpyValueMbr * dwVal / cuseLeftMbr
            cuseLeftMbr = dwVal
        End If
    Case lkeyCharType
        Select Case dwVal
        Case &H2&   ' NPK
            otyMbr = otyMbr And Not (otyPK Or otyPKL)
        Case &H4&   ' PK
            otyMbr = (otyMbr And Not otyPKL) Or otyPK
        Case &H40&  ' PKL
            otyMbr = (otyMbr And Not otyPK) Or otyPKL
        End Select
    End Select
    
    Call TraceExit("Aco.SetDwordProp")
End Sub

Friend Sub SetQwordProp(ByVal qkey As Long, ByVal qVal As Double, _
  Optional ByVal fFireEvent As Boolean = False)
    Call TraceEnter("Aco.SetQwordProp", _
      SzHex(qkey, 2) + ", " + CStr(qVal) + ", " + CStr(fFireEvent))
    
'   No interesting QWORD properties (yet).
    Select Case qkey
    End Select
    
    Call TraceExit("Aco.SetQwordProp")
End Sub

Friend Sub SetBoolProp(ByVal bkey As Long, ByVal fVal As Boolean)
    Call TraceEnter("Aco.SetBoolProp", SzHex(bkey, 2) + ", " + CStr(fVal))
    
'   No interesting Boolean properties (yet).
    Select Case bkey
    End Select
    
    Call TraceExit("Aco.SetBoolProp")
End Sub

Friend Sub SetDblProp(ByVal dkey As Long, ByVal dblVal As Double)
    Call TraceEnter("Aco.SetDblProp", SzHex(dkey, 2) + ", " + CStr(dblVal))
    
'   No interesting Double properties (yet).
    Select Case dkey
    End Select
    
    Call TraceExit("Aco.SetDblProp")
End Sub

Friend Sub SetSzProp(ByVal skey As Long, ByVal szVal As String)
    Call TraceEnter("Aco.SetSzProp", SzHex(skey, 2) + ", " + SzQuote(szVal))
    
'   No interesting String properties (yet).
    Select Case skey
    End Select
    
    Call TraceExit("Aco.SetSzProp")
End Sub

Friend Sub SetResourceProp(ByVal rkey As Long, ByVal dwVal As Long, _
  Optional ByVal fFireEvent As Boolean = False)
    Call TraceEnter("Aco.SetResourceProp", _
      SzHex(rkey, 2) + ", " + CStr(dwVal) + ", " + CStr(fFireEvent))
    
    Select Case rkey
    Case rkeyIcon
        iconMbr = dwVal
    End Select
    
    Call TraceExit("Aco.SetResourceProp")
End Sub

Friend Sub SetLinkProp(ByVal akey As Long, ByVal acoVal As AcoCls, _
  Optional ByVal fFireEvent As Boolean = False)
    Call TraceEnter("Aco.SetLinkProp", _
      SzHex(akey, 2) + ", " + SzTraceAco(acoVal) + ", " + CStr(fFireEvent))
    
    Select Case akey
        
    Case akeyContainer
    '   Don't set the container yet if it's going into inventory.
    '   Let Insert Inventory Item do that.
    '   This fixes a bug in which items were not being properly removed
    '   from the main pack when moved to a side pack.
        If acoVal Is Nothing Then
            Call SetAcoContainer(acoVal)
        ElseIf Not acoVal.fContainer Then
        '   New container is an NPC.  Set it to Nothing instead.
            Call SetAcoContainer(Nothing)
        ElseIf Not acoVal.fInInventory Then
            Call SetAcoContainer(acoVal)
        ElseIf acoVal Is otable.acoChar And Not fInInventory And _
          (otyMbr And otyContainer) = 0 Then
        '   Fix for Trade bug: implicit insert into main pack.
            Call Remove
            Call acoVal.InsertItem(0, Me)
        End If
        
    Case akeyEquippedBy
        Call SetAcoWearer(acoVal)
        
    End Select
    
    Call TraceExit("Aco.SetLinkProp")
End Sub


Friend Sub SetSzName(ByVal szName As String)
    szNameMbr = szName
End Sub

Friend Sub SetMcm(ByVal mcm As Long)
    mcmMbr = mcm
End Sub

Friend Sub SetOty(ByVal oty As Long)
    otyMbr = oty
End Sub

Friend Sub SetModel(ByVal model As Long)
    modelMbr = model
End Sub

Friend Sub SetIcon(ByVal icon As Long)
    iconMbr = icon
End Sub

Friend Sub SetIconOverlay(ByVal icon As Long)
    iconOverlayMbr = icon
End Sub

Friend Sub SetIconUnderlay(ByVal icon As Long)
    iconUnderlayMbr = icon
End Sub

Friend Sub SetIoc(ByVal ioc As Long)
    iocMbr = ioc
End Sub

Friend Sub SetEqm(ByVal eqm As Long)
    eqmMbr = eqm
End Sub

Friend Sub SetSzPlural(ByVal sz As String)
    szPluralMbr = sz
End Sub

Friend Sub SetCpyValue(ByVal cpyValue As Long)
    cpyValueMbr = cpyValue
End Sub

Friend Sub SetMaterial(ByVal material As Long)
    materialMbr = material
End Sub

Friend Sub SetWorkmanship(ByVal workmanship As Single)
    workmanshipMbr = workmanship
End Sub

Friend Sub SetCitemStack(ByVal citemStack As Long)
    citemStackMbr = citemStack
End Sub

Friend Sub SetCitemMaxStack(ByVal citemMaxStack As Long)
    citemMaxStackMbr = citemMaxStack
End Sub

Friend Sub SetCuseLeft(ByVal cuseLeft As Long)
    cuseLeftMbr = cuseLeft
End Sub

Friend Sub SetCuseMax(ByVal cuseMax As Long)
    cuseMaxMbr = cuseMax
End Sub

Friend Sub SetMaploc(ByVal maplocNew As MaplocCls)
    Set maplocMbr = maplocNew
End Sub

Friend Sub SetBurden(ByVal burden As Single)
    burdenMbr = burden
End Sub

Friend Sub SetAcoContainer(aco As AcoCls)
    
    If aco Is Nothing And Not (acoContainerMbr Is Nothing) And iitemMbr <> iNil Then
        If fContainer Then
            Call otable.RemovePack(iitemMbr)
        Else
            Call acoContainerMbr.RemoveItem(iitemMbr)
        End If
    End If
    
    Set acoContainerMbr = aco
    
End Sub

Friend Sub SetIitem(ByVal acoContainer As AcoCls, ByVal iitem As Long)
    Set acoContainerMbr = acoContainer
    iitemMbr = iitem
End Sub

Friend Sub SetAcoWearer(ByVal aco As AcoCls)
    
    If aco Is Nothing Then
        If acoWearerMbr Is otable.acoChar Then
            Call otable.RemoveEquipment(Me)
            eqmWearerMbr = eqmNil
        End If
    Else
        Set maplocMbr = Nothing
    End If
    
    Set acoWearerMbr = aco
    
End Sub

Friend Sub SetEqmWearer(ByVal eqm As Long)
    eqmWearerMbr = eqm
End Sub


Friend Sub SetOidMonarch(ByVal oid As Long)
    oidMonarchMbr = oid
End Sub

Friend Sub SetSpellid(ByVal spellid As Long)
    spellidMbr = spellid
End Sub

Friend Sub SetCitemAvail(ByVal citem As Long)
    citemAvailMbr = citem
End Sub

Friend Sub SetDistApproach(ByVal dist As Single)
    distApproachMbr = dist
End Sub

Friend Sub SetFOpen(ByVal fOpen As Boolean)
    fOpenMbr = fOpen
End Sub


'' Meaningless number provided solely for ACScript compatibility.
'Friend Sub SetDwTotalValue(ByVal dw As Long)
'    dwTotalValueMbr = dw
'End Sub


' Return True if this item is part of a merchant's catalog, False otherwise.
' Note that this can change as the item is bought or sold.
Friend Function fMerchantItem() As Boolean
    If acoContainerMbr Is Nothing Then
        fMerchantItem = False
    Else
        fMerchantItem = (acoContainerMbr.oty And otyMerchant) <> 0
    End If
End Function


Friend Sub SetFContainer(ByVal fContainer As Boolean)
    
    If fContainer Then
        If coacoContentsMbr Is Nothing Then
            Set coacoContentsMbr = New CoacoCls
        End If
    Else
        Set coacoContentsMbr = Nothing
    End If
    
End Sub

Friend Sub SetCitemMax(ByVal citem As Long)
    citemMaxMbr = citem
End Sub

Friend Sub SetCpackMax(ByVal cpack As Long)
    cpackMaxMbr = cpack
End Sub


Friend Sub FromGameData(ByVal gd As GameDataCls)
    
    szNameMbr = gd.szName
    modelMbr = gd.model
    iconMbr = gd.icon
    iconOverlayMbr = gd.iconOverlay
    iconUnderlayMbr = gd.iconUnderlay
    mcmMbr = gd.mcm
    otyMbr = gd.oty
    szPluralMbr = gd.szPlural
    citemMaxMbr = gd.citemMax
    cpackMaxMbr = gd.cpackMax
    cpyValueMbr = gd.cpyValue
    distApproachMbr = gd.distApproach / 240
    iocMbr = gd.ioc
    cuseLeftMbr = gd.cuseLeft
    cuseMaxMbr = gd.cuseMax
    citemStackMbr = gd.citemStack
    citemMaxStackMbr = gd.citemMaxStack
    eqmMbr = gd.eqm
    eqmWearerMbr = gd.eqmWearer
    workmanshipMbr = gd.workmanship
    burdenMbr = gd.burden
    spellidMbr = gd.spellid
    oidMonarchMbr = gd.oidMonarch
    materialMbr = gd.material
    
    If Not gd.acoContainer Is Nothing Then
        Call SetAcoContainer(gd.acoContainer)
    End If
    If Not gd.acoWearer Is Nothing Then
        Call SetAcoWearer(gd.acoWearer)
    End If
    
End Sub


' Insert an object at position iitem in this container.
Friend Sub InsertItem(ByVal iitem As Long, ByVal aco As AcoCls)
    
    Call Assert(fContainer, "Not a container.")
    
    If iitem < coacoContentsMbr.count Then
        Call coacoContentsMbr.AddAcoF(aco, iitem)
        Dim iitemT As Long
        For iitemT = iitem + 1 To coacoContentsMbr.count - 1
            Dim acoT As AcoCls
            Set acoT = coacoContentsMbr.Item(iitemT)
            Call acoT.SetIitem(Me, iitemT)
        Next iitemT
    Else
        Call coacoContentsMbr.AddAcoF(aco)
    End If
    
    If Not otable.acoChar Is Me And Not aco.acoContainer Is Me Then
        burdenMbr = burdenMbr + aco.burden
    End If
    Call aco.SetIitem(Me, iitem)
    
End Sub

' Remove an object at position iitem in this container.
Friend Sub RemoveItem(ByVal iitem As Long)
    
    Call Assert(fContainer, "Not a container.")
    
    Dim acoItem As AcoCls
    Set acoItem = coacoContentsMbr.Item(iitem)
    
    Call coacoContentsMbr.RemoveIitemF(iitem)
    Dim iitemT As Long
    For iitemT = iitem To coacoContentsMbr.count - 1
        Dim acoT As AcoCls
        Set acoT = coacoContentsMbr.Item(iitemT)
        Call acoT.SetIitem(Me, iitemT)
    Next iitemT
    
    If Not otable.acoChar Is Me Then
        burdenMbr = burdenMbr - acoItem.burden
    End If
    Call acoItem.SetIitem(Nothing, iNil)
    
End Sub

' Remove all objects in this container.
Friend Sub RemoveAll()
    
    Call Assert(fContainer, "Not a container.")
    
    If coacoContentsMbr.count > 0 Then
        Dim iitem As Long
        For iitem = 0 To coacoContentsMbr.count - 1
            Dim aco As AcoCls
            Set aco = coacoContentsMbr.Item(iitem)
            Call aco.SetIitem(Nothing, iNil)
        Next iitem
        
        Set coacoContentsMbr = New CoacoCls
    End If
    
End Sub


' Remove this item from its container or wearer/wielder.
Friend Sub Remove()
    
    If Not (acoContainer Is Nothing) And iitem <> iNil Then
        Call acoContainer.RemoveItem(iitem)
    End If
    
    If Not (acoWearerMbr Is Nothing) Then
        If acoWearerMbr Is otable.acoChar Then
            Call otable.RemoveEquipment(Me)
        End If
        Set acoWearerMbr = Nothing
        eqmWearerMbr = eqmNil
    End If
    
End Sub


Friend Sub SetOai(ByVal oai As OaiCls)
    Set oaiMbr = oai
End Sub


' Return True if this object is in the Secure Trade window.
Friend Function fTradeItem() As Boolean
    fTradeItem = fTradeItemMbr
End Function

Friend Sub SetFTradeItem(ByVal fNew As Boolean)
    fTradeItemMbr = fNew
End Sub


' Return True if this object has been referred to but not explicitly created yet.
Friend Function fPending() As Boolean
    fPending = fPendingMbr
End Function

' The Create Object packet for this object has been seen; clear the Pending flag.
Friend Sub ClearFPending()
    fPendingMbr = False
End Sub


' Return any pending message for this object.
Friend Function mbufPending() As MbufCls
    Set mbufPending = mbufPendingMbr
End Function

' Set the pending message for this object.
Friend Sub SetMbufPending(ByVal mbuf As MbufCls)
    Set mbufPendingMbr = mbuf
End Sub


' Return True if an Insert Inventory Item packet has been seen for this object
' before the Create Object packet was seen.
Friend Function FPendingInsert() As Boolean
    FPendingInsert = fPendingInsertMbr
End Function

' An Insert Inventory Item packet has been seen for this object before the
' Create Object packet was seen.  Make a note of it.
Friend Sub SetFPendingInsert()
    fPendingInsertMbr = True
End Sub

' The Create Object packet has been seen; clear the PendingInsert flag.
Friend Sub ClearFPendingInsert()
    fPendingInsertMbr = False
End Sub


' Return the geographical location of this object.
' If the item has no location of its own, but is being carried or worn
' by something with a location, return that location.
Friend Function MaplocTrans() As MaplocCls
    
    If Not (maplocMbr Is Nothing) Then
        Set MaplocTrans = maplocMbr
    ElseIf Not (acoContainerMbr Is Nothing) Then
        Set MaplocTrans = acoContainerMbr.MaplocTrans
    ElseIf Not (acoWearerMbr Is Nothing) Then
        Set MaplocTrans = acoWearerMbr.MaplocTrans
    Else
    '   Call TraceLine(szNameMbr + " (" + SzHex(oidMbr, 8) + ") isn't anywhere!")
        Set MaplocTrans = Nothing
    End If
    
End Function


Friend Function fOrphaned() As Boolean
    fOrphaned = fOrphanedMbr
End Function

Friend Function SetfOrphaned(ByVal fNew As Boolean)
    fOrphanedMbr = fNew
End Function


' This ACO is about to be deleted.  Break connections to other ACOs for better garbage collection.
Friend Function PrepForDestroy()
    
    Set acoContainerMbr = Nothing
    iitemMbr = iNil
    Set acoWearerMbr = Nothing
    eqmWearerMbr = eqmNil
    Set coacoContentsMbr = Nothing
    
End Function


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
'   Keep count of the outstanding ACOs for memory-leak detection.
    MemStats.caco = MemStats.caco + 1
    
    oidMbr = oidNil
    citemStackMbr = 1
    citemMaxStackMbr = 1
    iitemMbr = iNil
    
    citemAvailMbr = 1
    distApproachMbr = 0
    fOpenMbr = False
    
    fTradeItemMbr = False
    
    fPendingMbr = True
    fPendingInsertMbr = False
    
    fOrphanedMbr = False
    
End Sub

Private Sub Class_Terminate()
    
'   Keep count of the outstanding ACOs for memory-leak detection.
    MemStats.caco = MemStats.caco - 1
    
End Sub

Private Function SzTraceAco(ByVal aco As AcoCls) As String
    
    If aco Is Nothing Then
        SzTraceAco = "Nothing"
    Else
        SzTraceAco = SzQuote(aco.szName)
    End If
    
End Function



















