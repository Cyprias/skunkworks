VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AcoEmptyCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

' Dummy ACO for use by ac.GetObject(oidNil).  All properties are undefined.


'-----------------------------------
' ACScript compatibility properties
'-----------------------------------

Public Property Get Name() As Variant
    Name = Empty
End Property

Public Property Get Name2() As Variant
    Name2 = Empty
End Property

Public Property Get LongName() As Variant
    LongName = Empty
End Property

Public Property Get Location() As Variant
    Location = Empty
End Property

' Can't call this Type because that's a VB reserved word.
Public Property Get Type_() As Variant
    Type_ = Empty
End Property

Public Property Get TypeName() As Variant
    TypeName = Empty
End Property

Public Property Get Holder() As Variant
    Holder = Empty
End Property

Public Property Get HolderSlot() As Variant
    HolderSlot = Empty
End Property

Public Property Get Value() As Variant
    Value = Empty
End Property

Public Property Get TotalValue() As Variant
    TotalValue = Empty
End Property

Public Property Get StackCount() As Variant
    StackCount = Empty
End Property

Public Property Get StackMax() As Variant
    StackMax = Empty
End Property

Public Property Get UsesLeft() As Variant
    UsesLeft = Empty
End Property

Public Property Get UsesMax() As Variant
    UsesMax = Empty
End Property

Public Property Get Container() As Variant
    Container = Empty
End Property

Public Property Get ObjectID() As Variant
    ObjectID = Empty
End Property

Public Property Get Owner() As Variant
    Owner = Empty
End Property

Public Property Get model() As Variant
    model = Empty
End Property

Public Property Get icon() As Variant
    icon = Empty
End Property

Public Property Get Exists() As Variant
    Exists = Empty
End Property



