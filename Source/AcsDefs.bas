Attribute VB_Name = "AcsDefs"
Option Explicit
' ACScript API definitions.


' ConnectedToAC bit masks:
Public Const ctacConnected = &H1&
Public Const ctacCachedLogin = &H2&
Public Const ctacRealLogin = &H4&
Public Const ctacFullInventory = &H8&
Public Const ctacInventoryFromCache = &H10&
Public Const ctacObjectsFromCache = &H20&


Public Const evidMaxACS = 63


' OnAssessCreature array indices:
Public Const ifInfoCbi = 0
Public Const ifInfoCai = 1
Public Const ifInfoChi = 2

' Creature Basic Information:
Public Const cbiLvl = 0
Public Const cbiHealthCur = 1
Public Const cbiHealthMax = 2

' Creature Attribute Information:
Public Const caiStrength = 0
Public Const caiEndurance = 1
Public Const caiQuickness = 2
Public Const caiCoordination = 3
Public Const caiFocus = 4
Public Const caiSelf = 5
Public Const caiStaminaCur = 6
Public Const caiManaCur = 7
Public Const caiStaminaMax = 8
Public Const caiManaMax = 9

' Creature Human Information:
Public Const chiRank = 0
Public Const chiCfollower = 1
Public Const chiLoyalty = 2
Public Const chiLeadership = 3
Public Const chiPK = 4
Public Const chiSzGender = 5
Public Const chiSzRace = 6
Public Const chiSzProfession = 7
Public Const chiSzFellowship = 8
Public Const chiSzMonarch = 9
Public Const chiSzPatron = 10


' OnAssessItem array indices:
Public Const ifInfoIbi = 1
Public Const ifInfoIai = 2
Public Const ifInfoIwi = 3
Public Const ifInfoIei = 4
Public Const ifInfoIpi = 5

' Item Basic Information:
Public Const ibiOty = 0
Public Const ibiCpyValue = 1
Public Const ibiBurden = 2
Public Const ibiCpageTotal = 3
Public Const ibiCpageUsed = 4
Public Const ibiFOpen = 5
Public Const ibiSzInscription = 6
Public Const ibiSzInscriber = 7
Public Const ibiSzUnknown = 8
Public Const ibiSzDescDetailed = 9
Public Const ibiSzDescSimple = 10
Public Const ibiSzComment = 11
Public Const ibiFractEfficiency = 12
Public Const ibiManaCur = 13

' Item Armor Information:
Public Const iaiSlashing = 0
Public Const iaiPiercing = 1
Public Const iaiBludgeoning = 2
Public Const iaiFire = 3
Public Const iaiAcid = 4
Public Const iaiCold = 5
Public Const iaiElectrical = 6
Public Const iaiArmorLevel = 7

' Item Weapon Information:
Public Const iwiDmty = 0
Public Const iwiSpeed = 1
Public Const iwiSkid = 2
Public Const iwiDamageMax = 3
Public Const iwiDamageRange = 4
Public Const iwiDamageBonus = 5
Public Const iwiUnknown = 6
Public Const iwiDefenseBonus = 7
Public Const iwiAttackBonus = 8
Public Const iwiHighlights = 9

' Item Enchantment Information:
Public Const ieiDifficulty = 0
Public Const ieiSpellcraft = 1
Public Const ieiRankReq = 2
Public Const ieiManaCur = 3
Public Const ieiManaMax = 4
Public Const ieiUnknown = 5
Public Const ieiCsecPerMana = 6
Public Const ieiSzRaceReq = 7
Public Const ieiSklvlReq = 8
Public Const ieiSkidReq = 9
Public Const ieiSzSpellNames = 10
Public Const ieiSzSpellDesc = 11
Public Const ieiFractEfficiency = 12

' Item Portal Information:
Public Const ipiLvlMin = 0
Public Const ipiLvlMax = 1
Public Const ipiSzDest = 2

















