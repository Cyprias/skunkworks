VERSION 1.0 CLASS
BEGIN
    MultiUse = -1  'True
    Persistable = 0  'NotPersistable
    DataBindingBehavior = 0  'vbNone
    DataSourceBehavior  = 0  'vbNone
    MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AcshCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' ACSH: ACScript Event Handler.
' This is a wrapper class that connects ACScript-style event handlers to
' the native skapi event API.

' -------------------------------------------------------------------
' !!!!!!!!!!!!!!!!!!!!!!   R E A D   T H I S   !!!!!!!!!!!!!!!!!!!!!!
' -------------------------------------------------------------------
' !!!     This file is generated automatically by Events.xsl.     !!!
' !!!    Any edits you make to it by hand WILL be overwritten.    !!!
' !!!          Make your changes to Events.xml instead.           !!!
' -------------------------------------------------------------------

'------------
' Properties
'------------

Public szHandler As String      ' Name of handler function.
Public fStandard As Boolean     ' True if this is a standard ACMsg_* handler.

'----------
' Handlers
'----------

Public Sub OnActionComplete()
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod)
    Call CheckForError
End Sub

Public Sub OnAddToInventory(ByVal aco As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.oid, aco.szName, OidFromAco(aco.acoContainer), aco.iitem)
    Call CheckForError
End Sub

Public Sub OnAdjustStack(ByVal aco As AcoCls, ByVal citemPrev As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.oid, aco.szName, aco.citemStack)
    Call CheckForError
End Sub

Public Sub OnAnimationSelf(ByVal dwA As Long, ByVal dwB As Long, ByVal dwC As Long, ByVal dwD As Long, ByVal dwE As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, dwA, dwB, dwC, dwD, dwE)
    Call CheckForError
End Sub

Public Sub OnApproachVendor(ByVal acoMerchant As AcoCls, ByVal mcmBuy As Long, ByVal cpyMaxBuy As Long, ByVal fractBuy As Single, ByVal fractSell As Single, ByVal coaco As CoacoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, acoMerchant.oid, fractBuy, fractSell, coaco)
    Call CheckForError
End Sub

Public Sub OnAssessCreature(ByVal aco As AcoCls, ByVal fSuccess As Boolean, ByVal cbi As CbiCls, ByVal cai As CaiCls, ByVal chi As ChiCls)
    On Error Resume Next
    Dim rgfInfo, dncbi, dncai, dnchi
    ReDim rgfInfo(2)
    If Not (cbi Is Nothing) Then
        rgfInfo(ifInfoCbi) = 1
        ReDim dncbi(2)
        dncbi(cbiLvl) = cbi.lvl
        dncbi(cbiHealthCur) = cbi.healthCur
        dncbi(cbiHealthMax) = cbi.healthMax
    End If
    If Not (cai Is Nothing) Then
        rgfInfo(ifInfoCai) = 1
        ReDim dncai(9)
        dncai(caiStrength) = cai.strength
        dncai(caiEndurance) = cai.endurance
        dncai(caiQuickness) = cai.quickness
        dncai(caiCoordination) = cai.coordination
        dncai(caiFocus) = cai.focus
        dncai(caiSelf) = cai.self
        dncai(caiStaminaCur) = cai.staminaCur
        dncai(caiManaCur) = cai.manaCur
        dncai(caiStaminaMax) = cai.staminaMax
        dncai(caiManaMax) = cai.manaMax
    End If
    If Not (chi Is Nothing) Then
        rgfInfo(ifInfoChi) = 1
        ReDim dnchi(10)
        dnchi(chiRank) = chi.rank
        dnchi(chiCfollower) = chi.cfollower
        dnchi(chiLoyalty) = chi.loyalty
        dnchi(chiLeadership) = chi.leadership
        dnchi(chiPK) = chi.fPK
        dnchi(chiSzGender) = chi.szGender
        dnchi(chiSzRace) = chi.szRace
        dnchi(chiSzProfession) = chi.szProfession
        dnchi(chiSzFellowship) = chi.szFellowship
        dnchi(chiSzMonarch) = chi.szMonarch
        dnchi(chiSzPatron) = chi.szPatron
    End If
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.oid, fSuccess, aco.szName, rgfInfo, dncbi, dncai, dnchi)
    Call CheckForError
End Sub

Public Sub OnAssessItem(ByVal aco As AcoCls, ByVal fSuccess As Boolean, ByVal ibi As IbiCls, ByVal iai As IaiCls, ByVal iwi As IwiCls, ByVal iei As IeiCls, ByVal ipi As IpiCls)
    On Error Resume Next
    Dim rgfInfo, dnibi, dniai, dniwi, dniei, dnipi
    ReDim rgfInfo(5)
    If Not (ibi Is Nothing) Then
        rgfInfo(0) = ibi.flags
        rgfInfo(ifInfoIbi) = 1
        ReDim dnibi(13)
        dnibi(ibiOty) = ibi.oty
        dnibi(ibiCpyValue) = ibi.cpyValue
        dnibi(ibiBurden) = ibi.burden
        dnibi(ibiFOpen) = ibi.fOpen
        dnibi(ibiCpageTotal) = ibi.cpageTotal
        dnibi(ibiCpageUsed) = ibi.cpageUsed
        dnibi(ibiSzInscription) = ibi.szInscription
        dnibi(ibiSzInscriber) = ibi.szInscriber
        dnibi(ibiSzUnknown) = ibi.szUnknown
        dnibi(ibiSzDescDetailed) = ibi.szDescDetailed
        dnibi(ibiSzDescSimple) = ibi.szDescSimple
        dnibi(ibiSzComment) = ibi.szComment
        dnibi(ibiFractEfficiency) = ibi.fractEfficiency
        dnibi(ibiManaCur) = ibi.manaCur
    End If
    If Not (iai Is Nothing) Then
        rgfInfo(ifInfoIai) = 1
        ReDim dniai(7)
        dniai(iaiSlashing) = iai.protSlashing
        dniai(iaiPiercing) = iai.protPiercing
        dniai(iaiBludgeoning) = iai.protBludgeoning
        dniai(iaiCold) = iai.protCold
        dniai(iaiFire) = iai.protFire
        dniai(iaiAcid) = iai.protAcid
        dniai(iaiElectrical) = iai.protElectrical
        dniai(iaiArmorLevel) = iai.al
    End If
    If Not (iwi Is Nothing) Then
        rgfInfo(ifInfoIwi) = 1
        ReDim dniwi(9)
        dniwi(iwiDmty) = iwi.dmty
        dniwi(iwiSpeed) = iwi.speed
        dniwi(iwiSkid) = iwi.skid
        dniwi(iwiDamageMax) = iwi.dhealth
        dniwi(iwiDamageRange) = iwi.scaleDamageRange
        dniwi(iwiDamageBonus) = iwi.scaleDamageBonus
        dniwi(iwiUnknown) = iwi.fractUnknown
        dniwi(iwiDefenseBonus) = iwi.scaleDefenseBonus
        dniwi(iwiAttackBonus) = iwi.scaleAttackBonus
        dniwi(iwiHighlights) = iwi.dwHighlights
    End If
    If Not (iei Is Nothing) Then
        rgfInfo(ifInfoIei) = 1
        ReDim dniei(12)
        dniei(ieiDifficulty) = iei.difficulty
        dniei(ieiSpellcraft) = iei.spellcraft
        dniei(ieiRankReq) = iei.rankReq
        dniei(ieiManaCur) = iei.manaCur
        dniei(ieiManaMax) = iei.manaMax
        dniei(ieiUnknown) = iei.dwUnknown
        dniei(ieiCsecPerMana) = iei.csecPerMana
        dniei(ieiSzRaceReq) = iei.szRaceReq
        dniei(ieiSklvlReq) = iei.sklvlReq
        dniei(ieiSkidReq) = iei.skidReq
        dniei(ieiSzSpellNames) = iei.szSpellNames
        dniei(ieiSzSpellDesc) = iei.szSpellDesc
        dniei(ieiFractEfficiency) = iei.fractEfficiency
    End If
    If Not (ipi Is Nothing) Then
        rgfInfo(ifInfoIpi) = 1
        ReDim dnipi(2)
        dnipi(ipiLvlMin) = ipi.lvlMin
        dnipi(ipiLvlMax) = ipi.lvlMax
        dnipi(ipiSzDest) = ipi.szDest
    End If
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.oid, fSuccess, aco.szName, rgfInfo, dnibi, dniai, dniwi, dniei, dnipi)
    Call CheckForError
End Sub

Public Sub OnChatBoxClick(ByVal aco As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.szName, aco.oid)
    Call CheckForError
End Sub

Public Sub OnChatBoxMessage(ByVal szMsg As String, ByVal cmc As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szMsg, cmc)
    Call CheckForError
End Sub

Public Sub OnChatBroadcast(ByVal acoSender As AcoCls, ByVal szMsg As String, ByVal cmc As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szMsg, acoSender.szName, acoSender.oid, cmc)
    Call CheckForError
End Sub

Public Sub OnChatEmoteCustom(ByVal acoSender As AcoCls, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, acoSender.oid, acoSender.szName, szMsg)
    Call CheckForError
End Sub

Public Sub OnChatEmoteStandard(ByVal acoSender As AcoCls, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, acoSender.oid, acoSender.szName, szMsg)
    Call CheckForError
End Sub

Public Sub OnChatLocal(ByVal acoSender As AcoCls, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szMsg, acoSender.szName, acoSender.oid)
    Call CheckForError
End Sub

Public Sub OnChatServer(ByVal szMsg As String, ByVal cmc As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szMsg, cmc)
    Call CheckForError
End Sub

Public Sub OnChatSpell(ByVal acoSender As AcoCls, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szMsg, acoSender.szName, acoSender.oid)
    Call CheckForError
End Sub

Public Sub OnCloseContainer(ByVal acoContainer As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, acoContainer)
    Call CheckForError
End Sub

Public Sub OnCombatMode(ByVal fOn As Boolean)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, fOn)
    Call CheckForError
End Sub

Public Sub OnCommand(ByVal szCmd As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szCmd)
    Call CheckForError
End Sub

Public Sub OnControlEvent(ByVal szPanel As String, ByVal szControl As String, ByVal dictSzValue As Dictionary)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szPanel, szControl, dictSzValue)
    Call CheckForError
End Sub

Public Sub OnControlProperty(ByVal szPanel As String, ByVal szControl As String, ByVal szProperty As String, ByVal vValue As Variant)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szPanel, szControl, szProperty, vValue)
    Call CheckForError
End Sub

Public Sub OnCraftConfirm(ByVal szOdds As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szOdds)
    Call CheckForError
End Sub

Public Sub OnDeathMessage(ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szMsg)
    Call CheckForError
End Sub

Public Sub OnDeathOther(ByVal acoVictim As AcoCls, ByVal acoKiller As AcoCls, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, acoVictim.oid, OidFromAco(acoKiller), szMsg)
    Call CheckForError
End Sub

Public Sub OnDeathSelf(ByVal acoKiller As AcoCls, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, OidFromAco(acoKiller), szMsg)
    Call CheckForError
End Sub

Public Sub OnDisconnect()
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod)
    Call CheckForError
End Sub

Public Sub OnEnd3D()
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod)
    Call CheckForError
End Sub

Public Sub OnEndPortalOther(ByVal aco As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.oid)
    Call CheckForError
End Sub

Public Sub OnEndPortalSelf()
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod)
    Call CheckForError
End Sub

Public Sub OnFellowCreate(ByVal szName As String, ByVal fShareExp As Boolean, ByVal oidLeader As Long, ByVal cofellow As CofellowCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szName, fShareExp, oidLeader, cofellow)
    Call CheckForError
End Sub

Public Sub OnFellowDisband()
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod)
    Call CheckForError
End Sub

Public Sub OnFellowDismiss(ByVal oid As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, oid)
    Call CheckForError
End Sub

Public Sub OnFellowInvite(ByVal szSender As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szSender)
    Call CheckForError
End Sub

Public Sub OnFellowQuit(ByVal oid As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, oid)
    Call CheckForError
End Sub

Public Sub OnFellowRecruit(ByVal fellow As FellowCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, fellow)
    Call CheckForError
End Sub

Public Sub OnFellowUpdate(ByVal fellow As FellowCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, fellow)
    Call CheckForError
End Sub

Public Sub OnHotkey(ByVal cmid As String, ByVal vk As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, cmid, vk)
    Call CheckForError
End Sub

Public Sub OnItemManaBar(ByVal aco As AcoCls, ByVal fractMana As Single)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco, fractMana)
    Call CheckForError
End Sub

Public Sub OnLeaveVendor(ByVal acoMerchant As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, acoMerchant)
    Call CheckForError
End Sub

Public Sub OnLocChangeCreature(ByVal aco As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco)
    Call CheckForError
End Sub

Public Sub OnLocChangeOther(ByVal aco As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.oid, aco.maploc.landblock, aco.maploc.X, aco.maploc.Y, aco.maploc.z, aco.maploc.head)
    Call CheckForError
End Sub

Public Sub OnLocChangeSelf(ByVal maploc As MaplocCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, maploc.landblock, maploc.X, maploc.Y, maploc.z, maploc.head)
    Call CheckForError
End Sub

Public Sub OnLogon(ByVal acoChar As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, acoChar.oid, acoChar.szName, skapi.healthCur, skapi.staminaCur, skapi.manaCur)
    Call CheckForError
End Sub

Public Sub OnMeleeDamageOther(ByVal szName As String, ByVal dhealth As Long, ByVal dmty As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szName, dhealth)
    Call CheckForError
End Sub

Public Sub OnMeleeDamageSelf(ByVal szName As String, ByVal dhealth As Long, ByVal dmty As Long, ByVal bpart As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szName, dhealth, bpart)
    Call CheckForError
End Sub

Public Sub OnMeleeEvadeOther(ByVal szName As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szName)
    Call CheckForError
End Sub

Public Sub OnMeleeEvadeSelf(ByVal szName As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szName)
    Call CheckForError
End Sub

Public Sub OnMeleeLastAttacker(ByVal aco As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco)
    Call CheckForError
End Sub

Public Sub OnMoveItem(ByVal aco As AcoCls, ByVal acoContainer As AcoCls, ByVal iitem As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco, acoContainer, iitem)
    Call CheckForError
End Sub

Public Sub OnMyPlayerKill(ByVal aco As AcoCls, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.oid, szMsg)
    Call CheckForError
End Sub

Public Sub OnNavStop(ByVal nsc As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, nsc)
    Call CheckForError
End Sub

Public Sub OnObjectCreate(ByVal aco As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.szName, aco.oid, aco.oty)
    Call CheckForError
End Sub

Public Sub OnObjectCreatePlayer(ByVal aco As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.szName, aco.oid)
    Call CheckForError
End Sub

Public Sub OnObjectDestroy(ByVal aco As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco)
    Call CheckForError
End Sub

Public Sub OnOpenContainer(ByVal aco As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.oid, aco.citemContents)
    Call CheckForError
End Sub

Public Sub OnOpenContainerPanel(ByVal acoContainer As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod)
    Call CheckForError
End Sub

Public Sub OnPluginMsg(ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szMsg)
    Call CheckForError
End Sub

Public Sub OnPortalStormed()
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod)
    Call CheckForError
End Sub

Public Sub OnPortalStormWarning(ByVal severity As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, severity)
    Call CheckForError
End Sub

Public Sub OnRemoveFromInventory(ByVal aco As AcoCls, ByVal acoContainer As AcoCls, ByVal iitem As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.oid, aco.szName)
    Call CheckForError
End Sub

Public Sub OnSetFlagOther(ByVal aco As AcoCls, ByVal iflag As Long, ByVal fOn As Boolean)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.oid, iflag, fOn)
    Call CheckForError
End Sub

Public Sub OnSetFlagSelf(ByVal iflag As Long, ByVal fOn As Boolean)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, iflag, fOn)
    Call CheckForError
End Sub

Public Sub OnSpellAdd(ByVal spell As SpellCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, spell)
    Call CheckForError
End Sub

Public Sub OnSpellCastSelf(ByVal spellid As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, spellid)
    Call CheckForError
End Sub

Public Sub OnSpellExpire(ByVal spellid As Long, ByVal layer As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, spellid, layer)
    Call CheckForError
End Sub

Public Sub OnSpellExpireSilent(ByVal spellid As Long, ByVal layer As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, spellid, layer)
    Call CheckForError
End Sub

Public Sub OnSpellFailSelf(ByVal arc As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, arc)
    Call CheckForError
End Sub

Public Sub OnStart3D()
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod)
    Call CheckForError
End Sub

Public Sub OnStartPortalSelf()
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod)
    Call CheckForError
End Sub

Public Sub OnStatLevel(ByVal lvl As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, lvl)
    Call CheckForError
End Sub

Public Sub OnStatSkillExp(ByVal skid As Long, ByVal szSkill As String, ByVal diff As Long, ByVal expTotal As Double, ByVal dsklvl As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, skid, szSkill, diff, expTotal, dsklvl)
    Call CheckForError
End Sub

Public Sub OnStatTotalBurden(ByVal burden As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, burden)
    Call CheckForError
End Sub

Public Sub OnStatTotalExp(ByVal exp As Double)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, exp)
    Call CheckForError
End Sub

Public Sub OnStatTotalPyreals(ByVal cpy As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, cpy)
    Call CheckForError
End Sub

Public Sub OnStatUnspentExp(ByVal exp As Double)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, exp)
    Call CheckForError
End Sub

Public Sub OnTargetHealth(ByVal aco As AcoCls, ByVal fractHealth As Single)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.oid, fractHealth, aco.szName)
    Call CheckForError
End Sub

Public Sub OnTell(ByVal acoSender As AcoCls, ByVal acoReceiver As AcoCls, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, acoSender.szName, szMsg, acoSender.oid, acoReceiver.oid)
    Call CheckForError
End Sub

Public Sub OnTellAllegiance(ByVal szSender As String, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szSender, szMsg)
    Call CheckForError
End Sub

Public Sub OnTellCovassal(ByVal szSender As String, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szSender, szMsg)
    Call CheckForError
End Sub

Public Sub OnTellFellowship(ByVal szSender As String, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szSender, szMsg)
    Call CheckForError
End Sub

Public Sub OnTellFollower(ByVal szSender As String, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szSender, szMsg)
    Call CheckForError
End Sub

Public Sub OnTellMelee(ByVal acoSender As AcoCls, ByVal acoReceiver As AcoCls, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, acoSender.szName, szMsg, acoSender.oid, acoReceiver.oid)
    Call CheckForError
End Sub

Public Sub OnTellMisc(ByVal acoSender As AcoCls, ByVal acoReceiver As AcoCls, ByVal szMsg As String, ByVal cmc As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, acoSender.szName, szMsg, acoSender.oid, acoReceiver.oid, cmc)
    Call CheckForError
End Sub

Public Sub OnTellPatron(ByVal szSender As String, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szSender, szMsg)
    Call CheckForError
End Sub

Public Sub OnTellServer(ByVal acoSender As AcoCls, ByVal acoReceiver As AcoCls, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, acoSender.szName, szMsg, acoSender.oid, acoReceiver.oid)
    Call CheckForError
End Sub

Public Sub OnTellVassal(ByVal szSender As String, ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szSender, szMsg)
    Call CheckForError
End Sub

Public Sub OnTimer(ByVal timer As TimerCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, timer)
    Call CheckForError
End Sub

Public Sub OnTipMessage(ByVal szMsg As String)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, szMsg)
    Call CheckForError
End Sub

Public Sub OnTradeAccept(ByVal aco As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.oid)
    Call CheckForError
End Sub

Public Sub OnTradeAdd(ByVal aco As AcoCls, ByVal side As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.oid, side)
    Call CheckForError
End Sub

Public Sub OnTradeEnd(ByVal arc As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, arc)
    Call CheckForError
End Sub

Public Sub OnTradeReset(ByVal aco As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, aco.oid)
    Call CheckForError
End Sub

Public Sub OnTradeStart(ByVal acoSender As AcoCls, ByVal acoReceiver As AcoCls)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, acoSender.oid, acoReceiver.oid)
    Call CheckForError
End Sub

Public Sub OnVitals(ByVal health As Long, ByVal stamina As Long, ByVal mana As Long)
    On Error Resume Next
    Call CallByName(skapi.objScript, szHandler, VbMethod, health, stamina, mana)
    Call CheckForError
End Sub

'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    MemStats.cacsh = MemStats.cacsh + 1
    
End Sub

Private Sub Class_Terminate()
    
    MemStats.cacsh = MemStats.cacsh - 1
    
End Sub

Private Function OidFromAco(ByVal aco As AcoCls) As Long
    
    If aco Is Nothing Then
        OidFromAco = oidNil
    Else
        OidFromAco = aco.oid
    End If
    
End Function

Private Sub CheckForError()
    
    Dim errno As Long
    errno = Err.Number
    If errno <> 0 Then
        If errno = 438 And fStandard Then
        '   Don't report missing standard handlers.
        ElseIf errno = 440 Then
        '   Already reported by ScriptError event.
        Else
            Call skapi.RaiseHandlerError(szHandler, Err.Description, Err.Number)
        End If
        Call Err.Clear
    End If
    
End Sub


