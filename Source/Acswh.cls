VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AcswhCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' This class implements the Width and Height globals visible to the script.
' Note that the properties and methods here are simply wrappers for native
' APIs implemented by the skapi object.


'--------------
' Private data
'--------------


'------------
' Properties
'------------

Public Property Get Width() As Long
    Width = skapi.dxpRes
End Property

Public Property Get Height() As Long
    Height = skapi.dypRes
End Property


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------












