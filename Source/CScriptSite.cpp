// CScriptSite.c
#include "stdafx.h"
#include "CScriptSite.h"


CScriptSite::CScriptSite()
	  : m_pScript(NULL), m_pParse(NULL), m_fTrace(false)
	{
	if (m_fTrace) TraceEnter("CScriptSite::CScriptSite");
	
	m_pnobjHead = NULL;
	cfile = 0;
	m_hwndHost = NULL;
	m_heventStop = NULL;
	m_heventStopped = NULL;
	m_hthreadStop = NULL;
	m_fInterrupted = false;

	if (m_fTrace) TraceExit("CScriptSite::CScriptSite");
	}

CScriptSite::~CScriptSite()
	{
	if (m_fTrace) TraceEnter("CScriptSite::~CScriptSite");
	
	Close();
	
	if (m_fTrace) TraceExit("CScriptSite::~CScriptSite");
	}

void CScriptSite::Close()
	{
	if (m_fTrace) TraceEnter("CScriptSite::Close");
	
	HRESULT hres;
	
	if (m_pScript != NULL)
		{
	//	This appears to be redundant.
	//	if (m_fTrace) TraceLine("SetScriptState");
	//	hres = m_pScript->SetScriptState(SCRIPTSTATE_DISCONNECTED);
	//	_ASSERTE(SUCCEEDED(hres));
		
		if (m_fTrace) TraceLine("Close");
		hres = m_pScript->Close();
		_ASSERTE(SUCCEEDED(hres));
		
		m_pParse = NULL;
		m_pScript = NULL;
		}
	
	if (m_hthreadStop != NULL)
		{
		SetEvent(m_heventStop);
		m_hthreadStop = NULL;
		}
	if (m_heventStop != NULL)
		{
		CloseHandle(m_heventStop);
		m_heventStop = NULL;
		}
	if (m_heventStopped != NULL)
		{
		CloseHandle(m_heventStopped);
		m_heventStopped = NULL;
		}
	
	while (m_pnobjHead != NULL)
		{
		CNobj* pnobjNext;
		pnobjNext = m_pnobjHead->m_pnobjNext;
		delete m_pnobjHead;
		m_pnobjHead = pnobjNext;
		}
	
	while (cfile > 0)
		{
		--cfile;
		if (rgddh[cfile] != NULL)
			rgddh[cfile]->Detach();
		rgddh[cfile] = NULL;
		rgdwContext[cfile] = 0;
		rgstrFile[cfile] = NULL;
		rgilineFirst[cfile] = 0;
		}
	
	if (m_fTrace) TraceExit("CScriptSite::Close");
	}

STDMETHODIMP CScriptSite::GetItemInfo(LPCOLESTR pstrName, DWORD dwReturnMask, 
  IUnknown **ppunkItem, ITypeInfo **ppTypeInfo)
	{
	USES_CONVERSION;
	if (m_fTrace) TraceEnter("CScriptSite::GetItemInfo(\"%s\", 0x%X)", OLE2T(pstrName), dwReturnMask);
	
	CNobj* pnobj;
	for (pnobj = m_pnobjHead; pnobj != NULL; pnobj = pnobj->m_pnobjNext)
		{
		if (m_fTrace) TraceLine("wcscmp(\"%s\", \"%s\")", 
			OLE2T(pnobj->m_strName), OLE2T(pstrName));
		if(::wcscmp(pnobj->m_strName, pstrName) == 0)
			{
			if (dwReturnMask & SCRIPTINFO_IUNKNOWN)
				{
				if (m_fTrace) TraceLine("return pobj");
				*ppunkItem = pnobj->m_pobj;
				if (*ppunkItem != NULL)
					(*ppunkItem)->AddRef();
				}
			
			if (dwReturnMask & SCRIPTINFO_ITYPEINFO)
				{
				m_cGetTypeinfo++;
				if (m_fTrace)
					{
					TraceLine("return typeinfo");
					TraceLine("cGetTypeinfo = %u", m_cGetTypeinfo);
					}
				
				CComPtr< IProvideClassInfo > pPCI = NULL;
				HRESULT hres = pnobj->m_pobj->QueryInterface(&pPCI);
				if (SUCCEEDED(hres))
					{
					hres = pPCI->GetClassInfo(ppTypeInfo);
					_ASSERTE(SUCCEEDED(hres));
					if (*ppTypeInfo != NULL)
						(*ppTypeInfo)->AddRef();
					}
				else
					*ppTypeInfo = NULL;
				}
			
			if (m_fTrace) TraceExit("CScriptSite::GetItemInfo");
			return S_OK;
			}
		}
	
	if (m_fTrace) TraceExit("CScriptSite::GetItemInfo");
	return TYPE_E_ELEMENTNOTFOUND;
	}

STDMETHODIMP CScriptSite::OnStateChange(SCRIPTSTATE ssScriptState)
	{
	if (m_fTrace) TraceEnter("CScriptSite::OnStateChange");
	
	switch (ssScriptState)
		{
	case SCRIPTSTATE_UNINITIALIZED:
		if (m_fTrace) TraceLine("SCRIPTSTATE_UNINITIALIZED");
		break;
		
	case SCRIPTSTATE_INITIALIZED:
		if (m_fTrace) TraceLine("SCRIPTSTATE_INITIALIZED");
		break;
		
	case SCRIPTSTATE_STARTED:
		if (m_fTrace) TraceLine("SCRIPTSTATE_STARTED");
		break;
		
	case SCRIPTSTATE_CONNECTED:
		if (m_fTrace) TraceLine("SCRIPTSTATE_CONNECTED");
		break;
		
	case SCRIPTSTATE_DISCONNECTED:
		if (m_fTrace) TraceLine("SCRIPTSTATE_DISCONNECTED");
		break;
		
	case SCRIPTSTATE_CLOSED:
		if (m_fTrace) TraceLine("SCRIPTSTATE_CLOSED");
		break;
		
	default:
		if (m_fTrace) TraceLine("Unknown SCRIPTSTATE value" );
		break;
		}
	
	if (m_fTrace) TraceExit("CScriptSite::OnStateChange");
	return S_OK;
	}



