// CScriptSite.h

#define cfileMax	100

class CScriptSite: 
  public IActiveScriptSite, 
  public IActiveScriptSiteWindow, 
  public IActiveScriptSiteDebug
	{
public:
	class CNobj	// Named Object
		{
	public:
		CNobj(BSTR strName, IUnknown *pobj)
		 : m_pnobjNext(NULL), m_strName(strName), m_pobj(pobj)
			{
		//	OutputDebugString("[ CNobj::CNobj\n");
			
		//	OutputDebugString("] CNobj::CNobj\n");
			}
		
	//	~CNobj()
	//		{
	//		OutputDebugString("[ CNobj::~CNobj\n");
	//		
	//		OutputDebugString("] CNobj::~CNobj\n");
	//		}
		
		CNobj* m_pnobjNext;
		_bstr_t m_strName;
		CComPtr< IUnknown > m_pobj;
		};
	
public:
	
	CComPtr< IActiveScript > m_pScript;
	CComPtr< IActiveScriptParse > m_pParse;
	
	CNobj* m_pnobjHead;
	
	HANDLE m_heventStop;
	HANDLE m_heventStopped;
	unsigned long m_hthreadStop;
	BOOL m_fInterrupted;
	
	long m_cGetTypeinfo;
	
	CComPtr< IDebugDocumentHelper > rgddh[cfileMax];
	DWORD rgdwContext[cfileMax];
	BSTR rgstrFile[cfileMax];
	long rgilineFirst[cfileMax];
	DWORD cfile;
	
	HWND m_hwndHost;
	
	BOOL m_fTrace;
	
	CScriptSite();
	~CScriptSite();
	
	void Close();
	DWORD IfileFromDwContext(DWORD dwContext);
	
	void TraceEnter(LPCSTR pszFunc, ...)
		{
		va_list args;
		va_start(args, pszFunc);
		TCHAR sz[1004];
		strcpy(sz, "[ ");
		int cch = _vsntprintf(sz + 2, 1000, pszFunc, args);
		strcpy(sz + 2 + cch, "\n");
		OutputDebugString(sz);
		}
	
	void TraceLine(LPCSTR psz, ...)
		{
		va_list args;
		va_start(args, psz);
		TCHAR sz[1004];
		strcpy(sz, "  ");
		int cch = _vsntprintf(sz + 2, 1000, psz, args);
		strcpy(sz + 2 + cch, "\n");
		OutputDebugString(sz);
		}
	
	void TraceHresult(LPCSTR pszFunc, HRESULT hres)
		{
		TraceLine("%s: %08lX", pszFunc, hres);
		}
	
	void TraceExit(LPCSTR pszFunc, ...)
		{
		va_list args;
		va_start(args, pszFunc);
		TCHAR sz[1004];
		strcpy(sz, "] ");
		int cch = _vsntprintf(sz + 2, 1000, pszFunc, args);
		strcpy(sz + 2 + cch, "\n");
		OutputDebugString(sz);
		}
	
	///////////////////////////////
	// IActiveScriptSite Methods //
	///////////////////////////////
	
	STDMETHOD(GetLCID)(LCID *plcid)
		{
	//	TraceEnter("CScriptSite::GetLCID");
		
	//	TraceExit("CScriptSite::GetLCID");
		return E_NOTIMPL;
		}
	
	STDMETHOD(GetItemInfo)(LPCOLESTR pstrName, DWORD dwReturnMask, 
	  IUnknown **ppunkItem, ITypeInfo **ppTypeInfo);
	
	STDMETHOD(GetDocVersionString)(BSTR *pbstrVersionString)
		{
	//	TraceEnter("CScriptSite::GetDocVersionString");
		
	//	TraceExit("CScriptSite::GetDocVersionString");
		return E_NOTIMPL;
		}
	
	STDMETHOD(OnScriptTerminate)(const VARIANT *pvarResult, 
	  const EXCEPINFO *pexcepinfo)
		{
	//	TraceEnter("CScriptSite::OnScriptTerminate");
		
	//	TraceExit("CScriptSite::OnScriptTerminate");
		return S_OK;
		}
	
	STDMETHOD(OnStateChange)(SCRIPTSTATE ssScriptState);
	
//	STDMETHOD(OnScriptError)(IActiveScriptError *pase);
	
	STDMETHOD(OnEnterScript)()
		{
	//	TraceEnter("CScriptSite::OnEnterScript");
		
	//	TraceExit("CScriptSite::OnEnterScript");
		return S_OK;
		}
	
	STDMETHOD(OnLeaveScript)()
		{
	//	TraceEnter("CScriptSite::OnLeaveScript");
		
	//	TraceExit("CScriptSite::OnLeaveScript");
		return S_OK;
		}
	
	/////////////////////////////////////
	// IActiveScriptSiteWindow Methods //
	/////////////////////////////////////
	
    STDMETHOD(GetWindow)(HWND* phwnd)
		{
		if (!phwnd)     
			{
			return E_INVALIDARG;   
			}
		else
			{
			*phwnd = m_hwndHost; 
			return S_OK;
			}
		}
	
	STDMETHOD(EnableModeless)(BOOL fEnable)
		{
		return S_OK;
		}
	
	};
