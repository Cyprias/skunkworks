VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CaiCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' Creature Attribute Information (used by OnAssessCreature)


Public strength As Long
Public endurance As Long
Public quickness As Long
Public coordination As Long
Public focus As Long
Public self As Long
Public staminaCur As Long
Public staminaMax As Long
Public manaCur As Long
Public manaMax As Long



