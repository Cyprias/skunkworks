VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CbiCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' Creature Basic Information (used by OnAssessCreature)


Public species As Long
Public lvl As Long
Public healthCur As Long
Public healthMax As Long


Private dictRaw As Dictionary


Public Property Get raw() As Dictionary
    Set raw = dictRaw
End Property


Friend Sub SetRaw(ByVal dict As Dictionary)
    Set dictRaw = dict
End Sub


