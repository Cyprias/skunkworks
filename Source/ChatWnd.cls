VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ChatWndCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' opmChatWnd* output handler.


Private rgsz() As String
Private rgcmc() As Long
Private csz As Long
Private iwndBuf As Long


Public Sub OutputSz(ByVal sz As String, ByVal opm As Long, ByVal cmc As Long)
    
    Dim iwnd As Long
    Select Case opm
    Case opmChatWnd
        iwnd = 0
    Case opmChatWnd1
        iwnd = 1
    Case opmChatWnd2
        iwnd = 2
    Case opmChatWnd3
        iwnd = 3
    Case opmChatWnd4
        iwnd = 4
    End Select
    
    If iwnd <> iwndBuf And csz > 0 Then
        Call SendBuffer()
    End If
    iwndBuf = iwnd
    
'   Parse the string into lines.  Buffer partial lines in rgsz.
    Dim ich As Long, ichLim As Long, ichNext As Long
    ich = 0
    Do While ich < Len(sz)
    '   Find the next line break, if any.
        ichLim = IchInStr(ich, sz, vbLf)
        If ichLim <> iNil Then
            ichNext = ichLim + 1
            If ichLim > ich Then
                If SzMid(sz, ichLim - 1, 1) = vbCr Then
                    ichLim = ichLim - 1
                End If
            End If
        Else
        '   No line break; take whatever's left.
            ichLim = Len(sz)
            ichNext = ichLim
        End If
        
    '   Make sure the arrays are big enough.
        If csz > UBound(rgsz) Then
            ReDim Preserve rgsz(csz + 10)
            ReDim Preserve rgcmc(csz + 10)
        End If
        
    '   Add the current substring and color code.
        rgsz(csz) = SzMid(sz, ich, ichLim - ich)
        rgcmc(csz) = cmc
        csz = csz + 1
        
        If ichLim < ichNext Then
        '   We've buffered a complete line; send it through to the plugin.
            Call SendBuffer()
        End If
        
        ich = ichNext
    Loop
    
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    ReDim rgsz(20)
    ReDim rgcmc(20)
    csz = 0
    iwndBuf = 0
    
End Sub

Private Sub SendBuffer()
    
    Call skapi.QueueForChatWnd(iwndBuf, rgsz, rgcmc, csz)
    
'   Empty the buffer.
    Do While csz > 0
        csz = csz - 1
        rgsz(csz) = ""
    Loop
    
End Sub





















