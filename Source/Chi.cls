VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ChiCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' Creature Human Information (used by OnAssessCreature)


Public rank As Long
Public cfollower As Long
Public loyalty As Long
Public leadership As Long
Public fPK As Boolean
Public szGender As String
Public szRace As String
Public szProfession As String
Public szFellowship As String
Public szMonarch As String
Public szPatron As String


Private Sub Class_Initialize()
    
    loyalty = -1
    leadership = -1
    
End Sub


