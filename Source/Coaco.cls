VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CoacoCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' Zero-origin ACO collection class.
' The standard VB collection is one-origin, but ACScript compatibility
' (and common sense) dictates zero-origin.


'--------------
' Private data
'--------------

Private coacoMbr As Collection
Private fMutableMbr As Boolean


'------------
' Properties
'------------

Public Property Get count() As Long
    count = coacoMbr.count
End Property

Public Property Get Item(ByVal iitem As Long) As AcoCls
Attribute Item.VB_UserMemId = 0
    Set Item = coacoMbr.Item(1 + iitem)
End Property


'---------
' Methods
'---------

Public Function FContainsAco(ByVal aco As AcoCls) As Boolean
    
    FContainsAco = (IitemFindAco(aco) <> iitemNil)
    
End Function

Public Function CoacoClone() As CoacoCls
    
    Set CoacoClone = New CoacoCls
    Call CoacoClone.SetMutable
    Call CoacoClone.AddCoaco(Me)
    
End Function

Public Sub Clear()
    
    Do While coacoMbr.count > 0
        Call coacoMbr.Remove(coacoMbr.count)
    Loop
    
End Sub

Public Sub AddAco(ByVal aco As AcoCls, Optional ByVal iitem As Long = iitemNil)
    
    Call Assert(fMutableMbr, "Collection cannot be changed.")
    
    Call AddAcoF(aco, iitem)
    
End Sub

Public Sub RemoveAco(ByVal aco As AcoCls)
    
    Call Assert(fMutableMbr, "Collection cannot be changed.")
    
    Call RemoveAcoF(aco)
    
End Sub

Public Sub RemoveIitem(ByVal iitem As Long)
    
    Call Assert(fMutableMbr, "Collection cannot be changed.")
    
    Call RemoveIitemF(iitem)
    
End Sub

Public Sub AddCoaco(ByVal coaco2 As CoacoCls)
    
    Call Assert(fMutableMbr, "Collection cannot be changed.")
    
    Dim aco As AcoCls
    For Each aco In coaco2
        If Not FContainsAco(aco) Then
            Call AddAcoF(aco)
        End If
    Next aco
    
End Sub

Public Sub RemoveCoaco(ByVal coaco2 As CoacoCls)
    
    Call Assert(fMutableMbr, "Collection cannot be changed.")
    
    Dim aco As AcoCls
    For Each aco In coaco2
        Call RemoveAcoF(aco)
    Next aco
    
End Sub

Public Sub Intersect(ByVal coaco2 As CoacoCls)
    
    Call Assert(fMutableMbr, "Collection cannot be changed.")
    
    Dim iitem As Long, aco As AcoCls
    iitem = 0
    Do While iitem < count
        Set aco = Item(iitem)
        If coaco2.FContainsAco(aco) Then
            iitem = iitem + 1
        Else
            Call RemoveIitemF(iitem)
        End If
    Loop
    
End Sub

Public Sub Sort(ByVal sortby As Object)
    
    Call Assert(fMutableMbr, "Collection cannot be changed.")
    
    Dim caco As Long
    caco = coacoMbr.count
    If caco > 0 Then
    '   Copy the collection to a temp array.
        Dim rgaco() As AcoCls
        ReDim rgaco(caco - 1)
        Dim iaco As Long
        For iaco = 0 To caco - 1
                Set rgaco(iaco) = coacoMbr(1 + iaco)
        Next iaco
        
    '   Sort the array and rebuild the collection.
        Call FromRgaco(rgaco, caco, sortby)
    End If
    
End Sub

' NewEnum must return the IUnknown interface of a
' collection's enumerator.
Public Function NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
   Set NewEnum = coacoMbr.[_NewEnum]
End Function


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------

Friend Sub SetMutable()
    fMutableMbr = True
End Sub

Friend Sub AddAcoF(ByVal aco As AcoCls, Optional ByVal iitem As Long = iitemNil)
    
    If iitem >= 0 And iitem < coacoMbr.count Then
        Call coacoMbr.Add(aco, , 1 + iitem)
    Else
        Call coacoMbr.Add(aco)
    End If
    
End Sub

Public Sub RemoveAcoF(ByVal aco As AcoCls)
    
    Call RemoveIitemF(IitemFindAco(aco))
    
End Sub

Friend Sub RemoveIitemF(ByVal iitem As Long)
    
    If iitem <> iitemNil Then
        Call coacoMbr.Remove(1 + iitem)
    End If
    
End Sub

Friend Sub FromRgaco(rgaco() As AcoCls, ByVal caco As Long, _
  Optional ByVal sortby As Object = Nothing)
    
    If caco > 0 And Not (sortby Is Nothing) Then
    '   Sort the array.
        On Error Resume Next
        Call SortRgacoEx(rgaco, 0, caco - 1, sortby)
        If Err.Number <> 0 Then
            Call LogError(Err.Source, Err.Description, Err.Number)
            Call Err.Clear
        End If
        On Error GoTo 0
    End If
    
    Set coacoMbr = New Collection
    
    Dim iaco As Long
    For iaco = 0 To caco - 1
        Call AddAcoF(rgaco(iaco))
    Next iaco
    
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    MemStats.ccoaco = MemStats.ccoaco + 1
    
    Set coacoMbr = New Collection
    fMutableMbr = False
    
End Sub

Private Sub Class_Terminate()
    
    MemStats.ccoaco = MemStats.ccoaco - 1
    
End Sub


Private Function IitemFindAco(ByVal aco As AcoCls) As Long
    
    IitemFindAco = iitemNil
    
    Dim iitem As Long
    For iitem = 0 To coacoMbr.count - 1
        If coacoMbr(1 + iitem) Is aco Then
            IitemFindAco = iitem
            Exit For
        End If
    Next iitem
    
End Function


' QuickSort algorithm
Private Sub SortRgacoEx(rgaco() As AcoCls, _
  ByVal iFirst As Long, ByVal iLast As Long, ByVal sortby As Object)
    
    If iFirst < iLast Then
        ' Only two elements in this subdivision; exchange if
        ' they are out of order, and end recursive calls
        If iLast - iFirst = 1 Then
            ''@B CallSortCompare
            If sortby.CompareAcos(rgaco(iFirst), rgaco(iLast)) > 0 Then
                Call SwapAcos(rgaco(iFirst), rgaco(iLast))
            End If
            ''@E CallSortCompare
        Else
            Dim i As Long, j As Long, iRand As Long
            Dim acoSplit As AcoCls
            
            ' Pick pivot element at random and move to end
            ' (consider calling Randomize before sorting)
            iRand = iFirst + Int((iLast + 1 - iFirst) * Rnd)
            Call SwapAcos(rgaco(iLast), rgaco(iRand))
            Set acoSplit = rgaco(iLast)
            
            ' Move in from both sides toward pivot element
            i = iFirst
            j = iLast
            Do
                Do While i < j And sortby.CompareAcos(rgaco(i), acoSplit) <= 0
                    i = i + 1
                Loop
                Do While j > i And sortby.CompareAcos(rgaco(j), acoSplit) >= 0
                    j = j - 1
                Loop
                
                ' If you haven't reached pivot element, it means
                ' that the two elements on either side are out of
                ' order, so swap them
                If i < j Then
                    Call SwapAcos(rgaco(i), rgaco(j))
                End If
            Loop While i < j
            
            ' Move pivot element back to its proper place
            Call SwapAcos(rgaco(i), rgaco(iLast))
            
            ' Recursively call SortRgacoEx (pass smaller
            ' subdivision first to use less stack space)
            If i - iFirst < iLast - i Then
                Call SortRgacoEx(rgaco, iFirst, i - 1, sortby)
                Call SortRgacoEx(rgaco, i + 1, iLast, sortby)
            Else
                Call SortRgacoEx(rgaco, i + 1, iLast, sortby)
                Call SortRgacoEx(rgaco, iFirst, i - 1, sortby)
            End If
        End If
    End If
    
End Sub

Private Sub SwapAcos(ByRef aco1 As AcoCls, ByRef aco2 As AcoCls)
    
    Dim acoT As AcoCls
    Set acoT = aco1
    Set aco1 = aco2
    Set aco2 = acoT
    
End Sub













