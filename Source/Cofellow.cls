VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CofellowCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' Fellow collection class.


'--------------
' Private data
'--------------

Private cofellow As Collection


'------------
' Properties
'------------

Public Property Get count() As Long
    count = cofellow.count
End Property

Public Property Get Item(ByVal ifellow As Long) As FellowCls
Attribute Item.VB_UserMemId = 0
    Set Item = cofellow.Item(1 + ifellow)
End Property


'---------
' Methods
'---------

' NewEnum must return the IUnknown interface of a
' collection's enumerator.
Public Function NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
   Set NewEnum = cofellow.[_NewEnum]
End Function


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------

Friend Sub Clear()
    Set cofellow = New Collection
End Sub

Friend Function FAdd(ByVal fellowNew As FellowCls) As Boolean
        
        Dim ifellow As Long
        For ifellow = 0 To cofellow.count - 1
                Dim fellow As FellowCls
                Set fellow = cofellow(1 + ifellow)
                If fellow.oid = fellowNew.oid Then
                '       Replace the existing fellow.
                        Call cofellow.Remove(1 + ifellow)
                        If ifellow > 0 Then
                            Call cofellow.Add(fellowNew, , , ifellow)
                        Else
                            Call cofellow.Add(fellowNew, , 1)
                        End If
                        FAdd = False
                        Exit Function
                End If
        Next ifellow
        
'       Add the new fellow.
        Call cofellow.Add(fellowNew)
        FAdd = True
        
End Function

Friend Sub Remove(ByVal oid As Long)
    
    Dim ifellow As Long
    For ifellow = 0 To cofellow.count - 1
        Dim fellow As FellowCls
        Set fellow = cofellow(1 + ifellow)
        If fellow.oid = oid Then
            Call cofellow.Remove(1 + ifellow)
            Exit For
        End If
    Next ifellow
    
End Sub

Friend Sub SetLeader(ByVal oid As Long)
    
    Dim ifellow As Long
    For ifellow = 0 To cofellow.count - 1
        Dim fellow As FellowCls
        Set fellow = cofellow(1 + ifellow)
        If fellow.oid = oid Then
            If ifellow <> 0 Then
            '   Move leader to the front.
                Call cofellow.Remove(1 + ifellow)
                Call cofellow.Add(fellow, , 1)
            End If
            Exit For
        End If
    Next ifellow
    
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call Clear
End Sub














