Attribute VB_Name = "ComdlgUtil"
Option Explicit
' Common Dialog utility functions.


'------
' APIs
'------

Public Function FGetSzFileOpen(szFile As String, comdlg As CommonDialog, _
  ByVal szFilter As String, ByVal szExtDefault As String, _
  Optional ByRef szDir As String = "", _
  Optional ByVal fShowReadOnly As Boolean = False, _
  Optional ByRef fReadOnly As Boolean = False) As Boolean
    
    comdlg.FileName = szFile
    comdlg.Filter = szFilter
    comdlg.FilterIndex = 1
    comdlg.DefaultExt = szExtDefault
    If szFile <> "" Then
        comdlg.InitDir = SzDirFromPath(szFile)
    Else
        comdlg.InitDir = szDir
    End If
    comdlg.flags = cdlOFNNoChangeDir
    If Not fShowReadOnly Then
        comdlg.flags = comdlg.flags + cdlOFNHideReadOnly
    End If
    
    If FShowOpen(comdlg) Then
        szFile = comdlg.FileName
        szDir = SzDirFromPath(szFile)
        FGetSzFileOpen = True
    Else
        szFile = ""
        FGetSzFileOpen = False
    End If
    
End Function

Public Function FGetSzFileSave(szFile As String, comdlg As CommonDialog, _
  ByVal szFilter As String, ByVal szExtDefault As String, _
  Optional ByRef szDir As String = "") As Boolean
    
    comdlg.FileName = szFile
    comdlg.Filter = szFilter
    comdlg.FilterIndex = 1
    comdlg.DefaultExt = szExtDefault
    If szFile <> "" Then
        comdlg.InitDir = SzDirFromPath(szFile)
    Else
        comdlg.InitDir = szDir
    End If
    comdlg.flags = cdlOFNNoChangeDir Or cdlOFNOverwritePrompt
    
    If FShowSave(comdlg) Then
        szFile = comdlg.FileName
        szDir = SzDirFromPath(szFile)
        FGetSzFileSave = True
    Else
        szFile = ""
        FGetSzFileSave = False
    End If
    
End Function


Public Function FShowOpen(comdlg As CommonDialog) As Boolean
    
    FShowOpen = False
    comdlg.CancelError = True
    On Error GoTo LExit
    
    comdlg.ShowOpen
    
    FShowOpen = True
    
LExit:
End Function

Public Function FShowSave(comdlg As CommonDialog) As Boolean
    
    FShowSave = False
    comdlg.CancelError = True
    On Error GoTo LExit
    
    comdlg.ShowSave
    
    FShowSave = True
    
LExit:
End Function













