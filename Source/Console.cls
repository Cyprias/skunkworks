VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ConsoleCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' This class implements the console object visible to the script.


'------------
' Properties
'------------

Public Property Get szScript() As String
    szScript = formSWConsole.mrulistScript.szFile
End Property


'---------
' Methods
'---------

' Stop the script's execution and return control to the console UI.
' This is like clicking the Stop button.
Public Sub StopScript()
    
    Call formSWConsole.StopScript
    
End Sub

' Stop the script's execution and return control to the console UI.
' This is like clicking the Stop button.
Public Sub RunScript(ByVal szFile As String)
    
'   Make sure the file exists.
    On Error Resume Next
    Call fso.GetFile(szFile)
    If Err.Number <> 0 Then
        Call Err.Raise(Err.Number, Err.Source, szFile + ": " + Err.Description)
    End If
    
    Call formSWConsole.StopScript
    formSWConsole.szScriptNext = fso.GetAbsolutePathName(szFile)
    
End Sub

' Adjust the console's priority up or down.
Public Sub SetPriority(ByVal priority As Long)
    
'   Call SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS)
    Call SetThreadPriority(GetCurrentThread(), priority)
    
End Sub

' Clear the console output window.
Public Sub Clear()
    
    formSWConsole.textOutput.Text = ""
    formSWConsole.textOutput.SelStart = 0
    formSWConsole.textOutput.SelLength = 0
    
End Sub


'----------
' Privates
'----------

Private Sub Class_Terminate()
    
'   Revert to default priority when script exits.
'   Call SetPriorityClass(GetCurrentProcess(), NORMAL_PRIORITY_CLASS)
    Call SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_NORMAL)
    
End Sub












