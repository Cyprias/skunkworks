VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CospellCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' Spell collection class.


'--------------
' Private data
'--------------

Private Const spellidVitae As Long = 666

Private cospellMbr As Collection


'------------
' Properties
'------------

Public Property Get count() As Long
    count = cospellMbr.count
End Property

Public Property Get Item(ByVal ispell As Long) As SpellCls
Attribute Item.VB_UserMemId = 0
    Set Item = cospellMbr.Item(1 + ispell)
End Property


'---------
' Methods
'---------

' NewEnum must return the IUnknown interface of a
' collection's enumerator.
Public Function NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
   Set NewEnum = cospellMbr.[_NewEnum]
End Function


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------

Friend Sub AddSpell(ByVal spell As SpellCls)
    
    If spell.layer = 0 Then
    '   Replace any existing spell with the same spellid.
        Call RemoveSpellid(spell.spellid, spell.layer, False)
    End If
    
    Call cospellMbr.Add(spell)
    
    If spell.spellid = spellidVitae Then
        self.fractVitae = spell.dlvl
    Else
        Call UpdateSkinfo(spell.spellid, spell.flags, spell.avsAffected)
    End If
    
End Sub

Friend Sub RemoveSpellid(ByVal spellid As Long, ByVal layer As Long, _
  Optional ByVal fUpdate As Boolean = True)
    
    Dim ispell As Long
    For ispell = 0 To cospellMbr.count - 1
        Dim spell As SpellCls
        Set spell = Item(ispell)
        If spell.spellid = spellid And spell.layer = layer Then
            Call RemoveIspell(ispell, fUpdate)
            Exit For
        End If
    Next ispell
    
End Sub

Friend Sub Clear()
    
    Call Class_Initialize
    
End Sub

Friend Sub ClearTimedSpells()
    
    Dim ispell As Long
    ispell = 0
    Do While ispell < cospellMbr.count
        Dim spell As SpellCls
        Set spell = Item(ispell)
        If spell.csecDuration > 0 Then
            Call RemoveIspell(ispell, True)
        Else
            ispell = ispell + 1
        End If
    Loop
    
End Sub


Friend Sub InsertSpellByDiff(ByVal spell As SpellCls)
    
    Dim ispell As Long
    For ispell = 0 To cospellMbr.count - 1
        Dim spellT As SpellCls
        Set spellT = Item(ispell)
        If spellT.diff > spell.diff Then Exit For
    Next ispell
    
    If ispell < cospellMbr.count Then
        Call cospellMbr.Add(spell, , 1 + ispell)
    Else
        Call cospellMbr.Add(spell)
    End If
    
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    Set cospellMbr = New Collection
    
End Sub

Private Sub RemoveIspell(ByVal ispell As Long, ByVal fUpdate As Boolean)
    
    Dim spell As SpellCls
    Set spell = cospellMbr.Item(1 + ispell)
    Call cospellMbr.Remove(1 + ispell)
    
    If fUpdate Then
        If spell.spellid = spellidVitae Then
            self.fractVitae = 1
        Else
            Call UpdateSkinfo(spell.spellid, spell.flags, spell.avsAffected)
        End If
    End If
    
End Sub

Private Sub UpdateSkinfo(ByVal spellid As Long, _
  ByVal flags As Long, ByVal avsAffected As Long)
    
'   Figure out which attribute, vital, or skill needs updating.
    Dim skinfo As SkinfoCls
    Select Case flags And &H13&
    Case &H1&
    '   It's a primary attribute.
        Set skinfo = self.SkinfoFromAttr(avsAffected)
    Case &H2&
    '   It's a vital (secondary attribute).
        Set skinfo = self.SkinfoFromVital(avsAffected)
    Case &H10&
    '   It's a skill.
        Set skinfo = self.SkinfoFromSkid(avsAffected)
    Case Else
    '   It's something else (e.g. armor level).
        Exit Sub
    End Select
    
'   Find all the outstanding spell effects for that attr/vital/skid.
'   Keep track of found spells by family.  Spells in the same family
'   supersede each other; only the most potent takes effect.  Spells
'   in different families are cumulative; e.g. you can be both buffed
'   and debuffed in a given skill at the same time, because they're
'   different families.
    Dim rgfamily(4) As Long, rgdlvl(4) As Long, rgdiff(4) As Long
    Dim cfamily As Long, ifamily As Long
    cfamily = 0
    
    Dim ispell As Long, spell As SpellCls, dlvl As Long
    For ispell = 0 To cospellMbr.count - 1
        Set spell = Item(ispell)
        If (spell.flags And &H13&) = (flags And &H13&) And _
          spell.avsAffected = avsAffected Then
        '   Found a spell affecting the proper stat.
        '   Check against spell families found so far.
            Dim fNewFamily As Boolean
            fNewFamily = True
        '   REVIEW: Be nice to understand these flags more thoroughly.
            If (spell.flags And &HF000&) = &H5000& Then
            '   Spell has a percentage (multiplicative) effect.
                dlvl = skinfo.lvlUnbuffed * (spell.dlvl - 1)
            Else
            '   Spell has a constant (additive) effect.
                dlvl = spell.dlvl
            End If
            For ifamily = 0 To cfamily - 1
                If spell.family = rgfamily(ifamily) Then
                '   Spell matches an existing family; keep the one
                '   with the biggest effect (positive or negative).
                '    If Abs(spell.dlvl) > Abs(rgdlvl(ifamily)) Then
                '   Wrong, make that the biggest difficulty.
                    If spell.diff > rgdiff(ifamily) Then
                        rgdlvl(ifamily) = dlvl
                        rgdiff(ifamily) = spell.diff
                    End If
                    fNewFamily = False
                    Exit For
                End If
            Next ifamily
            If fNewFamily Then
            '   Note new family.
                rgfamily(cfamily) = spell.family
                rgdlvl(cfamily) = dlvl
                rgdiff(cfamily) = spell.diff
                cfamily = cfamily + 1
            End If
        End If
    Next ispell
    
'   Add up the effects of the most potent spell in each family.
    dlvl = 0
    For ifamily = 0 To cfamily - 1
        dlvl = dlvl + rgdlvl(ifamily)
    Next ifamily
    
'   Apply the cumulative spell effect.
    Call skinfo.SetDlvlSpells(dlvl)
    
End Sub











