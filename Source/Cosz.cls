VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CoszCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' Zero-origin string collection class.
' The standard VB collection is one-origin, but ACScript compatibility 
' (and common sense) dictates zero-origin.
' Properties and methods are otherwise the same as the standard VB collection.


Private cosz As Collection


Public Property Get Count() As Long
    Count = cosz.Count
End Property

Public Property Get Item(ByVal iitem As Long) As String
Attribute Item.VB_UserMemId = 0
    Item = cosz.Item(1 + iitem)
End Property

Friend Sub Add(ByVal sz As String)
    Call cosz.Add(sz)
End Sub

Friend Sub Remove(ByVal iitem As Long)
    Call cosz.Remove(1 + iitem)
End Sub

' NewEnum must return the IUnknown interface of a
' collection's enumerator.
Public Function NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
   Set NewEnum = cosz.[_NewEnum]
End Function


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    Set cosz = New Collection
    
End Sub





