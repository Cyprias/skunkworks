VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CschkCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Option Compare Text
' Control Surrogate for Checkbox
Implements CsctlCls


'--------------
' Private data
'--------------

Private szPanelMbr As String, szControlMbr As String
Private csviewMbr As CsviewCls
Private WithEvents dcctlMbr As DecalControls.CheckBox
Attribute dcctlMbr.VB_VarHelpID = -1

Private ehdChange As Long
Private ehdDestroy As Long


'-----------------
' Csctl interface
'-----------------

Private Property Get CsctlCls_szValue() As String
    CsctlCls_szValue = CStr(dcctlMbr.Checked)
End Property

Private Sub CsctlCls_Init(ByVal szPanel As String, ByVal szControl As String, _
  ByVal csview As CsviewCls, ByVal dcctl As Object, ByVal elemControl As IXMLDOMElement)
    
    szPanelMbr = szPanel
    szControlMbr = szControl
    Set csviewMbr = csview
    Set dcctlMbr = dcctl
    
End Sub

Private Function CsctlCls_GetProperty(ByVal szProperty As String, _
  rgvArg() As Variant, ByVal cvArg As Long) As Variant
    
    CsctlCls_GetProperty = CallByName(dcctlMbr, szProperty, VbGet)
    
End Function

Private Sub CsctlCls_SetProperty(ByVal szProperty As String, _
  rgvArg() As Variant, ByVal cvArg As Long, ByVal vValue As Variant)
    
    Call CallByName(dcctlMbr, szProperty, VbLet, vValue)
    
End Sub


'------------------------
' Control event handlers
'------------------------

Private Sub dcctlMbr_Change(ByVal nID As Long, ByVal bChecked As Boolean)
    Call csviewMbr.HandleEvent(ehdChange, szControlMbr, "Change", True, nID, bChecked)
End Sub

Private Sub dcctlMbr_Destroy(ByVal nID As Long)
    Call csviewMbr.HandleEvent(ehdDestroy, szControlMbr, "Destroy", False, nID)
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceLine("Cschk.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceLine(szPanelMbr + "." + szControlMbr + ".Terminate")
End Sub


