VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CsctlCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Option Compare Text
' Generic Control Surrogate


'--------------
' Private data
'--------------

Private szPanelMbr As String, szControlMbr As String
Private csviewMbr As CsviewCls
Private dcctlMbr As Object


'---------
' Methods
'---------

Public Property Get szValue() As String
    szValue = ""
End Property

Public Sub Init(ByVal szPanel As String, ByVal szControl As String, _
  ByVal csview As CsviewCls, ByVal dcctl As Object, ByVal elemControl As IXMLDOMElement)
    
    szPanelMbr = szPanel
    szControlMbr = szControl
    Set csviewMbr = csview
    Set dcctlMbr = dcctl
    
End Sub

Public Function GetProperty(ByVal szProperty As String, _
  rgvArg() As Variant, ByVal cvArg As Long) As Variant
    
    GetProperty = CallByName(dcctlMbr, szProperty, VbGet)
    
End Function

Public Sub SetProperty(ByVal szProperty As String, _
  rgvArg() As Variant, ByVal cvArg As Long, ByVal vValue As Variant)
    
    Call CallByName(dcctlMbr, szProperty, VbLet, vValue)
    
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceLine("Csctl.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceLine(szPanelMbr + "." + szControlMbr + ".Terminate")
End Sub





