VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CsedtCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Option Compare Text
' Control Surrogate for Edit box
Implements CsctlCls


'--------------
' Private data
'--------------

Private szPanelMbr As String, szControlMbr As String
Private csviewMbr As CsviewCls
Private WithEvents dcctlMbr As DecalControls.Edit
Attribute dcctlMbr.VB_VarHelpID = -1

Private ehdBegin As Long
Private ehdChange As Long
Private ehdDestroy As Long
Private ehdEnd As Long


'-----------------
' Csctl interface
'-----------------

Private Property Get CsctlCls_szValue() As String
    CsctlCls_szValue = dcctlMbr.Text
End Property

Private Sub CsctlCls_Init(ByVal szPanel As String, ByVal szControl As String, _
  ByVal csview As CsviewCls, ByVal dcctl As Object, ByVal elemControl As IXMLDOMElement)
    
    szPanelMbr = szPanel
    szControlMbr = szControl
    Set csviewMbr = csview
    Set dcctlMbr = dcctl
    
End Sub

Private Function CsctlCls_GetProperty(ByVal szProperty As String, _
  rgvArg() As Variant, ByVal cvArg As Long) As Variant
    
    Select Case szProperty
    Case "Capture"
        Call dcctlMbr.Capture
    Case "Select"
        Call dcctlMbr.Select(rgvArg(0), rgvArg(1))
    Case "SetMargins"
        Call dcctlMbr.SetMargins(rgvArg(0), rgvArg(1))
    Case Else
        CsctlCls_GetProperty = CallByName(dcctlMbr, szProperty, VbGet)
    End Select
    
End Function

Private Sub CsctlCls_SetProperty(ByVal szProperty As String, _
  rgvArg() As Variant, ByVal cvArg As Long, ByVal vValue As Variant)
    
    Call CallByName(dcctlMbr, szProperty, VbLet, vValue)
    
End Sub


'------------------------
' Control event handlers
'------------------------

Private Sub dcctlMbr_Begin(ByVal nID As Long)
    Call csviewMbr.HandleEvent(ehdBegin, szControlMbr, "Begin", False, nID)
End Sub

Private Sub dcctlMbr_Change(ByVal nID As Long, ByVal strText As String)
    Call csviewMbr.HandleEvent(ehdChange, szControlMbr, "Change", False, nID, strText)
End Sub

Private Sub dcctlMbr_Destroy(ByVal nID As Long)
    Call csviewMbr.HandleEvent(ehdDestroy, szControlMbr, "Destroy", False, nID)
End Sub

Private Sub dcctlMbr_End(ByVal nID As Long, ByVal bSuccess As Boolean)
'   Send OnControlEvent only if bSuccess is True.
    Call csviewMbr.HandleEvent(ehdEnd, szControlMbr, "End", bSuccess, nID, bSuccess)
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceLine("Csedt.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceLine(szPanelMbr + "." + szControlMbr + ".Terminate")
End Sub



