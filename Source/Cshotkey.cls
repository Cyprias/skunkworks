VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CshotkeyCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' Control Surrogate for Hotkey


'--------------
' Private data
'--------------

Private szPanelMbr As String, szControlMbr As String
Private csviewMbr As CsviewCls
Private WithEvents hotkeyMbr As DecalInput.Hotkey
Attribute hotkeyMbr.VB_VarHelpID = -1

Private ehdHotkey As Long


'------------------
' Friend functions
'------------------

Friend Sub Init(ByVal szPanel As String, ByVal szControl As String, _
  ByVal csview As CsviewCls, ByVal elemControl As IXMLDOMElement)
    
    szPanelMbr = szPanel
    szControlMbr = szControl
    Set csviewMbr = csview
    Set hotkeyMbr = New DecalInput.Hotkey
    
    hotkeyMbr.Key = elemControl.getAttribute("key")
    hotkeyMbr.Tag = elemControl.getAttribute("tag")
    If Not IsNull(elemControl.getAttribute("enabled")) Then
        hotkeyMbr.Enabled = elemControl.getAttribute("enabled")
    End If
    
End Sub

Friend Property Get dcctl() As DecalInput.Hotkey
    Set dcctl = hotkeyMbr
End Property


'------------------------
' Control event handlers
'------------------------

Private Sub hotkeyMbr_Hotkey(ByVal Source As DecalInput.Hotkey)
    Call csviewMbr.HandleEvent(ehdHotkey, szControlMbr, "Hotkey", False, Source)
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceLine("Cshotkey.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceLine(szPanelMbr + "." + szControlMbr + ".Terminate")
End Sub




