VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CskeyCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' Control Surrogate for Hotkey


'--------------
' Private data
'--------------

Private szMbr As String
Private vkMbr As Long
Private WithEvents hotkeyMbr As DecalInput.Hotkey
Attribute hotkeyMbr.VB_VarHelpID = -1


'------------------
' Friend functions
'------------------

Friend Sub Init(ByVal sz As String, ByVal vk As Long)
    Call TraceEnter("cskey.Init", SzQuote(sz) + ", " + CStr(vk))
    
    szMbr = sz
    vkMbr = vk
    Set hotkeyMbr = New DecalInput.Hotkey
    hotkeyMbr.Key = swkeys.SzFromVkDecal(vk)
    hotkeyMbr.Enabled = True
    
    Call TraceExit("cskey.Init")
End Sub


'------------------------
' Control event handlers
'------------------------

Private Sub hotkeyMbr_Hotkey(ByVal Source As DecalInput.IHotkey)
    Call TraceEnter("cskey.Hotkey", SzQuote(szMbr))
    
'   Don't fire hotkeys when typing into chat buffer.
    If Not Hooks.ChatState Then
        Call swcontrols.SendHotkeyMsg(szMbr, vkMbr)
    End If
    
    Call TraceExit("cskey.Hotkey")
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceLine("Cskey.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceEnter("cskey.Terminate", SzQuote(szMbr))
    
    Set hotkeyMbr = Nothing
    
    Call TraceExit("cskey.Terminate")
End Sub



