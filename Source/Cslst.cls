VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CslstCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Option Compare Text
' Control Surrogate for List
Implements CsctlCls


'--------------
' Private data
'--------------

Private szPanelMbr As String, szControlMbr As String
Private csviewMbr As CsviewCls
Private WithEvents dcctlMbr As DecalControls.List
Attribute dcctlMbr.VB_VarHelpID = -1

Private ehdChange As Long
Private ehdDestroy As Long

Private xClicked As Long, yClicked As Long


'-----------------
' Csctl interface
'-----------------

Private Property Get CsctlCls_szValue() As String
    CsctlCls_szValue = CStr(xClicked) + "," + CStr(yClicked)
End Property

Private Sub CsctlCls_Init(ByVal szPanel As String, ByVal szControl As String, _
  ByVal csview As CsviewCls, ByVal dcctl As Object, ByVal elemControl As IXMLDOMElement)
    
    szPanelMbr = szPanel
    szControlMbr = szControl
    Set csviewMbr = csview
    Set dcctlMbr = dcctl
    
'   Figure out which columns contain icons.
    Dim selCol As IXMLDOMSelection
    Set selCol = elemControl.selectNodes("column")
    Call TraceLine("selCol.length = " + CStr(selCol.Length))
    Dim mpicolfIcon() As Boolean: ReDim mpicolfIcon(selCol.Length - 1)
    Dim icol As Long
    icol = 0
    Do
        Dim elemCol As IXMLDOMElement
        Set elemCol = selCol.nextNode
        If elemCol Is Nothing Then Exit Do
        
        mpicolfIcon(icol) = FEqSzI(elemCol.getAttribute("progid"), _
          "DecalControls.IconColumn")
        icol = icol + 1
    Loop
    
'   Find all the row entries.
    Dim selRow As IXMLDOMSelection
    Set selRow = elemControl.selectNodes("row")
    Call TraceLine("selRow.length = " + CStr(selRow.Length))
    Do
        Dim elemRow As IXMLDOMElement
        Set elemRow = selRow.nextNode
        If elemRow Is Nothing Then Exit Do
        
        Dim irow As Long
        irow = dcctlMbr.AddRow()
        icol = 0
        
    '   Find all the data entries for this row.
        Dim selData As IXMLDOMSelection
        Set selData = elemRow.selectNodes("data")
        Call TraceLine("selData.length = " + CStr(selData.Length))
        Do
            Dim elemData As IXMLDOMElement
            Set elemData = selData.nextNode
            If elemData Is Nothing Then Exit Do
            
            Dim szColor As String: szColor = SzFromVNull(elemData.getAttribute("color"))
            If szColor <> "" Then
                dcctlMbr.Color(icol, irow) = CLng(SzFixHex(szColor))
            End If
            
            Dim isub As Long
            isub = 0
            If mpicolfIcon(icol) Then
                Dim icon As Long, vIconLib As Variant
                icon = CLng(SzFixHex(elemData.Text))
                vIconLib = elemData.getAttribute("iconlibrary")
                If IsNull(vIconLib) Then
                    If icon > 0 And icon < &H6000000 Then icon = icon + &H6000000
                Else
                    dcctlMbr.Data(icol, irow, isub) = site.LoadResourceModule(vIconLib)
                    Call TraceLine("iconlib = " + CStr(dcctlMbr.Data(icol, irow, isub)))
                End If
                isub = isub + 1
                dcctlMbr.Data(icol, irow, isub) = icon
            Else
                dcctlMbr.Data(icol, irow, isub) = SzFixHex(elemData.Text)
            End If
            Call TraceLine("dcctlMbr.Data(" + CStr(icol) + ", " + CStr(irow) + ") = " + _
              CStr(dcctlMbr.Data(icol, irow, isub)))
            icol = icol + 1
        Loop
    Loop
    
End Sub

Private Function CsctlCls_GetProperty(ByVal szProperty As String, _
  rgvArg() As Variant, ByVal cvArg As Long) As Variant
    
    Select Case szProperty
    Case "AddRow"
        CsctlCls_GetProperty = dcctlMbr.AddRow
    Case "Clear"
        Call dcctlMbr.Clear
    Case "DeleteRow"
        Call dcctlMbr.DeleteRow(rgvArg(0))
    Case "InsertRow"
        Call dcctlMbr.InsertRow(rgvArg(0))
    Case "JumpToPosition"
        Call dcctlMbr.JumpToPosition(rgvArg(0))
    Case "Color"
        CsctlCls_GetProperty = dcctlMbr.Color(rgvArg(0), rgvArg(1))
    Case "ColumnWidth"
        CsctlCls_GetProperty = dcctlMbr.ColumnWidth(rgvArg(0))
    Case "Data"
        If cvArg < 3 Then
            CsctlCls_GetProperty = dcctlMbr.Data(rgvArg(0), rgvArg(1))
        Else
            CsctlCls_GetProperty = dcctlMbr.Data(rgvArg(0), rgvArg(1), rgvArg(3))
        End If
    Case Else
        CsctlCls_GetProperty = CallByName(dcctlMbr, szProperty, VbGet)
    End Select
    
End Function

Private Sub CsctlCls_SetProperty(ByVal szProperty As String, _
  rgvArg() As Variant, ByVal cvArg As Long, ByVal vValue As Variant)
    
    Select Case szProperty
    Case "Color"
        dcctlMbr.Color(rgvArg(0), rgvArg(1)) = vValue
    Case "ColumnWidth"
        dcctlMbr.ColumnWidth(rgvArg(0)) = vValue
    Case "Data"
        If cvArg < 3 Then
            dcctlMbr.Data(rgvArg(0), rgvArg(1)) = vValue
        Else
            dcctlMbr.Data(rgvArg(0), rgvArg(1), rgvArg(2)) = vValue
        End If
    Case Else
        Call CallByName(dcctlMbr, szProperty, VbLet, vValue)
    End Select
    
End Sub


'------------------------
' Control event handlers
'------------------------

Private Sub dcctlMbr_Change(ByVal nID As Long, ByVal nX As Long, ByVal nY As Long)

    xClicked = nX
    yClicked = nY
    
    Call csviewMbr.HandleEvent(ehdChange, szControlMbr, "Change", True, nID, nX, nY)
    
End Sub

Private Sub dcctlMbr_Destroy(ByVal nID As Long)
    Call csviewMbr.HandleEvent(ehdDestroy, szControlMbr, "Destroy", False, nID)
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceLine("Cslst.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceLine(szPanelMbr + "." + szControlMbr + ".Terminate")
End Sub






