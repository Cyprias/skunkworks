VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CstimerCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' Control Surrogate for Timer


'--------------
' Private data
'--------------

Private szPanelMbr As String, szControlMbr As String
Private csviewMbr As CsviewCls
Private WithEvents timerMbr As DecalInput.Timer
Attribute timerMbr.VB_VarHelpID = -1

Private ehdTimeout As Long


'------------------
' Friend functions
'------------------

Friend Sub Init(ByVal szPanel As String, ByVal szControl As String, _
  ByVal csview As CsviewCls, ByVal elemControl As IXMLDOMElement)
    
    szPanelMbr = szPanel
    szControlMbr = szControl
    Set csviewMbr = csview
    Set timerMbr = New DecalInput.Timer
    
    timerMbr.Tag = elemControl.getAttribute("tag")
    
End Sub

Friend Property Get dcctl() As DecalInput.Timer
    Set dcctl = timerMbr
End Property


'------------------------
' Control event handlers
'------------------------

Private Sub timerMbr_Timeout(ByVal Source As DecalInput.IDecalTimer)
    Call csviewMbr.HandleEvent(ehdTimeout, szControlMbr, "Timeout", False, Source)
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceLine("Cstimer.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceLine(szPanelMbr + "." + szControlMbr + ".Terminate")
End Sub



