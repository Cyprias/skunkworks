VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CsviewCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' Scripting surrogate for View objects.


'--------------
' Private data
'--------------

' Event Handler Defined constants:
Private Const ehdUnknown = 0
Private Const ehdNotDefined = 1
Private Const ehdDefined = 2

Private szPanelMbr As String
Private WithEvents vwMbr As View
Attribute vwMbr.VB_VarHelpID = -1
Private iviewMbr As IView
Private fClose As Boolean

Private WithEvents swxsite As SWXSCRIPTLib.swxsite
Attribute swxsite.VB_VarHelpID = -1
Private WithEvents Hooks As ACHooks
Attribute Hooks.VB_VarHelpID = -1
Private objScript As Object

Private ehdActivate As Long
Private ehdDeactivate As Long
Private ehdSize As Long
Private ehdSizing As Long

Private ehdAC3DRegionChanged As Long
Private ehdChatClickIntercept As Long
Private ehdChatParserIntercept As Long
Private ehdChatTextIntercept As Long
Private ehdContainerOpened As Long
Private ehdMessageProcessed As Long
Private ehdObjectDestroyed As Long
Private ehdObjectSelected As Long
Private ehdStatusTextIntercept As Long


'------------------
' Friend functions
'------------------

Friend Sub Init(ByVal szPanel As String, ByVal vw As View)
    Call TraceEnter("Csview.Init", SzQuote(szPanel))
    
    szPanelMbr = szPanel
    Set vwMbr = vw
    Set iviewMbr = vw
    fClose = False
    
    Set Hooks = site.Hooks
    
    Call TraceExit("Csview.Init")
End Sub

Friend Sub LoadScript(sel As IXMLDOMSelection, ByVal szDir As String)
    Call TraceEnter("Csview.LoadScript")
    On Error GoTo LError
    
    If sel.Length = 0 Then GoTo LExit
    
'   Create a script host for the specified scripting language.
    Dim elemScript As IXMLDOMElement, szLang As String
    Set elemScript = sel(0)
    szLang = SzFromVNull(elemScript.getAttribute("language"))
    Call Assert(szLang <> "", "No script language specified.")
    Set swxsite = New SWXSCRIPTLib.swxsite
    swxsite.fTrace = swoptions.fTrace
    swxsite.hwndHost = site.hwnd
    Call swxsite.Begin(szLang, szPanelMbr, False, "")
    
'   Expose API objects.
    Dim uiapi As UiapiCls
    Set uiapi = New UiapiCls
    Call uiapi.Init(szPanelMbr, szDir, Me)
    Call swxsite.addObject("uiapi", uiapi, True)
    Call swxsite.addObject("pluginsite", New PlugsiteCls, False)
    Call swxsite.addObject("hooks", Hooks, False)
    Call swxsite.addObject("view", Me, False)
    
'   Load all the script code.
    For Each elemScript In sel
        Dim szSrc As String, szCode As String
        szSrc = SzFromVNull(elemScript.getAttribute("src"))
        If szSrc <> "" Then
        '   Expand szSrc relative to the script directory.
            Dim szDirSave As String
            szDirSave = CurDir()
            Call SetCurrentDirectory(szDir)
            szSrc = fso.GetAbsolutePathName(szSrc)
            Call SetCurrentDirectory(szDirSave)
        '   Load the contents of the script file.
            Call TraceLine("szSrc = " + SzQuote(szSrc))
            szCode = SzReadFile(szSrc)
            Call swxsite.AddCode(szCode, SzLeaf(szSrc), 1)
        End If
    '   Load the inline script code, if any.
        szCode = elemScript.Text
        If szCode <> "" Then
            Call swxsite.AddCode(szCode, szPanelMbr, 1)
        End If
    Next elemScript
    
    Set objScript = swxsite.objScript
    
    GoTo LExit
    
LError:
    Call ReportError(Err.Source + ": " + Err.Description, "Csview.LoadScript")
    On Error Resume Next
    If Not swxsite Is Nothing Then
        Call swxsite.End
    End If
    Set swxsite = Nothing
    Set objScript = Nothing
    
LExit:
    Call TraceExit("Csview.LoadScript")
End Sub

Friend Sub AddControl(ByVal szControl As String, ByVal dcctl As Object)
    Call TraceEnter("Csview.AddControl", SzQuote(szControl))
    
    If Not swxsite Is Nothing Then
    '   Expose the control object to the script.
        Call swxsite.addObject(szControl, dcctl, False)
    End If
    
    Call TraceExit("Csview.AddControl")
End Sub

Friend Sub StartScript()
    Call TraceEnter("Csview.StartScript")
    
    If Not swxsite Is Nothing Then
    '   Run the script's init code.
        Call swxsite.Run(False)
    End If
    
    Call TraceExit("Csview.StartScript")
End Sub

Friend Function CallFunction(ByVal szFunction As String, _
  ByRef v1 As Variant, ByRef v2 As Variant, ByRef v3 As Variant, _
  ByRef v4 As Variant, ByRef v5 As Variant) As Variant
    Call TraceEnter("Csview.CallFunction", SzQuote(szFunction))
    
    If Not swxsite Is Nothing Then
        On Error Resume Next
        If IsEmpty(v1) Then
            CallFunction = CallByName(objScript, szFunction, VbMethod)
        ElseIf IsEmpty(v2) Then
            CallFunction = CallByName(objScript, szFunction, VbMethod, v1)
        ElseIf IsEmpty(v3) Then
            CallFunction = CallByName(objScript, szFunction, VbMethod, v1, v2)
        ElseIf IsEmpty(v4) Then
            CallFunction = CallByName(objScript, szFunction, VbMethod, v1, v2, v3)
        ElseIf IsEmpty(v5) Then
            CallFunction = CallByName(objScript, szFunction, VbMethod, v1, v2, v3, v4)
        Else
            CallFunction = CallByName(objScript, szFunction, VbMethod, v1, v2, v3, v4, v5)
        End If
        If Err.Number <> 0 Then
            Call ReportError(Err.Description, szPanelMbr + "." + szFunction)
        End If
    End If
    
    If fClose Then Call swcontrols.RemoveControls(szPanelMbr)
    
    Call TraceExit("Csview.CallFunction")
End Function

Friend Sub HandleEvent(ByRef ehd As Long, _
  ByVal szControl As String, ByVal szEvent As String, ByVal fSend As Boolean, _
  Optional ByVal v1 As Variant = Empty, Optional ByVal v2 As Variant = Empty, _
  Optional ByVal v3 As Variant = Empty, Optional ByVal v4 As Variant = Empty)
'    Call TraceEnter("Csview.HandleEvent", SzQuote(szControl + "_" + szEvent) + ", " + CStr(fSend))
    
    If Not swxsite Is Nothing Then
        Dim szHandler As String
        szHandler = szControl + "_" + szEvent
        
    '   Is there a handler defined for this event?
        If ehd = ehdUnknown Then
            If swxsite.FFunctionDefined(szHandler) Then
                ehd = ehdDefined
            Else
                ehd = ehdNotDefined
            End If
        End If
        If ehd = ehdDefined Then
        '   Yes; call it.
            On Error Resume Next
            If IsEmpty(v1) Then
                fSend = fSend And Not FFromV(CallByName(objScript, szHandler, VbMethod))
            ElseIf IsEmpty(v2) Then
                fSend = fSend And Not FFromV(CallByName(objScript, szHandler, VbMethod, v1))
            ElseIf IsEmpty(v3) Then
                fSend = fSend And Not FFromV(CallByName(objScript, szHandler, VbMethod, v1, v2))
            ElseIf IsEmpty(v4) Then
                fSend = fSend And Not FFromV(CallByName(objScript, szHandler, VbMethod, v1, v2, v3))
            Else
                fSend = fSend And Not FFromV(CallByName(objScript, szHandler, VbMethod, v1, v2, v3, v4))
            End If
            If Err.Number <> 0 Then
                Call ReportError(Err.Description, szPanelMbr + "." + szHandler)
            End If
        End If
    End If
    
    If fSend Then
        Call swcontrols.SendControlMsg(szPanelMbr, szControl)
    End If
    
    If fClose Then Call swcontrols.RemoveControls(szPanelMbr)
    
'    Call TraceExit("Csview.HandleEvent")
End Sub

Friend Function MarkForClose()
    Call TraceEnter("Csview.MarkForClose")
    
    fClose = True
    
    Call TraceExit("Csview.MarkForClose")
End Function

Friend Sub ShutDown()
    Call TraceEnter("Csview.ShutDown")
    
    If Not swxsite Is Nothing Then
        On Error Resume Next
        Call swxsite.End
        Set swxsite = Nothing
        Set objScript = Nothing
    End If
    
    Call TraceExit("Csview.ShutDown")
End Sub


'------------------------
' Swxsite event handlers
'------------------------

Private Sub swxsite_ScriptError(ByVal strFile As String, ByVal iline As Long, _
  ByVal strSrc As String, ByVal strDesc As String, ByVal errcode As Long)
    
    If strDesc <> "" Then
        Call ReportError("Script error in " + strFile + _
          " line " + CStr(iline) + ": " + strDesc)
    End If
    
End Sub


'----------------------
' Hooks event handlers
'----------------------

Private Sub Hooks_AC3DRegionChanged(ByVal left As Long, ByVal top As Long, ByVal right As Long, ByVal bottom As Long)
    Call HandleEvent(ehdAC3DRegionChanged, "hooks", "AC3DRegionChanged", False, left, top, right, bottom)
End Sub

Private Sub Hooks_ChatClickIntercept(ByVal bstrText As String, ByVal lID As Long, bEat As Boolean)
    Call HandleEvent(ehdChatClickIntercept, "hooks", "ChatClickIntercept", False, bstrText, lID, bEat)
End Sub

Private Sub Hooks_ChatParserIntercept(ByVal bstrText As String, bEat As Boolean)
    Call HandleEvent(ehdChatParserIntercept, "hooks", "ChatParserIntercept", False, bstrText, bEat)
End Sub

Private Sub Hooks_ChatTextIntercept(ByVal bstrText As String, ByVal lColor As Long, ByVal lTarget As Long, bEat As Boolean)
    Call HandleEvent(ehdChatTextIntercept, "hooks", "ChatTextIntercept", False, bstrText, lColor, lTarget, bEat)
End Sub

Private Sub Hooks_ContainerOpened(ByVal lGUID As Long)
    Call HandleEvent(ehdContainerOpened, "hooks", "ContainerOpened", False, lGUID)
End Sub

Private Sub Hooks_MessageProcessed(ByVal pbData As Long, ByVal dwSize As Long)
    Call HandleEvent(ehdMessageProcessed, "hooks", "MessageProcessed", False, pbData, dwSize)
End Sub

Private Sub Hooks_ObjectDestroyed(ByVal lGUID As Long)
    Call HandleEvent(ehdObjectDestroyed, "hooks", "ObjectDestroyed", False, lGUID)
End Sub

Private Sub Hooks_ObjectSelected(ByVal lGUID As Long)
    Call HandleEvent(ehdObjectSelected, "hooks", "ObjectSelected", False, lGUID)
End Sub

Private Sub Hooks_StatusTextIntercept(ByVal bstrText As String, bEat As Boolean)
    Call HandleEvent(ehdStatusTextIntercept, "hooks", "StatusTextIntercept", False, bstrText, bEat)
End Sub


'---------------------
' View event handlers
'---------------------

Private Sub vwMbr_Activate()
    Call HandleEvent(ehdActivate, "view", "Activate", False)
End Sub

Private Sub vwMbr_Deactivate()
    Call HandleEvent(ehdDeactivate, "view", "Deactivate", False)
End Sub

Private Function vwMbr_Size() As Boolean
    
'   Open-code this handler in order to pass back the return value.
'   Call HandleEvent(ehdSize, "view", "Size", False)
    
    vwMbr_Size = False
    
    If Not swxsite Is Nothing Then
        Dim szHandler As String
        szHandler = "view_Size"
        
    '   Is there a handler defined for this event?
        If ehdSize = ehdUnknown Then
            If swxsite.FFunctionDefined(szHandler) Then
                ehdSize = ehdDefined
            Else
                ehdSize = ehdNotDefined
            End If
        End If
        If ehdSize = ehdDefined Then
        '   Yes; call it.
            On Error Resume Next
            vwMbr_Size = FFromV(CallByName(objScript, szHandler, VbMethod))
            If Err.Number <> 0 Then
                Call ReportError(Err.Description, szPanelMbr + "." + szHandler)
            End If
        End If
    End If
    
End Function

Private Sub vwMbr_Sizing(ByVal left As Long, ByVal top As Long, ByVal width As Long, ByVal height As Long)
    Call HandleEvent(ehdSizing, "view", "Sizing", False, left, top, width, height)
End Sub


'------------------------
' IView wrapper functions
'------------------------

Public Property Get Control(ByVal strName As String) As IControl
    Set Control = iviewMbr.Control(strName)
End Property

Public Property Set Control(ByVal strName As String, ByVal newVal As IControl)
    Set iviewMbr.Control(strName) = newVal
End Property

Public Property Get Title() As String
    Title = iviewMbr.Title
End Property

Public Property Let Title(ByVal newVal As String)
    iviewMbr.Title = newVal
End Property

Public Sub Alert()
    Call iviewMbr.Alert
End Sub

Public Sub SetIcon(ByVal icon As Long, ByVal iconlibrary As Variant)
    Call iviewMbr.SetIcon(icon, iconlibrary)
End Sub

Public Sub Activate()
    Call iviewMbr.Activate
End Sub

Public Sub Deactivate()
    Call iviewMbr.Deactivate
End Sub

Public Property Get Position() As RectCls
    Set Position = New RectCls
    Position.rect = iviewMbr.Position
End Property

Public Property Set Position(ByVal newVal As RectCls)
    iviewMbr.Position = newVal.rect
End Property

Public Function LoadControl(ByVal pParent As ILayerSite, ByVal nID As Long, ByVal pXMLSource As Object) As Long
    LoadControl = iviewMbr.LoadControl(pParent, nID, pXMLSource)
End Function

Public Sub LoadSchema(ByVal strXMLSchema As String)
    Call iviewMbr.LoadSchema(strXMLSchema)
End Sub

Public Property Get Activated() As Boolean
    Activated = iviewMbr.Activated
End Property

Public Property Let Activated(ByVal newVal As Boolean)
    iviewMbr.Activated = newVal
End Property

Public Property Get Alpha() As Long
    Alpha = iviewMbr.Alpha
End Property

Public Property Let Alpha(ByVal Alpha As Long)
    iviewMbr.Alpha = Alpha
End Property

Public Property Get Transparent() As Boolean
    Transparent = iviewMbr.Transparent
End Property

Public Property Let Transparent(ByVal newVal As Boolean)
    iviewMbr.Transparent = newVal
End Property


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceLine("Csview.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceLine(szPanelMbr + ".Terminate")
End Sub





