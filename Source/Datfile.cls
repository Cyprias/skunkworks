VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DatfileCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' AC Data file (Cell.dat or Portal.dat)


'--------------
' Private data
'--------------

Private Type EmfentType
    unk1 As Long
    emfid As Long
    lfa As Long
    cb As Long
    unk2 As Long
    unk3 As Long
End Type

Private Type DatdirType
    rglfaSubdir(61) As Long
    cemfent As Long
    rgemfent(61) As EmfentType
End Type

Private Const lfaCbBlock As Long = &H144&
Private Const lfaLfaDatdirRoot As Long = &H160&

Private hFile As Long, hmap As Long, pbMap As Long
Private cbBlock As Long
Private lfaDatdirRoot As Long


'------------
' Properties
'------------


'---------
' Methods
'---------

Public Sub Create(ByVal szFile As String, Optional ByVal fWrite As Boolean = False)
    
    Call DebugLine("Opening " + szFile + "...")
    hFile = CreateFile(szFile, _
      IIf(fWrite, GENERIC_READ Or GENERIC_WRITE, GENERIC_READ), _
      FILE_SHARE_READ Or FILE_SHARE_WRITE, 0, _
      OPEN_EXISTING, FILE_FLAG_RANDOM_ACCESS, hNil)
    Call Assert(hFile <> hInvalid, "Could not open DAT file " + szFile + ".")
    
    hmap = CreateFileMapping(hFile, 0, _
      IIf(fWrite, PAGE_READWRITE, PAGE_READONLY), _
      0, 0, vbNullString)
    If hmap <> hInvalid Then
        pbMap = MapViewOfFile(hmap, _
        IIf(fWrite, FILE_MAP_WRITE, FILE_MAP_READ), _
        0, 0, 0)
        If pbMap = 0 Then
            Call DebugLine("MapViewOfFile failed for " + SzLeaf(szFile) + ".")
            Call CloseHandle(hmap)
            hmap = hInvalid
        End If
    Else
        Call DebugLine("CreateFileMapping failed for " + SzLeaf(szFile) + ".")
    End If
    If pbMap = 0 Then
        Call DebugLine("Using file I/O functions.")
    End If
    
    cbBlock = DwFromLfa(lfaCbBlock)
    lfaDatdirRoot = DwFromLfa(lfaLfaDatdirRoot)
    
End Sub

Public Function EmfOpen(ByVal emfid As Long) As EmfCls
    
    Dim emfent As EmfentType
    If FLookUpEmfid(emfent, emfid, lfaDatdirRoot) Then
        Set EmfOpen = New EmfCls
        Call EmfOpen.Create(Me, emfid, emfent.lfa, emfent.cb)
    Else
        Set EmfOpen = Nothing
    End If
    
End Function

Public Sub ReadPv(ByVal pv As Long, ByVal cb As Long, ByRef lfaCur As Long)
    
    Do While cb > 0
        Call Assert(lfaCur <> 0, "End of embedded file.")
        Dim lfaBlock As Long, lfaLimBlock As Long, cbChunk As Long
        lfaBlock = lfaCur - (lfaCur Mod cbBlock)
        lfaLimBlock = lfaBlock + cbBlock
        cbChunk = lfaLimBlock - lfaCur
        If cbChunk > cb Then cbChunk = cb
        Call ReadFromLfa(pv, lfaCur, cbChunk)
        pv = pv + cbChunk
        cb = cb - cbChunk
        lfaCur = lfaCur + cbChunk
        If lfaCur = lfaLimBlock Then
            lfaCur = DwFromLfa(lfaBlock)
            If lfaCur <> 0 Then lfaCur = lfaCur + 4
        End If
    Loop
    
End Sub

Public Sub SkipCb(ByVal cb As Long, ByRef lfaCur As Long)
    
    Do While cb > 0
        Call Assert(lfaCur <> 0, "End of embedded file.")
        Dim lfaBlock As Long, lfaLimBlock As Long, cbChunk As Long
        lfaBlock = lfaCur - (lfaCur Mod cbBlock)
        lfaLimBlock = lfaBlock + cbBlock
        cbChunk = lfaLimBlock - lfaCur
        If cbChunk > cb Then cbChunk = cb
        cb = cb - cbChunk
        lfaCur = lfaCur + cbChunk
        If lfaCur = lfaLimBlock Then
            lfaCur = DwFromLfa(lfaBlock)
            If lfaCur <> 0 Then lfaCur = lfaCur + 4
        End If
    Loop
    
End Sub

Public Sub WritePv(ByVal pv As Long, ByVal cb As Long, ByRef lfaCur As Long)
    
    Do While cb > 0
        Call Assert(lfaCur <> 0, "End of embedded file.")
        Dim lfaBlock As Long, lfaLimBlock As Long, cbChunk As Long
        lfaBlock = lfaCur - (lfaCur Mod cbBlock)
        lfaLimBlock = lfaBlock + cbBlock
        cbChunk = lfaLimBlock - lfaCur
        If cbChunk > cb Then cbChunk = cb
        Call WriteToLfa(lfaCur, pv, cbChunk)
        pv = pv + cbChunk
        cb = cb - cbChunk
        lfaCur = lfaCur + cbChunk
        If lfaCur = lfaLimBlock Then
            lfaCur = DwFromLfa(lfaBlock)
            If lfaCur <> 0 Then lfaCur = lfaCur + 4
        End If
    Loop
    
End Sub

Public Function LfaAlignDw(ByVal lfa As Long) As Long
    
    Dim lfaBlock As Long, lfaLimBlock As Long
    lfaBlock = lfa - (lfa Mod cbBlock)
    lfaLimBlock = lfaBlock + cbBlock
    
    lfa = (lfa + 3) And &HFFFFFFFC
    
    If lfa = lfaLimBlock Then
        lfa = DwFromLfa(lfaBlock)
        If lfa <> 0 Then lfa = lfa + 4
    End If
    
    LfaAlignDw = lfa
End Function


Public Sub EnumEmfid(obj As Object, Optional ByVal lfaDatdir As Long = 0)
    
    If lfaDatdir = 0 Then lfaDatdir = lfaDatdirRoot
    
    Dim datdir As DatdirType
    Call ReadDatdir(datdir, lfaDatdir)
    
    Dim i As Long
    For i = 0 To datdir.cemfent - 1
        If datdir.rglfaSubdir(0) <> 0 Then
            Call EnumEmfid(obj, datdir.rglfaSubdir(i))
        End If
        Call obj.AcceptEmfid(datdir.rgemfent(i).emfid)
    Next i
    
    If datdir.rglfaSubdir(0) <> 0 Then
        Call EnumEmfid(obj, datdir.rglfaSubdir(datdir.cemfent))
    End If
    
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    hFile = hInvalid
    hmap = hInvalid
    pbMap = 0
    
End Sub

Private Sub Class_Terminate()
    
    If pbMap <> 0 Then Call UnmapViewOfFile(pbMap)
    If hmap <> hInvalid Then Call CloseHandle(hmap)
    If hFile <> hInvalid Then Call CloseHandle(hFile)
    
End Sub


Private Function DwFromLfa(ByVal lfa As Long) As Long
    
    If pbMap <> 0 Then
        Call CopyMemFromPv(DwFromLfa, pbMap + lfa, 4)
    Else
        Dim cbRead As Long
        Call SetFilePointer(hFile, lfa, 0, FILE_BEGIN)
        Call ReadFile(hFile, DwFromLfa, 4, cbRead, 0)
    End If
    
End Function

Private Sub ReadFromLfa(ByVal pv As Long, ByVal lfa As Long, ByRef cb As Long)
    
    If pbMap <> 0 Then
        Call CopyPv(pv, pbMap + lfa, cb)
    Else
        Call SetFilePointer(hFile, lfa, 0, FILE_BEGIN)
        Call ReadFilePb(hFile, pv, cb, cb, 0)
    End If
    
End Sub

Private Sub WriteToLfa(ByVal lfa As Long, ByVal pv As Long, ByRef cb As Long)
    
    If pbMap <> 0 Then
        Call CopyPv(pbMap + lfa, pv, cb)
    Else
        Call SetFilePointer(hFile, lfa, 0, FILE_BEGIN)
        Call WriteFilePb(hFile, pv, cb, cb, 0)
    End If
    
End Sub


Private Function FLookUpEmfid(ByRef emfentOut As EmfentType, _
  ByVal emfid As Long, ByVal lfaDatdir As Long) As Boolean
    
    Dim datdir As DatdirType
    Call ReadDatdir(datdir, lfaDatdir)
'   Call PxDatdir(lfaDatdir, datdir)
    
    Dim i As Long
    For i = 0 To datdir.cemfent - 1
        If FUgt(datdir.rgemfent(i).emfid, emfid) Then Exit For
        If datdir.rgemfent(i).emfid = emfid Then
        '   Found it.
            emfentOut = datdir.rgemfent(i)
            FLookUpEmfid = True
            Exit Function
        End If
    Next i
    
'   Not found.  If there's a subdir, recurse to search that.
    If datdir.rglfaSubdir(0) <> 0 Then
        FLookUpEmfid = FLookUpEmfid(emfentOut, emfid, datdir.rglfaSubdir(i))
    Else
        FLookUpEmfid = False
    End If
    
End Function

Private Sub ReadDatdir(datdir As DatdirType, ByVal lfa As Long)
    
    Call ReadPv(VarPtr(datdir), Len(datdir), lfa + 4)
    
End Sub

Private Function FUgt(ByVal dw1 As Long, ByVal dw2 As Long) As Boolean
    
    If (dw1 >= 0) = (dw2 >= 0) Then
    '   Signs are the same; just compare.
        FUgt = (dw1 > dw2)
    Else
    '   Signs are different.  The one that's negative is the bigger one unsigned.
        FUgt = (dw1 < 0)
    End If
    
End Function

Private Sub PxDatdir(ByVal lfa As Long, datdir As DatdirType)
    
    Call DebugLine("")
    Call DebugLine("lfa:     " + SzHex(lfa, 8))
    Call DebugLine("cemfent: " + CStr(datdir.cemfent))
    Call DebugLine( _
      SzPadLeft("subdir", 8) + "    " + _
      SzPadLeft("emfid", 8) + "  " + _
      SzPadLeft("lfa", 8) + "  " + _
      SzPadLeft("cb", 8) _
      )
    Call DebugLine( _
      SzPadLeft("------", 8) + "    " + _
      SzPadLeft("-----", 8) + "  " + _
      SzPadLeft("---", 8) + "  " + _
      SzPadLeft("--", 8) _
      )
    
    Dim i As Long
    For i = 0 To datdir.cemfent - 1
        Call DebugLine( _
          SzHex(datdir.rglfaSubdir(i), 8) + "    " + _
          SzHex(datdir.rgemfent(i).emfid, 8) + "  " + _
          SzHex(datdir.rgemfent(i).lfa, 8) + "  " + _
          SzHex(datdir.rgemfent(i).cb, 8) + "  " + _
          SzPadLeft(CStr(datdir.rgemfent(i).cb), 5) _
          )
    Next i
    Call DebugLine(SzHex(datdir.rglfaSubdir(datdir.cemfent), 8))

End Sub
















