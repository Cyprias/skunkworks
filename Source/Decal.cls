VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "DecalCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' This class provides access to information about the user's Decal installation.


'--------------
' Private data
'--------------

Private szDirMbr As String


'------------
' Properties
'------------

' Return the full path of the Decal installation directory.
Public Property Get szDir() As String
    szDir = szDirMbr
End Property

' Return the full path of Messages.xml.
Public Property Get szMessagesXml() As String
    
    szMessagesXml = fso.BuildPath(szDirMbr, "Messages.xml")
    
End Property

Public Property Get szVerDecal() As String
    
    szVerDecal = SzExeVerFromFile(fso.BuildPath(szDir, "DenAgent.exe"))
    
End Property

' Return the internal version stamp of Message.xml.
Public Property Get szVerMessages() As String
    
'   Open Messages.xml.
    Dim xmldocMessages As DOMDocument40: Set xmldocMessages = New DOMDocument40
    xmldocMessages.async = False
    Call xmldocMessages.Load(szMessagesXml)
    Call xmldocMessages.setProperty("SelectionLanguage", "XPath")
    
'   Get its revision string.
    Dim elemRevision As IXMLDOMElement
    Set elemRevision = xmldocMessages.documentElement.selectSingleNode("/schema/revision")
    
    szVerMessages = elemRevision.Attributes.getNamedItem("version").nodeValue
    
End Property


'---------
' Methods
'---------

' Return True if the plugin specified by the given CLSID is enabled, False if disabled.
Public Function FPluginEnabled(ByVal szClsid As String) As Boolean
    Call TraceEnter("FPluginEnabled", SzQuote(szClsid))
    
    FPluginEnabled = FEnabled("Plugins", szClsid)
    
    Call TraceExit("FPluginEnabled", CStr(FPluginEnabled))
End Function

' Return True if the filter specified by the given CLSID is enabled, False if disabled.
Public Function FFilterEnabled(ByVal szClsid As String) As Boolean
    Call TraceEnter("FFilterEnabled", SzQuote(szClsid))
    
    FFilterEnabled = FEnabled("NetworkFilters", szClsid)
    
    Call TraceExit("FFilterEnabled", CStr(FFilterEnabled))
End Function


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceEnter("Decal.Initialize")
    
    Dim fFound As Boolean
    fFound = False
    
'   Get the installation path from the registry.
    Dim hkey As Long
    hkey = HkeyOpen(HKEY_LOCAL_MACHINE, "SOFTWARE\Decal\Agent", KEY_QUERY_VALUE)
    If hkey <> hNil Then
        fFound = FGetSzValue(szDirMbr, hkey, "AgentPath")
        Call RegCloseKey(hkey)
    End If
    
    If Not fFound Then
        Call MsgBox("Can't find Decal installation directory.", _
          vbExclamation + vbOKOnly)
    End If
    
    Call TraceExit("Decal.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceEnter("Decal.Terminate")
    
    Call TraceExit("Decal.Terminate")
End Sub


' Read a plugin or filter enable flag from the registry.
Private Function FEnabled(ByVal szCategory As String, ByVal szClsid As String) As Boolean
    
    FEnabled = False
    
    Dim hkey As Long
    hkey = HkeyOpen(HKEY_LOCAL_MACHINE, _
      "SOFTWARE\Decal\" + szCategory + "\" + szClsid, KEY_QUERY_VALUE)
    If hkey <> hNil Then
        Dim dwVal As Long
        If FGetDwordValue(dwVal, hkey, "Enabled") Then
            FEnabled = dwVal
        End If
        Call RegCloseKey(hkey)
    End If
    
End Function






















