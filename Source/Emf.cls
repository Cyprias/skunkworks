VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "EmfCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' EMF: Embedded File in Cell.dat or Portal.dat


'--------------
' Private data
'--------------

Private datfileMbr As DatfileCls
Private emfidMbr As Long
Private lfaFirstMbr As Long, cbMbr As Long
Private lfaCur As Long, ibCur As Long


'------------
' Properties
'------------

Public Property Get emfid() As Long
    emfid = emfidMbr
End Property

Public Property Get ib() As Long
    ib = ibCur
End Property

Public Property Get cb() As Long
    cb = cbMbr
End Property


'---------
' Methods
'---------

Public Sub Create(ByVal datfile As DatfileCls, ByVal emfid As Long, _
  ByVal lfaFirst As Long, ByVal cb As Long)
    
    Set datfileMbr = datfile
    emfidMbr = emfid
    lfaFirstMbr = lfaFirst
    cbMbr = cb
    
    Call Reset
    
End Sub

Public Sub Reset()
    
    lfaCur = lfaFirstMbr + 4
    ibCur = 0
    
End Sub

Public Function BRead() As Byte
    
    Call ReadPv(VarPtr(BRead), 1)
    
End Function

Public Function WRead() As Integer
    
    Call ReadPv(VarPtr(WRead), 2)
    
End Function

Public Function DwRead() As Long
    
    Call ReadPv(VarPtr(DwRead), 4)
    
End Function

Public Function QwRead() As Double
    
    Dim dwLow As Long, dwHigh As Long
    dwLow = DwRead()
    dwHigh = DwRead()
    
    QwRead = CDbl(dwHigh) * 4294967296# + dwLow
    
End Function

Public Function FloatRead() As Single
    
    Call ReadPv(VarPtr(FloatRead), 4)
    
End Function

Public Sub ReadPv(ByVal pv As Long, ByVal cb As Long)
    
    Call datfileMbr.ReadPv(pv, cb, lfaCur)
    ibCur = ibCur + cb
    
End Sub

Public Sub SkipCb(ByVal cb As Long)
    
    Debug.Assert cb >= 0
    
    Call datfileMbr.SkipCb(cb, lfaCur)
    ibCur = ibCur + cb
    
End Sub

Public Sub AlignDw()
    
    lfaCur = datfileMbr.LfaAlignDw(lfaCur)
    ibCur = (ibCur + 3) And &HFFFFFFFC
    
End Sub

' Read and return a string from the current message.
Public Function SzRead() As String
    
    Dim cch As Long, ich As Long
    cch = WRead()
    SzRead = ""
    For ich = 0 To cch - 1
        SzRead = SzRead + Chr(BRead())
    Next ich
    Call AlignDw
    
End Function

' Bizarrely, strings in the spell table are stored with the hex digits of
' each character swapped.  I.e. 'a' is &H16 instead of &H61 as it should be.
' Read a string of this sort, swapping the digits back to their proper order.
Public Function SzReadSwapped() As String
    
    Dim cch As Long, ich As Long
    cch = WRead()
    SzReadSwapped = ""
    For ich = 0 To cch - 1
        Dim ch As Byte
        ch = BRead()
        ch = Int((ch And &HF0) / &H10) Or ((ch And &HF) * &H10)
        SzReadSwapped = SzReadSwapped + Chr(ch)
    Next ich
    Call AlignDw
    
End Function

' Read and return a short string from the current message.
Public Function SzReadShort() As String
    
    Dim cch As Long, ich As Long
    cch = BRead()   ' Or is this a PackedWORD?
    SzReadShort = ""
    For ich = 0 To cch - 1
        SzReadShort = SzReadShort + Chr(BRead())
    Next ich
    
End Function


Public Function WriteB(ByVal b As Byte)
    
    Call WritePv(VarPtr(b), 1)
    
End Function

Public Sub WritePv(ByVal pv As Long, ByVal cb As Long)
    
    Call datfileMbr.WritePv(pv, cb, lfaCur)
    ibCur = ibCur + cb
    
End Sub


Public Sub Px(Optional ByVal cbLim As Long = 0)
    
    If cbLim = 0 Then cbLim = cbMbr
    
    Call DebugLine("cb:        " + CStr(cbMbr))
    Call Reset
    
    Dim ib As Long, dib As Long
    For ib = 0 To cbLim - 4 Step 16
        Dim szLine As String, szDecimal As String
        szLine = SzHex(ib, 6) + ": "
        szDecimal = ""
        
        For dib = 0 To 12 Step 4
            If ib + dib + 4 <= cbLim Then
                Dim dw As Long: dw = DwRead()
                szLine = szLine + SzHex(dw, 8) + "  "
                szDecimal = szDecimal + SzPadLeft(CStr(dw), 12)
            End If
        Next dib
        
    '   szLine = szLine + String(50 - Len(szLine), " ") + szDecimal
        Call DebugLine(szLine)
    Next ib
    
    Call Reset
    
End Sub


'----------
' Privates
'----------














