<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  version="1.0">


<xsl:template match="events">

	<xsl:choose>
	
	<xsl:when test="@target = 'Acsh.cls'">
		<xsl:call-template name="GenAcshCls">
			<xsl:with-param name="events" select="."/>
		</xsl:call-template>
	</xsl:when>
	
	<xsl:when test="@target = 'Skev.cls'">
		<xsl:call-template name="GenSkevCls">
			<xsl:with-param name="events" select="."/>
		</xsl:call-template>
	</xsl:when>
	
	<xsl:when test="@target = 'T.js'">
		<xsl:call-template name="GenTJs">
			<xsl:with-param name="events" select="."/>
		</xsl:call-template>
	</xsl:when>
	
	<xsl:when test="@target = 'TVB.frm'">
		<xsl:call-template name="GenTVB">
			<xsl:with-param name="events" select="."/>
		</xsl:call-template>
	</xsl:when>
	
	</xsl:choose>
	
</xsl:template>


<xsl:template name="GenAcshCls">
  <xsl:param name="events"/>
VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "AcshCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' ACSH: ACScript Event Handler.
' This is a wrapper class that connects ACScript-style event handlers to
' the native skapi event API.
#
' -------------------------------------------------------------------
' !!!!!!!!!!!!!!!!!!!!!!   R E A D   T H I S   !!!!!!!!!!!!!!!!!!!!!!
' -------------------------------------------------------------------
' !!!     This file is generated automatically by Events.xsl.     !!!
' !!!    Any edits you make to it by hand WILL be overwritten.    !!!
' !!!          Make your changes to Events.xml instead.           !!!
' -------------------------------------------------------------------
#
'------------
' Properties
'------------
#
Public szHandler As String      ' Name of handler function.
Public fStandard As Boolean     ' True if this is a standard ACMsg_* handler.
#
#
'----------
' Handlers
'----------
#
<!-- Generate ACSH methods. -->
<xsl:for-each select="$events/event">
	<xsl:call-template name="GenAcsh">
		<xsl:with-param name="event" select="."/>
	</xsl:call-template>
</xsl:for-each>
#
'----------
' Privates
'----------
#
Private Sub Class_Initialize()
	#
    MemStats.cacsh = MemStats.cacsh + 1
	#
End Sub
#
Private Sub Class_Terminate()
	#
    MemStats.cacsh = MemStats.cacsh - 1
	#
End Sub
#
Private Function OidFromAco(ByVal aco As AcoCls) As Long
	#
    If aco Is Nothing Then
        OidFromAco = oidNil
    Else
        OidFromAco = aco.oid
    End If
	#
End Function
#
Private Sub CheckForError()
	#
    Dim errno As Long
    errno = Err.Number
    If errno &lt;&gt; 0 Then
        If errno = 438 And fStandard Then
        '   Don't report missing standard handlers.
        ElseIf errno = 440 Then
        '   Already reported by ScriptError event.
        Else
            Call skapi.RaiseHandlerError(szHandler, Err.Description, Err.Number)
        End If
        Call Err.Clear
    End If
	#
End Sub
#
</xsl:template>

<xsl:template name="GenAcsh">
  <xsl:param name="event"/>
	#
	Public Sub <xsl:value-of select="$event/@name"/>(<xsl:call-template name="GenParams">
			<xsl:with-param name="event" select="."/>
		</xsl:call-template>)
	    On Error Resume Next
	    <xsl:value-of select="$event/acsh"/>
	    Call CallByName(skapi.objScript, szHandler, VbMethod<xsl:if test="$event/acsh/@params != ''">, <xsl:value-of select="$event/acsh/@params"/></xsl:if>)
	    Call CheckForError
	End Sub
	#
</xsl:template>


<xsl:template name="GenSkevCls">
  <xsl:param name="events"/>
VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SkevCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' Skev: SkunkWorks Events
' This class provides COM event sourcing for game events.
#
' -------------------------------------------------------------------
' !!!!!!!!!!!!!!!!!!!!!!   R E A D   T H I S   !!!!!!!!!!!!!!!!!!!!!!
' -------------------------------------------------------------------
' !!!     This file is generated automatically by Events.xsl.     !!!
' !!!    Any edits you make to it by hand WILL be overwritten.    !!!
' !!!          Make your changes to Events.xml instead.           !!!
' -------------------------------------------------------------------
#
'--------
' Events
'--------
#
<!-- Generate event declarations. -->
<xsl:for-each select="$events/event">
	<xsl:call-template name="GenEvent">
		<xsl:with-param name="event" select="."/>
	</xsl:call-template>
</xsl:for-each>
#
'------------------
' Friend functions
'------------------
#
<!-- Generate Raise functions. -->
<xsl:for-each select="$events/event">
	<xsl:call-template name="GenRaise">
		<xsl:with-param name="event" select="."/>
	</xsl:call-template>
</xsl:for-each>
#
'----------
' Privates
'----------
#
Private Sub Class_Initialize()
    Call TraceEnter("Skev.Initialize")
	#
    Call TraceExit("Skev.Initialize")
End Sub
#
Private Sub Class_Terminate()
    Call TraceEnter("Skev.Terminate")
	#
    Call TraceExit("Skev.Terminate")
End Sub
#
</xsl:template>

<xsl:template name="GenEvent">
  <xsl:param name="event"/>
	Public Event <xsl:value-of select="$event/@name"/>(<xsl:call-template name="GenParams">
			<xsl:with-param name="event" select="."/>
		</xsl:call-template>)
</xsl:template>

<xsl:template name="GenRaise">
  <xsl:param name="event"/>
	#
	Friend Sub Raise<xsl:value-of select="$event/@name"/>(<xsl:call-template name="GenParams">
			<xsl:with-param name="event" select="."/>
		</xsl:call-template>)
		On Error Resume Next
		#
		If swoptions.pxe &gt;= <xsl:value-of select="$event/@pxe"/><xsl:if test="not($event/@pxe)">pxeMost</xsl:if> Then
			Call skapi.OutputLine("<xsl:value-of select="$event/@name"/>("<xsl:call-template name="GenTraceParams">
				<xsl:with-param name="event" select="."/>
			</xsl:call-template> + ")", swoptions.opmPxe)
		End If
		#
	'	Tell WaitEvent if this is the event it's waiting for.
		MessageCrackers.evidLast = evid<xsl:value-of select="$event/@name"/>
		mpevidcevent(evid<xsl:value-of select="$event/@name"/>) = mpevidcevent(evid<xsl:value-of select="$event/@name"/>) + 1
		#
		RaiseEvent <xsl:value-of select="$event/@name"/>(<xsl:call-template name="GenParams">
			<xsl:with-param name="event" select="."/>
			<xsl:with-param name="fTypes" select="false()"/>
		</xsl:call-template>)
		If Err.Number &lt;&gt; 0 Then
			Call skapi.RaiseHandlerError("skev.<xsl:value-of select="$event/@name"/>", Err.Description, Err.Number)
			Call Err.Clear
		End If
		#
	'	Call the registered event handler(s).
		Dim llobj As LlobjCls
		Set llobj = mpevidllobj(evid<xsl:value-of select="$event/@name"/>)
		Do Until llobj Is Nothing
			Call llobj.obj.<xsl:value-of select="$event/@name"/>(<xsl:call-template name="GenParams">
				<xsl:with-param name="event" select="."/>
				<xsl:with-param name="fTypes" select="false()"/>
			</xsl:call-template>)
			If Err.Number &lt;&gt; 0 Then
				Call skapi.RaiseHandlerError(TypeName(llobj.obj) + ".<xsl:value-of select="$event/@name"/>", Err.Description, Err.Number)
				Call Err.Clear
			End If
			Set llobj = llobj.llobjNext
		Loop
		#
	End Sub
	#
</xsl:template>


<xsl:template name="GenTJs">
  <xsl:param name="events"/>
// Welcome to t.js for SKUNKWORKS (tm) by Greg Kusnick
//
// t.js is provided by Therkyn of Morningthaw, who just couldnt let go of
// it from ACScript.  These messages are intended to be summary in nature.
// So if sometimes you'd like to see more info, go ahead and change the text
// to see what you'd like to see.
//
// This is a VERY noisy script.  That can be solved by commenting out the
// output lines in the event that's causing the noise.
//
// Some have been commented out by default.
//
// Good Luck!
// therkyn
// therkyn@hotmail.com
#
// -------------------------------------------------------------------
// !!!!!!!!!!!!!!!!!!!!!!   R E A D   T H I S   !!!!!!!!!!!!!!!!!!!!!!
// -------------------------------------------------------------------
// !!!     This file is generated automatically by Events.xsl.     !!!
// !!!    Any edits you make to it by hand WILL be overwritten.    !!!
// !!!          Make your changes to Events.xml instead.           !!!
// -------------------------------------------------------------------
#
// Set up your handlers variable as a global variable
var h = new myHandlers();
#
// Shortcut function for outputting to Screen "sSay = Screen Say"
function sSay(text)
{
	skapi.OutputLine(text,opmChatWnd);
}
#
<!-- Generate handler functions. -->
<xsl:for-each select="$events/event">
	<xsl:call-template name="GenHandler">
		<xsl:with-param name="event" select="."/>
	</xsl:call-template>
</xsl:for-each>
#
<!-- Generate handler class constructor. -->
function myHandlers()
{
<xsl:for-each select="$events/event">
	<xsl:call-template name="GenConstructor">
		<xsl:with-param name="event" select="."/>
	</xsl:call-template>
</xsl:for-each>
}
#
function fAddHandlers()
{
<xsl:for-each select="$events/event">
	<xsl:call-template name="GenAddHandler">
		<xsl:with-param name="event" select="."/>
	</xsl:call-template>
</xsl:for-each>
}
#
function fRemoveHandlers()
{
<xsl:for-each select="$events/event">
	<xsl:call-template name="GenRemoveHandler">
		<xsl:with-param name="event" select="."/>
	</xsl:call-template>
</xsl:for-each>
}
#
function main()
{
	fAddHandlers();
	#
	skapi.OutputLine("Welcome to t.js, starting now...",opmChatWnd);
	#
	while (1)
		{skapi.WaitEvent(10)}
	#
	fRemoveHandlers();
}
#
</xsl:template>

<xsl:template name="GenHandler">
  <xsl:param name="event"/>
	#
	function MyHandlers_<xsl:value-of select="$event/@name"/>(<xsl:call-template name="GenParams">
			<xsl:with-param name="event" select="."/>
			<xsl:with-param name="fTypes" select="false()"/>
		</xsl:call-template>)
	{
	    <xsl:value-of select="$event/tjs"/>
	}
	#
</xsl:template>

<xsl:template name="GenConstructor">
  <xsl:param name="event"/>
	this.<xsl:value-of select="$event/@name"/> = MyHandlers_<xsl:value-of select="$event/@name"/>;
</xsl:template>

<xsl:template name="GenAddHandler">
  <xsl:param name="event"/>
	skapi.AddHandler(evid<xsl:value-of select="$event/@name"/>, h);
</xsl:template>

<xsl:template name="GenRemoveHandler">
  <xsl:param name="event"/>
	skapi.RemoveHandler(evid<xsl:value-of select="$event/@name"/>, h);
</xsl:template>


<xsl:template name="GenTVB">
  <xsl:param name="events"/>
VERSION 5.00
Begin VB.Form formTVB 
   Caption         =   "TVB"
   ClientHeight    =   1185
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   2130
   LinkTopic       =   "Form1"
   ScaleHeight     =   1185
   ScaleWidth      =   2130
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Interval        =   100
      Left            =   1680
      Top             =   120
   End
End
Attribute VB_Name = "formTVB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
#
' Our handle on the SkunkWorks API object:
Private skapi As SkapiCls
#
' SkunkWorks Events object for receiving game events:
Private WithEvents skev As SkevCls
Attribute skev.VB_VarHelpID = -1
#
Private Sub Form_Load()
	#
'   Instantiate the skapi.
    Set skapi = New SkapiCls
	#
'   Grab a handle to the event source object.
    Set skev = skapi.skev
	#
End Sub
#
Private Sub Timer1_Timer()
	#
'   Must call WaitEvent frequently so events can fire
'   and to keep event queue from overflowing.
    Call skapi.WaitEvent(0)
	#
End Sub
#
' Handler functions for events raised by skapi.skev.
' No need to register these explicitly; VB does that for us when we say WithEvents.

#
<!-- Generate handler functions. -->
<xsl:for-each select="$events/event">
	<xsl:call-template name="GenSkevHandler">
		<xsl:with-param name="event" select="."/>
	</xsl:call-template>
</xsl:for-each>
#
</xsl:template>

<xsl:template name="GenSkevHandler">
  <xsl:param name="event"/>
	#
	Private Sub skev_<xsl:value-of select="$event/@name"/>(<xsl:call-template name="GenParams">
			<xsl:with-param name="event" select="."/>
		</xsl:call-template>)
	    Call skapi.OutputLine("<xsl:value-of select="$event/@name"/>", opmDebugLog)
	End Sub
	#
</xsl:template>


<xsl:template name="GenParams">
  <xsl:param name="event"/>
  <xsl:param name="fTypes" select="true()"/>
	<xsl:for-each select="$event/param">
		<xsl:if test="position() > 1">, </xsl:if>
		<xsl:if test="$fTypes">ByVal </xsl:if><xsl:value-of select="@name"/><xsl:if test="$fTypes"> As <xsl:value-of select="@type"/></xsl:if>
	</xsl:for-each>
</xsl:template>

<xsl:template name="GenTraceParams">
  <xsl:param name="event"/>
	<xsl:for-each select="$event/param"
		><xsl:if test="position() = 1"> + </xsl:if
		><xsl:if test="position() > 1"> + ", " + </xsl:if
		><xsl:choose>
		<xsl:when test="@type = 'Long' or @type = 'Boolean'"
			><xsl:choose>
			<xsl:when test="@trace = 'hex'"
				>"0x" + SzHex(<xsl:value-of select="@name"/>, 8)</xsl:when>
			<xsl:otherwise
				>CStr(<xsl:value-of select="@name"/>)</xsl:otherwise>
			</xsl:choose>
		</xsl:when>
		<xsl:when test="@type = 'Single' or @type = 'Double'"
			>Format(<xsl:value-of select="@name"/>, "0.00")</xsl:when>
		<xsl:when test="@type = 'String'"
			>SzQuote(<xsl:value-of select="@name"/>)</xsl:when>
		<xsl:when test="@type = 'AcoCls' or @type = 'FellowCls'"
			>"0x" + SzHex(<xsl:value-of select="@name"/>.oid, 8) + " " + SzQuote(<xsl:value-of select="@name"/>.szName)</xsl:when>
		<xsl:when test="@type = 'MaplocCls'"
			><xsl:value-of select="@name"/>.sz(3)</xsl:when>
		<xsl:when test="@type = 'SpellCls'"
			>CStr(<xsl:value-of select="@name"/>.spellid)</xsl:when>
		<xsl:otherwise
			>"<xsl:value-of select="@name"/>"</xsl:otherwise>
		</xsl:choose
	></xsl:for-each
></xsl:template>


</xsl:stylesheet>
