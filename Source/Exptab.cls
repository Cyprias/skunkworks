VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ExptabCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' Experience table


'--------------
' Private data
'--------------

Private rgexpMbr() As Double


'------------
' Properties
'------------

Public k1 As Single, k2 As Single
Public fKGober As Boolean


Public Property Get lvlMax() As Long
    lvlMax = UBound(rgexpMbr)
End Property

Public Property Let lvlMax(ByVal lvl As Long)
    ReDim rgexpMbr(lvl)
End Property


Public Property Get rgexp(ByVal lvl As Long) As Double
    
    If lvl < 0 Then
        rgexp = 0
    ElseIf lvl <= lvlMax Then
        rgexp = rgexpMbr(lvl)
    ElseIf fKGober Then
    '   Thanks to Ken Gober for this formula.
        rgexp = Round((((((lvl + 25#) * lvl + 250) * lvl + 1250) * lvl + 3125) * lvl - 4643) / 9 - 0.49, 0)
    Else
    '   Use exponential extrapolation for skills and attrs.
        rgexp = k1 * exp(k2 * lvl)
    End If
    
End Property

Public Property Let rgexp(ByVal lvl As Long, ByVal exp As Double)
    rgexpMbr(lvl) = exp
End Property


'---------
' Methods
'---------

' Binary search for the lvl corresponding to the given exp.
Public Function LvlFromExp(ByVal exp As Double) As Long
    
    Dim lvlFirst As Long, lvlLim As Long
    lvlFirst = 0
    lvlLim = 1000
    Do While lvlFirst + 1 < lvlLim
        Dim lvlTry As Long
        lvlTry = Int((lvlFirst + lvlLim) / 2)
        If rgexp(lvlTry) <= exp Then
            lvlFirst = lvlTry
        Else
            lvlLim = lvlTry
        End If
    Loop
    
    LvlFromExp = lvlFirst
    
End Function


'----------
' Privates
'----------






