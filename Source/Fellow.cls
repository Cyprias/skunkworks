VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "FellowCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


'--------------
' Private data
'--------------

Private oidMbr As Long
Private szNameMbr As String
Private lvlMbr As Long
Private fShareLootMbr As Boolean

Private healthMaxMbr As Long
Private staminaMaxMbr As Long
Private manaMaxMbr As Long
Private healthCurMbr As Long
Private staminaCurMbr As Long
Private manaCurMbr As Long


'------------
' Properties
'------------

Public Property Get oid() As Long
    oid = oidMbr
End Property

Public Property Get szName() As String
Attribute szName.VB_UserMemId = 0
    szName = szNameMbr
End Property

Public Property Get lvl() As Long
    lvl = lvlMbr
End Property

Public Property Get fShareLoot() As Boolean
    fShareLoot = fShareLootMbr
End Property


Public Property Get healthMax() As Long
    healthMax = healthMaxMbr
End Property

Public Property Get staminaMax() As Long
    staminaMax = staminaMaxMbr
End Property

Public Property Get manaMax() As Long
    manaMax = manaMaxMbr
End Property

Public Property Get healthCur() As Long
    healthCur = healthCurMbr
End Property

Public Property Get staminaCur() As Long
    staminaCur = staminaCurMbr
End Property

Public Property Get manaCur() As Long
    manaCur = manaCurMbr
End Property


'---------
' Methods
'---------

Public Sub Assess()
    
    Call skapi.AssessOid(oidMbr)
    
End Sub


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------

Friend Sub Init(ByVal oid As Long, ByVal szName As String, _
  ByVal lvl As Long, ByVal fShareLoot As Boolean, _
  ByVal healthMax As Long, ByVal staminaMax As Long, ByVal manaMax As Long, _
  ByVal healthCur As Long, ByVal staminaCur As Long, ByVal manaCur As Long)
    
    oidMbr = oid
    szNameMbr = szName
    lvlMbr = lvl
    fShareLootMbr = fShareLoot
    
    healthMaxMbr = healthMax
    staminaMaxMbr = staminaMax
    manaMaxMbr = manaMax
    healthCurMbr = healthCur
    staminaCurMbr = staminaCur
    manaCurMbr = manaCur
    
End Sub


'----------
' Privates
'----------














