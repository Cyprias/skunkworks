Attribute VB_Name = "FormUtil"
Option Explicit
' Utility functions for dealing with forms.


'--------------
' Private data
'--------------

Private fInRestoreFormPos As Boolean


'------
' APIs
'------

Public Sub MinimizeForm(formArg As Form)
    
    Call ShowWindow(formArg.hwnd, SW_MINIMIZE)
    
End Sub

Public Sub RestoreForm(formArg As Form)
    
    Call ShowWindow(formArg.hwnd, SW_RESTORE)
    
End Sub

Public Sub SetFormAlwaysOnTop(formArg As Form, _
  Optional ByVal fTopmost As Boolean = True)
    
    Call SetWindowPos(formArg.hwnd, _
      IIf(fTopmost, HWND_TOPMOST, HWND_NOTOPMOST), _
      0, 0, 0, 0, _
      SWP_NOMOVE + SWP_NOSIZE + SWP_SHOWWINDOW)
    
End Sub


Public Sub SaveFormPosition(formArg As Form, _
  Optional ByVal szName As String = "", _
  Optional ByVal szApp As String = "")
    
    If szName = "" Then szName = formArg.Caption
    If szApp = "" Then szApp = App.EXEName
    
    If Not fInRestoreFormPos Then
        Call SaveSetting(szApp, "Windows", szName, _
          CStr(formArg.Left) + "," + CStr(formArg.Top) + "," + _
          CStr(formArg.Width) + "," + CStr(formArg.Height) + "," + _
          CStr(formArg.WindowState))
    End If
    
End Sub

Public Sub RestoreFormPosition(formArg As Form, _
  Optional ByVal szName As String = "", _
  Optional ByVal fSize As Boolean = True, _
  Optional ByVal szApp As String = "")
    fInRestoreFormPos = True
    
    If szName = "" Then szName = formArg.Caption
    If szApp = "" Then szApp = App.EXEName
    
    Dim sz As String
    sz = GetSetting(szApp, "Windows", szName)
    If sz <> "" Then
        Dim rgsz() As String
        Dim X As Long, Y As Long, dx As Long, dy As Long, ws As Long
        rgsz = Split(sz, ",")
        X = CLng(rgsz(0))
        Y = CLng(rgsz(1))
        dx = CLng(rgsz(2))
        dy = CLng(rgsz(3))
        If UBound(rgsz) >= 4 Then
            ws = CLng(rgsz(4))
        Else
            ws = vbNormal
        End If
        
        If fSize Then
            If ws = vbNormal Then
                Call formArg.Move(X, Y, dx, dy)
            Else
                formArg.WindowState = ws
            End If
        Else
            Call formArg.Move(X, Y)
        End If
    End If
    
    fInRestoreFormPos = False
End Sub


