VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "GameDataCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


Public szName As String
Public model As Long
Public icon As Long
Public iconOverlay As Long
Public iconUnderlay As Long
Public mcm As Long
Public oty As Long
Public szPlural As String
Public citemMax As Long
Public cpackMax As Long
Public cpyValue As Long
Public distApproach As Single
Public ioc As Long
Public cuseLeft As Long
Public cuseMax As Long
Public citemStack As Long
Public citemMaxStack As Long
Public acoContainer As AcoCls
Public acoWearer As AcoCls
Public eqm As Long
Public eqmWearer As Long
Public workmanship As Single
Public burden As Long
Public spellid As Long
Public oidMonarch As Long
Public material As Long


Private Sub Class_Initialize()
    
    citemStack = 1
    citemMaxStack = 1
    
End Sub


