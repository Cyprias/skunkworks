Attribute VB_Name = "Gen"
Option Explicit
' This module implements the console's Regen Crackers and Regen SkapiDefs functions.


'--------------
' Private data
'--------------

Private szMessageCrackersXml As String, tModMessageCrackersXml As Date
Private szGenCrackersXsl As String, tModGenCrackersXsl As Date
Private szMessageCrackersBas As String

Private Const cchTab As Long = 4


'------------------
' Public functions
'------------------

' Check to see if message crackers need to be regenerated, and if so, regenerate them.
' If fForceRegen is True, then skip the check and unconditionally regenerate.
' When regenerate is done, set fQuitOut to True to tell the console to exit.
' If no regeneration is done, set fQuitOut to False.
Public Sub CheckCrackers(ByRef fQuitOut As Boolean, ByVal fForceRegen As Boolean)
    
    fQuitOut = False
    
'   Collect revision info on the input files (Messages.xml, MessageCrackers.xml, and
'   GenCrackers.xsl).
    Call GetNamesAndDates
    
    If fForceRegen Then
    '   User invoke Regen Crackers from config menu; don't bother checking dates.
        If fVBInstalled Then
            If Not formSWConsole.FConfirmYesNo("Regenerate message crackers now?") Then Exit Sub
        Else
            If Not formSWConsole.FConfirmYesNo( _
              "After regenerating crackers you must recompile Skapi.dll.  " + _
              "SkunkWorks can't find a Visual Basic compiler on your system.  " + _
              "Are you sure you want to regenerate?", _
              False, IDI_EXCLAMATION) Then Exit Sub
        End If
    Else
    '   Autocheck on startup.  Check revision info against the revision stamps
    '   compiled into MessageCrackers.bas.
        Dim szVerMessages As String, szTModMessageCrackers As String, szTModGenCrackers As String
        Call skapi.GetCrackerVersionInfo(szVerMessages, szTModMessageCrackers, szTModGenCrackers)
        Dim fOK As Boolean
        fOK = (StrComp(szVerMessages, decalapp.szVerMessages, vbTextCompare) >= 0)
    '   If fDev is enabled, check dates of cracker sources.
    '   Otherwise just check Messages.xml.
        If swoptions.fDev Then
            fOK = fOK And _
              szTModMessageCrackers = CStr(tModMessageCrackersXml) And _
              szTModGenCrackers = CStr(tModGenCrackersXsl)
        End If
    '   If all dates are OK, we're done.
        If fOK Then Exit Sub
        
    '   Crackers are out of date; prompt user for confirmation.
        Dim szMsg As String, fDontAsk As Boolean, ibutton As Long
        If fVBInstalled Then
            szMsg = _
                "This version of SkunkWorks was compiled with a different version of " + _
                "Messages.xml than the one you now have installed.  For best results, " + _
                "you should regenerate message crackers and rebuild Skapi.dll." + vbCrLf + _
                vbCrLf + _
                "Do you want to regenerate message crackers now?" + vbCrLf + _
                "If you choose No, some script events may not work properly."
            
            ibutton = formMsgBox.IbuttonMsgBox(szMsg, "Yes|No|Cancel", 0, "", _
              IDI_QUESTION, True, fDontAsk, formSWConsole)
            
            Select Case ibutton
            Case 0      ' Yes
                If fDontAsk Then swoptions.fCheckMsgVer = False
            '   Fall through to regen code.
            Case 1      ' No
                If fDontAsk Then swoptions.fCheckMsgVer = False
                Exit Sub
            Case Else   ' Cancel
                fQuitOut = True
                Exit Sub
            End Select
        Else
            szMsg = _
                "This version of SkunkWorks was compiled with a different version of " + _
                "Messages.xml than the one you now have installed.  For best results, " + _
                "you should update to a newer version of Skapi.dll." + vbCrLf + _
                vbCrLf + _
                "Do you want to continue with this version?" + vbCrLf + _
                "If you choose OK, some script events may not work properly."
            
            ibutton = formMsgBox.IbuttonMsgBox(szMsg, "OK|Cancel", 0, "", _
              IDI_QUESTION, True, fDontAsk, formSWConsole)
            
            Select Case ibutton
            Case 0      ' OK
                If fDontAsk Then swoptions.fCheckMsgVer = False
                Exit Sub
            Case Else   ' Cancel
                fQuitOut = True
                Exit Sub
            End Select
        End If
    End If
    
'   Do the regen.
    Call GenCrackersEx
    fQuitOut = True
    
    Call formSWConsole.Alert( _
      "Message crackers have been regenerated.  SkunkWorks will now exit." + vbCrLf + _
      vbCrLf + _
      "See the Setup section of the documentation for further instructions.")
    
'   This was annoying me, so I took it out.
''   Bring up Regen section of the docs.
'    Call ShowURL(SzURLFromSzFile(fso.BuildPath(swoptions.szDocsDir, "Setup.html")) + "#_Regen2")
    
End Sub


' Check to see if SkapiDefs.* need to be regenerated, and if so, regenerate them.
' If fForceRegen is True, then skip the check and unconditionally regenerate.
Public Sub CheckSkapiDefs(ByVal fForceRegen As Boolean)
    
'   Bring up the "Regenerate" window for visual feedback.
    Call formShowText.ShowText("", "Regenerate SkapiDefs", vbModeless, formSWConsole)
    Call formShowText.Refresh
    DoEvents
    
    Call CheckFile("SkapiDefs.xml", "SkapiDefsCs.xsl", swoptions.szLibDir, "SkapiDefs.cs", fForceRegen)
    Call CheckFile("SkapiDefs.xml", "SkapiDefsJs.xsl", swoptions.szLibDir, "SkapiDefs.js", fForceRegen)
    Call CheckFile("SkapiDefs.xml", "SkapiDefsVbs.xsl", swoptions.szLibDir, "SkapiDefs.vbs", fForceRegen)
    Call CheckFile("SkapiDefs.xml", "SkapiDefsVbs.xsl", swoptions.szSourceDir, "SkapiDefs.bas", fForceRegen)
    Call CheckFile("SkapiDefs.xml", "SkapiDefs.xsl", swoptions.szDocsDir, "SkapiDefs.html", fForceRegen)
    
    Call Unload(formShowText)
    
End Sub

Public Sub GenSkev()
    
'   Bring up the "Regenerate" window for visual feedback.
    Call formShowText.ShowText("", "Regenerate Events", vbModeless, formSWConsole)
    Call formShowText.Refresh
    DoEvents
    
    Call CheckFile("Events.xml", "Events.xsl", swoptions.szSourceDir, "Acsh.cls", True)
    Call CheckFile("Events.xml", "Events.xsl", swoptions.szSourceDir, "Skev.cls", True)
    Call CheckFile("Events.xml", "Events.xsl", swoptions.szLibDir, "T.js", True)
    Call CheckFile("Events.xml", "Events.xsl", swoptions.szLibDir, "TVB.frm", True)
    
    Call Unload(formShowText)
    
End Sub


'----------
' Privates
'----------

' Check one language of SkapiDefs to see if it needs to be regenerated, and if so,
' regenerate it.
' If fForceRegen is True, then skip the check and unconditionally regenerate.
Private Sub CheckFile(ByVal szFileXml As String, ByVal szFileXsl As String, _
  ByVal szDirOut As String, ByVal szFileOut As String, ByVal fForceRegen As Boolean)
    
    szFileXml = fso.BuildPath(swoptions.szSourceDir, szFileXml)
    szFileXsl = fso.BuildPath(swoptions.szSourceDir, szFileXsl)
    szFileOut = fso.BuildPath(szDirOut, szFileOut)
    
    If fso.FileExists(szFileXml) And fso.FileExists(szFileXsl) Then
        Dim tModXml As Date: tModXml = fso.GetFile(szFileXml).DateLastModified
        Dim tModXsl As Date: tModXsl = fso.GetFile(szFileXsl).DateLastModified
        Dim tModOut As Date
        If fso.FileExists(szFileOut) Then
            tModOut = fso.GetFile(szFileOut).DateLastModified
        Else
            tModOut = tNil
        End If
        
        If tModXml > tModOut Or tModXsl > tModOut Or fForceRegen Then
            Call GenFile(szFileXml, szFileXsl, szFileOut, True)
        End If
    End If

End Sub

' Generate one language of SkapiDefs.
Private Sub GenFile(ByVal szFileXml As String, ByVal szFileXsl As String, _
  ByVal szFileOut As String, ByVal fShow As Boolean)
    
'   Open the common XML source.
    Dim xmldocXml As DOMDocument40: Set xmldocXml = New DOMDocument40
    xmldocXml.preserveWhiteSpace = True
    xmldocXml.async = False
    Call xmldocXml.Load(szFileXml)
    
'   Set the target attribute.
    Call xmldocXml.documentElement.setAttribute("target", SzLeaf(szFileOut))
    
'   Open the language-specific transform.
    Dim xmldocXsl As DOMDocument40: Set xmldocXsl = New DOMDocument40
    xmldocXsl.preserveWhiteSpace = True
    xmldocXsl.async = False
    Call xmldocXsl.Load(szFileXsl)
    
'   Open the output file.
    Dim ts As TextStream
    Set ts = fso.CreateTextFile(szFileOut, True)
    If FEqSzFile(SzExtension(szFileOut), "bas") Then
    '   VB module header boilerplate.
        Call ts.WriteLine("Attribute VB_Name = " + SzQuote(SzLeafNoExt(szFileOut)))
    End If
    
'   Carry out the transform.
    Call TransformDoc(ts, xmldocXml, xmldocXsl, SzExtension(szFileOut), fShow)
    
    Call ts.WriteLine("")
    Call ts.WriteLine("")
    Call ts.Close
    
End Sub

' Carry out an XSL transform, writing the result to tsOut.
Private Sub TransformDoc(tsOut As TextStream, _
  xmldocXml As DOMDocument40, xmldocXsl As DOMDocument40, _
  ByVal szExt As String, ByVal fShow As Boolean)
    
'   Do the tranform, getting the result as a string.
    Dim szResult As String
    szResult = xmldocXml.transformNode(xmldocXsl)
    
'   Pretty-print the result to clean up line spacing and indentation and such.
'   (Hard to get right in XSL without making the XSL file unreadable.)
    Select Case LCase(szExt)
    Case "cs", "js"
        szResult = SzPrettyPrintJs(szResult)
    Case "bas", "cls", "frm", "vbs"
        szResult = SzPrettyPrintVB(szResult)
    Case "html"
        szResult = SzMassageHtml(szResult)
    End Select
    
    If fShow Then
    '   Show the result in the feedback window.
        formShowText.textShow.Text = szResult
        Call formShowText.Refresh
        DoEvents
    End If
    
'   Write it to the file.
    Call tsOut.Write(szResult)
    
End Sub

' Collect revision info for the cracker input and output files.
Private Sub GetNamesAndDates()
    
    szMessageCrackersXml = fso.BuildPath(swoptions.szSourceDir, "MessageCrackers.xml")
    If fso.FileExists(szMessageCrackersXml) Then
        tModMessageCrackersXml = fso.GetFile(szMessageCrackersXml).DateLastModified
    Else
        tModMessageCrackersXml = 0
    End If
    
    szGenCrackersXsl = fso.BuildPath(swoptions.szSourceDir, "GenCrackers.xsl")
    If fso.FileExists(szGenCrackersXsl) Then
        tModGenCrackersXsl = fso.GetFile(szGenCrackersXsl).DateLastModified
    Else
        tModGenCrackersXsl = 0
    End If
    
    szMessageCrackersBas = fso.BuildPath(swoptions.szSourceDir, "MessageCrackers.bas")
    
End Sub

' Unconditionally regenerate message crackers.
Private Sub GenCrackersEx()
    
'   Open Messages.xml.
    Dim xmldocMessages As DOMDocument40: Set xmldocMessages = New DOMDocument40
    xmldocMessages.async = False
    Call xmldocMessages.Load(decalapp.szMessagesXml)
    Call xmldocMessages.setProperty("SelectionLanguage", "XPath")
    If swoptions.fDev Then
        Dim elemRev As IXMLDOMElement
        Set elemRev = xmldocMessages.selectSingleNode("/schema/revision")
        Call xmldocMessages.Save(swoptions.SzFileInAppDir("Messages-" + _
          elemRev.getAttribute("version") + ".xml"))
    End If
    
'   Open MessageCrackers.xml.
    Dim xmldocCrackers As DOMDocument40: Set xmldocCrackers = New DOMDocument40
    xmldocCrackers.preserveWhiteSpace = True
    xmldocCrackers.async = False
    Call xmldocCrackers.Load(szMessageCrackersXml)
    Call xmldocCrackers.setProperty("SelectionLanguage", "XPath")
    
'   Annotate root node of crackers with file revision dates.
    Dim elemCrackers As IXMLDOMElement
    Set elemCrackers = xmldocCrackers.documentElement
    Call elemCrackers.setAttribute("tModMessageCrackers", CStr(tModMessageCrackersXml))
    Call elemCrackers.setAttribute("tModGenCrackers", CStr(tModGenCrackersXsl))
    
'   Construct a combined XML doc containing both messages and crackers.
    Dim xmldocCombined As DOMDocument40: Set xmldocCombined = New DOMDocument40
    Call xmldocCombined.setProperty("SelectionLanguage", "XPath")
    Dim elemRoot As IXMLDOMElement, elemMessages As IXMLDOMElement
    Set elemRoot = xmldocCombined.createElement("root")
    Set elemMessages = xmldocCombined.createElement("messages")
    Call elemMessages.appendChild(xmldocMessages.documentElement)
    Call elemRoot.appendChild(elemMessages)
    Call elemRoot.appendChild(elemCrackers)
    Call xmldocCombined.appendChild(elemRoot)
    
'   Gather up any message definitions in the Crackers tree and move them to
'   the Messages tree.  (This is for pseudo-messages generated by the plugin.)
    Dim elemDst As IXMLDOMElement
    Set elemDst = elemMessages.selectSingleNode("schema/messages")
    Dim sel As IXMLDOMSelection
    Set sel = elemCrackers.selectNodes("message")
    Do
        Dim elemMessage As IXMLDOMElement
        Set elemMessage = sel.nextNode
        If elemMessage Is Nothing Then Exit Do
        
        Call elemDst.appendChild(elemMessage)
    Loop
    
'   Open GenCrackers.xsl, which contains the transformations.
    Dim xmldocStylesheet As DOMDocument40: Set xmldocStylesheet = New DOMDocument40
    xmldocStylesheet.preserveWhiteSpace = True
    xmldocStylesheet.async = False
    Call xmldocStylesheet.Load(szGenCrackersXsl)
    
'   Open the output file (MessageCrackers.bas).
    Dim ts As TextStream
    Set ts = fso.CreateTextFile(szMessageCrackersBas, True)
    Call ts.WriteLine("Attribute VB_Name = " + SzQuote(SzLeafNoExt(szMessageCrackersBas)))
    
'   Bring up the "Regen" window for visual feedback.
    Call formShowText.ShowText("", "Regenerate Message Crackers", _
      vbModeless, formSWConsole)
    Call formShowText.Refresh
    DoEvents
    
'   Generate the dispatch table.
    Dim szResult As String
    szResult = xmldocCombined.transformNode(xmldocStylesheet)
    szResult = SzPrettyPrintVB(szResult)
    formShowText.textShow.Text = szResult
    Call formShowText.Refresh
    DoEvents
    Call ts.Write(szResult)
    
'   Walk the tree to generate the individual crackers.
'   This could be done by the stylesheet but I like the visual feedback.
    Set sel = elemCrackers.selectNodes("cracker")
    Do
        Dim elemCracker As IXMLDOMElement
        Set elemCracker = sel.nextNode
        If elemCracker Is Nothing Then Exit Do
        
    '   Transform this cracker.
        szResult = elemCracker.transformNode(xmldocStylesheet)
        
    '   Prettyprint the result to clean up line spacing and indentation.
        szResult = SzPrettyPrintVB(szResult)
        
    '   Show it in the feedback window.
        formShowText.textShow.Text = szResult
        Call formShowText.Refresh
        DoEvents
        
    '   Write it to the file.
        Call ts.WriteLine("")
        Call ts.Write(szResult)
    Loop
    
'   Clean up.
    Call Unload(formShowText)
    
    Call ts.WriteLine("")
    Call ts.WriteLine("")
    Call ts.Close
    
End Sub

' Prettyprint VB/VBS transform output to clean up line space and indentation.
Private Function SzPrettyPrintVB(ByVal szIn As String) As String
    
    Dim szOut As String
    szOut = ""
    
    Dim ctab As Long, fNeedBlankLine As Boolean, fContinuation As Boolean
    ctab = 0
    fNeedBlankLine = False
    fContinuation = False
    
    Do While szIn <> ""
        Dim szLine As String
        Call SplitSz(szLine, szIn, szIn, vbLf)
        If FSuffixMatchSzI(szLine, vbCr) Then
            szLine = Left(szLine, Len(szLine) - 1)
        End If
        szLine = SzProcessEscapes(SzTrimWhiteSpace(szLine))
        
        If FPrefixMatchSzI(szLine, "<?") Then
        '   Discard XML directives inserted by transformer.
        ElseIf szLine = "" Then
        '   Discard totally blank lines.
        ElseIf szLine = "#" Then
        '   Take note of intentionally blank lines.
            fNeedBlankLine = True
        Else
            If fNeedBlankLine Then
                szOut = szOut + Space(ctab * cchTab) + vbCrLf
                fNeedBlankLine = False
            End If
            
            If FPrefixMatchSzI(szLine, "'") And ctab > 0 Then
                szLine = SzTrimWhiteSpace(Right(szLine, Len(szLine) - 1))
                szOut = szOut + Space((ctab - 1) * cchTab) + _
                  "'" + Space(cchTab - 1) + szLine + vbCrLf
            Else
                If FPrefixMatchSzI(szLine, "End") Or _
                  FPrefixMatchSzI(szLine, "Else") Or _
                  FPrefixMatchSzI(szLine, "Next") Or _
                  FPrefixMatchSzI(szLine, "Loop") Or _
                  FPrefixMatchSzI(szLine, "Case") Or _
                  FPrefixMatchSzI(szLine, "]") Then
                    ctab = ctab - 1
                End If
                
                If fContinuation Then szLine = "  " + szLine
                szOut = szOut + Space(ctab * cchTab) + szLine + vbCrLf
                
                If FPrefixMatchSzI(szLine, "Public Function") Or _
                  FPrefixMatchSzI(szLine, "Public Sub") Or _
                  FPrefixMatchSzI(szLine, "Friend Function") Or _
                  FPrefixMatchSzI(szLine, "Friend Sub") Or _
                  FPrefixMatchSzI(szLine, "Private Function") Or _
                  FPrefixMatchSzI(szLine, "Private Sub") Or _
                  FSuffixMatchSzI(szLine, "Then") Or _
                  FSuffixMatchSzI(szLine, "Else") Or _
                  FPrefixMatchSzI(szLine, "For") Or _
                  FPrefixMatchSzI(szLine, "Do") Or _
                  FPrefixMatchSzI(szLine, "Select") Or _
                  FPrefixMatchSzI(szLine, "Case") Or _
                  FPrefixMatchSzI(szLine, "BEGIN") Or _
                  FPrefixMatchSzI(szLine, "[") Then
                    ctab = ctab + 1
                End If
            End If
            
            fContinuation = FSuffixMatchSzI(szLine, "_")
        End If
    Loop
    
    SzPrettyPrintVB = szOut
End Function

' Prettyprint VB/VBS transform output to clean up line space and indentation.
Private Function SzPrettyPrintJs(ByVal szIn As String) As String
    
    Dim szOut As String, fNeedBlankLine As Boolean
    szOut = ""
    fNeedBlankLine = False
    
    Do While szIn <> ""
        szOut = szOut + SzPPJsStmt(szIn, fNeedBlankLine, False, 0)
    Loop
    
    SzPrettyPrintJs = szOut
End Function

Private Function SzPPJsStmt(ByRef szIn As String, ByRef fNeedBlankLine As Boolean, _
  ByRef fEndBlock As Boolean, ByVal ctab As Long) As String
    
    fEndBlock = False
    
    Dim szOut As String
    
    Do While szIn <> ""
        Dim szLine As String
        Call SplitSz(szLine, szIn, szIn, vbLf)
        If FSuffixMatchSzI(szLine, vbCr) Then
            szLine = Left(szLine, Len(szLine) - 1)
        End If
        szLine = SzProcessEscapes(SzTrimWhiteSpace(szLine))
        
        If FPrefixMatchSzI(szLine, "<?") Then
        '   Discard XML directives inserted by transformer.
        ElseIf szLine = "" Then
        '   Discard totally blank lines.
        ElseIf szLine = "#" Then
        '   Take note of intentionally blank lines.
            fNeedBlankLine = True
        Else
            If fNeedBlankLine Then
                szOut = szOut + Space(ctab * cchTab) + vbCrLf
                fNeedBlankLine = False
            End If
            
            If FPrefixMatchSzI(szLine, "//") Then
                szLine = SzTrimWhiteSpace(Right(szLine, Len(szLine) - 2))
                If ctab > 0 Then
                    szOut = szOut + Space((ctab - 1) * cchTab) + _
                      "//" + Space(cchTab - 2) + szLine + vbCrLf
                Else
                    szOut = szOut + "// " + szLine + vbCrLf
                End If
                
            ElseIf FPrefixMatchSzI(szLine, "case") And ctab > 0 Then
                szOut = szOut + Space((ctab - 1) * cchTab) + szLine + vbCrLf
                Exit Do
                
            ElseIf FPrefixMatchSzI(szLine, "{") And _
              Not FSuffixMatchSzI(szLine, "}") Then
                szOut = szOut + Space(ctab * cchTab) + szLine + vbCrLf
                Do While szIn <> ""
                    Dim fEndBlockInner As Boolean
                    szOut = szOut + SzPPJsStmt(szIn, fNeedBlankLine, fEndBlockInner, ctab)
                    If fEndBlockInner Then Exit Do
                Loop
                Exit Do
                
            ElseIf FSuffixMatchSzI(szLine, "}") And _
              Not FPrefixMatchSzI(szLine, "{") Then
                szOut = szOut + Space(ctab * cchTab) + szLine + vbCrLf
                fEndBlock = True
                Exit Do
                
            Else
                szOut = szOut + Space(ctab * cchTab) + szLine + vbCrLf
                
                If FPrefixMatchSzI(szLine, "function") Or _
                  FPrefixMatchSzI(szLine, "if") Or _
                  FPrefixMatchSzI(szLine, "else") Or _
                  FPrefixMatchSzI(szLine, "for") Or _
                  FPrefixMatchSzI(szLine, "while") Or _
                  FPrefixMatchSzI(szLine, "switch") Then
                    If Not FSuffixMatchSzI(szLine, ";") And _
                      Not FSuffixMatchSzI(szLine, "}") Then
                        szOut = szOut + SzPPJsStmt(szIn, fNeedBlankLine, False, ctab + 1)
                    End If
                End If
                
                Exit Do
            End If
            
        End If
    Loop
    
    SzPPJsStmt = szOut
End Function

' Scan sz for HTML escape characters and convert them to their ASCII equivalents.
Private Function SzProcessEscapes(ByVal sz As String) As String
    
    Dim ich As Long, ichLim As Long
    ich = 0
    Do
        ich = IchInStr(ich, sz, "&")
        If ich = iNil Then Exit Do
        ichLim = IchInStr(ich, sz, ";")
        If ichLim = iNil Then Exit Do
        ichLim = ichLim + 1
        Dim szEsc As String
        szEsc = SzMid(sz, ich, ichLim - ich)
        Select Case szEsc
        Case "&lt;"
            szEsc = "<"
        Case "&gt;"
            szEsc = ">"
        Case "&amp;"
            szEsc = "&"
        End Select
        sz = Left(sz, ich) + szEsc + Right(sz, Len(sz) - ichLim)
        ich = ich + Len(szEsc)
    Loop
    
    SzProcessEscapes = sz
End Function


' Clean up the HTML output produced by SkapiDefs.xsl to conform to SkunkWorks
' documentation standards.
Private Function SzMassageHtml(ByVal sz As String) As String
    
    Dim ich As Long
    
'   Strip out weird header directives.
    Const szDel1 = " xmlns:msxsl=""urn:schemas-microsoft-com:xslt"""
    ich = IchInStr(0, sz, szDel1)
    If ich <> iNil Then
        sz = Left(sz, ich) + Right(sz, Len(sz) - (ich + Len(szDel1)))
    End If
    
    Const szDel2 = "<META http-equiv=""Content-Type"" content=""text/html; charset=UTF-16"">"
    ich = IchInStr(0, sz, szDel2)
    If ich <> iNil Then
        sz = Left(sz, ich) + Right(sz, Len(sz) - (ich + Len(szDel2)))
    End If
    
'   The transform wants to produce labels of the form <a name="text"></a>
'   IE seems to work better with <a name="text/>
'   Search through the file for such labels and make the correction.
    ich = 0
    Do
        ich = IchInStr(ich, sz, "<a name=")
        If ich = iNil Then Exit Do
        
        ich = IchInStr(ich, sz, "></a>")
        sz = Left(sz, ich) + "/>" + Right(sz, Len(sz) - (ich + 5))
        
        DoEvents
    Loop
    
    SzMassageHtml = sz
End Function


' Open a browser window on the given URL.
Private Sub ShowURL(ByVal szURL As String)
    
    Dim hkey As Long
    hkey = HkeyOpen(HKEY_CLASSES_ROOT, "htmlfile\shell\open\command", KEY_QUERY_VALUE)
    If hkey <> hNil Then
        Dim szBrowser As String
        If FGetSzValue(szBrowser, hkey, "") Then
            Call Shell(szBrowser + " " + SzQuote(szURL), vbNormalFocus)
        End If
        Call RegCloseKey(hkey)
    End If
    
End Sub

























