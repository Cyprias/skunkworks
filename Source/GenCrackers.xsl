<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  xmlns:SkunkWorks="http://skusnkworks.sourceforge.net"
  version="1.0">


<!--
This stylesheet is part of a system for automatically generating message decoder 
functions from Messages.xml.  The specific actions to be taken for each message 
are specified in MessageCrackers.xml, which knows what to do with each field, but 
not how they are laid out in the message.  SkunkWorks uses this stylesheet to 
combine MessageCrackers.xml with Messages.xml to produce optimized Visual Basic 
source code for decoding each message.
-->

<!--
Change fTrace to true() to annotate the generated VB source with transformation 
debug output.  The result won't be valid VB, but might help to diagnose 
subtle codegen bugs.
-->
<xsl:variable name="fTrace" select="false()"/>

<xsl:variable name="fPx" select="false()"/>


<!--
Generate code for the top-level message dispatcher.  Also generate cracker 
subroutines for compound types defined by Messages.xml.
-->
<xsl:template match="root">
	<xsl:variable name="messages" select="messages"/>
	<xsl:variable name="crackers" select="crackers"/>
	Option Explicit
	#
	<!--
	Insert warning comment into generated code.  The warning does not apply here.
	-->
	' -------------------------------------------------------------------
	' !!!!!!!!!!!!!!!!!!!!!!   R E A D   T H I S   !!!!!!!!!!!!!!!!!!!!!!
	' -------------------------------------------------------------------
	' !!!  This file is generated automatically by GenCrackers.xsl.   !!!
	' !!!    Any edits you make to it by hand WILL be overwritten.    !!!
	' !!!      Make your changes to MessageCrackers.xml instead.      !!!
	' -------------------------------------------------------------------
	#
	' Revision info for the files used in the generation of this file.
	Public Const szVerMessages As String = "<xsl:value-of select="$messages/schema/revision/@version"/>"
	Public Const szTModMessageCrackers As String = "<xsl:value-of select="$crackers/@tModMessageCrackers"/>"
	Public Const szTModGenCrackers As String = "<xsl:value-of select="$crackers/@tModGenCrackers"/>"
	#
	Public evidLast As Long
	Public mpevidcevent(evidMax-1) As Long
	Public fPxThisMsg As Boolean
	#
	Private tickDelOrphans As Long
	#
	' Extract the message type and subtype (if relevant) from the given mbuf 
	' and call the appropriate cracker function.
	Public Function EvidDispatchEvent(ByVal mbuf As MbufCls) As Long
		evidLast = evidNil
		#
	'	Hex-dump message to debug log as required by prevailing debug options.
		If swoptions.pxm = pxmAll Then Call mbuf.PxMsg(swoptions.fPxWholeMsg)
		fPxThisMsg = False
		#
	'	Construct a unified key from the message type and subtype (if present).
		Dim mty As Long, oidChar As Long
		mty = DwFromWords(mbuf.Get_DWORD(), 0)
		If mty = mtyGameEvent Then
			oidChar = mbuf.Get_DWORD()
			Call mbuf.SkipCb(4)
			mty = mty Or mbuf.Get_DWORD()
		ElseIf mty = mtyClientEvent Then
			If Not otable.acoChar Is Nothing Then
				oidChar = otable.acoChar.oid
			Else
				oidChar = oidNil
			End If
			Call mbuf.SkipCb(4)
			mty = mty Or mbuf.Get_DWORD()
			If swoptions.fPxCTS Then Call mbuf.PxMsg(swoptions.fPxWholeMsg)
		ElseIf Not otable.acoChar Is Nothing Then
			oidChar = otable.acoChar.oid
		Else
			oidChar = oidNil
		End If
		#
	'	Dispatch to the appropriate cracker.
		Select Case mty
			#
			<!-- Generate a case label and function call for each defined cracker. -->
			<xsl:for-each select="$crackers/cracker">
				<xsl:call-template name="GenCase">
					<xsl:with-param name="cracker" select="."/>
				</xsl:call-template>
			</xsl:for-each>
			#
		Case Else
		'	Hex-dump unknown/unhandled messages to debug log as required.
			Select Case swoptions.pxm
			Case pxmUnhandled
				Call mbuf.PxMsg(swoptions.fPxWholeMsg)
			Case pxmUnknown
				If mbuf.FUnknownMty(mty) Then Call mbuf.PxMsg(swoptions.fPxWholeMsg)
			End Select
			#
		End Select
		#
	'	Dispatch raw message handlers, if any.
		On Error Resume Next
		Dim llobj As LlobjCls
		Set llobj = llobjRaw
		Do Until llobj Is Nothing
			If llobj.serial = mty Then
				Call mbuf.ResetRead()
				Call llobj.obj.OnRawServerMessage(mty, mbuf)
				If Err.Number &lt;&gt; 0 Then
					Call skapi.RaiseHandlerError(TypeName(llobj.obj) + ".OnRawServerMessage", Err.Description, Err.Number)
					Call Err.Clear
				End If
			End If
			Set llobj = llobj.llobjNext
		Loop
		On Error Goto 0
		#
		If fPxThisMsg Then Call mbuf.PxMsg(True)
		#
 	'	Check for orphaned ACOs once every minute or so (but not if we're in portal space, 
 	'	since we might be there longer than the 25-second ACO destruction timer).
		If LSubNoOflow(mbuf.tick, tickDelOrphans) >= 60000 And _
		  skapi.plig = pligInWorld Then
			Call otable.DeleteOrphanedAcos()
			tickDelOrphans = mbuf.tick
		End If
		#
	'	Retry failed ID requests.
		If idq.fRequestOutstanding Then
			If LSubNoOflow(mbuf.tick, idq.tickRequest) >= 2000 Then
				Call idq.Timeout(mbuf.tick)
			End If
		End If
		#
		If LSubNoOflow(mbuf.tick, tickMemStats) >= 60000 Then
			Call LogMemStats(mbuf.tick)
		End If
		#
	'	Return the event ID of the last script event fired.
		EvidDispatchEvent = evidLast
	End Function
	#
	' Check to see if the script was started after character login, and if so, 
	' fire a phony OnLogon event.  Required for ACScript compatibility.
	Public Function FFirePhonyLogon() As Boolean
		#
		If Not (skapi.objScript Is Nothing) And _
		  (ctac And ctacCachedLogin) &lt;&gt; 0 And _
		  (ctac And ctacInventoryFromCache) = 0 Then
			<xsl:variable name="eventOnLogon">
				<event name="OnLogon" params="otable.acoChar"/>
			</xsl:variable>
			<xsl:call-template name="FireEvents">
				<xsl:with-param name="events" select="msxsl:node-set($eventOnLogon)/*"/>
			</xsl:call-template>
		'	Set ConnectedToAC bits to indicate post-login script start.
			ctac = ctac Or ctacInventoryFromCache Or ctacObjectsFromCache
			FFirePhonyLogon = True
		Else
			FFirePhonyLogon = False
		End If
		#
	End Function
	#
	' Check to see if any timer events are pending, and if so, fire one.
	Public Function FFireTimer(ByRef cmsecWait As Long) As Boolean
		#
		If timerFirst Is Nothing Then
		'	No timer events scheduled.
			FFireTimer = False
		Else
			Dim cmsec As Long
			cmsec = timerFirst.cmsec
			If cmsec >= 0 Then
			'	Timer has expired; remove it from the list.
				Dim timer As TimerCls
				Set timer = timerFirst
				Call timer.RemoveFromList()
				#
			'	Fire an event.
				<xsl:variable name="eventOnTimer">
					<event name="OnTimer" params="timer"/>
				</xsl:variable>
				<xsl:call-template name="FireEvents">
					<xsl:with-param name="events" select="msxsl:node-set($eventOnTimer)/*"/>
				</xsl:call-template>
				FFireTimer = True
			Else
			'	Timer hasn't expired yet.
				If cmsecWait > -cmsec Then cmsecWait = -cmsec
				FFireTimer = False
			End If
		End If
		#
	End Function
	#
	<xsl:if test="$fPx">
	Private Sub PxField_BYTE(ByVal szField As String, ByVal b As Byte)
		Call DebugLine(SzPadRight(szField + ":", 20) + _
		  "      " + SzHex(b, 2) + "  " + SzPadLeft(CStr(b), 10))
	End Sub
	#
	Private Sub PxField_WORD(ByVal szField As String, ByVal w As Integer)
		Call DebugLine(SzPadRight(szField + ":", 20) + _
		  "    " + SzHex(w, 4) + "  " + SzPadLeft(CStr(w), 10))
	End Sub
	#
	Private Sub PxField_DWORD(ByVal szField As String, ByVal dw As Long)
		Call DebugLine(SzPadRight(szField + ":", 20) + _
		  SzHex(dw, 8) + "  " + SzPadLeft(CStr(dw), 10))
	End Sub
	#
	Private Sub PxField_float(ByVal szField As String, ByVal float As Single)
		Call DebugLine(SzPadRight(szField + ":", 20) + _
		  SzPadLeft(FormatNumber(float, 3), 10))
	End Sub
	#
	Private Sub PxField_double(ByVal szField As String, ByVal dbl As Double)
		Call DebugLine(SzPadRight(szField + ":", 20) + _
		  SzPadLeft(FormatNumber(dbl, 3), 10))
	End Sub
	#
	Private Sub PxField_String(ByVal szField As String, ByVal sz As String)
		Call DebugLine(SzPadRight(szField + ":", 20) + _
		  SzQuote(sz))
	End Sub
	#
	</xsl:if>
	#
	<!-- Generate crackers for the compound types. -->
	<!-- Loop over the compound types defined by Messages.xml. -->
	<xsl:for-each select="$messages/schema/datatypes/type[.//field]">
		<xsl:variable name="type" select="."/>
		
		<!-- Generate a cracker for this type. -->
		<xsl:call-template name="GenTypeCracker">
			<xsl:with-param name="type"		select="$type"/>
			<xsl:with-param name="cracker"	select="$crackers/type[@name = $type/@name]"/>
			<xsl:with-param name="messages"	select="$messages"/>
		</xsl:call-template>
		
	</xsl:for-each>
	
</xsl:template>


<!--
Generate a dispatch case for one cracker, consisting of a case label and a 
function call.  The single parameter cracker is the XML element representing 
the cracker specification from MessageCrackers.xml.
-->
<xsl:template name="GenCase">
  <xsl:param name="cracker"/>
		#
	Case <xsl:call-template name="SzMtyFromCracker">
		  <xsl:with-param name="cracker" select="$cracker"/>
		  </xsl:call-template>	' <xsl:value-of select="$cracker/@desc"/>
		Call <xsl:call-template name="SzFuncNameFromCracker">
		  <xsl:with-param name="cracker" select="$cracker"/>
		  </xsl:call-template>(mbuf, oidChar)
		#
</xsl:template>


<!--
Generate a cracker function for a compound type.  The parameter type is 
the type definition element from Messages.xml; the parameter cracker is the 
corresponding type cracker element (if any) from MessageCrackers.xml.
-->
<xsl:template name="GenTypeCracker">
  <xsl:param name="type"/>
  <xsl:param name="cracker"/>
  <xsl:param name="messages"/>
	
	<xsl:choose>
	<xsl:when test="$cracker[@fSkip]">
		' Skipping type <xsl:value-of select="$type/@name"/>
	</xsl:when>
	<xsl:otherwise>
		<!-- Extract the type name and construct a suitable function name. -->
		<xsl:variable name="szType" select="$type/@name"/>
		<xsl:variable name="szCrackerName">Decode<xsl:value-of select="$szType"/></xsl:variable>
		#
		<!-- Generate the cracker function. -->
		Private Function <xsl:value-of select="$szCrackerName"/>(ByVal mbuf As MbufCls, _
		  ByVal oidChar as Long, ByVal flagsLoc As Long) As <xsl:call-template 
		  name="SzVBTypeFromSzType"><xsl:with-param name="szType" select="$szType"/></xsl:call-template>
			Set <xsl:value-of select="$szCrackerName"/> = Nothing
			#
			<!--
			Generate the actual decoding logic.  We pass the set of message elements 
			(fields, vectors, etc) to be decoded as nodesetElems, and the set of actions 
			we wish to take as nodeActions.
			-->
			<xsl:call-template name="GenCrackerBody">
				<xsl:with-param name="nodesetElems"	select="$type/*"/>
				<xsl:with-param name="nodeActions"	select="$cracker"/>
				<xsl:with-param name="messages"		select="$messages"/>
			</xsl:call-template>
			#
		End Function
	</xsl:otherwise>
	</xsl:choose>
	#
</xsl:template>


<!--
Generate code for a single message cracker.
-->
<xsl:template match="cracker">
	<xsl:variable name="messages" select="/root/messages"/>
	
	<!-- Extract the cracker's message type and subtype. -->
	<xsl:variable name="type" select="@type"/>
	<xsl:variable name="subtype" select="@subtype"/>
	<!-- Construct a suitable function name for the cracker. -->
	<xsl:variable name="szCrackerName"><xsl:call-template name="SzFuncNameFromCracker">
	  <xsl:with-param name="cracker" select="."/>
	  </xsl:call-template></xsl:variable>
	
	<!-- Find the corresponding message definition in Messages.xml.  If it's a subtype 
	     of message F7B0 (Game Event), find the appropriate case definition. -->
	<xsl:variable name="nodeMessage" select="$messages/schema/messages/message[@type = $type]"/>
	<xsl:variable name="rtree">
		<xsl:choose>
		<xsl:when test="$subtype">
			<xsl:copy-of select="$nodeMessage/switch/case[@value = concat('0x', $subtype)]/*"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:copy-of select="$nodeMessage/*"/>
		</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
	<xsl:variable name="nodesetElems" select="msxsl:node-set($rtree)/*"/>
	
	<!-- Generate the cracker function. -->
	' <xsl:value-of select="@desc"/>
	Private Function <xsl:value-of select="$szCrackerName"/>(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
		#
		<xsl:if test="@px = 'true'">Call mbuf.PxMsg(True)</xsl:if>
		<!--
		Generate the actual decoding logic.  We pass the set of message elements 
		(fields, vectors, etc) to be decoded as nodesetElems, and the set of actions 
		we wish to take as nodeActions.  fTrimTail=true means that we can cut short 
		the decoding after the last interesting field, ignoring any uninteresting 
		(a.k.a. "boring") fields that remain.
		-->
		<xsl:call-template name="GenCrackerBody">
			<xsl:with-param name="nodesetElems"	select="$nodesetElems"/>
			<xsl:with-param name="nodeActions"	select="."/>
			<xsl:with-param name="fTrimTail"	select="true()"/>
			<xsl:with-param name="messages"		select="$messages"/>
		</xsl:call-template>
		#
	End Function
	
</xsl:template>


<!--
Combine the message type and subtype of the given cracker into a single 32-bit 
quantity and return a string containing its hexadecimal representation in valid 
Visual Basic syntax.  Sorry for the cramped layout but this needs to expand to 
a single token with no white space.
-->
<xsl:template name="SzMtyFromCracker"><xsl:param 
  name="cracker"/><xsl:call-template name="SzHexVB">
	<xsl:with-param name="szHex"><xsl:value-of 
	  select="$cracker/@type"/><xsl:value-of 
	  select="$cracker/@subtype"/><xsl:if 
	  test="not($cracker/@subtype)">0000</xsl:if></xsl:with-param>
  </xsl:call-template></xsl:template>


<!--
Calculate a suitable function name for the given cracker.  Sorry for the cramped 
layout but this needs to expand to a single token with no white space.
-->
<xsl:template name="SzFuncNameFromCracker"><xsl:param 
  name="cracker"/>Decode<xsl:value-of 
  select="$cracker/@type"/><xsl:if 
  test="$cracker/@subtype">_</xsl:if><xsl:value-of 
  select="$cracker/@subtype"/></xsl:template>


<!--
Generate decoding logic for a message or type cracker.  The parameter 
nodesetElems is the set of message elements (fields, vectors, etc) to be 
decoded; the parameter nodeActions is the set of actions to be taken on 
those elements.  If fTrimTail is true, we can cut short the decoding after 
the last interesting element, ignoring any uninteresting (a.k.a. "boring") 
elements that remain.  fTrimTail=false means that we must fully decode even 
the boring elements (e.g. because we're inside a vector and there might be 
more interesting stuff coming).
-->
<xsl:template name="GenCrackerBody">
  <xsl:param name="nodesetElems"/>
  <xsl:param name="nodeActions"/>
  <xsl:param name="fTrimTail" select="false()"/>
  <xsl:param name="messages"/>
	
	<!-- Generate a code fragment representing the decoded elements. -->
	<xsl:variable name="rtree">
		<xsl:call-template name="CodefragCrackerFromElems">
			<xsl:with-param name="nodesetElems"	select="$nodesetElems"/>
			<xsl:with-param name="nodeActions"	select="$nodeActions"/>
			<xsl:with-param name="fTrimTail"	select="$fTrimTail"/>
			<xsl:with-param name="messages"		select="$messages"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="codefrag" select="msxsl:node-set($rtree)/codefrag"/>
	
	<!-- Expand the code fragment here. -->
	<xsl:call-template name="ExpandCodefrag">
		<xsl:with-param name="codefrag" select="$codefrag"/>
	</xsl:call-template>
	#
	<!-- Check for unused field actions (in case field names have changed in Messages.xml). -->
	<xsl:for-each select="$nodeActions//field">
		<xsl:variable name="szUnique"><xsl:call-template name="SzUniqueFromElem">
		  <xsl:with-param name="node" select="."/>
		  </xsl:call-template></xsl:variable>
		<xsl:choose>
		<xsl:when test="$codefrag/uses[@action = $szUnique]">
		<!--
		'	<xsl:value-of select="$szUnique"/> was used.
		-->
		</xsl:when>
		<xsl:otherwise>
			Error: <xsl:value-of select="$szUnique"/> was not used.
		</xsl:otherwise>
		</xsl:choose>
	</xsl:for-each>
	
</xsl:template>


<!--
Generate decoding logic for the list of message elements (fields, vectors, etc) 
specified by nodesetElems.  The parameter nodeActions is the set of actions 
to be taken on those elements.  If fTrimTail is true, we can cut short the 
decoding after the last interesting element, ignoring any uninteresting (a.k.a. 
"boring") elements that remain.  fTrimTail=false means that we must fully decode 
even the boring elements (e.g. because we're inside a vector and there might be 
more interesting stuff coming).

CodefragCrackerFromElems differs from CodefragFromElems in that this function 
generates a complete cracker body, including initialization, event firing, 
and finalization, whereas CodefragFromElems generates just the raw decoding 
logic.

The generated code is not expanded in line but is returned as an XML element of 
type "codefrag", with attributes cbSkip and fBoring.  Code fragments of this 
sort can be combined and optimized in various ways before finally being 
expanded in line by ExpandCodefrag.
-->
<xsl:template name="CodefragCrackerFromElems">
  <xsl:param name="nodesetElems"/>
  <xsl:param name="nodeActions"/>
  <xsl:param name="fTrimTail"/>
  <xsl:param name="messages"/>
	
	<!-- Generate code for the element list. -->
	<xsl:variable name="rtreeElems">
		<xsl:call-template name="CodefragFromElems">
			<xsl:with-param name="nodesetElems"	select="$nodesetElems"/>
			<xsl:with-param name="nodeActions"	select="$nodeActions"/>
			<xsl:with-param name="fTrimTail"	select="$fTrimTail"/>
			<xsl:with-param name="messages"		select="$messages"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="codefragElems" select="msxsl:node-set($rtreeElems)/codefrag"/>
	
	<!-- Wrap a cracker around it. -->
	<xsl:call-template name="CodefragWrapCracker">
		<xsl:with-param name="codefragElems"	select="$codefragElems"/>
		<xsl:with-param name="nodeActions"		select="$nodeActions"/>
	</xsl:call-template>
	
</xsl:template>


<!--
Given a codefrag (in codefragElems) representing a decoded element list, add 
initialization, event firing, and finalization code as specific by nodeActions 
to produce a complete cracker body.  Return the result as a new codefrag.
-->
<xsl:template name="CodefragWrapCracker">
  <xsl:param name="codefragElems"/>
  <xsl:param name="nodeActions"/>
	
	<codefrag cbSkip="0" fBoring="false">
		<xsl:if test="$fTrace">
			<xsl:attribute name="szTraceIn">CodefragWrapCracker()</xsl:attribute>
			<xsl:attribute name="szTraceOut">CodefragWrapCracker: 0, false</xsl:attribute>
		</xsl:if>
		<xsl:copy-of select="$codefragElems/uses"/>
		
		<xsl:if test="$nodeActions/init">
			#
			<xsl:value-of select="$nodeActions/init"/>
			#
		</xsl:if>
		
		<xsl:call-template name="ExpandCodefrag">
			<xsl:with-param name="codefrag" select="$codefragElems"/>
		</xsl:call-template>
		
		<xsl:if test="$nodeActions/mid">
			#
			<xsl:value-of select="$nodeActions/mid"/>
			#
		</xsl:if>
		
		<!--
		If nodeActions includes an "events" element, pass in the entire element, 
		including text nodes and individual "event" elements, for expansion by 
		FireEvents.  This enables crackers to implement conditional event firing 
		by embedding the "event" elements under If statements and whatnot.
		-->
		<xsl:if test="$nodeActions/events">
			<xsl:call-template name="FireEvents">
				<xsl:with-param name="events" select="$nodeActions/events/child::node()"/>
			</xsl:call-template>
		</xsl:if>
		<!-- Otherwise just pass the collected "event" elements in as a group. -->
		<xsl:if test="$nodeActions/event">
			<xsl:call-template name="FireEvents">
				<xsl:with-param name="events" select="$nodeActions/event"/>
			</xsl:call-template>
		</xsl:if>
		
		<xsl:if test="$nodeActions/final">
			#
			<xsl:value-of select="$nodeActions/final"/>
			#
		</xsl:if>
		
	</codefrag>
	
</xsl:template>


<!--
Generate code to fire one or more script events.  The "events" parameter specifies 
the set of events to fire, and may contain arbitrary VB code (as text nodes) 
intermixed with "event" elements.
-->
<xsl:template name="FireEvents">
  <xsl:param name="events"/>
	#
'	Fire script events.
	<xsl:for-each select="$events">
		<xsl:choose>
		<xsl:when test="name() = 'event'">
			<!-- Event element; expand it. -->
			<xsl:apply-templates select="."/>
		</xsl:when>
		<xsl:when test="self::text()">
			<!-- Text node containing VB source code; spew it out verbatim. -->
			<xsl:value-of select="."/>
		</xsl:when>
		</xsl:choose>
	</xsl:for-each>
	#
</xsl:template>


<!--
Generate code to fire one script event.
-->
<xsl:template match="event">
	
	<!-- Expand any setup code in the event specification. -->
	<xsl:value-of select="."/>
	
	<!-- Fire the event. -->
	Call skev.Raise<xsl:value-of select="@name"/>(<xsl:value-of select="@params"/>)
	
</xsl:template>


<!--
Generate code to exit the cracker function early if no event handler is defined.
-->
<xsl:template name="QuitIfNoHandler">
  <xsl:param name="evid"/>
	If mpevidllobj(evid<xsl:value-of select="$evid"/>) Is Nothing Then Exit Function
</xsl:template>


<!--
Generate decoding logic for the list of message elements (fields, vectors, etc) 
specified by nodesetElems.  The parameter nodeActions is the set of actions 
to be taken on those elements.  If fTrimTail is true, we can cut short the 
decoding after the last interesting element, ignoring any uninteresting (a.k.a. 
"boring") elements that remain.  fTrimTail=false means that we must fully decode 
even the boring elements (e.g. because we're inside a vector and there might be 
more interesting stuff coming).  szMaskVar, if specified, is the name of the 
flags variable to be masked against by mask elements.

CodefragFromElems differs from CodefragCrackerFromElems in that this function 
generates just the raw decoding logic, whereas CodefragCrackerFromElems generates 
a complete cracker body, including initialization, event firing, and finalization.
-->
<xsl:template name="CodefragFromElems">
  <xsl:param name="nodesetElems"/>
  <xsl:param name="nodeActions"/>
  <xsl:param name="fTrimTail"/>
  <xsl:param name="szMaskVar"/>
  <xsl:param name="messages"/>
	
	<!-- Is the set of elements non-empty? -->
	<xsl:choose>
	<xsl:when test="count($nodesetElems) != 0">
		
		<!--
		For fTrimTail to work properly, we have to generate code in reverse order, 
		depth-first and bottom-up.  So start by recursing to generate code for 
		everything but the first element.
		-->
		<xsl:variable name="rtreeTail">
			<xsl:call-template name="CodefragFromElems">
				<xsl:with-param name="nodesetElems"	select="$nodesetElems[1]/following-sibling::*"/>
				<xsl:with-param name="nodeActions"	select="$nodeActions"/>
				<xsl:with-param name="fTrimTail"	select="$fTrimTail"/>
				<xsl:with-param name="szMaskVar"	select="$szMaskVar"/>
				<xsl:with-param name="messages"		select="$messages"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="codefragTail" select="msxsl:node-set($rtreeTail)/codefrag"/>
		
		<!--
		Now generate code for the first element.  If the entire tail was boring, 
		and trimming is in effect, go ahead and enable trimming of the head too.  
		This is how fTrimTail gets properly propagated down into nested maskmaps, 
		so we can stop decoding immediately after the last interesting field, no 
		matter how deeply nested.
		-->
		<xsl:variable name="rtreeHead">
			<xsl:call-template name="CodefragFromElem">
				<xsl:with-param name="nodeElem"		select="$nodesetElems[1]"/>
				<xsl:with-param name="nodesetElems"	select="$nodesetElems"/>
				<xsl:with-param name="nodeActions"	select="$nodeActions"/>
				<xsl:with-param name="fTrimTail"	select="$fTrimTail and $codefragTail/@fBoring = 'true'"/>
				<xsl:with-param name="szMaskVar"	select="$szMaskVar"/>
				<xsl:with-param name="messages"		select="$messages"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="codefragHead" select="msxsl:node-set($rtreeHead)/codefrag"/>
		
		<!-- Finally, combine the two code fragments. -->
		<xsl:call-template name="CodefragCombine">
			<xsl:with-param name="codefrag1"	select="$codefragHead"/>
			<xsl:with-param name="codefrag2"	select="$codefragTail"/>
			<xsl:with-param name="fTrimTail"	select="$fTrimTail"/>
		</xsl:call-template>
		
	</xsl:when>
	<xsl:otherwise>

		<!-- Empty element list (i.e. end of recursion); return the null codefrag. -->
		<codefrag cbSkip="0" fBoring="true"/>
		
	</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>


<!--
Generate decoding logic for the single message element specified by nodeElem.  
nodesetElems is the complete set of elements being decoded; nodeActions is the 
set of actions to be taken, one of which may apply to nodeElem.  fTrimTail and 
szMaskVar are as described for CodefragFromElems.
-->
<xsl:template name="CodefragFromElem">
  <xsl:param name="nodeElem"/>
  <xsl:param name="nodesetElems"/>
  <xsl:param name="nodeActions"/>
  <xsl:param name="fTrimTail"/>
  <xsl:param name="szMaskVar"/>
  <xsl:param name="messages"/>
	
	<!-- Dispatch on element type. -->
	<xsl:choose>
	<xsl:when test="name($nodeElem) = 'vector'">
		
		<xsl:call-template name="CodefragFromVector">
			<xsl:with-param name="vector"		select="$nodeElem"/>
			<xsl:with-param name="nodesetElems"	select="$nodesetElems"/>
			<xsl:with-param name="nodeActions"	select="$nodeActions"/>
			<xsl:with-param name="messages"		select="$messages"/>
		</xsl:call-template>
		
	</xsl:when>
	<xsl:when test="name($nodeElem) = 'maskmap'">
		
		<xsl:call-template name="CodefragFromMaskmap">
			<xsl:with-param name="maskmap"		select="$nodeElem"/>
			<xsl:with-param name="nodesetElems"	select="$nodesetElems"/>
			<xsl:with-param name="nodeActions"	select="$nodeActions"/>
			<xsl:with-param name="fTrimTail"	select="$fTrimTail"/>
			<xsl:with-param name="messages"		select="$messages"/>
		</xsl:call-template>
		
	</xsl:when>
	<xsl:when test="name($nodeElem) = 'mask'">
		
		<xsl:call-template name="CodefragFromMask">
			<xsl:with-param name="mask"			select="$nodeElem"/>
			<xsl:with-param name="nodesetElems"	select="$nodesetElems"/>
			<xsl:with-param name="nodeActions"	select="$nodeActions"/>
			<xsl:with-param name="fTrimTail"	select="$fTrimTail"/>
			<xsl:with-param name="szMaskVar"	select="$szMaskVar"/>
			<xsl:with-param name="messages"		select="$messages"/>
		</xsl:call-template>
		
	</xsl:when>
	<xsl:when test="name($nodeElem) = 'switch'">
		
		<xsl:call-template name="CodefragFromSwitch">
			<xsl:with-param name="switch"		select="$nodeElem"/>
			<xsl:with-param name="nodesetElems"	select="$nodesetElems"/>
			<xsl:with-param name="nodeActions"	select="$nodeActions"/>
			<xsl:with-param name="fTrimTail"	select="$fTrimTail"/>
			<xsl:with-param name="messages"		select="$messages"/>
		</xsl:call-template>
		
	</xsl:when>
	<xsl:when test="name($nodeElem) = 'case'">
		
		<xsl:call-template name="CodefragFromCase">
			<xsl:with-param name="case"			select="$nodeElem"/>
			<xsl:with-param name="nodesetElems"	select="$nodesetElems"/>
			<xsl:with-param name="nodeActions"	select="$nodeActions"/>
			<xsl:with-param name="fTrimTail"	select="$fTrimTail"/>
			<xsl:with-param name="szCaseVar"	select="$szMaskVar"/>
			<xsl:with-param name="messages"		select="$messages"/>
		</xsl:call-template>
		
	</xsl:when>
	<xsl:when test="name($nodeElem) = 'field'">
		
		<xsl:call-template name="CodefragFromField">
			<xsl:with-param name="field"		select="$nodeElem"/>
			<xsl:with-param name="nodesetElems"	select="$nodesetElems"/>
			<xsl:with-param name="nodeActions"	select="$nodeActions"/>
			<xsl:with-param name="messages"		select="$messages"/>
		</xsl:call-template>
		
	</xsl:when>
	<xsl:when test="name($nodeElem) = 'align'">
		
		<codefrag cbSkip="0" fBoring="true">
			<xsl:if test="$fTrace">
				<xsl:attribute name="szTraceIn">CodefragFromElem(align)</xsl:attribute>
				<xsl:attribute name="szTraceOut">CodefragFromElem: 0, true</xsl:attribute>
			</xsl:if>
			#
			Call mbuf.Align_<xsl:value-of select="$nodeElem/@type"/>()
			#
		</codefrag>
		
	</xsl:when>
	<xsl:otherwise>
		
		<codefrag cbSkip="0" fBoring="false">
			<xsl:if test="$fTrace">
				<xsl:attribute name="szTraceIn">CodefragFromElem(<xsl:value-of select="name($nodeElem)"/>)</xsl:attribute>
				<xsl:attribute name="szTraceOut">CodefragFromElem: 0, false</xsl:attribute>
			</xsl:if>
			#
			*** Unknown element type <xsl:value-of select="name($nodeElem)"/> ***
			#
		</codefrag>
		
	</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>


<!--
Generate decoding logic for the vector element specified by vector.  nodesetElems 
is the complete set of elements being decoded; nodeActions is the set of actions 
to be taken, one of which may apply to vector.
-->
<xsl:template name="CodefragFromVector">
  <xsl:param name="vector"/>
  <xsl:param name="nodesetElems"/>
  <xsl:param name="nodeActions"/>
  <xsl:param name="messages"/>
	
	<xsl:variable name="szVecName" select="$vector/@name"/>
	<xsl:variable name="nodeVecAction" select="$nodeActions/vector[@name = $szVecName]"/>
	<xsl:variable name="szLoopVar"><xsl:call-template name="SzVarNameFromElem">
	  <xsl:with-param name="nodeElem" select="$vector"/>
	  <xsl:with-param name="nodeAction" select="$nodeVecAction"/>
	  </xsl:call-template></xsl:variable>
	<xsl:variable name="szFieldName" select="$vector/@length"/>
	<xsl:variable name="nodeField" select="$vector/parent::node()/field[@name = $szFieldName]"/>
	<xsl:variable name="nodeFieldAction" select="$nodeActions/field[@name = $szFieldName]"/>
	<xsl:variable name="szFieldVar"><xsl:call-template name="SzVarNameFromElem">
	  <xsl:with-param name="nodeElem" select="$nodeField"/>
	  <xsl:with-param name="nodeAction" select="$nodeFieldAction"/>
	  </xsl:call-template></xsl:variable>
	
<!-- Include mask value (if present) in szLengthExpr. -->
	<xsl:variable name="szLengthExpr"><xsl:choose>
		<xsl:when test="$vector/@mask">LExtractMask(<xsl:value-of select="$szFieldVar"/>, <xsl:call-template name="SzHexVBStrip0x">
			<xsl:with-param name="szHex" select="$vector/@mask"/>
		</xsl:call-template>)</xsl:when>
		<xsl:otherwise><xsl:value-of select="$szFieldVar"/></xsl:otherwise>
		</xsl:choose></xsl:variable>
	
	<xsl:variable name="fBoring" select="count($nodeVecAction) = 0 and count($vector//field[@atype = 'ObjectID']) = 0"/>
	
	<!-- Generate code for the vector's element list. -->
	<xsl:variable name="rtreeElems">
		<xsl:call-template name="CodefragFromElems">
			<xsl:with-param name="nodesetElems"	select="$vector/*"/>
			<xsl:with-param name="nodeActions"	select="$nodeVecAction"/>
			<xsl:with-param name="fTrimTail"	select="false()"/>
			<xsl:with-param name="messages"		select="$messages"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="codefragElems" select="msxsl:node-set($rtreeElems)/codefrag"/>
	
	<!--
	If there's no action for this vector, and the body is of constant size, 
	we can skip the whole thing in one call.
	-->
	<xsl:choose>
	<xsl:when test="$fBoring and string-length($codefragElems) = 0">
		
		<codefrag cbSkip="0" fBoring="true">
			<xsl:if test="$fTrace">
				<xsl:attribute name="szTraceIn">CodefragFromVector(<xsl:value-of select="$szVecName"/>)</xsl:attribute>
				<xsl:attribute name="szTraceOut">CodefragFromVector: 0, true</xsl:attribute>
			</xsl:if>
			#
		'	Skip: <xsl:value-of select="$szVecName"/>, length: <xsl:value-of select="$szFieldVar"/>
			Call mbuf.SkipCb(<xsl:value-of select="$szLengthExpr"/> * <xsl:value-of select="$codefragElems/@cbSkip"/>)
			#
		</codefrag>
		
	</xsl:when>
	<xsl:otherwise>
		
		<!-- Wrap a loop around the body. -->
		<codefrag cbSkip="0">
			<xsl:attribute name="fBoring"><xsl:value-of select="$fBoring"/></xsl:attribute>
			<xsl:if test="$fTrace">
				<xsl:attribute name="szTraceIn">CodefragFromVector(<xsl:value-of select="$szVecName"/>)</xsl:attribute>
				<xsl:attribute name="szTraceOut">CodefragFromVector: 0, <xsl:value-of select="$fBoring"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="$nodeVecAction">
				<uses><xsl:attribute name="action"><xsl:call-template name="SzUniqueFromElem">
				  <xsl:with-param name="node" select="$nodeVecAction"/>
				  </xsl:call-template></xsl:attribute></uses>
			</xsl:if>
			<xsl:copy-of select="$codefragElems/uses"/>
			#
		'	vector: <xsl:value-of select="$szVecName"/>, length: <xsl:value-of select="$szFieldVar"/>
			Dim <xsl:value-of select="$szLoopVar"/> As Long
			For <xsl:value-of select="$szLoopVar"/> = 0 To <xsl:value-of select="$szLengthExpr"/> - 1
				
				<xsl:variable name="rtreeBody">
					<xsl:call-template name="CodefragWrapCracker">
						<xsl:with-param name="codefragElems"	select="$codefragElems"/>
						<xsl:with-param name="nodeActions"		select="$nodeVecAction"/>
					</xsl:call-template>
				</xsl:variable>
				<xsl:variable name="codefragBody" select="msxsl:node-set($rtreeBody)/codefrag"/>
				
				<xsl:call-template name="ExpandCodefrag">
					<xsl:with-param name="codefrag" select="$codefragBody"/>
				</xsl:call-template>
				
			Next <xsl:value-of select="$szLoopVar"/>
			#
		</codefrag>
		
	</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>


<!--
Generate decoding logic for the maskmap element specified by maskmap.  nodesetElems 
is the complete set of elements being decoded; nodeActions is the set of actions 
to be taken.
-->
<xsl:template name="CodefragFromMaskmap">
  <xsl:param name="maskmap"/>
  <xsl:param name="nodesetElems"/>
  <xsl:param name="nodeActions"/>
  <xsl:param name="fTrimTail"/>
  <xsl:param name="messages"/>
	
	<xsl:variable name="szFieldName" select="$maskmap/@name"/>
	<xsl:variable name="nodeAction" select="$nodeActions/field[@name = $szFieldName]"/>
	<xsl:variable name="szVarName"><xsl:call-template name="SzVarNameFromElem">
	  <xsl:with-param name="nodeElem" select="$maskmap/parent::node()//field[@name = $szFieldName]"/>
	  <xsl:with-param name="nodeAction" select="$nodeAction"/>
	  </xsl:call-template></xsl:variable>
	
	<!-- Generate code for the contained mask groups. -->
	<xsl:variable name="rtreeElems">
		<xsl:call-template name="CodefragFromElems">
			<xsl:with-param name="nodesetElems"	select="$maskmap/*"/>
			<xsl:with-param name="nodeActions"	select="$nodeActions"/>
			<xsl:with-param name="fTrimTail"	select="$fTrimTail"/>
		<!-- Include xor value (if present) in szMaskVar to be passed down to nested mask elements. -->
			<xsl:with-param name="szMaskVar"><xsl:choose>
				<xsl:when test="$maskmap/@xor">(<xsl:value-of select="$szVarName"/> Xor <xsl:call-template name="SzHexVBStrip0x">
					<xsl:with-param name="szHex" select="$maskmap/@xor"/>
				</xsl:call-template>)</xsl:when>
				<xsl:otherwise><xsl:value-of select="$szVarName"/></xsl:otherwise>
				</xsl:choose></xsl:with-param>
			<xsl:with-param name="messages"		select="$messages"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="codefragElems" select="msxsl:node-set($rtreeElems)/codefrag"/>
	
	<!-- The whole map is boring if all the groups are. -->
	<xsl:variable name="fBoring" select="$codefragElems/@fBoring = 'true'"/>
	
	<codefrag cbSkip="0">
		<xsl:attribute name="fBoring"><xsl:value-of select="$fBoring"/></xsl:attribute>
		<xsl:if test="$fTrace">
			<xsl:attribute name="szTraceIn">CodefragFromMaskmap(<xsl:value-of select="$szFieldName"/>)</xsl:attribute>
			<xsl:attribute name="szTraceOut">CodefragFromMaskmap: 0, <xsl:value-of select="$fBoring"/></xsl:attribute>
		</xsl:if>
		<xsl:copy-of select="$codefragElems/uses"/>
	'	maskmap: <xsl:value-of select="$szFieldName"/>
		<xsl:call-template name="ExpandCodefrag">
			<xsl:with-param name="codefrag" select="$codefragElems"/>
		</xsl:call-template>
	</codefrag>
	
</xsl:template>


<!--
Generate decoding logic for the mask element specified by mask.  nodesetElems 
is the complete set of elements being decoded; nodeActions is the set of actions 
to be taken.  szMaskVar is the name of the flags variable to be masked against.
-->
<xsl:template name="CodefragFromMask">
  <xsl:param name="mask"/>
  <xsl:param name="nodesetElems"/>
  <xsl:param name="nodeActions"/>
  <xsl:param name="fTrimTail"/>
  <xsl:param name="szMaskVar"/>
  <xsl:param name="messages"/>
	
	<!-- Generate code for the mask's element list. -->
	<xsl:variable name="rtreeElems">
		<xsl:call-template name="CodefragFromElems">
			<xsl:with-param name="nodesetElems"	select="$mask/*"/>
			<xsl:with-param name="nodeActions"	select="$nodeActions"/>
			<xsl:with-param name="fTrimTail"	select="$fTrimTail"/>
			<xsl:with-param name="messages"		select="$messages"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="codefragElems" select="msxsl:node-set($rtreeElems)/codefrag"/>
	
	<!-- The whole group is boring if all the elements are. -->
	<xsl:variable name="fBoring" select="$codefragElems/@fBoring = 'true'"/>
	
	<!-- Wrap a test around it. -->
	<codefrag cbSkip="0">
		<xsl:attribute name="fBoring"><xsl:value-of select="$fBoring"/></xsl:attribute>
		<xsl:if test="$fTrace">
			<xsl:attribute name="szTraceIn">CodefragFromMask(<xsl:value-of select="$mask/@value"/>)</xsl:attribute>
			<xsl:attribute name="szTraceOut">CodefragFromMask: 0, <xsl:value-of select="$fBoring"/></xsl:attribute>
		</xsl:if>
		<xsl:copy-of select="$codefragElems/uses"/>
		If (<xsl:value-of select="$szMaskVar"/> And <xsl:call-template name="SzHexVBStrip0x">
		  <xsl:with-param name="szHex" select="$mask/@value"/>
		  </xsl:call-template>) &lt;&gt; 0 Then
			
			<xsl:call-template name="ExpandCodefrag">
				<xsl:with-param name="codefrag" select="$codefragElems"/>
			</xsl:call-template>
			
		End If
	</codefrag>
	
</xsl:template>


<!--
Generate decoding logic for the switch element specified by switch.  nodesetElems 
is the complete set of elements being decoded; nodeActions is the set of actions 
to be taken.
-->
<xsl:template name="CodefragFromSwitch">
  <xsl:param name="switch"/>
  <xsl:param name="nodesetElems"/>
  <xsl:param name="nodeActions"/>
  <xsl:param name="fTrimTail"/>
  <xsl:param name="messages"/>
	
	<xsl:variable name="szFieldName" select="$switch/@name"/>
	<xsl:variable name="nodeSwitchAction" select="$nodeActions/switch[@name = $szFieldName]"/>
	<xsl:variable name="nodeFieldAction" select="$nodeActions/field[@name = $szFieldName]"/>
	<xsl:variable name="szVarName"><xsl:call-template name="SzVarNameFromElem">
	  <xsl:with-param name="nodeElem" select="$switch/parent::node()/field[@name = $szFieldName]"/>
	  <xsl:with-param name="nodeAction" select="$nodeFieldAction"/>
	  </xsl:call-template></xsl:variable>
	
	<!-- Generate code for the contained case groups. -->
	<xsl:variable name="rtreeElems">
		<xsl:call-template name="CodefragFromElems">
			<xsl:with-param name="nodesetElems"	select="$switch/*"/>
			<xsl:with-param name="nodeActions"	select="$nodeSwitchAction"/>
			<xsl:with-param name="fTrimTail"	select="$fTrimTail"/>
			<xsl:with-param name="szMaskVar"	select="$szVarName"/>
			<xsl:with-param name="messages"		select="$messages"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="codefragElems" select="msxsl:node-set($rtreeElems)/codefrag"/>
	
	<!-- The whole switch is boring if all the groups are. -->
	<xsl:variable name="fBoring" select="$codefragElems/@fBoring = 'true'"/>
	
	<!-- Wrap a Select statement around it. -->
	<codefrag cbSkip="0">
		<xsl:attribute name="fBoring"><xsl:value-of select="$fBoring"/></xsl:attribute>
		<xsl:if test="$fTrace">
			<xsl:attribute name="szTraceIn">CodefragFromswitch(<xsl:value-of select="$szFieldName"/>)</xsl:attribute>
			<xsl:attribute name="szTraceOut">CodefragFromswitch: 0, <xsl:value-of select="$fBoring"/></xsl:attribute>
		</xsl:if>
		<xsl:copy-of select="$codefragElems/uses"/>
	'	switch: <xsl:value-of select="$szFieldName"/>
		Select Case <xsl:value-of select="$szVarName"/>
			<xsl:call-template name="ExpandCodefrag">
				<xsl:with-param name="codefrag" select="$codefragElems"/>
			</xsl:call-template>
		End Select
	</codefrag>
	
</xsl:template>


<!--
Generate decoding logic for the case element specified by case.  nodesetElems 
is the complete set of elements being decoded; nodeActions is the set of actions 
to be taken.  szCaseVar is the name of the variable to be tested against.
-->
<xsl:template name="CodefragFromCase">
  <xsl:param name="case"/>
  <xsl:param name="nodesetElems"/>
  <xsl:param name="nodeActions"/>
  <xsl:param name="fTrimTail"/>
  <xsl:param name="szCaseVar"/>
  <xsl:param name="messages"/>
	
	<xsl:variable name="szCaseVal" select="$case/@value"/>
	<xsl:variable name="nodeCaseAction" select="$nodeActions/case[@value = $szCaseVal]"/>
	
	<!-- Generate code for the case's element list. -->
	<xsl:variable name="rtreeElems">
		<xsl:call-template name="CodefragFromElems">
			<xsl:with-param name="nodesetElems"	select="$case/*"/>
			<xsl:with-param name="nodeActions"	select="$nodeCaseAction"/>
			<xsl:with-param name="fTrimTail"	select="$fTrimTail"/>
			<xsl:with-param name="messages"		select="$messages"/>
		</xsl:call-template>
	</xsl:variable>
	<xsl:variable name="codefragElems" select="msxsl:node-set($rtreeElems)/codefrag"/>
	
	<!-- The whole group is boring if all the elements are. -->
	<xsl:variable name="fBoring" select="$codefragElems/@fBoring = 'true'"/>
	
	<!-- Wrap a Case around it. -->
	<codefrag cbSkip="0">
		<xsl:attribute name="fBoring"><xsl:value-of select="$fBoring"/></xsl:attribute>
		<xsl:if test="$fTrace">
			<xsl:attribute name="szTraceIn">CodefragFromCase(<xsl:value-of select="$szCaseVal"/>)</xsl:attribute>
			<xsl:attribute name="szTraceOut">CodefragFromCase: 0, <xsl:value-of select="$fBoring"/></xsl:attribute>
		</xsl:if>
		<xsl:copy-of select="$codefragElems/uses"/>
		Case <xsl:call-template name="SzHexVBStrip0x">
		  <xsl:with-param name="szHex" select="$szCaseVal"/>
		  </xsl:call-template>
			
			<xsl:call-template name="ExpandCodefrag">
				<xsl:with-param name="codefrag" select="$codefragElems"/>
			</xsl:call-template>
			
	</codefrag>
	
</xsl:template>


<!--
Generate decoding logic for the field element specified by field.  nodesetElems 
is the complete set of elements being decoded; nodeActions is the set of actions 
to be taken, one of which may apply to field.
-->
<xsl:template name="CodefragFromField">
  <xsl:param name="field"/>
  <xsl:param name="nodesetElems"/>
  <xsl:param name="nodeActions"/>
  <xsl:param name="messages"/>
	
	<xsl:variable name="szFieldName" select="$field/@name"/>
	<!-- REVIEW: Kluge: use mask value to disambiguate duplicate field names. -->
	<xsl:variable name="nodeAction" 
	  select="$nodeActions/field[@name = $szFieldName and 
	    (not(@mask) or @mask = $field/parent::*/@value)]"/>
	<xsl:variable name="szFieldType" select="$field/@type"/>
	<xsl:variable name="nodeTypeDef" select="$messages/schema/datatypes/type[@name = $szFieldType]"/>
	<xsl:variable name="szVarName"><xsl:call-template name="SzVarNameFromElem">
	  <xsl:with-param name="nodeElem" select="$field"/>
	  <xsl:with-param name="nodeAction" select="$nodeAction"/>
	  </xsl:call-template></xsl:variable>
	<xsl:variable name="szVarType"><xsl:call-template name="SzVBTypeFromSzType">
	  <xsl:with-param name="szType" select="$szFieldType"/>
	  </xsl:call-template></xsl:variable>
	<xsl:variable name="fOid" select="$field/@atype = 'ObjectID'"/>
	<xsl:variable name="fBoring" select="count($nodeAction) = 0 and not($fOid)"/>
	
	<xsl:choose>
	<xsl:when test="$nodeTypeDef/@primitive = 'true' or $szFieldType = 'Variant'">
		
		<!-- Primitive type; decode in line. -->
		<xsl:choose>
		<xsl:when test="$nodeAction or $fOid or $fPx or 
		  $nodesetElems[name() = 'vector' and @length = $szFieldName] or 
		  $nodesetElems[name() = 'switch' and @name = $szFieldName] or 
		  $nodesetElems[name() = 'maskmap' and @name = $szFieldName]">
		  	
			<!--
			We need to save the field value in a variable when it's needed for 
			an action or as the argument to a vector, switch, or maskmap.
			-->
			<codefrag cbSkip="0">
				<xsl:attribute name="fBoring"><xsl:value-of select="$fBoring"/></xsl:attribute>
				<xsl:if test="$fTrace">
					<xsl:attribute name="szTraceIn">CodefragFromField(<xsl:value-of select="$szFieldName"/>), case 1</xsl:attribute>
					<xsl:attribute name="szTraceOut">CodefragFromField: 0, <xsl:value-of select="$fBoring"/></xsl:attribute>
				</xsl:if>
				<xsl:if test="$nodeAction">
					<uses><xsl:attribute name="action"><xsl:call-template name="SzUniqueFromElem">
					  <xsl:with-param name="node" select="$nodeAction"/>
					  </xsl:call-template></xsl:attribute></uses>
				</xsl:if>
				#
			'	field: <xsl:value-of select="$szFieldName"/>, type: <xsl:value-of select="$szFieldType"/>
				Dim <xsl:value-of select="$szVarName"/> As <xsl:value-of 
				  select="$szVarType"/> : <xsl:value-of 
				  select="$szVarName"/> = mbuf.Get_<xsl:value-of 
				  select="$szFieldType"/>()
				<xsl:if test="$fOid">
					If <xsl:value-of select="$szVarName"/> &lt;&gt; oidNil And <xsl:value-of select="$szVarName"/> = otable.oidPx Then
						fPxThisMsg = True
					End If
				</xsl:if>
				<xsl:if test="$nodeAction/@aco">
					Dim <xsl:value-of select="$nodeAction/@aco"/> As AcoCls : Set <xsl:value-of 
					  select="$nodeAction/@aco"/> = otable.AcoFromOid(<xsl:value-of 
					  select="$szVarName"/>, True, True)
				</xsl:if>
				<xsl:value-of select="$nodeAction"/>
				<xsl:if test="$fPx">
					Call PxField_<xsl:value-of select="$szFieldType"/>("<xsl:value-of select="$szFieldName"/>", <xsl:value-of select="$szVarName"/>)
				</xsl:if>
				#
			</codefrag>
			
		</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="cbType"><xsl:call-template name="CbFromSzType">
			  <xsl:with-param name="szType" select="$szFieldType"/>
			  </xsl:call-template></xsl:variable>
			
			<!-- Field value is not needed; just skip over it. -->
			<xsl:choose>
			<xsl:when test="$cbType != 0">
				
				<!--
				Field is of constant size; encode the skip as an attribute of 
				the codefrag so it can be combined with other skips.
				-->
				<codefrag fBoring="true">
					<xsl:attribute name="cbSkip"><xsl:value-of select="$cbType"/></xsl:attribute>
					<xsl:attribute name="szSkip"><xsl:value-of select="concat(' ', $szFieldName)"/></xsl:attribute>
					<xsl:if test="$fTrace">
						<xsl:attribute name="szTraceIn">CodefragFromField(<xsl:value-of select="$szFieldName"/>), case 2</xsl:attribute>
						<xsl:attribute name="szTraceOut">CodefragFromField: <xsl:value-of select="$cbType"/>, true</xsl:attribute>
					</xsl:if>
				</codefrag>
				
			</xsl:when>
			<xsl:otherwise>
				
				<!-- Field is of variable size; calculate the skip at runtime. -->
				<codefrag cbSkip="0" fBoring="true">
					<xsl:if test="$fTrace">
						<xsl:attribute name="szTraceIn">CodefragFromField(<xsl:value-of select="$szFieldName"/>), case 3</xsl:attribute>
						<xsl:attribute name="szTraceOut">CodefragFromField: 0, true</xsl:attribute>
					</xsl:if>
					#
				'	Skip: <xsl:value-of select="$szFieldName"/>, type: <xsl:value-of select="$szFieldType"/>
					Call mbuf.Skip_<xsl:value-of select="$szFieldType"/>()
					#
				</codefrag>
				
			</xsl:otherwise>
			</xsl:choose>
			
		</xsl:otherwise>
		</xsl:choose>
		
	</xsl:when>
	<xsl:otherwise>
		
		<!-- Compound type; call subroutine to decode. -->
		<codefrag cbSkip="0">
			<xsl:attribute name="fBoring"><xsl:value-of select="$fBoring"/></xsl:attribute>
			<xsl:if test="$fTrace">
				<xsl:attribute name="szTraceIn">CodefragFromField(<xsl:value-of select="$szFieldName"/>), case 4</xsl:attribute>
				<xsl:attribute name="szTraceOut">CodefragFromField: 0, <xsl:value-of select="$fBoring"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="$nodeAction">
				<uses><xsl:attribute name="action"><xsl:call-template name="SzUniqueFromElem">
				  <xsl:with-param name="node" select="$nodeAction"/>
				  </xsl:call-template></xsl:attribute></uses>
			</xsl:if>
			#
		'	field: <xsl:value-of select="$szFieldName"/>, type: <xsl:value-of select="$szFieldType"/>
			Dim <xsl:value-of select="$szVarName"/> As <xsl:value-of select="$szVarType"/>
			Set <xsl:value-of select="$szVarName"/> = Decode<xsl:value-of select="$szFieldType"/>(mbuf, oidChar, <xsl:choose>
				<xsl:when test="$nodeActions/field[@varname = 'flagsLoc']">flagsLoc</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
				</xsl:choose>)
			<xsl:value-of select="$nodeAction"/>
			#
		</codefrag>
		
	</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>


<!--
Combine two codefrags and optimize the result by consolidating skips and/or 
trimming away boring tails.
-->
<xsl:template name="CodefragCombine">
  <xsl:param name="codefrag1"/>
  <xsl:param name="codefrag2"/>
  <xsl:param name="fTrimTail"/>
	
	<!-- The combined code fragment is boring only if both parts are boring. -->
	<xsl:variable name="fBoring" 
	  select="$codefrag1/@fBoring = 'true' and $codefrag2/@fBoring = 'true'"/>
	
	<xsl:choose>
	<xsl:when test="$fBoring and $fTrimTail">
		
		<!-- The boring combination is eligible to be trimmed. -->
		<xsl:choose>
		<xsl:when test="$fTrace">

			<!-- Trace mode: show what would have been trimmed away. -->
			<codefrag cbSkip="0" fBoring="true">
				<xsl:attribute name="szTraceIn">CodefragCombine((<xsl:value-of select="$codefrag1/@cbSkip"/>, <xsl:value-of select="$codefrag1/@fBoring"/>), (<xsl:value-of select="$codefrag2/@cbSkip"/>, <xsl:value-of select="$codefrag2/@fBoring"/>), <xsl:value-of select="$fTrimTail"/>)</xsl:attribute>
				<xsl:attribute name="szTraceOut">CodefragCombine: 0, true, trimmed</xsl:attribute>
			'	----- trim -----
				<xsl:call-template name="ExpandCodefrag">
					<xsl:with-param name="codefrag"	select="$codefrag1"/>
				</xsl:call-template>
				<xsl:call-template name="ExpandCodefrag">
					<xsl:with-param name="codefrag" select="$codefrag2"/>
				</xsl:call-template>
			</codefrag>
			
		</xsl:when>
		<xsl:otherwise>
			
			<!-- Production mode; really trim away the boring combination. -->
			<codefrag cbSkip="0" fBoring="true"/>
			
		</xsl:otherwise>
		</xsl:choose>
		
	</xsl:when>
	<xsl:when test="string-length($codefrag1) = 0">
		
		<!-- First fragment is empty; consolidate the cbSkip. -->
		<codefrag>
			<xsl:attribute name="cbSkip"><xsl:value-of select="$codefrag1/@cbSkip + $codefrag2/@cbSkip"/></xsl:attribute>
			<xsl:attribute name="szSkip"><xsl:value-of select="concat($codefrag1/@szSkip, $codefrag2/@szSkip)"/></xsl:attribute>
			<xsl:attribute name="fBoring"><xsl:value-of select="$fBoring"/></xsl:attribute>
			<xsl:if test="$fTrace">
				<xsl:attribute name="szTraceIn">CodefragCombine((<xsl:value-of select="$codefrag1/@cbSkip"/>, <xsl:value-of select="$codefrag1/@fBoring"/>), (<xsl:value-of select="$codefrag2/@cbSkip"/>, <xsl:value-of select="$codefrag2/@fBoring"/>), <xsl:value-of select="$fTrimTail"/>)</xsl:attribute>
				<xsl:attribute name="szTraceOut">CodefragCombine: <xsl:value-of select="$codefrag1/@cbSkip + $codefrag2/@cbSkip"/>, <xsl:value-of select="$fBoring"/></xsl:attribute>
			</xsl:if>
			<xsl:copy-of select="$codefrag1/uses"/>
			<xsl:copy-of select="$codefrag2/uses"/>
			<xsl:call-template name="ExpandCodefrag">
				<xsl:with-param name="codefrag"	select="$codefrag2"/>
				<xsl:with-param name="fSkip"	select="false()"/>
			</xsl:call-template>
		</codefrag>
		
	</xsl:when>
	<xsl:otherwise>
		
		<!-- First fragment is nonempty; discharge the second guy's cbSkip. -->
		<codefrag>
			<xsl:attribute name="cbSkip"><xsl:value-of select="$codefrag1/@cbSkip"/></xsl:attribute>
			<xsl:attribute name="szSkip"><xsl:value-of select="$codefrag1/@szSkip"/></xsl:attribute>
			<xsl:attribute name="fBoring"><xsl:value-of select="$fBoring"/></xsl:attribute>
			<xsl:if test="$fTrace">
				<xsl:attribute name="szTraceIn">CodefragCombine((<xsl:value-of select="$codefrag1/@cbSkip"/>, <xsl:value-of select="$codefrag1/@fBoring"/>), (<xsl:value-of select="$codefrag2/@cbSkip"/>, <xsl:value-of select="$codefrag2/@fBoring"/>), <xsl:value-of select="$fTrimTail"/>)</xsl:attribute>
				<xsl:attribute name="szTraceOut">CodefragCombine: <xsl:value-of select="$codefrag1/@cbSkip"/>, <xsl:value-of select="$fBoring"/></xsl:attribute>
			</xsl:if>
			<xsl:copy-of select="$codefrag1/uses"/>
			<xsl:copy-of select="$codefrag2/uses"/>
			<xsl:call-template name="ExpandCodefrag">
				<xsl:with-param name="codefrag"	select="$codefrag1"/>
				<xsl:with-param name="fSkip"	select="false()"/>
			</xsl:call-template>
			<xsl:call-template name="ExpandCodefrag">
				<xsl:with-param name="codefrag" select="$codefrag2"/>
			</xsl:call-template>
		</codefrag>
		
	</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>


<!--
Expand the given codefrag by discharging any accumulated skips and rendering 
the code in line.  In trace mode, annotate the output with the name of the 
template that generated this fragment.
-->
<xsl:template name="ExpandCodefrag">
  <xsl:param name="codefrag"/>
  <xsl:param name="fSkip" select="true()"/>
	
	<xsl:if test="$fTrace">
		[ <xsl:value-of select="$codefrag/@szTraceIn"/>
	</xsl:if>
	
	<xsl:if test="$codefrag/@cbSkip != 0 and $fSkip">
		#
	'	Skip:<xsl:value-of select="$codefrag/@szSkip"/>
		Call mbuf.SkipCb(<xsl:value-of select="$codefrag/@cbSkip"/>)
		#
	</xsl:if>
	<xsl:value-of select="$codefrag"/>
	
	<xsl:if test="$fTrace">
		] <xsl:value-of select="$codefrag/@szTraceOut"/>
	</xsl:if>
	
</xsl:template>


<!--
Given a message element in nodeElem, and an action in nodeAction, return 
the string name of the variable in which the element value should be stored.
-->
<xsl:template name="SzVarNameFromElem">
  <xsl:param name="nodeElem"/>
  <xsl:param name="nodeAction"/>
	<xsl:variable name="szFieldName" select="$nodeElem/@name"/>
	<xsl:variable name="szVarName" select="$nodeAction/@varname"/>
	
	<xsl:choose>
	<xsl:when test="$szVarName"><xsl:value-of select="$szVarName"/></xsl:when>
	<xsl:when test="$nodeAction"><xsl:value-of select="$szFieldName"/></xsl:when>
	<xsl:when test="$szFieldName='type'"><xsl:call-template name="SzUniqueFromElem">
		<xsl:with-param name="node" select="$nodeElem"/>
		</xsl:call-template></xsl:when>
	<xsl:otherwise>t_<xsl:value-of select="$szFieldName"/></xsl:otherwise>
	</xsl:choose>
</xsl:template>


<!--
Given a message or action element in node, return a unique variable name for that element.
-->
<xsl:template name="SzUniqueFromElem">
  <xsl:param name="node"/>
	<xsl:choose>
	<xsl:when test="name($node) = 'message' or 
	                name($node) = 'cracker' or 
	                name($node) = 'type'">t</xsl:when>
	<xsl:when test="name($node) = 'case' or 
	                name($node) = 'mask'"><xsl:call-template name="SzUniqueFromElem">
		<xsl:with-param name="node" select="$node/parent::node()"/>
		</xsl:call-template>_<xsl:value-of select="$node/@value"/></xsl:when>
	<xsl:otherwise><xsl:call-template name="SzUniqueFromElem">
		<xsl:with-param name="node" select="$node/parent::node()"/>
		</xsl:call-template>_<xsl:value-of select="$node/@name"/></xsl:otherwise>
	</xsl:choose>
</xsl:template>


<!--
Given the name of a primitive data type from Messages.xml, return the size 
in bytes of a variable of that type.
-->
<xsl:template name="CbFromSzType">
  <xsl:param name="szType"/>
	
	<xsl:choose>
	<xsl:when test="$szType = 'BYTE'">1</xsl:when>
	<xsl:when test="$szType = 'WORD'">2</xsl:when>
	<xsl:when test="$szType = 'DWORD'">4</xsl:when>
	<xsl:when test="$szType = 'QWORD'">8</xsl:when>
	<xsl:when test="$szType = 'float'">4</xsl:when>
	<xsl:when test="$szType = 'double'">8</xsl:when>
	<xsl:otherwise>0</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>


<!--
Given the name of a data type from Messages.xml, return the string name of 
the corresponsing Visual Basic type.
-->
<xsl:template name="SzVBTypeFromSzType">
  <xsl:param name="szType"/>
	
	<xsl:choose>
	<xsl:when test="$szType = 'BYTE'">Byte</xsl:when>
	<xsl:when test="$szType = 'WORD'">Long</xsl:when> <!-- Using Longs to hold WORDs to avoid signed/unsigned difficulties. -->
	<xsl:when test="$szType = 'PackedWORD'">Long</xsl:when> <!-- Using Longs to hold PackedWORDs to avoid signed/unsigned difficulties. -->
	<xsl:when test="$szType = 'DWORD'">Long</xsl:when>
	<xsl:when test="$szType = 'PackedDWORD'">Long</xsl:when>
	<xsl:when test="$szType = 'QWORD'">Double</xsl:when>
	<xsl:when test="$szType = 'float'">Single</xsl:when>
	<xsl:when test="$szType = 'double'">Double</xsl:when>
	<xsl:when test="$szType = 'String'">String</xsl:when>
	<xsl:when test="$szType = 'WString'">String</xsl:when>
	<xsl:when test="$szType = 'Variant'">Variant</xsl:when>
	<xsl:when test="$szType = 'Location'">MaplocCls</xsl:when>
	<xsl:when test="$szType = 'Enchantment'">SpellCls</xsl:when>
	<xsl:when test="$szType = 'PhysicsData'">PhysicsDataCls</xsl:when>
	<xsl:when test="$szType = 'GameData'">GameDataCls</xsl:when>
	<xsl:when test="$szType = 'FellowInfo'">FellowCls</xsl:when>
	<xsl:when test="$szType = 'AttributeData'">AVDataCls</xsl:when>
	<xsl:when test="$szType = 'VitalData'">AVDataCls</xsl:when>
	<xsl:when test="$szType = 'SkillData'">SkillDataCls</xsl:when>
	<xsl:otherwise>Object</xsl:otherwise>
	</xsl:choose>
	
</xsl:template>


<!--
Given a raw hexadecimal literal in szHex, return a string representing the 
same hexadecimal value in valid Visual Basic syntax.
-->
<xsl:template name="SzHexVBStrip0x"><xsl:param 
  name="szHex"/><xsl:call-template name="SzHexVB">
    <xsl:with-param name="szHex" select="substring-after($szHex, '0x')"/>
    </xsl:call-template></xsl:template>


<!--
Given a raw hexadecimal literal in szHex, return a string representing the 
same hexadecimal value in valid Visual Basic syntax.
-->
<xsl:template name="SzHexVB"><xsl:param 
  name="szHex"/>&amp;H<xsl:value-of select="$szHex"/>&amp;</xsl:template>


</xsl:stylesheet>
