VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "GetSkapiCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


Public Property Get skapi() As SkapiCls
    
    Set skapi = SkGlobals.skapi
    
End Property


Private Sub Class_Initialize()
    
    If SkGlobals.skapi Is Nothing Then
        Set SkGlobals.skapi = New SkapiCls
    End If
    
End Sub
