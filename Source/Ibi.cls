VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IbiCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' Item Basic Information (used by OnAssessItem)


Public oty As Long
Public cpyValue As Long
Public burden As Long
Public material As Long
Public szDescSimple As String
Public szDescDetailed As String
Public szComment As String
Public szInscription As String
Public szInscriber As String
Public cpageTotal As Long
Public cpageUsed As Long
Public fOpen As Boolean
Public fLocked As Boolean
Public diffLock As Long
Public manaCur As Long
Public fractEfficiency As Double
Public probDestroy As Double
Public fractManaConvMod As Double
Public manaCost As Long
Public ckeyOnRing As Long
Public rareid As Long

Public skidWieldReq As Long
Public lvlWieldReq As Long

Public vitalRestored As Long
Public dlvlRestored As Long
Public fractRestorationBonus As Double

Public ctink As Long
Public szTinkerer As String
Public szImbuer As String
Public szCreator As String

Public fAttuned As Boolean
Public fBonded As Boolean
Public fRetained As Boolean
Public fDropOnDeath As Boolean
Public fDestroyOnDeath As Boolean
Public fUnenchantable As Boolean
Public fSellable As Boolean
Public fIvoryable As Boolean
Public fDyeable As Boolean
Public fUnlimitedUses As Boolean

' For ACScript compatibility:
Public flags As Long
Public szUnknown As String


Private workmanshipMbr As Long
Private citemSalvagedMbr As Long
Private dictRaw As Dictionary


Public Property Get workmanship() As Single
    workmanship = workmanshipMbr / citemSalvagedMbr
End Property

Public Property Get citemSalvaged() As Long
    citemSalvaged = citemSalvagedMbr
End Property

Public Property Get raw() As Dictionary
    Set raw = dictRaw
End Property


' Old property names preserved for compatibility:
Public Property Get sklvlWieldReq() As Long
    sklvlWieldReq = lvlWieldReq
End Property

Public Property Let sklvlWieldReq(ByVal lvlNew As Long)
    lvlWieldReq = lvlNew
End Property


Friend Sub SetWorkmanship(ByVal workmanship As Long)
    workmanshipMbr = workmanship
End Sub

Friend Sub SetCitemSalvaged(ByVal citemSalvaged As Long)
    citemSalvagedMbr = citemSalvaged
End Sub

Friend Sub SetRaw(ByVal dict As Dictionary)
    Set dictRaw = dict
End Sub


Private Sub Class_Initialize()
    
    workmanshipMbr = 0
    citemSalvagedMbr = 1
    fractManaConvMod = 0
    fSellable = True
    
End Sub


