VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IdqCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


'--------------
' Private data
'--------------

Private Const ctryMax As Long = 3
Private Const coidMax As Long = 200

Private rgoid(ctryMax - 1, coidMax - 1) As Long
Private rgioidMac(ctryMax - 1) As Long

Private oidRequest As Long
Private ctryRequest As Long


'------------
' Properties
'------------

Public tickRequest As Long

Public Property Get fRequestOutstanding() As Boolean
    
    fRequestOutstanding = (oidRequest <> oidNil)
    
End Property


'---------
' Methods
'---------

Public Sub Reset()
    Call TraceEnter("idq.Reset")
    
    Dim ctry As Long
    For ctry = 0 To ctryMax - 1
        rgioidMac(ctry) = 0
    Next ctry
    
    oidRequest = oidNil
    ctryRequest = 0
    
    Call TraceExit("idq.Reset")
End Sub

Public Sub QueueIdRequest(ByVal oid As Long, ByVal ctry As Long, ByVal tick As Long)
    Call TraceEnter("idq.QueueIdRequest", _
      SzHex(oid, 8) + ", " + CStr(ctry) + ", " + SzHex(tick, 8))
    
    If Not FOidInQueue(oid) Then
        Call AppendToQueue(oid, ctry)
    End If
    
    If oidRequest = oidNil Then
        Call SendNextRequest(tick)
    End If
    
    Call TraceExit("idq.QueueIdRequest")
End Sub

Public Sub ResponseReceived(ByVal oid As Long, ByVal tick As Long)
    Call TraceEnter("idq.ResponseReceived", SzHex(oid, 8) + ", " + SzHex(tick, 8))
    
    If oid = oidRequest Then oidRequest = oidNil
    Call RemoveFromQueue(oid)
    Call SendNextRequest(tick)
    
    Call TraceExit("idq.ResponseReceived")
End Sub

Public Sub Timeout(ByVal tick As Long)
    Call TraceEnter("idq.Timeout", SzHex(tick, 8))
    
    Call RemoveFromQueue(oidRequest, ctryRequest)
    
    If ctryRequest + 1 < ctryMax Then
    '   Move to next queue for retry.
        Call AppendToQueue(oidRequest, ctryRequest + 1)
    End If
    
    Call SendNextRequest(tick)
    
    Call TraceExit("idq.Timeout")
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    Call Reset
    
End Sub

Private Function FOidInQueue(ByVal oid As Long) As Boolean
    Call TraceEnter("idq.FOidInQueue", SzHex(oid, 8))
    
    FOidInQueue = False
    
    Dim ctry As Long, ioid As Long
    For ctry = 0 To ctryMax - 1
        If IoidInQueue(oid, ctry) <> iNil Then
            FOidInQueue = True
            Exit For
        End If
    Next ctry
    
    Call TraceExit("idq.FOidInQueue", CStr(FOidInQueue))
End Function

Private Function IoidInQueue(ByVal oid As Long, ByVal ctry As Long) As Long
    
    IoidInQueue = iNil
    
    Dim ioid As Long
    For ioid = 0 To rgioidMac(ctry) - 1
        If rgoid(ctry, ioid) = oid Then
            IoidInQueue = ioid
            Exit For
        End If
    Next ioid
    
End Function

Private Sub AppendToQueue(ByVal oid As Long, ByVal ctry As Long)
    Call TraceEnter("idq.AppendToQueue", SzHex(oid, 8))
    
    Dim ioid As Long
    ioid = rgioidMac(ctry)
    rgoid(ctry, ioid) = oid
    rgioidMac(ctry) = ioid + 1
    
    Call Px
    
    Call TraceExit("idq.AppendToQueue")
End Sub

Private Sub RemoveFromQueue(ByVal oid As Long, Optional ByVal ctry As Long = iNil)
    Call TraceEnter("idq.RemoveFromQueue", SzHex(oid, 8) + ", " + CStr(ctry))
    
    If ctry = -1 Then
        For ctry = 0 To ctryMax - 1
            Call RemoveFromQueue(oid, ctry)
        Next ctry
    Else
        Dim ioid As Long
        ioid = IoidInQueue(oid, ctry)
        If ioid <> iNil Then
            Do While ioid + 1 < rgioidMac(ctry)
                rgoid(ctry, ioid) = rgoid(ctry, ioid + 1)
                ioid = ioid + 1
            Loop
            rgioidMac(ctry) = ioid
            
            Call Px
        End If
    End If
    
    Call TraceExit("idq.RemoveFromQueue")
End Sub

Private Sub SendNextRequest(ByVal tick As Long)
    Call TraceEnter("idq.SendNextRequest", SzHex(tick, 8))
    
    oidRequest = oidNil
    
    Dim ctry As Long
    For ctry = 0 To ctryMax - 1
        Do While rgioidMac(ctry) > 0
            Dim oid As Long
            oid = rgoid(ctry, 0)
            
        '   Make sure the object still exists.
            If Not otable.AcoFromOid(oid) Is Nothing Then
                Call skapi.AssessOidRaw(oid)
                oidRequest = oid
                ctryRequest = ctry
                tickRequest = tick
                Exit For
            End If
            
            Call RemoveFromQueue(oid, ctry)
        Loop
    Next ctry
    
    Call TraceExit("idq.SendNextRequest")
End Sub

Private Sub Px()
    
    Dim sz As String
    sz = ""
    
    Dim ctry As Long
    For ctry = 0 To ctryMax - 1
        sz = sz + " " + CStr(rgioidMac(ctry))
    Next ctry
    
    Call DebugLine("idq:" + sz)
    
End Sub





















