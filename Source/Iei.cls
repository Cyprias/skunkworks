VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IeiCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' Item Enchantment Information (used by OnAssessItem)


Public difficulty As Long
Public spellcraft As Long
Public manaCur As Long
Public manaMax As Long
Public csecPerMana As Single
Public fractEfficiency As Double
Public szRaceReq As String
Public skidReq As Long
Public sklvlReq As Long
Public rankReq As Long
Public szSpellNames As String
Public szSpellDesc As String

Public dwUnknown As Long

Private rgspellidMbr() As Long
Private rgflagsSpellMbr() As Long
Private cspellidMbr As Long

Public Property Get cspellid() As Long
    cspellid = cspellidMbr
End Property

Public Property Get rgspellid(ispellid) As Long
    rgspellid = rgspellidMbr(ispellid)
End Property

Public Property Get rgflagsSpell(ispellid) As Long
    rgflagsSpell = rgflagsSpellMbr(ispellid)
End Property


Friend Sub SetCspellid(ByVal cspellid As Long)
    ReDim rgspellidMbr(cspellid - 1)
    ReDim rgflagsSpellMbr(cspellid - 1)
End Sub

Friend Sub AddSpellid(ByVal spellid As Long, ByVal flags As Long)
    rgspellidMbr(cspellidMbr) = spellid
    rgflagsSpellMbr(cspellidMbr) = flags
    cspellidMbr = cspellidMbr + 1
End Sub

Private Sub Class_Initialize()
    cspellidMbr = 0
End Sub


