VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "InventoryCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' This class implements the Inventory object visible to the script.  Note
' that the properties and methods here are simply wrappers for native APIs
' implemented by the skapi object.


'--------------
' Private data
'--------------


'------------
' Properties
'------------

Public Property Get GetItem(ipack, iitem) As Long
    Dim aco As AcoCls
    Set aco = skapi.AcoFromIpackIitem(ipack, iitem)
    If aco Is Nothing Then
        GetItem = oidNil
    Else
        GetItem = aco.oid
    End If
End Property

Public Property Get HaveAny(szItem) As Long
    Dim aco As AcoCls
    Set aco = skapi.AcoFindInInv(szItem)
    If aco Is Nothing Then
        HaveAny = 0
    Else
        HaveAny = aco.citemStack
    End If
End Property

Public Property Get HowMany(szItem) As Long
    HowMany = skapi.CitemOfKindInInv(szItem)
End Property

Public Property Get Initialized() As Boolean
    Initialized = (skapi.plig = pligInWorld)
End Property

Public Property Get ItemsInPack(ipack) As Long
    Dim acoPack As AcoCls
    Set acoPack = skapi.AcoFromIpackIitem(ipack, iitemNil)
    If acoPack Is Nothing Then
        ItemsInPack = 0
    Else
        ItemsInPack = acoPack.citemContents
    End If
End Property

Public Property Get NumberOfPacks() As Long
    NumberOfPacks = skapi.cpack
End Property


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------






























