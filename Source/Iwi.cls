VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "IwiCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' Item Weapon Information (used by OnAssessItem)


Public dmty As Long
Public speed As Long
Public skid As Long
Public dhealth As Long
Public scaleDamageRange As Double
Public scaleDamageBonus As Double
Public scaleDefenseBonus As Double
Public scaleMissileDBonus As Double
Public scaleMagicDBonus As Double
Public scaleAttackBonus As Double
Public scalePvMElemBonus As Double
Public dhealthElemBonus As Long
Public dwHighlights As Long

Public fCriticalStrike As Boolean
Public fCripplingBlow As Boolean
Public fArmorRending As Boolean
Public fSlashRending As Boolean
Public fPierceRending As Boolean
Public fBludgeonRending As Boolean
Public fLightningRending As Boolean
Public fFireRending As Boolean
Public fColdRending As Boolean
Public fAcidRending As Boolean
Public fPhantasmal As Boolean

' For ACScript compatibility:
Public fractUnknown As Double


' New research indicates that fractUnknown is actually missile weapon launch speed in m/s.
Public Property Get vLaunch() As Double
    vLaunch = fractUnknown
End Property

' Missile weapon range in meters.
Public Property Get distRange() As Double
    distRange = vLaunch * vLaunch / 9.78
End Property

Public Property Get scalePvPElemBonus() As Double
    scalePvPElemBonus = 1 + (scalePvMElemBonus - 1) / 4
End Property


Private Sub Class_Initialize()
    
    scalePvMElemBonus = 1
    
End Sub



