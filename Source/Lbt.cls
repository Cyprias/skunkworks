VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LbtCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' Landblock Terrain


'--------------
' Private data
'--------------

'--------------
' Private data
'--------------

Private landblockMbr as Long
Private latMbr As Single, lngMbr As Single
Private rgzMbr(8, 8) As Long
Private rgflagsMbr(8, 8) As Long

Private Type PtType
    lat As Single
    lng As Single
    z As Long
End Type


'------------
' Properties
'------------

Public Property Get landblock() As Long
    landblock = landblockMbr
End Property

Public Property Get latMin() As Single
    latMin = latMbr
End Property

Public Property Get lngMin() As Single
    lngMin = lngMbr
End Property

Public Property Get latMax() As Single
    latMax = latMbr + 0.8
End Property

Public Property Get lngMax() As Single
    lngMax = lngMbr + 0.8
End Property

' Return the altitude in meters of the grid point at ix,iy (for ix,iy in the range 0-8).
Public Property Get rgz(ByVal ix As Long, ByVal iy As Long) As Long
    rgz = rgzMbr(ix, iy)
End Property

' Return the landscape texture number of the grid point at ix,iy (for ix,iy in the range 0-8).
Public Property Get rgitex(ByVal ix As Long, ByVal iy As Long) As Long

    Dim flags As Long
    flags = rgflagsMbr(ix, iy)
    If flags < 0 Then
        rgitex = &H41&
    ElseIf (flags And 3) <> 0 Then
        rgitex = &H40&
    Else
        rgitex = (flags And &HFC&) \ 4
    End If
    
End Property

' Return True if this landblock is impassable (all water).
Public Property Get fImpassable() As Boolean
    
    fImpassable = True
    
    Dim ix As Long, iy As Long
    For ix = 0 To 8
        For iy = 0 To 8
            Dim itex As Long
            itex = rgitex(ix, iy)
            If itex < 16 Or itex > 20 Then
                fImpassable = False
                Exit Property
            End If
        Next iy
    Next ix
    
End Property


'---------
' Methods
'---------


'------------------
' Friend functions
'------------------

Friend Sub Load(ByVal landblock As Long)
    
    landblockMbr = landblock And &HFFFF0000
    Call LatLngFromLandblockXY(latMbr, lngMbr, landblockMbr, 0, 0)
    
    Dim emfid As Long, emf As EmfCls
    emfid = (landblock And &HFFFF0000) Or &HFFFF&
    Set emf = datfileCell.EmfOpen(emfid)
    
    Dim ix As Long, iy As Long
    If Not emf Is Nothing Then
        Call emf.SkipCb(4)  ' emfidSelf
        Call emf.SkipCb(4)  ' unknown DWORD (always 1?)
        
        For ix = 0 To 8
            For iy = 0 To 8
                rgflagsMbr(ix, iy) = emf.WRead() And &HFFFF&
            Next iy
        Next ix
        For ix = 0 To 8
            For iy = 0 To 8
                Dim iz As Long
                iz = emf.BRead()
                If iz < 200 Then
                    rgzMbr(ix, iy) = iz * 2
                ElseIf iz < 240 Then
                    rgzMbr(ix, iy) = 400 + (iz - 200) * 4
                ElseIf iz < 250 Then
                    rgzMbr(ix, iy) = 560 + (iz - 240) * 8
                Else
                    rgzMbr(ix, iy) = 640 + (iz - 250) * 10
                End If
            Next iy
        Next ix
    Else
        For ix = 0 To 8
            For iy = 0 To 8
                rgflagsMbr(ix, iy) = -1
            Next iy
        Next ix
    End If
    
End Sub









