VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LlobjCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' This class implements a Link List of Objects and is used for keeping track of
' registered event handlers.


Public llobjNext As LlobjCls    ' Next element in the chain.
Public obj As Object            ' The handler object being linked.
Public serial As Long           ' Cookie for ac.UnregisterHandler.


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    MemStats.cllobj = MemStats.cllobj + 1
    
    serial = iNil
    
End Sub

Private Sub Class_Terminate()
    
    MemStats.cllobj = MemStats.cllobj - 1
    
End Sub


