Attribute VB_Name = "Loc"
Option Explicit


'-------------
' Public data
'-------------

' Location information as it's laid out in memory.
Public Type LocinfoType
    tyobj As Long               ' Class tag of this object type.
    landblock As Long           ' 32-bit landblock ID.
    quatRot(3) As Single        ' Heading encoded as a quaternion.
    matRot(2, 2) As Single      ' Heading encoded as a 3x3 rotation matrix.
    X As Single                 ' X coord in meters within the landblock.
    Y As Single                 ' Y coord in meters within the landblock.
    z As Single                 ' Altitude in meters above or below sea level.
End Type

Public Const dungidNil = 0


'-------------
' Public APIs
'-------------

' Return the heading in degrees of the given location info block.
Public Function HeadFromLocinfo(ByRef locinfo As LocinfoType) As Single
    
    HeadFromLocinfo = HeadFromWZ(locinfo.quatRot(0), locinfo.quatRot(3))
    
End Function

' Return the heading in degrees given the W and Z elements of the heading quaternion.
Public Function HeadFromWZ(ByVal wHead As Single, ByVal zHead As Single) As Single
    
    Dim head As Single
    If wHead = 1 Or wHead = -1 Then
        head = 0
    Else
    '   wHead is actually cos(head/2); however, VB lacks an Acos function
    '   so do it the hard way.
        head = Atn(-wHead / Sqr(-wHead * wHead + 1)) + 2 * Atn(1)
    '   Now we have head/2 in radians; double it and convert to degrees.
        head = head * 360 / pi
    '   For angles less than zero or greater than 2 pi, wHead is ambiguous.
    '   Use zHead to disambiguate.
        If zHead > 0 Then head = 360 - head
    End If
    
    HeadFromWZ = head
End Function

Public Sub LatLngFromLandblockXY(ByRef latOut As Single, ByRef lngOut As Single, _
  ByVal landblock As Long, ByVal X As Single, ByVal Y As Single)
    
'   This code is based on the following empirical observations:
'
'   1.  If you go to 0.0, 0.0 and do a /loc, you get something between
'           Landblock: 7F7F001B, X: 72.0, Y: 72.0
'       and
'           Landblock: 7F7F001B, X: 95.9, Y: 95.9
'
'   2.  X and Y values vary linearly from 0 to 192 across a landblock.
'
'   3.  Those 192 XY units correspond to 0.8 map units.  That is, a landblock
'       is 0.8 x 0.8 map units in size.
'
'   Observation 1 can be interpreted in two different ways: (a) the origin is
'   at X=72,Y=72, and AC is truncating coordinate values to one digit, or (b)
'   the origin is at X=84,Y=84, and AC is rounding to one digit.  (I expected
'   to find it at X=96,Y=96, but that doesn't seem to fit the data.)
'
'   Taking interpretation (b) as correct, then the SW corner of landblock
'   00000000 must be at -(0x7F + 84/192) in landblock units.
'
'   It appears that the X and Y numbers reported by /loc are measured in
'   meters.  At least a bow with an 80-yard range can shoot about 75 XY
'   units.  This makes a landblock 192 x 192 meters, and a map unit
'   240 x 240 meters.  The entire world then is 48,960 meters (~49 km)
'   from edge to edge.
    
    Dim ilbLat As Long, ilbLng As Long, fGlobalCoords As Boolean
    Call DecodeLandblock(ilbLat, ilbLng, 0, False, fGlobalCoords, landblock)
    
    If fGlobalCoords Then
        latOut = (CSng(ilbLat - &H7F) * 192 + Y - 84) / 240
        lngOut = (CSng(ilbLng - &H7F) * 192 + X - 84) / 240
    Else
        latOut = (Y - 84) / 240
        lngOut = (X - 84) / 240
    End If
    
End Sub
    
Public Sub LandblockXYFromLatLng(ByRef landblockOut As Long, ByRef xOut As Single, ByRef yOut As Single, _
  ByVal lat As Single, ByVal lng As Single)
    
    Dim sLng As Single, sLat As Single
    sLng = (lng + 101.95) / 0.8
    sLat = (lat + 101.95) / 0.8
    
    Dim ilbLng As Long, ilbLat As Long
    ilbLng = Int(sLng)
    ilbLat = Int(sLat)
    If ilbLng < 0 Then ilbLng = 0 Else If ilbLng > 254 Then ilbLng = 254
    If ilbLat < 0 Then ilbLat = 0 Else If ilbLat > 254 Then ilbLat = 254
    
    landblockOut = DwFromWords(ilbLng * &H100 + ilbLat, 0)
    xOut = (sLng - ilbLng) * 192
    yOut = (sLat - ilbLat) * 192
    
End Sub

Public Sub DecodeLandblock(ByRef ilbLatOut As Long, ByRef ilbLngOut As Long, _
  ByRef dungidOut As Long, ByRef fIndoorsOut As Boolean, ByRef fGlobalCoordsOut As Boolean, _
  ByVal landblock As Long)
    
    ilbLngOut = Int(landblock / &H1000000) And &HFF&
    ilbLatOut = Int(landblock / &H10000) And &HFF&
    
    fIndoorsOut = (landblock And &HFF00&) <> 0
    
    If fIndoorsOut Then
        Dim dungid As Long
        dungid = Int(landblock / &H10000) And &HFFFF&
        
    '   Are we in one of the sub-sea dungeons?
        If FSubseaIlbLatLng(ilbLatOut, ilbLngOut) Then
        '   Yes; use local coords.
            dungidOut = dungid
            fGlobalCoordsOut = False
            
    '   Are we in one of the known surface-contiguous dungeons?
        ElseIf FContiguousDungid(dungid) Then
        '   Yes; use global coords.
            dungidOut = dungid
            fGlobalCoordsOut = True
            
        Else
        '   It's a surface building, not a dungeon.
            dungidOut = dungidNil
            fGlobalCoordsOut = True
        End If
    Else
    '   We're outdoors.
        dungidOut = dungidNil
        fGlobalCoordsOut = True
    End If
    
End Sub

Public Function FGlobalCoordsFromDungid(ByVal dungid As Long) As Boolean
    
    Dim ilbLng As Long, ilbLat As Long
    ilbLng = Int(dungid / &H100) And &HFF&
    ilbLat = dungid And &HFF&
    
    FGlobalCoordsFromDungid = Not FSubseaIlbLatLng(ilbLat, ilbLng)
    
End Function

Public Function FSubseaIlbLatLng(ByVal ilbLat As Long, ByVal ilbLng As Long) As Boolean
    
    If ilbLng = 0 Then
    '   Most dungeons are under the extreme western ocean.
        FSubseaIlbLatLng = True
        
    ElseIf ilbLat = 0 Then
    '   Some RQs are under the extreme southern ocean.
        FSubseaIlbLatLng = True
        
    ElseIf ilbLng + ilbLat <= 4 Then
    '   Extreme SW corner.
        FSubseaIlbLatLng = True
    
    ElseIf ilbLng < 17 And ilbLat < 18 Then
    '   Caul.
        FSubseaIlbLatLng = False
    
    ElseIf ilbLng < 8 And ilbLat >= 20 Then
    '   Most dungeons are under the extreme western ocean.
        FSubseaIlbLatLng = True
        
    ElseIf ilbLng >= &H50 And ilbLng < &H70 And ilbLat >= &H40 And ilbLat < &H80 Then
    '   RQs and a few other dungeons are in the inland sea.
        FSubseaIlbLatLng = True
        
    Else
        FSubseaIlbLatLng = False
    End If
    
End Function

Public Function FContiguousDungid(ByVal dungid As Long) As Boolean
    
    Select Case dungid
    Case &H02D4&    ' Raven Auger dungeon @ 68.4N 66.6E
        FContiguousDungid = True
    Case &H40D8&    ' Bane Grievver nest @ 71.1N 50.2W
        FContiguousDungid = True
    Case &H934B&    ' Xarabydun
        FContiguousDungid = True
    Case &H9626&    ' Lugian Dwelling (N of Qbar)
        FContiguousDungid = True
    Case &HA05B&    ' Reedshark Skinner nest @ 29.0S 26.6E
        FContiguousDungid = True
    Case &HB070&    ' Yanshi Undermine
        FContiguousDungid = True
    Case &HB131&    ' Dungeon of Tatters anteroom
        FContiguousDungid = True
    Case &HBB62&    ' Swamp Temple anteroom
        FContiguousDungid = True
    Case &HC75E&    ' Greenmire B&B
        FContiguousDungid = True
    Case &HF784&    ' Tusker Emporium
        FContiguousDungid = True
    Case Else
        FContiguousDungid = False
    End Select
    
End Function


'----------
' Privates
'----------












