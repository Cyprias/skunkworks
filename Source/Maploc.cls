VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MaplocCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' This class represents a map location in the AC world.


'--------------
' Private data
'--------------

Private landblockMbr As Long, xMbr As Single, yMbr As Single
Private zMbr As Single, headMbr As Single
Private latMbr As Single, lngMbr As Single

Private dungidMbr As Long
Private fIndoorsMbr As Boolean


'-------------------
' Public properties
'-------------------

' Return the latitude of this location.  Positive is north, negative south.
Public Property Get lat() As Single
    lat = latMbr
End Property

' Return the longitude of this locations.  Positive is east, negative west.
Public Property Get lng() As Single
    lng = lngMbr
End Property

' Return the landblock of the this location.
Public Property Get landblock() As Variant
    landblock = VFromL(landblockMbr)
End Property

' Return the x offset of this location within its landblock.
Public Property Get X() As Single
    X = xMbr
End Property

' Return the y offset of this location within its landblock.
Public Property Get Y() As Single
    Y = yMbr
End Property

' Return the altitude of this location.
Public Property Get z() As Single
    z = zMbr
End Property

' Return the compass heading of this locations, in degrees clockwise from north.
Public Property Get head() As Single
    head = headMbr
End Property

' Return the compass heading in radians clockwise from north.
Public Property Get headRad() As Single
    headRad = headMbr * 2 * 3.14159 / 360
End Property


' Return True if this location is indoors (in a dungeon or a building aboveground),
' False if outdoors.
Public Property Get fIndoors() As Boolean
    
    fIndoors = fIndoorsMbr
    
End Property

' Return True if this location is in a dungeon, False if aboveground
' (indoors or out).
Public Property Get fInDungeon() As Boolean
    
    fInDungeon = (dungidMbr <> dungidNil)
    
End Property

' Return the dungeon ID if we're in a dungeon, or zero if aboveground
' (indoors or out).
Public Property Get dungid() As Long
    
    dungid = dungidMbr
    
End Property


' Return a string representation of the maploc coordinates.
Public Property Get sz(Optional ByVal cdigit As Long = 2, _
  Optional ByVal fComma As Boolean = False) As String
Attribute sz.VB_UserMemId = 0
    
    Dim szFormat As String
    szFormat = "0." + String(cdigit, "0")
    
    sz = ""
    
    If latMbr >= 0 Then
        sz = sz + Format(latMbr, szFormat) + "N"
    Else
        sz = sz + Format(-latMbr, szFormat) + "S"
    End If
    
    If fComma Then
        sz = sz + ","
    Else
        sz = sz + " "
    End If
    
    If lngMbr >= 0 Then
        sz = sz + Format(lngMbr, szFormat) + "E"
    Else
        sz = sz + Format(-lngMbr, szFormat) + "W"
    End If
    
End Property

Public Function szEx(Optional ByVal cdigit As Long = 2, _
  Optional ByVal fComma As Boolean = False) As String
    szEx = sz(cdigit, fComma)
End Function


' Return a five-element array describing the location.  For ACScript compatibility.
Public Property Get rgv() As Variant
    
    Dim rgvT(4) As Variant
    rgvT(0) = landblockMbr
    rgvT(1) = xMbr
    rgvT(2) = yMbr
    rgvT(3) = zMbr
    rgvT(4) = headMbr
    
    rgv = rgvT
    
End Property


' Return Landblock Terrain info for this location.
Public Property Get lbt() As LbtCls
    
    If dungidMbr = dungidNil Then
        Set lbt = New LbtCls
        Call lbt.Load(landblockMbr)
    Else
        Set lbt = Nothing
    End If
    
End Property


'----------------
' Public methods
'----------------

' Return the 2D distance in map units to maploc2.
Public Function Dist2DToMaploc(ByVal maploc2 As MaplocCls) As Single
    
    Call Assert(Not (maploc2 Is Nothing), "Dist2DToMaploc: argument is null")
    
    Dist2DToMaploc = Sqr((maploc2.lat - latMbr) ^ 2 + (maploc2.lng - lngMbr) ^ 2)
    
End Function

' Return the 3D distance in map units to maploc2.
Public Function Dist3DToMaploc(ByVal maploc2 As MaplocCls) As Single
    
    Call Assert(Not (maploc2 Is Nothing), "Dist3DToMaploc: argument is null")
    
    Dist3DToMaploc = Sqr((maploc2.lat - latMbr) ^ 2 + _
      (maploc2.lng - lngMbr) ^ 2 + ((maploc2.z - zMbr) / 240) ^ 2)
    
End Function

' Return the compass heading to maploc2.
Public Function HeadToMaploc(ByVal maploc2 As MaplocCls) As Single
    
    Call Assert(Not (maploc2 Is Nothing), "HeadToMaploc: argument is null")
    
    Dim dx As Single: dx = maploc2.lng - lngMbr
    Dim dy As Single: dy = maploc2.lat - latMbr
    Dim theta As Single
    If dy = 0 Then
        If dx > 0 Then
            theta = pi / 2
        Else
            theta = -pi / 2
        End If
    Else
        theta = Atn(dx / dy)
    End If
    
    If dy < 0 Then theta = theta + pi
    If theta < 0 Then theta = theta + 2 * pi
    
    HeadToMaploc = 360 * theta / (2 * pi)
    
End Function


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------

' Initialize the location from landblock data.
Friend Sub FromLandblockXY(ByVal landblock As Long, ByVal X As Single, ByVal Y As Single, _
  ByVal z As Single, ByVal head As Single)
    
    landblockMbr = landblock
    xMbr = X
    yMbr = Y
    zMbr = z
    headMbr = head
    
    Call LatLngFromLandblockXY(latMbr, lngMbr, landblock, X, Y)
    Call DecodeLandblock(0, 0, dungidMbr, fIndoorsMbr, False, landblockMbr)
    
End Sub

' Initialize the location from latitude and longitude.
Friend Sub FromLatLng(ByVal lat As Single, ByVal lng As Single, _
  ByVal z As Single, ByVal head As Single)
    
    latMbr = lat
    lngMbr = lng
    zMbr = z
    headMbr = head
    
    Call LandblockXYFromLatLng(landblockMbr, xMbr, yMbr, lat, lng)
    Call DecodeLandblock(0, 0, dungidMbr, fIndoorsMbr, False, landblockMbr)
    
End Sub

' Initialize the location from a string in standard format (e.g. "22.5N 15.3W").
Friend Sub FromSz(ByVal sz As String)
    
    Dim szLat As String, szLng As String
    If IchInStr(0, sz, ", ") <> iNil Then
        Call SplitSz(szLat, szLng, sz, ", ")
'   Accept comma as value separator for compatibility,
'   but only if there are periods in the string as well.
'   This fixes a localization bug in locales where comma
'   is the decimal separator.
    ElseIf IchInStr(0, sz, ",") <> iNil And _
      IchInStr(0, sz, ".") <> iNil Then
        Call SplitSz(szLat, szLng, sz, ",")
    Else
        Call SplitSz(szLat, szLng, sz, " ")
    End If
    szLat = SzTrimWhiteSpace(szLat)
    szLng = SzTrimWhiteSpace(szLng)
    
    Dim lat As Single, lng As Single
    
    If FSuffixMatchSzI(szLat, "N") Then
        lat = CDbl(Left(szLat, Len(szLat) - 1))
    ElseIf FSuffixMatchSzI(szLat, "S") Then
        lat = -CDbl(Left(szLat, Len(szLat) - 1))
    Else
        lat = CDbl(szLat)
    End If
    
    If FSuffixMatchSzI(szLng, "E") Then
        lng = CDbl(Left(szLng, Len(szLng) - 1))
    ElseIf FSuffixMatchSzI(szLng, "W") Then
        lng = -CDbl(Left(szLng, Len(szLng) - 1))
    Else
        lng = CDbl(szLng)
    End If
    
    Call FromLatLng(lat, lng, 0, 0)
    
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    MemStats.cmaploc = MemStats.cmaploc + 1
    
End Sub

Private Sub Class_Terminate()
    
    MemStats.cmaploc = MemStats.cmaploc - 1
    
End Sub






















