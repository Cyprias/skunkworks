VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MbufCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' Mbuf: message buffer


'--------------
' Private data
'--------------

Private rgbMsg() As Byte
Private cbMsg As Long
Private ibNext As Long
Private tickMbr As Long

Private Const szUnknown = "(unknown)"


'------------
' Properties
'------------

Public Property Get cbOfMsg() As Long
    cbOfMsg = cbMsg
End Property

Public Property Get ibInMsg() As Long
    ibInMsg = ibNext
End Property

Public Property Get tick() As Long
    tick = tickMbr
End Property


Public Property Get mty() As Long
    
    Dim mtyHigh As Long, mtyLow As Long
    Call CopyMemory(mtyHigh, rgbMsg(0), 4)
    If mtyHigh = &HF7B0& And cbMsg >= 16 Then
        Call CopyMemory(mtyLow, rgbMsg(12), 4)
    ElseIf mtyHigh = &HF7B1& And cbMsg >= 12 Then
        Call CopyMemory(mtyLow, rgbMsg(8), 4)
    Else
        mtyLow = 0
    End If
    mty = DwFromWords(mtyHigh, mtyLow)
    
End Property

Public Property Get szMty() As String
    szMty = SzFromMty(mty)
End Property


'---------
' Methods
'---------

Public Sub ResetRead()
    
    ibNext = 0
    
End Sub

' Skip to the next DWORD boundary in the current message.  If already at
' a DWORD boundary, do nothing.
Public Sub Align_DWORD()
    
    ibNext = (ibNext + 3) And &HFFFFFFFC
    
End Sub

' Skip over the specified number of bytes in the current message.
Public Sub SkipCb(ByVal cb As Long)
    
    Call AssertCbAvail(cb)
    
    ibNext = ibNext + cb
    
End Sub

' Read and return the next BYTE (8 bits) in the current message.
Public Function Get_BYTE() As Byte
    
    Call AssertCbAvail(1)
    
    Get_BYTE = rgbMsg(ibNext)
    ibNext = ibNext + 1
    
End Function

' Read and return the next WORD (16 bits) in the current message.
Public Function Get_WORD() As Integer
    
    Call AssertCbAvail(2)
    
    Call CopyMemory(Get_WORD, rgbMsg(ibNext), 2)
    ibNext = ibNext + 2
    
End Function

' Read and return the next PackedWORD (8 or 16 bits) in the current message.
Public Function Get_PackedWORD() As Integer
    
    Get_PackedWORD = Get_BYTE()
    If (Get_PackedWORD And &H80) <> 0 Then
        Get_PackedWORD = (Get_PackedWORD And &H7F) * &H100 Or Get_BYTE()
    End If
    
End Function

' Skip over one PackedWORD (8 or 16 bits) in the current message.
Public Function Skip_PackedWORD() As Integer
    
    Call Get_PackedWORD
    
End Function

' Read and return the next DWORD (32 bits) in the current message.
Public Function Get_DWORD() As Long
    
    Call AssertCbAvail(4)
    
    Call CopyMemory(Get_DWORD, rgbMsg(ibNext), 4)
    ibNext = ibNext + 4
    
End Function

' Read and return the next PackedDWORD (16 or 32 bits) in the current message.
Public Function Get_PackedDWORD() As Long
    
    Get_PackedDWORD = Get_WORD()
    If (Get_PackedDWORD And &H8000&) <> 0 Then
        Get_PackedDWORD = (Get_PackedDWORD And &H7FFF&) * &H10000 Or _
          (CLng(Get_WORD()) And &HFFFF&)
    End If
    
End Function

' Skip over one PackedDWORD (16 or 32 bits) in the current message.
Public Function Skip_PackedDWORD() As Long
    
    Call Get_PackedDWORD
    
End Function

' Read and return the next QWORD (64 bits) in the current message.
Public Function Get_QWORD() As Double
    
    Call AssertCbAvail(8)
    
    Dim dwLow As Long, dwHigh As Long
    Call CopyMemory(dwLow, rgbMsg(ibNext), 4)
    Call CopyMemory(dwHigh, rgbMsg(ibNext + 4), 4)
    ibNext = ibNext + 8
    
    Get_QWORD = CDbl(dwHigh) * 4294967296# + dwLow
    
End Function

' Read and return the next single-precision float in the current message.
Public Function Get_float() As Single
    
    Call AssertCbAvail(4)
    
    Call CopyMemory(Get_float, rgbMsg(ibNext), 4)
    ibNext = ibNext + 4
    
End Function

' Read and return the next double-precision float in the current message.
Public Function Get_double() As Double
    
    Call AssertCbAvail(8)
    
    Call CopyMemory(Get_double, rgbMsg(ibNext), 8)
    ibNext = ibNext + 8
    
End Function

' Read and return a string from the current message.
Public Function Get_String() As String
    
    Dim cch As Long, sz As String
    
    Call Align_DWORD
    cch = (Get_WORD() And &HFFFF&)
    
    Call AssertCbAvail(cch)
    sz = String(cch, vbNullChar)
    Call CopyMemToSz(sz, rgbMsg(ibNext), cch)
    ibNext = ibNext + cch
    
    Call Align_DWORD
    
    Get_String = sz
    
End Function

' Skip over one string in the current message.
Public Sub Skip_String()
    
    Dim cch As Long
    
    Call Align_DWORD
    cch = (Get_WORD() And &HFFFF&)
    Call SkipCb(cch)
    Call Align_DWORD
    
End Sub

' Read and return a WString from the current message.
Public Function Get_WString() As String
    
    Dim sz As String, cch As Long, ich As Long
    sz = ""
    cch = Get_PackedWORD()
    For ich = 0 To cch - 1
        sz = sz + ChrW(Get_WORD() And &HFFFF&)
    Next ich
    
    Get_WString = sz
    
End Function

' Skip over one WString in the current message.
Public Sub Skip_WString()
    
    Dim cch As Long
    cch = (Get_WORD() And &HFFFF&)
    Call SkipCb(cch * 2)
    
End Sub

Public Function Get_Variant() As Variant
    
    Call GetVariantByRef(Get_Variant)
    
End Function

Friend Function GetVariantByRef(ByRef v As Variant)
    Call TraceEnter("Mbuf.GetVariantByRef")
    
    Dim vtype As Long
    vtype = Get_DWORD()
    Call TraceLine("vtype = " + SzHex(vtype, 4))
    
    If (vtype And vbArray) <> 0 Then
        Dim cv As Long
        cv = Get_DWORD()
        Call TraceLine("cv = " + CStr(cv))
        ReDim v(cv - 1)
        Dim iv
        For iv = 0 To cv - 1
            Call GetVariantData(v(iv), vtype And Not vbArray)
        Next iv
    Else
        Call GetVariantData(v, vtype)
    End If
    
    Call TraceExit("Mbuf.GetVariantByRef", SzHex(VarType(v), 4))
End Function

Private Sub GetVariantData(ByRef v As Variant, ByVal vtype As Long)
    Call TraceEnter("Mbuf.GetVariantData", SzHex(vtype, 4))
    
    Select Case vtype
    Case vbEmpty
        v = Empty
    Case vbNull
        v = Null
    Case vbBoolean
        v = CBool(Get_DWORD())
    Case vbByte
        v = Get_BYTE()
    Case vbInteger
        v = Get_WORD()
    Case vbLong
        v = Get_DWORD()
    Case vbSingle
        v = Get_float()
    Case vbDouble
        v = Get_double()
    Case vbDate
        v = CDate(Get_double())
    Case vbString
        v = Get_String()
    Case vbVariant
        Call GetVariantByRef(v)
    Case Else
        Call SkError("Unsupported variant type " + CStr(vtype))
    End Select
    
    Call TraceExit("Mbuf.GetVariantData")
End Sub


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------

Friend Sub FromRgb(rgb() As Byte, ByVal cb As Long)
    
    cbMsg = cb
    ReDim rgbMsg(cbMsg - 1)
    Call CopyMemory(rgbMsg(0), rgb(0), cbMsg)
    
End Sub

Friend Sub StartPut()
    
    cbMsg = 0
    
End Sub

Friend Sub PutMty(ByVal mty As Long)
    
    Call Put_DWORD(((mty And &HFFFF0000) \ &H10000) And &HFFFF&)
    If (mty And &HFFFF&) <> 0 Then
        Call Put_DWORD(0)
        Call Put_DWORD(0)
        Call Put_DWORD(mty And &HFFFF&)
    End If
    
End Sub

' Append a BYTE (8 bits) to the current message.
Friend Sub Put_BYTE(ByVal b As Byte)
    
    Call EnsureCb(1)
    rgbMsg(cbMsg) = b
    cbMsg = cbMsg + 1
    
End Sub

' Append a WORD (16 bits) to the current message.
Friend Sub Put_WORD(ByVal w As Integer)
    
    Call EnsureCb(2)
    Call CopyMemory(rgbMsg(cbMsg), w, 2)
    cbMsg = cbMsg + 2
    
End Sub

' Append a DWORD (32 bits) to the current message.
Friend Sub Put_DWORD(ByVal dw As Long)
    
    Call EnsureCb(4)
    Call CopyMemory(rgbMsg(cbMsg), dw, 4)
    cbMsg = cbMsg + 4
    
End Sub

' Append a single-precision float to the current message.
Friend Sub Put_float(ByVal float As Single)
    
    Call EnsureCb(4)
    Call CopyMemory(rgbMsg(cbMsg), float, 4)
    cbMsg = cbMsg + 4
    
End Sub

' Append a double-precision float to the current message.
Friend Sub Put_double(ByVal d As Double)
    
    Call EnsureCb(8)
    Call CopyMemory(rgbMsg(cbMsg), d, 8)
    cbMsg = cbMsg + 8
    
End Sub

' Append a string to the current message.
Friend Sub Put_String(ByVal sz As String)
    
    Dim cch As Long
    cch = Len(sz)
    
    Call PutAlignDword
'   Workaround for 32K overflow bug:
'   Call Put_WORD(cch + 1)
    Call EnsureCb(2)
    Call CopyMemory(rgbMsg(cbMsg), cch, 2)
    cbMsg = cbMsg + 2
    
    Call EnsureCb(cch)
    Call CopyMemFromSz(rgbMsg(cbMsg), sz, cch)
    cbMsg = cbMsg + cch
    
    Call PutAlignDword
    
End Sub

' Append a variant to the current message.
Friend Sub Put_Variant(ByRef v As Variant)
    
    Dim vtype As Long
    vtype = VarType(v)
    Call Put_DWORD(vtype)
    
    If (vtype And vbArray) <> 0 Then
        Dim cv As Long
        cv = UBound(v) + 1
        Call Put_DWORD(cv)
        Dim iv
        For iv = 0 To cv - 1
            Call PutVariantData(v(iv), vtype And Not vbArray)
        Next iv
    Else
        Call PutVariantData(v, vtype)
    End If
    
End Sub

Private Sub PutVariantData(ByRef v As Variant, ByVal vtype As Long)
    
    Select Case vtype
    Case vbNull, vbEmpty
    '   No value.
    Case vbBoolean
        Call Put_DWORD(CLng(v))
    Case vbByte
        Call Put_BYTE(v)
    Case vbInteger
        Call Put_WORD(v)
    Case vbLong
        Call Put_DWORD(v)
    Case vbSingle
        Call Put_float(v)
    Case vbDouble
        Call Put_double(v)
    Case vbDate
        Call Put_double(CDbl(v))
    Case vbString
        Call Put_String(v)
    Case vbVariant
        Call Put_Variant(v)
    Case Else
        Call SkError("Unsupported variant type " + CStr(vtype))
    End Select
    
End Sub

Friend Sub PutRgdw(rgdw() As Long, cdw As Long)
    
    Dim cb As Long
    cb = cdw * 4
    Call EnsureCb(cb)
    Call CopyMemory(rgbMsg(cbMsg), rgdw(0), cb)
    cbMsg = cbMsg + cb
    
End Sub

Friend Sub PutAlignDword()
    
    Dim cb As Long
    cb = ((cbMsg + 3) And &HFFFFFFFC) - cbMsg
    If cb > 0 Then
        Call EnsureCb(cb)
        Call ZeroMemory(rgbMsg(cbMsg), cb)
        cbMsg = cbMsg + cb
    End If
    
End Sub

Friend Sub GetAsRgb(rgb() As Byte)
    
    ReDim rgb(cbMsg - 1)
    Call CopyMemory(rgb(0), rgbMsg(0), cbMsg)
    
End Sub

Friend Function FQueueMsg(ByVal queue As QueueCls, _
  ByVal cmsecWait As Long, ByVal fDoEvents As Boolean) As Boolean
    
    FQueueMsg = queue.FWriteMsg(rgbMsg, cbMsg, GetTickCount(), cmsecWait, fDoEvents)
    
End Function


' Read a single message from the given queue.
Friend Function FDequeueMsg(ByVal queue As QueueCls, _
  ByVal cmsecWait As Long, ByVal fDoEvents As Boolean) As Boolean
    
    FDequeueMsg = queue.FReadMsg(rgbMsg, cbMsg, tickMbr, cmsecWait, fDoEvents)
    ibNext = 0
    
End Function


Friend Function MbufClone() As MbufCls
    
    Set MbufClone = New MbufCls
    Call MbufClone.CloneFrom(Me)
    
End Function

Friend Sub CloneFrom(ByVal mbuf As MbufCls)
    
    Call mbuf.GetAsRgb(rgbMsg)
    cbMsg = mbuf.cbOfMsg
    ibNext = 0
    tickMbr = mbuf.tick
    
End Sub


Friend Function FUnknownMty(ByVal mty As Long) As Boolean
    
    FUnknownMty = (SzFromMty(mty) = szUnknown)
    
End Function


' Display the current message to the debug log.
Friend Function PxMsg(Optional ByVal fWholeMsg As Boolean = True)
    
    Call DebugLine("")
    Call DebugLine("tick  = " + CStr(tickMbr))
    Call DebugLine("cbMsg = " + CStr(cbMsg))
    
    Dim mty As Long
    Dim ib As Long, fContinue As Boolean
    ib = 0
    fContinue = True
    Do While ib < cbMsg And fContinue
        Dim szLine As String
        
        If ib = 4 And (cbMsg And 3) = 1 Then
        '   Hack to deal with those annoying one-byte sequence fields.
            
            szLine = SzHex(ib, 4) + ":" + _
              "  " + SzPadLeft(SzHex(rgbMsg(ib), 2), 8) + _
              "  " + Space(4) + " " + Space(4) + _
              "  " + SzAsciiFromDw(rgbMsg(ib))
            
            ib = ib + 1
        Else
            Dim dw As Long, w1 As Integer, w2 As Integer
            Call CopyMemory(dw, rgbMsg(ib), 4)
            Call CopyMemory(w1, rgbMsg(ib), 2)
            Call CopyMemory(w2, rgbMsg(ib + 2), 2)
            
            szLine = SzHex(ib, 4) + ":" + _
              "  " + SzHex(dw, 8) + _
              "  " + SzHex(w1 And &HFFFF&, 4) + " " + SzHex(w2 And &HFFFF&, 4) + _
              "  " + SzAsciiFromDw(dw)
            If ib = 0 Then
                mty = DwFromWords(dw, 0)
                szLine = szLine + "  " + SzFromMty(mty)
                If mty <> mtyGameEvent And mty <> mtyClientEvent And Not fWholeMsg Then fContinue = False
            ElseIf ib = 8 And mty = mtyClientEvent Then
                mty = mty Or dw
                szLine = szLine + "  " + SzFromMty(mty)
                If Not fWholeMsg Then fContinue = False
            ElseIf ib = 12 And mty = mtyGameEvent Then
                mty = mty Or dw
                szLine = szLine + "  " + SzFromMty(mty)
                If Not fWholeMsg Then fContinue = False
            End If
            
            ib = ib + 4
        End If
        
        Call DebugLine(szLine)
    Loop
    
End Function


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceEnter("Mbuf.Initialize")
    
    ReDim rgbMsg(4095)
    tickMbr = GetTickCount()
    
    Call TraceExit("Mbuf.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceEnter("Mbuf.Terminate")
    
    Call TraceExit("Mbuf.Terminate")
End Sub


Private Sub EnsureCb(ByVal cb As Long)
    
    If cbMsg + cb > UBound(rgbMsg) + 1 Then
        ReDim Preserve rgbMsg(Round((cbMsg + cb) * 1.1))
    End If
    
End Sub


' Reinterpret a DWORD as a four-character string.
Private Function SzAsciiFromDw(ByVal dw As Long) As String
    
    SzAsciiFromDw = _
      ChFromB(dw And &HFF&) + _
      ChFromB(Int(dw / &H100&) And &HFF&) + _
      ChFromB(Int(dw / &H10000) And &HFF&) + _
      ChFromB(Int(dw / &H1000000) And &HFF&)

End Function

Private Function ChFromB(ByVal b As Byte) As String
    
    If b >= &H20 And b < &H7F Then
        ChFromB = Chr(b)
    Else
        ChFromB = "."
    End If
    
End Function


Private Sub AssertCbAvail(ByVal cb As Long)
    
    If ibNext + cb > cbMsg Then
        Call SkError("Decoding error.")
    End If
    
End Sub


Private Function SzFromMty(ByVal mty As Long) As String
    
    Select Case mty
    
    Case mtyDestroyObject: SzFromMty = "mtyDestroyObject"
    Case mtyAdjustStackSize: SzFromMty = "mtyAdjustStackSize"
    Case mtyPlayerKill: SzFromMty = "mtyPlayerKill"
    Case mtyIndirectText: SzFromMty = "mtyIndirectText"
    Case mtyEmoteText: SzFromMty = "mtyEmoteText"
    Case mtyCreatureMessage: SzFromMty = "mtyCreatureMessage"
    Case mtyCreatureMessageRanged: SzFromMty = "mtyCreatureMessageRanged"
    Case mtySetCharacterDWORD: SzFromMty = "mtySetCharacterDWORD"
    Case mtySetObjectDWORD: SzFromMty = "mtySetObjectDWORD"
    Case mtySetCharacterQWORD: SzFromMty = "mtySetCharacterQWORD"
    Case mtySetObjectBoolean: SzFromMty = "mtySetObjectBoolean"
    Case mtySetObjectString: SzFromMty = "mtySetObjectString"
    Case mtySetObjectResource: SzFromMty = "mtySetObjectResource"
    Case mtySetCharacterLink: SzFromMty = "mtySetCharacterLink"
    Case mtySetObjectLink: SzFromMty = "mtySetObjectLink"
    Case mtySetCharacterPosition: SzFromMty = "mtySetCharacterPosition"
    Case mtySetCharacterSkillLevel: SzFromMty = "mtySetCharacterSkillLevel"
    Case mtySetCharacterSkillState: SzFromMty = "mtySetCharacterSkillState"
    Case mtySetCharacterAttribute: SzFromMty = "mtySetCharacterAttribute"
    Case mtySetCharacterMaximumVital: SzFromMty = "mtySetCharacterMaximumVital"
    Case mtySetCharacterCurrentVital: SzFromMty = "mtySetCharacterCurrentVital"
    Case mtyLifestoneMaterialize: SzFromMty = "mtyLifestoneMaterialize"
    Case mtyChangeModel: SzFromMty = "mtyChangeModel"
    Case mtyCharCreationInitilisation: SzFromMty = "mtyCharCreationInitilisation"
    Case mtyEnd3DMode: SzFromMty = "mtyEnd3DMode"
    Case mtyCharDeletion: SzFromMty = "mtyCharDeletion"
    Case mtyRequestLogin: SzFromMty = "mtyRequestLogin"
    Case mtyCharacterList: SzFromMty = "mtyCharacterList"
    Case mtyCharacterLoginFailure: SzFromMty = "mtyCharacterLoginFailure"
    Case mtyCreateObject: SzFromMty = "mtyCreateObject"
'    Case mtyLoginCharacter: SzFromMty = "mtyLoginCharacter"
    Case mtyRemoveItem: SzFromMty = "mtyRemoveItem"
    Case mtySetPositionAndMotion: SzFromMty = "mtySetPositionAndMotion"
    Case mtyWieldObject: SzFromMty = "mtyWieldObject"
    Case mtyMoveObjectIntoInventory: SzFromMty = "mtyMoveObjectIntoInventory"
    Case mtyToggleObjectVisibility: SzFromMty = "mtyToggleObjectVisibility"
    Case mtyAnimation: SzFromMty = "mtyAnimation"
    Case mtyJumping: SzFromMty = "mtyJumping"
    Case mtyApplySoundEffect: SzFromMty = "mtyApplySoundEffect"
    Case mtyEnterPortalMode: SzFromMty = "mtyEnterPortalMode"
    Case mtyApplyVisualSoundEffect: SzFromMty = "mtyApplyVisualSoundEffect"
    Case mtyGameEvent: SzFromMty = "mtyGameEvent"
    Case mtyUpdateObject: SzFromMty = "mtyUpdateObject"
    Case mtyTurbineChat: SzFromMty = "mtyTurbineChat"
    Case mtyStart3DMode: SzFromMty = "mtyStart3DMode"
    Case mtyServerMessage: SzFromMty = "mtyServerMessage"
    Case mtyServerName: SzFromMty = "mtyServerName"
    Case mtyUpdateResource: SzFromMty = "mtyUpdateResource"
    Case mtyDatFilePatchList: SzFromMty = "mtyDatFilePatchList"
    Case mtyMessageBox: SzFromMty = "mtyMessageBox"
    Case mtyLoginCharacter: SzFromMty = "mtyLoginCharacter"
    Case mtyAllegianceInfo: SzFromMty = "mtyAllegianceInfo"
    Case mtyInsertInventoryItem: SzFromMty = "mtyInsertInventoryItem"
    Case mtyWearItem: SzFromMty = "mtyWearItem"
    Case mtyCloseContainer: SzFromMty = "mtyCloseContainer"
    Case mtyApproachVendor: SzFromMty = "mtyApproachVendor"
    Case mtyFailureToGiveItem: SzFromMty = "mtyFailureToGiveItem"
    Case mtyRemoveFellowshipMember: SzFromMty = "mtyRemoveFellowshipMember"
    Case mtyDismissFellowshipMember: SzFromMty = "mtyDismissFellowshipMember"
    Case mtyReadTableOfContents: SzFromMty = "mtyReadTableOfContents"
    Case mtyReadPage: SzFromMty = "mtyReadPage"
    Case mtyIdentifyObject: SzFromMty = "mtyIdentifyObject"
    Case mtyGroupChat: SzFromMty = "mtyGroupChat"
    Case mtySetPackContents: SzFromMty = "mtySetPackContents"
    Case mtyDropFromInventory: SzFromMty = "mtyDropFromInventory"
    Case mtyAttackCompleted: SzFromMty = "mtyAttackCompleted"
    Case mtyDeleteSpellFromSpellbook: SzFromMty = "mtyDeleteSpellFromSpellbook"
    Case mtyYourDeath: SzFromMty = "mtyYourDeath"
    Case mtyKillDeathMessage: SzFromMty = "mtyKillDeathMessage"
    Case mtyInflictMeleeDamage: SzFromMty = "mtyInflictMeleeDamage"
    Case mtyReceiveMeleeDamage: SzFromMty = "mtyReceiveMeleeDamage"
    Case mtyOtherMeleeEvade: SzFromMty = "mtyOtherMeleeEvade"
    Case mtySelfMeleeEvade: SzFromMty = "mtySelfMeleeEvade"
    Case mtyStartMeleeAttack: SzFromMty = "mtyStartMeleeAttack"
    Case mtyUpdateHealth: SzFromMty = "mtyUpdateHealth"
    Case mtyAgeCommandResult: SzFromMty = "mtyAgeCommandResult"
    Case mtyReadyPreviousActionComplete: SzFromMty = "mtyReadyPreviousActionComplete"
    Case mtyPingReply: SzFromMty = "mtyPingReply"
    Case mtySquelchedUsersList: SzFromMty = "mtySquelchedUsersList"
    Case mtyEnterTrade: SzFromMty = "mtyEnterTrade"
    Case mtyEndTrade: SzFromMty = "mtyEndTrade"
    Case mtyAddTradeItem: SzFromMty = "mtyAddTradeItem"
    Case mtyAcceptTrade: SzFromMty = "mtyAcceptTrade"
    Case mtyUnAcceptTrade: SzFromMty = "mtyUnAcceptTrade"
    Case mtyResetTrade: SzFromMty = "mtyResetTrade"
    Case mtyFailureToTradeAnItem: SzFromMty = "mtyFailureToTradeAnItem"
    Case mtyHousePurchaseMaintainence: SzFromMty = "mtyHousePurchaseMaintainence"
    Case mtyHouseGuestList: SzFromMty = "mtyHouseGuestList"
    Case mtyUpdateItemManaBar: SzFromMty = "mtyUpdateItemManaBar"
    Case mtyHousesAvailable: SzFromMty = "mtyHousesAvailable"
    Case mtyConfirmationPanel: SzFromMty = "mtyConfirmationPanel"
    Case mtyConfirmationPanelClosed: SzFromMty = "mtyConfirmationPanelClosed"
    Case mtyAllegianceMemberLoginOut: SzFromMty = "mtyAllegianceMemberLoginOut"
    Case mtyActionFailure: SzFromMty = "mtyActionFailure"
    Case mtyErrorWithParameter: SzFromMty = "mtyErrorWithParameter"
    Case mtyTell: SzFromMty = "mtyTell"
    Case mtyCreateFellowship: SzFromMty = "mtyCreateFellowship"
    Case mtyDisbandFellowship: SzFromMty = "mtyDisbandFellowship"
    Case mtyAddFellowshipMember: SzFromMty = "mtyAddFellowshipMember"
    Case mtyAddSpellToSpellbook: SzFromMty = "mtyAddSpellToSpellbook"
    Case mtyAddCharacterEnchantment: SzFromMty = "mtyAddCharacterEnchantment"
    Case mtyRemoveCharacterEnchantment: SzFromMty = "mtyRemoveCharacterEnchantment"
    Case mtyRemoveMultipleCharacterEnchantments: SzFromMty = "mtyRemoveMultipleCharacterEnchantments"
    Case mtyRemoveAllCharacterEnchantmentsSilent: SzFromMty = "mtyRemoveAllCharacterEnchantmentsSilent"
    Case mtyRemoveCharacterEnchantmentSilent: SzFromMty = "mtyRemoveCharacterEnchantmentSilent"
    Case mtyRemoveMultipleCharacterEnchantmentsSilent: SzFromMty = "mtyRemoveMultipleCharacterEnchantmentsSilent"
    Case mtyStatusMessage: SzFromMty = "mtyStatusMessage"
    
    Case mtyClientEvent: SzFromMty = "mtyClientEvent"
    Case mtyCTSUpdateSingleOption: SzFromMty = "mtyCTSUpdateSingleOption"
    Case mtyCTSUseItem: SzFromMty = "mtyCTSUseItem"
    Case mtyCTSUpdateOptions: SzFromMty = "mtyCTSUpdateOptions"
    
    Case mtySWCommand: SzFromMty = "mtySWCommand"
    Case mtySWControl: SzFromMty = "mtySWControl"
    Case mtySWLeaveVendor: SzFromMty = "mtySWLeaveVendor"
    Case mtySWPropVal: SzFromMty = "mtySWPropVal"
    Case mtySWPluginMsg: SzFromMty = "mtySWPluginMsg"
    Case mtySWHotkey: SzFromMty = "mtySWHotkey"
    Case mtySWNavStop: SzFromMty = "mtySWNavStop"
    Case mtySWDestroyAcos: SzFromMty = "mtySWDestroyAcos"
    Case mtySWDisconnect: SzFromMty = "mtySWDisconnect"
    Case mtySWChatTextIntercept: SzFromMty = "mtySWChatTextIntercept"
    Case mtySWStatusTextIntercept: SzFromMty = "mtySWStatusTextIntercept"
    Case mtySWOpenContainerPanel: SzFromMty = "mtySWOpenContainerPanel"
    Case mtySWCloseContainer: SzFromMty = "mtySWCloseContainer"
    Case mtySWChatClickIntercept: SzFromMty = "mtySWChatClickIntercept"
    
    Case Else: SzFromMty = szUnknown
    End Select
    
End Function



































