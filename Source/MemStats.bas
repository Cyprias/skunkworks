Attribute VB_Name = "MemStats"
Option Explicit


'-------------
' Public data
'-------------

Public tickMemStats As Long
Public caco As Long
Public cmaploc As Long
Public ccoaco As Long
Public cllobj As Long
Public cacsh As Long
Public cWaitEvent As Long


'--------------
' Private data
'--------------

Private szFileMemStats As String
Private pbStackRoot As Long

Private Type PROCESS_MEMORY_COUNTERS
    cb As Long
    PageFaultCount As Long
    PeakWorkingSetSize As Long
    WorkingSetSize As Long
    QuotaPeakPagedPoolUsage As Long
    QuotaPagedPoolUsed As Long
    QuotaPeakNonPagedPoolUsage As Long
    QuotaNonPagedPoolUsed As Long
    PagefileUsage As Long
    PeakPagefileUsage As Long
End Type

Declare Function GetProcessMemoryInfo Lib "psapi" (ByVal hprocess As Long, _
  pmc As PROCESS_MEMORY_COUNTERS, ByVal cb As Long) As Long


'------------------
' Public functions
'------------------

Public Sub LogMemStats(Optional ByVal tick As Long = 0)
    
    If swoptions.fMemStats Then
        Dim tsMemStats As TextStream
        If szFileMemStats = "" Then
            szFileMemStats = swoptions.SzFileInAppDir("MemStats.csv")
            Set tsMemStats = fso.CreateTextFile(szFileMemStats)
            Call tsMemStats.WriteLine( _
                "tick" + "," + _
                "cacoOtable" + "," + _
                "caco" + "," + _
                "cmaploc" + "," + _
                "ccoaco" + "," + _
                "cllobj" + "," + _
                "cacsh" + "," + _
                "cWaitEvent" + "," + _
                "stack" + "," + _
                "skapi mem" + "," + _
                "client mem" _
                )
            
            Dim dwJunk As Long: dwJunk = 0
            pbStackRoot = VarPtr(dwJunk)
        Else
            Set tsMemStats = fso.OpenTextFile(szFileMemStats, ForAppending)
        End If
        
        Dim cbSkapi As Long, cbClient As Long
        cbSkapi = CbGetWorkingSet(GetCurrentProcess())
        If acapp.hprocess <> hNil Then
            cbClient = CbGetWorkingSet(acapp.hprocess)
        Else
            cbClient = 0
        End If
        
    '   Measure stack depth.
        Dim pbStackCur As Long, cbStack As Long
        pbStackCur = VarPtr(cbSkapi)
        If pbStackCur > pbStackRoot Then pbStackRoot = pbStackCur
    '   Stack grows downward from pbStackRoot.
        cbStack = pbStackRoot - pbStackCur
        
        Call tsMemStats.WriteLine( _
          CStr(tick) + "," + _
          CStr(otable.caco) + "," + _
          CStr(caco) + "," + _
          CStr(cmaploc) + "," + _
          CStr(ccoaco) + "," + _
          CStr(cllobj) + "," + _
          CStr(cacsh) + "," + _
          CStr(cWaitEvent) + "," + _
          CStr(cbStack) + "," + _
          CStr(cbSkapi) + "," + _
          CStr(cbClient) _
          )
        Call tsMemStats.Close
        Call DebugLine("MemStats tick " + CStr(tick))
    End If
    
    tickMemStats = tick
    
End Sub


'----------
' Privates
'----------

Private Function CbGetWorkingSet(ByVal hprocess As Long)

    If fRunningOnNT Then
        Dim pmc As PROCESS_MEMORY_COUNTERS
        pmc.cb = Len(pmc)
        Call GetProcessMemoryInfo(hprocess, pmc, Len(pmc))
        CbGetWorkingSet = pmc.WorkingSetSize
    Else
        CbGetWorkingSet = 0
    End If
    
End Function











