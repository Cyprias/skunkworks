Attribute VB_Name = "MessageCrackers"
Option Explicit

' -------------------------------------------------------------------
' !!!!!!!!!!!!!!!!!!!!!!   R E A D   T H I S   !!!!!!!!!!!!!!!!!!!!!!
' -------------------------------------------------------------------
' !!!  This file is generated automatically by GenCrackers.xsl.   !!!
' !!!    Any edits you make to it by hand WILL be overwritten.    !!!
' !!!      Make your changes to MessageCrackers.xml instead.      !!!
' -------------------------------------------------------------------

' Revision info for the files used in the generation of this file.
Public Const szVerMessages As String = "2006.08.22.1"
Public Const szTModMessageCrackers As String = "10/22/2006 12:54:41 PM"
Public Const szTModGenCrackers As String = "9/9/2006 10:22:58 AM"

Public evidLast As Long
Public mpevidcevent(evidMax-1) As Long
Public fPxThisMsg As Boolean

Private tickDelOrphans As Long

' Extract the message type and subtype (if relevant) from the given mbuf
' and call the appropriate cracker function.
Public Function EvidDispatchEvent(ByVal mbuf As MbufCls) As Long
    evidLast = evidNil
    
'   Hex-dump message to debug log as required by prevailing debug options.
    If swoptions.pxm = pxmAll Then Call mbuf.PxMsg(swoptions.fPxWholeMsg)
    fPxThisMsg = False
    
'   Construct a unified key from the message type and subtype (if present).
    Dim mty As Long, oidChar As Long
    mty = DwFromWords(mbuf.Get_DWORD(), 0)
    If mty = mtyGameEvent Then
        oidChar = mbuf.Get_DWORD()
        Call mbuf.SkipCb(4)
        mty = mty Or mbuf.Get_DWORD()
    ElseIf mty = mtyClientEvent Then
        If Not otable.acoChar Is Nothing Then
            oidChar = otable.acoChar.oid
        Else
            oidChar = oidNil
        End If
        Call mbuf.SkipCb(4)
        mty = mty Or mbuf.Get_DWORD()
        If swoptions.fPxCTS Then Call mbuf.PxMsg(swoptions.fPxWholeMsg)
    ElseIf Not otable.acoChar Is Nothing Then
        oidChar = otable.acoChar.oid
    Else
        oidChar = oidNil
    End If
    
'   Dispatch to the appropriate cracker.
    Select Case mty
        
    Case &H00240000&	' Destroy Object
        Call Decode0024(mbuf, oidChar)
        
    Case &H01970000&	' Adjust Stack Size
        Call Decode0197(mbuf, oidChar)
        
    Case &H019E0000&	' Player Kill
        Call Decode019E(mbuf, oidChar)
        
    Case &H01E00000&	' Indirect Text
        Call Decode01E0(mbuf, oidChar)
        
    Case &H01E20000&	' Emote Text
        Call Decode01E2(mbuf, oidChar)
        
    Case &H02BB0000&	' Creature Message
        Call Decode02BB(mbuf, oidChar)
        
    Case &H02BC0000&	' Creature Message (Ranged)
        Call Decode02BC(mbuf, oidChar)
        
    Case &H02CD0000&	' Set Character DWORD
        Call Decode02CD(mbuf, oidChar)
        
    Case &H02CE0000&	' Set Object DWORD
        Call Decode02CE(mbuf, oidChar)
        
    Case &H02CF0000&	' Set Character QWORD
        Call Decode02CF(mbuf, oidChar)
        
    Case &H02D10000&	' Set Character Boolean
        Call Decode02D1(mbuf, oidChar)
        
    Case &H02D20000&	' Set Object Boolean
        Call Decode02D2(mbuf, oidChar)
        
    Case &H02D60000&	' Set Object String
        Call Decode02D6(mbuf, oidChar)
        
    Case &H02D80000&	' Set Object Resource
        Call Decode02D8(mbuf, oidChar)
        
    Case &H02D90000&	' Set Character Link
        Call Decode02D9(mbuf, oidChar)
        
    Case &H02DA0000&	' Set Object Link
        Call Decode02DA(mbuf, oidChar)
        
    Case &H02DD0000&	' Set Character Skill Level
        Call Decode02DD(mbuf, oidChar)
        
    Case &H02E10000&	' Set Character Skill State
        Call Decode02E1(mbuf, oidChar)
        
    Case &H02E30000&	' Set Character Attribute
        Call Decode02E3(mbuf, oidChar)
        
    Case &H02E70000&	' Set Character Maximum Vital
        Call Decode02E7(mbuf, oidChar)
        
    Case &H02E90000&	' Set Character Current Vital
        Call Decode02E9(mbuf, oidChar)
        
    Case &HF6530000&	' End 3D Mode
        Call DecodeF653(mbuf, oidChar)
        
    Case &HF6580000&	' Character List
        Call DecodeF658(mbuf, oidChar)
        
    Case &HF7450000&	' Create Object
        Call DecodeF745(mbuf, oidChar)
        
    Case &HF7470000&	' Remove Item
        Call DecodeF747(mbuf, oidChar)
        
    Case &HF7480000&	' Set Position And Motion
        Call DecodeF748(mbuf, oidChar)
        
    Case &HF74A0000&	' Move Object Into Inventory
        Call DecodeF74A(mbuf, oidChar)
        
    Case &HF74B0000&	' Toggle Object Visibility
        Call DecodeF74B(mbuf, oidChar)
        
    Case &HF74C0000&	' Animation
        Call DecodeF74C(mbuf, oidChar)
        
    Case &HF7510000&	' Enter Portal Mode
        Call DecodeF751(mbuf, oidChar)
        
    Case &HF7B00013&	' Login Character
        Call DecodeF7B0_0013(mbuf, oidChar)
        
    Case &HF7B00022&	' Insert Inventory Item
        Call DecodeF7B0_0022(mbuf, oidChar)
        
    Case &HF7B00023&	' Wear Item
        Call DecodeF7B0_0023(mbuf, oidChar)
        
    Case &HF7B00062&	' Approach Vendor
        Call DecodeF7B0_0062(mbuf, oidChar)
        
    Case &HF7B000A3&	' Quit Fellowship
        Call DecodeF7B0_00A3(mbuf, oidChar)
        
    Case &HF7B000A4&	' Dismiss Fellowship Member
        Call DecodeF7B0_00A4(mbuf, oidChar)
        
    Case &HF7B000C9&	' Identify Object
        Call DecodeF7B0_00C9(mbuf, oidChar)
        
    Case &HF7B00147&	' Group Chat
        Call DecodeF7B0_0147(mbuf, oidChar)
        
    Case &HF7B00196&	' Set Pack Contents
        Call DecodeF7B0_0196(mbuf, oidChar)
        
    Case &HF7B0019A&	' Drop From Inventory
        Call DecodeF7B0_019A(mbuf, oidChar)
        
    Case &HF7B001A8&	' Delete Spell from Spellbook
        Call DecodeF7B0_01A8(mbuf, oidChar)
        
    Case &HF7B001AD&	' Kill Death Message
        Call DecodeF7B0_01AD(mbuf, oidChar)
        
    Case &HF7B001B1&	' Inflict Melee Damage
        Call DecodeF7B0_01B1(mbuf, oidChar)
        
    Case &HF7B001B2&	' Receive Melee Damage
        Call DecodeF7B0_01B2(mbuf, oidChar)
        
    Case &HF7B001B3&	' Other Melee Evade
        Call DecodeF7B0_01B3(mbuf, oidChar)
        
    Case &HF7B001B4&	' Self Melee Evade
        Call DecodeF7B0_01B4(mbuf, oidChar)
        
    Case &HF7B001C0&	' Update Health
        Call DecodeF7B0_01C0(mbuf, oidChar)
        
    Case &HF7B001C7&	' Ready
        Call DecodeF7B0_01C7(mbuf, oidChar)
        
    Case &HF7B001FD&	' Enter Trade
        Call DecodeF7B0_01FD(mbuf, oidChar)
        
    Case &HF7B001FF&	' End Trade
        Call DecodeF7B0_01FF(mbuf, oidChar)
        
    Case &HF7B00200&	' Add Trade Item
        Call DecodeF7B0_0200(mbuf, oidChar)
        
    Case &HF7B00202&	' Accept Trade
        Call DecodeF7B0_0202(mbuf, oidChar)
        
    Case &HF7B00205&	' Reset Trade
        Call DecodeF7B0_0205(mbuf, oidChar)
        
    Case &HF7B00264&	' Update Item Mana Bar
        Call DecodeF7B0_0264(mbuf, oidChar)
        
    Case &HF7B00274&	' Confirmation Panel
        Call DecodeF7B0_0274(mbuf, oidChar)
        
    Case &HF7B0028A&	' Action Failure
        Call DecodeF7B0_028A(mbuf, oidChar)
        
    Case &HF7B002BD&	' Tell
        Call DecodeF7B0_02BD(mbuf, oidChar)
        
    Case &HF7B002BE&	' Create Fellowship
        Call DecodeF7B0_02BE(mbuf, oidChar)
        
    Case &HF7B002BF&	' Disband Fellowship
        Call DecodeF7B0_02BF(mbuf, oidChar)
        
    Case &HF7B002C0&	' Add Fellowship Member
        Call DecodeF7B0_02C0(mbuf, oidChar)
        
    Case &HF7B002C1&	' Add Spell to Spellbook
        Call DecodeF7B0_02C1(mbuf, oidChar)
        
    Case &HF7B002C2&	' Add Character Enchantment
        Call DecodeF7B0_02C2(mbuf, oidChar)
        
    Case &HF7B002C3&	' Remove Character Enchantment
        Call DecodeF7B0_02C3(mbuf, oidChar)
        
    Case &HF7B002C5&	' Remove Multiple Character Enchantments
        Call DecodeF7B0_02C5(mbuf, oidChar)
        
    Case &HF7B002C7&	' Remove Character Enchantment (Silent)
        Call DecodeF7B0_02C7(mbuf, oidChar)
        
    Case &HF7B002C8&	' Remove Multiple Character Enchantments (Silent)
        Call DecodeF7B0_02C8(mbuf, oidChar)
        
    Case &HF7DB0000&	' Update Object
        Call DecodeF7DB(mbuf, oidChar)
        
    Case &HF7DF0000&	' Start 3D Mode
        Call DecodeF7DF(mbuf, oidChar)
        
    Case &HF7E00000&	' Server Message
        Call DecodeF7E0(mbuf, oidChar)
        
    Case &HF7E10000&	' Server Name
        Call DecodeF7E1(mbuf, oidChar)
        
    Case &HF7B10005&	' CTS Update Single Option
        Call DecodeF7B1_0005(mbuf, oidChar)
        
    Case &HF7B101A1&	' CTS Update Options
        Call DecodeF7B1_01A1(mbuf, oidChar)
        
    Case &HF6190000&	'
        Call DecodeF619(mbuf, oidChar)
        
    Case &HF6250000&	'
        Call DecodeF625(mbuf, oidChar)
        
    Case &HF6570000&	'
        Call DecodeF657(mbuf, oidChar)
        
    Case &HF7460000&	'
        Call DecodeF746(mbuf, oidChar)
        
    Case &HF7490000&	'
        Call DecodeF749(mbuf, oidChar)
        
    Case &HF74E0000&	'
        Call DecodeF74E(mbuf, oidChar)
        
    Case &HF7500000&	'
        Call DecodeF750(mbuf, oidChar)
        
    Case &HF7550000&	'
        Call DecodeF755(mbuf, oidChar)
        
    Case &HF7B00020&	'
        Call DecodeF7B0_0020(mbuf, oidChar)
        
    Case &HF7B00052&	'
        Call DecodeF7B0_0052(mbuf, oidChar)
        
    Case &HF7B000A0&	'
        Call DecodeF7B0_00A0(mbuf, oidChar)
        
    Case &HF7B000B4&	'
        Call DecodeF7B0_00B4(mbuf, oidChar)
        
    Case &HF7B000B8&	'
        Call DecodeF7B0_00B8(mbuf, oidChar)
        
    Case &HF7B001F4&	'
        Call DecodeF7B0_01F4(mbuf, oidChar)
        
    Case &HF7B00203&	'
        Call DecodeF7B0_0203(mbuf, oidChar)
        
    Case &HF7B00207&	'
        Call DecodeF7B0_0207(mbuf, oidChar)
        
    Case &HF7B0021D&	'
        Call DecodeF7B0_021D(mbuf, oidChar)
        
    Case &HF7B00257&	'
        Call DecodeF7B0_0257(mbuf, oidChar)
        
    Case &HF7B0027A&	'
        Call DecodeF7B0_027A(mbuf, oidChar)
        
    Case &HE0000000&	' SkunkWorks Command
        Call DecodeE000(mbuf, oidChar)
        
    Case &HE0010000&	' SkunkWorks Control Event
        Call DecodeE001(mbuf, oidChar)
        
    Case &HE0020000&	' SkunkWorks Leave Vendor
        Call DecodeE002(mbuf, oidChar)
        
    Case &HE0030000&	' SkunkWorks Property Value
        Call DecodeE003(mbuf, oidChar)
        
    Case &HE0040000&	' SkunkWorks Plugin Message
        Call DecodeE004(mbuf, oidChar)
        
    Case &HE0050000&	' SkunkWorks Hotkey
        Call DecodeE005(mbuf, oidChar)
        
    Case &HE0060000&	' SkunkWorks Nav Stop
        Call DecodeE006(mbuf, oidChar)
        
    Case &HE0070000&	' SkunkWorks Destroy ACOs
        Call DecodeE007(mbuf, oidChar)
        
    Case &HE0080000&	' SkunkWorks Disconnect
        Call DecodeE008(mbuf, oidChar)
        
    Case &HE0090000&	' SkunkWorks ChatParserIntercept
        Call DecodeE009(mbuf, oidChar)
        
    Case &HE00A0000&	' SkunkWorks StatusTextIntercept
        Call DecodeE00A(mbuf, oidChar)
        
    Case &HE00B0000&	' SkunkWorks Open Container Panel
        Call DecodeE00B(mbuf, oidChar)
        
    Case &HE00C0000&	' SkunkWorks Close Container
        Call DecodeE00C(mbuf, oidChar)
        
    Case &HE00D0000&	' SkunkWorks ChatClickIntercept
        Call DecodeE00D(mbuf, oidChar)
        
    Case Else
    '   Hex-dump unknown/unhandled messages to debug log as required.
        Select Case swoptions.pxm
        Case pxmUnhandled
            Call mbuf.PxMsg(swoptions.fPxWholeMsg)
        Case pxmUnknown
            If mbuf.FUnknownMty(mty) Then Call mbuf.PxMsg(swoptions.fPxWholeMsg)
        End Select
        
    End Select
    
'   Dispatch raw message handlers, if any.
    On Error Resume Next
    Dim llobj As LlobjCls
    Set llobj = llobjRaw
    Do Until llobj Is Nothing
        If llobj.serial = mty Then
            Call mbuf.ResetRead()
            Call llobj.obj.OnRawServerMessage(mty, mbuf)
            If Err.Number <> 0 Then
                Call skapi.RaiseHandlerError(TypeName(llobj.obj) + ".OnRawServerMessage", Err.Description, Err.Number)
                Call Err.Clear
            End If
        End If
        Set llobj = llobj.llobjNext
    Loop
    On Error Goto 0
    
    If fPxThisMsg Then Call mbuf.PxMsg(True)
    
'   Check for orphaned ACOs once every minute or so (but not if we're in portal space,
'   since we might be there longer than the 25-second ACO destruction timer).
    If LSubNoOflow(mbuf.tick, tickDelOrphans) >= 60000 And _
      skapi.plig = pligInWorld Then
        Call otable.DeleteOrphanedAcos()
        tickDelOrphans = mbuf.tick
    End If
    
'   Retry failed ID requests.
    If idq.fRequestOutstanding Then
        If LSubNoOflow(mbuf.tick, idq.tickRequest) >= 2000 Then
            Call idq.Timeout(mbuf.tick)
        End If
    End If
    
    If LSubNoOflow(mbuf.tick, tickMemStats) >= 60000 Then
        Call LogMemStats(mbuf.tick)
    End If
    
'   Return the event ID of the last script event fired.
    EvidDispatchEvent = evidLast
End Function

' Check to see if the script was started after character login, and if so,
' fire a phony OnLogon event.  Required for ACScript compatibility.
Public Function FFirePhonyLogon() As Boolean
    
    If Not (skapi.objScript Is Nothing) And _
      (ctac And ctacCachedLogin) <> 0 And _
      (ctac And ctacInventoryFromCache) = 0 Then
        
    '   Fire script events.
        Call skev.RaiseOnLogon(otable.acoChar)
        
    '   Set ConnectedToAC bits to indicate post-login script start.
        ctac = ctac Or ctacInventoryFromCache Or ctacObjectsFromCache
        FFirePhonyLogon = True
    Else
        FFirePhonyLogon = False
    End If
    
End Function

' Check to see if any timer events are pending, and if so, fire one.
Public Function FFireTimer(ByRef cmsecWait As Long) As Boolean
    
    If timerFirst Is Nothing Then
    '   No timer events scheduled.
        FFireTimer = False
    Else
        Dim cmsec As Long
        cmsec = timerFirst.cmsec
        If cmsec >= 0 Then
        '   Timer has expired; remove it from the list.
            Dim timer As TimerCls
            Set timer = timerFirst
            Call timer.RemoveFromList()
            
        '   Fire an event.
            
        '   Fire script events.
            Call skev.RaiseOnTimer(timer)
            
            FFireTimer = True
        Else
        '   Timer hasn't expired yet.
            If cmsecWait > -cmsec Then cmsecWait = -cmsec
            FFireTimer = False
        End If
    End If
    
End Function

Private Function DecodePosition(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As Object
    Set DecodePosition = Nothing
    
'   field: flags, type: DWORD
    Dim t_flags As Long : t_flags = mbuf.Get_DWORD()
    
'   field: landcell, type: DWORD
    Dim landblock As Long : landblock = mbuf.Get_DWORD()
    
'   field: x, type: float
    Dim x As Single : x = mbuf.Get_float()
    
'   field: y, type: float
    Dim y As Single : y = mbuf.Get_float()
    
'   field: z, type: float
    Dim z As Single : z = mbuf.Get_float()
    
'   maskmap: flags
    If ((t_flags Xor &H00000078&) And &H00000008&) <> 0 Then
        
    '   field: wQuat, type: float
        Dim wQuat As Single : wQuat = mbuf.Get_float()
        
    End If
    If ((t_flags Xor &H00000078&) And &H00000010&) <> 0 Then
        
    '   Skip: xQuat
        Call mbuf.SkipCb(4)
        
    End If
    If ((t_flags Xor &H00000078&) And &H00000020&) <> 0 Then
        
    '   Skip: yQuat
        Call mbuf.SkipCb(4)
        
    End If
    If ((t_flags Xor &H00000078&) And &H00000040&) <> 0 Then
        
    '   field: zQuat, type: float
        Dim zQuat As Single : zQuat = mbuf.Get_float()
        
    End If
'   maskmap: flags
    If (t_flags And &H00000001&) <> 0 Then
        
    '   Skip: dx dy dz
        Call mbuf.SkipCb(12)
        
    End If
    If (t_flags And &H000002&) <> 0 Then
        
    '   Skip: unknown
        Call mbuf.SkipCb(4)
        
    End If
    
    Dim maploc As MaplocCls : Set maploc = New MaplocCls
    Call maploc.FromLandblockXY(landblock, x, y, z, HeadFromWZ(wQuat, zQuat))
    Set DecodePosition = maploc
    
End Function

Private Function DecodePosition0(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As Object
    Set DecodePosition0 = Nothing
    
'   field: landcell, type: DWORD
    Dim landblock As Long : landblock = mbuf.Get_DWORD()
    
'   field: x, type: float
    Dim x As Single : x = mbuf.Get_float()
    
'   field: y, type: float
    Dim y As Single : y = mbuf.Get_float()
    
'   field: z, type: float
    Dim z As Single : z = mbuf.Get_float()
    
'   field: wQuat, type: float
    Dim wQuat As Single : wQuat = mbuf.Get_float()
    
'   Skip: xQuat yQuat
    Call mbuf.SkipCb(8)
    
'   field: zQuat, type: float
    Dim zQuat As Single : zQuat = mbuf.Get_float()
    
    Dim maploc As MaplocCls : Set maploc = New MaplocCls
    Call maploc.FromLandblockXY(landblock, x, y, z, HeadFromWZ(wQuat, zQuat))
    Set DecodePosition0 = maploc
    
End Function

Private Function DecodeModelData(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As Object
    Set DecodeModelData = Nothing
    
'   Skip: eleven
    Call mbuf.SkipCb(1)
    
'   field: paletteCount, type: BYTE
    Dim t_paletteCount As Byte : t_paletteCount = mbuf.Get_BYTE()
    
'   field: textureCount, type: BYTE
    Dim t_textureCount As Byte : t_textureCount = mbuf.Get_BYTE()
    
'   field: modelCount, type: BYTE
    Dim t_modelCount As Byte : t_modelCount = mbuf.Get_BYTE()
    
'   maskmap: paletteCount
    If (t_paletteCount And &HFF&) <> 0 Then
        
    '   Skip: palette, type: PackedDWORD
        Call mbuf.Skip_PackedDWORD()
        
    End If
    
'   vector: palettes, length: t_paletteCount
    Dim t_palettes As Long
    For t_palettes = 0 To t_paletteCount - 1
        
    '   Skip: palette, type: PackedDWORD
        Call mbuf.Skip_PackedDWORD()
        
    '   Skip: offset length
        Call mbuf.SkipCb(2)
        
    Next t_palettes
    
'   vector: textures, length: t_textureCount
    Dim t_textures As Long
    For t_textures = 0 To t_textureCount - 1
        
    '   Skip: index
        Call mbuf.SkipCb(1)
        
    '   Skip: old, type: PackedDWORD
        Call mbuf.Skip_PackedDWORD()
        
    '   Skip: new, type: PackedDWORD
        Call mbuf.Skip_PackedDWORD()
        
    Next t_textures
    
'   vector: models, length: t_modelCount
    Dim t_models As Long
    For t_models = 0 To t_modelCount - 1
        
    '   Skip: index
        Call mbuf.SkipCb(1)
        
    '   Skip: model, type: PackedDWORD
        Call mbuf.Skip_PackedDWORD()
        
    Next t_models
    
    Call mbuf.Align_DWORD()
    
End Function

Private Function DecodePhysicsData(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As PhysicsDataCls
    Set DecodePhysicsData = Nothing
    
    Dim pd As PhysicsDataCls : Set pd = New PhysicsDataCls
    Dim rgbAnim(11) As Byte
    
'   field: flags, type: DWORD
    Dim t_flags As Long : t_flags = mbuf.Get_DWORD()
    
'   Skip: unknown
    Call mbuf.SkipCb(4)
    
'   maskmap: flags
    If (t_flags And &H00010000&) <> 0 Then
        
    '   field: byteCount, type: DWORD
        Dim t_byteCount As Long : t_byteCount = mbuf.Get_DWORD()
        
    '   vector: bytes, length: t_byteCount
        Dim ibAnim As Long
        For ibAnim = 0 To t_byteCount - 1
            
        '   field: byte, type: BYTE
            Dim bAnim As Byte : bAnim = mbuf.Get_BYTE()
            If ibAnim <= UBound(rgbAnim) Then
                rgbAnim(ibAnim) = bAnim
            End If
            
        Next ibAnim
        
    '   Skip: unknown10000
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags And &H00020000&) <> 0 Then
        
    '   Skip: unknown20000
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags And &H00008000&) <> 0 Then
        
    '   field: position, type: Position0
        Dim maploc As Object
        Set maploc = DecodePosition0(mbuf, oidChar, 0)
        Set pd.maploc = maploc
        
    End If
    If (t_flags And &H00000002&) <> 0 Then
        
    '   Skip: animations
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags And &H00000800&) <> 0 Then
        
    '   Skip: sounds
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags And &H00001000&) <> 0 Then
        
    '   Skip: unknown1000
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags And &H00000001&) <> 0 Then
        
    '   Skip: model
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags And &H00000020&) <> 0 Then
        
    '   Skip: equipper equipperSlot
        Call mbuf.SkipCb(8)
        
    End If
    If (t_flags And &H00000040&) <> 0 Then
        
    '   field: equippedCount, type: DWORD
        Dim t_equippedCount As Long : t_equippedCount = mbuf.Get_DWORD()
        
    '   Skip: equipped, length: t_equippedCount
        Call mbuf.SkipCb(t_equippedCount * 8)
        
    End If
    If (t_flags And &H00000080&) <> 0 Then
        
    '   Skip: scale
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags And &H00000100&) <> 0 Then
        
    '   Skip: unknown100
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags And &H00000200&) <> 0 Then
        
    '   Skip: unknown200
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags And &H00040000&) <> 0 Then
        
    '   Skip: unknown40000
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags And &H00000004&) <> 0 Then
        
    '   Skip: dx dy dz
        Call mbuf.SkipCb(12)
        
    End If
    If (t_flags And &H00000008&) <> 0 Then
        
    '   Skip: unknown8_1 unknown8_2 unknown8_3
        Call mbuf.SkipCb(12)
        
    End If
    If (t_flags And &H00000010&) <> 0 Then
        
    '   Skip: rx ry rz
        Call mbuf.SkipCb(12)
        
    End If
    If (t_flags And &H00002000&) <> 0 Then
        
    '   Skip: unknown2000
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags And &H00004000&) <> 0 Then
        
    '   Skip: unknown4000
        Call mbuf.SkipCb(4)
        
    End If
    
'   Skip: unknown1 unknown2 unknown3 unknown4 unknown5 unknown6 unknown7 unknown8 unknown9
    Call mbuf.SkipCb(18)
    
    Call mbuf.Align_DWORD()
    
    If rgbAnim(0) = 0 And _
      rgbAnim(2) = &H3D And _
      rgbAnim(4) = 2 Then
        If rgbAnim(8) = &H0B Then
            pd.fOpenable = True
            pd.fOpen = True
        ElseIf rgbAnim(8) = &H0C Then
            pd.fOpenable = True
            pd.fOpen = False
        End If
    End If
    
    Set DecodePhysicsData = pd
    
End Function

Private Function DecodeGameData(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As GameDataCls
    Set DecodeGameData = Nothing
    
    Dim gd As GameDataCls : Set gd = New GameDataCls
    
'   field: flags1, type: DWORD
    Dim t_flags1 As Long : t_flags1 = mbuf.Get_DWORD()
    
'   field: name, type: String
    Dim szName As String : szName = mbuf.Get_String()
    gd.szName = szName
    
'   field: type, type: PackedDWORD
    Dim model As Long : model = mbuf.Get_PackedDWORD()
    gd.model = model
    
'   field: icon, type: PackedDWORD
    Dim icon As Long : icon = mbuf.Get_PackedDWORD()
    gd.icon = icon
    
'   field: category, type: DWORD
    Dim mcm As Long : mcm = mbuf.Get_DWORD()
    gd.mcm = mcm
    
'   field: behavior, type: DWORD
    Dim oty As Long : oty = mbuf.Get_DWORD()
    gd.oty = oty
    
    Call mbuf.Align_DWORD()
    
'   maskmap: behavior
    If (oty And &H04000000&) <> 0 Then
        
    '   Skip: flags2
        Call mbuf.SkipCb(4)
        
    End If
'   maskmap: flags1
    If (t_flags1 And &H00000001&) <> 0 Then
        
    '   field: namePlural, type: String
        Dim szPlural As String : szPlural = mbuf.Get_String()
        gd.szPlural = szPlural
        
    End If
    If (t_flags1 And &H00000002&) <> 0 Then
        
    '   field: itemSlots, type: BYTE
        Dim citemMax As Byte : citemMax = mbuf.Get_BYTE()
        gd.citemMax = citemMax
        
    End If
    If (t_flags1 And &H00000004&) <> 0 Then
        
    '   field: packSlots, type: BYTE
        Dim cpackMax As Byte : cpackMax = mbuf.Get_BYTE()
        gd.cpackMax = cpackMax
        
    End If
    If (t_flags1 And &H00000100&) <> 0 Then
        
    '   Skip: ammunition
        Call mbuf.SkipCb(2)
        
    End If
    If (t_flags1 And &H00000008&) <> 0 Then
        
    '   field: value, type: DWORD
        Dim cpyValue As Long : cpyValue = mbuf.Get_DWORD()
        gd.cpyValue = cpyValue
        
    End If
    If (t_flags1 And &H00000010&) <> 0 Then
        
    '   Skip: unknown10
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags1 And &H00000020&) <> 0 Then
        
    '   field: approachDistance, type: float
        Dim distApproach As Single : distApproach = mbuf.Get_float()
        gd.distApproach = distApproach
        
    End If
    If (t_flags1 And &H00080000&) <> 0 Then
        
    '   Skip: usableOn
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags1 And &H00000080&) <> 0 Then
        
    '   field: iconHighlight, type: DWORD
        Dim ioc As Long : ioc = mbuf.Get_DWORD()
        gd.ioc = ioc
        
    End If
    If (t_flags1 And &H00000200&) <> 0 Then
        
    '   Skip: wieldType
        Call mbuf.SkipCb(1)
        
    End If
    If (t_flags1 And &H00000400&) <> 0 Then
        
    '   field: uses, type: WORD
        Dim cuseLeft As Long : cuseLeft = mbuf.Get_WORD()
        gd.cuseLeft = cuseLeft
        
    End If
    If (t_flags1 And &H00000800&) <> 0 Then
        
    '   field: usesLimit, type: WORD
        Dim cuseMax As Long : cuseMax = mbuf.Get_WORD()
        gd.cuseMax = cuseMax
        
    End If
    If (t_flags1 And &H00001000&) <> 0 Then
        
    '   field: stack, type: WORD
        Dim citemStack As Long : citemStack = mbuf.Get_WORD()
        gd.citemStack = citemStack
        
    End If
    If (t_flags1 And &H00002000&) <> 0 Then
        
    '   field: stackLimit, type: WORD
        Dim citemMaxStack As Long : citemMaxStack = mbuf.Get_WORD()
        gd.citemMaxStack = citemMaxStack
        
    End If
    If (t_flags1 And &H00004000&) <> 0 Then
        
    '   field: container, type: DWORD
        Dim oidContainer As Long : oidContainer = mbuf.Get_DWORD()
        Dim acoContainer As AcoCls : Set acoContainer = otable.AcoFromOid(oidContainer, True, True)
        Set gd.acoContainer = acoContainer
        
    End If
    If (t_flags1 And &H00008000&) <> 0 Then
        
    '   field: equipper, type: DWORD
        Dim oidWearer As Long : oidWearer = mbuf.Get_DWORD()
        Dim acoWearer As AcoCls : Set acoWearer = otable.AcoFromOid(oidWearer, True, True)
        Set gd.acoWearer = acoWearer
        
    End If
    If (t_flags1 And &H00010000&) <> 0 Then
        
    '   field: equipPossible, type: DWORD
        Dim eqm As Long : eqm = mbuf.Get_DWORD()
        gd.eqm = eqm
        
    End If
    If (t_flags1 And &H00020000&) <> 0 Then
        
    '   field: equipActual, type: DWORD
        Dim eqmWearer As Long : eqmWearer = mbuf.Get_DWORD()
        gd.eqmWearer = eqmWearer
        
    End If
    If (t_flags1 And &H00040000&) <> 0 Then
        
    '   Skip: coverage
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags1 And &H00100000&) <> 0 Then
        
    '   Skip: unknown100000
        Call mbuf.SkipCb(1)
        
    End If
    If (t_flags1 And &H00800000&) <> 0 Then
        
    '   Skip: unknown800000
        Call mbuf.SkipCb(1)
        
    End If
    If (t_flags1 And &H08000000&) <> 0 Then
        
    '   Skip: unknown8000000
        Call mbuf.SkipCb(2)
        
    End If
    If (t_flags1 And &H01000000&) <> 0 Then
        
    '   field: workmanship, type: float
        Dim workmanship As Single : workmanship = mbuf.Get_float()
        gd.workmanship = workmanship
        
    End If
    If (t_flags1 And &H00200000&) <> 0 Then
        
    '   field: burden, type: WORD
        Dim burden As Long : burden = mbuf.Get_WORD()
        gd.burden = burden
        
    End If
    If (t_flags1 And &H00400000&) <> 0 Then
        
    '   field: spell, type: WORD
        Dim spellid As Long : spellid = mbuf.Get_WORD()
        gd.spellid = spellid
        
    End If
    If (t_flags1 And &H02000000&) <> 0 Then
        
    '   Skip: owner
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags1 And &H04000000&) <> 0 Then
        
    '   field: acl, type: DwellingACL
        Dim t_acl As Object
        Set t_acl = DecodeDwellingACL(mbuf, oidChar, 0)
        
    End If
    If (t_flags1 And &H20000000&) <> 0 Then
        
    '   Skip: hookTypeUnknown hookType
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags1 And &H00000040&) <> 0 Then
        
    '   field: monarch, type: DWORD
        Dim oidMonarch As Long : oidMonarch = mbuf.Get_DWORD()
        gd.oidMonarch = oidMonarch
        
    End If
    If (t_flags1 And &H10000000&) <> 0 Then
        
    '   Skip: hookableOn
        Call mbuf.SkipCb(2)
        
    End If
    If (t_flags1 And &H40000000&) <> 0 Then
        
    '   field: iconOverlay, type: PackedDWORD
        Dim iconOverlay As Long : iconOverlay = mbuf.Get_PackedDWORD()
        gd.iconOverlay = iconOverlay
        
    End If
'   maskmap: behavior
    If (oty And &H04000000&) <> 0 Then
        
    '   field: iconUnderlay, type: PackedDWORD
        Dim iconUnderlay As Long : iconUnderlay = mbuf.Get_PackedDWORD()
        gd.iconUnderlay = iconUnderlay
        
    End If
'   maskmap: flags1
    If (t_flags1 And &H80000000&) <> 0 Then
        
    '   field: material, type: DWORD
        Dim material As Long : material = mbuf.Get_DWORD()
        gd.material = material
        
    End If
    
    Call mbuf.Align_DWORD()
    
    Set DecodeGameData = gd
    
End Function

Private Function DecodeAttributeData(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As AVDataCls
    Set DecodeAttributeData = Nothing
    
    Dim avd As AVDataCls : Set avd = New AVDataCls
    
'   field: raised, type: DWORD
    Dim dlvl As Long : dlvl = mbuf.Get_DWORD()
    avd.dlvl = dlvl
    
'   field: innate, type: DWORD
    Dim lvlOrig As Long : lvlOrig = mbuf.Get_DWORD()
    avd.lvlOrig = lvlOrig
    
'   field: xp, type: DWORD
    Dim xp As Long : xp = mbuf.Get_DWORD()
'   XP values can exceed 7FFFFFF.  Convert to unsigned double.
    avd.expInvested = UDblFromDw(xp)
    
    Set DecodeAttributeData = avd
    
End Function

Private Function DecodeVitalData(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As AVDataCls
    Set DecodeVitalData = Nothing
    
    Dim avd As AVDataCls : Set avd = New AVDataCls
    
'   field: raised, type: DWORD
    Dim dlvl As Long : dlvl = mbuf.Get_DWORD()
    avd.dlvl = dlvl
    
'   Skip: unknown
    Call mbuf.SkipCb(4)
    
'   field: xp, type: DWORD
    Dim xp As Long : xp = mbuf.Get_DWORD()
'   XP values can exceed 7FFFFFF.  Convert to unsigned double.
    avd.expInvested = UDblFromDw(xp)
    
'   field: current, type: DWORD
    Dim lvlCur As Long : lvlCur = mbuf.Get_DWORD()
    avd.lvlCur = lvlCur
    
    Set DecodeVitalData = avd
    
End Function

Private Function DecodeSkillData(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As SkillDataCls
    Set DecodeSkillData = Nothing
    
    Dim sd As SkillDataCls : Set sd = New SkillDataCls
    
'   field: raised, type: WORD
    Dim dlvl As Long : dlvl = mbuf.Get_WORD()
    sd.dlvl = dlvl
    
'   Skip: unknown1
    Call mbuf.SkipCb(2)
    
'   field: state, type: DWORD
    Dim skts As Long : skts = mbuf.Get_DWORD()
    sd.skts = skts
    
'   field: xp, type: DWORD
    Dim xp As Long : xp = mbuf.Get_DWORD()
'   XP values can exceed 7FFFFFF.  Convert to unsigned double.
    sd.expInvested = UDblFromDw(xp)
    
'   field: bonus, type: DWORD
    Dim dlvlFree As Long : dlvlFree = mbuf.Get_DWORD()
    sd.dlvlFree = dlvlFree
    
'   field: diff, type: DWORD
    Dim diff As Long : diff = mbuf.Get_DWORD()
    sd.diff = diff
    
'   Skip: unknown2
    Call mbuf.SkipCb(8)
    
    Set DecodeSkillData = sd
    
End Function

Private Function DecodeCharacterPropertyData(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As Object
    Set DecodeCharacterPropertyData = Nothing
    
'   field: flags, type: DWORD
    Dim t_flags As Long : t_flags = mbuf.Get_DWORD()
    
'   Skip: unknown1
    Call mbuf.SkipCb(4)
    
'   maskmap: flags
    If (t_flags And &H00000001&) <> 0 Then
        
    '   field: dwordCount, type: WORD
        Dim t_dwordCount As Long : t_dwordCount = mbuf.Get_WORD()
        
    '   Skip: dwordUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: dwords, length: t_dwordCount
        Dim idw As Long
        For idw = 0 To t_dwordCount - 1
            
        '   field: key, type: DWORD
            Dim lkey As Long : lkey = mbuf.Get_DWORD()
            
        '   field: value, type: DWORD
            Dim dwVal As Long : dwVal = mbuf.Get_DWORD()
            
            Call self.SetDwordProp(lkey, dwVal)
            
        Next idw
        
    End If
    If (t_flags And &H00000080&) <> 0 Then
        
    '   field: qwordCount, type: WORD
        Dim t_qwordCount As Long : t_qwordCount = mbuf.Get_WORD()
        
    '   Skip: qwordUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: qwords, length: t_qwordCount
        Dim iqw As Long
        For iqw = 0 To t_qwordCount - 1
            
        '   field: key, type: DWORD
            Dim qkey As Long : qkey = mbuf.Get_DWORD()
            
        '   field: value, type: QWORD
            Dim qVal As Double : qVal = mbuf.Get_QWORD()
            
            Call self.SetQwordProp(qkey, qVal)
            
        Next iqw
        
    End If
    If (t_flags And &H00000002&) <> 0 Then
        
    '   field: booleanCount, type: WORD
        Dim t_booleanCount As Long : t_booleanCount = mbuf.Get_WORD()
        
    '   Skip: booleanUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: booleans, length: t_booleanCount
        Dim ibool As Long
        For ibool = 0 To t_booleanCount - 1
            
        '   field: key, type: DWORD
            Dim bkey As Long : bkey = mbuf.Get_DWORD()
            
        '   field: value, type: DWORD
            Dim fVal As Long : fVal = mbuf.Get_DWORD()
            
            Call self.SetBoolProp(bkey, fVal)
            
        Next ibool
        
    End If
    If (t_flags And &H00000004&) <> 0 Then
        
    '   field: doubleCount, type: WORD
        Dim t_doubleCount As Long : t_doubleCount = mbuf.Get_WORD()
        
    '   Skip: doubleUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: doubles, length: t_doubleCount
        Dim idbl As Long
        For idbl = 0 To t_doubleCount - 1
            
        '   field: key, type: DWORD
            Dim dkey As Long : dkey = mbuf.Get_DWORD()
            
        '   field: value, type: double
            Dim dblVal As Double : dblVal = mbuf.Get_double()
            
            Call self.SetDblProp(dkey, dblVal)
            
        Next idbl
        
    End If
    If (t_flags And &H00000010&) <> 0 Then
        
    '   field: stringCount, type: WORD
        Dim t_stringCount As Long : t_stringCount = mbuf.Get_WORD()
        
    '   Skip: stringUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: strings, length: t_stringCount
        Dim isz As Long
        For isz = 0 To t_stringCount - 1
            
        '   field: key, type: DWORD
            Dim skey As Long : skey = mbuf.Get_DWORD()
            
        '   field: value, type: String
            Dim szVal As String : szVal = mbuf.Get_String()
            
            Call self.SetSzProp(skey, szVal)
            
        Next isz
        
    End If
    If (t_flags And &H00000040&) <> 0 Then
        
    '   field: resourceCount, type: WORD
        Dim t_resourceCount As Long : t_resourceCount = mbuf.Get_WORD()
        
    '   Skip: resourceUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: resources, length: t_resourceCount
        Dim ires As Long
        For ires = 0 To t_resourceCount - 1
            
        '   field: key, type: DWORD
            Dim rkey As Long : rkey = mbuf.Get_DWORD()
            
        '   field: value, type: DWORD
            Dim rVal As Long : rVal = mbuf.Get_DWORD()
            
        Next ires
        
    End If
    If (t_flags And &H00000008&) <> 0 Then
        
    '   field: linkCount, type: WORD
        Dim t_linkCount As Long : t_linkCount = mbuf.Get_WORD()
        
    '   Skip: linkUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: links, length: t_linkCount
        Dim ilink As Long
        For ilink = 0 To t_linkCount - 1
            
        '   field: key, type: DWORD
            Dim akey As Long : akey = mbuf.Get_DWORD()
            
        '   field: value, type: DWORD
            Dim oidVal As Long : oidVal = mbuf.Get_DWORD()
            
        Next ilink
        
    End If
    If (t_flags And &H00000020&) <> 0 Then
        
    '   field: positionCount, type: WORD
        Dim t_positionCount As Long : t_positionCount = mbuf.Get_WORD()
        
    '   Skip: positionUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: positions, length: t_positionCount
        Dim ipos As Long
        For ipos = 0 To t_positionCount - 1
            
        '   field: key, type: DWORD
            Dim pkey As Long : pkey = mbuf.Get_DWORD()
            
        '   field: value, type: Position0
            Dim maplocVal As Object
            Set maplocVal = DecodePosition0(mbuf, oidChar, 0)
            
        Next ipos
        
    End If
    
End Function

Private Function DecodeCharacterVectorData(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As Object
    Set DecodeCharacterVectorData = Nothing
    
'   field: flags, type: DWORD
    Dim t_flags As Long : t_flags = mbuf.Get_DWORD()
    
'   Skip: unknown2
    Call mbuf.SkipCb(4)
    
'   maskmap: flags
    If (t_flags And &H00000001&) <> 0 Then
        
    '   field: attributeFlags, type: DWORD
        Dim t_attributeFlags As Long : t_attributeFlags = mbuf.Get_DWORD()
        
    '   maskmap: attributeFlags
        If (t_attributeFlags And &H00000001&) <> 0 Then
            
        '   field: strength, type: AttributeData
            Dim avdStrength As AVDataCls
            Set avdStrength = DecodeAttributeData(mbuf, oidChar, 0)
            Call self.SetAtinfo(attrStrength, avdStrength)
            
        End If
        If (t_attributeFlags And &H00000002&) <> 0 Then
            
        '   field: endurance, type: AttributeData
            Dim avdEndurance As AVDataCls
            Set avdEndurance = DecodeAttributeData(mbuf, oidChar, 0)
            Call self.SetAtinfo(attrEndurance, avdEndurance)
            
        End If
        If (t_attributeFlags And &H00000004&) <> 0 Then
            
        '   field: quickness, type: AttributeData
            Dim avdQuickness As AVDataCls
            Set avdQuickness = DecodeAttributeData(mbuf, oidChar, 0)
            Call self.SetAtinfo(attrQuickness, avdQuickness)
            
        End If
        If (t_attributeFlags And &H00000008&) <> 0 Then
            
        '   field: coordination, type: AttributeData
            Dim avdCoordination As AVDataCls
            Set avdCoordination = DecodeAttributeData(mbuf, oidChar, 0)
            Call self.SetAtinfo(attrCoordination, avdCoordination)
            
        End If
        If (t_attributeFlags And &H00000010&) <> 0 Then
            
        '   field: focus, type: AttributeData
            Dim avdFocus As AVDataCls
            Set avdFocus = DecodeAttributeData(mbuf, oidChar, 0)
            Call self.SetAtinfo(attrFocus, avdFocus)
            
        End If
        If (t_attributeFlags And &H00000020&) <> 0 Then
            
        '   field: self, type: AttributeData
            Dim avdSelf As AVDataCls
            Set avdSelf = DecodeAttributeData(mbuf, oidChar, 0)
            Call self.SetAtinfo(attrSelf, avdSelf)
            
        End If
        If (t_attributeFlags And &H00000040&) <> 0 Then
            
        '   field: health, type: VitalData
            Dim avdHealth As AVDataCls
            Set avdHealth = DecodeVitalData(mbuf, oidChar, 0)
            Call self.SetVtinfo(vitalHealthMax, avdHealth)
            Call self.SetHealth(avdHealth.lvlCur)
            
        End If
        If (t_attributeFlags And &H00000080&) <> 0 Then
            
        '   field: stamina, type: VitalData
            Dim avdStamina As AVDataCls
            Set avdStamina = DecodeVitalData(mbuf, oidChar, 0)
            Call self.SetVtinfo(vitalStaminaMax, avdStamina)
            Call self.SetStamina(avdStamina.lvlCur)
            
        End If
        If (t_attributeFlags And &H00000100&) <> 0 Then
            
        '   field: mana, type: VitalData
            Dim avdMana As AVDataCls
            Set avdMana = DecodeVitalData(mbuf, oidChar, 0)
            Call self.SetVtinfo(vitalManaMax, avdMana)
            Call self.SetMana(avdMana.lvlCur)
            
        End If
    End If
    If (t_flags And &H00000002&) <> 0 Then
        
    '   field: skillCount, type: WORD
        Dim t_skillCount As Long : t_skillCount = mbuf.Get_WORD()
        
    '   Skip: skillUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: skills, length: t_skillCount
        Dim skills As Long
        For skills = 0 To t_skillCount - 1
            
        '   field: key, type: DWORD
            Dim skid As Long : skid = mbuf.Get_DWORD()
            
        '   field: value, type: SkillData
            Dim sd As SkillDataCls
            Set sd = DecodeSkillData(mbuf, oidChar, 0)
            Call self.SetSkinfo(skid, sd)
            
        Next skills
        
    End If
    If (t_flags And &H00000100&) <> 0 Then
        
    '   field: spellbookCount, type: WORD
        Dim t_spellbookCount As Long : t_spellbookCount = mbuf.Get_WORD()
        
    '   Skip: spellbookUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: spellbook, length: t_spellbookCount
        Dim ispellidSpellbook As Long
        For ispellidSpellbook = 0 To t_spellbookCount - 1
            
        '   field: spell, type: DWORD
            Dim spellidSpellbook As Long : spellidSpellbook = mbuf.Get_DWORD()
            Call self.AddSpellid(-1, spellidSpellbook)
            
        '   Skip: charge
            Call mbuf.SkipCb(4)
            
        Next ispellidSpellbook
        
    End If
    If (t_flags And &H00000200&) <> 0 Then
        
    '   field: enchantmentMask, type: DWORD
        Dim t_enchantmentMask As Long : t_enchantmentMask = mbuf.Get_DWORD()
        
    '   maskmap: enchantmentMask
        If (t_enchantmentMask And &H0001&) <> 0 Then
            
        '   field: lifeSpellCount, type: DWORD
            Dim t_lifeSpellCount As Long : t_lifeSpellCount = mbuf.Get_DWORD()
            
        '   vector: lifeSpells, length: t_lifeSpellCount
            Dim ispellLife As Long
            For ispellLife = 0 To t_lifeSpellCount - 1
                
            '   field: enchantment, type: Enchantment
                Dim spellLife As SpellCls
                Set spellLife = DecodeEnchantment(mbuf, oidChar, 0)
                Call cospell.AddSpell(spellLife)
                
            Next ispellLife
            
        End If
        If (t_enchantmentMask And &H0002&) <> 0 Then
            
        '   field: creatureSpellCount, type: DWORD
            Dim t_creatureSpellCount As Long : t_creatureSpellCount = mbuf.Get_DWORD()
            
        '   vector: creatureSpells, length: t_creatureSpellCount
            Dim ispellCreature As Long
            For ispellCreature = 0 To t_creatureSpellCount - 1
                
            '   field: enchantment, type: Enchantment
                Dim spellCreature As SpellCls
                Set spellCreature = DecodeEnchantment(mbuf, oidChar, 0)
                Call cospell.AddSpell(spellCreature)
                
            Next ispellCreature
            
        End If
        If (t_enchantmentMask And &H0004&) <> 0 Then
            
        '   field: vitae, type: Enchantment
            Dim spellVitae As SpellCls
            Set spellVitae = DecodeEnchantment(mbuf, oidChar, 0)
            Call cospell.AddSpell(spellVitae)
            
        End If
    End If
    
End Function

Private Function DecodeCharacterOptionData(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As Object
    Set DecodeCharacterOptionData = Nothing
    
'   field: flags, type: DWORD
    Dim t_flags As Long : t_flags = mbuf.Get_DWORD()
    
'   field: options, type: DWORD
    Dim chop As Long : chop = mbuf.Get_DWORD()
    Call self.SetChop(chop)
    
'   maskmap: flags
    If (t_flags And &H00000001&) <> 0 Then
        
    '   field: shortcutCount, type: DWORD
        Dim t_shortcutCount As Long : t_shortcutCount = mbuf.Get_DWORD()
        
    '   Skip: shortcuts, length: t_shortcutCount
        Call mbuf.SkipCb(t_shortcutCount * 12)
        
    End If
    
'   field: tab1Count, type: DWORD
    Dim t_tab1Count As Long : t_tab1Count = mbuf.Get_DWORD()
    
'   vector: tab1, length: t_tab1Count
    Dim ispell1 As Long
    For ispell1 = 0 To t_tab1Count - 1
        
    '   field: spell, type: DWORD
        Dim spellid1 As Long : spellid1 = mbuf.Get_DWORD()
        Call self.AddSpellid(0, spellid1)
        
    Next ispell1
    
'   maskmap: flags
    If (t_flags And &H00000010&) <> 0 Then
        
    '   field: tab2Count, type: DWORD
        Dim t_tab2Count As Long : t_tab2Count = mbuf.Get_DWORD()
        
    '   vector: tab2, length: t_tab2Count
        Dim ispell2 As Long
        For ispell2 = 0 To t_tab2Count - 1
            
        '   field: spell, type: DWORD
            Dim spellid2 As Long : spellid2 = mbuf.Get_DWORD()
            Call self.AddSpellid(1, spellid2)
            
        Next ispell2
        
    '   field: tab3Count, type: DWORD
        Dim t_tab3Count As Long : t_tab3Count = mbuf.Get_DWORD()
        
    '   vector: tab3, length: t_tab3Count
        Dim ispell3 As Long
        For ispell3 = 0 To t_tab3Count - 1
            
        '   field: spell, type: DWORD
            Dim spellid3 As Long : spellid3 = mbuf.Get_DWORD()
            Call self.AddSpellid(2, spellid3)
            
        Next ispell3
        
    '   field: tab4Count, type: DWORD
        Dim t_tab4Count As Long : t_tab4Count = mbuf.Get_DWORD()
        
    '   vector: tab4, length: t_tab4Count
        Dim ispell4 As Long
        For ispell4 = 0 To t_tab4Count - 1
            
        '   field: spell, type: DWORD
            Dim spellid4 As Long : spellid4 = mbuf.Get_DWORD()
            Call self.AddSpellid(3, spellid4)
            
        Next ispell4
        
    '   field: tab5Count, type: DWORD
        Dim t_tab5Count As Long : t_tab5Count = mbuf.Get_DWORD()
        
    '   vector: tab5, length: t_tab5Count
        Dim ispell5 As Long
        For ispell5 = 0 To t_tab5Count - 1
            
        '   field: spell, type: DWORD
            Dim spellid5 As Long : spellid5 = mbuf.Get_DWORD()
            Call self.AddSpellid(4, spellid5)
            
        Next ispell5
        
    '   field: tab6Count, type: DWORD
        Dim t_tab6Count As Long : t_tab6Count = mbuf.Get_DWORD()
        
    '   vector: tab6, length: t_tab6Count
        Dim ispell6 As Long
        For ispell6 = 0 To t_tab6Count - 1
            
        '   field: spell, type: DWORD
            Dim spellid6 As Long : spellid6 = mbuf.Get_DWORD()
            Call self.AddSpellid(5, spellid6)
            
        Next ispell6
        
    '   field: tab7Count, type: DWORD
        Dim t_tab7Count As Long : t_tab7Count = mbuf.Get_DWORD()
        
    '   vector: tab7, length: t_tab7Count
        Dim ispell7 As Long
        For ispell7 = 0 To t_tab7Count - 1
            
        '   field: spell, type: DWORD
            Dim spellid7 As Long : spellid7 = mbuf.Get_DWORD()
            Call self.AddSpellid(6, spellid7)
            
        Next ispell7
        
    End If
    If (t_flags And &H00000008&) <> 0 Then
        
    '   field: fillcompsCount, type: WORD
        Dim t_fillcompsCount As Long : t_fillcompsCount = mbuf.Get_WORD()
        
    '   Skip: fillcompsUnknown
        Call mbuf.SkipCb(2)
        
    '   Skip: fillcomps, length: t_fillcompsCount
        Call mbuf.SkipCb(t_fillcompsCount * 8)
        
    End If
    If (t_flags And &H00000020&) <> 0 Then
        
    '   Skip: unk20mask3
        Call mbuf.SkipCb(4)
        
    End If
    If (t_flags And &H00000040&) <> 0 Then
        
    '   field: optionFlags, type: DWORD
        Dim chop2 As Long : chop2 = mbuf.Get_DWORD()
        Call self.SetChop2(chop2)
        
    End If
    If (t_flags And &H00000100&) <> 0 Then
        
    '   Skip: unknown100_1
        Call mbuf.SkipCb(4)
        
    '   field: optionStringCount, type: WORD
        Dim t_optionStringCount As Long : t_optionStringCount = mbuf.Get_WORD()
        
    '   Skip: optionStringUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: optionStrings, length: t_optionStringCount
        Dim t_optionStrings As Long
        For t_optionStrings = 0 To t_optionStringCount - 1
            
        '   Skip: key
            Call mbuf.SkipCb(4)
            
        '   Skip: value, type: String
            Call mbuf.Skip_String()
            
        Next t_optionStrings
        
    End If
    If (t_flags And &H00000200&) <> 0 Then
        
    '   Skip: unknown200_1 unknown200_2
        Call mbuf.SkipCb(5)
        
    '   field: optionPropertyCount, type: BYTE
        Dim t_optionPropertyCount As Byte : t_optionPropertyCount = mbuf.Get_BYTE()
        
    '   vector: optionProperties, length: t_optionPropertyCount
        Dim t_optionProperties As Long
        For t_optionProperties = 0 To t_optionPropertyCount - 1
            
        '   field: type, type: DWORD
            Dim t_flags_0x00000200_optionProperties_type As Long : t_flags_0x00000200_optionProperties_type = mbuf.Get_DWORD()
            
        '   switch: type
            Select Case t_flags_0x00000200_optionProperties_type
            Case &H1000008c&
                
            '   Skip: unknown
                Call mbuf.SkipCb(4)
                
            '   field: windowCount, type: DWORD
                Dim t_windowCount As Long : t_windowCount = mbuf.Get_DWORD()
                
            '   vector: windows, length: t_windowCount
                Dim t_windows As Long
                For t_windows = 0 To t_windowCount - 1
                    
                '   field: type, type: DWORD
                    Dim t_flags_0x00000200_optionProperties_type_0x1000008c_windows_type As Long : t_flags_0x00000200_optionProperties_type_0x1000008c_windows_type = mbuf.Get_DWORD()
                    
                '   switch: type
                    Select Case t_flags_0x00000200_optionProperties_type_0x1000008c_windows_type
                    Case &H1000008b&
                        
                    '   Skip: unknown
                        Call mbuf.SkipCb(1)
                        
                    '   field: propertyCount, type: BYTE
                        Dim t_propertyCount As Byte : t_propertyCount = mbuf.Get_BYTE()
                        
                    '   vector: properties, length: t_propertyCount
                        Dim t_properties As Long
                        For t_properties = 0 To t_propertyCount - 1
                            
                        '   field: key, type: DWORD
                            Dim t_key As Long : t_key = mbuf.Get_DWORD()
                            
                        '   switch: key
                            Select Case t_key
                            Case &H1000008d&
                                
                            '   Skip: unknown
                                Call mbuf.SkipCb(4)
                                
                            '   field: titleSource, type: BYTE
                                Dim t_titleSource As Byte : t_titleSource = mbuf.Get_BYTE()
                                
                            '   switch: titleSource
                                Select Case t_titleSource
                                Case &H00&
                                    
                                '   Skip: stringID fileID
                                    Call mbuf.SkipCb(8)
                                    
                                Case &H01&
                                    
                                '   Skip: value, type: WString
                                    Call mbuf.Skip_WString()
                                    
                                End Select
                                
                            '   Skip: unknown_1b unknown_1c
                                Call mbuf.SkipCb(6)
                                
                            Case &H1000008a&
                                
                            '   Skip: unknown value
                                Call mbuf.SkipCb(5)
                                
                            Case &H10000089&
                                
                            '   Skip: unknown value
                                Call mbuf.SkipCb(8)
                                
                            Case &H10000088&
                                
                            '   Skip: unknown value
                                Call mbuf.SkipCb(8)
                                
                            Case &H10000087&
                                
                            '   Skip: unknown value
                                Call mbuf.SkipCb(8)
                                
                            Case &H10000086&
                                
                            '   Skip: unknown value
                                Call mbuf.SkipCb(8)
                                
                            Case &H1000007F&
                                
                            '   Skip: unknown value
                                Call mbuf.SkipCb(12)
                                
                            End Select
                        Next t_properties
                        
                    End Select
                Next t_windows
                
            Case &H10000081&
                
            '   Skip: unknown activeOpacity
                Call mbuf.SkipCb(8)
                
            Case &H10000080&
                
            '   Skip: unknown inactiveOpacity
                Call mbuf.SkipCb(8)
                
            End Select
        Next t_optionProperties
        
        Call mbuf.Align_DWORD()
        
    End If
    
End Function

Private Function DecodeEnchantment(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As SpellCls
    Set DecodeEnchantment = Nothing
    
    Dim spell As SpellCls : Set spell = New SpellCls
    
'   field: spell, type: WORD
    Dim spellid As Long : spellid = mbuf.Get_WORD()
    
'   field: layer, type: WORD
    Dim layer As Long : layer = mbuf.Get_WORD()
    
'   field: family, type: WORD
    Dim family As Long : family = mbuf.Get_WORD()
    
'   Skip: unknown0
    Call mbuf.SkipCb(2)
    
'   field: difficulty, type: DWORD
    Dim diff As Long : diff = mbuf.Get_DWORD()
    
'   field: elapsedTime, type: double
    Dim csecElapsed As Double : csecElapsed = mbuf.Get_double()
    
'   field: duration, type: double
    Dim csecDuration As Double : csecDuration = mbuf.Get_double()
    
'   field: caster, type: DWORD
    Dim oidCaster As Long : oidCaster = mbuf.Get_DWORD()
    Dim acoCaster As AcoCls : Set acoCaster = otable.AcoFromOid(oidCaster, True, True)
    
'   Skip: unknown1 unknown2 startTime
    Call mbuf.SkipCb(16)
    
'   field: flags, type: DWORD
    Dim flags As Long : flags = mbuf.Get_DWORD()
    
'   field: key, type: DWORD
    Dim avsAffected As Long : avsAffected = mbuf.Get_DWORD()
    
'   field: value, type: float
    Dim dlvl As Single : dlvl = mbuf.Get_float()
    
'   Skip: unknown3
    Call mbuf.SkipCb(4)
    
    Call spell.FromEnchantment(spellid, layer, family, diff, flags, _
      avsAffected, dlvl, acoCaster, csecDuration, -csecElapsed)
    Set DecodeEnchantment = spell
    
End Function

Private Function DecodeFellowInfo(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As FellowCls
    Set DecodeFellowInfo = Nothing
    
    Dim fellow As FellowCls : Set fellow = New FellowCls
    
'   field: fellow, type: DWORD
    Dim oidFellow As Long : oidFellow = mbuf.Get_DWORD()
    
'   Skip: unknown1
    Call mbuf.SkipCb(2)
    
'   field: level, type: WORD
    Dim lvl As Long : lvl = mbuf.Get_WORD()
    
'   field: maxHealth, type: DWORD
    Dim healthMax As Long : healthMax = mbuf.Get_DWORD()
    
'   field: maxStam, type: DWORD
    Dim staminaMax As Long : staminaMax = mbuf.Get_DWORD()
    
'   field: maxMana, type: DWORD
    Dim manaMax As Long : manaMax = mbuf.Get_DWORD()
    
'   field: curHealth, type: DWORD
    Dim healthCur As Long : healthCur = mbuf.Get_DWORD()
    
'   field: curStam, type: DWORD
    Dim staminaCur As Long : staminaCur = mbuf.Get_DWORD()
    
'   field: curMana, type: DWORD
    Dim manaCur As Long : manaCur = mbuf.Get_DWORD()
    
'   field: shareLoot, type: DWORD
    Dim fShareLoot As Long : fShareLoot = mbuf.Get_DWORD()
    
'   field: name, type: String
    Dim szFellow As String : szFellow = mbuf.Get_String()
    
    Call fellow.Init(oidFellow, szFellow, lvl, fShareLoot, _
      healthMax, staminaMax, manaMax, healthCur, staminaCur, manaCur)
    Set DecodeFellowInfo = fellow
    
End Function

Private Function DecodeDwellingACL(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As Object
    Set DecodeDwellingACL = Nothing
    
'   Skip: flags open allegiance
    Call mbuf.SkipCb(12)
    
'   field: guestCount, type: WORD
    Dim t_guestCount As Long : t_guestCount = mbuf.Get_WORD()
    
'   Skip: guestLimit
    Call mbuf.SkipCb(2)
    
'   Skip: guestList, length: t_guestCount
    Call mbuf.SkipCb(t_guestCount * 8)
    
End Function

Private Function DecodeDwellingItem(ByVal mbuf As MbufCls, _
      ByVal oidChar as Long, ByVal flagsLoc As Long) As Object
    Set DecodeDwellingItem = Nothing
    
'   Skip: quantityRequired quantityPaid type
    Call mbuf.SkipCb(12)
    
'   Skip: name, type: String
    Call mbuf.Skip_String()
    
'   Skip: pluralName, type: String
    Call mbuf.Skip_String()
    
End Function

' Destroy Object
Private Function Decode0024(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
'   No point implicitly creating an ACO just to destroy it.
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, False, True)
    If aco Is Nothing Then Exit Function
    
    Dim fInInventory As Boolean, acoContainer As AcoCls, iitem As Long
    fInInventory = aco.fInInventory
    If fInInventory Then
        Set acoContainer = aco.acoContainer
        iitem = aco.iitem
        Call otable.RemoveFromInventory(aco)
    End If
    
'   Fire script events.
    If fInInventory Then
        Call skev.RaiseOnRemoveFromInventory(aco, acoContainer, iitem)
    End If
    Call skev.RaiseOnObjectDestroy(aco)
    
    Call aco.Remove()
'   Call otable.DeleteAco(aco, True)
    
End Function

' Adjust Stack Size
Private Function Decode0197(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: item, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
'   AdjustStack can apparently come before the creation of the object,
'   e.g. in the case of a fired arrow.  Ignore it in that case.
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, False, True)
    If aco Is Nothing Then Exit Function
    Dim citemPrev As Long : citemPrev = aco.citemStack
    
'   field: count, type: DWORD
    Dim citem As Long : citem = mbuf.Get_DWORD()
    Call aco.SetBurden(aco.burden * citem/aco.citemStack)
    Call aco.SetCitemStack(citem)
    
'   field: value, type: DWORD
    Dim cpy As Long : cpy = mbuf.Get_DWORD()
    Call aco.SetCpyValue(cpy)
    
'   Fire script events.
    Call skev.RaiseOnAdjustStack(aco, citemPrev)
    
End Function

' Player Kill
Private Function Decode019E(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: text, type: String
    Dim szMsg As String : szMsg = mbuf.Get_String()
    
'   field: killed, type: DWORD
    Dim oidVictim As Long : oidVictim = mbuf.Get_DWORD()
    Dim acoVictim As AcoCls : Set acoVictim = otable.AcoFromOid(oidVictim, True, True)
    
'   field: killer, type: DWORD
    Dim oidKiller As Long : oidKiller = mbuf.Get_DWORD()
    Dim acoKiller As AcoCls : Set acoKiller = otable.AcoFromOid(oidKiller, True, True)
    
    Dim fMyPK As Boolean : fMyPK = (oidKiller = oidChar)
    If oidVictim = oidChar Then
        Call cospell.ClearTimedSpells()
    End If
    
'   Fire script events.
    If oidVictim = oidChar Then
        Call skev.RaiseOnDeathSelf(acoKiller, szMsg)
    ElseIf fMyPK Then
        Call skev.RaiseOnMyPlayerKill(acoVictim, szMsg)
    Else
        Call skev.RaiseOnDeathOther(acoVictim, acoKiller, szMsg)
    End If
    
End Function

' Indirect Text
Private Function Decode01E0(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: sender, type: DWORD
    Dim oidSender As Long : oidSender = mbuf.Get_DWORD()
    Dim acoSender As AcoCls : Set acoSender = otable.AcoFromOid(oidSender, True, True)
    
'   field: senderName, type: String
    Dim szSender As String : szSender = mbuf.Get_String()
    
'   field: text, type: String
    Dim szMsg As String : szMsg = mbuf.Get_String()
    
    Call acoSender.SetSzName(szSender)
    
'   Fire script events.
    Call skev.RaiseOnChatEmoteCustom(acoSender, szMsg)
    
End Function

' Emote Text
Private Function Decode01E2(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: sender, type: DWORD
    Dim oidSender As Long : oidSender = mbuf.Get_DWORD()
    Dim acoSender As AcoCls : Set acoSender = otable.AcoFromOid(oidSender, True, True)
    
'   field: senderName, type: String
    Dim szSender As String : szSender = mbuf.Get_String()
    
'   field: text, type: String
    Dim szMsg As String : szMsg = mbuf.Get_String()
    
    Call acoSender.SetSzName(szSender)
    
'   Fire script events.
    Call skev.RaiseOnChatEmoteStandard(acoSender, szMsg)
    
End Function

' Creature Message
Private Function Decode02BB(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: text, type: String
    Dim szMsg As String : szMsg = mbuf.Get_String()
    
'   field: senderName, type: String
    Dim szSender As String : szSender = mbuf.Get_String()
    
'   field: sender, type: DWORD
    Dim oidSender As Long : oidSender = mbuf.Get_DWORD()
    Dim acoSender As AcoCls : Set acoSender = otable.AcoFromOid(oidSender, True, True)
    
'   field: type, type: DWORD
    Dim tych As Long : tych = mbuf.Get_DWORD()
    
    Call acoSender.SetSzName(szSender)
    
'   Fire script events.
    Select Case tych
    Case tychPublicChat
        Call skev.RaiseOnChatLocal(acoSender, szMsg)
    Case tychPlayerSpellcasting
        Call skev.RaiseOnChatSpell(acoSender, szMsg)
    Case Else
        Call skev.RaiseOnChatBroadcast(acoSender, szMsg, tych)
    End Select
    
End Function

' Creature Message (Ranged)
Private Function Decode02BC(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: text, type: String
    Dim szMsg As String : szMsg = mbuf.Get_String()
    
'   field: senderName, type: String
    Dim szSender As String : szSender = mbuf.Get_String()
    
'   field: sender, type: DWORD
    Dim oidSender As Long : oidSender = mbuf.Get_DWORD()
    Dim acoSender As AcoCls : Set acoSender = otable.AcoFromOid(oidSender, True, True)
    
'   Skip: range
    Call mbuf.SkipCb(4)
    
'   field: type, type: DWORD
    Dim tych As Long : tych = mbuf.Get_DWORD()
    
    Call acoSender.SetSzName(szSender)
    
'   Fire script events.
    Call skev.RaiseOnChatBroadcast(acoSender, szMsg, tych)
    
End Function

' Set Character DWORD
Private Function Decode02CD(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: key, type: DWORD
    Dim lkey As Long : lkey = mbuf.Get_DWORD()
    
'   field: value, type: DWORD
    Dim dwVal As Long : dwVal = mbuf.Get_DWORD()
    
    Call self.SetDwordProp(lkey, dwVal, True)
    
End Function

' Set Object DWORD
Private Function Decode02CE(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   field: key, type: DWORD
    Dim lkey As Long : lkey = mbuf.Get_DWORD()
    
'   field: value, type: DWORD
    Dim dwVal As Long : dwVal = mbuf.Get_DWORD()
    
    Call aco.SetDwordProp(lkey, dwVal)
    
'   Fire script events.
'   Klugery to simulate obsolete Set Character Flag events.
    Select Case lkey
    Case lkeyCharType
        Select Case dwVal
        case &H2
            If oid = oidChar Then
                Call skev.RaiseOnSetFlagSelf(4, False)
            Else
                Call skev.RaiseOnSetFlagOther(aco, 4, False)
            End If
        Case &H4
            If oid = oidChar Then
                Call skev.RaiseOnSetFlagSelf(4, True)
            Else
                Call skev.RaiseOnSetFlagOther(aco, 4, True)
            End If
        End Select
    End Select
    
End Function

' Set Character QWORD
Private Function Decode02CF(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: key, type: DWORD
    Dim qkey As Long : qkey = mbuf.Get_DWORD()
    
'   field: value, type: QWORD
    Dim qVal As Double : qVal = mbuf.Get_QWORD()
    
    Call self.SetQwordProp(qkey, qVal, True)
    
End Function

' Set Character Boolean
Private Function Decode02D1(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: key, type: DWORD
    Dim bkey As Long : bkey = mbuf.Get_DWORD()
    
'   field: value, type: DWORD
    Dim fVal As Long : fVal = mbuf.Get_DWORD()
    
    Call self.SetBoolProp(bkey, fVal)
    
End Function

' Set Object Boolean
Private Function Decode02D2(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   field: key, type: DWORD
    Dim bkey As Long : bkey = mbuf.Get_DWORD()
    
'   field: value, type: DWORD
    Dim fVal As Long : fVal = mbuf.Get_DWORD()
    
    Call aco.SetBoolProp(bkey, fVal)
    
End Function

' Set Object String
Private Function Decode02D6(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: key, type: DWORD
    Dim skey As Long : skey = mbuf.Get_DWORD()
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
    Call mbuf.Align_DWORD()
    
'   field: value, type: String
    Dim szVal As String : szVal = mbuf.Get_String()
    
    Call aco.SetSzProp(skey, szVal)
    
End Function

' Set Object Resource
Private Function Decode02D8(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   field: key, type: DWORD
    Dim rkey As Long : rkey = mbuf.Get_DWORD()
    
'   field: value, type: DWORD
    Dim dwVal As Long : dwVal = mbuf.Get_DWORD()
    
    Call aco.SetResourceProp(rkey, dwVal)
    
End Function

' Set Character Link
Private Function Decode02D9(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: key, type: DWORD
    Dim akey As Long : akey = mbuf.Get_DWORD()
    
'   field: value, type: DWORD
    Dim oidVal As Long : oidVal = mbuf.Get_DWORD()
    Dim acoVal As AcoCls : Set acoVal = otable.AcoFromOid(oidVal, True, True)
    
'   Fire script events.
    Select Case akey
    Case akeyLastAttacker
        Call skev.RaiseOnMeleeLastAttacker(acoVal)
    End Select
    
End Function

' Set Object Link
Private Function Decode02DA(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   field: key, type: DWORD
    Dim akey As Long : akey = mbuf.Get_DWORD()
    
'   field: value, type: DWORD
    Dim oidVal As Long : oidVal = mbuf.Get_DWORD()
    
'   Not sure what key 20 is but it's filling up the otable with dead objects.
    If akey <> &H20 Then
        Dim fWasInInventory As Boolean, acoContainer As AcoCls, iitem As Long
        fWasInInventory = aco.fInInventory
        Set acoContainer = aco.acoContainer
        iitem = aco.iitem
        Call aco.SetLinkProp(akey, otable.AcoFromOid(oidVal, True, True))
    End If
    
'   Fire script events.
    If aco.fInInventory And Not fWasInInventory Then
        Call skev.RaiseOnAddToInventory(aco)
    ElseIf fWasInInventory And Not aco.fInInventory Then
        Call skev.RaiseOnRemoveFromInventory(aco, acoContainer, iitem)
    End If
    
End Function

' Set Character Skill Level
Private Function Decode02DD(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: key, type: DWORD
    Dim skid As Long : skid = mbuf.Get_DWORD()
    
'   field: value, type: SkillData
    Dim sd As SkillDataCls
    Set sd = DecodeSkillData(mbuf, oidChar, 0)
    Call self.SetSkinfo(skid, sd)
    
'   Fire script events.
    Call skev.RaiseOnStatSkillExp(skid, SzFromSkid(skid), sd.diff, sd.expInvested, sd.dlvl)
    
End Function

' Set Character Skill State
Private Function Decode02E1(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: key, type: DWORD
    Dim skid As Long : skid = mbuf.Get_DWORD()
    
'   field: value, type: DWORD
    Dim skts As Long : skts = mbuf.Get_DWORD()
    
    Call self.SetSkts(skid, skts)
    
End Function

' Set Character Attribute
Private Function Decode02E3(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: key, type: DWORD
    Dim attr As Long : attr = mbuf.Get_DWORD()
    
'   field: value, type: AttributeData
    Dim avd As AVDataCls
    Set avd = DecodeAttributeData(mbuf, oidChar, 0)
    
    Call self.SetAtinfo(attr, avd)
    
End Function

' Set Character Maximum Vital
Private Function Decode02E7(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: key, type: DWORD
    Dim vital As Long : vital = mbuf.Get_DWORD()
    
'   field: value, type: VitalData
    Dim avd As AVDataCls
    Set avd = DecodeVitalData(mbuf, oidChar, 0)
    
    Call self.SetVtinfo(vital, avd)
    
End Function

' Set Character Current Vital
Private Function Decode02E9(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: sequence
    Call mbuf.SkipCb(1)
    
'   field: key, type: DWORD
    Dim vital As Long : vital = mbuf.Get_DWORD()
    
'   field: value, type: DWORD
    Dim vlvl As Long : vlvl = mbuf.Get_DWORD()
    
    Call self.SetVital(vital, vlvl)
    
'   Fire script events.
    Call skev.RaiseOnVitals(self.healthCur, self.staminaCur, self.manaCur)
    
End Function

' End 3D Mode
Private Function DecodeF653(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
    If plig <> pligInWorld Then Exit Function
    plig = pligInPortal
    ctac = ctacConnected
    
'   Fire script events.
    Call skev.RaiseOnEnd3D()
    
    szFellowship = ""
    Call cofellow.Clear()
    Call cospell.Clear()
'   Clear the entire object table, but not until after event handler has
'   had a final crack at it.
    Call otable.DeleteAllAcos()
    Call LogMemStats(mbuf.tick)
    Call acapp.InvalRes
    
End Function

' Character List
Private Function DecodeF658(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: unknown1
    Call mbuf.SkipCb(4)
    
'   field: characterCount, type: DWORD
    Dim cchar As Long : cchar = mbuf.Get_DWORD()
    If cchar > 0 Then
        ReDim SkGlobals.rgszChar(cchar - 1)
    Else
        ReDim SkGlobals.rgszChar(0)
    End If
    SkGlobals.cchar = cchar
    
'   vector: characters, length: cchar
    Dim ichar As Long
    For ichar = 0 To cchar - 1
        
    '   Skip: character
        Call mbuf.SkipCb(4)
        
    '   field: name, type: String
        Dim szChar As String : szChar = mbuf.Get_String()
        
    '   Skip: deleteTimeout
        Call mbuf.SkipCb(4)
        
        Call TraceLine(szChar)
    '   Insert szChar into rgszChar in alphabetical order.
        Dim icharT As Long
        For icharT = 0 To ichar - 1
            Dim szT As String : szT = SkGlobals.rgszChar(icharT)
        '   Use Binary compare because Text compare ignores hyphens.
            If StrComp(szChar, szT, vbBinaryCompare) < 0 Then
            '   Insert szChar here.
                SkGlobals.rgszChar(icharT) = szChar
            '   Bump previous contents to next slot.
                szChar = szT
            End If
        Next icharT
        SkGlobals.rgszChar(icharT) = szChar
        
    Next ichar
    
'   Skip: unknown2 slotCount
    Call mbuf.SkipCb(8)
    
'   field: zonename, type: String
    Dim szAccount As String : szAccount = mbuf.Get_String()
    Call self.SetSzAccount(szAccount)
    
End Function

' Create Object
Private Function DecodeF745(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    Dim acoContainerPrev As AcoCls : Set acoContainerPrev = aco.acoContainer
    
'   field: model, type: ModelData
    Dim t_model As Object
    Set t_model = DecodeModelData(mbuf, oidChar, 0)
    
'   field: physics, type: PhysicsData
    Dim pd As PhysicsDataCls
    Set pd = DecodePhysicsData(mbuf, oidChar, 0)
    If pd.fOpenable Then Call aco.SetFOpen(pd.fOpen)
    Call aco.SetMaploc(pd.maploc)
    
'   field: game, type: GameData
    Dim gd As GameDataCls
    Set gd = DecodeGameData(mbuf, oidChar, 0)
    Call aco.FromGameData(gd)
    
    Call otable.NoteAcoCreated(aco)
'   Call otable.CheckPendingInventory()
    
    Dim fAddToInv As Boolean : fAddToInv = False
    If aco.FPendingInsert() Then
    '   The object has already been inserted into a pack, prior to its
    '   formal creation.  Now that it's been created, go ahead and fire
    '   the OnAddToInventory event.
    '   REVIEW: This can violate WaitEvent mode 1 by firing two
    '   events on one call to WaitEvent.
        fAddToInv = True
        Call aco.ClearFPendingInsert()
    ElseIf acoContainerPrev Is Nothing And Not (aco.acoContainer Is Nothing) And _
      aco.acoContainer Is otable.acoChar Then
        If aco.fContainer Then
            Call otable.InsertPack(1, aco)
        Else
        '   The object is being created in pack zero, with no prior Insert message.
        '   Assume it's being inserted at position zero.
            Call aco.Remove()
            Call aco.acoContainer.InsertItem(0, aco)
        End If
        fAddToInv = True
    End If
    
'   Fire script events.
    If (aco.oty And otyPlayer) <> 0 Then
        Call skev.RaiseOnObjectCreatePlayer(aco)
    Else
        Call skev.RaiseOnObjectCreate(aco)
        If fAddToInv Then
            Call skev.RaiseOnAddToInventory(aco)
        End If
    End If
    
'   Dispatch any pending messages (e.g. inserts or removals) that arrived
'   before the Create message.
    Dim mbufPending As MbufCls : Set mbufPending = aco.mbufPending
    If Not (mbufPending Is Nothing) Then
        Call aco.SetMbufPending(Nothing)
        evidLast = EvidDispatchEvent(mbufPending)
    End If
    
End Function

' Remove Item
Private Function DecodeF747(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
'   For some reason the player is "removed" during login (weird as that seems).
'   Ignore any such removals.
    If oid = oidChar Then Exit Function
    
'   Equally weirdly, objects can be removed before they're created.
'   (I've seen this happen with arrow fired at close range.)
'   Detect that case and postpone the removal until after the creation.
    If aco.fPending Then
        Call aco.SetMbufPending(mbuf.MbufClone())
        Exit Function
    End If
    
'   Fire script events.
    Call skev.RaiseOnObjectDestroy(aco)
    
    Call aco.Remove()
'   Call otable.DeleteAco(aco, True)
    
End Function

' Set Position And Motion
Private Function DecodeF748(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   field: position, type: Position
    Dim maploc As Object
    Set maploc = DecodePosition(mbuf, oidChar, 0)
    Dim maplocPrev As MaplocCls : Set maplocPrev = aco.maploc
    Call aco.SetMaploc(maploc)
    
'   Fire script events.
    If oid = oidChar Then
        Call skev.RaiseOnLocChangeSelf(maploc)
    ElseIf (aco.oty And otyPlayer) <> 0 Then
        Call skev.RaiseOnLocChangeOther(aco)
    ElseIf aco.mcm = mcmCreature Then
        Call skev.RaiseOnLocChangeCreature(aco)
    End If
    
End Function

' Move Object Into Inventory
Private Function DecodeF74A(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
'   No point implicitly creating an ACO just for this.
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, False, True)
    If aco Is Nothing Then Exit Function
    
    If Not (aco.acoContainer Is otable.acoChar) And _
      Not (aco.acoWearer Is otable.acoChar) Then
        Call aco.SetMaploc(Nothing)
        Call aco.SetAcoWearer(Nothing)
    End If
    
End Function

' Toggle Object Visibility
Private Function DecodeF74B(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   field: portalType, type: WORD
    Dim portalType As Long : portalType = mbuf.Get_WORD()
    
    If oid = oidChar Then
        If portalType = &H4410& Then
        '   Enter portal.
            If plig < pligInWorld Then Exit Function
            plig = pligInPortal
            fFirstSPMInPortal = True
        ElseIf portalType = &H0408& Then
        '   Exit portal.
            If plig = pligInWorld Then Exit Function
            plig = pligInWorld
            ctac = ctac Or ctacFullInventory
        End If
    End If
    
'   Fire script events.
    Select Case portalType
    Case &H4410&
        If oid = oidChar Then
            Call skev.RaiseOnStartPortalSelf()
        End If
    Case &H0408&
        If oid = oidChar Then
            Call skev.RaiseOnEndPortalSelf()
        Else
            Call skev.RaiseOnEndPortalOther(aco)
        End If
    End Select
    
End Function

' Animation
Private Function DecodeF74C(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
    Dim dwE As Long
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   field: logins, type: WORD
    Dim logins As Long : logins = mbuf.Get_WORD()
    
'   field: sequence, type: WORD
    Dim sequence As Long : sequence = mbuf.Get_WORD()
    
'   field: index, type: WORD
    Dim index As Long : index = mbuf.Get_WORD()
    
'   field: activity, type: WORD
    Dim activity As Long : activity = mbuf.Get_WORD()
    
'   field: animation_type, type: BYTE
    Dim animation_type As Byte : animation_type = mbuf.Get_BYTE()
    
'   field: type_flags, type: BYTE
    Dim type_flags As Byte : type_flags = mbuf.Get_BYTE()
    
'   field: stance, type: WORD
    Dim stance As Long : stance = mbuf.Get_WORD()
    
'   switch: animation_type
    Select Case animation_type
    Case &H0000&
        
    '   field: flags, type: DWORD
        Dim flags As Long : flags = mbuf.Get_DWORD()
        dwE = flags
        
    '   maskmap: flags
        If (flags And &H00000001&) <> 0 Then
            
        '   Skip: stance2
            Call mbuf.SkipCb(2)
            
        End If
        If (flags And &H00000002&) <> 0 Then
            
        '   field: animation_1, type: WORD
            Dim animation_1 As Long : animation_1 = mbuf.Get_WORD()
            If stance = &H3D Then
                Select Case animation_1
                Case &H0B
                    Call aco.SetFOpen(True)
                Case &H0C
                    Call aco.SetFOpen(False)
                End Select
            End If
            
        End If
        If (flags And &H00000008&) <> 0 Then
            
        '   Skip: animation_2
            Call mbuf.SkipCb(2)
            
        End If
        If (flags And &H00000020&) <> 0 Then
            
        '   Skip: animation_3
            Call mbuf.SkipCb(2)
            
        End If
        If (flags And &H00000004&) <> 0 Then
            
        '   Skip: float_1
            Call mbuf.SkipCb(4)
            
        End If
        If (flags And &H00000010&) <> 0 Then
            
        '   Skip: float_2
            Call mbuf.SkipCb(4)
            
        End If
        If (flags And &H00000040&) <> 0 Then
            
        '   Skip: float_3
            Call mbuf.SkipCb(4)
            
        End If
        
    '   Skip: animations, length: flags
        Call mbuf.SkipCb(LExtractMask(flags, &H00000F80&) * 8)
        
    Case &H0006&
        
    '   field: target, type: DWORD
        Dim oid6 As Long : oid6 = mbuf.Get_DWORD()
        dwE = oid6
        
    '   Skip: landblock xOffset yOffset zOffset flags_2 float_1 float_2 unknown_3 animation_speed float_4 heading unknown_value
        Call mbuf.SkipCb(48)
        
    Case &H0007&
        
    '   field: landblock, type: DWORD
        Dim landblock As Long : landblock = mbuf.Get_DWORD()
        dwE = landblock
        
    '   Skip: xOffset yOffset zOffset flags_2 float_1 float_2 unknown_3 animation_speed float_4 heading unknown_value
        Call mbuf.SkipCb(44)
        
    Case &H0008&
        
    '   field: target, type: DWORD
        Dim oid8 As Long : oid8 = mbuf.Get_DWORD()
        dwE = oid8
        
    '   Skip: unknown_value flags_2 animation_speed heading
        Call mbuf.SkipCb(16)
        
    Case &H0009&
        
    '   field: flags_2, type: DWORD
        Dim flags_2 As Long : flags_2 = mbuf.Get_DWORD()
        dwE = flags_2
        
    End Select
    
'   Fire script events.
    If oid = oidChar Then
        Dim fCombatModeNew As Boolean
        fCombatModeNew = (stance <> stanceStanding)
        If fCombatModeNew = fCombatMode Then
            Dim dwA As Long, dwB As Long, dwC As Long, dwD As Long
            dwA = mbuf.cbOfMsg + 12
            dwB = DwFromWords(sequence, logins)
            dwC = DwFromWords(activity, index)
            dwD = DwFromWords(stance, type_flags * &H100& + animation_type)
            Call skev.RaiseOnAnimationSelf(dwA, dwB, dwC, dwD, dwE)
        Else
            fCombatMode = fCombatModeNew
            Call skev.RaiseOnCombatMode(fCombatMode)
        End If
    End If
    
End Function

' Enter Portal Mode
Private Function DecodeF751(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
    If plig < pligInWorld Then Exit Function
    plig = pligInPortal
    fFirstSPMInPortal = True
    
'   Fire script events.
    Call skev.RaiseOnStartPortalSelf()
    
End Function

' Login Character
Private Function DecodeF7B0_0013(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
    Dim acoChar As AcoCls : Set acoChar = otable.AcoFromOid(oidChar, True, True)
    Set otable.acoChar = acoChar
    Call acoChar.SetFContainer(True)
    Call acoChar.RemoveAll()
    Do While otable.cpack > 0
        Call otable.RemovePack(otable.cpack - 1)
    Loop
    Call otable.InsertPack(0, acoChar)
    
    Dim ipackDst As Long, iitemDst As Long
    ipackDst = 1
    iitemDst = 0
    
'   field: properties, type: CharacterPropertyData
    Dim cpd As Object
    Set cpd = DecodeCharacterPropertyData(mbuf, oidChar, 0)
    
'   field: vectors, type: CharacterVectorData
    Dim cvd As Object
    Set cvd = DecodeCharacterVectorData(mbuf, oidChar, 0)
    
'   field: options, type: CharacterOptionData
    Dim cod As Object
    Set cod = DecodeCharacterOptionData(mbuf, oidChar, 0)
    
'   field: inventoryCount, type: DWORD
    Dim t_inventoryCount As Long : t_inventoryCount = mbuf.Get_DWORD()
    
'   vector: inventory, length: t_inventoryCount
    Dim iitem As Long
    For iitem = 0 To t_inventoryCount - 1
        
    '   field: item, type: DWORD
        Dim oidItem As Long : oidItem = mbuf.Get_DWORD()
        Dim acoItem As AcoCls : Set acoItem = otable.AcoFromOid(oidItem, True, True)
        
    '   field: type, type: DWORD
        Dim dwContainer As Long : dwContainer = mbuf.Get_DWORD()
        Dim fContainer As Boolean
        fContainer = (dwContainer <> 0)
        
        Call acoItem.SetFContainer(fContainer)
        If fContainer Then
            Call otable.InsertPack(ipackDst, acoItem)
            ipackDst = ipackDst + 1
        Else
            Call acoChar.InsertItem(iitemDst, acoItem)
            iitemDst = iitemDst + 1
        End If
        
    Next iitem
    
'   field: equippedCount, type: DWORD
    Dim t_equippedCount As Long : t_equippedCount = mbuf.Get_DWORD()
    
'   vector: equipped, length: t_equippedCount
    Dim iitemEquip As Long
    For iitemEquip = 0 To t_equippedCount - 1
        
    '   field: item, type: DWORD
        Dim oidEquip As Long : oidEquip = mbuf.Get_DWORD()
        Dim acoEquip As AcoCls : Set acoEquip = otable.AcoFromOid(oidEquip, True, True)
        
    '   field: slot, type: DWORD
        Dim eqm As Long : eqm = mbuf.Get_DWORD()
        
    '   Skip: unknown3
        Call mbuf.SkipCb(4)
        
        Call acoEquip.SetAcoWearer(acoChar)
        Call acoEquip.SetEqmWearer(eqm)
        Call otable.AddEquipment(acoEquip)
        
    Next iitemEquip
    
    fCombatMode = False
    If skapi.objScript Is Nothing Then
    '   ACScript compatibility hack:
    '   If script hasn't start yet, call it CachedLogin.
        ctac = ctac Or ctacCachedLogin
    Else
        ctac = ctac Or ctacRealLogin
    End If
    
'   Fire script events.
    Call skev.RaiseOnLogon(acoChar)
    
End Function

' Insert Inventory Item
Private Function DecodeF7B0_0022(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: item, type: DWORD
    Dim oidItem As Long : oidItem = mbuf.Get_DWORD()
    Dim acoItem As AcoCls : Set acoItem = otable.AcoFromOid(oidItem, True, True)
    
'   field: container, type: DWORD
    Dim oidPack As Long : oidPack = mbuf.Get_DWORD()
    Dim acoPack As AcoCls : Set acoPack = otable.AcoFromOid(oidPack, True, True)
    
'   field: slot, type: DWORD
    Dim iitem As Long : iitem = mbuf.Get_DWORD()
    
'   field: type, type: DWORD
    Dim kind As Long : kind = mbuf.Get_DWORD()
    
    Dim fWasInInventory As Boolean, acoContainerPrev As AcoCls, iitemPrev As Long
    fWasInInventory = acoItem.fInInventory
    If fWasInInventory Then
        Set acoContainerPrev = acoItem.acoContainer
        iitemPrev = acoItem.iitem
    End If
    Call acoItem.SetMaploc(Nothing)
'   Insert can happen before creation, so don't trust acoItem.fContainer.
'   If acoItem.fContainer Then
    If kind > 0 Then
        Dim ipack As Long : ipack = iitem + 1
        iitem = iitem + 1
        If acoItem.acoContainer Is otable.acoChar Then
            Call otable.RemovePack(acoItem.iitem)
        End If
        If acoPack Is otable.acoChar Then
            Call otable.InsertPack(ipack, acoItem)
        Else
        '   REVIEW: Keep track of packs in house storage?
        End If
    Else
        Call acoItem.Remove
    '   REVIEW: Giving/selling things to NPCs causes this message to be
    '   sent, even though the NPCs aren't flagged as containers (nor
    '   would we want to treat them as such).
        If acoPack.fContainer Then
            Call acoPack.InsertItem(iitem, acoItem)
        End If
    End If
    
'   Insert Inventory Item can come before the Create Object message for
'   the item being inserted (e.g. in the case of stack splitting).  If that's the
'   case, don't fire the event here, but set a flag to tell Create Object to fire
'   it after the object has been created.  This at least appears to be the way
'   ACScript does it.
    If acoItem.fPending And acoItem.fInInventory Then
        Call acoItem.SetFPendingInsert()
    End If
    
'   Fire script events.
    If acoItem.fInInventory Then
        If fWasInInventory Then
            Call skev.RaiseOnMoveItem(acoItem, acoContainerPrev, iitemPrev)
        End If
        If Not acoItem.fPending Then
            Call skev.RaiseOnAddToInventory(acoItem)
        End If
    Else
        If fWasInInventory Then
            Call skev.RaiseOnRemoveFromInventory(acoItem, acoContainerPrev, iitemPrev)
        End If
    End If
    
End Function

' Wear Item
Private Function DecodeF7B0_0023(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: item, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   field: slot, type: DWORD
    Dim eqm As Long : eqm = mbuf.Get_DWORD()
    
    Call aco.SetAcoWearer(otable.acoChar)
    Call aco.SetEqmWearer(eqm)
    Call otable.AddEquipment(aco)
    
End Function

' Approach Vendor
Private Function DecodeF7B0_0062(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
    Dim coaco As CoacoCls : Set coaco = New CoacoCls
    Call coaco.SetMutable
    
    Dim ibItemPrev As Long : ibItemPrev = iNil  ' For debugging.
    
'   field: merchant, type: DWORD
    Dim oidMerchant As Long : oidMerchant = mbuf.Get_DWORD()
    Dim acoMerchant As AcoCls : Set acoMerchant = otable.AcoFromOid(oidMerchant, True, True)
    
'   field: buyCategories, type: DWORD
    Dim mcmBuy As Long : mcmBuy = mbuf.Get_DWORD()
    
'   Skip: unknown1
    Call mbuf.SkipCb(4)
    
'   field: buyValue, type: DWORD
    Dim cpyMaxBuy As Long : cpyMaxBuy = mbuf.Get_DWORD()
    
'   Skip: unknown2
    Call mbuf.SkipCb(4)
    
'   field: buyRate, type: float
    Dim fractBuy As Single : fractBuy = mbuf.Get_float()
    
'   field: sellRate, type: float
    Dim fractSell As Single : fractSell = mbuf.Get_float()
    
'   field: itemCount, type: DWORD
    Dim t_itemCount As Long : t_itemCount = mbuf.Get_DWORD()
    
'   vector: items, length: t_itemCount
    Dim iitem As Long
    For iitem = 0 To t_itemCount - 1
        
        Dim ibItem As Long
        ibItem = mbuf.ibInMsg  ' For debugging.
        
    '   field: count, type: WORD
        Dim citemAvail As Long : citemAvail = mbuf.Get_WORD()
        
    '   Skip: flags
        Call mbuf.SkipCb(2)
        
    '   field: object, type: DWORD
        Dim oid As Long : oid = mbuf.Get_DWORD()
        Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
        Call aco.SetCitemAvail(citemAvail)
        
    '   field: game, type: GameData
        Dim gd As GameDataCls
        Set gd = DecodeGameData(mbuf, oidChar, 0)
        Call aco.FromGameData(gd)
        
        Call coaco.AddAcoF(aco)
        ibItemPrev = ibItem  ' For debugging.
        
    Next iitem
    
'   Fire script events.
    Call skev.RaiseOnApproachVendor(acoMerchant, mcmBuy, cpyMaxBuy, fractBuy, fractSell, coaco)
    
End Function

' Quit Fellowship
Private Function DecodeF7B0_00A3(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: fellow, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    
    If oid = oidChar Then
        szFellowship = ""
        Call cofellow.Clear()
    Else
        Call cofellow.Remove(oid)
    End If
    
'   Fire script events.
    Call skev.RaiseOnFellowQuit(oid)
    
End Function

' Dismiss Fellowship Member
Private Function DecodeF7B0_00A4(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: fellow, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    
    If oid = oidChar Then
        szFellowship = ""
        Call cofellow.Clear()
    Else
        Call cofellow.Remove(oid)
    End If
    
'   Fire script events.
    Call skev.RaiseOnFellowDismiss(oid)
    
End Function

' Identify Object
Private Function DecodeF7B0_00C9(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
    Dim cbi As CbiCls, cai As CaiCls, chi As ChiCls
    Dim ibi As IbiCls, iai As IaiCls, iwi As IwiCls, iei As IeiCls, ipi As IpiCls
    Dim kindWieldReq As Long, idWieldReq As Long, lvlWieldReq As Long
    kindWieldReq = iNil
    idWieldReq = iNil
    lvlWieldReq = iNil
    Dim dictRaw As Dictionary
    Set dictRaw = New Dictionary
    dictRaw.CompareMode = vbBinaryCompare
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    Call idq.ResponseReceived(oid, mbuf.tick)
    
'   field: flags, type: DWORD
    Dim flags As Long : flags = mbuf.Get_DWORD()
    
'   field: success, type: DWORD
    Dim fSuccess As Long : fSuccess = mbuf.Get_DWORD()
    
'   maskmap: flags
    If (flags And &H00000001&) <> 0 Then
        
    '   field: dwordCount, type: WORD
        Dim t_dwordCount As Long : t_dwordCount = mbuf.Get_WORD()
        
    '   Skip: dwordUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: dwords, length: t_dwordCount
        Dim dwords As Long
        For dwords = 0 To t_dwordCount - 1
            
        '   field: key, type: DWORD
            Dim lkey As Long : lkey = mbuf.Get_DWORD()
            
        '   field: value, type: DWORD
            Dim dwVal As Long : dwVal = mbuf.Get_DWORD()
            
            dictRaw.Item(DwFromWords(1, lkey)) = dwVal
            
            Select Case lkey
                
            Case lkeyBurden
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.burden = dwVal
            Case lkeyRareID
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.rareid = dwVal
            Case lkeyValue
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.cpyValue = dwVal
            Case lkeyMaterial
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.material = dwVal
            Case lkeyWorkmanship
                If ibi Is Nothing Then Set ibi = New IbiCls
                Call ibi.SetWorkmanship(dwVal)
            Case lkeySalvageCount
                If ibi Is Nothing Then Set ibi = New IbiCls
                Call ibi.SetCitemSalvaged(dwVal)
            Case lkeyManaCur
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.manaCur = dwVal
            Case lkeyCpageTotal
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.cpageTotal = dwVal
            Case lkeyCpageUsed
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.cpageUsed = dwVal
            Case lkeyVitalRestored
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.vitalRestored = dwVal
            Case lkeyDlvlRestored
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.dlvlRestored = dwVal
            Case lkeyTinkerCount
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.ctink = dwVal
            Case lkeyManaCost
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.manaCost = dwVal
            Case lkeyCkeyOnRing
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.ckeyOnRing = dwVal
            Case lkeyLockDiff
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.diffLock = dwVal
                
            Case lkeyWieldReqKind
                kindWieldReq = dwVal
            Case lkeyWieldReqID
                idWieldReq = dwVal
            Case lkeyWieldReqLvl
                lvlWieldReq = dwVal
                
            Case lkeyAttuned
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.fAttuned = dwVal
            Case lkeyBonded
                If ibi Is Nothing Then Set ibi = New IbiCls
                Select Case dwVal
                Case 1
                    ibi.fBonded = True
                Case -1
                    ibi.fDropOnDeath = True
                End Select
            '   Case lkeyRetained
            '   If ibi Is Nothing Then Set ibi = New IbiCls
            '   ibi.fRetained = dwVal
            Case lkeyEnchantability
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.fUnenchantable = (dwVal = 9999)
                
            Case lkeyArmorLevel
                If iai Is Nothing Then Set iai = New IaiCls
                iai.al = dwVal
                
            Case lkeyImbues
                If iwi Is Nothing Then Set iwi = New IwiCls
                iwi.fCriticalStrike		= (dwVal And &H00000001&) <> 0
                iwi.fCripplingBlow		= (dwVal And &H00000002&) <> 0
                iwi.fArmorRending		= (dwVal And &H00000004&) <> 0
                iwi.fSlashRending		= (dwVal And &H00000008&) <> 0
                iwi.fPierceRending		= (dwVal And &H00000010&) <> 0
                iwi.fBludgeonRending	= (dwVal And &H00000020&) <> 0
                iwi.fAcidRending		= (dwVal And &H00000040&) <> 0
                iwi.fColdRending		= (dwVal And &H00000080&) <> 0
                iwi.fLightningRending	= (dwVal And &H00000100&) <> 0
                iwi.fFireRending		= (dwVal And &H00000200&) <> 0
                iwi.fPhantasmal			= (dwVal And &H80000000&) <> 0
            Case lkeyDmtyWand
                If iwi Is Nothing Then Set iwi = New IwiCls
                iwi.dmty				= dwVal
            Case lkeyElemDmgBonus
                If iwi Is Nothing Then Set iwi = New IwiCls
                iwi.dhealthElemBonus	= dwVal
                
            Case lkeySpellcraft
                If iei Is Nothing Then Set iei = New IeiCls
                iei.spellcraft = dwVal
            Case lkeyManaMax
                If iei Is Nothing Then Set iei = New IeiCls
                iei.manaMax = dwVal
                If iei.manaCur = 0 Then iei.manaCur = ibi.manaCur
            Case lkeyLoreReq
                If iei Is Nothing Then Set iei = New IeiCls
                iei.difficulty = dwVal
            Case lkeyRankReq
                If iei Is Nothing Then Set iei = New IeiCls
                iei.rankReq = dwVal
            Case lkeySklvlReq
                If iei Is Nothing Then Set iei = New IeiCls
                iei.sklvlReq = dwVal
            Case lkeySkidReq
                If iei Is Nothing Then Set iei = New IeiCls
                iei.skidReq = dwVal
                
            Case lkeyLvlMin
                If ipi Is Nothing Then Set ipi = New IpiCls
                ipi.lvlMin = dwVal
            Case lkeyLvlMax
                If ipi Is Nothing Then Set ipi = New IpiCls
                ipi.lvlMax = dwVal
            Case lkeyPortalFlags
                If ipi Is Nothing Then Set ipi = New IpiCls
                ipi.flags = dwVal
                
            Case lkeySpecies
                If cbi Is Nothing Then Set cbi = New CbiCls
                cbi.species = dwVal
            Case lkeyCreatureLevel
                If cbi Is Nothing Then Set cbi = New CbiCls
                cbi.lvl = dwVal
                
            Case lkeyGender
                If chi Is Nothing Then Set chi = New ChiCls
                chi.szGender = SzFromGender(dwVal)
            Case lkeyRaceReq
                If Not chi Is Nothing Then
                    chi.szRace = SzFromRace(dwVal)
                ElseIf Not iei Is Nothing Then
                    iei.szRaceReq = SzFromRace(dwVal)
                End If
            Case lkeyCharType
                If chi Is Nothing Then Set chi = New ChiCls
                chi.fPK = (dwVal And &H4) <> 0
            Case lkeyRank
                If chi Is Nothing Then Set chi = New ChiCls
                chi.rank = dwVal
            Case lkeyFollowers
                If chi Is Nothing Then Set chi = New ChiCls
                chi.cfollower = dwVal
                
            Case lkeyDescriptionFormat, lkeySetWithCount, lkeySetWithMaterial
            '   Not actually using these, just trying to cut down on DebugLine spam.
                
            Case Else
                Call DebugLine("lkey  = " + SzHex(lkey, 2) + ", " + _
                  "dwVal = " + SzHex(dwVal, 8) + ", " + _
                  "aco = " + SzHex(oid, 8) + " " + aco.szName)
            End Select
            
        Next dwords
        
    End If
    If (flags And &H00002000&) <> 0 Then
        
    '   field: qwordCount, type: WORD
        Dim t_qwordCount As Long : t_qwordCount = mbuf.Get_WORD()
        
    '   Skip: qwordUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: qwords, length: t_qwordCount
        Dim qwords As Long
        For qwords = 0 To t_qwordCount - 1
            
        '   field: key, type: DWORD
            Dim qkey As Long : qkey = mbuf.Get_DWORD()
            
        '   field: value, type: QWORD
            Dim qVal As Double : qVal = mbuf.Get_QWORD()
            
            dictRaw.Item(DwFromWords(&H2000, qkey)) = qVal
            
            Select Case qkey
                
            Case Else
                Call DebugLine("bkey = " + SzHex(qkey, 2) + ", " + _
                  "qVal = " + CStr(qVal) + ", " + _
                  "aco = " + SzHex(oid, 8) + " " + aco.szName)
            End Select
            
        Next qwords
        
    End If
    If (flags And &H00000002&) <> 0 Then
        
    '   field: booleanCount, type: WORD
        Dim t_booleanCount As Long : t_booleanCount = mbuf.Get_WORD()
        
    '   Skip: booleanUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: booleans, length: t_booleanCount
        Dim booleans As Long
        For booleans = 0 To t_booleanCount - 1
            
        '   field: key, type: DWORD
            Dim bkey As Long : bkey = mbuf.Get_DWORD()
            
        '   field: value, type: DWORD
            Dim fVal As Long : fVal = mbuf.Get_DWORD()
            
            dictRaw.Item(DwFromWords(2, bkey)) = fVal
            
            Select Case bkey
            Case bkeyOpen
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.fOpen = fVal
            Case bkeyLocked
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.fLocked = fVal
            Case bkeyRetained
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.fRetained = fVal
            Case bkeySellable
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.fSellable = fVal
            Case bkeyIvoryable
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.fIvoryable = fVal
            Case bkeyDyeable
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.fDyeable = fVal
            Case bkeyUnlimitedUses
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.fUnlimitedUses = fVal
            Case bkeyDropOnDeath
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.fDropOnDeath = fVal
                
            Case Else
                Call DebugLine("bkey = " + SzHex(bkey, 2) + ", " + _
                  "fVal = " + CStr(fVal) + ", " + _
                  "aco = " + SzHex(oid, 8) + " " + aco.szName)
            End Select
            
        Next booleans
        
    End If
    If (flags And &H00000004&) <> 0 Then
        
    '   field: doubleCount, type: WORD
        Dim t_doubleCount As Long : t_doubleCount = mbuf.Get_WORD()
        
    '   Skip: doubleUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: doubles, length: t_doubleCount
        Dim doubles As Long
        For doubles = 0 To t_doubleCount - 1
            
        '   field: key, type: DWORD
            Dim dkey As Long : dkey = mbuf.Get_DWORD()
            
        '   field: value, type: double
            Dim dblVal As Double : dblVal = mbuf.Get_double()
            
            dictRaw.Item(DwFromWords(4, dkey)) = dblVal
            
            Select Case dkey
            Case dkeyEfficiency
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.fractEfficiency = dblVal
            Case dkeyProbDestroy
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.probDestroy = dblVal
            Case dkeyManaConvMod
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.fractManaConvMod = dblVal
            Case dkeyRestorationBonus
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.fractRestorationBonus = dblVal
                
            Case dkeyCsecPerMana
                If iei Is Nothing Then Set iei = New IeiCls
                iei.csecPerMana = 1/dblVal
                
            Case dkeyMeleeDBonus
                If iwi Is Nothing Then Set iwi = New IwiCls
                iwi.scaleDefenseBonus = dblVal
            Case dkeyMissileDBonus
                If iwi Is Nothing Then Set iwi = New IwiCls
                iwi.scaleMissileDBonus = dblVal
            Case dkeyMagicDBonus
                If iwi Is Nothing Then Set iwi = New IwiCls
                iwi.scaleMagicDBonus = dblVal
            Case dkeyPvMElemBonus
                If iwi Is Nothing Then Set iwi = New IwiCls
                iwi.scalePvMElemBonus = dblVal
                
            Case Else
                Call DebugLine("dkey = " + SzHex(dkey, 2) + ", " + _
                  "dblVal = " + CStr(dblVal) + ", " + _
                  "aco = " + SzHex(oid, 8) + " " + aco.szName)
            End Select
            
        Next doubles
        
    End If
    If (flags And &H00000008&) <> 0 Then
        
    '   field: stringCount, type: WORD
        Dim t_stringCount As Long : t_stringCount = mbuf.Get_WORD()
        
    '   Skip: stringUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: strings, length: t_stringCount
        Dim strings As Long
        For strings = 0 To t_stringCount - 1
            
        '   field: key, type: DWORD
            Dim skey As Long : skey = mbuf.Get_DWORD()
            
        '   field: value, type: String
            Dim szVal As String : szVal = mbuf.Get_String()
            
            dictRaw.Item(DwFromWords(8, skey)) = szVal
            
            Select Case skey
            Case skeyInscription
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.szInscription = szVal
            Case skeyInscriber
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.szInscriber = szVal
            Case skeyUseInstructions
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.szComment = szVal
            Case skeySimpleDesc
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.szDescSimple = szVal
            Case skeyDetailedDesc
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.szDescDetailed = szVal
            Case skeyCreator
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.szCreator = szVal
            Case skeyLastTinkerer
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.szTinkerer = szVal
            Case skeyImbuer
                If ibi Is Nothing Then Set ibi = New IbiCls
                ibi.szImbuer = szVal
                
            Case skeyPortalDest
                If ipi Is Nothing Then Set ipi = New IpiCls
                ipi.szDest = szVal
                
            Case skeyTitle
                If chi Is Nothing Then Set chi = New ChiCls
                chi.szProfession = szVal
            Case skeyFellowship
                If chi Is Nothing Then Set chi = New ChiCls
                chi.szFellowship = szVal
            Case skeyMonarch
                If chi Is Nothing Then Set chi = New ChiCls
                chi.szMonarch = szVal
            Case skeyPatron
                If chi Is Nothing Then Set chi = New ChiCls
                chi.szPatron = szVal
                
            Case Else
                Call DebugLine("skey = " + SzHex(skey, 2) + ", " + _
                  "szVal = " + SzQuote(szVal) + ", " + _
                  "aco = " + SzHex(oid, 8) + " " + aco.szName)
            End Select
            
        Next strings
        
    End If
    If (flags And &H00001000&) <> 0 Then
        
    '   field: resourceCount, type: WORD
        Dim t_resourceCount As Long : t_resourceCount = mbuf.Get_WORD()
        
    '   Skip: resourceUnknown
        Call mbuf.SkipCb(2)
        
    '   Skip: resources, length: t_resourceCount
        Call mbuf.SkipCb(t_resourceCount * 8)
        
    End If
    If (flags And &H00000010&) <> 0 Then
        
    '   field: spellCount, type: WORD
        Dim cspellid As Long : cspellid = mbuf.Get_WORD()
        If cspellid > 0 Then
            If iei Is Nothing Then Set iei = New IeiCls
            Call iei.SetCspellid(cspellid)
        End If
        
    '   Skip: spellUnknown
        Call mbuf.SkipCb(2)
        
    '   vector: spells, length: cspellid
        Dim spells As Long
        For spells = 0 To cspellid - 1
            
        '   field: spell, type: WORD
            Dim spellid As Long : spellid = mbuf.Get_WORD()
            
        '   field: flags, type: WORD
            Dim flagsSpell As Long : flagsSpell = mbuf.Get_WORD()
            
            Call iei.AddSpellid(spellid, flagsSpell And &HFFFF&)
            
        Next spells
        
    End If
    If (flags And &H00000080&) <> 0 Then
        
    '   field: protSlashing, type: float
        Dim protSlashing As Single : protSlashing = mbuf.Get_float()
        
    '   field: protPiercing, type: float
        Dim protPiercing As Single : protPiercing = mbuf.Get_float()
        
    '   field: protBludgeoning, type: float
        Dim protBludgeoning As Single : protBludgeoning = mbuf.Get_float()
        
    '   field: protCold, type: float
        Dim protCold As Single : protCold = mbuf.Get_float()
        
    '   field: protFire, type: float
        Dim protFire As Single : protFire = mbuf.Get_float()
        
    '   field: protAcid, type: float
        Dim protAcid As Single : protAcid = mbuf.Get_float()
        
    '   field: protLightning, type: float
        Dim protElectrical As Single : protElectrical = mbuf.Get_float()
        If iai Is Nothing Then Set iai = New IaiCls
        iai.protSlashing = protSlashing
        iai.protPiercing = protPiercing
        iai.protBludgeoning = protBludgeoning
        iai.protFire = protFire
        iai.protAcid = protAcid
        iai.protCold = protCold
        iai.protElectrical = protElectrical
        
    End If
    If (flags And &H00000100&) <> 0 Then
        
    '   field: flags1, type: DWORD
        Dim t_flags1 As Long : t_flags1 = mbuf.Get_DWORD()
        
    '   field: health, type: DWORD
        Dim healthCur As Long : healthCur = mbuf.Get_DWORD()
        If cbi Is Nothing Then Set cbi = New CbiCls
        cbi.healthCur = healthCur
        
    '   field: healthMax, type: DWORD
        Dim healthMax As Long : healthMax = mbuf.Get_DWORD()
        If cbi Is Nothing Then Set cbi = New CbiCls
        cbi.healthMax = healthMax
        
    '   maskmap: flags1
        If (t_flags1 And &H00000008&) <> 0 Then
            
        '   field: strength, type: DWORD
            Dim sklvlStrength As Long : sklvlStrength = mbuf.Get_DWORD()
            
        '   field: endurance, type: DWORD
            Dim sklvlEndurance As Long : sklvlEndurance = mbuf.Get_DWORD()
            
        '   field: quickness, type: DWORD
            Dim sklvlQuickness As Long : sklvlQuickness = mbuf.Get_DWORD()
            
        '   field: coordination, type: DWORD
            Dim sklvlCoordination As Long : sklvlCoordination = mbuf.Get_DWORD()
            
        '   field: focus, type: DWORD
            Dim sklvlFocus As Long : sklvlFocus = mbuf.Get_DWORD()
            
        '   field: self, type: DWORD
            Dim sklvlSelf As Long : sklvlSelf = mbuf.Get_DWORD()
            
        '   field: stamina, type: DWORD
            Dim staminaCur As Long : staminaCur = mbuf.Get_DWORD()
            
        '   field: mana, type: DWORD
            Dim manaCurCreature As Long : manaCurCreature = mbuf.Get_DWORD()
            
        '   field: staminaMax, type: DWORD
            Dim staminaMax As Long : staminaMax = mbuf.Get_DWORD()
            
        '   field: manaMax, type: DWORD
            Dim manaMaxCreature As Long : manaMaxCreature = mbuf.Get_DWORD()
            If cai Is Nothing Then Set cai = New CaiCls
            cai.strength = sklvlStrength
            cai.endurance = sklvlEndurance
            cai.quickness = sklvlQuickness
            cai.coordination = sklvlCoordination
            cai.focus = sklvlFocus
            cai.self = sklvlSelf
            cai.staminaCur = staminaCur
            cai.staminaMax = staminaMax
            cai.manaCur = manaCurCreature
            cai.manaMax = manaMaxCreature
            
        End If
        If (t_flags1 And &H00000001&) <> 0 Then
            
        '   Skip: attrHighlight attrColor
            Call mbuf.SkipCb(4)
            
        End If
    End If
    If (flags And &H00000020&) <> 0 Then
        
    '   field: weapType, type: DWORD
        Dim dmty As Long : dmty = mbuf.Get_DWORD()
        
    '   field: weapSpeed, type: DWORD
        Dim speed As Long : speed = mbuf.Get_DWORD()
        
    '   field: weapSkill, type: DWORD
        Dim skidWeapon As Long : skidWeapon = mbuf.Get_DWORD()
        
    '   field: weapDamage, type: DWORD
        Dim dhealth As Long : dhealth = mbuf.Get_DWORD()
        
    '   field: weapVariance, type: double
        Dim scaleDamageRange As Double : scaleDamageRange = mbuf.Get_double()
        
    '   field: weapModifier, type: double
        Dim scaleDamageBonus As Double : scaleDamageBonus = mbuf.Get_double()
        
    '   Skip: weapUnknown1
        Call mbuf.SkipCb(8)
        
    '   field: weapPower, type: double
        Dim vLaunch As Double : vLaunch = mbuf.Get_double()
        
    '   field: weapAttack, type: double
        Dim scaleAttackBonus As Double : scaleAttackBonus = mbuf.Get_double()
        If iwi Is Nothing Then Set iwi = New IwiCls
        iwi.dmty = dmty
        iwi.speed = speed
        iwi.skid = skidWeapon
        iwi.dhealth = dhealth
        iwi.scaleDamageRange = scaleDamageRange
        iwi.scaleDamageBonus = scaleDamageBonus
        iwi.scaleAttackBonus = scaleAttackBonus
        iwi.fractUnknown = vLaunch
        
    '   Skip: weapUnknown3
        Call mbuf.SkipCb(4)
        
    End If
    If (flags And &H00000040&) <> 0 Then
        
    '   Skip: unknown40_1 unknown40_2 unknown40_3
        Call mbuf.SkipCb(12)
        
    End If
    If (flags And &H00000200&) <> 0 Then
        
    '   Skip: protHighlight protColor
        Call mbuf.SkipCb(4)
        
    End If
    If (flags And &H00000800&) <> 0 Then
        
    '   field: weapHighlight, type: WORD
        Dim dwHighlights As Long : dwHighlights = mbuf.Get_WORD()
        If iwi Is Nothing Then Set iwi = New IwiCls
        iwi.dwHighlights = dwHighlights
        
    End If
    
'   Fire script events.
    Dim oai As OaiCls
    Set oai = New OaiCls
    If Not (cbi Is Nothing) Then
        Call cbi.SetRaw(dictRaw)
        Call oai.FromCreature(fSuccess, cbi, cai, chi)
        Call aco.SetOai(oai)
        Call skev.RaiseOnAssessCreature(aco, fSuccess, cbi, cai, chi)
    Else
        If ibi Is Nothing Then Set ibi = New IbiCls
        ibi.oty = aco.oty
        ibi.flags = flags
        Select Case kindWieldReq
        Case 2
            ibi.skidWieldReq = idWieldReq
            ibi.lvlWieldReq = lvlWieldReq
        Case 7
            If idWieldReq = 1 Then
                ibi.skidWieldReq = skidNil
                ibi.lvlWieldReq = lvlWieldReq
            End If
        End Select
        If Not (iei Is Nothing) Then
            iei.fractEfficiency = ibi.fractEfficiency
        End If
        Call ibi.SetRaw(dictRaw)
        Call oai.FromItem(fSuccess, ibi, iai, iwi, iei, ipi)
        Call aco.SetOai(oai)
        Call skev.RaiseOnAssessItem(aco, fSuccess, ibi, iai, iwi, iei, ipi)
    End If
    
End Function

' Group Chat
Private Function DecodeF7B0_0147(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: type, type: DWORD
    Dim dwMask As Long : dwMask = mbuf.Get_DWORD()
    
'   field: senderName, type: String
    Dim szSender As String : szSender = mbuf.Get_String()
    
'   field: text, type: String
    Dim szMsg As String : szMsg = mbuf.Get_String()
    
'   Fire script events.
    Select Case dwMask
    Case &H00000800&	' @f: Fellowship broadcast
        Call skev.RaiseOnTellFellowship(szSender, szMsg)
    Case &H00001000&	' @v: Patron to vassal
        Call skev.RaiseOnTellPatron(szSender, szMsg)
    Case &H00002000&	' @p: Vassal to patron
        Call skev.RaiseOnTellVassal(szSender, szMsg)
    Case &H00004000&	' @m: Follower to monarch
        Call skev.RaiseOnTellFollower(szSender, szMsg)
    Case &H01000000&	' @c: Covassal broadcast
        Call skev.RaiseOnTellCovassal(szSender, szMsg)
    Case &H02000000&	' @a: Allegiance broadcast by monarch or speaker
        Call skev.RaiseOnTellAllegiance(szSender, szMsg)
    End Select
    
End Function

' Set Pack Contents
Private Function DecodeF7B0_0196(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: container, type: DWORD
    Dim oidPack As Long : oidPack = mbuf.Get_DWORD()
    Dim acoPack As AcoCls : Set acoPack = otable.AcoFromOid(oidPack, True, True)
    Call acoPack.SetFContainer(True)
    Call acoPack.RemoveAll
    
'   field: itemCount, type: DWORD
    Dim t_itemCount As Long : t_itemCount = mbuf.Get_DWORD()
    
'   vector: items, length: t_itemCount
    Dim iitem As Long
    For iitem = 0 To t_itemCount - 1
        
    '   field: item, type: DWORD
        Dim oidItem As Long : oidItem = mbuf.Get_DWORD()
        Dim acoItem As AcoCls : Set acoItem = otable.AcoFromOid(oidItem, True, True)
        
    '   field: type, type: DWORD
        Dim dwContainer As Long : dwContainer = mbuf.Get_DWORD()
        Dim fContainer As Boolean
        fContainer = (dwContainer <> False)
        
        Call acoItem.SetFContainer(fContainer)
        Call acoPack.InsertItem(iitem, acoItem)
        
    Next iitem
    
'   Fire script events.
    If plig = pligInWorld Then
        Call skev.RaiseOnOpenContainer(acoPack)
    End If
    
End Function

' Drop From Inventory
Private Function DecodeF7B0_019A(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: item, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   Apparently this message can be sent all by itself in the case of DIs
'   dropping directly on the ground if your toon doesn't leave a corpse.  In
'   that case fWasInInventory will be true, since we won't have seen a Set
'   Wielder/Container for this item, and we won't be seeing a Set Position And
'   Motion either, so this guy has to do it all.
    Dim fWasInInventory As Boolean, acoContainer As AcoCls, iitem As Long
    fWasInInventory = aco.fInInventory
    If fWasInInventory Then
        Set acoContainer = aco.acoContainer
        iitem = aco.iitem
        Call aco.SetAcoContainer(Nothing)
        Call aco.SetMaploc(otable.acoChar.maploc)
    End If
    
'   Fire script events.
    If fWasInInventory Then
        Call skev.RaiseOnRemoveFromInventory(aco, acoContainer, iitem)
    End If
    
End Function

' Delete Spell from Spellbook
Private Function DecodeF7B0_01A8(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: spell, type: WORD
    Dim spellid As Long : spellid = mbuf.Get_WORD()
    
    Call self.RemoveSpellid(-1, spellid)
    
End Function

' Kill Death Message
Private Function DecodeF7B0_01AD(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: text, type: String
    Dim szMsg As String : szMsg = mbuf.Get_String()
    
'   Fire script events.
    Call skev.RaiseOnDeathMessage(szMsg)
    
End Function

' Inflict Melee Damage
Private Function DecodeF7B0_01B1(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: target, type: String
    Dim szTarget As String : szTarget = mbuf.Get_String()
    
'   field: type, type: DWORD
    Dim dmty As Long : dmty = mbuf.Get_DWORD()
    
'   Skip: severity
    Call mbuf.SkipCb(8)
    
'   field: damage, type: DWORD
    Dim dhealth As Long : dhealth = mbuf.Get_DWORD()
    
'   Fire script events.
    Call skev.RaiseOnMeleeDamageOther(szTarget, dhealth, dmty)
    
End Function

' Receive Melee Damage
Private Function DecodeF7B0_01B2(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: attacker, type: String
    Dim szSource As String : szSource = mbuf.Get_String()
    
'   field: type, type: DWORD
    Dim dmty As Long : dmty = mbuf.Get_DWORD()
    
'   Skip: severity
    Call mbuf.SkipCb(8)
    
'   field: damage, type: DWORD
    Dim dhealth As Long : dhealth = mbuf.Get_DWORD()
    
'   field: location, type: DWORD
    Dim bpart As Long : bpart = mbuf.Get_DWORD()
    
'   Fire script events.
    Call skev.RaiseOnMeleeDamageSelf(szSource, dhealth, dmty, bpart)
    
End Function

' Other Melee Evade
Private Function DecodeF7B0_01B3(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: target, type: String
    Dim szTarget As String : szTarget = mbuf.Get_String()
    
'   Fire script events.
    Call skev.RaiseOnMeleeEvadeOther(szTarget)
    
End Function

' Self Melee Evade
Private Function DecodeF7B0_01B4(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: attacker, type: String
    Dim szSource As String : szSource = mbuf.Get_String()
    
'   Fire script events.
    Call skev.RaiseOnMeleeEvadeSelf(szSource)
    
End Function

' Update Health
Private Function DecodeF7B0_01C0(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   field: health, type: float
    Dim fractHealth As Single : fractHealth = mbuf.Get_float()
    
'   Fire script events.
    Call skev.RaiseOnTargetHealth(aco, fractHealth)
    
End Function

' Ready
Private Function DecodeF7B0_01C7(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Fire script events.
    Call skev.RaiseOnActionComplete()
    
End Function

' Enter Trade
Private Function DecodeF7B0_01FD(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: trader, type: DWORD
    Dim oidSender As Long : oidSender = mbuf.Get_DWORD()
    Dim acoSender As AcoCls : Set acoSender = otable.AcoFromOid(oidSender, True, True)
    
'   field: tradee, type: DWORD
    Dim oidReceiver As Long : oidReceiver = mbuf.Get_DWORD()
    Dim acoReceiver As AcoCls : Set acoReceiver = otable.AcoFromOid(oidReceiver, True, True)
    
    Call otable.ClearTradeItems()
    
'   Fire script events.
    Call skev.RaiseOnTradeStart(acoSender, acoReceiver)
    
End Function

' End Trade
Private Function DecodeF7B0_01FF(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: reason, type: DWORD
    Dim arc As Long : arc = mbuf.Get_DWORD()
    
'   Fire script events.
    Call skev.RaiseOnTradeEnd(arc)
    
    Call otable.ClearTradeItems()
    
End Function

' Add Trade Item
Private Function DecodeF7B0_0200(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: item, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   field: side, type: DWORD
    Dim side As Long : side = mbuf.Get_DWORD()
    
    Call otable.AddTradeItem(aco, side)
    
'   Fire script events.
    Call skev.RaiseOnTradeAdd(aco, side)
    
End Function

' Accept Trade
Private Function DecodeF7B0_0202(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: trader, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   Fire script events.
    Call skev.RaiseOnTradeAccept(aco)
    
End Function

' Reset Trade
Private Function DecodeF7B0_0205(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: trader, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   Fire script events.
    Call skev.RaiseOnTradeReset(aco)
    
    Call otable.ClearTradeItems()
    
End Function

' Update Item Mana Bar
Private Function DecodeF7B0_0264(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: item, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   field: mana, type: float
    Dim fractMana As Single : fractMana = mbuf.Get_float()
    
'   field: show, type: DWORD
    Dim fShow As Long : fShow = mbuf.Get_DWORD()
    
'   Fire script events.
    If fShow Then
        Call skev.RaiseOnItemManaBar(aco, fractMana)
    End If
    
End Function

' Confirmation Panel
Private Function DecodeF7B0_0274(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: type, type: DWORD
    Dim kind As Long : kind = mbuf.Get_DWORD()
    
'   Skip: number
    Call mbuf.SkipCb(4)
    
'   field: text, type: String
    Dim szArg As String : szArg = mbuf.Get_String()
    
'   Fire script events.
    Select Case kind
    Case 4
        Call skev.RaiseOnFellowInvite(szArg)
    Case 5
        Call skev.RaiseOnCraftConfirm(szArg)
    End Select
    
End Function

' Action Failure
Private Function DecodeF7B0_028A(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: type, type: DWORD
    Dim arc As Long : arc = mbuf.Get_DWORD()
    SkGlobals.arc = arc
    
'   Fire script events.
    Call skev.RaiseOnSpellFailSelf(arc)
    
End Function

' Tell
Private Function DecodeF7B0_02BD(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: text, type: String
    Dim szMsg As String : szMsg = mbuf.Get_String()
    
'   field: senderName, type: String
    Dim szSender As String : szSender = mbuf.Get_String()
    
'   field: sender, type: DWORD
    Dim oidSender As Long : oidSender = mbuf.Get_DWORD()
    Dim acoSender As AcoCls : Set acoSender = otable.AcoFromOid(oidSender, True, True)
    
'   field: target, type: DWORD
    Dim oidReceiver As Long : oidReceiver = mbuf.Get_DWORD()
    Dim acoReceiver As AcoCls : Set acoReceiver = otable.AcoFromOid(oidReceiver, True, True)
    
'   field: type, type: DWORD
    Dim tych As Long : tych = mbuf.Get_DWORD()
    
    Call acoSender.SetSzName(szSender)
    
'   Fire script events.
    Select Case tych
    Case tychBroadcast
        Call skev.RaiseOnTellServer(acoSender, acoReceiver, szMsg)
    Case tychPrivateTell, tychOutgoingTell
        Call skev.RaiseOnTell(acoSender, acoReceiver, szMsg)
    Case 6
        Call skev.RaiseOnTellMelee(acoSender, acoReceiver, szMsg)
    Case Else
        Call skev.RaiseOnTellMisc(acoSender, acoReceiver, szMsg, tych)
    End Select
    
End Function

' Create Fellowship
Private Function DecodeF7B0_02BE(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
    Call cofellow.Clear()
    
'   field: fellowCount, type: WORD
    Dim t_fellowCount As Long : t_fellowCount = mbuf.Get_WORD()
    
'   Skip: fellowUnknown
    Call mbuf.SkipCb(2)
    
'   vector: fellows, length: t_fellowCount
    Dim ifellow As Long
    For ifellow = 0 To t_fellowCount - 1
        
    '   field: fellow, type: FellowInfo
        Dim fellow As FellowCls
        Set fellow = DecodeFellowInfo(mbuf, oidChar, 0)
        Call cofellow.FAdd(fellow)
        
    Next ifellow
    
'   field: name, type: String
    Dim szName As String : szName = mbuf.Get_String()
    szFellowship = szName
    
'   field: leader, type: DWORD
    Dim oidLeader As Long : oidLeader = mbuf.Get_DWORD()
    Call cofellow.SetLeader(oidLeader)
    
'   Skip: unknown1
    Call mbuf.SkipCb(4)
    
'   field: shareXP, type: DWORD
    Dim fShareExp As Long : fShareExp = mbuf.Get_DWORD()
    
'   Fire script events.
    Call skev.RaiseOnFellowCreate(szName, fShareExp, oidLeader, cofellow)
    
End Function

' Disband Fellowship
Private Function DecodeF7B0_02BF(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Fire script events.
    Call skev.RaiseOnFellowDisband()
    
    szFellowship = ""
    Call cofellow.Clear()
    
End Function

' Add Fellowship Member
Private Function DecodeF7B0_02C0(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: fellow, type: FellowInfo
    Dim fellow As FellowCls
    Set fellow = DecodeFellowInfo(mbuf, oidChar, 0)
    
    Dim fNewFellow As Boolean
    fNewFellow = cofellow.FAdd(fellow)
    
'   Fire script events.
    If fNewFellow Then
        Call skev.RaiseOnFellowRecruit(fellow)
    Else
        Call skev.RaiseOnFellowUpdate(fellow)
    End If
    
End Function

' Add Spell to Spellbook
Private Function DecodeF7B0_02C1(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: spell, type: WORD
    Dim spellid As Long : spellid = mbuf.Get_WORD()
    
    Call self.AddSpellid(-1, spellid)
    
'   Fire script events.
    Call skev.RaiseOnSpellCastSelf(spellid)
    
End Function

' Add Character Enchantment
Private Function DecodeF7B0_02C2(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: enchantment, type: Enchantment
    Dim spell As SpellCls
    Set spell = DecodeEnchantment(mbuf, oidChar, 0)
    Call cospell.AddSpell(spell)
    
'   Fire script events.
    Call skev.RaiseOnSpellAdd(spell)
    
End Function

' Remove Character Enchantment
Private Function DecodeF7B0_02C3(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: spell, type: WORD
    Dim spellid As Long : spellid = mbuf.Get_WORD()
    
'   field: layer, type: WORD
    Dim layer As Long : layer = mbuf.Get_WORD()
    
    Call cospell.RemoveSpellid(spellid, layer)
    
'   Fire script events.
    Call skev.RaiseOnSpellExpire(spellid, layer)
    
End Function

' Remove Multiple Character Enchantments
Private Function DecodeF7B0_02C5(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: count, type: DWORD
    Dim cspell As Long : cspell = mbuf.Get_DWORD()
    Dim rgspellid() As Long, rglayer() As Long
    ReDim rgspellid(cspell - 1)
    ReDim rglayer(cspell - 1)
    
'   vector: enchantments, length: cspell
    Dim ispell As Long
    For ispell = 0 To cspell - 1
        
    '   field: spell, type: WORD
        Dim spellid As Long : spellid = mbuf.Get_WORD()
        
    '   field: layer, type: WORD
        Dim layer As Long : layer = mbuf.Get_WORD()
        
        rgspellid(ispell) = spellid
        rglayer(ispell) = layer
        
    Next ispell
    
'   Fire script events.
    For ispell = 0 To cspell - 1
        spellid = rgspellid(ispell)
        layer = rglayer(ispell)
        Call cospell.RemoveSpellid(spellid, layer)
        Call skev.RaiseOnSpellExpire(spellid, layer)
    Next ispell
    
End Function

' Remove Character Enchantment (Silent)
Private Function DecodeF7B0_02C7(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: spell, type: WORD
    Dim spellid As Long : spellid = mbuf.Get_WORD()
    
'   field: layer, type: WORD
    Dim layer As Long : layer = mbuf.Get_WORD()
    
    Call cospell.RemoveSpellid(spellid, layer)
    
'   Fire script events.
    Call skev.RaiseOnSpellExpireSilent(spellid, layer)
    
End Function

' Remove Multiple Character Enchantments (Silent)
Private Function DecodeF7B0_02C8(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: count, type: DWORD
    Dim cspell As Long : cspell = mbuf.Get_DWORD()
    Dim rgspellid() As Long, rglayer() As Long
    ReDim rgspellid(cspell - 1)
    ReDim rglayer(cspell - 1)
    
'   vector: enchantments, length: cspell
    Dim ispell As Long
    For ispell = 0 To cspell - 1
        
    '   field: spell, type: WORD
        Dim spellid As Long : spellid = mbuf.Get_WORD()
        
    '   field: layer, type: WORD
        Dim layer As Long : layer = mbuf.Get_WORD()
        
        rgspellid(ispell) = spellid
        rglayer(ispell) = layer
        
    Next ispell
    
'   Fire script events.
    For ispell = 0 To cspell - 1
        spellid = rgspellid(ispell)
        layer = rglayer(ispell)
        Call cospell.RemoveSpellid(spellid, layer)
        Call skev.RaiseOnSpellExpire(spellid, layer)
    Next ispell
    
End Function

' Update Object
Private Function DecodeF7DB(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: object, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
'   field: model, type: ModelData
    Dim t_model As Object
    Set t_model = DecodeModelData(mbuf, oidChar, 0)
    
'   field: physics, type: PhysicsData
    Dim t_physics As PhysicsDataCls
    Set t_physics = DecodePhysicsData(mbuf, oidChar, 0)
    
'   field: game, type: GameData
    Dim gd As GameDataCls
    Set gd = DecodeGameData(mbuf, oidChar, 0)
    Call aco.FromGameData(gd)
    
End Function

' Start 3D Mode
Private Function DecodeF7DF(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
    plig = pligInPortal
    fFirstSPMInPortal = True
'   Invalidate dxpRes and dypRes in case user did TweakAC when we weren't looking.
    Call acapp.InvalRes
    Call skapi.PrepForLogin()
    Call LogMemStats(mbuf.tick)
    tickDelOrphans = mbuf.tick
    
'   Fire script events.
    Call skev.RaiseOnStart3D()
    
End Function

' Server Message
Private Function DecodeF7E0(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: text, type: String
    Dim szMsg As String : szMsg = mbuf.Get_String()
    
'   field: type, type: DWORD
    Dim tych As Long : tych = mbuf.Get_DWORD()
    
'   Fire script events.
    Call skev.RaiseOnChatServer(szMsg, tych)
    
End Function

' Server Name
Private Function DecodeF7E1(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Skip: players unknown
    Call mbuf.SkipCb(8)
    
'   field: server, type: String
    Dim szServer As String : szServer = mbuf.Get_String()
'   Change case from all caps to initial cap.
    szWorld = UCase(Left(szServer, 1)) + LCase(Right(szServer, Len(szServer) - 1))
    Call TraceLine("szWorld = " + SzQuote(szWorld))
    Call FlushLog()
    
    plig = pligAtLogin
    
End Function

' CTS Update Single Option
Private Function DecodeF7B1_0005(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: key, type: DWORD
    Dim iflag As Long : iflag = mbuf.Get_DWORD()
    
'   field: value, type: DWORD
    Dim fVal As Long : fVal = mbuf.Get_DWORD()
    
    Call self.SetChopIflag(iflag, fVal)
    
End Function

' CTS Update Options
Private Function DecodeF7B1_01A1(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
    Call self.ResetSpelltabs()
    
'   field: options, type: CharacterOptionData
    Dim cod As Object
    Set cod = DecodeCharacterOptionData(mbuf, oidChar, 0)
    
End Function

'
Private Function DecodeF619(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF625(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF657(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF746(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF749(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF74E(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF750(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF755(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF7B0_0020(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF7B0_0052(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF7B0_00A0(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF7B0_00B4(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF7B0_00B8(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF7B0_01F4(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF7B0_0203(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF7B0_0207(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF7B0_021D(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF7B0_0257(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

'
Private Function DecodeF7B0_027A(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
End Function

' SkunkWorks Command
Private Function DecodeE000(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: szCmd, type: String
    Dim szCmd As String : szCmd = mbuf.Get_String()
    
'   Fire script events.
    Call skev.RaiseOnCommand(szCmd)
    
End Function

' SkunkWorks Control Event
Private Function DecodeE001(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
    Dim dictSzValue As Dictionary : Set dictSzValue = New Dictionary
    dictSzValue.CompareMode = TextCompare
    
'   field: szPanel, type: String
    Dim szPanel As String : szPanel = mbuf.Get_String()
    
'   field: szControlClicked, type: String
    Dim szControlClicked As String : szControlClicked = mbuf.Get_String()
    
'   field: ccontrol, type: DWORD
    Dim t_ccontrol As Long : t_ccontrol = mbuf.Get_DWORD()
    
'   vector: rgcontrol, length: t_ccontrol
    Dim icontrol As Long
    For icontrol = 0 To t_ccontrol - 1
        
    '   field: szControl, type: String
        Dim szControl As String : szControl = mbuf.Get_String()
        
    '   field: szValue, type: String
        Dim szValue As String : szValue = mbuf.Get_String()
        
        dictSzValue.Item(szControl) = szValue
        
    Next icontrol
    
'   Fire script events.
    Call skev.RaiseOnControlEvent(szPanel, szControlClicked, dictSzValue)
    
End Function

' SkunkWorks Leave Vendor
Private Function DecodeE002(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: oidVendor, type: DWORD
    Dim oidMerchant As Long : oidMerchant = mbuf.Get_DWORD()
    If oidMerchant <> oidNil And oidMerchant = otable.oidPx Then
        fPxThisMsg = True
    End If
    Dim acoMerchant As AcoCls : Set acoMerchant = otable.AcoFromOid(oidMerchant, True, True)
    
'   Fire script events.
    Call skev.RaiseOnLeaveVendor(acoMerchant)
    
End Function

' SkunkWorks Property Value
Private Function DecodeE003(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: szPanel, type: String
    Dim szPanel As String : szPanel = mbuf.Get_String()
    
'   field: szControl, type: String
    Dim szControl As String : szControl = mbuf.Get_String()
    
'   field: szProperty, type: String
    Dim szProperty As String : szProperty = mbuf.Get_String()
    
'   field: vValue, type: Variant
    Dim vValue As Variant : vValue = mbuf.Get_Variant()
    
'   Fire script events.
    Call skev.RaiseOnControlProperty(szPanel, szControl, szProperty, vValue)
    
End Function

' SkunkWorks Plugin Message
Private Function DecodeE004(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: szMsg, type: String
    Dim szMsg As String : szMsg = mbuf.Get_String()
    
'   Fire script events.
    Call skev.RaiseOnPluginMsg(szMsg)
    
End Function

' SkunkWorks Hotkey
Private Function DecodeE005(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: cmid, type: String
    Dim cmid As String : cmid = mbuf.Get_String()
    
'   field: vk, type: DWORD
    Dim vk As Long : vk = mbuf.Get_DWORD()
    
'   Fire script events.
    Call skev.RaiseOnHotkey(cmid, vk)
    
End Function

' SkunkWorks Nav Stop
Private Function DecodeE006(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: nsc, type: DWORD
    Dim nsc As Long : nsc = mbuf.Get_DWORD()
    
'   Fire script events.
    Call skev.RaiseOnNavStop(nsc)
    
End Function

' SkunkWorks Destroy ACOs
Private Function DecodeE007(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: coid, type: DWORD
    Dim t_coid As Long : t_coid = mbuf.Get_DWORD()
    
'   vector: rgoid, length: t_coid
    Dim ioid As Long
    For ioid = 0 To t_coid - 1
        
    '   field: oid, type: DWORD
        Dim oid As Long : oid = mbuf.Get_DWORD()
        If oid <> oidNil And oid = otable.oidPx Then
            fPxThisMsg = True
        End If
    '   No point implicitly creating an ACO just to destroy it.
        Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, False, False)
        If Not aco Is Nothing Then
            Call TraceLine("Client deleted ACO " + SzHex(oid, 8) + "  " + aco.szName)
            Call otable.DeleteAco(aco, False)
        Else
            Call DebugLine("Client deleted non-existent OID " + SzHex(oid, 8))
        End If
        
    Next ioid
    
End Function

' SkunkWorks Disconnect
Private Function DecodeE008(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   Fire script events.
    Call skev.RaiseOnDisconnect()
    
End Function

' SkunkWorks ChatParserIntercept
Private Function DecodeE009(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: szMsg, type: String
    Dim szMsg As String : szMsg = mbuf.Get_String()
    
'   field: cmc, type: DWORD
    Dim cmc As Long : cmc = mbuf.Get_DWORD()
    
'   Fire script events.
    Call skev.RaiseOnChatBoxMessage(szMsg, cmc)
    
End Function

' SkunkWorks StatusTextIntercept
Private Function DecodeE00A(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: szMsg, type: String
    Dim szMsg As String : szMsg = mbuf.Get_String()
    
'   Fire script events.
    Call skev.RaiseOnTipMessage(szMsg)
    
End Function

' SkunkWorks Open Container Panel
Private Function DecodeE00B(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: oidContainer, type: DWORD
    Dim oidContainer As Long : oidContainer = mbuf.Get_DWORD()
    If oidContainer <> oidNil And oidContainer = otable.oidPx Then
        fPxThisMsg = True
    End If
    Dim acoContainer As AcoCls : Set acoContainer = otable.AcoFromOid(oidContainer, True, True)
    
'   Fire script events.
    Call skev.RaiseOnOpenContainerPanel(acoContainer)
    
End Function

' SkunkWorks Close Container
Private Function DecodeE00C(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: oidContainer, type: DWORD
    Dim oidContainer As Long : oidContainer = mbuf.Get_DWORD()
    If oidContainer <> oidNil And oidContainer = otable.oidPx Then
        fPxThisMsg = True
    End If
    Dim acoContainer As AcoCls : Set acoContainer = otable.AcoFromOid(oidContainer, True, True)
    
'   Fire script events.
    Call skev.RaiseOnCloseContainer(acoContainer)
    
End Function

' SkunkWorks ChatClickIntercept
Private Function DecodeE00D(ByVal mbuf As MbufCls, ByVal oidChar as Long) As Long
    
'   field: szName, type: String
    Dim szName As String : szName = mbuf.Get_String()
    
'   field: oid, type: DWORD
    Dim oid As Long : oid = mbuf.Get_DWORD()
    Dim aco As AcoCls : Set aco = otable.AcoFromOid(oid, True, True)
    
    Call aco.SetSzName(szName)
    
'   Fire script events.
    Call skev.RaiseOnChatBoxClick(aco)
    
End Function


