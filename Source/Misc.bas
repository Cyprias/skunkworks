Attribute VB_Name = "Misc"
Option Explicit


Public fUnsignedLong As Boolean


Private Declare Function VariantChangeType Lib "oleaut32" _
  (varDst As Variant, varSrc As Variant, ByVal flags As Long, ByVal vt As Long) As Long
Private Const vbUlong = vbByte + 2



Public Function LFromV(v As Variant) As Long
    
'   Watch out for unsigned variants being passed in from JScript.
    Select Case VarType(v)
    Case vbLong
        LFromV = v
    Case vbUlong
        If (v And &H80000000) = 0 Then
            LFromV = CLng(v)
        Else
            LFromV = CLng(v And &H7FFFFFFF) Or &H80000000
        End If
    Case vbSingle, vbDouble
    '   Apparently JScript passes in numbers >= 0x80000000 as floats.
        If v >= 2147483648# And v <= 4294967295# Then
            LFromV = CLng(v - 2147483648#) Or &H80000000
        Else
        '   Out of range; go ahead and overflow.
            LFromV = v
        End If
    Case Else
        Dim vSigned As Variant
        If VariantChangeType(vSigned, v, 0, vbLong) = 0 Then
            LFromV = vSigned
        Else
        '   Uncoercible; go ahead and overflow.
            LFromV = v
        End If
    End Select
    
End Function

Public Function VFromL(ByVal l As Long) As Variant
    
    VFromL = l
    
    If fUnsignedLong Then
    '   Convert to unsigned for passing back to JScript.
        Dim vUnsigned As Variant
        Call VariantChangeType(vUnsigned, VFromL, 0, 19)
        VFromL = vUnsigned
    End If
    
End Function

' We've been given an unsigned DWORD potentially exceeding 2147483647.
' To VB it looks like a signed long.  Convert to unsigned Double.
Public Function UDblFromDw(ByVal dw As Long) As Double
    
    UDblFromDw = dw
    If (dw And &H80000000) <> 0 Then
        UDblFromDw = UDblFromDw + 4294967296#
    End If
    
End Function

Public Function LSubNoOflow(ByVal l1 As Long, ByVal l2 As Long) As Long
    
    On Error Resume Next
    Call Err.Clear
    
    LSubNoOflow = l1 - l2
    
    If Err.Number = 6 Then  ' overflow
        LSubNoOflow = ((l1 Xor &H80000000) - l2) Xor &H80000000
    ElseIf Err.Number <> 0 Then
    '   Pass it on.
        Call Err.Raise(Err.Number, Err.Source, Err.Description)
    End If
    
End Function

Public Function LExtractMask(ByVal l As Long, ByVal mask As Long) As Long
    
    Do While (mask And 1) = 0
        l = l \ 2
        mask = mask \ 2
    Loop
    
    LExtractMask = (l And mask)
    
End Function









