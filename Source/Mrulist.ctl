VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.UserControl MrulistCtl 
   ClientHeight    =   315
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4800
   ScaleHeight     =   21
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   320
   Begin VB.ComboBox comboFilename 
      Height          =   315
      Left            =   0
      OLEDropMode     =   1  'Manual
      TabIndex        =   0
      Top             =   0
      Width           =   4095
   End
   Begin VB.CommandButton cmdBrowse 
      Caption         =   "..."
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   4080
      TabIndex        =   1
      Top             =   0
      Width           =   330
   End
   Begin MSComDlg.CommonDialog comdlg 
      Left            =   4200
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "MrulistCtl"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' MRU dropdown list of filenames.


'--------------
' Private data
'--------------

Private szAppMbr As String
Private szTitleMbr As String
Private Const szTitleDefault As String = "Open"
Private szFilterMbr As String
Private Const szFilterDefault As String = "All files|*.*"

Private Const cszFileMbr As Long = 10
Private rgszFileMbr(cszFileMbr - 1) As String
Private fRestored As Boolean

Private fInRestoreSettings As Boolean

Private Const vbDropEffectShortcut = 4


'-----------------
' Outgoing events
'-----------------

Public Event Change(ByVal szFile As String)


'------------
' Properties
'------------

Public Property Get szApp() As String
    
    szApp = szAppMbr
    
End Property

Public Property Let szApp(ByVal szNew As String)
    
    szAppMbr = szNew
    
End Property


Public Property Get szTitle() As String
    
    szTitle = szTitleMbr
    
End Property

Public Property Let szTitle(ByVal szNew As String)
    
    szTitleMbr = szNew
    
End Property


Public Property Get szFilter() As String
    
    szFilter = szFilterMbr
    
End Property

Public Property Let szFilter(ByVal szNew As String)
    
    szFilterMbr = szNew
    
End Property


Public Property Get szFile() As String
    
    szFile = comboFilename.Text
    
End Property

Public Property Let szFile(ByVal szNew As String)
    
    Call SetSzFile(szNew)
    
End Property


Public Property Get rgszFile(ByVal isz As Long) As String
    
    If Not fRestored Then Call RestoreSettings
    
    If isz < cszFileMbr Then
        rgszFile = rgszFileMbr(isz)
    Else
        rgszFile = ""
    End If
    
End Property

Public Property Get cszFile() As Long
    
    cszFile = cszFileMbr
    
End Property


Public Property Get Enabled() As Boolean
Attribute Enabled.VB_UserMemId = -514
    Enabled = UserControl.Enabled
End Property

Public Property Let Enabled(ByVal fNew As Boolean)
    UserControl.Enabled = fNew
    comboFilename.Enabled = fNew
    cmdBrowse.Enabled = fNew
    Call PropertyChanged("Enabled")
End Property


'---------
' Methods
'---------

Public Sub Browse()
    
    Call cmdBrowse_Click
    
End Sub

Public Sub Clear()
    
    Call comboFilename.Clear
    
    Dim isz As Long
    For isz = 0 To cszFileMbr - 1
        rgszFileMbr(isz) = ""
    Next isz
    
    Call SaveSettings
    
    RaiseEvent Change("")
    
End Sub


'----------
' Controls
'----------

Private Sub UserControl_Initialize()
    
    szAppMbr = App.EXEName
    szTitleMbr = szTitleDefault
    szFilterMbr = szFilterDefault
    fRestored = False
    
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    
    szAppMbr = PropBag.ReadProperty("szApp", App.EXEName)
    szTitleMbr = PropBag.ReadProperty("szTitle", szTitleDefault)
    szFilterMbr = PropBag.ReadProperty("szFilter", szFilterDefault)
    Enabled = PropBag.ReadProperty("Enabled", True)
    
    If Ambient.UserMode Then Call RestoreSettings
    
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    
    Call PropBag.WriteProperty("szApp", szAppMbr, App.EXEName)
    Call PropBag.WriteProperty("szTitle", szTitleMbr, szTitleDefault)
    Call PropBag.WriteProperty("szFilter", szFilterMbr, szFilterDefault)
    Call PropBag.WriteProperty("Enabled", Enabled, True)
    
End Sub

Private Sub UserControl_Resize()
    
    Dim xButton As Long
    xButton = ScaleWidth - cmdBrowse.Width
    Call comboFilename.Move(0, 0, xButton - 2)
    Call cmdBrowse.Move(xButton, 0, cmdBrowse.Width, comboFilename.Height)
    
End Sub


Private Sub comboFilename_DropDown()
    
    If Not fRestored Then Call RestoreSettings
    
    Dim szSave As String
    szSave = comboFilename.Text
    
    Call comboFilename.Clear
    
    Dim isz As Long
    For isz = 0 To cszFileMbr - 1
        If Len(rgszFileMbr(isz)) = 0 Then Exit For
        Call comboFilename.AddItem(rgszFileMbr(isz))
    Next isz
    
    comboFilename.Text = szSave
    
End Sub

Private Sub comboFilename_Click()
    
    If comboFilename.Text <> "" Then
        If fso.FileExists(comboFilename.Text) Then
            Call comboFilename_Validate(False)
        End If
    End If
    
End Sub

Private Sub comboFilename_OLEDragOver(Data As DataObject, Effect As Long, _
  Button As Integer, Shift As Integer, X As Single, Y As Single, State As Integer)
    
    If Data.GetFormat(vbCFFiles) Then
        Effect = Effect And vbDropEffectShortcut
    Else
        Effect = vbDropEffectNone
    End If
    
End Sub

Private Sub comboFilename_OLEDragDrop(Data As DataObject, Effect As Long, _
  Button As Integer, Shift As Integer, X As Single, Y As Single)
    
    If Data.GetFormat(vbCFFiles) Then
        Effect = Effect And vbDropEffectShortcut
        comboFilename.Text = Data.Files.Item(1)
        If fso.FileExists(comboFilename.Text) Then
            Call comboFilename_Validate(False)
        End If
    Else
        Effect = vbDropEffectNone
    End If
    
End Sub

Private Sub comboFilename_Validate(Cancel As Boolean)
    
    If comboFilename.Text <> "" Then
        If Not fso.FileExists(comboFilename.Text) Then
            Call MsgBox("No such file: " + comboFilename.Text, _
              vbExclamation + vbOKOnly)
            Cancel = True
            Exit Sub
        End If
        
        Call SetSzFile(fso.GetAbsolutePathName(comboFilename.Text))
    Else
        RaiseEvent Change("")
    End If
    
End Sub


Private Sub cmdBrowse_Click()
    
    Dim szFile As String
    comdlg.DialogTitle = szTitleMbr
    If Not FGetSzFileOpen(szFile, comdlg, szFilterMbr, "", _
      SzDirFromPath(comboFilename.Text)) Then
        Exit Sub
    End If
    
    comboFilename.Text = szFile
    Call comboFilename.SetFocus
    Call comboFilename_Validate(False)
    
End Sub


'----------
' Privates
'----------

Private Sub SetSzFile(ByVal sz As String)
    
    Debug.Assert sz <> ""
    
    If Not fRestored Then Call RestoreSettings
    
    comboFilename.Text = sz
    
    Dim szInsert As String
    szInsert = sz
    
    Dim isz As Long
    For isz = 0 To cszFileMbr - 1
        If Len(rgszFileMbr(isz)) = 0 Or _
          FEqSzFile(rgszFileMbr(isz), sz) Then
            rgszFileMbr(isz) = szInsert
            Exit For
        Else
            Dim szT As String
            szT = rgszFileMbr(isz)
            rgszFileMbr(isz) = szInsert
            szInsert = szT
        End If
    Next isz
    
    Call SaveSettings
    
    RaiseEvent Change(sz)
    
End Sub

Private Sub SaveSettings()
    
    If fInRestoreSettings Then Exit Sub
    
    Dim isz As Long
    For isz = 0 To cszFileMbr - 1
        Dim sz As String
        sz = rgszFileMbr(isz)
        Call SaveSetting(szAppMbr, UserControl.Extender.Name, "File " + CStr(isz), sz)
    Next isz
    
End Sub

Private Sub RestoreSettings()
    fInRestoreSettings = True
    
    Dim iszDst As Long
    iszDst = 0
    
    Dim isz As Long
    For isz = 0 To cszFileMbr - 1
        Dim sz As String
        sz = GetSetting(szAppMbr, UserControl.Extender.Name, "File " + CStr(isz), "")
        If sz <> "" Then
            If FFileExists(sz) Then
                rgszFileMbr(iszDst) = sz
                iszDst = iszDst + 1
            End If
        End If
    Next isz
    
    fRestored = True
    
    fInRestoreSettings = False
End Sub


























