VERSION 5.00
Begin VB.Form formMsgBox 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Message"
   ClientHeight    =   2070
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8070
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "MsgBox.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2070
   ScaleWidth      =   8070
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CheckBox checkDontAsk 
      Caption         =   "Don't ask again"
      Height          =   255
      Left            =   960
      TabIndex        =   2
      Top             =   960
      Width           =   3975
   End
   Begin VB.CommandButton rgcmd 
      Caption         =   "Command1"
      Height          =   375
      Index           =   0
      Left            =   3240
      TabIndex        =   3
      Top             =   1440
      Width           =   1095
   End
   Begin VB.PictureBox picIcon 
      AutoRedraw      =   -1  'True
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   240
      ScaleHeight     =   32
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   32
      TabIndex        =   1
      Top             =   360
      Width           =   480
   End
   Begin VB.Label labelMsg 
      AutoSize        =   -1  'True
      Height          =   195
      Left            =   960
      TabIndex        =   0
      Top             =   240
      Width           =   6855
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "formMsgBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


'--------------
' Private data
'--------------

Private ibuttonClicked As Long


'---------
' Methods
'---------

Public Sub Alert(ByVal szMsg As String, _
  Optional ByVal idIcon As Long = IDI_INFORMATION, _
  Optional formOwner As Form = Nothing)
    
    Call IbuttonMsgBox(szMsg, "OK", 0, , idIcon, , , formOwner)
    
End Sub

Public Function FConfirmYesNo(ByVal szMsg As String, _
  Optional fDefault As Boolean = True, _
  Optional ByVal idIcon As Long = IDI_QUESTION, _
  Optional formOwner As Form = Nothing) As Boolean
    
    FConfirmYesNo = (IbuttonMsgBox(szMsg, "Yes|No", IIf(fDefault, 0, 1), , _
      idIcon, , , formOwner) = 0)
    
End Function

Public Function FConfirmOKCancel(ByVal szMsg As String, _
  Optional fDefault As Boolean = True, _
  Optional ByVal idIcon As Long = IDI_QUESTION, _
  Optional formOwner As Form = Nothing) As Boolean
    
    FConfirmOKCancel = (IbuttonMsgBox(szMsg, "OK|Cancel", IIf(fDefault, 0, 1), , _
      idIcon, , , formOwner) = 0)
    
End Function

Public Function IbuttonMsgBox(ByVal szMsg As String, ByVal szButtons As String, _
  Optional ByVal ibuttonDefault As Long = 0, _
  Optional ByVal szTitle As String = "", _
  Optional ByVal idIcon As Long = IDI_APPLICATION, _
  Optional ByVal fShowDontAsk As Boolean = False, _
  Optional ByRef fDontAskOut As Boolean, _
  Optional formOwner As Form = Nothing) As Long
    
    If szTitle = "" Then szTitle = swoptions.szAppName
    Caption = szTitle
    
    Dim hicon As Long
    hicon = LoadStdIcon(hNil, idIcon)
    picIcon.Picture = LoadPicture("")
    Call DrawIcon(picIcon.hdc, 0, 0, hicon)
    
    Dim dyBetween As Long, dyLine As Long
    dyBetween = labelMsg.Top
    dyLine = labelMsg.Height
    
    labelMsg.Caption = szMsg
    
    If labelMsg.Height = dyLine Then labelMsg.WordWrap = False
    
    Dim yNext As Long
    yNext = labelMsg.Top + labelMsg.Height
    
    If yNext < dyBetween + picIcon.Height Then
        picIcon.Top = dyBetween
        yNext = dyBetween + picIcon.Height
        labelMsg.Top = (dyBetween + yNext - labelMsg.Height) / 2
    Else
        picIcon.Top = (dyBetween + yNext - picIcon.Height) / 2
    End If
    
    checkDontAsk.Value = vbUnchecked
    checkDontAsk.Visible = fShowDontAsk
    If fShowDontAsk Then
        yNext = yNext + dyBetween
        checkDontAsk.Top = yNext
        yNext = yNext + checkDontAsk.Height
    End If
    
    Dim cbutton As Long, szButton As String
    cbutton = 0
    Do While szButtons <> ""
        Call SplitSz(szButton, szButtons, szButtons, "|")
        If cbutton > 0 Then Call Load(rgcmd(cbutton))
        rgcmd(cbutton).Caption = szButton
        cbutton = cbutton + 1
    Loop
    
    Dim dxButton As Long, dxButtons As Long
    Const dxBetween As Long = 120
    dxButton = rgcmd(0).Width
    dxButtons = cbutton * dxButton + (cbutton - 1) * dxBetween
    
    Dim dxMargin As Long, dxTotal As Long
    dxMargin = picIcon.Left
    dxTotal = labelMsg.Left + labelMsg.Width + dxMargin
    If dxTotal < dxButtons + 2 * dxMargin Then
        dxTotal = dxButtons + 2 * dxMargin
    End If
    
    Dim xFirst As Long
    xFirst = (dxTotal - dxButtons) / 2
    yNext = yNext + dyBetween
    
    Dim ibutton As Long
    For ibutton = 0 To cbutton - 1
        Call rgcmd(ibutton).Move(xFirst + ibutton * (dxButton + dxBetween), _
          yNext)
        rgcmd(ibutton).Default = (ibutton = ibuttonDefault)
        rgcmd(ibutton).Visible = True
    Next ibutton
    yNext = yNext + rgcmd(0).Height
    
    yNext = yNext + dyBetween
    Dim dxBorders As Long, dyBorders As Long
    dxBorders = Width - ScaleX(ScaleWidth, ScaleMode, vbTwips)
    dyBorders = Height - ScaleY(ScaleHeight, ScaleMode, vbTwips)
    Width = ScaleX(dxTotal, ScaleMode, vbTwips) + dxBorders
    Height = ScaleY(yNext, ScaleMode, vbTwips) + dyBorders
    
    Call PlaySystemSound(idIcon)
    
    Dim ipointerPrev As Long
    ipointerPrev = Screen.MousePointer
    Screen.MousePointer = vbDefault
    
    ibuttonClicked = iNil
    Call Show(vbModal, formOwner)
    
    Screen.MousePointer = ipointerPrev
    
    fDontAskOut = (checkDontAsk.Value = vbChecked)
    IbuttonMsgBox = ibuttonClicked
    
    Call Unload(Me)
    
End Function

Public Sub PlaySystemSound(ByVal idIcon As Long)
    
    Dim szSound As String
    Select Case idIcon
    Case IDI_QUESTION
        szSound = "SystemQuestion"
    Case IDI_INFORMATION
        szSound = "SystemAsterisk"
    Case IDI_EXCLAMATION
        szSound = "SystemExclamation"
    Case IDI_ERROR
        szSound = "SystemHand"
    Case Else
        szSound = "SystemDefault"
    End Select
    
    Call PlaySound(szSound, 0, SND_APPLICATION Or SND_ASYNC)
    
End Sub


'----------
' Controls
'----------

Private Sub picIcon_KeyPress(KeyAscii As Integer)
    Call KeyCommand(Chr(KeyAscii))
End Sub

Private Sub rgcmd_KeyPress(Index As Integer, KeyAscii As Integer)
    Call KeyCommand(Chr(KeyAscii))
End Sub

Private Sub rgcmd_Click(Index As Integer)
    
    ibuttonClicked = Index
    Call Hide
    
End Sub

' Catch the case where the user clicks the Close box instead of one of the buttons.
Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    
    If UnloadMode = vbFormControlMenu Then
        ibuttonClicked = iNil
        Call Hide
        Cancel = True
    End If
    
End Sub


'----------
' Privates
'----------

Private Sub KeyCommand(ByVal szKey As String)
    
    Dim icmd As Integer
    For icmd = 0 To rgcmd.UBound
        If FEqSzI(szKey, Left(rgcmd(icmd).Caption, 1)) Then
            Call rgcmd_Click(icmd)
            Exit For
        End If
    Next icmd
    
End Sub























