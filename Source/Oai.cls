VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OaiCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' OnAssess Info


'--------------
' Private data
'--------------

Private fSuccessMbr As Boolean
Private cbiMbr As CbiCls
Private caiMbr As CaiCls
Private chiMbr As ChiCls
Private ibiMbr As IbiCls
Private iaiMbr As IaiCls
Private iwiMbr As IwiCls
Private ieiMbr As IeiCls
Private ipiMbr As IpiCls
Private tickMbr As Long


'------------
' Properties
'------------

Public Property Get fSuccess() As Boolean
    fSuccess = fSuccessMbr
End Property


Public Property Get cbi() As CbiCls
    Set cbi = cbiMbr
End Property

Public Property Get cai() As CaiCls
    Set cai = caiMbr
End Property

Public Property Get chi() As ChiCls
    Set chi = chiMbr
End Property


Public Property Get ibi() As IbiCls
    Set ibi = ibiMbr
End Property

Public Property Get iai() As IaiCls
    Set iai = iaiMbr
End Property

Public Property Get iwi() As IwiCls
    Set iwi = iwiMbr
End Property

Public Property Get iei() As IeiCls
    Set iei = ieiMbr
End Property

Public Property Get ipi() As IpiCls
    Set ipi = ipiMbr
End Property


Public Property Get cmsecSinceAssess() As Long
    cmsecSinceAssess = LSubNoOflow(GetTickCount(), tickMbr)
End Property


'------------------
' Friend functions
'------------------

Friend Sub FromCreature(ByVal fSuccess As Boolean, _
  ByVal cbi As CbiCls, ByVal cai As CaiCls, ByVal chi As ChiCls)
    
    fSuccessMbr = fSuccess
    Set cbiMbr = cbi
    Set caiMbr = cai
    Set chiMbr = chi
    tickMbr = GetTickCount()
    
End Sub

Friend Sub FromItem(ByVal fSuccess As Boolean, _
  ByVal ibi As IbiCls, ByVal iai As IaiCls, ByVal iwi As IwiCls, _
  ByVal iei As IeiCls, ByVal ipi As IpiCls)
    
    fSuccessMbr = fSuccess
    Set ibiMbr = ibi
    Set iaiMbr = iai
    Set iwiMbr = iwi
    Set ieiMbr = iei
    Set ipiMbr = ipi
    tickMbr = GetTickCount()
    
End Sub









