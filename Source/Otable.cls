VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "OtableCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Option Compare Text
' This class implements the main object table in which ACOs are stored and
' looked up.  It is NOT exposed directly to the script, but is accessed via
' methods of the skapi object.


'--------------
' Private data
'--------------

Private dictOidAco As Dictionary

Private cpackMbr As Long
Private rgacoPack(7) As AcoCls

Private coacoEquippedMbr As CoacoCls


'------------
' Properties
'------------

' Get/set the object representing the logged-in character.
Public acoChar As AcoCls

Public coacoTradeLeft As CoacoCls
Public coacoTradeRight As CoacoCls

' Debugging aid: set this variable to the OID of an object you want to
' track, and MessageCrackers.bas will hex-dump to the debug log all
' messages pertaining to that object.
Public oidPx As Long

' Return the number of packs (including the main pack) currently being carried
' by the logged-in character.
Public Property Get cpack() As Long
    cpack = cpackMbr
End Property

Public Property Get caco() As Long
    caco = dictOidAco.count
End Property

Public Property Get rgaco() As Variant
    rgaco = dictOidAco.Items
End Property


'---------
' Methods
'---------

Public Sub PrepForLogin()
    
    Call DeleteAllAcos
    
End Sub

' Return the ACO representing the pack at position ipack (where ipack = 0
' is the main pack).
Public Function AcoFromIpack(ByVal ipack As Long) As AcoCls
    
    If ipack < cpackMbr Then
        Set AcoFromIpack = rgacoPack(ipack)
    Else
        Set AcoFromIpack = Nothing
    End If
    
End Function

' Return the ACO representing the item at position iitem in pack ipack.
Public Function AcoFromIpackIitem(ByVal ipack As Long, ByVal iitem As Long) As AcoCls
    
    If ipack < cpackMbr Then
        Set AcoFromIpackIitem = rgacoPack(ipack).AcoFromIitem(iitem)
    Else
        Set AcoFromIpackIitem = Nothing
    End If
    
End Function

' Return the ACO corresponding to the given object ID.  If no such object exists,
' and fCreate is False, return Nothing; if fCreate is True, create an ACO and
' return it.  If fContainer is True, the created object will be a container.
Public Function AcoFromOid(ByVal oid As Long, _
  Optional ByVal FCreate As Boolean = False, _
  Optional ByVal fServerRef As Boolean = False) As AcoCls
    
    Dim aco As AcoCls
    If oid = oidNil Then
        Set aco = Nothing
    ElseIf dictOidAco.Exists(oid) Then
        Set aco = dictOidAco.Item(oid)
    ElseIf FCreate Then
        Set aco = New AcoCls
        Call aco.Init(oid)
        Set dictOidAco.Item(oid) = aco
    Else
        Set aco = Nothing
    End If
    
    If oidPx <> oidNil And oid = oidPx And fServerRef Then
        fPxThisMsg = True
    End If
    
    Set AcoFromOid = aco
End Function

' Search the object table for an object with the specified name.  If more
' than one such object exists, return an arbitrary one from that set.  If
' none exists, return Nothing.
Public Function AcoFromSz(ByVal sz As String) As AcoCls
    
    Set AcoFromSz = Nothing
    
    Dim rgaco As Variant, iaco As Long
    rgaco = dictOidAco.Items
    For iaco = 0 To UBound(rgaco)
        Dim aco As AcoCls: Set aco = rgaco(iaco)
        
        If aco.szName Like sz Then
            Set AcoFromSz = aco
            Exit For
        End If
        
    Next iaco
    
End Function

' Search the logged-in character's inventory for an object with the specified
' name.  If acoSkip is specified, then skip over that ACO when searching, i.e. do
' not consider it as a candidate for matching.  If more than one matching object
' exists in inventory, return an arbitrary one from that set (but not the one
' specified by acoSkip).  If none exists (aside from acoSkip), return Nothing.
Public Function AcoFindInInv(ByVal szItem As String, ByVal acoSkip As AcoCls) As AcoCls
    
    Dim ipack As Long, iitem As Long
    For ipack = 0 To cpackMbr - 1
        Dim acoPack As AcoCls
        Set acoPack = rgacoPack(ipack)
        For iitem = 0 To acoPack.citemContents - 1
            Dim acoItem As AcoCls
            Set acoItem = acoPack.AcoFromIitem(iitem)
            If acoItem.szName Like szItem And _
              Not (acoItem Is acoSkip) Then
                Set AcoFindInInv = acoItem
                Exit Function
            End If
        Next iitem
    Next ipack
    
    Set AcoFindInInv = Nothing
    
End Function

' Return the total number of items of the specified kind in the logged-in
' character's inventory.
Public Function CitemOfKindInInv(ByVal szItem As String) As Long
    
    Dim citem As Long
    citem = 0
    
    Dim ipack As Long, iitem As Long
    For ipack = 0 To cpackMbr - 1
        Dim acoPack As AcoCls
        Set acoPack = rgacoPack(ipack)
        For iitem = 0 To acoPack.citemContents - 1
            Dim acoItem As AcoCls
            Set acoItem = acoPack.AcoFromIitem(iitem)
            If acoItem.szName Like szItem Then
                citem = citem + acoItem.citemStack
            End If
        Next iitem
    Next ipack
    
    CitemOfKindInInv = citem
    
End Function


' Insert a pack at position ipack in the collection of packs.
Public Sub InsertPack(ByVal ipack As Long, ByVal acoPack As AcoCls)
    
    Call Assert(cpackMbr < 8, "Too many packs.")
    
    Dim ipackT As Long, acoT As AcoCls
    For ipackT = cpackMbr To ipack + 1 Step -1
        Set acoT = rgacoPack(ipackT - 1)
        Set rgacoPack(ipackT) = acoT
        Call acoT.SetIitem(acoChar, ipackT)
    Next ipackT
    cpackMbr = cpackMbr + 1
    
    Set rgacoPack(ipack) = acoPack
    Call acoPack.SetIitem(acoChar, ipack)
    
End Sub

Public Sub RemovePack(ByVal ipack As Long)
    
    Dim acoPack As AcoCls
    Set acoPack = rgacoPack(ipack)
    
    Dim ipackT As Long, acoT As AcoCls
    For ipackT = ipack To cpackMbr - 2
        Set acoT = rgacoPack(ipackT + 1)
        Set rgacoPack(ipackT) = acoT
        Call acoT.SetIitem(acoChar, ipackT)
    Next ipackT
    cpackMbr = cpackMbr - 1
    Set rgacoPack(cpackMbr) = Nothing
    
    Call acoPack.SetIitem(Nothing, iNil)
    
End Sub


' A Create Object message has been processed for the given ACO.  Mark the object
' complete and decrement the count of pending objects as appropriate.
Public Sub NoteAcoCreated(ByVal aco As AcoCls)
    
    Call aco.ClearFPending
    
End Sub

'Public Sub CheckPendingInventory()
'
'    If (cstm And cstmFullInventory) <> 0 Then Exit Sub
'    If plig < pligInWorld Then Exit Sub
'
'    Dim ipack As Long, iitem As Long
'    For ipack = 0 To cpackMbr - 1
'        Dim acoPack As AcoCls
'        Set acoPack = rgacoPack(ipack)
'        If acoPack.fPending Then Exit Sub
'
'        For iitem = 0 To acoPack.citemContents - 1
'            Dim acoItem As AcoCls
'            Set acoItem = acoPack.AcoFromIitem(iitem)
'            If acoItem.fPending Then Exit Sub
'        Next iitem
'    Next ipack
'
'    cstm = cstm Or cstmFullInventory
'
'End Sub


Public Sub RemoveFromInventory(ByVal aco As AcoCls)
    
    If aco.fContainer Then
        Call RemovePack(aco.iitem)
    Else
        Call aco.Remove
    End If
    
End Sub


Public Sub AddEquipment(ByVal aco As AcoCls)
    
    Call coacoEquippedMbr.AddAcoF(aco)
    
End Sub

Public Sub RemoveEquipment(ByVal aco As AcoCls)
    
    Call coacoEquippedMbr.RemoveAcoF(aco)
    
End Sub

' Return any equipped item covering any part of the given equipment mask.
Public Function AcoFromEqm(ByVal eqmFind As Long) As AcoCls
    
    Dim aco As AcoCls
    For Each aco In coacoEquippedMbr
        If (aco.eqmWearer And eqmFind) <> 0 Then
            Set AcoFromEqm = aco
            Exit Function
        End If
    Next aco
    
    Set AcoFromEqm = Nothing
End Function

' Return a collection of all equipped items covering any part of the given equipment mask.
' Pass eqmAll as eqmFind to get all equipped items.
Public Function CoacoFromEqm(ByVal eqmFind As Long) As CoacoCls
    
    Dim coaco As CoacoCls
    Set coaco = New CoacoCls
    Call coaco.SetMutable
    
    Dim aco As AcoCls
    For Each aco In coacoEquippedMbr
        If (aco.eqmWearer And eqmFind) <> 0 Then
            Call coaco.AddAcoF(aco)
        End If
    Next aco
    
    Set CoacoFromEqm = coaco
End Function

' Return an equipment mask indicating what clothing, armor, and weapon slots are
' currently equipped.
Public Property Get eqmEquipped() As Long
    
    eqmEquipped = eqmNil
    
    Dim aco As AcoCls
    For Each aco In coacoEquippedMbr
        eqmEquipped = eqmEquipped Or aco.eqmWearer
    Next aco
    
End Property


Public Sub AddTradeItem(ByVal aco As AcoCls, ByVal side As Long)
    
    Select Case side
    Case 1
        Call coacoTradeRight.AddAcoF(aco)
    Case 2
        Call coacoTradeLeft.AddAcoF(aco)
    End Select
    
    Call aco.SetFTradeItem(True)
    
End Sub

Public Sub ClearTradeItems()
    
    Dim aco As AcoCls
    For Each aco In coacoTradeLeft
        Call aco.SetFTradeItem(False)
    Next aco
    For Each aco In coacoTradeRight
        Call aco.SetFTradeItem(False)
    Next aco
    
    Call coacoTradeLeft.Clear
    Call coacoTradeRight.Clear
    
End Sub

' An object has been deleted from the game world.  Remove it from the object table.
Public Sub DeleteAco(ByVal aco As AcoCls, ByVal fDelDependents As Boolean)
    
    If aco.oid = oidPx Then
        Call DebugLine("ACO 0x" + SzHex(aco.oid, 8) + " " + SzQuote(aco.szName) + " is being deleted.")
    End If
    
    If fDelDependents Then
    '   If this ACO can contain or wield other ACOs, delete them too.
        If aco.fContainer Or aco.mcm = mcmCreature Then
            Call DeleteDependentAcos(aco)
        End If
    End If
    
'   Set dictOidAco.Item(aco.oid) = Nothing
    Call dictOidAco.Remove(aco.oid)
    
'   Disconnect from other ACOs for faster garbage collection.
    Call aco.PrepForDestroy
    
End Sub

' Clean up orphaned weapons that client forgot to delete.
Public Sub DeleteOrphanedAcos()
    Call TraceEnter("DeleteOrphanedAcos")
    
    Dim rgaco As Variant, iaco As Long
    rgaco = dictOidAco.Items
    For iaco = 0 To UBound(rgaco)
        Dim aco As AcoCls: Set aco = rgaco(iaco)
        
        If Not FOrphanedAco(aco) Then
            Call aco.SetfOrphaned(False)
            
        ElseIf Not aco.fOrphaned Then
            Call aco.SetfOrphaned(True)
            
        Else
            Call DebugLine("Deleting orphaned ACO " + _
              SzHex(aco.oid, 8) + " " + aco.szName)
            Call DeleteAco(aco, False)
        End If
        
    Next iaco
    
    Call TraceExit("DeleteOrphanedAcos")
End Sub

' User has logged out.  Delete everything.
Public Sub DeleteAllAcos()
    Call TraceEnter("DeleteAllAcos")
    
    Set acoChar = Nothing
    
    Do While cpackMbr > 0
        cpackMbr = cpackMbr - 1
        Set rgacoPack(cpackMbr) = Nothing
    Loop
    
    Set coacoEquippedMbr = New CoacoCls
    Set coacoTradeLeft = New CoacoCls
    Set coacoTradeRight = New CoacoCls
   
    Dim rgaco As Variant, iaco As Long
    rgaco = dictOidAco.Items
    For iaco = 0 To UBound(rgaco)
        Dim aco As AcoCls: Set aco = rgaco(iaco)
        
        Call aco.PrepForDestroy
        
    Next iaco
    Set dictOidAco = New Dictionary
    
    Call TraceExit("DeleteAllAcos")
End Sub


'Public Sub PxPendingAcos()
'
'    Dim rgaco As Variant, iaco As Long
'    rgaco = dictOidAco.Items
'    For iaco = 0 To UBound(rgaco)
'        Dim aco As AcoCls: Set aco = rgaco(iaco)
'
'        If aco.fPending() Then
'            Call DebugLine("")
'            Call aco.Px
'        End If
'
'    Next iaco
'
'End Sub


Public Sub Display()
    
    Call formOtable.Display(Me)
    
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    Set dictOidAco = New Dictionary
    
    cpackMbr = 0
    Dim ipack As Long
    For ipack = 0 To UBound(rgacoPack)
        Set rgacoPack(ipack) = Nothing
    Next ipack
    
    Set coacoEquippedMbr = New CoacoCls
    Set coacoTradeLeft = New CoacoCls
    Set coacoTradeRight = New CoacoCls
    
    oidPx = oidNil
    
End Sub

Private Sub DeleteDependentAcos(ByVal acoOwner As AcoCls)
    Call TraceEnter("otable.DeleteDependentAcos", SzQuote(acoOwner.szName))
    
    Dim rgaco As Variant, iaco As Long
    rgaco = dictOidAco.Items
    For iaco = 0 To UBound(rgaco)
        Dim aco As AcoCls: Set aco = rgaco(iaco)
        
        If aco.acoContainer Is acoOwner Or aco.acoWearer Is acoOwner Then
            Call DeleteAco(aco, False)
        End If
        
    Next iaco
    
    Call TraceExit("otable.DeleteDependentAcos")
End Sub

Private Function FOrphanedAco(ByVal aco As AcoCls) As Boolean
    
    If aco.acoContainer Is Nothing And _
      aco.acoWearer Is Nothing And _
      aco.maploc Is Nothing Then
    '   Item is in limbo, neither carried nor equipped by anybody,
    '   nor in the world at any specific location.
        FOrphanedAco = True
        Exit Function
    End If
    
    If Not aco.acoContainer Is Nothing Then
        If Not aco.acoContainer.FExists Then
        '   Item is in the inventory of a deleted ACO.
            FOrphanedAco = True
            Exit Function
        End If
    End If
    
    If Not aco.acoWearer Is Nothing Then
        If Not aco.acoWearer.FExists Then
        '   Item is equipped by a deleted ACO.
            FOrphanedAco = True
            Exit Function
        End If
    End If
    
    FOrphanedAco = False
    
End Function
















