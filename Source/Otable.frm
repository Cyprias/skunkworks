VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form formOtable 
   Caption         =   "Object Table"
   ClientHeight    =   4545
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Otable.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4545
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox textProperties 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1335
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   3120
      Width           =   4335
   End
   Begin MSComctlLib.TreeView tview 
      Height          =   2895
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4335
      _ExtentX        =   7646
      _ExtentY        =   5106
      _Version        =   393217
      LineStyle       =   1
      Style           =   7
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "formOtable"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


'--------------
' Private data
'--------------

Private otableMbr As OtableCls


'----------------
' Public methods
'----------------

Public Sub Display(ByVal otable As OtableCls)
    
    Set otableMbr = otable
    
    Dim nodeEquip As Node
    Set nodeEquip = tview.Nodes.Add(, , , "Equipment")
    nodeEquip.Tag = oidNil
    Dim coacoEquipped As CoacoCls
    Set coacoEquipped = otable.CoacoFromEqm(eqmAll)
    Dim acoItem As AcoCls, nodeItem As Node
    For Each acoItem In coacoEquipped
        Set nodeItem = tview.Nodes.Add(nodeEquip, tvwChild, , _
          SzHex(acoItem.oid, 8) + "  " + acoItem.szName)
        nodeItem.Tag = acoItem.oid
    Next acoItem
    
    Dim nodeInv As Node
    Set nodeInv = tview.Nodes.Add(, , , "Inventory")
    nodeInv.Tag = oidNil
    Dim ipack As Long
    For ipack = 0 To otableMbr.cpack - 1
        Dim acoPack As AcoCls, nodePack As Node
        Set acoPack = otableMbr.AcoFromIpack(ipack)
        Set nodePack = tview.Nodes.Add(nodeInv, tvwChild, , _
          SzHex(acoPack.oid, 8) + "  " + acoPack.szName)
        nodePack.Tag = acoPack.oid
        Dim iitem As Long
        For iitem = 0 To acoPack.citemContents - 1
            Set acoItem = acoPack.AcoFromIitem(iitem)
            Set nodeItem = tview.Nodes.Add(nodePack, tvwChild, , _
              SzHex(acoItem.oid, 8) + "  " + acoItem.szName)
            nodeItem.Tag = acoItem.oid
        Next iitem
    Next ipack
    
    Call Show(vbModal)
    Call Unload(Me)
    
End Sub


'----------
' Controls
'----------

Private Sub Form_Load()
    
'   Restore previous window size and position.
    Call RestoreFormPosition(Me, "", True, swoptions.szAppName)
'   Force a Resize event in the case when the size doesn't actually change
'   so that controls get laid out properly the first time.
    Call Form_Resize
    
End Sub

Private Sub Form_Resize()
    
    If ScaleWidth > 0 Then
    '   Lay out controls to conform to new window size.
        Dim dxMargin As Long, dyMargin As Long
        dxMargin = tview.Left
        dyMargin = dxMargin
        
        Dim dyAvail As Long, dyTree As Long, dyProps As Long
        dyAvail = ScaleHeight - dyMargin * 3
        dyTree = dyAvail * 2 / 3
        dyProps = dyAvail * 1 / 3
        
        Call tview.Move(dxMargin, dyMargin, _
          ScaleWidth - dxMargin * 2, dyTree)
        Call textProperties.Move(dxMargin, dyMargin + dyTree + dyMargin, _
          ScaleWidth - dxMargin * 2, dyProps)
        
    '   Save new size/position in registry.
        Call SaveFormPosition(Me, "", swoptions.szAppName)
    End If
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    
'   Save window size/position in registry.
    Call SaveFormPosition(Me, "", swoptions.szAppName)
    
End Sub


Private Sub tview_NodeClick(ByVal Node As MSComctlLib.Node)
    
    Dim sz As String
    sz = ""
    
    Dim oid As Long
    oid = Node.Tag
    If oid <> oidNil Then
        Dim aco As AcoCls
        Set aco = otableMbr.AcoFromOid(oid)
        
        sz = sz + "oid:           " + SzHex(oid, 8) + vbCrLf
        sz = sz + "szName:        " + aco.szName + vbCrLf
        If aco.citemMaxStack > 1 Then
            sz = sz + "szPlural:      " + aco.szPlural + vbCrLf
            sz = sz + "citemStack:    " + CStr(aco.citemStack) + vbCrLf
            sz = sz + "citemMaxStack: " + CStr(aco.citemMaxStack) + vbCrLf
        End If
        If aco.eqm <> eqmNil Then
            sz = sz + "eqm:           " + SzHex(aco.eqm, 8) + vbCrLf
        End If
        sz = sz + "mcm:           " + SzHex(aco.mcm, 8) + vbCrLf
        If aco.ocm <> ocmNil Then
            sz = sz + "ocm:           " + SzHex(aco.ocm, 8) + vbCrLf
        End If
        If aco.olc <> olcNil Then
            sz = sz + "olc:           " + SzHex(aco.olc, 8) + vbCrLf
        End If
        sz = sz + "oty:           " + SzHex(aco.oty, 8) + vbCrLf
        sz = sz + "cpyValue:      " + CStr(aco.cpyValue) + vbCrLf
        If aco.cuseMax > 0 Then
            sz = sz + "cuseLeft:      " + CStr(aco.cuseLeft) + vbCrLf
            sz = sz + "cuseMax:       " + CStr(aco.cuseMax) + vbCrLf
        End If
        If aco.material <> 0 Then
            sz = sz + "material:      " + CStr(aco.material) + vbCrLf
        End If
        If aco.spellid <> 0 Then
            sz = sz + "spellid:       " + CStr(aco.spellid) + vbCrLf
        End If
        If aco.oidMonarch <> 0 Then
            sz = sz + "oidMonarch:    " + SzHex(aco.oidMonarch, 8) + vbCrLf
        End If
        If Not (aco.maploc Is Nothing) Then
            sz = sz + "maploc:        " + aco.maploc.sz(3) + vbCrLf
        End If
        If Not (aco.acoContainer Is Nothing) Then
            sz = sz + "Container:     " + SzHex(aco.acoContainer.oid, 8) + "  " + aco.acoContainer.szName + vbCrLf
            sz = sz + "iitem:         " + CStr(aco.iitem) + vbCrLf
        End If
        If Not (aco.acoWearer Is Nothing) Then
            sz = sz + "Wearer:        " + SzHex(aco.acoWearer.oid, 8) + "  " + aco.acoWearer.szName + vbCrLf
            sz = sz + "eqmWearer:     " + SzHex(aco.eqmWearer, 8) + vbCrLf
        End If
        If aco.fContainer Then
            sz = sz + "citemContents: " + CStr(aco.citemContents) + vbCrLf
            sz = sz + "citemMax:      " + CStr(aco.citemMax) + vbCrLf
            sz = sz + "cpackMax:      " + CStr(aco.cpackMax) + vbCrLf
        End If
    End If
    
    textProperties.Text = sz
    
End Sub


'----------
' Privates
'----------
































