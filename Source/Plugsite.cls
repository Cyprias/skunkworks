VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PlugsiteCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' IPluginSite2 surrogate class for IDispatch access from script.


' REVIEW: Revisit this when things settle down.

'--------------------------------
' IPluginSite2 wrapper functions
'--------------------------------

' No point exposing this one since it's not implemented in Decal.
'Public Sub Unload()
'    Call site2.Unload
'End Sub

Public Property Get Object(ByVal Path As String) As Object
    Set Object = site2.Object(Path)
End Property

Public Property Get Decal() As IDecal
    Set Decal = site2.Decal
End Property

Public Property Get Hooks() As IACHooks
    Set Hooks = site2.Hooks
End Property

Public Property Get PluginSite() As Object
'   IPluginSite needs an IDispatch wrapper too, which is Me.
    Set PluginSite = Me
End Property

Public Sub RegisterSinks(ByVal pPlugin As Object)
    Call site2.RegisterSinks(pPlugin)
End Sub


'-------------------------------
' IPluginSite wrapper functions
'-------------------------------

' No point exposing this one since it's not implemented in Decal.
'Public Sub UnloadPlugin(ByVal nID As Long)
'    Call site.UnloadPlugin(nID)
'End Sub

' Beyond the scope of script.
'Public Function GetDirectDraw(ByVal riid As Guid) As Long
'    GetDirectDraw = site.GetDirectDraw(riid)
'End Function

Public Function GetPrimarySurface() As ICanvas
    Set GetPrimarySurface = site.GetPrimarySurface()
End Function

' Beyond the scope of script.
'Public Function Get3DDevice(ByVal riid As Guid) As Long
'    Get3DDevice = site.Get3DDevice(riid)
'End Function

Public Function LoadBitmapFile(ByVal strFilename As String) As IImageCache
    Set LoadBitmapFile = site.LoadBitmapFile(strFilename)
End Function

Public Function GetIconCache(ByVal psz As SizeCls) As IIconCache
    Set GetIconCache = site.GetIconCache(psz.size)
End Function

' This one's a bit screwed up in the IDL.
Public Function LoadBitmapPortal(ByVal nFile As Long) As IImageCache
    Set LoadBitmapPortal = site.LoadBitmapPortal(nFile)
End Function

Public Function DecalCreateFont(ByVal szFaceName As String, ByVal nHeight As Long, ByVal dwFlags As Long) As IFontCache
    Set DecalCreateFont = site.DecalCreateFont(szFaceName, nHeight, dwFlags)
End Function

Public Function GetScreenSize() As SizeCls
    Set GetScreenSize = New SizeCls
    Call site.GetScreenSize(GetScreenSize.size)
End Function

Public Function CreateCanvas(ByVal psz As SizeCls) As ICanvas
    Set CreateCanvas = site.CreateCanvas(psz.size)
End Function

' Need to wrap ViewParams to expose this.
'Public Function CreateView(ByVal pParams As ViewParams, ByVal pLayer As ILayer) As IView
'    Set CreateView = site.CreateView(pParams, pLayer)
'End Function

Public Function LoadView(ByVal strSchema As String) As IView
    Set LoadView = site.LoadView(strSchema)
End Function

Public Function CreateBrushImage(ByVal nColor As Long) As IImageCache
    Set CreateBrushImage = site.CreateBrushImage(nColor)
End Function

Public Function LoadImageSchema(ByVal pSchema As Object) As IImageCache
    Set LoadImageSchema = site.LoadImageSchema(pSchema)
End Function

Public Function CreateFontSchema(ByVal nDefHeight As Long, ByVal nDefOptions As Long, ByVal pSchema As Object) As IFontCache
    Set CreateFontSchema = site.CreateFontSchema(nDefHeight, nDefOptions, pSchema)
End Function

Public Function LoadResourceModule(ByVal strLibrary As String) As Long
    LoadResourceModule = site.LoadResourceModule(strLibrary)
End Function

Public Property Get ResourcePath() As String
    ResourcePath = site.ResourcePath
End Property

' Superseded by PluginSite2.Object:
'Public Property Get Plugin(ByVal strProgID As String) As Object
'    Set Plugin = site.Plugin(strProgID)
'End Property

' Superseded by PluginSite2.Object:
'Public Property Get NetworkFilter(ByVal strProgID As String) As Object
'    Set NetworkFilter = site.NetworkFilter(strProgID)
'End Property

Public Function LoadViewObject(ByVal pSchema As Object) As IView
    Set LoadViewObject = site.LoadViewObject(pSchema)
End Function

Public Property Get hwnd() As Long
    hwnd = site.hwnd
End Property

Public Property Get Focus() As Boolean
    Focus = site.Focus
End Property

Public Property Get OldWndProc() As Long
    OldWndProc = site.OldWndProc
End Property

Public Function QueryKeyboardMap(ByVal bstrName As String) As Long
    QueryKeyboardMap = site.QueryKeyboardMap(bstrName)
End Function

Public Sub RedrawBar()
    Call site.RedrawBar
End Sub

Public Property Get FontName() As String
    FontName = site.FontName
End Property








