VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PortaldatCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Option Compare Text
' Portal.dat information


'--------------
' Private data
'--------------

Private datfilePortal As DatfileCls
Private Const emfidSkills = &HE000004
Private Const emfidSpells = &HE00000E
Private Const emfidLevels = &HE000018
Private Const emfidSpecies = &H2200000E

' Spell data from Portal.dat:
Private Type SpelldatType
    szName As String
    szDesc As String
    school As Long
    icon As Long
    family As Long
    dwFlags As Long
    mana As Long
    rangeMin As Single
    rangePerLvl As Single
    diff As Long
    csecDuration As Long
End Type

Private mpspellidspelldat() As SpelldatType
Private spellidMaxMbr As Long
Private dictSzSpellid As Dictionary

Private mpschoolskid(4) As Long

Private dictSzSkid As Dictionary


'------------
' Properties
'------------

Public exptabAttr As ExptabCls
Public exptabVital As ExptabCls
Public exptabTrain As ExptabCls
Public exptabSpec As ExptabCls
Public exptabChar As ExptabCls


Public Property Get spellidMax() As Long
    spellidMax = spellidMaxMbr
End Property


'---------
' Methods
'---------

Public Function SpellFromSpellid(ByVal spellid As Long) As SpellCls
    
    Set SpellFromSpellid = Nothing
    
    If spellid >= 0 And spellid <= spellidMax Then
        Dim spelldat As SpelldatType
        spelldat = mpspellidspelldat(spellid)
        If spelldat.family <> 0 Then
            Set SpellFromSpellid = SpellFromSpelldat(spellid, spelldat)
        End If
    End If
    
End Function

Public Function SzFromSpellid(ByVal spellid As Long) As String
    
    If spellid >= 0 And spellid <= spellidMax Then
        SzFromSpellid = mpspellidspelldat(spellid).szName
    Else
        SzFromSpellid = ""
    End If
    
End Function

Public Function SpellidFromSz(ByVal sz As String) As Long
    
    If dictSzSpellid.Exists(sz) Then
        SpellidFromSz = dictSzSpellid(sz)
    ElseIf FSuffixMatchSzI(sz, " VII") Then
    '   Asking for a level VII spell that doesn't exist.
    '   Try to find one by a different name.
        SpellidFromSz = SpellidFindVII(sz)
        If SpellidFromSz <> spellidNil Then
        '   Remember what we found for next time.
            dictSzSpellid(sz) = SpellidFromSz
        End If
    Else
        SpellidFromSz = spellidNil
    End If
    
End Function

Public Function CospellFromFamily(ByVal family As Long) As CospellCls
    
    Dim cospell As CospellCls
    Set cospell = New CospellCls
    
    Dim spellid As Long
    For spellid = 1 To spellidMaxMbr
        If mpspellidspelldat(spellid).family = family Then
            Call cospell.InsertSpellByDiff(SpellFromSpelldat(spellid, _
              mpspellidspelldat(spellid)))
        End If
    Next spellid
    
    Set CospellFromFamily = cospell
    
End Function


Public Function SkidFromSz(ByVal sz As String) As Long
    
    If dictSzSkid.Exists(sz) Then
        SkidFromSz = dictSzSkid(sz)
    Else
        SkidFromSz = skidNil
    End If
    
End Function


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    Set datfilePortal = New DatfileCls
    Call datfilePortal.Create(acapp.szPortalDat)
    
    mpschoolskid(1) = skidWarMagic
    mpschoolskid(2) = skidLifeMagic
    mpschoolskid(3) = skidItemEnchantment
    mpschoolskid(4) = skidCreatureEnchantment
    
    Call LoadSpells
    Call LoadSkills
    Call LoadLevels
    
'    Call PxSpecies
    
    Set datfilePortal = Nothing
    
End Sub


Private Sub LoadSpells()
    
    Dim emf As EmfCls
    Set emf = datfilePortal.EmfOpen(emfidSpells)
    
    Call Assert(emf.DwRead() = emfidSpells, "Spell table decoding error.")
    
    Dim cspell As Long
    cspell = emf.WRead()
    Call emf.SkipCb(2)
    
'   There are some gaps in the spellid space so allocate a bit extra.
    ReDim mpspellidspelldat(Int(cspell * 1.1))
    Set dictSzSpellid = New Dictionary
    dictSzSpellid.CompareMode = TextCompare
    spellidMaxMbr = 0
    
    Dim ispell As Long
    For ispell = 0 To cspell - 1
        Dim spellid As Long: spellid = emf.DwRead()
        Dim spelldat As SpelldatType
        
        spelldat.szName = emf.SzReadSwapped()
        spelldat.szDesc = emf.SzReadSwapped()
        spelldat.school = emf.DwRead()
        spelldat.icon = emf.DwRead()
        spelldat.family = emf.DwRead()
        spelldat.dwFlags = emf.DwRead()
        spelldat.mana = emf.DwRead()
        spelldat.rangeMin = emf.FloatRead()
        spelldat.rangePerLvl = emf.FloatRead()
        spelldat.diff = emf.DwRead()
        Call emf.SkipCb(16)
        Call Assert(emf.DwRead() = spellid, "Spell table decoding error.")
        Dim dwComps As Long
        dwComps = emf.DwRead()
        If dwComps = 0 Then
            Dim csecDuration As Double
            csecDuration = 0
            Call emf.ReadPv(VarPtr(csecDuration) + 4, 4)
            spelldat.csecDuration = csecDuration
            Do
                dwComps = emf.DwRead()
                If dwComps <> 0 Then Exit Do
                Call emf.SkipCb(4)
            Loop
        Else
            spelldat.csecDuration = -1
        End If
        
        Call emf.SkipCb(64)
        
'        Call DebugLine(spelldat.szName + ", " + _
'          Array("", "War", "Life", "Item", "Creature")(spelldat.school) + ", " + _
'          FormatNumber(spelldat.rangeMin, 4) + ", " + _
'          FormatNumber(spelldat.rangePerLvl, 4))
        
        mpspellidspelldat(spellid) = spelldat
        If dictSzSpellid.Exists(spelldat.szName) Then
'            Call TraceLine("Redundant spell name: " + SzQuote(spelldat.szName))
'            Call TraceLine("spellid1 = " + CStr(dictSzSpellid(spelldat.szName)) + ", " + _
'              "spellid2 = " + CStr(spellid))
        Else
            dictSzSpellid(spelldat.szName) = spellid
        End If
        
        If spellid > spellidMaxMbr Then spellidMaxMbr = spellid
        
        DoEvents
    Next ispell
    
    ReDim Preserve mpspellidspelldat(spellidMaxMbr)
    
End Sub

Private Sub LoadSkills()
    
    Dim emf As EmfCls
    Set emf = datfilePortal.EmfOpen(emfidSkills)
    
    Call Assert(emf.DwRead() = emfidSkills, "Skill table decoding error.")
    
    Dim cskill As Long
    cskill = emf.WRead()
    Call emf.SkipCb(2)
    
    Set dictSzSkid = New Dictionary
    dictSzSkid.CompareMode = TextCompare
    
'    Call TraceLine( _
'      SzPadLeft("skid", 4) + "  " + _
'      SzPadRight("name", 20) + "  " + _
'      SzPadLeft("formula", 9) + "  " + _
'      "description" _
'      )
'    Call TraceLine( _
'      SzPadLeft("----", 4) + "  " + _
'      SzPadRight("----", 20) + "  " + _
'      SzPadLeft("-------", 9) + "  " + _
'      "-----------" _
'      )
    
    Dim iskill As Long
    For iskill = 0 To cskill - 1
        Dim skid As Long, szName As String, szDesc As String
        skid = emf.DwRead()
        szDesc = emf.SzRead()
        szName = emf.SzRead()
        Call emf.SkipCb(&H24)
        Dim divisor As Long, attr1 As Long, attr2 As Long
        divisor = emf.DwRead()
        attr1 = emf.DwRead()
        attr2 = emf.DwRead()
        Call emf.SkipCb(&H18)
        
    '   REVIEW: Not sure why theses aren't nil in the table, but they aren't.
        If skid = skidLoyalty Or skid = skidSalvaging Then
            attr1 = attrNil
        End If
        
'        Call TraceLine( _
'          SzPadLeft(CStr(skid), 4) + "  " + _
'          SzPadRight(szName, 20) + "  " + _
'          SzPadLeft("(" + CStr(attr1) + " + " + CStr(attr2) + ")/" + CStr(divisor), 9) + "  " + _
'          szDesc _
'          )
        
        dictSzSkid(szName) = skid
        Call self.InitSkinfoSkid(skid, szName, szDesc, attr1, attr2, divisor)
        
        DoEvents
    Next iskill
    
End Sub

Private Sub LoadLevels()
    
    Dim emf As EmfCls
    Set emf = datfilePortal.EmfOpen(emfidLevels)
    
    Call Assert(emf.DwRead() = emfidLevels, "Level table decoding error.")
    
    Dim lvlMaxAttr As Long: lvlMaxAttr = emf.DwRead()
    Dim lvlMaxVital As Long: lvlMaxVital = emf.DwRead()
    Dim lvlMaxTrain As Long: lvlMaxTrain = emf.DwRead()
    Dim lvlMaxSpec As Long: lvlMaxSpec = emf.DwRead()
    Dim lvlMaxChar As Long: lvlMaxChar = emf.DwRead()
    
    Dim lvl As Long
    
    Set exptabAttr = New ExptabCls
    exptabAttr.k1 = 1006
    exptabAttr.k2 = 0.08
    exptabAttr.lvlMax = lvlMaxAttr
    For lvl = 0 To lvlMaxAttr
        exptabAttr.rgexp(lvl) = UDblFromDw(emf.DwRead())
    Next lvl
    
    Set exptabVital = New ExptabCls
    exptabVital.k1 = 664
    exptabVital.k2 = 0.08
    exptabVital.lvlMax = lvlMaxVital
    For lvl = 0 To lvlMaxVital
        exptabVital.rgexp(lvl) = UDblFromDw(emf.DwRead())
    Next lvl
    
    Set exptabTrain = New ExptabCls
    exptabTrain.k1 = 597.56
    exptabTrain.k2 = 0.0758
    exptabTrain.lvlMax = lvlMaxTrain
    For lvl = 0 To lvlMaxTrain
        exptabTrain.rgexp(lvl) = UDblFromDw(emf.DwRead())
    Next lvl
    
    Set exptabSpec = New ExptabCls
    exptabSpec.k1 = 17.34
    exptabSpec.k2 = 0.0853
    exptabSpec.lvlMax = lvlMaxSpec
    For lvl = 0 To lvlMaxSpec
        exptabSpec.rgexp(lvl) = UDblFromDw(emf.DwRead())
    Next lvl
    
'   Truncate character level table at 126 and use Ken Gober's formula beyond that.
    Set exptabChar = New ExptabCls
    exptabChar.fKGober = True
    exptabChar.lvlMax = lvlMaxChar
    For lvl = 0 To lvlMaxChar
        exptabChar.rgexp(lvl) = emf.QwRead()
    Next lvl
    
'    Call TraceLine( _
'      SzPadLeft("lvl", 12) + _
'      SzPadLeft("Attr", 12) + _
'      SzPadLeft("Vital", 12) + _
'      SzPadLeft("Train", 12) + _
'      SzPadLeft("Spec", 12) + _
'      SzPadLeft("Char", 12) _
'      )
'    Call TraceLine( _
'      SzPadLeft("----", 12) + _
'      SzPadLeft("----", 12) + _
'      SzPadLeft("-----", 12) + _
'      SzPadLeft("-----", 12) + _
'      SzPadLeft("----", 12) + _
'      SzPadLeft("----", 12) _
'      )
'    For lvl = 0 To 100
'        Call TraceLine( _
'          SzPadLeft(CStr(lvl), 12) + _
'          SzPadLeft(CStr(exptabAttr.rgexp(lvl)), 12) + _
'          SzPadLeft(CStr(exptabVital.rgexp(lvl)), 12) + _
'          SzPadLeft(CStr(exptabTrain.rgexp(lvl)), 12) + _
'          SzPadLeft(CStr(exptabSpec.rgexp(lvl)), 12) + _
'          SzPadLeft(CStr(exptabChar.rgexp(lvl)), 12) _
'          )
'    Next lvl
    
End Sub


Private Function SpellidFindVII(ByVal szNameVII As String) As Long
    
    SpellidFindVII = spellidNil
    
'   Look up the corresponding level VI spell.
    Dim szNameVI As String, spellidVI As Long
    szNameVI = Left(szNameVII, Len(szNameVII) - 1)
    spellidVI = SpellidFromSz(szNameVI)
    If spellidVI = spellidNil Then Exit Function
    
'   Create a wildcard pattern from the level VI description.
    Dim szPattern As String, effectVI As Single
    szPattern = SzStripNumbers(effectVI, mpspellidspelldat(spellidVI).szDesc)
    
    Dim spellidVII As Long, effectVII As Single
    spellidVII = spellidNil
    effectVII = 1000000    ' Arbitrary large number.
    
'   Search the spell table for unnumbered spells matching that pattern.
    Dim spellid As Long
    For spellid = 2052 To 2345  ' per Ken Gober
        Dim szLevel As String
        Call SplitSzRev("", szLevel, mpspellidspelldat(spellid).szName, " ")
        Select Case szLevel
        Case "I", "II", "III", "IV", "V", "VI", "VII"
        '   Ignore spells with level number suffixes.
        Case Else
            Dim szDesc As String: szDesc = mpspellidspelldat(spellid).szDesc
            If szDesc Like szPattern Then
            '   This spell matches the pattern.  If there's more than one,
            '   take the one whose effect is closest to (but still greater
            '   than) the level VI effect.  I don't know if that's right,
            '   but it sounds plausible.
                Dim effect As Single
                Call SzStripNumbers(effect, szDesc)
                If effect > effectVI And effect < effectVII Then
                    spellidVII = spellid
                    effectVII = effect
                End If
            End If
        End Select
    Next spellid
    
    SpellidFindVII = spellidVII
End Function

Private Function SzStripNumbers(ByRef effectOut As Single, _
  ByVal sz As String) As String
    
    Dim szOut As String
    szOut = ""
    effectOut = 0
    
    Dim ichNumber As Long
    ichNumber = iNil
    
    Dim ich As Long
    For ich = 0 To Len(sz) - 1
        Dim szCh As String
        szCh = SzMid(sz, ich, 1)
        If ichNumber = iNil Then
            If IsNumeric(szCh) Then
                ichNumber = ich
            Else
                szOut = szOut + szCh
            End If
        Else
            If Not IsNumeric(szCh) And szCh <> "." Then
                szOut = szOut + "*" + szCh
                effectOut = CSng(SzMid(sz, ichNumber, ich - ichNumber))
                ichNumber = iNil
            End If
        End If
    Next ich
    
    If ichNumber <> iNil Then
        szOut = szOut + "*"
        effectOut = CSng(SzMid(sz, ichNumber, Len(sz) - ichNumber))
    End If
    
    SzStripNumbers = szOut
    
End Function

        
Private Function SpellFromSpelldat(ByVal spellid As Long, _
  ByRef spelldat As SpelldatType) As SpellCls
    
    Set SpellFromSpelldat = New SpellCls
    Call SpellFromSpelldat.FromSpelldat(spellid, spelldat.szName, spelldat.szDesc, _
      mpschoolskid(spelldat.school), spelldat.family, spelldat.diff, spelldat.mana, _
      spelldat.rangeMin, spelldat.rangePerLvl, spelldat.csecDuration)
    
End Function

Private Sub PxSpecies()
    
    Dim emf As EmfCls
    Set emf = datfilePortal.EmfOpen(emfidSpecies)
    
    Call Assert(emf.DwRead() = emfidSpecies, "Species table decoding error.")
    
    Call emf.SkipCb(5)
    
    Dim cspecies As Long, ispecies As Long
    cspecies = emf.BRead()
    For ispecies = 0 To cspecies - 1
        Dim species As Long, szName As String
        species = emf.DwRead()
        szName = emf.SzReadShort()
        Call DebugLine(SzPadLeft(CStr(species), 2) + ": " + szName)
    Next ispecies
    
End Sub





