VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PxtCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


'----------
' Privates
'----------

Private xpMbr As Long, ypMbr As Long
Private clrBefMbr As Long, clrAftMbr As Long
Private clrCheckMbr As Long

Private rMid As Double, gMid As Double, bMid As Double
Private m02 As Double, m12 As Double, m22 As Double


'-------------------
' Public properties
'-------------------

Public Property Get xp() As Long
    xp = xpMbr
End Property

Public Property Let xp(ByVal xpNew As Long)
    xpMbr = xpNew
End Property


Public Property Get yp() As Long
    yp = ypMbr
End Property

Public Property Let yp(ByVal ypNew As Long)
    ypMbr = ypNew
End Property


Public Property Get clrBefore() As Long
    clrBefore = clrBefMbr
End Property

Public Property Let clrBefore(ByVal clrNew As Long)
    
    clrBefMbr = clrNew
    Call Precalc
    
End Property


Public Property Get clrAfter() As Long
    clrAfter = clrAftMbr
End Property

Public Property Let clrAfter(ByVal clrNew As Long)

    clrAftMbr = clrNew
    Call Precalc
    
End Property


' Return the current color of the specified pixel.
Public Property Get clrCur() As Long
    
    clrCur = acapp.ClrOfPixel(xpMbr, ypMbr)
    
End Property

' Return the pixel color as of the call to FCheck.
Public Property Get clrCheck() As Long
    
    clrCheck = clrCheckMbr
    
End Property

' Return a pxt object representing the reverse transition of this one
' (i.e. changing from clrAfter to clrBefore).
Public Property Get pxtReverse() As PxtCls
    
    Set pxtReverse = New PxtCls
    Call pxtReverse.Create(xpMbr, ypMbr, clrAftMbr, clrBefMbr)
    
End Property



'----------------
' Public methods
'----------------

' Return True if the pixel at xp,yp is more like clrAfter than like clrBefore,
' False otherwise.  xp and yp may be positive or negative.  Positive values
' measure down from upper left corner of the window; negative values measure
' up from the lower right.  For resolution independence, specify pixels near
' the right or bottom edges using negative xp or yp.
Public Function FCheck() As Boolean
    
    clrCheckMbr = acapp.ClrOfPixel(xpMbr, ypMbr)
    FCheck = FCheckClr()
    
End Function

' Wait for the pixel at xp,yp to change from clrBefore to clrAfter.  Color
' values need not be exact; the wait is satisfied as soon as the pixel's
' color becomes more like clrAfter than like clrBefore.  If csecTimeout
' seconds elapse before that happens, return False; otherwise return True.
Public Function FWait(ByVal csecTimeout As Single, _
  Optional ByVal fFireEvents As Boolean = False) As Boolean
    
    FWait = FWaitXyp(xpMbr, ypMbr, csecTimeout, fFireEvents)
    
End Function

' Return True if the pixel at xpA,ypA is more like clrAfter than like clrBefore,
' False otherwise.  xpA and ypA may be positive or negative.  Positive values
' measure down from upper left corner of the window; negative values measure
' up from the lower right.  For resolution independence, specify pixels near
' the right or bottom edges using negative xp or yp.
Public Function FCheckXyp(ByVal xpA As Long, ByVal ypA As Long) As Boolean
    
    clrCheckMbr = acapp.ClrOfPixel(xpA, ypA)
    FCheckXyp = FCheckClr()
    
End Function

' Wait for the pixel at xpA,ypA to change from clrBefore to clrAfter.  Color
' values need not be exact; the wait is satisfied as soon as the pixel's
' color becomes more like clrAfter than like clrBefore.  If csecTimeout
' seconds elapse before that happens, return False; otherwise return True.
Public Function FWaitXyp(ByVal xpA As Long, ByVal ypA As Long, _
  ByVal csecTimeout As Single, Optional ByVal fFireEvents As Boolean = False) As Boolean
    
    Dim tmr As TimerCls: Set tmr = New TimerCls
    Dim clrPrev As Long: clrPrev = &H80000000
    
    Do
        clrCheckMbr = acapp.ClrOfPixel(xpA, ypA)
        If clrCheckMbr <> clrPrev Then
            If FCheckClr() Then
                FWaitXyp = True
                Exit Do
            End If
            clrPrev = clrCheckMbr
        End If
        
        If tmr.cmsec > csecTimeout * 1000 Then
            FWaitXyp = False
            Exit Do
        End If
        
        If fFireEvents Then
            Call skapi.WaitEvent(100)
            If skapi.fExitWaitEvent Then Exit Do
        Else
            Call Sleep(100)
        End If
    Loop
    
End Function


'------------------
' Friend functions
'------------------

Friend Sub Create(ByVal xp As Long, ByVal yp As Long, _
  ByVal clrBefore As Long, ByVal clrAfter As Long)
    
    xpMbr = xp
    ypMbr = yp
    clrBefMbr = clrBefore
    clrAftMbr = clrAfter
    Call Precalc
    
    clrCheckMbr = clrNil
    
End Sub


'----------
' Privates
'----------

' Calculate the equation of the plane equidistant between clrBefore and
' clrAfter.
Private Sub Precalc()
    
    If clrBefMbr = clrNil Then clrBefMbr = acapp.ClrOfPixel(xpMbr, ypMbr)
    
    Dim rBef As Long: rBef = clrBefMbr And &HFF&
    Dim gBef As Long: gBef = Int(clrBefMbr / &H100&) And &HFF&
    Dim bBef As Long: bBef = Int(clrBefMbr / &H10000) And &HFF&
    
    Dim rAft As Long: rAft = clrAftMbr And &HFF&
    Dim gAft As Long: gAft = Int(clrAftMbr / &H100&) And &HFF&
    Dim bAft As Long: bAft = Int(clrAftMbr / &H10000) And &HFF&
    
    rMid = (rBef + rAft) / 2
    gMid = (gBef + gAft) / 2
    bMid = (bBef + bAft) / 2
    
    Dim pt3Bef As Pt3Type, pt3Aft As Pt3Type, pt3Mid As Pt3Type
    pt3Bef.X = rBef
    pt3Bef.Y = gBef
    pt3Bef.z = bBef
    
    pt3Aft.X = rAft
    pt3Aft.Y = gAft
    pt3Aft.z = bAft
    
    pt3Mid.X = rMid
    pt3Mid.Y = gMid
    pt3Mid.z = bMid
    
    Dim view As ViewType
    Call InitView(view)
    Call TranslateView(view, pt3Mid)
    Call ViewPoint(view, pt3Aft)
    
    m02 = view.mat(0, 2)
    m12 = view.mat(1, 2)
    m22 = view.mat(2, 2)
    
'   Call DebugLine( _
'     "rMid: " + Format(rMid, "0.00") + ", " + _
'     "gMid: " + Format(gMid, "0.00") + ", " + _
'     "bMid: " + Format(bMid, "0.00"))
'   Call DebugLine( _
'     "m02: " + Format(m02, "0.00") + ", " + _
'     "m12: " + Format(m12, "0.00") + ", " + _
'     "m22: " + Format(m22, "0.00"))
    
End Sub

' Return True if clrCheck lies "above" the equidistant plane (i.e. closer to
' clrAfter), False otherwise.
Private Function FCheckClr() As Boolean
    
    If clrAftMbr = clrNil Then
        FCheckClr = (clrCheckMbr = clrNil)
    ElseIf clrCheckMbr <> clrNil Then
        Dim rCur As Long: rCur = clrCheckMbr And &HFF&
        Dim gCur As Long: gCur = Int(clrCheckMbr / &H100&) And &HFF&
        Dim bCur As Long: bCur = Int(clrCheckMbr / &H10000) And &HFF&
        
        Dim zCur As Long
        zCur = (rCur - rMid) * m02 + (gCur - gMid) * m12 + (bCur - bMid) * m22
        
    '   Call DebugLine("clrCheck: " + SzHex(clrCheckMbr, 6) + ", " + _
    '     "zCur: " + CStr(zCur))
        
        FCheckClr = zCur > 0
    Else
    '   Call DebugLine("clrCheck: clrNil")
        FCheckClr = False
    End If
    
End Function












