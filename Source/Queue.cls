VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "QueueCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' This object implements a one-way inter-process queue using shared memory.
' (I tried named pipes but that was way more complicated.)


'--------------
' Private data
'--------------

Private hmutex As Long
Private hevent As Long
Private hFile As Long, hmap As Long, pbMap As Long

Private prgbShared As Long, cbShared As Long
Private pibIn As Long, pibOut As Long
Private prgbBuf As Long, cbBuf As Long

Private hmutexConnect As Long
Private fReading As Boolean


'-------------------
' Public properties
'-------------------

' Return True if someone is connected to the read end of the queue.
Public Property Get fConnected() As Boolean
'   If fTrace Then Call TraceEnter("queue.fConnected", "")
    
    If fReading Then
    '   I'm the reader.
        fConnected = True
    Else
    '   Check the connect mutex.  If a reader is connected, we won't get it.
        Select Case WaitForSingleObject(hmutexConnect, 0)
        Case WAIT_TIMEOUT
        '   Didn't get it; must be connected.
            fConnected = True
        Case Else
        '   Got it; must not be connected.
            Call ReleaseMutex(hmutexConnect)
            fConnected = False
        End Select
    End If
    
'   If fTrace Then Call TraceExit("queue.fConnected", CStr(fConnected))
End Property


'----------------
' Public methods
'----------------

Public Function FExists(ByVal szName As String) As Boolean
    
    Dim szMutex As String, hmutexT As Long
    szMutex = SzMutexConnect(szName)
    hmutexT = OpenMutex(MUTEX_ALL_ACCESS, False, szMutex)
    If hmutexT <> hNil Then
        Call CloseHandle(hmutexT)
        FExists = True
    Else
        FExists = False
    End If
    
End Function

Public Function FInUse(ByVal szName As String) As Boolean
    
    Dim szMutex As String, hmutexT As Long
    szMutex = SzMutexConnect(szName)
    hmutexT = OpenMutex(MUTEX_ALL_ACCESS, False, szMutex)
    If hmutexT <> hNil Then
        Select Case WaitForSingleObject(hmutexT, 0)
        Case WAIT_OBJECT_0, WAIT_ABANDONED
        '   Got it.
            Call ReleaseMutex(hmutexT)
            FInUse = False
        Case Else
            FInUse = True
        End Select
        Call CloseHandle(hmutexT)
    Else
        FInUse = False
    End If
    
End Function

' Create/connect to the queue.  If mode is ForWriting or ForAppending, connect
' the the write end; if ForReading, connect to the read end.
Public Function FCreate(ByVal szName As String, ByVal mode As Scripting.IOMode, _
  Optional ByVal cbSharedA As Long = 0) As Boolean
'   If fTrace Then Call TraceEnter("queue.FCreate", SzQuote(szName))
    
'   Call PxPriority
    
    Const cbMap As Long = &H80000   ' 512 KB
    Dim szObj As String
    
'   Create a mutex for controlling queue access.
    szObj = "SkunkWorks.queue" + szName + ".hmutex"
    hmutex = CreateMutex(0, False, szObj)
    Call Assert(hmutex <> hInvalid, "Could not create mutex " + szObj + ".")
    
'   Create an event for signaling new data available.
    szObj = "SkunkWorks.queue" + szName + ".hevent"
    hevent = CreateEvent(0, True, False, szObj)
    Call Assert(hevent <> hInvalid, "Could not create event " + szObj + ".")
    
'   Create a region of shared memory to hold the queue data.
    szObj = "SkunkWorks.queue" + szName + ".hmap"
    hmap = OpenFileMapping(FILE_MAP_WRITE, False, szObj)
    If hmap = hNil Then
    '   Mapping does not exist yet; i.e. we're the first to try to open it.
    '   Create it instead.
        
        Dim osvi As OSVERSIONINFO
        osvi.dwOSVersionInfoSize = Len(osvi)
        Call GetVersionEx(osvi)
        If osvi.dwPlatformId <> VER_PLATFORM_WIN32_NT Then
        '   Win9X requires that we create a temp file to back it.
        '   (NT allows it to be backed by the paging file.)
            Dim szFile As String
            szFile = SzQueueFile(szName)
            hFile = CreateFile(szFile, GENERIC_READ Or GENERIC_WRITE, 0, 0, _
              CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY Or FILE_FLAG_DELETE_ON_CLOSE, hNil)
            Call Assert(hFile <> hInvalid, "Could not create queue file " + szFile + ".")
        End If
        
    '   Grow the file to the proper size.
        Call SetFilePointer(hFile, cbMap, 0, FILE_BEGIN)
        Call SetEndOfFile(hFile)
        
        hmap = CreateFileMapping(hFile, 0, PAGE_READWRITE, 0, cbMap, szObj)
        Call Assert(hmap <> hInvalid, "Could not create file mapping " + szObj + ".")
    End If
    
'   Now map it into memory.
    pbMap = MapViewOfFile(hmap, FILE_MAP_WRITE, 0, 0, cbMap)
    Call Assert(pbMap <> 0, "Could not view file mapping.")
    
'   Queue is organized as a simple ring buffer of bytes.
    prgbShared = pbMap
    cbShared = cbSharedA
    pibIn = pbMap + cbShared
    pibOut = pbMap + cbShared + 4
    prgbBuf = pbMap + cbShared + 8
    cbBuf = cbMap - (cbShared + 8)
    
    If hFile <> hInvalid Or mode = ForWriting Then
    '   Discard any fossil data in the queue.
        Call Acquire
        ibIn = 0
        ibOut = 0
        Call Release
    End If
    
'   Create a mutex for detecting whether a reader is connected.
    szObj = SzMutexConnect(szName)
    hmutexConnect = CreateMutex(0, False, szObj)
    Call Assert(hmutexConnect <> hInvalid, "Could not create mutex " + szObj + ".")
    
    fReading = (mode = ForReading)
    If fReading Then
    '   Signal the writer that we've connected to the read end of the queue.
        Select Case WaitForSingleObject(hmutexConnect, 0)
        Case WAIT_OBJECT_0, WAIT_ABANDONED
        '   Got it.
            FCreate = True
        Case Else
'            Call SkError(szName + " queue is in use.")
            FCreate = False
        End Select
    Else
        FCreate = True
    End If
    
'   If fTrace Then Call TraceExit("queue.FCreate", CStr(FCreate))
End Function


' Write a message of length cbIn to the queue.
Public Function FWriteMsg(rgbIn() As Byte, ByVal cbIn As Long, ByVal tickIn As Long, _
  ByVal cmsecWait As Long, ByVal fDoEvents As Boolean) As Boolean
    
    Dim fRelease As Boolean
    fRelease = False
    FWriteMsg = False
    On Error GoTo LExit
    
    Call CheckCmsec(cmsecWait)
    
'   Get exclusive access to the queue.
    Call Acquire
    fRelease = True
    
'   Calculate the total space requirement include timestamp, size, data,
'   and padding out to DWORD boundary.
    Dim cbTotal As Long
    cbTotal = (4 + 4 + cbIn + 3) And &HFFFFFFFC
    
    Do
    '   See if there's enough space available.
        Dim cbAvail As Long
        cbAvail = ibOut - 1 - ibIn
        If cbAvail < 0 Then cbAvail = cbAvail + cbBuf
        If cbAvail >= cbTotal Then Exit Do
        
    '   Not enough space; wait a bit for the other guy to free some up
    '   by reading out data.
        Call Assert(ResetEvent(hevent), "ResetEvent failed.")
        Call Release
        fRelease = False
        
        If fDoEvents And cmsecWait > 0 Then DoEvents
        
        If WaitForSingleObject(hevent, cmsecWait) <> WAIT_OBJECT_0 Then GoTo LExit
        
        Call Acquire
        fRelease = True
    Loop
    
    Dim ibDst As Long
    ibDst = ibIn
    
'   Store timestamp to queue.
    Call CopyMemToPv(prgbBuf + ibDst, tickIn, 4)
    ibDst = ibDst + 4
    If ibDst = cbBuf Then ibDst = 0
    
'   Store message length to queue.
    Call CopyMemToPv(prgbBuf + ibDst, cbIn, 4)
    ibDst = ibDst + 4
    If ibDst = cbBuf Then ibDst = 0
    
'   Store message body.
    Dim ibSrc As Long
    ibSrc = 0
    Do While ibSrc < cbIn
        Dim cbChunk As Long
        cbChunk = cbBuf - ibDst
        If cbChunk > cbIn - ibSrc Then cbChunk = cbIn - ibSrc
        Call CopyMemToPv(prgbBuf + ibDst, rgbIn(ibSrc), cbChunk)
        ibSrc = ibSrc + cbChunk
        ibDst = ibDst + cbChunk
        If ibDst = cbBuf Then ibDst = 0
    Loop
    
'   Round up to DWORD boundary.
    ibDst = (ibDst + 3) And &HFFFFFFFC
    If ibDst = cbBuf Then ibDst = 0
    
    ibIn = ibDst
    Call Assert(SetEvent(hevent), "SetEvent failed.")
    
    FWriteMsg = True
    
LExit:
    
    If fRelease Then Call Release
    
End Function

' Read a message from the queue.  Return the message body in rgbOut and the size in cbOut.
' Wait up to cmsecWait milliseconds for a message to appear.
Public Function FReadMsg(rgbOut() As Byte, ByRef cbOut As Long, ByRef tickOut As Long, _
  ByVal cmsecWait As Long, ByVal fDoEvents As Boolean) As Boolean
    
    Dim fRelease As Boolean
    fRelease = False
    FReadMsg = False
    On Error GoTo LExit
    
    Call CheckCmsec(cmsecWait)
    
'   Get exclusive access to the queue.
    Call Acquire
    fRelease = True
    
    Do
    '   See if there's a message available.
        Dim cbAvail As Long
        cbAvail = ibIn - ibOut
        If cbAvail < 0 Then cbAvail = cbAvail + cbBuf
        If cbAvail > 0 Then Exit Do
        
    '   No message; wait a bit for the other guy to queue one.
        Call Assert(ResetEvent(hevent), "ResetEvent failed.")
        Call Release
        fRelease = False
        
        If fDoEvents And cmsecWait > 0 Then DoEvents
        
        If WaitForSingleObject(hevent, cmsecWait) <> WAIT_OBJECT_0 Then GoTo LExit
        
        Call Acquire
        fRelease = True
    Loop
    
    Dim ibSrc As Long
    ibSrc = ibOut
    
'   Fetch timestamp from queue.
    Call CopyMemFromPv(tickOut, prgbBuf + ibSrc, 4)
    ibSrc = ibSrc + 4
    If ibSrc = cbBuf Then ibSrc = 0
    
'   Fetch message length from queue.
    Call CopyMemFromPv(cbOut, prgbBuf + ibSrc, 4)
    ibSrc = ibSrc + 4
    If ibSrc = cbBuf Then ibSrc = 0
    
'   Resize caller's buffer as needed.
    If cbOut > UBound(rgbOut) + 1 Then
        Call Assert(cbOut < 100000, "Bogus message size")
        Call TraceLine("Growing message buffer to " + _
          CStr(cbOut) + " bytes ... ")
        ReDim rgbOut(cbOut - 1)
    End If
    
'   Fetch message body.
    Dim ibDst As Long
    ibDst = 0
    Do While ibDst < cbOut
        Dim cbChunk As Long
        cbChunk = cbBuf - ibSrc
        If cbChunk > cbOut - ibDst Then cbChunk = cbOut - ibDst
        Call CopyMemFromPv(rgbOut(ibDst), prgbBuf + ibSrc, cbChunk)
        ibDst = ibDst + cbChunk
        ibSrc = ibSrc + cbChunk
        If ibSrc = cbBuf Then ibSrc = 0
    Loop
    
'   Round up to DWORD boundary.
    ibSrc = (ibSrc + 3) And &HFFFFFFFC
    If ibSrc = cbBuf Then ibSrc = 0
    
    ibOut = ibSrc
    Call Assert(SetEvent(hevent), "SetEvent failed.")
    
    FReadMsg = True
    
LExit:
    
    If fRelease Then Call Release
    
End Function

Public Sub PutShared(ByVal pv As Long, ByVal cb As Long)
    
    Call Assert(cb <= cbShared, "Shared memory overflow.")
    Call CopyPv(prgbShared, pv, cb)
    
End Sub

Public Sub GetShared(ByVal pv As Long, ByVal cb As Long)
    
    Call Assert(cb <= cbShared, "Shared memory overflow.")
    Call CopyPv(pv, prgbShared, cb)
    
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceEnter("Queue.Initialize")
    
    hmutex = hInvalid
    hevent = hInvalid
    hFile = hInvalid
    hmap = hInvalid
    pbMap = 0
    
    hmutexConnect = hInvalid
    fReading = True
    
    Call TraceExit("Queue.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceEnter("Queue.Terminate")
    
    If pbMap <> 0 Then Call UnmapViewOfFile(pbMap)
    If hmap <> hInvalid Then Call CloseHandle(hmap)
    If hFile <> hInvalid Then Call CloseHandle(hFile)
    If hevent <> hInvalid Then Call CloseHandle(hevent)
    If hmutex <> hInvalid Then Call CloseHandle(hmutex)
    
    If hmutexConnect <> hInvalid Then
        If fReading Then Call ReleaseMutex(hmutexConnect)
        Call CloseHandle(hmutexConnect)
    End If
    
    Call TraceExit("Queue.Terminate")
End Sub


Private Function SzMutexConnect(ByVal szName As String) As String
    
    SzMutexConnect = "SkunkWorks.queue" + szName + ".hmutexConnect"
    
End Function

Private Function SzQueueFile(ByVal szName As String) As String
    
    SzQueueFile = SzFileInTempDir("SkunkWorks" + szName + ".queue")
    
End Function

' Get/set the ring buffer's input pointer.
Private Property Get ibIn() As Long
'   If fTrace Then Call TraceEnter("queue.ibIn", "")
    
    Call CopyMemFromPv(ibIn, pibIn, Len(ibIn))
    
'   If fTrace Then Call TraceExit("queue.ibIn", CStr(ibIn))
End Property

Private Property Let ibIn(ByVal ibNew As Long)
'   If fTrace Then Call TraceEnter("queue.ibIn", CStr(ibNew))
    
    Call CopyMemToPv(pibIn, ibNew, Len(ibNew))
    
'   If fTrace Then Call TraceExit("queue.ibIn", "")
End Property


' Get/set the ring buffer's output pointer.
Private Property Get ibOut() As Long
'   If fTrace Then Call TraceEnter("queue.ibOut", "")
    
    Call CopyMemFromPv(ibOut, pibOut, Len(ibOut))
    
'   If fTrace Then Call TraceExit("queue.ibOut", CStr(ibOut))
End Property

Private Property Let ibOut(ByVal ibNew As Long)
'   If fTrace Then Call TraceEnter("queue.ibOut", CStr(ibNew))
    
    Call CopyMemToPv(pibOut, ibNew, Len(ibNew))
    
'   If fTrace Then Call TraceExit("queue.ibOut", "")
End Property


' Get exclusive access to the ring buffer.
Private Sub Acquire()
    
    Call Assert(WaitForSingleObject(hmutex, 100) <> WAIT_TIMEOUT, _
      "Wait for mutex failed.")
    
End Sub

' Relinquish exclusive access to the ring buffer.
Private Sub Release()
    
    Call Assert(ReleaseMutex(hmutex), "ReleaseMutex failed.")
    
End Sub


Private Sub PxPriority()
    
    Call DebugLine(App.EXEName + " priority = " + _
      "0x" + Hex(GetPriorityClass(GetCurrentProcess())) + ":" + _
      CStr(GetThreadPriority(GetCurrentThread())))
    
End Sub

Private Sub CheckCmsec(ByVal cmsec As Long)
    
    Call Assert(cmsec >= 0 And cmsec <= 10000, _
      "Bad queue timeout: " + CStr(cmsec))
    
End Sub


















































