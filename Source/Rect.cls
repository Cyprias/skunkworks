VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "RectCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' Script-compatible wrapper for tagRECT.


'--------------
' Private data
'--------------

Private rectMbr As Decal.tagRECT


'-------------------
' Public properties
'-------------------

Public Property Get Left() As Long
    Left = rectMbr.Left
End Property

Public Property Let Left(ByVal l As Long)
    rectMbr.Left = l
End Property


Public Property Get Top() As Long
    Top = rectMbr.Top
End Property

Public Property Let Top(ByVal l As Long)
    rectMbr.Top = l
End Property


Public Property Get Right() As Long
    Right = rectMbr.Right
End Property

Public Property Let Right(ByVal l As Long)
    rectMbr.Right = l
End Property


Public Property Get Bottom() As Long
    Bottom = rectMbr.Bottom
End Property

Public Property Let Bottom(ByVal l As Long)
    rectMbr.Bottom = l
End Property


Public Property Get width() As Long
    width = rectMbr.Right - rectMbr.Left
End Property

Public Property Let width(ByVal l As Long)
    rectMbr.Right = rectMbr.Left + l
End Property


Public Property Get height() As Long
    height = rectMbr.Bottom - rectMbr.Top
End Property

Public Property Let height(ByVal l As Long)
    rectMbr.Bottom = rectMbr.Top + l
End Property


'------------------
' Friend functions
'------------------

Friend Property Get rect() As Decal.tagRECT
    rect = rectMbr
End Property

Friend Property Let rect(ByRef rectNew As Decal.tagRECT)
    rectMbr = rectNew
End Property
















