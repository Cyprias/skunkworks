Attribute VB_Name = "Registry"
Option Explicit


'-------------
' Definitions
'-------------

' Win32 registry constants:
Public Const REG_NONE = 0                       ' No value type
Public Const REG_SZ = 1                         ' Unicode nul terminated string
Public Const REG_EXPAND_SZ = 2                  ' Unicode nul terminated string
Public Const REG_BINARY = 3                     ' Free form binary
Public Const REG_DWORD = 4                      ' 32-bit number
Public Const REG_DWORD_LITTLE_ENDIAN = 4        ' 32-bit number (same as REG_DWORD)
Public Const REG_DWORD_BIG_ENDIAN = 5           ' 32-bit number
Public Const REG_LINK = 6                       ' Symbolic Link (unicode)
Public Const REG_MULTI_SZ = 7                   ' Multiple Unicode strings
Public Const REG_RESOURCE_LIST = 8              ' Resource list in the resource map
Public Const REG_FULL_RESOURCE_DESCRIPTOR = 9   ' Resource list in the hardware description

Public Const HKEY_CLASSES_ROOT = &H80000000
Public Const HKEY_CURRENT_USER = &H80000001
Public Const HKEY_LOCAL_MACHINE = &H80000002
Public Const HKEY_USERS = &H80000003

Public Const ERROR_NONE = 0
Public Const ERROR_BADDB = 1
Public Const ERROR_BADKEY = 2
Public Const ERROR_CANTOPEN = 3
Public Const ERROR_CANTREAD = 4
Public Const ERROR_CANTWRITE = 5
Public Const ERROR_OUTOFMEMORY = 6
Public Const ERROR_INVALID_PARAMETER = 7
Public Const ERROR_ACCESS_DENIED = 8
Public Const ERROR_INVALID_PARAMETERS = 87
Public Const ERROR_NO_MORE_ITEMS = 259

Public Const SYNCHRONIZE = &H100000
Public Const STANDARD_RIGHTS_ALL = &H1F0000
' Reg Key Security Options
Public Const KEY_QUERY_VALUE = &H1
Public Const KEY_SET_VALUE = &H2
Public Const KEY_CREATE_SUB_KEY = &H4
Public Const KEY_ENUMERATE_SUB_KEYS = &H8
Public Const KEY_NOTIFY = &H10
Public Const KEY_CREATE_LINK = &H20
Public Const KEY_ALL_ACCESS = &H3F
'Public Const KEY_ALL_ACCESS = ((STANDARD_RIGHTS_ALL Or KEY_QUERY_VALUE Or KEY_SET_VALUE Or KEY_CREATE_SUB_KEY Or KEY_ENUMERATE_SUB_KEYS Or KEY_NOTIFY Or KEY_CREATE_LINK) And (Not SYNCHRONIZE))

Public Const REG_OPTION_NON_VOLATILE = 0

' Win32 registry functions:
Declare Function RegCloseKey Lib "advapi32.dll" _
  (ByVal hkey As Long) As Long
Declare Function RegCreateKeyEx Lib "advapi32.dll" Alias "RegCreateKeyExA" _
  (ByVal hkey As Long, ByVal lpSubKey As String, ByVal Reserved As Long, _
   ByVal lpClass As String, ByVal dwOptions As Long, ByVal samDesired As Long, _
   ByVal lpSecurityAttributes As Long, phkResult As Long, lpdwDisposition As Long) As Long
Declare Function RegDeleteKey Lib "advapi32.dll" Alias "RegDeleteKeyA" _
  (ByVal hkey As Long, ByVal lpSubKey As String) As Long
Declare Function RegEnumKeyEx Lib "advapi32.dll" Alias "RegEnumKeyExA" _
  (ByVal hkey As Long, ByVal dwIndex As Long, ByVal lpName As String, _
   lpcbName As Long, ByVal lpReserved As Long, ByVal lpClass As String, _
   lpcbClass As Long, lpftLastWriteTime As FILETIME) As Long
Declare Function RegEnumValue Lib "advapi32.dll" Alias "RegEnumValueA" _
  (ByVal hkey As Long, ByVal dwIndex As Long, ByVal lpValueName As String, _
   lpcbValueName As Long, ByVal lpReserved As Long, lpType As Long, ByVal lpData As String, lpcbData As Long) As Long
Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" _
  (ByVal hkey As Long, ByVal lpSubKey As String, ByVal ulOptions As Long, _
   ByVal samDesired As Long, phkResult As Long) As Long

Declare Function RegQueryValueExString Lib "advapi32.dll" Alias "RegQueryValueExA" _
  (ByVal hkey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, _
   lpType As Long, ByVal lpData As String, lpcbData As Long) As Long
Declare Function RegQueryValueExLong Lib "advapi32.dll" Alias "RegQueryValueExA" _
  (ByVal hkey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, _
   lpType As Long, lpData As Long, lpcbData As Long) As Long
Declare Function RegQueryValueExNULL Lib "advapi32.dll" Alias "RegQueryValueExA" _
  (ByVal hkey As Long, ByVal lpValueName As String, ByVal lpReserved As Long, _
   lpType As Long, ByVal lpData As Long, lpcbData As Long) As Long

Declare Function RegSetValueExString Lib "advapi32.dll" Alias "RegSetValueExA" _
  (ByVal hkey As Long, ByVal lpValueName As String, ByVal Reserved As Long, _
   ByVal dwType As Long, ByVal lpValue As String, ByVal cbData As Long) As Long
Declare Function RegSetValueExLong Lib "advapi32.dll" Alias "RegSetValueExA" _
  (ByVal hkey As Long, ByVal lpValueName As String, ByVal Reserved As Long, _
   ByVal dwType As Long, lpValue As Long, ByVal cbData As Long) As Long


'------
' APIs
'------

Public Function HkeyOpen(ByVal hkeyRoot As Long, ByVal szPath As String, _
  ByVal keyaccess As Long) As Long
    
    Dim hkey As Long
    If RegOpenKeyEx(hkeyRoot, szPath, 0, keyaccess, hkey) <> 0 Then
        hkey = hNil
    End If
    
    HkeyOpen = hkey
End Function

Public Function HkeyOpenOrCreate(ByVal hkeyRoot As Long, _
  ByVal szPath As String, ByVal keyaccess As Long) As Long
    
    Dim hkey As Long
    Dim lDisposition As Long
    If RegCreateKeyEx(hkeyRoot, szPath, 0, vbNullString, _
      REG_OPTION_NON_VOLATILE, keyaccess, 0, hkey, lDisposition) <> 0 Then
        hkey = hNil
    End If
    
    HkeyOpenOrCreate = hkey
End Function


Public Function FSetDwordValue(ByVal hkey As Long, ByVal szValName As String, _
  ByVal lVal As Long) As Boolean
    
    FSetDwordValue = (RegSetValueExLong(hkey, szValName, 0, REG_DWORD, _
      lVal, 4) = 0)
    
End Function

Public Function FGetDwordValue(ByRef lVal As Long, _
  ByVal hkey As Long, ByVal szValName As String) As Boolean
    FGetDwordValue = False
    
    Dim dwType As Long, cbData As Long
    cbData = 4
    If RegQueryValueExLong(hkey, szValName, 0, dwType, lVal, cbData) <> 0 Then
        Exit Function
    End If
    If dwType <> REG_DWORD Or cbData <> 4 Then
        Exit Function
    End If
    
    FGetDwordValue = True
End Function


Public Function FSetSzValue(ByVal hkey As Long, ByVal szValName As String, _
  ByVal szVal As String) As Boolean
    
    FSetSzValue = (RegSetValueExString(hkey, szValName, 0, REG_SZ, _
      szVal + Chr(0), Len(szVal) + 1) = 0)
    
End Function

Public Function FGetSzValue(ByRef szVal As String, _
  ByVal hkey As Long, ByVal szValName As String) As Boolean
    FGetSzValue = False
    
    Dim dwType As Long, cbData As Long
    cbData = 0
    If RegQueryValueExString(hkey, szValName, 0, dwType, vbNullString, cbData) <> 0 Then
        Exit Function
    End If
    If dwType <> REG_SZ And dwType <> REG_EXPAND_SZ Then
        Exit Function
    End If
    
    szVal = String(cbData, vbNullChar)
    If RegQueryValueExString(hkey, szValName, 0, dwType, szVal, cbData) <> 0 Then
        Exit Function
    End If
    szVal = SzTrimNulls(szVal)
    
    FGetSzValue = True
End Function


Public Function FSetKeyDefaultValSz(ByVal hkeyRoot As Long, ByVal szPath As String, _
  ByVal szVal As String) As Boolean
    Dim hkey As Long
    hkey = hNil
    
    hkey = HkeyOpenOrCreate(hkeyRoot, szPath, KEY_ALL_ACCESS)
    If hkey = hNil Then GoTo LError
    
    If Not FSetSzValue(hkey, "", szVal) Then GoTo LError
    
    Call RegCloseKey(hkey)
    
    FSetKeyDefaultValSz = True
    Exit Function
    
LError:

    If hkey <> hNil Then Call RegCloseKey(hkey)
    
    FSetKeyDefaultValSz = False
End Function


Public Function FEnumKey(ByRef szName As String, ByRef szClass As String, _
  ByVal hkey As Long, ByVal ikey As Long) As Boolean
    
    FEnumKey = False
    
    Dim cchName As Long, cchClass As Long, ft As FILETIME
    cchName = 100
    cchClass = 100
    szName = String(cchName, vbNullChar)
    szClass = String(cchClass, vbNullChar)
    
    If RegEnumKeyEx(hkey, ikey, szName, cchName, 0, szClass, cchClass, ft) <> 0 Then Exit Function
    
    szName = SzTrimNulls(szName)
    szClass = SzTrimNulls(szClass)
    
    FEnumKey = True
End Function


' Register an file type by extension.
' Program path is the full path to the application executable file.
' Icon path to the executable or icon file that contains the icon for registered file types.  If this value is not given, it will use icon in ProgramPath.'
' szFileType is the name of your application, it should contain no spaces. '
' szFileDesc is the descripting for your application. '
' Ex. x = RegisterFile("3do", "c:\path\szFileType.exe","c:\path\icon.ico", "ApplicationName","A very nice application")
Public Function FRegisterFileType(ByVal szExt As String, _
  ByVal szProgramPath As String, Optional ByVal szIconPath As String, _
  Optional ByVal szFileType As String, Optional ByVal szFileDesc As String, _
  Optional ByVal szShellCommand As String) As Boolean
    
    FRegisterFileType = False    ' Assume the worst.
    
    If InStr(szExt, ".") = 0 Then szExt = "." & szExt
    If Trim(szIconPath) = "" Then szIconPath = szProgramPath
    If (UCase(szIconPath) Like "*.EXE") Or (UCase(szIconPath) Like "*.DLL") Then szIconPath = szIconPath & ",0"
    If szFileType = "" Then szFileType = "Reg.Application"
    If szFileDesc = "" Then szFileDesc = "RegDescription"
    If szShellCommand = "" Then szShellCommand = "open"
    
'   Define extension.
    If Not FSetKeyDefaultValSz(HKEY_CLASSES_ROOT, szExt, szFileType) Then
        Exit Function
    End If
    
'   Define file type.
    If Not FSetKeyDefaultValSz(HKEY_CLASSES_ROOT, szFileType, szFileDesc) Then
        Exit Function
    End If
    
'   Set icon.
    If Not FSetKeyDefaultValSz(HKEY_CLASSES_ROOT, szFileType + "\DefaultIcon", _
      szIconPath) Then
        Exit Function
    End If
    
'   Create shell command.
    Dim szRegPathCommand As String, szCmdLine
    szRegPathCommand = szFileType & "\shell\" + szShellCommand + "\command"
    szCmdLine = SzQuoteFilename(szProgramPath) + " %1"
    If Not FSetKeyDefaultValSz(HKEY_CLASSES_ROOT, szRegPathCommand, _
      szCmdLine) Then
        Exit Function
    End If
    
    FRegisterFileType = True
End Function

' Delete a registered file type.
Public Sub DeleteFileType(ByVal szFileType As String)
    
    Call RegDeleteKey(HKEY_CLASSES_ROOT, szFileType)
    
End Sub


' Return the path of the shell folder specified by szValName.
Public Function SzShellFolder(ByVal szValName As String) As String
    SzShellFolder = ""
    
    Dim hkey As Long
    hkey = HkeyOpen(HKEY_CURRENT_USER, _
      "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", _
      KEY_QUERY_VALUE)
    If hkey = hNil Then Exit Function
    
    Dim szVal As String
    If Not FGetSzValue(szVal, hkey, szValName) Then Exit Function
    
    Call RegCloseKey(hkey)
    
    SzShellFolder = szVal
End Function


'----------
' Privates
'----------















