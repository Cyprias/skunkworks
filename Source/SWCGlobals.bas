Attribute VB_Name = "SWCGlobals"
Option Explicit


Public fVBInstalled As Boolean
Public swoptions As SWOptionsCls

Public decalapp As DecalCls

Public skapi As SkapiCls


Public Sub Init()
    
    Call CheckVBInstalled
    
    Set decalapp = New DecalCls
    
End Sub


Private Sub CheckVBInstalled()
    
    fVBInstalled = False
    
    Dim hkey As Long
    hkey = HkeyOpen(HKEY_LOCAL_MACHINE, _
      "SOFTWARE\Microsoft\VisualStudio\6.0\Setup\Microsoft Visual Basic", KEY_QUERY_VALUE)
    If hkey <> hNil Then
        Dim szDir As String
        If FGetSzValue(szDir, hkey, "ProductDir") Then
            fVBInstalled = fso.FileExists(fso.BuildPath(szDir, "VB6.exe"))
        End If
        Call RegCloseKey(hkey)
    End If
    
End Sub













