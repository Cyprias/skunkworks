VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form formSWConsole 
   Caption         =   "SkunkWorks"
   ClientHeight    =   5805
   ClientLeft      =   60
   ClientTop       =   630
   ClientWidth     =   6870
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "SWConsole.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   5805
   ScaleWidth      =   6870
   Begin VB.TextBox textCmdLine 
      Height          =   285
      Left            =   120
      Locked          =   -1  'True
      TabIndex        =   10
      TabStop         =   0   'False
      Top             =   5160
      Visible         =   0   'False
      Width           =   6615
   End
   Begin VB.TextBox textOutput 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3255
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   9
      TabStop         =   0   'False
      Top             =   1800
      Width           =   6612
   End
   Begin MSComctlLib.StatusBar statbar 
      Align           =   2  'Align Bottom
      Height          =   285
      Left            =   0
      TabIndex        =   8
      Top             =   5520
      Width           =   6870
      _ExtentX        =   12118
      _ExtentY        =   503
      Style           =   1
      SimpleText      =   "Loading..."
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Timer timerMaintain 
      Enabled         =   0   'False
      Interval        =   50
      Left            =   5640
      Top             =   0
   End
   Begin VB.Frame frameScriptFile 
      Height          =   1575
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   6615
      Begin VB.CommandButton cmdOptions 
         Caption         =   "Options"
         Enabled         =   0   'False
         Height          =   315
         Left            =   5520
         TabIndex        =   4
         Top             =   1080
         Width           =   972
      End
      Begin SWConsole.MrulistCtl mrulistScript 
         Height          =   372
         Left            =   120
         TabIndex        =   0
         Top             =   480
         Width           =   5652
         _ExtentX        =   9975
         _ExtentY        =   661
         szApp           =   "SkunkWorks"
         szTitle         =   "Open Project"
         Enabled         =   0   'False
      End
      Begin VB.TextBox textArgs 
         Height          =   285
         Left            =   120
         TabIndex        =   3
         Top             =   1080
         Width           =   5292
      End
      Begin VB.CommandButton cmdStop 
         Caption         =   "Stop"
         Enabled         =   0   'False
         Height          =   315
         Left            =   5880
         TabIndex        =   2
         Top             =   840
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.CommandButton cmdGo 
         Caption         =   "Go"
         Enabled         =   0   'False
         Height          =   315
         Left            =   5880
         TabIndex        =   1
         Top             =   480
         Width           =   615
      End
      Begin VB.Label labelScript 
         Caption         =   "Script file:"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   975
      End
      Begin VB.Label labelArgs 
         Caption         =   "Arguments:"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   840
         Width           =   975
      End
   End
   Begin VB.Menu menuFile 
      Caption         =   "File"
      Begin VB.Menu menuNewScript 
         Caption         =   "New"
      End
      Begin VB.Menu menuLoadScript 
         Caption         =   "Open..."
      End
      Begin VB.Menu menuSep1 
         Caption         =   "-"
      End
      Begin VB.Menu menuFileClear 
         Caption         =   "Clear History"
         Enabled         =   0   'False
      End
      Begin VB.Menu menuSep2 
         Caption         =   "-"
      End
      Begin VB.Menu menuQuit 
         Caption         =   "Exit"
      End
   End
   Begin VB.Menu menuConfig 
      Caption         =   "Configure"
      Begin VB.Menu menuLoginScript 
         Caption         =   "Login Script..."
      End
      Begin VB.Menu menuSep3 
         Caption         =   "-"
      End
      Begin VB.Menu menuGenCrackers 
         Caption         =   "Regen Crackers"
      End
      Begin VB.Menu menuSep4 
         Caption         =   "-"
      End
      Begin VB.Menu menuDiagnostics 
         Caption         =   "Diagnostics"
         Begin VB.Menu menuTrace 
            Caption         =   "Trace"
         End
         Begin VB.Menu menuMemStats 
            Caption         =   "Mem Stats"
         End
         Begin VB.Menu menuLog 
            Caption         =   "Log Startup"
         End
         Begin VB.Menu menuCapture 
            Caption         =   "Capture Messages"
         End
         Begin VB.Menu menuSep5 
            Caption         =   "-"
         End
         Begin VB.Menu menuPxEvents 
            Caption         =   "Px Events"
            Begin VB.Menu menuPxe 
               Caption         =   "None"
               Index           =   0
            End
            Begin VB.Menu menuPxe 
               Caption         =   "All but Loc+Anim"
               Index           =   1
            End
            Begin VB.Menu menuPxe 
               Caption         =   "All"
               Index           =   2
            End
            Begin VB.Menu menuSep6 
               Caption         =   "-"
            End
            Begin VB.Menu menuPxeToChat 
               Caption         =   "To Chat Window"
            End
         End
         Begin VB.Menu menuPxMsgs 
            Caption         =   "Px Messages"
            Begin VB.Menu menuPxm 
               Caption         =   "None"
               Index           =   0
            End
            Begin VB.Menu menuPxm 
               Caption         =   "Unknown"
               Index           =   1
            End
            Begin VB.Menu menuPxm 
               Caption         =   "Unhandled"
               Index           =   2
            End
            Begin VB.Menu menuPxm 
               Caption         =   "All"
               Index           =   3
            End
            Begin VB.Menu menuOidPx 
               Caption         =   "Specific OID..."
            End
            Begin VB.Menu menuSep7 
               Caption         =   "-"
            End
            Begin VB.Menu menuPxCTS 
               Caption         =   "Client-to-Server"
            End
            Begin VB.Menu menuPxWholeMsg 
               Caption         =   "Whole Message"
            End
         End
         Begin VB.Menu menuSep8 
            Caption         =   "-"
         End
         Begin VB.Menu menuDisplayOtable 
            Caption         =   "Display Otable"
         End
      End
      Begin VB.Menu menuDev 
         Caption         =   "Developer"
         Begin VB.Menu menuGenSkapiDefs 
            Caption         =   "Regen SkapiDefs"
         End
         Begin VB.Menu menuGenSkev 
            Caption         =   "Regen Skev"
         End
      End
   End
   Begin VB.Menu menuDebug 
      Caption         =   "Debug"
      Begin VB.Menu menuEnableDebug 
         Caption         =   "Enable Script Debugger"
      End
      Begin VB.Menu menuBreakOnEntry 
         Caption         =   "Break on Entry"
      End
   End
   Begin VB.Menu menuHelp 
      Caption         =   "Help"
      Begin VB.Menu menuDocs 
         Caption         =   "Contents"
      End
      Begin VB.Menu menuSep9 
         Caption         =   "-"
      End
      Begin VB.Menu menuAbout 
         Caption         =   "About SkunkWorks..."
      End
   End
End
Attribute VB_Name = "formSWConsole"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' The SkunkWorks console window.


'--------------
' Private data
'--------------

' The original caption of this form.
Private szCaptionOrig As String

Private xmldocControls As DOMDocument40
Private szPanelName As String
Private fNeedControls As Boolean

Private WithEvents skapi As SkapiCls
Attribute skapi.VB_VarHelpID = -1

Private swxfile As SwxfileCls
Private coswxsite As Collection

Private fInWaitEvent As Boolean
Private fStartScript As Boolean
Private fScriptRunning As Boolean
Public fError As Boolean

Private fQuit As Boolean
Private fLoaded As Boolean
Private fErrorExit As Boolean

Private Enum StatusType
    statusInit
    statusReady
    statusLoading
    statusRunning
    statusStopping
End Enum
Private statusMbr As StatusType

Private Const opmStatus = &H8

Private Type CmdlineType
    fSilent As Boolean
    swxfile As SwxfileCls
    szArgs As String
    fRun As Boolean
    fStop As Boolean
    fQuit As Boolean
End Type


Public szScriptNext As String


Private Declare Sub CoFreeUnusedLibraries Lib "ole32" ()


'---------
' Methods
'---------

Public Sub Alert(ByVal szMsg As String, _
  Optional ByVal idIcon As Long = IDI_INFORMATION)
    Call formMsgBox.Alert(szMsg, idIcon, Me)
End Sub

Public Function FConfirmYesNo(ByVal szMsg As String, _
  Optional fDefault As Boolean = True, _
  Optional ByVal idIcon As Long = IDI_QUESTION) As Boolean
    FConfirmYesNo = formMsgBox.FConfirmYesNo(szMsg, fDefault, idIcon, Me)
End Function

Public Function FConfirmOKCancel(ByVal szMsg As String, _
  Optional fDefault As Boolean = True, _
  Optional ByVal idIcon As Long = IDI_QUESTION) As Boolean
    FConfirmOKCancel = formMsgBox.FConfirmOKCancel(szMsg, fDefault, idIcon, Me)
End Function

Public Function StopScript()
    Call TraceEnter("StopScript")
    
    Call SetStatus(statusStopping)
    
'   Stop the script engine(s).
    Dim swxsite As SwxsiteCls
    For Each swxsite In coswxsite
        Call swxsite.site.Stop
    Next swxsite
    
'   Tell WaitEvent to cut short its current timeout.
    Call skapi.ExitWaitEvent
    
    Call TraceExit("StopScript")
End Function


'----------
' Controls
'----------

Private Sub Form_Load()
    On Error GoTo LError
    
    Screen.MousePointer = vbHourglass
    
    Set swoptions = New SWOptionsCls
    If swoptions.fLog Then
        Call SWError.StartLog(swoptions.SzFileInAppDir("SWConsole.log"))
    End If
    
    fErrorExit = False
    statusMbr = statusInit
    
    Set coswxsite = New Collection
    
    Call SWCGlobals.Init
    Call CheckReqs
    Call InitSWLang
    Call TraceLine("Messages.xml version " + decalapp.szVerMessages)
    
'   Remember the window's original caption (which changes when a script is
'   running).
    szCaptionOrig = Caption
    
'   Filename filter string is too unwieldy to set at design time so do it now.
    mrulistScript.szFilter = SzFilterFromSzLang("", True, False)
    
'   Init diagnostic controls as specified in registry.
    Call EnableControls
    
'   Restore previous window size and position.
    Call RestoreFormPosition(Me, "Console", True, swoptions.szAppName)
'   Force a Resize event in the case when the size doesn't actually change
'   so that controls get laid out properly the first time.
    Call Form_Resize
    
'   Load in-game control panel schema as an XML doc.
    Call LoadControls
    
'   Make the window appear on screen.
    Call Show
    Call Refresh
    Screen.MousePointer = vbHourglass
    DoEvents
    
'   Create the skapi object.
    Set skapi = New SkapiCls
    Set SWCGlobals.skapi = skapi
    timerMaintain.Enabled = True
    
'   Register our output handlers.
    Call skapi.SetOutputHandler(opmConsole, Me)
    Call skapi.SetOutputHandler(opmStatus, Me)
    
'   Register our control-panel event handlers.
    Call RegisterConsoleHandlers
    
    Call Banner(opmConsole)
    
    If Not decalapp.FPluginEnabled(szClsidSWPlugin) Then
        Call OutputToConsole(">> SkunkWorks plugin is not enabled." + vbCrLf)
    End If
    If Not decalapp.FFilterEnabled(szClsidSWFilter) Then
        Call OutputToConsole(">> SkunkWorks filter is not enabled." + vbCrLf)
    End If
    
    fLoaded = True
    
    Dim cmdline As CmdlineType
    Call ParseCmdLine(cmdline, Command)
    Call ExecuteCmdLine(cmdline)
    If Not fLoaded Then Exit Sub
    
    Call SetStatus(statusReady)
    Call FlushLog
    
    Exit Sub
    
LError:
    Dim errno As Long, szSrc As String, szError As String
    errno = Err.Number
    szSrc = Err.Source
    szError = Err.Description
    If FEqSzI(szError, "Events queue is in use.") Then
        If FDelegateCmdLine(Command) Then
            fErrorExit = True
            Call Unload(Me)
            Exit Sub
        End If
        szError = "Another instance of the SkunkWorks console is already running."
    End If
    Call LogError(szSrc, szError, errno)
    Call MsgBox(szError, vbCritical + vbOKOnly)
    fErrorExit = True
    Call Unload(Me)
    
End Sub

Private Sub Form_Resize()
    
    If ScaleWidth > 0 Then
    '   Lay out controls to conform to new window size.
        Dim dxMargin As Long, dyMargin As Long
        dxMargin = textOutput.Left
        dyMargin = dxMargin
        
        Const dxMinFrame = 1500
        Const dxMinBlank = 1000
        Const dyMinOutput = 900
        
        Dim dxFrame As Long
        dxFrame = ScaleWidth - dxMargin * 2
        If dxFrame < dxMinFrame Then dxFrame = dxMinFrame
        Call frameScriptFile.Move(dxMargin, 0, dxFrame)
        
        cmdGo.Left = frameScriptFile.Width - dxMargin - cmdGo.Width
        Call cmdStop.Move(cmdGo.Left, cmdGo.Top, cmdGo.Width, cmdGo.Height)
        Dim dxMrulist As Long
        dxMrulist = cmdGo.Left - dxMargin * 2
        If dxMrulist < dxMinBlank Then dxMrulist = dxMinBlank
        Call mrulistScript.Move(dxMargin, mrulistScript.Top, dxMrulist)
        
        cmdOptions.Left = frameScriptFile.Width - dxMargin - cmdOptions.Width
        Dim dxArgs As Long
        dxArgs = cmdOptions.Left - dxMargin * 2
        If dxArgs < dxMinBlank Then dxArgs = dxMinBlank
        Call textArgs.Move(dxMargin, textArgs.Top, dxArgs)
        
        Dim yOutput As Long, dyOutput As Long
        yOutput = frameScriptFile.Top + frameScriptFile.Height + dyMargin
        dyOutput = ScaleHeight - statbar.Height - dyMargin - yOutput
        If dyOutput < dyMinOutput Then dyOutput = dyMinOutput
        Call textOutput.Move(dxMargin, yOutput, _
          ScaleWidth - dxMargin * 2, dyOutput)
        
    '   Save new size/position in registry.
        Call SaveFormPosition(Me, "Console", swoptions.szAppName)
    End If
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    
'   Don't ask questions if load failed.
    If fErrorExit Then Exit Sub
    
'   Save window size/position in registry.
    Call SaveFormPosition(Me, "Console", swoptions.szAppName)
    
    If skapi.plig > pligNotRunning Then
        If Not FConfirmYesNo( _
          "The game is still running.  If you close the console now, " + _
          "you'll have to quit and restart the game before running " + _
          "any more scripts." + vbCrLf + vbCrLf + "Close anyway?", _
          False, IDI_EXCLAMATION) Then
            Cancel = True
            Exit Sub
        End If
    End If
    
'   Stop the script if it's running.
    If fScriptRunning Then
        If Not FConfirmStopScript() Then
            Cancel = True
            Exit Sub
        End If
    End If
    
    If Not fNeedControls Then
        Call skapi.RemoveControls(szPanelName)
        fNeedControls = True
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call SWError.StopLog
    fLoaded = False
    
End Sub


' Create a new script file.
Private Sub menuNewScript_Click()
    
    Dim swxfileNew As SwxfileCls
    Set swxfileNew = New SwxfileCls
    If formSwxfile.FEditSwxfile(swxfileNew, Me) Then
        mrulistScript.szFile = swxfileNew.szFile
    End If
    
End Sub

' Browse for a script file.
Private Sub menuLoadScript_Click()
    
    Call mrulistScript.Browse
    
End Sub

' Clear the MRU list.
Private Sub menuFileClear_Click()
    
    Call mrulistScript.Clear
    
End Sub

' Exit the application.
Private Sub menuQuit_Click()
    
    Call Unload(Me)
    
End Sub


Private Sub menuLoginScript_Click()
    
    Call formSwoptions.Show(vbModal, Me)
    
End Sub

' Regenerate message crackers on demand.
Private Sub menuGenCrackers_Click()
    
    Dim fQuit As Boolean
    Call CheckCrackers(fQuit, True)
    If fQuit Then
        Call Unload(Me)
    End If
    
End Sub

' Regenerate SkapiDefs on demand.
Private Sub menuGenSkapiDefs_Click()
    
    Call CheckSkapiDefs(True)
    
End Sub

' Regenerate events on demand.
Private Sub menuGenSkev_Click()
    
    Call GenSkev
    
End Sub


Private Sub menuTrace_Click()
    
    swoptions.fTrace = Not swoptions.fTrace
    menuTrace.Checked = swoptions.fTrace
    
    Dim swxsite As SwxsiteCls
    For Each swxsite In coswxsite
        swxsite.site.fTrace = swoptions.fTrace
    Next swxsite
    
    If Not (skapi Is Nothing) Then Call skapi.ReloadOptions
    
End Sub

Private Sub menuMemStats_Click()
    
    swoptions.fMemStats = Not swoptions.fMemStats
    menuMemStats.Checked = swoptions.fMemStats
    If Not (skapi Is Nothing) Then Call skapi.ReloadOptions
    
End Sub

Private Sub menuLog_Click()
    
    swoptions.fLog = Not swoptions.fLog
    menuLog.Checked = swoptions.fLog
    Call Alert("This change will take effect the next time you start SkunkWorks.")
    
End Sub

Private Sub menuCapture_Click()
    
    swoptions.fCapture = Not swoptions.fCapture
    menuCapture.Checked = swoptions.fCapture
    Call Alert("This change will take effect the next time you start AC.")
    
End Sub


' Debugging aid: change the event log filter.
Private Sub menuPxe_Click(Index As Integer)
    
    swoptions.pxe = Index
    Call CheckPxe
    If Not (skapi Is Nothing) Then Call skapi.ReloadOptions
    
End Sub

' Debugging aid: log events to chat window or just to debug log.
Private Sub menuPxeToChat_Click()
    
    menuPxeToChat.Checked = Not menuPxeToChat.Checked
    swoptions.opmPxe = IIf(menuPxeToChat.Checked, opmChatWnd Or opmDebugLog, opmDebugLog)
    If Not (skapi Is Nothing) Then Call skapi.ReloadOptions
    
End Sub

' Debugging aid: change the message dump filter.
Private Sub menuPxm_Click(Index As Integer)
    
    swoptions.pxm = Index
    skapi.oidPx = oidNil
    Call CheckPxm
    If Not (skapi Is Nothing) Then Call skapi.ReloadOptions
    
End Sub

Private Sub menuOidPx_Click()
    
    Dim szOid As String
    szOid = InputBox("Enter the OID to trace:", "SkunkWorks", _
      SzHex(skapi.oidPx, 8))
    If szOid = "" Then Exit Sub
    
    Dim oid As Long
    On Error Resume Next
    oid = CLng("&H" + szOid)
    If Err.Number <> 0 Then
        Call Alert("Invalid OID.", IDI_EXCLAMATION)
        Exit Sub
    End If
    
    skapi.oidPx = oid
    swoptions.pxm = pxmNone
    Call CheckPxm
    
End Sub

' Debugging aid: dump client-to-server messages.
Private Sub menuPxCTS_Click()
    
    swoptions.fPxCTS = Not swoptions.fPxCTS
    menuPxCTS.Checked = swoptions.fPxCTS
    If Not (skapi Is Nothing) Then Call skapi.ReloadOptions
    
End Sub

' Debugging aid: dump whole messages or just headers.
Private Sub menuPxWholeMsg_Click()
    
    swoptions.fPxWholeMsg = Not swoptions.fPxWholeMsg
    menuPxWholeMsg.Checked = swoptions.fPxWholeMsg
    If Not (skapi Is Nothing) Then Call skapi.ReloadOptions
    
End Sub


' Debugging aid: display the object table in a window.
Private Sub menuDisplayOtable_Click()
    Call skapi.DisplayOtable
End Sub


Private Sub menuEnableDebug_Click()
    
    swoptions.fEnableDebug = Not swoptions.fEnableDebug
    menuEnableDebug.Checked = swoptions.fEnableDebug
    menuBreakOnEntry.Enabled = swoptions.fEnableDebug
    
End Sub

Private Sub menuBreakOnEntry_Click()
    
    swoptions.fBreakOnEntry = Not swoptions.fBreakOnEntry
    menuBreakOnEntry.Checked = swoptions.fBreakOnEntry
    
End Sub


' Open the User Guide document.
Private Sub menuDocs_Click()
    
    Call ShellExecute(hwndNil, "Open", _
      fso.BuildPath(swoptions.szDocsDir, "SkunkWorks.html"), _
      vbNullString, vbNullString, SW_SHOW)
    
End Sub

' Show the About dialog.
Private Sub menuAbout_Click()
    
    Call formAbout.Show(vbModal, Me)
    
End Sub


Private Sub mrulistScript_Change(ByVal szFile As String)
    
    If szFile <> "" Then
        Set swxfile = New SwxfileCls
        If FEqSzFile(SzExtension(szFile), "swx") Then
            Call swxfile.Load(szFile)
        Else
            Call swxfile.InsertScript(iNil, szFile)
        End If
        textArgs.Text = swxfile.szArgs
        cmdGo.Enabled = (statusMbr = statusReady)
        cmdOptions.Enabled = (statusMbr = statusReady)
    Else
        Set swxfile = Nothing
        textArgs.Text = ""
        cmdGo.Enabled = False
        cmdOptions.Enabled = False
    End If
    
    If Not fNeedControls And Not fScriptRunning Then
        Call UpdateScriptList(szFile)
    End If
    
End Sub


' Run the script specified in the dropdown mrulist control.
Private Sub cmdGo_Click()
    
    If fInWaitEvent Then
    '   Don't start the script inside WaitEvent.  Wait until we get
    '   back out to timerMaintain_Timer() and start it from there.
        fStartScript = True
    Else
    '   Load and run the script.
        Do
            szScriptNext = ""
            Call RunScript
            If szScriptNext = "" Then Exit Do
            mrulistScript.szFile = szScriptNext
        Loop
    End If
    
End Sub

' Stop the currently running script.
Private Sub cmdStop_Click()
    
    If fScriptRunning Then
        Call FConfirmStopScript
    End If
    
End Sub

Private Sub cmdOptions_Click()
    
    If Not FEqSzFile(SzExtension(swxfile.szFile), "swx") Then
        swxfile.szArgs = textArgs.Text
    End If
    
    If formSwxfile.FEditSwxfile(swxfile, Me) Then
        mrulistScript.szFile = swxfile.szFile
    End If
    
End Sub


Private Sub textCmdLine_Change()
    On Error GoTo LError
    
    Static fIgnoreCmdLineChange As Boolean
    If fIgnoreCmdLineChange Then Exit Sub
    
    Dim cmdline As CmdlineType
    Call ParseCmdLine(cmdline, textCmdLine.Text)
    cmdline.fSilent = True
    
    fIgnoreCmdLineChange = True
    textCmdLine.Text = ""
    fIgnoreCmdLineChange = False
    
    If fScriptRunning Then
        If cmdline.swxfile Is Nothing Then
            If cmdline.fStop Then
                Call StopScript
            End If
        Else
            Call skapi.OutputLine(">> Script running; command line ignored.", _
              opmDebugLog + opmConsole)
        End If
    Else
        Call ExecuteCmdLine(cmdline)
    End If
    
    Exit Sub
    
LError:
    Call LogError(Err.Source, Err.Description, Err.Number)
    Call skapi.OutputLine(">> " + Err.Source + ": " + Err.Description, _
      opmDebugLog + opmConsole)
    Call Err.Clear
    
End Sub


Private Sub timerMaintain_Timer()
    On Error GoTo LError
    
    Debug.Assert Not fScriptRunning
    
    fStartScript = False
    
'   Keep inventory and object tables up to date between scripts.
    fInWaitEvent = True
    Call skapi.WaitEvent(0)
    fInWaitEvent = False
    
    If fStartScript Then
        Call cmdGo_Click
    End If
    
    Exit Sub
    
LError:
    Call LogError(Err.Source, Err.Description, Err.Number)
    Call Err.Clear
    
End Sub




Private Sub skapi_HandlerError(ByVal szHandler As String, _
  ByVal szError As String, ByVal errno As Long)
    
    If coswxsite.Count > 0 Then
        Dim swxsite As SwxsiteCls
        Set swxsite = coswxsite(1)
        If swxsite.site.fInterrupted Then Exit Sub
    End If
    
    Call skapi.OutputLine(">> " + szHandler + ": " + szError, _
      opmDebugLog + opmConsole + opmChatWnd)
    fError = True
    
End Sub


'----------------
' Event handlers
'----------------

Public Sub OnStart3D()
    Call TraceEnter("SWConsole.OnStart3D")

    Call PopulateScriptList
    Call SetControlState
    
'   Create the panel in the rolled-up state.
    Call skapi.ShowControls(xmldocControls.xml, False)
    fNeedControls = False
    
    Call Banner(opmChatWnd)
    
    Call TraceExit("SWConsole.OnStart3D")
End Sub

Public Sub OnLogon(acoChar)
    Call TraceEnter("SWConsole.OnLogon")
    
    If swoptions.szLoginScript <> "" And Not fScriptRunning Then
        If FFileExists(swoptions.szLoginScript) Then
            mrulistScript.szFile = swoptions.szLoginScript
            Call cmdGo_Click
        End If
    End If
    
    Call TraceExit("SWConsole.OnLogon")
End Sub

Public Sub OnEnd3D()
    Call TraceEnter("SWConsole.OnEnd3D")
    
    fNeedControls = True
    
    Call TraceExit("SWConsole.OnEnd3D")
End Sub

Public Sub OnControlEvent(szPanel, szControl, dictSzValue)
    Call TraceEnter("SWConsole.OnControlEvent", _
      SzQuote(szPanel) + ", " + SzQuote(szControl))
    
    Select Case szPanel
        
    Case szPanelName
        Select Case szControl
            
        Case "btnBrowse"
            If Not fScriptRunning Then
                Call skapi.ShowControls(swoptions.SzFileInLibDir("FileBrowser.xml"))
                Call skapi.CallPanelFunction("FileBrowser", "Init", swoptions.szAppDir, "*.swx")
            End If
            
        Case "btnGoStop"
            Dim szButton As String
            szButton = dictSzValue("btnGoStop")
            If statusMbr = statusReady And FEqSzI(szButton, "Go") Then
                Call skapi.RemoveControls("FileBrowser")
                Dim szScript As String
                szScript = dictSzValue("lboxScript")
                If szScript <> "" Then
                    mrulistScript.szFile = szScript
                    Call cmdGo_Click
                Else
                    Call skapi.OutputLine("No script selected.", opmChatWnd)
                    Call Beep
                End If
            ElseIf statusMbr = statusRunning And FEqSzI(szButton, "Stop") Then
                Call StopScript
            End If
            
        Case "chkTrace"
            swoptions.fTrace = FEqSzI(dictSzValue("chkTrace"), "true")
            menuTrace.Checked = swoptions.fTrace
            Dim swxsite As SwxsiteCls
            For Each swxsite In coswxsite
                swxsite.site.fTrace = swoptions.fTrace
            Next swxsite
            If Not (skapi Is Nothing) Then Call skapi.ReloadOptions
            
        End Select
        
    Case "FileBrowser"
        Select Case szControl
            
        Case "btnOK"
            If Not fScriptRunning Then
                mrulistScript.szFile = dictSzValue("textFile")
            End If
            
        End Select
    
    End Select
    
    Call TraceExit("SWConsole.OnControlEvent")
End Sub


' opmStatus handler:
Public Sub OutputSz(ByVal sz As String, ByVal opm As Long, ByVal cmc As Long)
    
    If opm = opmConsole Then
        Call OutputToConsole(sz)
    Else
        Call SetStatusText(sz)
    End If
    
End Sub


'----------
' Privates
'----------

Private Sub CheckReqs()
    
    Call Assert(StrComp(decalapp.szVerDecal, "2.3.0.0", vbTextCompare) >= 0, _
      "SkunkWorks requires Decal version 2.3 or later.")
    
    Call Assert(StrComp(SzExeVerFromFile("VBScript.dll"), "5.5.0.0", vbTextCompare) >= 0, _
      "SkunkWorks requires VBScript version 5.5 or later.")
    
'    On Error Resume Next
'    Dim rex As RegExp, errno As Long
'    Set rex = New RegExp
'    errno = Err.Number
'    On Error GoTo 0
'
'    Call Assert(errno = 0, "Cannot create regular expressions.  Make sure VBScript.dll is properly registered.")
    
End Sub

Private Function FDelegateCmdLine(ByVal szCmdLine As String) As Boolean
    
'   Try to find the existing console window.
    Dim hwndOther As Long
    hwndOther = HwndFind(SzClassFromHwnd(hwnd), "SkunkWorks*", hwnd)
    If hwndOther = hwndNil Then
        FDelegateCmdLine = False
        Exit Function
    End If
    
'   Locate that window's textCmdLine control.
    Dim idControl As Long, hwndControl As Long
    idControl = GetWindowLong(textCmdLine.hwnd, GWL_ID)
    hwndControl = HwndChildFromId(hwndOther, idControl)
    If hwndControl = hwndNil Then
        FDelegateCmdLine = False
        Exit Function
    End If
    
    Call ShowWindow(hwndOther, SW_RESTORE)
    Call SetForegroundWindow(hwndOther)
    
'   Stuff the string into the control.
    Dim lResult As Long
    Call SendMessageTimeoutSz(hwndControl, WM_SETTEXT, 0, szCmdLine, _
      SMTO_NORMAL, 100, lResult)
    
    FDelegateCmdLine = True
    
End Function

Private Sub ExecuteCmdLine(cmdline As CmdlineType)
    
    If Not cmdline.fSilent Then
    '   Check for compatibility with the installed version of Messages.xml.
        If swoptions.fCheckMsgVer And fVBInstalled Then
            Call CheckCrackers(fQuit, False)
            If fQuit Or Not fLoaded Then
                If fLoaded Then Call Unload(Me)
                Exit Sub
            End If
        End If
    End If
    
    Set swxfile = cmdline.swxfile
    If swxfile Is Nothing Then
        If cmdline.fQuit Then
            Call Unload(Me)
        End If
    Else
        If swxfile.szFile <> "" Then
        '   SkunkWorks project file on command line; run it.
            mrulistScript.szFile = swxfile.szFile
        '   Override args after script filename has been set.
            textArgs.Text = cmdline.szArgs
            If cmdline.fRun Then
                Call cmdGo_Click
                If cmdline.fQuit Or Not fLoaded Then
                    If fLoaded Then Call Unload(Me)
                    Exit Sub
                End If
            End If
        Else
        '   ACScript-style command line; import it to a new project.
            swxfile.fRegisterStdHandlers = True
            swxfile.fRenameDPT = True
            If formSwxfile.FEditSwxfile(swxfile, Me) Then
                mrulistScript.szFile = swxfile.szFile
            Else
                Set swxfile = Nothing
            End If
        End If
    End If
    
End Sub

' Read and execute command-line options.
Private Sub ParseCmdLine(ByRef cmdlineOut As CmdlineType, _
  ByVal szCmdLine As String)
    Call TraceEnter("ParseCmdLine")
    
    cmdlineOut.fSilent = False
    Set cmdlineOut.swxfile = Nothing
    cmdlineOut.szArgs = ""
    cmdlineOut.fRun = False
    cmdlineOut.fStop = False
    cmdlineOut.fQuit = False
    
    Dim fArgs As Boolean, fActivateAC As Boolean
    fArgs = False
    fActivateAC = True
    
    Do
        Dim szToken As String
        szToken = SzNextToken(szCmdLine)
        If szToken = "" Then Exit Do
        
        If Left(szToken, 1) = "-" Or Left(szToken, 1) = "/" Then
            Select Case UCase(Right(szToken, Len(szToken) - 1))
                
            Case "A"
            '   -A "arguments": Stuff text into Arguments blank on form.
                cmdlineOut.szArgs = SzNextToken(szCmdLine)
                fArgs = True
                
            Case "L"
            '   -L: ACScript language spec.  Ignore next token.
                Call SzNextToken(szCmdLine)
                
            Case "N"
            '   -N: Don't activate AC.
                fActivateAC = False
                
            Case "R"
            '   -R: Run.
                cmdlineOut.fRun = True
                
            Case "S"
            '   -S: Silent.
                cmdlineOut.fSilent = True
                
            Case "STOP"
            '   -Stop: Stop the running script.
                cmdlineOut.fStop = True
                
            Case "X"
            '   -X: Quit when done.
                cmdlineOut.fQuit = True
                
            Case Else
                Call skapi.OutputLine("Unknown command line option " + szToken, _
                  opmConsole)
                
            End Select
        Else
        '   Script file name.
            Dim szFile As String
            szFile = fso.GetAbsolutePathName(szToken)
            
            If cmdlineOut.swxfile Is Nothing Then
                Set cmdlineOut.swxfile = New SwxfileCls
            End If
            
            If FEqSzFile(SzExtension(szFile), "swx") Then
                Call cmdlineOut.swxfile.Load(szFile)
            Else
                Call cmdlineOut.swxfile.InsertScript(iNil, szFile)
            End If
        End If
        
    Loop
    
    If Not (cmdlineOut.swxfile Is Nothing) Then
        cmdlineOut.swxfile.fActivateAC = cmdlineOut.swxfile.fActivateAC And fActivateAC
        If Not fArgs Then
            cmdlineOut.szArgs = cmdlineOut.swxfile.szArgs
        End If
    End If
    
    Call TraceExit("ParseCmdLine")
End Sub

' Read a token from the command line.
Private Function SzNextToken(ByRef szCmdLine As String) As String
    Call TraceEnter("SzNextToken")
    
    Dim szToken As String
    
    szCmdLine = SzTrimWhiteSpace(szCmdLine)
    If Left(szCmdLine, 1) = """" Then
        Dim ichLim As Long
        ichLim = IchInStr(1, szCmdLine, """")
        If ichLim <> iNil Then
            szToken = SzMid(szCmdLine, 1, ichLim - 1)
            ichLim = ichLim + 1
            szCmdLine = SzTrimWhiteSpace(Right(szCmdLine, Len(szCmdLine) - ichLim))
        Else
            szToken = Right(szCmdLine, Len(szCmdLine) - 1)
            szCmdLine = ""
        End If
    Else
        Call SplitSz(szToken, szCmdLine, szCmdLine, " ")
    End If
    
    SzNextToken = szToken
    Call TraceExit("SzNextToken")
End Function


Private Sub EnableControls()
    
    Call CheckPxe
    menuPxeToChat.Checked = (swoptions.opmPxe And opmChatWnd) <> 0
    
    Call CheckPxm
    menuPxCTS.Checked = swoptions.fPxCTS
    menuPxWholeMsg.Checked = swoptions.fPxWholeMsg
    
    menuTrace.Checked = swoptions.fTrace
    menuMemStats.Checked = swoptions.fMemStats
    menuCapture.Checked = swoptions.fCapture
    menuLog.Checked = swoptions.fLog
    
    menuEnableDebug.Checked = swoptions.fEnableDebug
    menuBreakOnEntry.Enabled = swoptions.fEnableDebug
    menuBreakOnEntry.Checked = swoptions.fBreakOnEntry
    
End Sub

Private Sub CheckPxe()
    
    Dim pxe As PxeType
    For pxe = pxeNone To pxeAll
        menuPxe(pxe).Checked = (swoptions.pxe = pxe)
    Next pxe
    
End Sub

Private Sub CheckPxm()
    
    Dim oidPx As Long
    If skapi Is Nothing Then
        oidPx = oidNil
    Else
        oidPx = skapi.oidPx
    End If
    
    Dim pxm As PxmType
    For pxm = pxmNone To pxmAll
        menuPxm(pxm).Checked = (swoptions.pxm = pxm And oidPx = oidNil)
    Next pxm
    menuOidPx.Checked = (oidPx <> oidNil)
    
End Sub


Private Sub RegisterConsoleHandlers()
    Call TraceEnter("RegisterConsoleHandlers")
    
    Call skapi.AddHandler(evidOnStart3D, Me)
    Call skapi.AddHandler(evidOnLogon, Me)
    Call skapi.AddHandler(evidOnEnd3D, Me)
    Call skapi.AddHandler(evidOnControlEvent, Me)
    
    Call TraceExit("RegisterConsoleHandlers")
End Sub



' Load and run the script specified by swxfile.  This may be either a single
' .js or .vbs file, or a text file containing a list of .js or .vbs filenames.
Private Sub RunScript()
    Call TraceEnter("RunScript")
    On Error GoTo LError
    
'   Clear the console window.
    Call ClearConsole
    
    Call SetStatus(statusLoading)
    
'   CD to the script's home directory so its file system calls work properly.
    Call SetCurrentDirectory(SzDirFromPath(mrulistScript.szFile))
    
    Dim fRenameDPT As Boolean
    fRenameDPT = swxfile.fRenameDPT
    
    Set skapi.objScript = Nothing
    fError = False
    
    Dim rgszLang() As String
    
'   Add all the script code.
    Dim szLang As String, szSrc As String, szInline As String
    Dim swxsite As SwxsiteCls
    Call swxfile.StartEnum
    Do While swxfile.FNextScript(szLang, szSrc, szInline)
        
        If szLang = "" Then
            Call SkError("Cannot determine scripting language for " + SzLeaf(szSrc))
        End If
        
    '   Do we have a script engine for this language yet?
        Set swxsite = SwxsiteFromSzLang(szLang)
        If swxsite Is Nothing Then
        '   No; initialize one.
            Set swxsite = New SwxsiteCls
            Set swxsite.site = New SWXSCRIPTLib.swxsite
            swxsite.site.fTrace = swoptions.fTrace
            swxsite.site.hwndHost = hwnd
            Call swxsite.site.Begin(szLang, swxfile.szLeafFile, swoptions.fEnableDebug, _
              "SkunkWorks.heventStop" + CStr(skapi.iq))
            
        '   Expose the APIs.
            Call swxsite.site.addObject("skapi", skapi, False)
            Call swxsite.site.addObject("console", New ConsoleCls, False)
            Call swxsite.site.addObject("ac", skapi.ac, False)
            Call swxsite.site.addObject("Inventory", skapi.Inventory, False)
            Call swxsite.site.addObject("acswh", skapi.acswh, True)
            
            ReDim Preserve rgszLang(coswxsite.Count)
            rgszLang(coswxsite.Count) = szLang
            Call coswxsite.Add(swxsite, szLang)
        End If
        
        If szSrc <> "" Then
            Dim szFileLeaf As String, szContents As String
            szFileLeaf = SzLeaf(szSrc)
            If fRenameDPT Then
                szContents = SzTransformFile(szSrc, szLang)
            Else
                szContents = SzReadFile(szSrc)
            End If
            Call SetStatusText("Loading " + szFileLeaf + "...")
            Call swxsite.site.AddCode(szContents, szFileLeaf, 1)
        End If
        
        If szInline <> "" Then
            If fRenameDPT Then
                szInline = SzCodeRenameDPT(szInline, szLang)
            End If
            Call swxsite.site.AddCode(szInline, swxfile.szLeafFile, 1)  ' REVIEW: iline
        End If
    Loop
    
'   Now that all the code has been loaded, add each engine to the others as a global
'   object, so each can see the others' global functions and variables.
    Dim iswxsite As Long, iswxsite2 As Long, swxsite2 As SwxsiteCls
    iswxsite = 0
    For Each swxsite In coswxsite
        iswxsite2 = 0
        For Each swxsite2 In coswxsite
            If Not (swxsite Is swxsite2) Then
                Call swxsite.site.addObject("__" + rgszLang(iswxsite2), _
                  swxsite2.site.objScript, True)
            End If
            iswxsite2 = iswxsite2 + 1
        Next swxsite2
        iswxsite = iswxsite + 1
    Next swxsite
    
    skapi.szArgs = textArgs.Text
    
'   Activate AC as specified by swxfile.
    If swxfile.fActivateAC Then
        Call skapi.ActivateAC
    End If
    
    timerMaintain.Enabled = False
    
    If swxfile.fRegisterStdHandlers Then
        Call skapi.OutputSz("Registering handlers...", opmStatus)
        Call skapi.ac.RegisterStandardHandlers
    End If
    
    Call SetStatus(statusRunning)
    fScriptRunning = True
    
'   Run the global code.
    Call TraceLine("site.Run")
    For Each swxsite In coswxsite
        Call swxsite.site.Run(swoptions.fBreakOnEntry)
        If fError Then GoTo LError
    Next swxsite
    
'   Find the main function.
    For Each swxsite In coswxsite
        If swxsite.site.FFunctionDefined("main") Then
            Set skapi.objScript = swxsite.site.objScript
            Exit For
        End If
    Next swxsite
    
'   Call the main function.
    If Not (skapi.objScript Is Nothing) Then
        Call TraceLine("skapi.objScript.main")
        Call skapi.objScript.main
    Else
        Call skapi.OutputLine(">> " + swxfile.szLeafFile + ": " + _
          "No main function.", _
          opmDebugLog + opmConsole + opmChatWnd)
        fError = True
    End If
    
LError:
    
    Dim errno As Long, szSource As String, szDesc As String
    errno = Err.Number
    szSource = Err.Source
    szDesc = Err.Description
    On Error Resume Next
    Call TraceLine("errno: " + CStr(errno) + ", " + _
      "szSource: " + szSource + ", " + _
      "szDesc: " + SzQuote(szDesc))
    
    Set skapi.objScript = Nothing
    Dim fInterrupted As Boolean
    fInterrupted = False
    For Each swxsite In coswxsite
        fInterrupted = fInterrupted Or swxsite.site.fInterrupted
    Next swxsite
    For Each swxsite In coswxsite
        Call swxsite.site.End
    Next swxsite
    Set coswxsite = New Collection
    Call CoFreeUnusedLibraries  ' Free up any COM DLLs loaded by the script.
    fScriptRunning = False
    
'   Remove any dangling control panels put up by the script (but leave miniconsole).
    Call skapi.RemoveControls("!" + szPanelName)
    Call skapi.Hotkey("", False)
    
'   Remove script's event handlers.
    Call skapi.RemoveHandler(evidNil, Nothing)
    
'   Stop any runaway navigation.
    Call skapi.CancelNav
    
    If Not fLoaded Then GoTo LExit
    
'   Put back our event handlers.
    Call RegisterConsoleHandlers
    timerMaintain.Enabled = True
    
'   If statusMbr = statusStopping Then
    If fInterrupted Then
        If szScriptNext = "" Then Call skapi.OutputLine("Script was interrupted.")
    ElseIf fError Then
        Call skapi.OutputLine("Script exited with errors.")
        Call PlaySystemSound(IDI_EXCLAMATION)
    ElseIf errno <> 0 Then
        Call skapi.OutputLine(">> " + szSource + ": " + szDesc)
        Call PlaySystemSound(IDI_ERROR)
    Else
        Call skapi.OutputLine("Script exited normally.")
        Call PlaySystemSound(IDI_INFORMATION)
    End If
    
    If Not fLoaded Then GoTo LExit
    
    Call SetStatus(statusReady)
    
LExit:
    Call TraceExit("RunScript")
End Sub

Private Function SwxsiteFromSzLang(ByVal szLang As String) As SwxsiteCls
    
    Set SwxsiteFromSzLang = Nothing
    
    On Error Resume Next
    Set SwxsiteFromSzLang = coswxsite(szLang)
    
End Function

Private Function SzTransformFile(ByVal szFile As String, _
  ByVal szLang As String) As String
    Call TraceEnter("SzTransformFile", SzQuote(szFile) + ", " + SzQuote(szLang))
    On Error GoTo LError
    
'   Calculate a temp filename.
    Dim szFileLeaf As String, szFileNew As String
    szFileLeaf = SzLeaf(szFile)
    szFileNew = SzFileInTempDir(szFileLeaf)
    
'   If a file by that name already exists, with suitable revision date,
'   reuse it.
    Dim ft As FILETIME, ftNew As FILETIME
    Dim hfind As Long, wfd As WIN32_FIND_DATA
    hfind = FindFirstFile(szFile, wfd)
    If hfind <> hInvalid Then
        ft = wfd.ftLastWriteTime
        Call FindClose(hfind)
    End If
    
    hfind = FindFirstFile(szFileNew, wfd)
    If hfind <> hInvalid Then
        ftNew = wfd.ftLastWriteTime
        Call FindClose(hfind)
        
        If CompareFileTime(ftNew, ft) > 0 Then
            Call TraceLine(CStr(fso.GetFile(szFileNew).DateLastModified) + _
              " > " + CStr(fso.GetFile(szFile).DateLastModified))
            Call TraceLine("Reusing " + szFileNew)
            SzTransformFile = SzReadFile(szFileNew)
            GoTo LExit
        End If
    End If
    
'   No such luck; create a new temp file.
    Call SetStatusText("Translating " + szFileLeaf + "...")
    
    Dim szContents As String
    szContents = SzReadFile(szFile)
    szContents = SzCodeRenameDPT(szContents, szLang)
    
    Dim ts As TextStream
    Set ts = fso.CreateTextFile(szFileNew)
    Call ts.Write(szContents)
    Call ts.Close
    
    SzTransformFile = szContents
    
    GoTo LExit
    
LError:
    Call Err.Raise(Err.Number, SzLeaf(szFile), Err.Description)
    
LExit:
    Call TraceExit("SzTransformFile")
End Function

Private Function SzCodeRenameDPT(ByVal szCode As String, _
  ByVal szLang As String) As String
    Call TraceEnter("SzCodeRenameDPT", SzQuote(szLang))
    
'   KLUGE: Some of the ACScript method and property names --
'   specifically ac.Print, ac.Debug, and obj.Type -- can't be implemented
'   directly in VB because those names are reserved words in VB.  To work
'   around this, I've defined those methods and properties with a trailing
'   underscore in their names (i.e. Print_, Debug_, and Type_).  That means
'   we'd better now search the script source for references and do the
'   corresponding edit there.  Note that this code will replace references
'   everywhere, even inside string constants, but that's life for now.
    
'   REVIEW: This RegExp code, although simple, concise, and fast, doesn't work
'   reliably on all systems.  Probably something to do with improper registration
'   of VBScript.dll, but since that seems to be fairly common I guess I'll just
'   have to do it the hard way.
'    Dim rex As RegExp : Set rex = New RegExp
'    rex.Pattern = "((\bac\.Print\b)|(\bac\.Debug\b)|(\.Type\b))"
'    rex.Global = True
'    rex.IgnoreCase = True
'    szCode = rex.Replace(szCode, "$1_")
    
    Dim fIgnoreCase As Boolean
    fIgnoreCase = True
    
    Dim ich As Long
    ich = 0
    Do
        Dim ichDot As Long, ichUnder As Long
        ichDot = IchInStr(ich, szCode, ".")
        If ichDot = iNil Then Exit Do
        
        If FMatchSubstring(szCode, ichDot + 1, "Debug", fIgnoreCase) Or _
          FMatchSubstring(szCode, ichDot + 1, "Print", fIgnoreCase) And _
          ichDot >= 2 Then
            ichUnder = ichDot + 6
            If FMatchSubstring(szCode, ichDot - 2, "ac", fIgnoreCase) And _
              FSepAtIch(szCode, ichDot - 3) And FSepAtIch(szCode, ichUnder) Then
            '   We have a reference to ac.Debug or ac.Print; append the underscore.
                szCode = Left(szCode, ichUnder) + "_" + Right(szCode, Len(szCode) - ichUnder)
            End If
        ElseIf FMatchSubstring(szCode, ichDot + 1, "Type", fIgnoreCase) Then
            ichUnder = ichDot + 5
            If FSepAtIch(szCode, ichUnder) Then
            '   We have a reference to something.Type; append the underscore.
                szCode = Left(szCode, ichUnder) + "_" + Right(szCode, Len(szCode) - ichUnder)
            End If
        End If
        
        ich = ichDot + 1
        DoEvents
    Loop
    
    SzCodeRenameDPT = szCode
    Call TraceExit("SzCodeRenameDPT")
End Function

Private Function FMatchSubstring(ByVal sz As String, ByVal ich As Long, ByVal szMatch As String, _
  ByVal fIgnoreCase As Boolean) As Boolean
    
    Dim szSub As String
    szSub = SzMid(sz, ich, Len(szMatch))
    If fIgnoreCase Then
        FMatchSubstring = FEqSzI(szSub, szMatch)
    Else
        FMatchSubstring = FEqSz(szSub, szMatch)
    End If
    
End Function

Private Function FSepAtIch(ByVal szCode As String, ByVal ich As Long) As Boolean
    
    If ich >= 0 Then
        FSepAtIch = SzMid(szCode, ich, 1) Like "[!A-Za-z0-9_]"
    Else
        FSepAtIch = True
    End If
    
End Function


' Ask the user should we really stop the script, and then try to stop it.
Private Function FConfirmStopScript() As Boolean
    Call TraceEnter("FConfirmStopScript")
    
    If FConfirmOKCancel("Stop the script?") Then
        Call StopScript
        FConfirmStopScript = True
    Else
        FConfirmStopScript = False
    End If
    
    Call TraceExit("FConfirmStopScript")
End Function


Private Sub LoadControls()
    Call TraceEnter("LoadControls")
    
    Set xmldocControls = New DOMDocument40
    xmldocControls.async = False
    Call xmldocControls.Load(swoptions.SzFileInSourceDir("Miniconsole.xml"))
    Call xmldocControls.setProperty("SelectionLanguage", "XPath")
    szPanelName = SzFromVNull(xmldocControls.documentElement.getAttribute("title"))
    If szPanelName = "" Then
        szPanelName = "SkunkWorks"
        Call xmldocControls.documentElement.setAttribute("title", szPanelName)
    End If
    
    If swoptions.fDev Then Call AddDebugControls
    
    fNeedControls = True
    
    Call TraceExit("LoadControls")
End Sub

Private Sub AddDebugControls()
    Call TraceEnter("AddDebugControls")
    
    Dim elemStatus As IXMLDOMElement
    Set elemStatus = xmldocControls.selectSingleNode("//control[@name='textStatus']")
    
    Dim elemTrace As IXMLDOMElement
    Set elemTrace = xmldocControls.createElement("control")
    Call elemTrace.setAttribute("name", "chkTrace")
    Call elemTrace.setAttribute("progid", "DecalControls.CheckBox")
    Call elemTrace.setAttribute("left", 215)
    Call elemTrace.setAttribute("top", "46")
    Call elemTrace.setAttribute("width", "50")
    Call elemTrace.setAttribute("height", "18")
    Call elemTrace.setAttribute("text", "Trace")
    
    Call elemStatus.setAttribute("width", "147")
    
    Call elemStatus.parentNode.appendChild(elemTrace)
    Call elemStatus.parentNode.appendChild(xmldocControls.createTextNode(vbCrLf + " "))
    
    Call TraceExit("AddDebugControls")
End Sub

Private Sub PopulateScriptList()
    Call TraceEnter("PopulateScriptList")
    
'   Find the script listbox.
    Dim elemLbox As IXMLDOMElement
    Set elemLbox = xmldocControls.selectSingleNode("//control[@name='lboxScript']")
    
'   Delete any existing children.
    Dim sel As IXMLDOMSelection
    Set sel = elemLbox.selectNodes("child::node()")
    Call sel.RemoveAll
    
    Dim dictSzDir As Dictionary: Set dictSzDir = New Dictionary
    Dim dictSzFile As Dictionary: Set dictSzFile = New Dictionary
    dictSzDir.CompareMode = TextCompare
    dictSzFile.CompareMode = TextCompare
    
'   Collect filenames from mrulist.
    Dim szFile As String, szDir As String
    Dim isz As Long, cszMru As Long
    cszMru = mrulistScript.cszFile
    For isz = 0 To cszMru - 1
        szFile = mrulistScript.rgszFile(isz)
        If szFile = "" Then Exit For
        If FFileExists(szFile) Then
            szDir = SzDirFromPath(szFile)
            dictSzDir.Item(szDir) = True
            dictSzFile.Item(szFile) = isz
        End If
    Next isz
    
'   Include Libraries in list of dirs to be scanned.
    dictSzDir.Item(swoptions.szLibDir) = True
    
'   Scan directories from mrulist for additional script files.
    Dim rgszDir
    rgszDir = dictSzDir.Keys
    For isz = 0 To UBound(rgszDir)
        szDir = rgszDir(isz)
        Dim szFileLeaf As String
        szFileLeaf = Dir(fso.BuildPath(szDir, "*.swx"))
        Do While szFileLeaf <> ""
            szFile = fso.BuildPath(szDir, szFileLeaf)
            If Not dictSzFile.Exists(szFile) Then
                dictSzFile.Item(szFile) = cszMru
            End If
            szFileLeaf = Dir()
        Loop
    Next isz
    
'   Add collected files to listbox in mru order.
    Dim iitem As Long, iitemSel As Long
    iitem = 0
    iitemSel = iNil
    Do While dictSzFile.Count > 0
        szFile = SzChooseFile(dictSzFile)
        
        Dim elemOption As IXMLDOMElement
        Set elemOption = xmldocControls.createElement("option")
        Call elemOption.setAttribute("text", SzLeaf(szFile))
        Call elemOption.setAttribute("data", szFile)
        
        Call elemLbox.appendChild(xmldocControls.createTextNode(vbCrLf + "   "))
        Call elemLbox.appendChild(elemOption)
        
        If iitemSel = iNil Then
            If FEqSzFile(szFile, mrulistScript.szFile) Then
                iitemSel = iitem
            End If
        End If
        iitem = iitem + 1
    Loop
    If elemLbox.hasChildNodes Then
        Call elemLbox.appendChild(xmldocControls.createTextNode(vbCrLf + "  "))
    End If
    
    If iitemSel <> iNil Then
    '   Select the appropriate item in the list.
        Call elemLbox.setAttribute("selected", CStr(iitemSel))
    End If
    
    Call TraceExit("PopulateScriptList")
End Sub

Private Function SzChooseFile(dictSzFile As Dictionary) As String
    
    Dim mruBest As Long, szBest As String
    mruBest = 1000000
    szBest = ""
    
    Dim rgsz, isz As Long
    rgsz = dictSzFile.Keys
    For isz = 0 To UBound(rgsz)
        Dim szFile As String, mru As Long
        szFile = rgsz(isz)
        mru = dictSzFile.Item(szFile)
        If mru < mruBest Or _
          (mru = mruBest And StrComp(szFile, szBest, vbTextCompare) < 0) Then
            mruBest = mru
            szBest = szFile
        End If
    Next isz
    
    Call dictSzFile.Remove(szBest)
    
    SzChooseFile = szBest
End Function

Private Sub SetControlState()
    Call TraceEnter("SetControlState")
    
    Dim elemBrowse As IXMLDOMElement
    Set elemBrowse = xmldocControls.selectSingleNode("//control[@name='btnBrowse']")
    Call elemBrowse.setAttribute("textcolor", IIf(fScriptRunning, &H808080, &H7BF3F7))
    
    Dim elemGoStop As IXMLDOMElement
    Set elemGoStop = xmldocControls.selectSingleNode("//control[@name='btnGoStop']")
    Call elemGoStop.setAttribute("text", IIf(fScriptRunning, "Stop", "Go"))
    
    Dim elemStatus As IXMLDOMElement
    Set elemStatus = xmldocControls.selectSingleNode("//control[@name='textStatus']")
    Call elemStatus.setAttribute("text", IIf(fScriptRunning, "Running...", "Ready"))
    
    Dim elemTrace As IXMLDOMElement
    Set elemTrace = xmldocControls.selectSingleNode("//control[@name='chkTrace']")
    If Not (elemTrace Is Nothing) Then
        Call elemTrace.setAttribute("checked", IIf(swoptions.fTrace, "true", "false"))
    End If
    
    Call TraceExit("SetControlState")
End Sub

Private Sub UpdateScriptList(ByVal szFile As String)
    Call TraceEnter("UpdateScriptList")
    
    Call PopulateScriptList
    Call SetControlState
    Call skapi.ShowControls(xmldocControls.xml, Not fScriptRunning)
    
    Call TraceExit("UpdateScriptList")
End Sub


Private Sub SetStatus(ByVal status As StatusType)
    Call TraceEnter("SetStatus", CStr(status))
    
    If statusMbr = status Then GoTo LExit
    
    Select Case status
        
    Case statusReady
    '   Restore original caption.
        Caption = szCaptionOrig
        
    '   Hide the Stop button; show the Go button.
        cmdStop.Enabled = False
        Call skapi.SetControlProperty(szPanelName, "btnBrowse", "TextColor", &H7BF3F7)
        Call skapi.SetControlProperty(szPanelName, "btnGoStop", "Text", "Go")
        Call skapi.SetControlProperty(szPanelName, "btnGoStop", "TextColor", &H7BF3F7)
        Call SetStatusText("Ready")
        mrulistScript.Enabled = True
        menuFileClear.Enabled = True
        cmdGo.Enabled = Not (swxfile Is Nothing)
        cmdOptions.Enabled = Not (swxfile Is Nothing)
        cmdStop.Visible = False
        cmdGo.Visible = True
    '   Don't steal focus back from game.
        If GetForegroundWindow() = hwnd Then
            If mrulistScript.szFile = "" Then
                Call mrulistScript.SetFocus
            Else
                Call cmdGo.SetFocus
            End If
        End If
        Call Refresh
        Screen.MousePointer = vbDefault
       
    Case statusLoading
        Screen.MousePointer = vbHourglass
    '   Disable the Go button.
        mrulistScript.Enabled = False
        menuFileClear.Enabled = False
        cmdGo.Enabled = False
        cmdOptions.Enabled = False
        Call skapi.SetControlProperty(szPanelName, "btnBrowse", "TextColor", &H808080)
        Call skapi.SetControlProperty(szPanelName, "btnGoStop", "TextColor", &H808080)
        Call SetStatusText("Loading...")
        Call Refresh
        
    Case statusRunning
    '   Append script name to window caption.
        Caption = szCaptionOrig + ": " + SzLeaf(mrulistScript.szFile)
        
    '   Hide the Go button; show the Stop button.
        mrulistScript.Enabled = False
        menuFileClear.Enabled = False
        cmdGo.Enabled = False
        cmdOptions.Enabled = False
        Call skapi.SetControlProperty(szPanelName, "btnGoStop", "Text", "Stop")
        Call skapi.SetControlProperty(szPanelName, "btnGoStop", "TextColor", &H7BF3F7)
        Call SetStatusText("Running...")
        cmdStop.Enabled = True
        cmdGo.Visible = False
        cmdStop.Visible = True
    '   Don't steal focus back from game.
        If GetForegroundWindow() = hwnd Then
            Call cmdStop.SetFocus
        End If
        Call Refresh
        Screen.MousePointer = vbDefault
        
    Case statusStopping
        Screen.MousePointer = vbHourglass
    '   Disable the Stop button.
        cmdStop.Enabled = False
        Call skapi.SetControlProperty(szPanelName, "btnGoStop", "TextColor", &H808080)
        Call SetStatusText("Stopping...")
        Call Refresh
        
    End Select
    
    statusMbr = status
    
LExit:
    Call TraceExit("SetStatus")
End Sub

Private Sub SetStatusText(ByVal sz As String)
    Call TraceEnter("SetStatusText", SzQuote(sz))
    
    statbar.SimpleText = " " + sz + " "
    Call skapi.SetControlProperty(szPanelName, "textStatus", "Text", sz)
    
    Call TraceExit("SetStatusText")
End Sub


Private Sub Banner(ByVal opm As Long)
    Call TraceEnter("Banner", Hex(opm))
    
    Call skapi.OutputLine("SkunkWorks version " + skapi.szVersion, opm)
    
    Call TraceExit("Banner")
End Sub


Private Sub PlaySystemSound(ByVal idIcon As Long)
    
'   REVIEW: This turns out to be really slow to load.
'   Call formMsgBox.PlaySystemSound(idIcon)
    
    Call Beep
    
End Sub


Private Sub ClearConsole()
    
    textOutput.Text = ""
    
End Sub

Private Sub OutputToConsole(ByVal sz As String)
    
    Dim szNew As String
    szNew = textOutput.Text
    
'   Parse the string into lines.  Make sure each line ends with CRLF, not just LF.
'   (VB's TextBox control is fussy that way.)
    Dim ich As Long, ichLim As Long, ichNext As Long
    ich = 0
    Do While ich < Len(sz)
        ichLim = IchInStr(ich, sz, vbLf)
        If ichLim <> iNil Then
            ichNext = ichLim + 1
            If ichLim > ich Then
                If SzMid(sz, ichLim - 1, 1) = vbCr Then
                    ichLim = ichLim - 1
                End If
            End If
        Else
            ichLim = Len(sz)
            ichNext = ichLim
        End If
        
        szNew = szNew + SzMid(sz, ich, ichLim - ich)
        If ichLim < ichNext Then
            szNew = szNew + vbCrLf
        End If
        
        ich = ichNext
    Loop
    
    Dim cchMaxString As Long
    cchMaxString = 30000
LRetry:
    If Len(szNew) > cchMaxString Then
        ich = IchInStr(Len(szNew) - cchMaxString - 1, szNew, vbLf)
        If ich <> iNil Then
            ich = ich + 1
        Else
            ich = Len(szNew) - cchMaxString
        End If
        szNew = Right(szNew, Len(szNew) - ich)
    End If
    
    On Error Resume Next
    textOutput.Text = szNew
    If Err.Number <> 0 Then
        If Err.Number = 7 And cchMaxString > 5000 Then
            cchMaxString = cchMaxString - 5000
            GoTo LRetry
        Else
            Call Err.Raise(Err.Number, Err.Source, Err.Description)
        End If
    End If
    textOutput.SelStart = Len(szNew)
    textOutput.SelLength = 0
    
    DoEvents
    
End Sub

























