VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SWControlsCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


'--------------
' Private data
'--------------

Private dictSzPanel As Dictionary
Private dictVkCskey As Dictionary

Private WithEvents swsrv As SWSrvCls
Attribute swsrv.VB_VarHelpID = -1

Private Const dxpRadar = 122            ' Width of radar display.
Private Const dypRadar = 122            ' Height of radar display.
Private Const dypDecalBar = 23          ' Height of Decal bar.


'---------
' Methods
'---------

Public Sub CloseAll()
    Call TraceEnter("SWControls.CloseAll")
    
'   Close all outstanding panels.
    Do While dictSzPanel.Count > 0
        Call RemovePanel(dictSzPanel.Keys(0))
    Loop
    
    Call TraceExit("SWControls.CloseAll")
End Sub


Public Sub SavePanelPos(vw As View)
    Call TraceEnter("SWControls.SavePanelPos", SzQuote(vw.Title))
    
    Dim xp3D As Long, yp3D As Long
    Dim xpLim3D As Long, ypLim3D As Long
    Call Get3DRect(xp3D, yp3D, xpLim3D, ypLim3D)
    
    Dim xpPanel As Long, ypPanel As Long
    Dim xpLimPanel As Long, ypLimPanel As Long
    xpPanel = vw.Position.Left
    ypPanel = vw.Position.Top
    xpLimPanel = vw.Position.Right
    ypLimPanel = vw.Position.Bottom
    
'   Force panel to be fully visible.
    If xpPanel < 0 Then
        xpLimPanel = xpLimPanel - xpPanel
        xpPanel = 0
    ElseIf xpLimPanel > xpLim3D Then
        xpPanel = xpPanel + xpLim3D - xpLimPanel
        xpLimPanel = xpLim3D
    End If
    If ypPanel < 0 Then
        ypLimPanel = ypLimPanel - ypPanel
        ypPanel = 0
    ElseIf ypLimPanel > ypLim3D Then
        ypPanel = ypPanel + ypLim3D - ypLimPanel
        ypLimPanel = ypLim3D
    End If
    
    If xpLimPanel >= xpLim3D - 50 Or _
      (ypPanel < dypRadar And xpLimPanel >= xpLim3D - dypRadar - 50) Then
    '   Panel is near right edge; save right-relative coords.
        xpPanel = xpPanel - xpLim3D
        xpLimPanel = xpLimPanel - xpLim3D
    End If
    If ypLimPanel >= ypLim3D - 50 Then
    '   Panel is near bottom edge; save bottom-relative coords.
        ypPanel = ypPanel - ypLim3D
        ypLimPanel = ypLimPanel - ypLim3D
    End If
    
    Dim szPos As String
    szPos = CStr(xpPanel) + ", " + CStr(ypPanel) + ", " + _
      CStr(xpLimPanel - xpPanel) + ", " + CStr(ypLimPanel - ypPanel)
    Call SaveSetting(swoptions.szAppName, "Windows", "Panel_" + vw.Title, szPos)
    Call TraceLine("szPos = " + szPos)
    
    Call TraceExit("SWControls.SavePanelPos")
End Sub

Public Sub RestorePanelPosXml(xmldoc As DOMDocument40, _
  ByVal fSetSize As Boolean)
    Call TraceEnter("SWControls.RestorePanelPosXml")
    
    Dim xp3D As Long, yp3D As Long
    Dim xpLim3D As Long, ypLim3D As Long
    Call Get3DRect(xp3D, yp3D, xpLim3D, ypLim3D)
    
    Dim szTitle As String
    szTitle = SzFromVNull(xmldoc.documentElement.getAttribute("title"))
    
    Dim xpPanel As Long, ypPanel As Long, dxpPanel As Long, dypPanel As Long
    
    Dim szPos As String, szT As String
    szPos = GetSetting(swoptions.szAppName, "Windows", "Panel_" + szTitle, "")
    If szPos <> "" Then
        Call TraceLine("szPos = " + szPos)
        
        Call SplitSz(szT, szPos, szPos, ",")
        xpPanel = CLng(szT)
        
        Call SplitSz(szT, szPos, szPos, ",")
        ypPanel = CLng(szT)
        
        Call SplitSz(szT, szPos, szPos, ",")
        dxpPanel = CLng(szT)
        
        Call SplitSz(szT, szPos, szPos, ",")
        dypPanel = CLng(szT)
        
    '   Convert right-relative coords to left-relative.
        If xpPanel < 0 Then
            xpPanel = xpPanel + xpLim3D
            If Not IsNull(xmldoc.documentElement.getAttribute("width")) Then
                xpPanel = xpPanel + dxpPanel - CLng(xmldoc.documentElement.getAttribute("width"))
            End If
        End If
        
    '   Convert bottom-relative coords to top-relative.
        If ypPanel < 0 Then
            ypPanel = ypPanel + ypLim3D
            If Not IsNull(xmldoc.documentElement.getAttribute("height")) Then
                ypPanel = ypPanel + dypPanel - CLng(xmldoc.documentElement.getAttribute("height"))
            End If
        End If
        
        If fSetSize Then
            Call xmldoc.documentElement.setAttribute("width", CStr(dxpPanel))
            Call xmldoc.documentElement.setAttribute("height", CStr(dypPanel))
        End If
    Else
    '   No saved position; default to just to left of (stretched) radar.
        szT = SzFromVNull(xmldoc.documentElement.getAttribute("width"))
        If szT <> "" Then
            dxpPanel = CLng(szT)
        Else
            dxpPanel = 180       ' Decal's default.
        End If
        xpPanel = xpLim3D - dxpRadar - 5 - dxpPanel
        ypPanel = dypDecalBar + 5
    End If
    
    Call TraceLine("xypPanel (preclip) = " + CStr(xpPanel) + "," + CStr(ypPanel))
    
'   Make sure panel's not off screen.
    If xpPanel < 0 Then
        xpPanel = 0
    ElseIf xpPanel > xpLim3D - 20 Then
        xpPanel = xpLim3D - 20
    End If
    If ypPanel < 0 Then
        ypPanel = 0
    ElseIf ypPanel > ypLim3D - 20 Then
        ypPanel = ypLim3D - 20
    End If
    
    Call TraceLine("xypPanel (clipped) = " + CStr(xpPanel) + "," + CStr(ypPanel))
    Call xmldoc.documentElement.setAttribute("left", CStr(xpPanel))
    Call xmldoc.documentElement.setAttribute("top", CStr(ypPanel))
    
    Call TraceExit("SWControls.RestorePanelPosXml")
End Sub

Public Sub ShowControls(ByVal szViewSchema As String, ByVal fActivate As Boolean, _
  ByVal szDir As String)
    Call TraceEnter("SWControls.ShowControls", _
      SzQuote(szViewSchema) + ", " + CStr(fActivate) + ", " + SzQuote(szDir))
    On Error GoTo LError
    
    Dim szPanel As String, dictSzControl As Dictionary, vw As View
    
    If IchInStr(0, szViewSchema, "<") <> iNil Or _
      FSuffixMatchSzI(szViewSchema, ".xml") Then
    '   Parse the view schema into an XML tree.
        Dim xmldoc As DOMDocument40: Set xmldoc = New DOMDocument40
        xmldoc.async = False
        xmldoc.preserveWhiteSpace = True
        Dim fLoaded As Boolean
        If FSuffixMatchSzI(szViewSchema, ".xml") Then
        '   It's a filename.
            szDir = fso.GetParentFolderName(szViewSchema)
            fLoaded = xmldoc.Load(szViewSchema)
        Else
        '   It's the raw XML.
            fLoaded = xmldoc.loadXML(szViewSchema)
        End If
        If Not fLoaded Then
            Call ReportError("Invalid view schema.", "ShowControls")
            GoTo LExit
        End If
        Call xmldoc.SetProperty("SelectionLanguage", "XPath")
        
    '   Pull out the title string.
        szPanel = SzFromVNull(xmldoc.documentElement.getAttribute("title"))
        
    '   If a panel already exists with that title, replace it.
        Call RemovePanel(szPanel)
        
        Call RestorePanelPosXml(xmldoc, False)
        
        Set dictSzControl = New Dictionary
        dictSzControl.CompareMode = TextCompare
        
    '   Create the view.
        Call TraceLine("site.LoadView")
        Set vw = site.LoadViewObject(xmldoc.documentElement)
        If vw Is Nothing Then
            Call ReportError("Could not create view.", "ShowControls")
            GoTo LExit
        End If
        Set dictSzControl("$vw") = vw
        
    '   Create a view surrogate.
        Dim csview As CsviewCls
        Set csview = New CsviewCls
        Call csview.Init(szPanel, vw)
        Set dictSzControl("$csview") = csview
        
    '   Check for a script block.
        Dim sel As IXMLDOMSelection
        Set sel = xmldoc.documentElement.selectNodes("script")
        If sel.Length > 0 Then
            Call csview.LoadScript(sel, szDir)
        End If
        
    '   Find all the named controls.
        Call TraceLine("xmldoc.selectNodes(control)")
        Set sel = xmldoc.documentElement.selectNodes("//control[@name]")
        Call TraceLine("sel.length = " + CStr(sel.Length))
        Dim elemControl As IXMLDOMElement, szControl As String
        Do
            Set elemControl = sel.nextNode
            If elemControl Is Nothing Then Exit Do
            
        '   Get the name and type of this control.
            Dim szProgid As String
            szControl = elemControl.getAttribute("name")
            szProgid = elemControl.getAttribute("progid")
            Call TraceLine("szControl = " + SzQuote(szControl) + ", " + _
              "szProgid = " + SzQuote(szProgid))
            
        '   Get a handle to the Decal control object.
            Dim dcctl As Object
            Call TraceLine("vw.Control")
            Set dcctl = vw.Control(szControl)
            
        '   Create a control surrogate object to handle events.
            Dim csctl As CsctlCls
            If FEqSzI(szProgid, "DecalControls.PushButton") Then
                Set csctl = New CsbtnCls
                
            ElseIf FEqSzI(szProgid, "DecalControls.CheckBox") Then
                Set csctl = New CschkCls
                
            ElseIf FEqSzI(szProgid, "DecalControls.Choice") Then
                Set csctl = New CschoCls
                
            ElseIf FEqSzI(szProgid, "DecalControls.Edit") Then
                Set csctl = New CsedtCls
                
            ElseIf FEqSzI(szProgid, "DecalControls.List") Then
                Set csctl = New CslstCls
                
            ElseIf FEqSzI(szProgid, "DecalControls.Notebook") Then
                Set csctl = New CsnbkCls
                
            ElseIf FEqSzI(szProgid, "DecalControls.Progress") Then
                Set csctl = New CsprgCls
                
            ElseIf FEqSzI(szProgid, "DecalControls.Slider") Then
                Set csctl = New CssldCls
                
            ElseIf FEqSzI(szProgid, "DecalControls.StaticText") Then
                Set csctl = New CstxtCls
                
            Else
            '   Unimplemented control type; create a generic surrogate.
                Set csctl = New CsctlCls
                
            End If
            
        '   Initialize the surrogate and save a reference to it.
            Call csctl.Init(szPanel, szControl, csview, dcctl, elemControl)
            Call csview.AddControl(szControl, dcctl)
            Set dictSzControl(szControl) = csctl
        Loop
        
    '   Find all hotkeys.
        Call TraceLine("xmldoc.selectNodes(hotkey)")
        Set sel = xmldoc.documentElement.selectNodes("//hotkey[@name]")
        Call TraceLine("sel.length = " + CStr(sel.Length))
        Do
            Set elemControl = sel.nextNode
            If elemControl Is Nothing Then Exit Do
            
        '   Get the name of this hotkey.
            szControl = elemControl.getAttribute("name")
            Call TraceLine("szControl = " + SzQuote(szControl))
            
        '   Create a control surrogate object to handle events.
            Dim cshotkey As CshotkeyCls
            Set cshotkey = New CshotkeyCls
            
        '   Initialize the surrogate and save a reference to it.
            Call cshotkey.Init(szPanel, szControl, csview, elemControl)
            Call csview.AddControl(szControl, cshotkey.dcctl)
            Set dictSzControl("$$" + szControl) = cshotkey
        Loop
        
    '   Find all timers.
        Call TraceLine("xmldoc.selectNodes(timer)")
        Set sel = xmldoc.documentElement.selectNodes("//timer[@name]")
        Call TraceLine("sel.length = " + CStr(sel.Length))
        Do
            Set elemControl = sel.nextNode
            If elemControl Is Nothing Then Exit Do
            
        '   Get the name of this timer.
            szControl = elemControl.getAttribute("name")
            Call TraceLine("szControl = " + SzQuote(szControl))
            
        '   Create a control surrogate object to handle events.
            Dim cstimer As CstimerCls
            Set cstimer = New CstimerCls
            
        '   Initialize the surrogate and save a reference to it.
            Call cstimer.Init(szPanel, szControl, csview, elemControl)
            Call csview.AddControl(szControl, cstimer.dcctl)
            Set dictSzControl("$$" + szControl) = cstimer
        Loop
        
    '   Run the script's init code (if any).
        Call csview.StartScript
        
    '   Keep a reference to the created view.
        Set dictSzPanel(szPanel) = dictSzControl
        
    '   If this is the SkunkWorks miniconsole, set up Command panel machinery.
        If FMiniconsoleVw(vw) Then
            Call swf.SetVwCmd(vw)
        End If
    Else
    '   szViewSchema is not XML; look it up as a panel name.
        szPanel = szViewSchema
        If Not dictSzPanel.Exists(szPanel) Then GoTo LExit
        Set dictSzControl = dictSzPanel(szPanel)
        Set vw = dictSzControl("$vw")
    End If
    
    If fActivate Then
        Call vw.Activate
    Else
        Call vw.Deactivate
    End If
    
    GoTo LExit
    
LError:
    Call ReportError(Err.Description, "ShowControls")
    
LExit:
    Call TraceExit("SWControls.ShowControls")
End Sub

Public Sub RemoveControls(ByVal szPanel As String)
    Call TraceEnter("SWControls.RemoveControls", SzQuote(szPanel))
    
    If szPanel = "*" Then
        Call CloseAll
    ElseIf FPrefixMatchSzI(szPanel, "!") Then
    '   Close all but the specified panel.
        Dim szPanelKeep As String
        szPanelKeep = Right(szPanel, Len(szPanel) - 1)
        Dim rgszPanel: rgszPanel = dictSzPanel.Keys
        Dim isz
        For isz = 0 To UBound(rgszPanel)
            If Not FEqSzI(rgszPanel(isz), szPanelKeep) Then
                Call RemovePanel(rgszPanel(isz))
            End If
        Next isz
    Else
        Call RemovePanel(szPanel)
    End If
    
    Call TraceExit("SWControls.RemoveControls")
End Sub

Public Function CallPanelFunction(ByVal szPanel As String, ByVal szFunction As String, _
  ByRef v1 As Variant, ByRef v2 As Variant, ByRef v3 As Variant, _
  ByRef v4 As Variant, ByRef v5 As Variant) As Variant
    Call TraceEnter("SWControls.CallPanelFunction", _
      SzQuote(szPanel) + ", " + SzQuote(szFunction))
    On Error GoTo LError
    
    If Not dictSzPanel.Exists(szPanel) Then GoTo LExit
    
'   Look up the view name.
    Dim dictSzControl As Dictionary
    Set dictSzControl = dictSzPanel(szPanel)
    
'   Call the named script function.
    Dim csview As CsviewCls
    Set csview = dictSzControl("$csview")
    CallPanelFunction = csview.CallFunction(szFunction, v1, v2, v3, v4, v5)
    
    GoTo LExit
    
LError:
    Call ReportError(Err.Description, "CallPanelFunction")
    
LExit:
    Call TraceExit("SWControls.CallPanelFunction")
End Function

Public Sub SendControlMsg(ByVal szPanel As String, ByVal szControlClicked As String)
    Call TraceEnter("SWControls.SendControlMsg", _
      SzQuote(szPanel) + ", " + SzQuote(szControlClicked))
    On Error GoTo LError
    
    Call TraceLine("Set dictSzControl")
    Dim dictSzControl As Dictionary
    Set dictSzControl = dictSzPanel(szPanel)
    
    Call TraceLine("Set vw")
    Dim vw As View
    Set vw = dictSzControl("$vw")
    
    Call TraceLine("mbuf.StartPut")
    Dim mbuf As MbufCls
    Set mbuf = swsrv.mbufEvent
    Call mbuf.StartPut
    Call mbuf.PutMty(mtySWControl)
    Call mbuf.Put_String(vw.Title)
    Call mbuf.Put_String(szControlClicked)
    
'   Count up the controls.
    Dim ccontrol As Long
    ccontrol = 0
    Dim rgsz: rgsz = dictSzControl.Keys
    Dim isz As Long, szControl As String
    For isz = 0 To UBound(rgsz)
        szControl = rgsz(isz)
        If Not FPrefixMatchSzI(szControl, "$") Then
            ccontrol = ccontrol + 1
        End If
    Next isz
    Call TraceLine("ccontrol = " + CStr(ccontrol))
    Call mbuf.Put_DWORD(ccontrol)
    
    For isz = 0 To UBound(rgsz)
        szControl = rgsz(isz)
        If Not FPrefixMatchSzI(szControl, "$") Then
        '   Append the name and value of this control.
            Dim csctl As CsctlCls, szValue As String
            Set csctl = dictSzControl(szControl)
            szValue = csctl.szValue
            Call TraceLine("szControl = " + SzQuote(szControl) + ", " + _
              "szValue = " + SzQuote(szValue))
            Call mbuf.Put_String(szControl)
            Call mbuf.Put_String(szValue)
        End If
    Next isz
    
    Call TraceLine("QueueEventMbuf")
    Call swsrv.QueueEventMbuf
    
    GoTo LExit
    
LError:
    Call ReportError(Err.Description, "SendControlMsg")
    
LExit:
    Call TraceExit("SWControls.SendControlMsg")
End Sub

Public Sub SendHotkeyMsg(ByVal sz As String, ByVal vk As Long)
    Call TraceEnter("SWControls.SendHotkeyMsg", SzQuote(sz) + ", " + CStr(vk))
    On Error GoTo LError
    
    Dim mbuf As MbufCls
    Set mbuf = swsrv.mbufEvent
    Call mbuf.StartPut
    Call mbuf.PutMty(mtySWHotkey)
    Call mbuf.Put_String(sz)
    Call mbuf.Put_DWORD(vk)
    
    Call swsrv.QueueEventMbuf
    
    GoTo LExit
    
LError:
    Call ReportError(Err.Description, "SendHotkeyMsg")
    
LExit:
    Call TraceExit("SWControls.SendHotkeyMsg")
End Sub


'--------------
' SWSrv events
'--------------

Private Sub swsrv_ShowControls(ByVal szViewSchema As String, ByVal fActivate As Boolean, _
  ByVal szSkapiDir As String)
    
    Call ShowControls(szViewSchema, fActivate, szSkapiDir)
    
End Sub

Private Sub swsrv_GetControlProperty(ByVal szPanel As String, _
  ByVal szControl As String, ByVal szProperty As String)
    Call TraceEnter("SWControls.GetControlProperty", SzQuote(szPanel) + ", " + _
      SzQuote(szControl) + ", " + SzQuote(szProperty))
    On Error GoTo LError
    
    If Not dictSzPanel.Exists(szPanel) Then GoTo LExit
    
'   Look up the view name.
    Dim dictSzControl As Dictionary
    Set dictSzControl = dictSzPanel(szPanel)
    
'   Get the control surrogate.
    Dim csctl As CsctlCls
    If Not dictSzControl.Exists(szControl) Then GoTo LExit
    Set csctl = dictSzControl(szControl)
    
'   Extract argument list, if any.
    Dim rgvArg() As Variant, cvArg As Long
    Call ParseArgs(szProperty, rgvArg, cvArg)
    
'   Call the surrogate to get the property value.
    Dim vValue As Variant
    vValue = csctl.GetProperty(szProperty, rgvArg, cvArg)
    
    If Not IsNull(vValue) And Not IsEmpty(vValue) Then
    '   Ship it back to skapi.
        Dim mbuf As MbufCls
        Set mbuf = swsrv.mbufEvent
        Call mbuf.StartPut
        Call mbuf.PutMty(mtySWPropVal)
        Call mbuf.Put_String(szPanel)
        Call mbuf.Put_String(szControl)
        Call mbuf.Put_String(szProperty)
        Call mbuf.Put_Variant(vValue)
        Call swsrv.QueueEventMbuf
    End If
    
    GoTo LExit
    
LError:
    Call ReportError("Invalid property: " + szControl + "." + szProperty, _
      "GetControlProperty")
    
LExit:
    Call TraceExit("SWControls.GetControlProperty")
End Sub

Private Sub swsrv_SetControlProperty(ByVal szPanel As String, _
  ByVal szControl As String, ByVal szProperty As String, ByVal vValue As Variant)
    Call TraceEnter("SWControls.SetControlProperty", SzQuote(szPanel) + ", " + _
      SzQuote(szControl) + ", " + SzQuote(szProperty) + ", " + CStr(vValue))
    On Error GoTo LError
    
    If Not dictSzPanel.Exists(szPanel) Then GoTo LExit
    
'   Look up the view name.
    Dim dictSzControl As Dictionary
    Set dictSzControl = dictSzPanel(szPanel)
    
'   Get the control surrogate.
    Dim csctl As CsctlCls
    If Not dictSzControl.Exists(szControl) Then GoTo LExit
    Set csctl = dictSzControl(szControl)
    
'   Extract argument list, if any.
    Dim rgvArg() As Variant, cvArg As Long
    Call ParseArgs(szProperty, rgvArg, cvArg)
    
'   Call the surrogate to set the property.
    Call csctl.SetProperty(szProperty, rgvArg, cvArg, vValue)
    
    GoTo LExit
    
LError:
    Call ReportError("Invalid property: " + szControl + "." + szProperty, _
      "SetControlProperty")
    
LExit:
    Call TraceExit("SWControls.SetControlProperty")
End Sub

Private Sub swsrv_CallPanelFunction(ByVal szPanel As String, ByVal szFunction As String, _
  ByRef v1 As Variant, ByRef v2 As Variant, ByRef v3 As Variant, _
  ByRef v4 As Variant, ByRef v5 As Variant)
  
    Call CallPanelFunction(szPanel, szFunction, v1, v2, v3, v4, v5)
    
End Sub

Private Sub swsrv_RemoveControls(ByVal szPanel As String)
    
    Call RemoveControls(szPanel)
    
End Sub

Private Sub swsrv_EnableHotkey(ByVal sz As String, ByVal vk As Long, _
  ByVal fEnable As Boolean)
    Call TraceEnter("SWControls.EnableHotkey", _
      SzQuote(sz) + ", " + CStr(vk) + ", " + CStr(fEnable))
    
    If fEnable Then
        Dim cskey As CskeyCls
        Set cskey = New CskeyCls
        Call cskey.Init(sz, vk)
        Set dictVkCskey(vk) = cskey
    ElseIf vk = &HFF Then
    '   Remove all hotkeys.
        Set dictVkCskey = New Dictionary
        dictVkCskey.CompareMode = BinaryCompare
    ElseIf dictVkCskey.Exists(vk) Then
        Call dictVkCskey.Remove(vk)
    End If
    
    Call TraceExit("SWControls.EnableHotkey")
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceEnter("SWControls.Initialize")
    
    Set dictSzPanel = New Dictionary
    dictSzPanel.CompareMode = TextCompare
    
    Set dictVkCskey = New Dictionary
    dictVkCskey.CompareMode = BinaryCompare
    
    Set swsrv = SWFGlobals.swsrv
    
    Call TraceExit("SWControls.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceEnter("SWControls.Terminate")
    
    Call CloseAll
    
    Set swsrv = Nothing
    
    Call TraceExit("SWControls.Terminate")
End Sub


Private Sub RemovePanel(ByVal szPanel As String)
    Call TraceEnter("SWControls.RemovePanel", SzQuote(szPanel))
    On Error GoTo LError
    
'   If there's no view with the given title, do nothing.
    If Not dictSzPanel.Exists(szPanel) Then GoTo LExit
    
'   Retrieve the view handle.
    Dim dictSzControl As Dictionary, vw As View
    Set dictSzControl = dictSzPanel(szPanel)
    Set vw = dictSzControl("$vw")
    
'   Remember its screen position.
    Call SavePanelPos(vw)
    
'   Shut down any associated script.
    Dim csview As CsviewCls
    Set csview = dictSzControl("$csview")
    Call csview.ShutDown
    
'   If this is the SkunkWorks miniconsole, shut down Command panel machinery.
    If FMiniconsoleVw(vw) Then
        Call swf.SetVwCmd(Nothing)
    End If
    
'   Let go of it so it can be cleaned up.
    Call TraceLine("dictSzPanel.Remove")
    Call dictSzPanel.Remove(szPanel)
    
    GoTo LExit
    
LError:
    Call ReportError(Err.Description, "RemovePanel")
    
LExit:
    Call TraceExit("SWControls.RemovePanel")
End Sub


Private Sub Get3DRect(ByRef xpOut As Long, ByRef ypOut As Long, _
  ByRef xpLimOut As Long, ByRef ypLimOut As Long)
    
    Const dypTopMatter = 28         ' Height of AC's health bars and such.
    Const dypBottomMatter = 100     ' Height of AC's chat window, etc.
    Const dxpRightMatter = 309      ' Width of right-hand UI panel.
    
    Dim dxpRes As Long, dypRes As Long
    Dim szRes As String, szResH As String, szResV As String
    szRes = SzGetIni(szUserPrefIni, "Display", "Resolution", "800x600")
    Call SplitSz(szResH, szResV, szRes, "x")
    dxpRes = CLng(szResH)
    dypRes = CLng(szResV)
    
    xpOut = 0
    ypOut = 0
    xpLimOut = dxpRes - dxpRightMatter
    ypLimOut = dypRes - dypBottomMatter - dypTopMatter
    Call TraceLine("rc3D = " + CStr(xpOut) + "," + CStr(ypOut) + "," + _
      CStr(xpLimOut) + "," + CStr(ypLimOut))
    
End Sub

Private Property Get szUserPrefIni() As String
    szUserPrefIni = fso.BuildPath(fso.BuildPath(SzShellFolder("Personal"), _
      "Asheron's Call"), "UserPreferences.ini")
End Property


Private Sub ParseArgs(ByRef szProperty As String, rgvArg() As Variant, ByRef cvArg As Long)
    
    cvArg = 0
    
    Dim szArgs As String
    Call SplitSz(szProperty, szArgs, szProperty, "(")
    If szArgs <> "" Then
        If FSuffixMatchSzI(szArgs, ")") Then
            szArgs = Left(szArgs, Len(szArgs) - 1)
        End If
        Do While szArgs <> ""
            Dim szArg As String, vArg As Variant
            Call SplitSz(szArg, szArgs, szArgs, ",")
            If FPrefixMatchSzI(szArg, """") Or _
              FPrefixMatchSzI(szArg, "'") Then
                vArg = SzStripQuotes(szArg)
            Else
                vArg = CLng(SzFixHex(SzTrimWhiteSpace(szArg)))
            End If
            Call TraceLine("vArg = " + CStr(vArg))
            ReDim Preserve rgvArg(cvArg)
            rgvArg(cvArg) = vArg
            cvArg = cvArg + 1
        Loop
    End If
    
End Sub


Private Function FMiniconsoleVw(ByVal vw As View) As Boolean
    On Error GoTo LExit
    
    FMiniconsoleVw = False
    
    If FEqSzI(vw.Title, "SkunkWorks") Then
        If Not (vw.Control("lboxCmd") Is Nothing) Then
            FMiniconsoleVw = True
        End If
    End If
    
LExit:
End Function


























