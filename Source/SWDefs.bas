Attribute VB_Name = "SWDefs"
Option Explicit
' SkunkWorks IPC definitions.


Public Const szClsidSWPlugin = "{C0535F3D-9DA2-4617-A7BD-4495F7C379E9}"
Public Const szClsidSWFilter = "{3F5C997C-9B10-429C-870D-022497195068}"


' SkunkWorks message types:
Public Const mtySWCommand = &HE0000000
Public Const mtySWControl = &HE0010000
Public Const mtySWLeaveVendor = &HE0020000
Public Const mtySWPropVal = &HE0030000
Public Const mtySWPluginMsg = &HE0040000
Public Const mtySWHotkey = &HE0050000
Public Const mtySWNavStop = &HE0060000
Public Const mtySWDestroyAcos = &HE0070000
Public Const mtySWDisconnect = &HE0080000
Public Const mtySWChatTextIntercept = &HE0090000
Public Const mtySWStatusTextIntercept = &HE00A0000
Public Const mtySWOpenContainerPanel = &HE00B0000
Public Const mtySWCloseContainer = &HE00C0000
Public Const mtySWChatClickIntercept = &HE00D0000


' SkunkWorks Command codes (in Commands queue):
Public Enum SwcType            ' Arguments:
'   ACHooks commands:
    swcAddChatText = 0
    swcAddChatTextRaw = 1
    swcAddStatusText = 2
    swcApplyItem = 3
    swcCastSpell = 4
    swcDropItem = 5
    swcFaceHeading = 6
    swcGiveItem = 7
    swcIDQueueAdd = 8
    swcInvokeChatParser = 9
    swcLogout = 10
    swcMoveItem = 11
    swcMoveItemEx = 12
    swcMoveItemExRaw = 13
    swcRaiseAttribute = 14
    swcRaiseSkill = 15
    swcRaiseVital = 16
    swcRequestID = 17
    swcSalvagePanelAdd = 18
    swcSalvagePanelSalvage = 19
    swcSelectItem = 20
    swcSetAutorun = 21
    swcSetCombatMode = 22
    swcSetCurrentSelection = 23
    swcSetCursorPosition = 24
    swcSetIdleTime = 25
    swcSetPreviousSelection = 26
    swcSetSelectedStackCount = 27
    swcSpellTabAdd = 28
    swcSpellTabDelete = 29
    swcTradeAccept = 30
    swcTradeAdd = 31
    swcTradeDecline = 32
    swcTradeEnd = 33
    swcTradeReset = 34
    swcUseItem = 35
    swcUseItemRaw = 36
    swcVendorBuyAll = 37
    swcVendorBuyListAdd = 38
    swcVendorBuyListClear = 39
    swcVendorSellAll = 40
    swcVendorSellListAdd = 41
    swcVendorSellListClear = 42
    
'   Other commands:
    swcButtonEvent = 50         ' ibutton as word, kmo as word
    swcCallPanelFunction = 51   ' szPanel, szFunction, vArg
    swcCancelNav = 52           ' none
    swcDisplaySz = 53           ' iwnd, csz, rg(sz,cmc)
    swcEnableHotkey = 54        ' sz, vk (dword), fEnable (dword)
    swcGoToLatLng = 55          ' lat, lng, dist (singles), cmsec, nopt, fRunAsDefault (dwords)
    swcMouseMove = 56           ' xp,yp as dwords
    swcReloadOptions = 57       ' none
    swcRemoveControls = 58      ' szPanel
    swcScriptMsg = 59           ' szMsg
    swcShowControls = 60        ' szViewSchema, fActivate as dword, szSkapiDir
    swcTurnToHead = 61          ' head as single
    swcVkEvent = 62             ' vk as word, kmo as word
    
    swcLimAck = 100
    
    swcGetControlProperty = 101 ' szPanel, szControl, szProp
    swcSetControlProperty = 102 ' szPanel, szControl, szProp, vValue
    
End Enum


Public Const idTimerPoll As Long = 1067

Public Const signatureCapture As Long = &H534D4341      ' ACMS
Public Const versionCapture As Long = 1


' Shared Variables (between SWFilter and Skapi).
Public Type SvarsType
    oidSel As Long
    oidSelPrev As Long
    fTextInput As Boolean
    fPointerBusy As Boolean
    
    landblock As Long
    X As Single
    Y As Single
    z As Single
    head As Single
    
    xpFirst3D As Long
    ypFirst3D As Long
    xpLim3D As Long
    ypLim3D As Long
End Type














