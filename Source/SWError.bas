Attribute VB_Name = "SWError"
Option Explicit
' Error-handling functions.


Public Const errnoStopScript As Long = 16699
Public Const errnoACNotRunning As Long = 16698
Public Const errnoBadMemloc As Long = 16697
Public Const errnoLocError As Long = 16696


Public fTrace As Boolean

Private tsLog As TextStream, szLog As String



Public Sub StartLog(ByVal szFile As String)
    Set tsLog = fso.CreateTextFile(szFile)
    szLog = szFile
End Sub

Public Sub FlushLog()
    If Not (tsLog Is Nothing) Then
        Call tsLog.Close
        Set tsLog = fso.OpenTextFile(szLog, ForAppending)
    End If
End Sub

Public Sub StopLog()
    If Not (tsLog Is Nothing) Then
        Call tsLog.Close
        Set tsLog = Nothing
    End If
End Sub


Public Sub TraceEnter(ByVal szFunc As String, Optional ByVal szArgs As String = "")
    If fTrace Then
        Call DebugLine("[ " + szFunc + "(" + szArgs + ")")
    End If
End Sub

Public Sub TraceExit(ByVal szFunc As String, Optional ByVal szResult As String = "")
    If fTrace Then
        If szResult = "" Then
            Call DebugLine("] " + szFunc)
        Else
            Call DebugLine("] " + szFunc + ": " + szResult)
        End If
    End If
End Sub

Public Sub TraceLine(ByVal szLine As String)
    If fTrace Then
        Call DebugLine("  " + szLine)
    End If
End Sub

Public Sub DebugLine(ByVal szLine As String)
    
    Call DebugSz(szLine + vbCrLf)
    
End Sub

Public Sub DebugSz(ByVal sz As String)
    
    Call OutputDebugString(sz)
    If Not (tsLog Is Nothing) Then Call tsLog.Write(sz)
    
End Sub


Public Sub NotImplemented()
    
    Call SkError("Not implemented.")
    
End Sub

Public Sub Assert(ByVal f As Boolean, ByVal szError As String)
    
    If Not f Then Call SkError(szError)
    
End Sub

Public Sub SkError(ByVal szError As String, Optional ByVal dwError As Long = 0)
    
    Call LogError(App.EXEName, szError, dwError)
    Call Err.Raise(16666, App.EXEName, szError)
    
End Sub

Public Sub LogError(ByVal szSource As String, ByVal szDesc As String, _
  Optional ByVal dwError As Long = 0)
    
    Dim szMsg As String
    szMsg = szSource + ": " + szDesc
    If dwError <> 0 Then szMsg = szMsg + "  (Error " + CStr(dwError) + ")"
    
    Call DebugLine(String(Len(szMsg) + 4, "*"))
    Call DebugLine("* " + szMsg + " *")
    Call DebugLine(String(Len(szMsg) + 4, "*"))
    Call FlushLog
    
End Sub




