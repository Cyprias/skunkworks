Attribute VB_Name = "SWFGlobals"
Option Explicit


Public site As DecalPlugins.IPluginSite
Public site2 As Decal.IPluginSite2
Public Hooks As ACHooks

Public decalapp As DecalCls

Public swf As SWFilterCls
Public swcontrols As SWControlsCls
Public swkeys As SWKeysCls
Public swoptions As SWOptionsCls
Public swsrv As SWSrvCls
Public hwndAC As Long

Public svars As SvarsType

Public Const cmcError As Long = 6


Public Sub ReportError(ByVal szMsg As String, _
  Optional ByVal szFunc As String = "")
    
    If szFunc <> "" Then szMsg = szFunc + ": " + szMsg
    szMsg = ">> " + szMsg
    
    Call DebugLine(szMsg)
    If Not (site Is Nothing) Then
        Call Hooks.AddChatText(szMsg, cmcError, 1)
    End If
    
End Sub

Public Function EnumThreadWndProcSwf(ByVal hwnd As Long, ByVal lparam As Long) As Long
    Call TraceEnter("EnumThreadWndProcSwf", SzHex(hwnd) + ", " + CStr(lparam))
    
    If FEqSzI(SzCaptionFromHwnd(hwnd), "Asheron's Call") Then
        Call TraceLine("Found hwndAC = " + SzHex(hwnd))
        hwndAC = hwnd
        EnumThreadWndProcSwf = False
    Else
        EnumThreadWndProcSwf = True
    End If
    
    Call TraceExit("EnumThreadWndProcSwf")
End Function

Public Sub TimerProcSwf(ByVal hwnd As Long, ByVal wm As Long, ByVal id As Long, ByVal tick As Long)
'    Call TraceEnter("TimerProcSwf", SzHex(hwnd) + ", " + CStr(tick))
    
''   Let plugin handle the polling while it's active.
'    If Not swsrv.fAttached Then
'    '   Poll the SkunkWorks command queue.  Any commands received will raise
'    '   events to be handled elsewhere.
'        Call swsrv.Poll
'    End If
    
    Call swf.Private_Poll
    
'    Call TraceExit("TimerProcSwf")
End Sub




