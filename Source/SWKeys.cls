VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SWKeysCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' Functions for sending keyboard and mouse input to AC.


'--------------
' Private data
'--------------

Private xpMouse As Long, ypMouse As Long, mkButtons As Long
Private ibuttonClick As Long, tickClick As Long
Private fShiftDown As Boolean, fCtrlDown As Boolean, fAltDown As Boolean


Private Declare Sub keybd_event Lib "user32" _
  (ByVal bVk As Byte, ByVal bScan As Byte, ByVal dwFlags As Long, ByVal dwExtraInfo As Long)

Private Declare Sub mouse_event Lib "user32" _
  (ByVal dwFlags As Long, ByVal dx As Long, ByVal dy As Long, ByVal cButtons As Long, ByVal dwExtraInfo As Long)


'---------
' Methods
'---------

Public Sub SendVk(ByVal hwnd As Long, ByVal vk As Long, ByVal kmo As Long)
    
    If (kmo And kmoDown) <> 0 Then
        If (vk And maskVkShift) <> 0 Then Call SendKey(hwnd, VK_SHIFT, False)
        If (vk And maskVkCtrl) <> 0 Then Call SendKey(hwnd, VK_CONTROL, False)
        If (vk And maskVkAlt) <> 0 Then Call SendKey(hwnd, VK_MENU, False)
        Call SendKey(hwnd, vk And &HFF, False)
    End If
    
    If (kmo And kmoUp) <> 0 Then
        Call SendKey(hwnd, vk And &HFF, True)
        If (vk And maskVkShift) <> 0 Then Call SendKey(hwnd, VK_SHIFT, True)
        If (vk And maskVkCtrl) <> 0 Then Call SendKey(hwnd, VK_CONTROL, True)
        If (vk And maskVkAlt) <> 0 Then Call SendKey(hwnd, VK_MENU, True)
    End If
    
End Sub

Public Sub SendIbutton(ByVal hwnd As Long, ByVal ibutton As Long, _
  ByVal kmo As Long)
    
    If hwnd <> hwndNil Then
        If (kmo And kmoDown) <> 0 Then
            Dim fDbl As Boolean
            fDbl = (ibutton = ibuttonClick And GetTickCount() - tickClick < 200)
            Select Case ibutton
            Case 0
                mkButtons = mkButtons Or MK_LBUTTON
                Call SendMouseMessage(hwnd, IIf(fDbl, WM_LBUTTONDBLCLK, WM_LBUTTONDOWN))
            Case 1
                mkButtons = mkButtons Or MK_RBUTTON
                Call SendMouseMessage(hwnd, IIf(fDbl, WM_RBUTTONDBLCLK, WM_RBUTTONDOWN))
            Case 2
                mkButtons = mkButtons Or MK_MBUTTON
                Call SendMouseMessage(hwnd, IIf(fDbl, WM_MBUTTONDBLCLK, WM_MBUTTONDOWN))
            End Select
            If fDbl Then
                ibuttonClick = iNil
            Else
                ibuttonClick = ibutton
                tickClick = GetTickCount()
            End If
        End If
        
        If (kmo And kmoUp) <> 0 Then
            Select Case ibutton
            Case 0
                mkButtons = mkButtons And Not MK_LBUTTON
                Call SendMouseMessage(hwnd, WM_LBUTTONUP)
            Case 1
                mkButtons = mkButtons And Not MK_RBUTTON
                Call SendMouseMessage(hwnd, WM_RBUTTONUP)
            Case 2
                mkButtons = mkButtons And Not MK_MBUTTON
                Call SendMouseMessage(hwnd, WM_MBUTTONUP)
            End Select
        End If
    Else
        If (kmo And kmoDown) <> 0 Then
            Select Case ibutton
            Case 0
                Call SendButton(hwnd, MOUSEEVENTF_LEFTDOWN)
            Case 1
                Call SendButton(hwnd, MOUSEEVENTF_RIGHTDOWN)
            Case 2
                Call SendButton(hwnd, MOUSEEVENTF_MIDDLEDOWN)
            End Select
        End If
        
        If (kmo And kmoUp) <> 0 Then
            Select Case ibutton
            Case 0
                Call SendButton(hwnd, MOUSEEVENTF_LEFTUP)
            Case 1
                Call SendButton(hwnd, MOUSEEVENTF_RIGHTUP)
            Case 2
                Call SendButton(hwnd, MOUSEEVENTF_MIDDLEUP)
            End Select
        End If
    End If
    
End Sub

Public Sub SendMotion(ByVal hwnd As Long, ByVal xp As Long, ByVal yp As Long)
    Call TraceEnter("SendMotion", CStr(xp) + ", " + CStr(yp))
    
'    Call Assert(hwnd <> hwndNil, "Invalid hwnd.")
    
    If hwnd <> hwndNil Then
        xpMouse = xp
        ypMouse = yp
        Call SendMouseMessage(hwnd, WM_MOUSEMOVE)
    Else
        Dim pt As POINTAPI
        pt.X = xp
        pt.Y = yp
        Call ClientToScreen(hwnd, pt)
        
        Dim dxpScreen As Long, dypScreen As Long
        dxpScreen = GetSystemMetrics(SM_CXSCREEN)
        dypScreen = GetSystemMetrics(SM_CYSCREEN)
        
        Dim xna As Long, yna As Long
        xna = pt.X / dxpScreen * 65535
        yna = pt.Y / dypScreen * 65535
        
        Call TraceLine("pt = " + CStr(pt.X) + "," + CStr(pt.Y))
        Call TraceLine("dxypScreen = " + CStr(dxpScreen) + "," + CStr(dypScreen))
        Call TraceLine("xna,yna = " + CStr(xna) + "," + CStr(yna))
        
        Call mouse_event(MOUSEEVENTF_ABSOLUTE Or MOUSEEVENTF_MOVE, xna, yna, 0, 0)
    End If
    
    Call TraceExit("SendMotion")
End Sub


' Return a Decal-compatible string encoding of the given VK.
Public Function SzFromVkDecal(ByVal vk As Long) As String
    Call TraceEnter("SzFromVkDecal", SzHex(vk, 4))
    
    Select Case vk
    Case VK_BACK: SzFromVkDecal = "{BACKSPACE}"
    Case VK_CAPITAL: SzFromVkDecal = "{CAPSLOCK}"
    Case VK_DELETE: SzFromVkDecal = "{DEL}"
    Case VK_DOWN: SzFromVkDecal = "{DOWN}"
    Case VK_END: SzFromVkDecal = "{END}"
    Case VK_RETURN: SzFromVkDecal = "{ENTER}"
    Case VK_ESCAPE: SzFromVkDecal = "{ESC}"
'   Case VK_HELP: SzFromVkDecal = "{HELP}"
    Case VK_HOME: SzFromVkDecal = "{HOME}"
    Case VK_INSERT: SzFromVkDecal = "{INS}"
    Case VK_INSERT: SzFromVkDecal = "{INSERT}"
    Case VK_LEFT: SzFromVkDecal = "{LEFT}"
    Case VK_NUMLOCK: SzFromVkDecal = "{NUMLOCK}"
    Case VK_NEXT: SzFromVkDecal = "{PGDN}"
    Case VK_PRIOR: SzFromVkDecal = "{PGUP}"
    Case VK_SNAPSHOT: SzFromVkDecal = "{PRTSC}"
    Case VK_RIGHT: SzFromVkDecal = "{RIGHT}"
    Case VK_SCROLL: SzFromVkDecal = "{SCROLLLOCK}"
    Case VK_TAB: SzFromVkDecal = "{TAB}"
    Case VK_UP: SzFromVkDecal = "{UP}"
    Case VK_F1: SzFromVkDecal = "{F1}"
    Case VK_F2: SzFromVkDecal = "{F2}"
    Case VK_F3: SzFromVkDecal = "{F3}"
    Case VK_F4: SzFromVkDecal = "{F4}"
    Case VK_F5: SzFromVkDecal = "{F5}"
    Case VK_F6: SzFromVkDecal = "{F6}"
    Case VK_F7: SzFromVkDecal = "{F7}"
    Case VK_F8: SzFromVkDecal = "{F8}"
    Case VK_F9: SzFromVkDecal = "{F9}"
    Case VK_F10: SzFromVkDecal = "{F10}"
    Case VK_F11: SzFromVkDecal = "{F11}"
    Case VK_F12: SzFromVkDecal = "{F12}"
'   Case VK_F13: SzFromVkDecal = "{F13}"
'   Case VK_F14: SzFromVkDecal = "{F14}"
'   Case VK_F15: SzFromVkDecal = "{F15}"
'   Case VK_F16: SzFromVkDecal = "{F16}"
    Case VK_ADD: SzFromVkDecal = "{+}"
    Case VK_SHIFT: SzFromVkDecal = "{SHIFT}"
    Case VK_CONTROL: SzFromVkDecal = "{CTRL}"
    Case VK_MENU: SzFromVkDecal = "{ALT}"
    Case Else: SzFromVkDecal = Chr(vk)
    End Select
    
LExit:
    Call TraceExit("SzFromVkDecal", SzQuote(SzFromVkDecal))
End Function


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceEnter("SWKeys.Initialize")
    
    ibuttonClick = iNil
    
    Call TraceExit("SWKeys.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceEnter("SWKeys.Terminate")
    
    Call TraceExit("SWKeys.Terminate")
End Sub

Private Sub SendKey(ByVal hwnd As Long, ByVal vk As Long, ByVal fKeyUp As Boolean)
    
    If hwnd <> hwndNil Then
        Dim lparam As Long
        lparam = MapVirtualKey(vk, 0) * &H10000 + 1
        If FExtendedVk(vk) Then lparam = lparam Or &H1000000
        If Not fKeyUp Then
            Call SendMessage(hwnd, WM_KEYDOWN, vk, lparam)
        '   Track shift key states for character translation.
            If vk = VK_SHIFT Then fShiftDown = True
            If vk = VK_CONTROL Then fCtrlDown = True
            If vk = VK_MENU Then fShiftDown = True
        '   If this is a translatable character, send a WM_CHAR message.
            Dim ch As Long
            ch = ChFromVk(vk)
            If ch <> 0 Then
                Call SendMessage(hwnd, WM_CHAR, ch, 0)
            End If
        Else
            Call SendMessage(hwnd, WM_KEYUP, vk, lparam Or &HC0000000)
        '   Track shift key state for character translation.
            If vk = VK_SHIFT Then fShiftDown = False
            If vk = VK_CONTROL Then fCtrlDown = False
            If vk = VK_MENU Then fShiftDown = False
        End If
    Else
        Dim ki As KeybdInputType
        ki.dwType = INPUT_KEYBOARD
        ki.wVk = vk
        ki.wScan = MapVirtualKey(vk, 0)
        ki.dwFlags = 0
        If FExtendedVk(vk) Then ki.dwFlags = ki.dwFlags Or KEYEVENTF_EXTENDEDKEY
        If fKeyUp Then ki.dwFlags = ki.dwFlags Or KEYEVENTF_KEYUP
        ki.time = 0
        ki.dwExtraInfo = 0
        Call keybd_event(ki.wVk, ki.wScan, ki.dwFlags, ki.dwExtraInfo)
    End If
    
End Sub

Private Sub SendButton(ByVal hwnd As Long, ByVal dwFlags As Long)
    
    Dim mi As MouseInputType
    mi.dwType = INPUT_MOUSE
    mi.dx = 0
    mi.dy = 0
    mi.mousedata = 0
    mi.dwFlags = dwFlags
    mi.time = 0
    mi.dwExtraInfo = 0
    
    Call mouse_event(mi.dwFlags, 0, 0, mi.mousedata, mi.dwExtraInfo)
    
End Sub

Private Sub SendMouseMessage(ByVal hwnd As Long, ByVal wm As Long)
    
    Call SendMessage(hwnd, wm, mkButtons, ypMouse * &H10000 + xpMouse)
    
End Sub

Private Function FExtendedVk(ByVal vk As Long) As Boolean
    
    Select Case vk
    Case VK_MENU, _
      VK_INSERT, VK_DELETE, VK_HOME, VK_END, VK_PRIOR, VK_NEXT, _
      VK_UP, VK_DOWN, VK_LEFT, VK_RIGHT, _
      VK_DIVIDE, VK_EXECUTE, VK_SNAPSHOT ' , VK_CONTROL
        FExtendedVk = True
    Case Else
        FExtendedVk = False
    End Select
    
End Function

Private Function ChFromVk(ByVal vk As Long) As Long
    
    Dim rgbKbdState(255) As Byte
    Call GetKeyboardState(rgbKbdState(0))
    
    rgbKbdState(VK_SHIFT) = IIf(fShiftDown, &H80, 0)
    rgbKbdState(VK_CONTROL) = IIf(fCtrlDown, &H80, 0)
    rgbKbdState(VK_MENU) = IIf(fAltDown, &H80, 0)
    
    Dim rgch(1) As Integer
    Call ToAscii(vk, MapVirtualKey(vk, 0), rgbKbdState(0), rgch(0), 0)
    
    ChFromVk = rgch(0)
    
End Function





















