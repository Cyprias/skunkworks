Attribute VB_Name = "SWLang"
Option Explicit


'--------------
' Private data
'--------------

Private rgszLang(10) As String
Private rgszExt(10) As String
Private iszMac As Long


'------------------
' Public functions
'------------------

Public Sub InitSWLang()
    Call TraceEnter("InitSWLang")
    
    iszMac = 0
    
'   Scan the registry for installed scripting languages.
    Dim ikey As Long, szExt As String
    ikey = 0
    Do While FEnumKey(szExt, "", HKEY_CLASSES_ROOT, ikey)
        If FPrefixMatchSzI(szExt, ".") Then
            Dim hkeyExt As Long
            hkeyExt = HkeyOpen(HKEY_CLASSES_ROOT, szExt, KEY_QUERY_VALUE)
            If hkeyExt <> hNil Then
                Dim szFileType As String
                If FGetSzValue(szFileType, hkeyExt, "") Then
                    Dim hkeyFileType As Long
                    hkeyFileType = HkeyOpen(HKEY_CLASSES_ROOT, _
                      szFileType, KEY_ENUMERATE_SUB_KEYS)
                    If hkeyFileType <> hNil Then
                        Dim hkeyEngine As Long
                        hkeyEngine = HkeyOpen(hkeyFileType, _
                          "ScriptEngine", KEY_QUERY_VALUE)
                        If hkeyEngine <> hNil Then
                            Dim szEngine As String
                            If FGetSzValue(szEngine, hkeyEngine, "") Then
                            '   REVIEW: Filter out encoded script types.
                                If Not FSuffixMatchSzI(szEngine, ".Encode") Then
                                    If fTrace Then Call TraceLine("Language found: " + _
                                      szEngine + " version " + SzVerFromSzEngine(szEngine))
                                    rgszLang(iszMac) = szEngine
                                    rgszExt(iszMac) = LCase(Right(szExt, Len(szExt) - 1))
                                    iszMac = iszMac + 1
                                End If
                            End If
                            Call RegCloseKey(hkeyEngine)
                        End If
                        Call RegCloseKey(hkeyFileType)
                    End If
                End If
                Call RegCloseKey(hkeyExt)
            End If
        End If
        ikey = ikey + 1
    Loop
    
    Call TraceExit("InitSWLang")
End Sub

Public Function SzExtFromSzLang(ByVal szLang As String) As String

    Dim isz As Long
    For isz = 0 To iszMac - 1
        If FEqSzI(rgszLang(isz), szLang) Then
            SzExtFromSzLang = rgszExt(isz)
            Exit Function
        End If
    Next isz
    
    SzExtFromSzLang = ""
    
End Function

Public Function SzLangFromSzExt(ByVal szExt As String) As String
    
    Dim isz As Long
    For isz = 0 To iszMac - 1
        If FEqSzI(rgszExt(isz), szExt) Then
            SzLangFromSzExt = rgszLang(isz)
            Exit Function
        End If
    Next isz
    
    SzLangFromSzExt = ""
    
End Function

' Deduce the scripting language from the filename extension.
Public Function SzLangFromSzFile(ByVal szFile As String) As String
    
    SzLangFromSzFile = SzLangFromSzExt(SzExtension(szFile))
    
End Function

Public Function SzFilterFromSzLang(ByVal szLang As String, _
  Optional ByVal fIncludeSwx As Boolean = False, _
  Optional ByVal fIncludeAll As Boolean = True) As String
    
    Dim szFilter As String
    szFilter = ""
    
    If fIncludeSwx Then
        Call AddFilter(szFilter, "SkunkWorks projects", "swx")
    End If
    
'   Put the specified language first.
    If szLang <> "" Then
        Call AddFilter(szFilter, _
          szLang + " files", SzExtFromSzLang(szLang))
    End If
    
'   Append all the rest.
    Dim isz As Long
    For isz = 0 To iszMac - 1
        If Not FEqSzI(rgszLang(isz), szLang) Then
            Call AddFilter(szFilter, rgszLang(isz) + " files", rgszExt(isz))
        End If
    Next isz
    
    If fIncludeAll Then
        Call AddFilter(szFilter, "All files", "*")
    End If
    
    SzFilterFromSzLang = szFilter
    
End Function


'----------
' Privates
'----------

Private Sub AddFilter(ByRef szFilter As String, _
  ByVal szDesc As String, ByVal szExt As String)
    
    If szFilter <> "" Then szFilter = szFilter + "|"
    szFilter = szFilter + szDesc + " (*." + szExt + ")|*." + szExt
    
End Sub


Private Function SzVerFromSzEngine(ByVal szEngine As String)
    
    SzVerFromSzEngine = "???"
    
    Dim hkeyClsid As Long
    hkeyClsid = HkeyOpen(HKEY_CLASSES_ROOT, szEngine + "\CLSID", KEY_QUERY_VALUE)
    If hkeyClsid <> hNil Then
        Dim szClsid As String
        If FGetSzValue(szClsid, hkeyClsid, "") Then
            Dim hkeyServer As Long
            hkeyServer = HkeyOpen(HKEY_CLASSES_ROOT, _
              "CLSID\" + szClsid + "\InProcServer32", KEY_QUERY_VALUE)
            If hkeyServer <> hNil Then
                Dim szExe As String
                If FGetSzValue(szExe, hkeyServer, "") Then
                    SzVerFromSzEngine = SzExeVerFromFile(szExe)
                End If
                Call RegCloseKey(hkeyServer)
            End If
        End If
        Call RegCloseKey(hkeyClsid)
    End If
    
End Function



