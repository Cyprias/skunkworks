VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SWOptionsCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


'--------------
' Private data
'--------------

Public Enum PxeType
    pxeNone
    pxeMost
    pxeAll
End Enum

Public Enum PxmType
    pxmNone
    pxmUnknown
    pxmUnhandled
    pxmAll
End Enum

Private szConsoleExeMbr As String
Private fActivateACMbr As Boolean
Private fCaptureMbr As Boolean
Private fCheckMsgVerMbr As Boolean
Private vkHotkeyMbr As Long
Private szCmdPfxMbr As String
Private szConsolePfxMbr As String
Private fDevMbr As Boolean
Private fLogMbr As Boolean
Private pxeMbr As PxmType, opmPxeMbr As Long
Private pxmMbr As PxmType, fPxCTSMbr As Boolean, fPxWholeMsgMbr As Boolean
Private fMemStatsMbr As Boolean
Private fEnableDebugMbr As Boolean
Private fBreakOnEntryMbr As Boolean
Private szLoginScriptMbr As String
Private dheadStopForTurnMbr As Long


'------------
' Properties
'------------

Public szAppDir As String
Public szDocsDir As String
Public szLibDir As String
Public szSourceDir As String


Public Property Get szAppName() As String
    szAppName = "SkunkWorks"
End Property


Public Property Get szConsoleExe() As String
    szConsoleExe = szConsoleExeMbr
End Property

Public Property Let szConsoleExe(ByVal szNew As String)
    szConsoleExeMbr = fso.GetAbsolutePathName(szNew)
    Call SaveSetting(szAppName, "Settings", "Console", szConsoleExeMbr)
End Property


Public Property Get fCapture() As Boolean
    fCapture = fCaptureMbr
End Property

Public Property Let fCapture(ByVal fNew As Boolean)
    fCaptureMbr = fNew
    Call SaveSetting(szAppName, "Settings", "Capture", CStr(fCaptureMbr))
End Property


Public Property Get fCheckMsgVer() As Boolean
    fCheckMsgVer = fCheckMsgVerMbr
End Property

Public Property Let fCheckMsgVer(ByVal fNew As Boolean)
    fCheckMsgVerMbr = fNew
    Call SaveSetting(szAppName, "Settings", "CheckMsgVer", CStr(fCheckMsgVerMbr))
End Property


Public Property Get vkHotkey() As Long
    vkHotkey = vkHotkeyMbr
End Property

Public Property Let vkHotkey(ByVal vkNew As Long)
    vkHotkeyMbr = vkNew
    Call SaveSetting(szAppName, "Settings", "Hotkey", SzHex(vkHotkeyMbr, 2))
End Property


Public Property Get szCmdPfx() As String
    szCmdPfx = szCmdPfxMbr
End Property

Public Property Let szCmdPfx(ByVal szNew As String)
    szCmdPfxMbr = szNew
    Call SaveSetting(szAppName, "Settings", "Command Prefix", szCmdPfxMbr)
End Property


Public Property Get szConsolePfx() As String
    szConsolePfx = szConsolePfxMbr
End Property

Public Property Let szConsolePfx(ByVal szNew As String)
    szConsolePfxMbr = szNew
    Call SaveSetting(szAppName, "Settings", "Console Prefix", szConsolePfxMbr)
End Property


Public Property Get fDev() As Boolean
    fDev = fDevMbr
End Property

Public Property Let fDev(ByVal fNew As Boolean)
    fDevMbr = fNew
    Call SaveSetting(szAppName, "Settings", "Dev", CStr(fDevMbr))
End Property


Public Property Get fLog() As Boolean
    fLog = fLogMbr
End Property

Public Property Let fLog(ByVal fNew As Boolean)
    fLogMbr = fNew
    Call SaveSetting(szAppName, "Settings", "Log", CStr(fLogMbr))
End Property


Public Property Get pxe() As PxeType
    pxe = pxeMbr
End Property

Public Property Let pxe(ByVal pxeNew As PxeType)
    pxeMbr = pxeNew
    Call SaveSetting(szAppName, "Settings", "Pxe", CStr(pxeMbr))
End Property


Public Property Get opmPxe() As Long
    opmPxe = opmPxeMbr
End Property

Public Property Let opmPxe(ByVal opmNew As Long)
    opmPxeMbr = opmNew
    Call SaveSetting(szAppName, "Settings", "opmPxe", "&H" + SzHex(opmPxeMbr, 2))
End Property


Public Property Get pxm() As PxmType
    pxm = pxmMbr
End Property

Public Property Let pxm(ByVal pxmNew As PxmType)
    pxmMbr = pxmNew
    Call SaveSetting(szAppName, "Settings", "Pxmode", CStr(pxmMbr))
End Property


Public Property Get fPxCTS() As Boolean
    fPxCTS = fPxCTSMbr
End Property

Public Property Let fPxCTS(ByVal fNew As Boolean)
    fPxCTSMbr = fNew
    Call SaveSetting(szAppName, "Settings", "PxCTS", CStr(fPxCTSMbr))
End Property


Public Property Get fPxWholeMsg() As Boolean
    fPxWholeMsg = fPxWholeMsgMbr
End Property

Public Property Let fPxWholeMsg(ByVal fNew As Boolean)
    fPxWholeMsgMbr = fNew
    Call SaveSetting(szAppName, "Settings", "PxWholeMsg", CStr(fPxWholeMsgMbr))
End Property


Public Property Get fTrace() As Boolean
    fTrace = SWError.fTrace
End Property

Public Property Let fTrace(ByVal fNew As Boolean)
    SWError.fTrace = fNew
    Call SaveSetting(szAppName, "Settings", "Trace", CStr(SWError.fTrace))
End Property


Public Property Get fMemStats() As Boolean
    fMemStats = fMemStatsMbr
End Property

Public Property Let fMemStats(ByVal fNew As Boolean)
    fMemStatsMbr = fNew
    Call SaveSetting(szAppName, "Settings", "MemStats", CStr(fMemStatsMbr))
End Property


Public Property Get fEnableDebug() As Boolean
    fEnableDebug = fEnableDebugMbr
End Property

Public Property Let fEnableDebug(ByVal fNew As Boolean)
    fEnableDebugMbr = fNew
    Call SaveSetting(szAppName, "Settings", "EnableDebug", CStr(fEnableDebugMbr))
End Property


Public Property Get fBreakOnEntry() As Boolean
    fBreakOnEntry = fBreakOnEntryMbr
End Property

Public Property Let fBreakOnEntry(ByVal fNew As Boolean)
    fBreakOnEntryMbr = fNew
    Call SaveSetting(szAppName, "Settings", "BreakOnEntry", CStr(fBreakOnEntryMbr))
End Property


Public Property Get szLoginScript() As String
    szLoginScript = szLoginScriptMbr
End Property

Public Property Let szLoginScript(ByVal szNew As String)
    szLoginScriptMbr = szNew
    Call SaveSetting(szAppName, "Settings", "Login Script", szLoginScriptMbr)
End Property


Public Property Get dheadStopForTurn() As Long
    dheadStopForTurn = dheadStopForTurnMbr
End Property

Public Property Let dheadStopForTurn(ByVal vkNew As Long)
    dheadStopForTurnMbr = vkNew
    Call SaveSetting(szAppName, "Settings", "StopFOrTurn", CStr(dheadStopForTurnMbr))
End Property


'---------
' Methods
'---------

Public Function SzFileInAppDir(ByVal szFileLeaf As String) As String
    SzFileInAppDir = fso.BuildPath(szAppDir, szFileLeaf)
End Function

Public Function SzFileInLibDir(ByVal szFileLeaf As String) As String
    SzFileInLibDir = fso.BuildPath(szLibDir, szFileLeaf)
End Function

Public Function SzFileInSourceDir(ByVal szFileLeaf As String) As String
    SzFileInSourceDir = fso.BuildPath(szSourceDir, szFileLeaf)
    If Not fso.FileExists(SzFileInSourceDir) Then
        SzFileInSourceDir = SzFileInAppDir(szFileLeaf)
    End If
End Function

Public Sub Reload()
    Call TraceEnter("SWOptions.Reload")
    
    szConsoleExeMbr = GetSetting(szAppName, "Settings", "Console", _
      SzQuote(SzFileInAppDir("SWConsole.exe")) + " /S")
    
    fActivateACMbr = CBool(GetSetting(szAppName, "Settings", "ActivateAC", "True"))
    fCaptureMbr = CBool(GetSetting(szAppName, "Settings", "Capture", "False"))
    fCheckMsgVerMbr = CBool(GetSetting(szAppName, "Settings", "CheckMsgVer", "True"))
    vkHotkeyMbr = CLng("&H" + GetSetting(szAppName, "Settings", "Hotkey", "00"))
    szCmdPfxMbr = GetSetting(szAppName, "Settings", "Command Prefix", "/sw ")
    szConsolePfxMbr = GetSetting(szAppName, "Settings", "Console Prefix", "/swc ")
    fDevMbr = CBool(GetSetting(szAppName, "Settings", "Dev", "False"))
    fLogMbr = CBool(GetSetting(szAppName, "Settings", "Log", "False"))
    pxeMbr = CLng(GetSetting(szAppName, "Settings", "Pxe", "0"))
    opmPxeMbr = CLng(GetSetting(szAppName, "Settings", "opmPxe", "&H11"))
    pxmMbr = CLng(GetSetting(szAppName, "Settings", "Pxmode", "0"))
    fPxCTSMbr = CBool(GetSetting(szAppName, "Settings", "PxCTS", "False"))
    fPxWholeMsgMbr = CBool(GetSetting(szAppName, "Settings", "PxWholeMsg", "True"))
    SWError.fTrace = CBool(GetSetting(szAppName, "Settings", "Trace", "False"))
    fMemStatsMbr = CBool(GetSetting(szAppName, "Settings", "MemStats", "False"))
    fEnableDebugMbr = CBool(GetSetting(szAppName, "Settings", "EnableDebug", "False"))
    fBreakOnEntryMbr = CBool(GetSetting(szAppName, "Settings", "BreakOnEntry", "False"))
    szLoginScriptMbr = GetSetting(szAppName, "Settings", "Login Script", "")
    dheadStopForTurnMbr = CLng(GetSetting(szAppName, "Settings", "StopForTurn", "60"))
    
    Const VK_CAPITAL = &H14
    If (GetKeyState(VK_CAPITAL) And 1) <> 0 Then
        SWError.fTrace = True
        fLogMbr = True
    End If
    
    Call TraceExit("SWOptions.Reload")
End Sub

'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceEnter("SWOptions.Initialize")
    
    Call FindDirs
    
    Call Reload
    
    Call TraceExit("SWOptions.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceEnter("SWOptions.Terminate")
    
    Call TraceExit("SWOptions.Terminate")
End Sub

Private Sub FindDirs()
    
    szAppDir = App.Path
    If FEqSzFile(SzLeaf(szAppDir), "Source") Then
    '   Must be running under VB debugger; go up one level to real app dir.
        szAppDir = SzDirFromPath(szAppDir)
    End If
    
    szDocsDir = fso.BuildPath(szAppDir, "Docs")
    If Not fso.FolderExists(szDocsDir) Then szDocsDir = szAppDir
    
    szLibDir = fso.BuildPath(szAppDir, "Libraries")
    If Not fso.FolderExists(szLibDir) Then szLibDir = szAppDir
    
    szSourceDir = fso.BuildPath(szAppDir, "Source")
    If Not fso.FolderExists(szSourceDir) Then szSourceDir = szAppDir
    
End Sub

































