VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SWPluginCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
Implements IPlugin2


'--------------
' Private data
'--------------

Private swf As SWFilterCls
Attribute swf.VB_VarHelpID = -1


'--------------------
' IPlugin2 interface
'--------------------

Private Sub IPlugin2_Initialize(ByVal site2 As Decal.IPluginSite2)
    Call TraceEnter("SWPlugin.IPlugin2_Initialize")
    On Error GoTo LNoFilter
    
'   Attach to the SkunkWorks netfilter.
    Set decalapp = New DecalCls
    If Not decalapp.FFilterEnabled(szClsidSWFilter) Then GoTo LNoFilter
    Set swf = site2.Object("services\DecalNet.NetService\SWFilter.SWFilterCls")
    If swf Is Nothing Then GoTo LNoFilter
    
    Call swf.Private_InitPlugin(site2)
    
    GoTo LExit
    
LNoFilter:
    Call LogError("SWPlugin", "SkunkWorks filter is not running.")
    
LExit:
    Call TraceExit("SWPlugin.IPlugin2_Initialize")
End Sub

Private Sub IPlugin2_Terminate()
    Call TraceEnter("SWPlugin.IPlugin2_Terminate")
    On Error Resume Next
    
    If Not (swf Is Nothing) Then
        Call swf.Private_TerminatePlugin
        Set swf = Nothing
    End If
    
    Call TraceExit("SWPlugin.IPlugin2_Terminate")
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceEnter("SWPlugin.Class_Initialize")
    
    Call TraceExit("SWPlugin.Class_Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceEnter("SWPlugin.Class_Terminate")
    
    Call TraceExit("SWPlugin.Class_Terminate")
End Sub


















