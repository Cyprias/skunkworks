VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SWSrvCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' This object encapsulates the inter-process communication with Skapi.dll.


'-------------
' Public data
'-------------

Public mbufEvent As MbufCls


'--------------
' Private data
'--------------

Private fAttachedMbr As Boolean
Private iqMbr As Long

Private queueCommands As QueueCls, mbufCommand As MbufCls
Private queueEvents As QueueCls
Private heventAck As Long

Private fnCapture As Long


'--------
' Events
'--------

' ACHooks commands:
Public Event AddChatText(ByVal szText As String, ByVal lColor As Long, ByVal lTarget As Long)
Public Event AddChatTextRaw(ByVal szText As String, ByVal lColor As Long, ByVal lTarget As Long)
Public Event AddStatusText(ByVal szText As String)
Public Event ApplyItem(ByVal UseThis As Long, ByVal OnThis As Long)
Public Event CastSpell(ByVal lSpellID As Long, ByVal lObjectID As Long)
Public Event DropItem(ByVal lObjectID As Long)
Public Event FaceHeading(ByVal fHeading As Single, ByVal bUnknown As Boolean)
Public Event GiveItem(ByVal lObject As Long, ByVal lDestination As Long)
Public Event IDQueueAdd(ByVal lObjectID As Long)
Public Event InvokeChatParser(ByVal szText As String)
Public Event Logout()
Public Event MoveItem(ByVal lObjectID As Long, ByVal lPackID As Long, ByVal lSlot As Long, ByVal bStack As Boolean)
Public Event MoveItemEx(ByVal lObjectID As Long, ByVal lDestinationID As Long)
Public Event MoveItemExRaw(ByVal lObject As Long, ByVal lDestination As Long, ByVal lMoveFlags As Long)
Public Event RaiseAttribute(ByVal AttribID As Long, ByVal lExperience As Long)
Public Event RaiseSkill(ByVal SkillID As Long, ByVal lExperience As Long)
Public Event RaiseVital(ByVal VitalID As Long, ByVal lExperience As Long)
Public Event RequestID(ByVal lObjectID As Long)
Public Event SalvagePanelAdd(ByVal lObjectID As Long)
Public Event SalvagePanelSalvage()
Public Event SelectItem(ByVal lObjectID As Long)
Public Event SetAutorun(ByVal bOnOff As Boolean)
Public Event SetCombatMode(ByVal pVal As Long)
Public Event SetCurrentSelection(ByVal pVal As Long)
Public Event SetCursorPosition(ByVal lX As Long, ByVal lY As Long)
Public Event SetIdleTime(ByVal dIdleTimeout As Double)
Public Event SetPreviousSelection(ByVal pVal As Long)
Public Event SetSelectedStackCount(ByVal pVal As Long)
Public Event SpellTabAdd(ByVal lTab As Long, ByVal lIndex As Long, ByVal lSpellID As Long)
Public Event SpellTabDelete(ByVal lTab As Long, ByVal lSpellID As Long)
Public Event TradeAccept()
Public Event TradeAdd(ByVal ItemID As Long)
Public Event TradeEnd()
Public Event TradeDecline()
Public Event TradeReset()
Public Event UseItem(ByVal lObjectID As Long, ByVal lUseState As Long)
Public Event UseItemRaw(ByVal lObjectID As Long, ByVal lUseState As Long, ByVal lUseMethod As Long)
Public Event VendorBuyAll()
Public Event VendorBuyListAdd(ByVal lID As Long, ByVal lAmount As Long)
Public Event VendorBuyListClear()
Public Event VendorSellAll()
Public Event VendorSellListAdd(ByVal lID As Long)
Public Event VendorSellListClear()

' Other commands:
Public Event ButtonEvent(ByVal ibutton As Long, ByVal kmo As Long)
Public Event CallPanelFunction(ByVal szPanel As String, ByVal szFunction As String, _
  ByRef v1 As Variant, ByRef v2 As Variant, ByRef v3 As Variant, _
  ByRef v4 As Variant, ByRef v5 As Variant)
Public Event CancelNav()
Public Event DisplaySz(ByVal sz As String, ByVal cmc As Long, ByVal iwnd As Long)
Public Event EnableHotkey(ByVal sz As String, ByVal vk As Long, ByVal fEnable As Boolean)
Public Event GoToLatLng(ByVal lat As Single, ByVal lng As Single, ByVal distArrive As Single, _
  ByVal cmsecTimeout As Long, ByVal nopt As Long, ByVal fRunAsDefault As Boolean)
Public Event MouseMove(ByVal xp As Long, ByVal yp As Long)
Public Event ReloadOptions()
Public Event RemoveControls(ByVal szPanel As String)
Public Event ScriptMsg(ByVal szMsg As String)
Public Event ShowControls(ByVal szViewSchema As String, ByVal fActivate As Boolean, _
  ByVal szSkapiDir As String)
Public Event TurnToHead(ByVal head As Single)
Public Event VkEvent(ByVal vk As Long, ByVal kmo As Long)

Public Event GetControlProperty(ByVal szPanel As String, _
  ByVal szControl As String, ByVal szProperty As String)
Public Event SetControlProperty(ByVal szPanel As String, _
  ByVal szControl As String, ByVal szProperty As String, ByVal vValue As Variant)

' Non-command events:
Public Event QueueFull(ByVal fConnected As Boolean)
Public Event Disconnect()


'------------
' Properties
'------------

Public Property Get fAttached() As Boolean
    fAttached = fAttachedMbr
End Property


Public Property Get fConnected() As Boolean
    fConnected = queueEvents.fConnected
End Property


Public Property Get fCapture() As Boolean
    fCapture = (fnCapture <> iNil)
End Property

Public Property Let fCapture(ByVal fNew As Boolean)
    Call TraceEnter("SWSrv.fCapture", "<- " + CStr(fNew))
    On Error GoTo LError
    
'   Capture message stream to a file for debugging purposes.
    If fNew And fnCapture = iNil Then
        Dim szFileCapture As String
        szFileCapture = SzFileInTempDir("Capture.acmsg")
        Call TraceLine("Capture to " + szFileCapture)
        Call DeleteFile(szFileCapture)
        fnCapture = FreeFile
        Open szFileCapture For Binary Access Write As fnCapture
        Put fnCapture, , signatureCapture
        Put fnCapture, , versionCapture
    ElseIf fnCapture <> iNil And Not fNew Then
        Close fnCapture
        fnCapture = iNil
    End If
    
    GoTo LExit
    
LError:
    Call LogError("SWSrv.fCapture", Err.Description, Err.Number)
    On Error Resume Next
    Close fnCapture
    fnCapture = iNil
    
LExit:
    Call TraceExit("SWSrv.fCapture")
End Property


'---------
' Methods
'---------

Public Sub Attach()
    Call TraceEnter("SWSrv.Attach")
    
    fAttachedMbr = True
    
    Call TraceExit("SWSrv.Attach")
End Sub

Public Sub Detach()
    Call TraceEnter("SWSrv.Detach")
    
    fAttachedMbr = False
    
    Call TraceExit("SWSrv.Detach")
End Sub


Public Sub LaunchConsole()
    Call TraceEnter("SWSrv.LaunchConsole")
    On Error GoTo LError
    
    If Not fConnected And swoptions.szConsoleExe <> "" Then
        Call TraceLine("Launching " + swoptions.szConsoleExe)
        Call Shell(swoptions.szConsoleExe, vbMinimizedNoFocus)
    End If
    
    GoTo LExit
    
LError:
    Call LogError("SWSrv.LaunchConsole", Err.Description, Err.Number)
    
LExit:
    Call TraceExit("SWSrv.LaunchConsole")
End Sub

Public Sub QueueEventMbuf()
    
    Dim rgb() As Byte
    Call mbufEvent.GetAsRgb(rgb)
    Call QueueEventRgb(rgb, mbufEvent.cbOfMsg, GetTickCount())
    
End Sub

' Queue raw event data for processing by Skapi.dll.
Public Sub QueueEventRgb(rgb() As Byte, ByVal cb As Long, ByVal tick As Long)
    
    Static fConnectedPrev As Boolean
    Dim fConnected As Boolean
    fConnected = queueEvents.fConnected
    
    If fConnectedPrev And Not fConnected Then
        RaiseEvent Disconnect
    End If
    
    If Not queueEvents.FWriteMsg(rgb, cb, tick, 0, False) Then
        RaiseEvent QueueFull(fConnected)
    End If
    
    If fnCapture <> iNil Then
    '   Write a copy to the capture file.
        Put fnCapture, , tick
        Put fnCapture, , cb
        Put fnCapture, , rgb
    End If
    
    fConnectedPrev = fConnected
    
End Sub


' Poll the commands queue for incoming commands.  Events will be raised as
' commands are dequeued.  This method must be called frequently by the plugin.
Public Sub Poll()
'    Call TraceEnter("SWSrv.Poll")
    On Error GoTo LError
    
'   See if the queue is non-empty (by waiting zero milliseconds).
    Do While mbufCommand.FDequeueMsg(queueCommands, 0, False)
    '   Dequeue the command.
        Dim swc As SwcType
        swc = mbufCommand.Get_DWORD()
        Call TraceEnter("SWSrv.PollLoop", "swc = " + CStr(swc))
        
    '   Dispatch on command type to dequeue args and raise the appropriate event.
        Select Case swc
            
    '   ACHooks commands:
        
        Case swcAddChatText
            Dim szText As String, lColor As Long, lTarget As Long
            szText = mbufCommand.Get_String()
            lColor = mbufCommand.Get_DWORD()
            lTarget = mbufCommand.Get_DWORD()
            RaiseEvent AddChatText(szText, lColor, lTarget)
            
        Case swcAddChatTextRaw
            szText = mbufCommand.Get_String()
            lColor = mbufCommand.Get_DWORD()
            lTarget = mbufCommand.Get_DWORD()
            RaiseEvent AddChatTextRaw(szText, lColor, lTarget)
            
        Case swcAddStatusText
            szText = mbufCommand.Get_String()
            RaiseEvent AddStatusText(szText)
            
        Case swcApplyItem
            Dim UseThis As Long, OnThis As Long
            UseThis = mbufCommand.Get_DWORD()
            OnThis = mbufCommand.Get_DWORD()
            RaiseEvent ApplyItem(UseThis, OnThis)
            
        Case swcCastSpell
            Dim lSpellID As Long, lObjectID As Long
            lSpellID = mbufCommand.Get_DWORD()
            lObjectID = mbufCommand.Get_DWORD()
            RaiseEvent CastSpell(lSpellID, lObjectID)
            
        Case swcDropItem
            lObjectID = mbufCommand.Get_DWORD()
            RaiseEvent DropItem(lObjectID)
            
        Case swcFaceHeading
            Dim fHeading As Single, bUnknown As Boolean
            fHeading = mbufCommand.Get_float()
            bUnknown = mbufCommand.Get_DWORD()
            RaiseEvent FaceHeading(fHeading, bUnknown)
            
        Case swcGiveItem
            Dim lObject As Long, lDestination As Long
            lObject = mbufCommand.Get_DWORD()
            lDestination = mbufCommand.Get_DWORD()
            RaiseEvent GiveItem(lObject, lDestination)
            
        Case swcIDQueueAdd
            lObjectID = mbufCommand.Get_DWORD()
            RaiseEvent IDQueueAdd(lObjectID)
            
        Case swcInvokeChatParser
            szText = mbufCommand.Get_String()
            RaiseEvent InvokeChatParser(szText)
            
        Case swcLogout
            RaiseEvent Logout
            
        Case swcMoveItem
            Dim lPackID As Long, lSlot As Long, bStack As Boolean
            lObjectID = mbufCommand.Get_DWORD()
            lPackID = mbufCommand.Get_DWORD()
            lSlot = mbufCommand.Get_DWORD()
            bStack = mbufCommand.Get_DWORD()
            RaiseEvent MoveItem(lObjectID, lPackID, lSlot, bStack)
            
        Case swcMoveItemEx
            Dim lDestinationID As Long
            lObjectID = mbufCommand.Get_DWORD()
            lDestinationID = mbufCommand.Get_DWORD()
            RaiseEvent MoveItemEx(lObjectID, lDestinationID)
            
        Case swcMoveItemExRaw
            Dim lMoveFlags As Long
            lObject = mbufCommand.Get_DWORD()
            lDestination = mbufCommand.Get_DWORD()
            lMoveFlags = mbufCommand.Get_DWORD()
            RaiseEvent MoveItemExRaw(lObject, lDestination, lMoveFlags)
            
        Case swcRaiseAttribute
            Dim AttribID As Long, lExperience As Long
            AttribID = mbufCommand.Get_DWORD()
            lExperience = mbufCommand.Get_DWORD()
            RaiseEvent RaiseAttribute(AttribID, lExperience)
            
        Case swcRaiseSkill
            Dim SkillID As Long
            SkillID = mbufCommand.Get_DWORD()
            lExperience = mbufCommand.Get_DWORD()
            RaiseEvent RaiseSkill(SkillID, lExperience)
            
        Case swcRaiseVital
            Dim VitalID As Long
            VitalID = mbufCommand.Get_DWORD()
            lExperience = mbufCommand.Get_DWORD()
            RaiseEvent RaiseVital(VitalID, lExperience)
            
        Case swcRequestID
            lObjectID = mbufCommand.Get_DWORD()
            RaiseEvent RequestID(lObjectID)
            
        Case swcSalvagePanelAdd
            lObjectID = mbufCommand.Get_DWORD()
            RaiseEvent SalvagePanelAdd(lObjectID)
            
        Case swcSalvagePanelSalvage
            RaiseEvent SalvagePanelSalvage
            
        Case swcSelectItem
            lObjectID = mbufCommand.Get_DWORD()
            RaiseEvent SelectItem(lObjectID)
            
        Case swcSetAutorun
            Dim bOnOff As Boolean
            bOnOff = mbufCommand.Get_DWORD()
            RaiseEvent SetAutorun(bOnOff)
            
        Case swcSetCombatMode
            Dim pVal As Long
            pVal = mbufCommand.Get_DWORD()
            RaiseEvent SetCombatMode(pVal)
            
        Case swcSetCurrentSelection
            pVal = mbufCommand.Get_DWORD()
            RaiseEvent SetCurrentSelection(pVal)
            
        Case swcSetCursorPosition
            Dim lX As Long, lY As Long
            lX = mbufCommand.Get_DWORD()
            lY = mbufCommand.Get_DWORD()
            RaiseEvent SetCursorPosition(lX, lY)
            
        Case swcSetIdleTime
            Dim dIdleTimeout As Double
            dIdleTimeout = mbufCommand.Get_double()
            RaiseEvent SetIdleTime(dIdleTimeout)
            
        Case swcSetPreviousSelection
            pVal = mbufCommand.Get_DWORD()
            RaiseEvent SetPreviousSelection(pVal)
            
        Case swcSetSelectedStackCount
            pVal = mbufCommand.Get_DWORD()
            RaiseEvent SetSelectedStackCount(pVal)
            
        Case swcSpellTabAdd
            Dim lTab As Long, lIndex As Long
            lTab = mbufCommand.Get_DWORD()
            lIndex = mbufCommand.Get_DWORD()
            lSpellID = mbufCommand.Get_DWORD()
            RaiseEvent SpellTabAdd(lTab, lIndex, lSpellID)
            
        Case swcSpellTabDelete
            lTab = mbufCommand.Get_DWORD()
            lSpellID = mbufCommand.Get_DWORD()
            RaiseEvent SpellTabDelete(lTab, lSpellID)
            
        Case swcTradeAccept
            RaiseEvent TradeAccept
            
        Case swcTradeAdd
            Dim ItemID As Long
            ItemID = mbufCommand.Get_DWORD()
            RaiseEvent TradeAdd(ItemID)
            
        Case swcTradeEnd
            RaiseEvent TradeEnd
            
        Case swcTradeDecline
            RaiseEvent TradeDecline
            
        Case swcTradeReset
            RaiseEvent TradeReset
            
        Case swcUseItem
            Dim lUseState As Long
            lObjectID = mbufCommand.Get_DWORD()
            lUseState = mbufCommand.Get_DWORD()
            RaiseEvent UseItem(lObjectID, lUseState)
            
        Case swcUseItemRaw
            Dim lUseMethod As Long
            lObjectID = mbufCommand.Get_DWORD()
            lUseState = mbufCommand.Get_DWORD()
            lUseMethod = mbufCommand.Get_DWORD()
            RaiseEvent UseItemRaw(lObjectID, lUseState, lUseMethod)
            
        Case swcVendorBuyAll
            RaiseEvent VendorBuyAll
            
        Case swcVendorBuyListAdd
            Dim lID As Long, lAmount As Long
            lID = mbufCommand.Get_DWORD()
            lAmount = mbufCommand.Get_DWORD()
            RaiseEvent VendorBuyListAdd(lID, lAmount)
            
        Case swcVendorBuyListClear
            RaiseEvent VendorBuyListClear
            
        Case swcVendorSellAll
            RaiseEvent VendorSellAll
            
        Case swcVendorSellListAdd
            lID = mbufCommand.Get_DWORD()
            RaiseEvent VendorSellListAdd(lID)
            
        Case swcVendorSellListClear
            RaiseEvent VendorSellListClear
            
    '   Other commands:
        
        Case swcButtonEvent
            Dim ibutton As Long, kmo As Long
            ibutton = mbufCommand.Get_WORD()
            kmo = mbufCommand.Get_WORD()
            RaiseEvent ButtonEvent(ibutton, kmo)
            
        Case swcCallPanelFunction
            Dim szPanel As String, szFunction As String
            Dim v1 As Variant, v2 As Variant, v3 As Variant, v4 As Variant, v5 As Variant
            szPanel = mbufCommand.Get_String()
            szFunction = mbufCommand.Get_String()
            Call mbufCommand.GetVariantByRef(v1)
            Call mbufCommand.GetVariantByRef(v2)
            Call mbufCommand.GetVariantByRef(v3)
            Call mbufCommand.GetVariantByRef(v4)
            Call mbufCommand.GetVariantByRef(v5)
            RaiseEvent CallPanelFunction(szPanel, szFunction, v1, v2, v3, v4, v5)
            
        Case swcCancelNav
            RaiseEvent CancelNav
            
        Case swcDisplaySz
            Dim iwnd As Long, isz As Long, csz As Long, sz As String, cmc As Long
            iwnd = mbufCommand.Get_DWORD()
            csz = mbufCommand.Get_DWORD()
            For isz = 0 To csz - 1
                sz = mbufCommand.Get_String()
                cmc = mbufCommand.Get_DWORD()
                RaiseEvent DisplaySz(sz, cmc, iwnd)
            Next isz
        '   REVIEW: Disable this until Decal is fixed.
'            RaiseEvent DisplaySz(vbLf, 0, iwnd)
            
        Case swcEnableHotkey
            Dim vk As Long, fEnable As Boolean
            sz = mbufCommand.Get_String()
            vk = mbufCommand.Get_DWORD()
            fEnable = mbufCommand.Get_DWORD()
            RaiseEvent EnableHotkey(sz, vk, fEnable)
            
        Case swcGetControlProperty
            Dim szControl As String, szProperty As String
            szPanel = mbufCommand.Get_String()
            szControl = mbufCommand.Get_String()
            szProperty = mbufCommand.Get_String()
            RaiseEvent GetControlProperty(szPanel, szControl, szProperty)
            
        Case swcGoToLatLng
            Dim lat As Single, lng As Single, distArrive As Single
            Dim cmsecTimeout As Long, nopt As Long
            Dim fRunAsDefault As Boolean
            lat = mbufCommand.Get_float()
            lng = mbufCommand.Get_float()
            distArrive = mbufCommand.Get_float()
            cmsecTimeout = mbufCommand.Get_DWORD()
            nopt = mbufCommand.Get_DWORD()
            fRunAsDefault = mbufCommand.Get_DWORD()
            RaiseEvent GoToLatLng(lat, lng, distArrive, cmsecTimeout, nopt, fRunAsDefault)
            
        Case swcMouseMove
            Dim xp As Long, yp As Long
            xp = mbufCommand.Get_DWORD()
            yp = mbufCommand.Get_DWORD()
            RaiseEvent MouseMove(xp, yp)
            
        Case swcReloadOptions
            RaiseEvent ReloadOptions
            
        Case swcRemoveControls
            sz = mbufCommand.Get_String()
            RaiseEvent RemoveControls(sz)
            
        Case swcScriptMsg
            sz = mbufCommand.Get_String()
            RaiseEvent ScriptMsg(sz)
            
        Case swcSetControlProperty
            Dim vValue As Variant
            szPanel = mbufCommand.Get_String()
            szControl = mbufCommand.Get_String()
            szProperty = mbufCommand.Get_String()
            Call mbufCommand.GetVariantByRef(vValue)
            RaiseEvent SetControlProperty(szPanel, szControl, szProperty, vValue)
            
        Case swcShowControls
            Dim fActivate As Boolean, szSkapiDir As String
            sz = mbufCommand.Get_String()
            fActivate = CBool(mbufCommand.Get_DWORD())
            szSkapiDir = mbufCommand.Get_String()
            RaiseEvent ShowControls(sz, fActivate, szSkapiDir)
            
        Case swcTurnToHead
            Dim head As Single
            head = mbufCommand.Get_float()
            RaiseEvent TurnToHead(head)
            
        Case swcVkEvent
            vk = mbufCommand.Get_WORD()
            kmo = mbufCommand.Get_WORD()
            RaiseEvent VkEvent(vk, kmo)
            
        End Select
        
        GoTo LAck
        
LError:
        Call LogError("SWSrv.Poll", Err.Description, Err.Number)
        
LAck:
        If swc < swcLimAck Then
        '   Acknowledge receipt of a command from the Commands queue.  To maintain
        '   synchronization between script and game, script execution is blocked
        '   until this ack is received.
            Call TraceEnter("SWSrv.Ack")
            Call Assert(SetEvent(heventAck), "SetEvent failed.")
            Call TraceExit("SWSrv.Ack")
        End If
        
        Call TraceExit("SWSrv.PollLoop")
    Loop
    
'    Call TraceExit("SWSrv.Poll")
End Sub


Public Sub UpdateSvars()
    
    Call queueEvents.PutShared(VarPtr(svars), Len(svars))
    
End Sub

Public Property Get iq() As Long
    iq = iqMbr
End Property


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceEnter("SWSrv.Initialize")
    
    heventAck = hInvalid
    fnCapture = iNil
    
'   Create the queues connecting us with Skapi.dll.
    iqMbr = 0
    Do
        Set queueCommands = New QueueCls
        If Not queueCommands.FInUse("Commands" + CStr(iqMbr)) Then
            Call DebugLine("SWSrv: Using queue " + CStr(iqMbr))
            
            Call Assert(queueCommands.FCreate("Commands" + CStr(iqMbr), ForReading), _
              "Commands queue is in use.")
            Set mbufCommand = New MbufCls
            
            Set queueEvents = New QueueCls
            Call Assert(queueEvents.FCreate("Events" + CStr(iqMbr), ForWriting, Len(svars)), _
              "Events queue is in use.")
            Set mbufEvent = New MbufCls
            
            Exit Do
        End If
        
        iqMbr = iqMbr + 1
        If iqMbr = 100 Then Call SkError("Commands queue is in use.")
    Loop
    
'   Create the command ack event.
    heventAck = CreateEvent(0, False, False, "SkunkWorks.heventAck")
    Call Assert(heventAck <> hInvalid, "Could not create event.")
    Call ResetEvent(heventAck)
    
    fAttachedMbr = False
    
    Call TraceExit("SWSrv.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceEnter("SWSrv.Terminate")
    
    If heventAck <> hInvalid Then Call CloseHandle(heventAck)
    
    If fnCapture <> iNil Then Close fnCapture
    
    Call TraceExit("SWSrv.Terminate")
End Sub























