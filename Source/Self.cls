VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SelfCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' This class collects information about the currently logged-in character.


'--------------
' Private data
'--------------

Private burdenMbr As Long
Private cpyCashMbr As Long
Private expTotalMbr As Double
Private expUnspentMbr As Double
Private skpUnspentMbr As Long
Private lvlMbr As Long
Private rankMbr As Long

Private tBirthMbr As Date
Private csecAgeMbr As Long
Private cdeathMbr As Long

Private szNameMbr As String
Private szGenderMbr As String
Private szRaceMbr As String
Private szProfessionMbr As String
Private szAccountMbr As String

Private chopMbr As Long
Private chop2Mbr As Long

Private healthMbr As Long
Private staminaMbr As Long
Private manaMbr As Long

' Skill & attribute info:
Private mpattrskinfo(attrMax - 1) As SkinfoCls
Private mpvitalskinfo(vitalMax - 1) As SkinfoCls
Private mpskidskinfo(skidMax - 1) As SkinfoCls

' Spellbook info:
Private cospellidSpellbook As Collection

' Spell tab info:
Private rgcospellidTabs(6) As Collection


'------------
' Properties
'------------

Public fractVitae As Single

' Return the character's name.
Public Property Get szName() As String
    szName = szNameMbr
End Property

' Return the character's gender in string form ("Male" or "Female").
Public Property Get szGender() As String
    szGender = szGenderMbr
End Property

' Return the name of character's "heritage group" in string form (e.g. "Sho").
Public Property Get szRace() As String
    szRace = szRaceMbr
End Property

' Return the character's profession as a string (e.g. "Adventurer").
Public Property Get szProfession() As String
    szProfession = szProfessionMbr
End Property

' Return the name of this account.
Public Property Get szAccount() As String
    szAccount = szAccountMbr
End Property


' Return the character's Character Options settings.
Public Property Get chop() As Long
    chop = chopMbr
End Property

Public Property Get chop2() As Long
    chop2 = chop2Mbr
End Property


' Return the character's current level.
Public Property Get lvl() As Long
    lvl = lvlMbr
End Property

' Return the character's allegiance rank.
Public Property Get rank() As Long
    rank = rankMbr
End Property

' Return the total amount of experience (XP) earned by this character.
Public Property Get expTotal() As Double
    If lvlMbr < 125 Then
        expTotal = expTotalMbr
    Else
    '   expTotalMbr is wrong for very high-level characters due to DWORD
    '   overflow in the message protocol.  Calculate it the hard way.
        expTotal = expSpent + expUnspent
    End If
End Property

' Return the total amount of XP spent on improving this character.
Public Property Get expSpent() As Double
    
    expSpent = 0
    
    Dim attr As Long
    For attr = 1 To attrMax - 1
        expSpent = expSpent + mpattrskinfo(attr).expInvested
    Next attr
    
    Dim vital As Long
    For vital = 1 To vitalMax - 1
        If Not (mpvitalskinfo(vital) Is Nothing) Then
            expSpent = expSpent + mpvitalskinfo(vital).expInvested
        End If
    Next vital
    
    Dim skid As Long
    For skid = 1 To skidMax - 1
        If Not (mpskidskinfo(skid) Is Nothing) Then
            expSpent = expSpent + mpskidskinfo(skid).expInvested
        End If
    Next skid
    
End Property

' Return the amount of unspent XP available to this character.
Public Property Get expUnspent() As Double
    expUnspent = expUnspentMbr
End Property

' Return the number of unspent skill points available to this character.
Public Property Get skpUnspent() As Long
    skpUnspent = skpUnspentMbr
End Property


' Return the current burden in BU.
Public Property Get burdenCur() As Long
    burdenCur = burdenMbr
End Property

' Return the amount of cash being carried in the form of pyreals.
Public Property Get cpyCash() As Long
    cpyCash = cpyCashMbr
End Property


' Return the character's current health.
Public Property Get healthCur() As Long
    healthCur = healthMbr
End Property

' Return the character's current stamina.
Public Property Get staminaCur() As Long
    staminaCur = staminaMbr
End Property

' Return the character's current mana.
Public Property Get manaCur() As Long
    manaCur = manaMbr
End Property


Public Property Get tBirth() As Date
    tBirth = tBirthMbr
End Property

Public Property Get csecAge() As Long
    csecAge = csecAgeMbr
End Property

Public Property Get cdeath() As Long
    cdeath = cdeathMbr
End Property


'---------
' Methods
'---------

Public Function SkinfoFromAttr(ByVal attr As Long) As SkinfoCls
    Set SkinfoFromAttr = mpattrskinfo(attr)
End Function

Public Function SkinfoFromVital(ByVal vital As Long) As SkinfoCls
    Set SkinfoFromVital = mpvitalskinfo(vital)
End Function

Public Function SkinfoFromSkid(ByVal skid As Long) As SkinfoCls
    Set SkinfoFromSkid = mpskidskinfo(skid)
End Function


Public Function AlvlUnbuffedFromAttr(ByVal attr As Long) As Long
    If attr = attrNil Then
        AlvlUnbuffedFromAttr = 0
    Else
        AlvlUnbuffedFromAttr = mpattrskinfo(attr).lvlUnbuffed
    End If
End Function

Public Function AlvlCurFromAttr(ByVal attr As Long) As Long
    If attr = attrNil Then
        AlvlCurFromAttr = 0
    Else
        AlvlCurFromAttr = mpattrskinfo(attr).lvlCur
    End If
End Function

Public Function VlvlUnbuffedFromVital(ByVal vital As Long) As Long
    VlvlUnbuffedFromVital = mpvitalskinfo(vital).lvlUnbuffed
End Function

Public Function VlvlDefaultFromVital(ByVal vital As Long) As Long
    VlvlDefaultFromVital = mpvitalskinfo(vital).lvlDefault
End Function

Public Function SklvlUnbuffedFromSkid(ByVal skid As Long) As Long
    SklvlUnbuffedFromSkid = mpskidskinfo(skid).lvlUnbuffed
End Function

Public Function SklvlDefaultFromSkid(ByVal skid As Long) As Long
    SklvlDefaultFromSkid = mpskidskinfo(skid).lvlDefault
End Function


' Return the spell ID at the specified location in the player's spell
' shortcut slots.
Public Function rgspellid(ByVal itab As Long, ByVal ispell As Long) As Long
    
    Dim cospellid As Collection
    If itab < 0 Then
        Set cospellid = cospellidSpellbook
    ElseIf itab <= UBound(rgcospellidTabs) Then
        Set cospellid = rgcospellidTabs(itab)
    Else
        rgspellid = spellidNil
        Exit Function
    End If
    
    If ispell < 0 Then
        rgspellid = cospellid.count
    ElseIf ispell < cospellid.count Then
        rgspellid = cospellid(1 + ispell)
    Else
        rgspellid = spellidNil
    End If
    
End Function

Public Function FSpellidInSpellbook(ByVal spellid As Long) As Boolean
    
    FSpellidInSpellbook = False
    
    Dim spellidT As Variant
    For Each spellidT In cospellidSpellbook
        If spellidT = spellid Then
            FSpellidInSpellbook = True
            Exit For
        End If
    Next spellidT
    
End Function


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------

Friend Sub InitSkinfoSkid(ByVal skid As Long, _
  ByVal szName As String, ByVal szDesc As String, _
  ByVal attr1 As Long, ByVal attr2 As Long, ByVal divisor As Long)
    
    Set mpskidskinfo(skid) = SkinfoNew(szName, szDesc, attr1, attr2, divisor)
    
End Sub

Friend Sub PrepForLogin()
    
    burdenMbr = 0
    cpyCashMbr = 0
    expTotalMbr = 0
    expUnspentMbr = 0
    skpUnspentMbr = 0
    lvlMbr = 0
    rankMbr = 0
    
    tBirthMbr = 0
    csecAgeMbr = 0
    cdeathMbr = 0
    
    szNameMbr = ""
    szGenderMbr = ""
    szRaceMbr = ""
    szProfessionMbr = ""
    
    healthMbr = 0
    staminaMbr = 0
    manaMbr = 0
    
    fractVitae = 1
    
    Dim attr As Long
    For attr = 1 To attrMax - 1
        Call mpattrskinfo(attr).SetDlvlSpells(0)
    Next attr
    
    Dim vital As Long
    For vital = 1 To vitalMax - 1
        If Not (mpvitalskinfo(vital) Is Nothing) Then
            Call mpvitalskinfo(vital).SetDlvlSpells(0)
        End If
    Next vital
    
    Dim skid As Long
    For skid = 1 To skidMax - 1
        If Not (mpskidskinfo(skid) Is Nothing) Then
            Call mpskidskinfo(skid).SetDlvlSpells(0)
        End If
    Next skid
    
    Set cospellidSpellbook = New Collection
    Call ResetSpelltabs
    
End Sub

Friend Sub ResetSpelltabs()
    
    Dim itab As Long
    For itab = 0 To UBound(rgcospellidTabs)
        Set rgcospellidTabs(itab) = New Collection
    Next itab
    
End Sub


Friend Sub SetDwordProp(ByVal lkey As Long, ByVal dwVal As Long, _
  Optional ByVal fFireEvent As Boolean = False)
    Call TraceEnter("Self.SetDwordProp", _
      SzHex(lkey, 2) + ", " + CStr(dwVal) + ", " + CStr(fFireEvent))
    
    Select Case lkey
    Case lkeyBurden
        burdenMbr = dwVal
        Call otable.acoChar.SetBurden(dwVal)
        If fFireEvent Then Call skev.RaiseOnStatTotalBurden(dwVal)
    Case lkeyTotalPyreals
        cpyCashMbr = dwVal
        If fFireEvent Then Call skev.RaiseOnStatTotalPyreals(dwVal)
    Case lkeySkillCreditsAvailable
        skpUnspentMbr = dwVal
    Case lkeyCreatureLevel
        lvlMbr = dwVal
        If fFireEvent Then Call skev.RaiseOnStatLevel(dwVal)
    Case lkeyRank
        rankMbr = dwVal
    Case lkeyDeaths
        cdeathMbr = dwVal
    Case lkeyAge
        csecAgeMbr = dwVal
        
    Case lkeyDateOfBirth
        Dim tzi As TIME_ZONE_INFORMATION
        Call GetTimeZoneInformation(tzi)
        tBirthMbr = #1/1/1970# - tzi.Bias / 1440 + dwVal / 86400
        Call TraceLine("Birthday = " + CStr(tBirthMbr))
        
    Case lkeyGender
'        Call TraceLine("Gender = " + CStr(dwVal))
       Call SetSzGender(SzFromGender(dwVal))
        
    Case lkeyRaceReq
'        Call TraceLine("Race = " + CStr(dwVal))
       Call SetSzRace(SzFromRace(dwVal))
    End Select
    
    Call TraceExit("Self.SetDwordProp")
End Sub

Friend Sub SetQwordProp(ByVal qkey As Long, ByVal qVal As Double, _
  Optional ByVal fFireEvent As Boolean = False)
    Call TraceEnter("Self.SetQwordProp", _
      SzHex(qkey, 2) + ", " + CStr(qVal) + ", " + CStr(fFireEvent))
    
    Select Case qkey
    Case qkeyExpTotal
        expTotalMbr = qVal
        If fFireEvent Then Call skev.RaiseOnStatTotalExp(qVal)
    Case qkeyExpUnassigned
        expUnspentMbr = qVal
        If fFireEvent Then Call skev.RaiseOnStatUnspentExp(qVal)
    End Select
    
    Call TraceExit("Self.SetQwordProp")
End Sub

Friend Sub SetBoolProp(ByVal bkey As Long, ByVal fVal As Boolean)
    Call TraceEnter("Self.SetBoolProp", SzHex(bkey, 2) + ", " + CStr(fVal))
    
'   No interesting Boolean properties (yet).
    Select Case bkey
    End Select
    
    Call TraceExit("Self.SetBoolProp")
End Sub

Friend Sub SetDblProp(ByVal dkey As Long, ByVal dblVal As Double)
    Call TraceEnter("Self.SetDblProp", SzHex(dkey, 2) + ", " + CStr(dblVal))
    
'   No interesting Double properties (yet).
    Select Case dkey
    End Select
    
    Call TraceExit("Self.SetDblProp")
End Sub

Friend Sub SetSzProp(ByVal skey As Long, ByVal szVal As String)
    Call TraceEnter("Self.SetSzProp", SzHex(skey, 2) + ", " + SzQuote(szVal))
    
    Select Case skey
    Case skeyName
        Call SetSzName(szVal)
    '   Login comes before the character's ObjectCreate method, so acoChar
    '   doesn't actually know its name yet.  Set it now in case user wants
    '   to refer to it from the OnLogon event handler.
        Call otable.acoChar.SetSzName(szVal)
    Case skeyTitle
        Call SetSzProfession(szVal)
    End Select
    
    Call TraceExit("Self.SetSzProp")
End Sub


Friend Sub SetBurden(ByVal burdenNew As Long)
    burdenMbr = burdenNew
    Call otable.acoChar.SetBurden(burdenNew)
End Sub

Friend Sub SetCpyCash(ByVal cpyNew As Long)
    cpyCashMbr = cpyNew
End Sub

Friend Sub SetExpTotal(ByVal expTotalNew As Double)
    expTotalMbr = expTotalNew
End Sub

Friend Sub SetExpUnspent(ByVal expUnspentNew As Double)
    expUnspentMbr = expUnspentNew
End Sub

Friend Sub SetSkpUnspent(ByVal skpUnspentNew As Long)
    skpUnspentMbr = skpUnspentNew
End Sub

Friend Sub SetLvl(ByVal lvlNew As Long)
    lvlMbr = lvlNew
End Sub

Friend Sub SetRank(ByVal rankNew As Long)
    rankMbr = rankNew
End Sub

Friend Sub SetSzName(ByVal szNameNew As String)
    szNameMbr = szNameNew
End Sub

Friend Sub SetSzGender(ByVal szGenderNew As String)
    szGenderMbr = szGenderNew
End Sub

Friend Sub SetSzRace(ByVal szRaceNew As String)
    szRaceMbr = szRaceNew
End Sub

Friend Sub SetSzProfession(ByVal szProfessionNew As String)
    szProfessionMbr = szProfessionNew
End Sub

Friend Sub SetSzAccount(ByVal szAccountNew As String)
    szAccountMbr = szAccountNew
End Sub

Friend Sub SetHealth(ByVal healthNew As Long)
    healthMbr = healthNew
End Sub

Friend Sub SetStamina(ByVal staminaNew As Long)
    staminaMbr = staminaNew
End Sub

Friend Sub SetMana(ByVal manaNew As Long)
    manaMbr = manaNew
End Sub

Friend Sub SetVital(ByVal vital As Long, ByVal vlvl As Long)
        
    Select Case vital
    Case vitalHealthCur
        healthMbr = vlvl
    Case vitalStaminaCur
        staminaMbr = vlvl
    Case vitalManaCur
        manaMbr = vlvl
    End Select
        
End Sub


Friend Sub SetChop(ByVal chopNew As Long)
    chopMbr = chopNew
End Sub

Friend Sub SetChop2(ByVal chop2New As Long)
    chop2Mbr = chop2New
End Sub

Friend Sub SetChopIflag(ByVal iflag As Long, ByVal fVal As Boolean)
    
    Dim chopMask As Long
    Select Case iflag
    Case &H0: chopMask = chopRepeatAttacks
    Case &H1: chopMask = chopIgnoreAllegiance
    Case &H2: chopMask = chopIgnoreFellowship
    Case &HF: chopMask = chopShareXP
    Case &H10: chopMask = chopAcceptCorpsePerm
    Case &H11: chopMask = chopShareLoot
    Case &H12: chopMask = chopAutoFellow
    Case &H19: chopMask = chopUseChargeAttack
    Case &H1B: chopMask = chopAllegianceChat
    End Select
    
    If fVal Then
        chopMbr = chopMbr Or chopMask
    Else
        chopMbr = chopMbr And Not chopMask
    End If
    
End Sub


Friend Sub SetAtinfo(ByVal attr As Long, ByVal avd As AVDataCls)
    
    Call mpattrskinfo(attr).SetAtinfo(avd)
    
End Sub

Friend Sub SetVtinfo(ByVal vital As Long, ByVal avd As AVDataCls)
    
    Call mpvitalskinfo(vital).SetVtinfo(avd)
    
End Sub

Friend Sub SetSkinfo(ByVal skid As Long, ByVal sd As SkillDataCls)
    
    If sd.skts = sktsUntrained And FUnusableSkid(skid) Then
        sd.skts = sktsUnusable
    End If
    
    Call mpskidskinfo(skid).SetSkinfo(sd)
    
End Sub

Friend Sub SetSkts(ByVal skid As Long, ByVal skts As Long)
    
    If skts = sktsUntrained And FUnusableSkid(skid) Then
        skts = sktsUnusable
    End If
    
    Call mpskidskinfo(skid).SetSkts(skts)
    
End Sub


Friend Sub AddSpellid(ByVal itab As Long, ByVal spellid As Long)
    
    Dim cospellid As Collection
    If itab < 0 Then
        Set cospellid = cospellidSpellbook
    ElseIf itab <= UBound(rgcospellidTabs) Then
        Set cospellid = rgcospellidTabs(itab)
    Else
        Exit Sub
    End If
    
    Call cospellid.Add(spellid)
    
End Sub

Friend Sub RemoveSpellid(ByVal itab As Long, ByVal spellid As Long)

    Dim cospellid As Collection
    If itab < 0 Then
        Set cospellid = cospellidSpellbook
    ElseIf itab <= UBound(rgcospellidTabs) Then
        Set cospellid = rgcospellidTabs(itab)
    Else
        Exit Sub
    End If
    
    Dim ispell As Long
    For ispell = 0 To cospellid.count - 1
        If cospellid(1 + ispell) = spellid Then
            Call cospellid.Remove(1 + ispell)
            Exit For
        End If
    Next ispell
    
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
'   Set up standard skill formulae.
    Call InitSkinfos
    
    Call PrepForLogin
    
End Sub

Private Sub InitSkinfos()
    
'   Attributes:
    Set mpattrskinfo(attrStrength) = SkinfoNew("Strength", _
      "Measures your character's muscular power.")
    Set mpattrskinfo(attrEndurance) = SkinfoNew("Endurance", _
      "Measures how healthy your character is.")
    Set mpattrskinfo(attrQuickness) = SkinfoNew("Quickness", _
      "Measures how fast your character is.")
    Set mpattrskinfo(attrCoordination) = SkinfoNew("Coordination", _
      "Measures your character's reflexes.")
    Set mpattrskinfo(attrFocus) = SkinfoNew("Focus", _
      "Measures your character's mind and senses.")
    Set mpattrskinfo(attrSelf) = SkinfoNew("Self", _
      "Measures your character's willpower.")
    
'   Vitals:
    Set mpvitalskinfo(vitalHealthMax) = SkinfoNew("Health", _
      "If you run out of health, you will die!", _
      attrEndurance, attrNil, 2)
    Set mpvitalskinfo(vitalStaminaMax) = SkinfoNew("Stamina", _
      "Affects your actions and movement.", _
      attrEndurance, attrNil, 1)
    Set mpvitalskinfo(vitalManaMax) = SkinfoNew("Mana", _
      "Affects how much magic you can cast.", _
      attrSelf, attrNil, 1)
    
End Sub

Private Function SkinfoNew(ByVal szName As String, ByVal szDesc As String, _
  Optional ByVal attr1 As Long = attrNil, _
  Optional ByVal attr2 As Long = attrNil, _
  Optional ByVal divisor As Long = 1) As SkinfoCls
    
    Dim skinfo As SkinfoCls: Set skinfo = New SkinfoCls
    Call skinfo.Init(szName, szDesc, attr1, attr2, divisor)
    Set SkinfoNew = skinfo
    
End Function

Private Function FUnusableSkid(ByVal skid As Long) As Boolean
    
    Select Case skid
'   These skills can't be used unless trained.  Apparently this list is
'   just hard-coded in the client.
    Case skidAlchemy, skidCooking, skidFletching, skidHealing, skidLockpick, _
      skidManaConversion, skidCreatureEnchantment, skidItemEnchantment, _
      skidLifeMagic, skidWarMagic
        FUnusableSkid = True
    Case Else
        FUnusableSkid = False
    End Select
    
End Function






















