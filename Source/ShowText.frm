VERSION 5.00
Begin VB.Form formShowText 
   Caption         =   "Show Text"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "ShowText.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox textShow 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3015
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Width           =   4455
   End
End
Attribute VB_Name = "formShowText"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


'--------------
' Private data
'--------------

Private szWindow As String


'---------
' Methods
'---------

Public Sub ShowText(ByVal szText As String, ByVal szCaption As String, _
  Optional modality As FormShowConstants = vbModal, _
  Optional formOwner As Form = Nothing)
    
    szWindow = szCaption    ' Do this before Load method gets called.
    
    textShow.Text = szText
    Caption = szCaption
    If Not (formOwner Is Nothing) Then Icon = formOwner.Icon
    Call Show(modality, formOwner)
    
End Sub


'----------
' Controls
'----------

Private Sub Form_Load()
    
    If szWindow = "" Then szWindow = Caption
    
'   Hack: Default window to reasonable size.
    If GetSetting(swoptions.szAppName, "Windows", szWindow) = "" Then
        Call SaveSetting(swoptions.szAppName, "Windows", szWindow, _
          CStr(Screen.Width * 1 / 6) + "," + CStr(Screen.Height * 1 / 6) + "," + _
          CStr(Screen.Width * 2 / 3) + "," + CStr(Screen.Height * 2 / 3))
    End If
    
    Call RestoreFormPosition(Me, szWindow, True, swoptions.szAppName)
    
End Sub

Private Sub Form_Resize()
    
    If ScaleWidth <> 0 Then
        Dim dxMargin As Long, dyMargin As Long
        dxMargin = textShow.Left
        dyMargin = textShow.Top
        Call textShow.Move(dxMargin, dyMargin, _
          ScaleWidth - dxMargin * 2, ScaleHeight - dyMargin * 2)
        
        Call SaveFormPosition(Me, szWindow, swoptions.szAppName)
    End If
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    
    Call SaveFormPosition(Me, szWindow, swoptions.szAppName)
    
End Sub










