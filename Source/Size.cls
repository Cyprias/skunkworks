VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SizeCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' Script-compatible wrapper for tagSIZE.


'--------------
' Private data
'--------------

Private sizeMbr As DecalPlugins.tagSIZE


'-------------------
' Public properties
'-------------------

Public Property Get cx() As Long
    cx = sizeMbr.cx
End Property

Public Property Let cx(ByVal l As Long)
    sizeMbr.cx = l
End Property


Public Property Get cy() As Long
    cy = sizeMbr.cy
End Property

Public Property Let cy(ByVal l As Long)
    sizeMbr.cy = l
End Property


'------------------
' Friend functions
'------------------

Friend Property Get size() As DecalPlugins.tagSIZE
    size = sizeMbr
End Property

Friend Property Let rect(ByRef sizeNew As DecalPlugins.tagSIZE)
    sizeMbr = sizeNew
End Property



