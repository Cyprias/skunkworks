Attribute VB_Name = "SkapiDefs"
Option Explicit

' -------------------------------------------------------------------
' !!!!!!!!!!!!!!!!!!!!!!   R E A D   T H I S   !!!!!!!!!!!!!!!!!!!!!!
' -------------------------------------------------------------------
' !!!  This file is generated automatically by SkapiDefsVbs.xsl.  !!!
' !!!    Any edits you make to it by hand WILL be overwritten.    !!!
' !!!         Make your changes to SkapiDefs.xml instead.         !!!
' -------------------------------------------------------------------

' arc: Action result codes
Public Const arcSuccess                  =                           0    ' Your spellcasting attempt (or other action) succeeded.
Public Const arcCombatMode               =                           2    ' One of the parties entered combat mode.
Public Const arcChargedTooFar            =                          28    ' You charged too far!
Public Const arcTooBusy                  =                          29    ' You're too busy!
Public Const arcCantMoveToObject         =                          57    ' Unable to move to object!
Public Const arcCanceled                 =                          81    ' One of the parties canceled or moved out of range.
Public Const arcFatigued                 =                        1015    ' You are too fatigued to attack!
Public Const arcOutOfAmmo                =                        1016    ' You are out of ammunition!
Public Const arcMissleMisfire            =                        1017    ' Your missile attack misfired!
Public Const arcImpossibleSpell          =                        1018    ' You've attempted an impossible spell path!
Public Const arcDontKnowSpell            =                        1022    ' You don't know that spell!
Public Const arcIncorrectTarget          =                        1023    ' Incorrect target type.
Public Const arcOutOfComps               =                        1024    ' You don't have all the components for this spell.
Public Const arcNotEnoughMana            =                        1025    ' You don't have enough Mana to cast this spell.
Public Const arcFizzle                   =                        1026    ' Your spell fizzled.
Public Const arcNoTarget                 =                        1027    ' Your spell's target is missing!
Public Const arcSpellMisfire             =                        1028    ' Your projectile spell mislaunched!
Public Const arcSummonFailure            =                        1188    ' You fail to summon the portal!
Public Const arcNotAvailable             =                        1323    ' That person is not available.

' attr: Attribute IDs
Public Const attrNil                     =                           0    ' No attribute.
Public Const attrStrength                =                           1
Public Const attrEndurance               =                           2
Public Const attrQuickness               =                           3
Public Const attrCoordination            =                           4
Public Const attrFocus                   =                           5
Public Const attrSelf                    =                           6
Public Const attrMax                     =                           7

' bpart: Body part IDs
Public Const bpartHead                   =                           0
Public Const bpartChest                  =                           1
Public Const bpartAbdomen                =                           2
Public Const bpartUpperArm               =                           3
Public Const bpartLowerArm               =                           4
Public Const bpartHand                   =                           5
Public Const bpartUpperLeg               =                           6
Public Const bpartLowerLeg               =                           7
Public Const bpartFoot                   =                           8

' chop: Character option masks
Public Const chopRepeatAttacks           =                 &H00000002&    ' Automatically Repeat Attacks
Public Const chopIgnoreAllegiance        =                 &H00000004&    ' Ignore Allegiance Requests
Public Const chopIgnoreFellowship        =                 &H00000008&    ' Ignore Fellowship Requests
Public Const chopAcceptGifts             =                 &H00000040&    ' Let Other Players Give You Items
Public Const chopKeepTargetInView        =                 &H00000080&    ' Keep Combat Targets in View
Public Const chopTooltips                =                 &H00000100&    ' Display 3D Tooltips
Public Const chopDeceive                 =                 &H00000200&    ' Attempt to Deceive Other Players
Public Const chopRunAsDefault            =                 &H00000400&    ' Run as Default Movement
Public Const chopStayInChat              =                 &H00000800&    ' Stay in Chat Mode After Sending a Message
Public Const chopAdvancedCombat          =                 &H00001000&    ' Advanced Combat Interface
Public Const chopAutoTarget              =                 &H00002000&    ' Auto Target
Public Const chopVividTargeting          =                 &H00008000&    ' Vivid Targeting Indicator
Public Const chopIgnoreTrade             =                 &H00020000&    ' Ignore All Trade Requests
Public Const chopShareXP                 =                 &H00040000&    ' Share Fellowship Experience
Public Const chopAcceptCorpsePerm        =                 &H00080000&    ' Accept Corpse-Looting Permissions
Public Const chopShareLoot               =                 &H00100000&    ' Share Fellowship Loot
Public Const chopStretchUI               =                 &H00200000&    ' Stretch UI
Public Const chopShowCoords              =                 &H00400000&    ' Show Coordinates By the Radar
Public Const chopSpellDurations          =                 &H00800000&    ' Display Spell Durations
Public Const chopDisableHouseFX          =                 &H02000000&    ' Disable House Restriction Effects
Public Const chopDragStartsTrade         =                 &H04000000&    ' Drag Item to Player Opens Trade
Public Const chopShowAllegianceLogons    =                 &H08000000&    ' Show Allegiance Logons
Public Const chopUseChargeAttack         =                 &H10000000&    ' Use Charge Attack
Public Const chopAutoFellow              =                 &H20000000&    ' Automatically Accept Fellowship Requests
Public Const chopAllegianceChat          =                 &H40000000&    ' Listen to Allegiance Chat
Public Const chopCraftingDialog          =                 &H80000000&    ' Use Crafting Chance of Success Dialog
Public Const chopAcceptAllegiance        =                 &H00000004&    ' (Old name for compatibility)
Public Const chopAcceptFellowship        =                 &H00000008&    ' (Old name for compatibility)

' chop2: Character option masks (extended)
Public Const chop2AlwaysDaylight         =                 &H00000001&    ' Always Daylight Outdoors
Public Const chop2ShowDateOfBirth        =                 &H00000002&    ' Allow Others to See Your Date of Birth
Public Const chop2ShowChessRank          =                 &H00000004&    ' Allow Others to See Your Chess Rank
Public Const chop2ShowFishingSkill       =                 &H00000008&    ' Allow Others to See Your Fishing Skill
Public Const chop2ShowDeaths             =                 &H00000010&    ' Allow Others to See Your Number of Deaths
Public Const chop2ShowAge                =                 &H00000020&    ' Allow Others to See Your Age
Public Const chop2Timestamps             =                 &H00000040&    ' Display Timestamps
Public Const chop2SalvageMultiple        =                 &H00000080&    ' Salvage Multiple Materials at Once

' cmc: Chat message colors
Public Const cmcGreen                    =                           0
Public Const cmcWhite                    =                           2    ' Local broadcast chat.
Public Const cmcYellow                   =                           3
Public Const cmcDarkYellow               =                           4
Public Const cmcMagenta                  =                           5
Public Const cmcRed                      =                           6
Public Const cmcLightBlue                =                           7
Public Const cmcRose                     =                           8
Public Const cmcPaleRose                 =                           9
Public Const cmcDimWhite                 =                          12
Public Const cmcCyan                     =                          13
Public Const cmcPaleBlue                 =                          14    ' SkunkWorks default color.
Public Const cmcBlue                     =                          17    ' Spellcasting.
Public Const cmcCoral                    =                          22

' chrm: Chat recipient masks
Public Const chrmLocal                   =                 &H00000001&    ' Local chat broadcast
Public Const chrmEmote                   =                 &H00000002&    ' Local emote broadcast (@e)
Public Const chrmFellowship              =                 &H00000800&    ' Fellowship broadcast (@f)
Public Const chrmVassals                 =                 &H00001000&    ' Patron to vassal (@v)
Public Const chrmPatron                  =                 &H00002000&    ' Vassal to patron (@p)
Public Const chrmMonarch                 =                 &H00004000&    ' Follower to monarch (@m)
Public Const chrmCovassals               =                 &H01000000&    ' Covassal broadcast (@c)
Public Const chrmAllegiance              =                 &H02000000&    ' Allegiance broadcast by monarch or speaker (@a)

' cmid: Command IDs
' cmid definitions appear here in the same order they appear on AC's Keyboard Configuration screen.  Where the name of a cmid differs from the descriptive text on that screen, the text appears in the Description column below.
' cmid values may be concatenated using the + operator to specify a sequence of commands.
Public Const cmidMovementJump            =            "{MovementJump}"
Public Const cmidMovementForward         =         "{MovementForward}"
Public Const cmidMovementBackup          =          "{MovementBackup}"
Public Const cmidMovementTurnLeft        =        "{MovementTurnLeft}"
Public Const cmidMovementTurnRight       =       "{MovementTurnRight}"
Public Const cmidMovementWalkMode        =        "{MovementWalkMode}"
Public Const cmidMovementStop            =            "{MovementStop}"
Public Const cmidMovementStrafeRight     =     "{MovementStrafeRight}"
Public Const cmidMovementStrafeLeft      =      "{MovementStrafeLeft}"
Public Const cmidMovementRunLock         =         "{MovementRunLock}"
Public Const cmidReady                   =                   "{Ready}"
Public Const cmidCrouch                  =                  "{Crouch}"
Public Const cmidSitting                 =                 "{Sitting}"
Public Const cmidSleeping                =                "{Sleeping}"
Public Const cmidSelectionSelf           =           "{SelectionSelf}"
Public Const cmidSelectionPreviousCompassItem="{SelectionPreviousCompassItem}"
Public Const cmidSelectionNextCompassItem="{SelectionNextCompassItem}"
Public Const cmidSelectionUseNextUnopenedCorpse="{SelectionUseNextUnopenedCorpse}"
Public Const cmidSelectionGive           =           "{SelectionGive}"
Public Const cmidSelectionDrop           =           "{SelectionDrop}"
Public Const cmidSelectionPickUp         =         "{SelectionPickUp}"
Public Const cmidSelectionSplitStack     =     "{SelectionSplitStack}"
Public Const cmidSelectionUseClosestUnopenedCorpse="{SelectionUseClosestUnopenedCorpse}"
Public Const cmidSelectionClosestCompassItem="{SelectionClosestCompassItem}"
Public Const cmidSelectionLastAttacker   =   "{SelectionLastAttacker}"
Public Const cmidSelectionClosestMonster = "{SelectionClosestMonster}"
Public Const cmidSelectionPreviousItem   =   "{SelectionPreviousItem}"
Public Const cmidSelectionNextItem       =       "{SelectionNextItem}"
Public Const cmidSelectionPreviousSelection="{SelectionPreviousSelection}"
Public Const cmidSelectionClosestItem    =    "{SelectionClosestItem}"
Public Const cmidSelectionPreviousMonster="{SelectionPreviousMonster}"
Public Const cmidSelectionNextMonster    =    "{SelectionNextMonster}"
Public Const cmidSelectionClosestPlayer  =  "{SelectionClosestPlayer}"
Public Const cmidSelectionPreviousPlayer = "{SelectionPreviousPlayer}"
Public Const cmidSelectionNextPlayer     =     "{SelectionNextPlayer}"
Public Const cmidSelectionPreviousFellow = "{SelectionPreviousFellow}"
Public Const cmidSelectionNextFellow     =     "{SelectionNextFellow}"
Public Const cmidSelectionExamine        =        "{SelectionExamine}"
Public Const cmidUSE                     =                     "{USE}"
Public Const cmidToggleAbusePanel        =        "{ToggleAbusePanel}"
Public Const cmidToggleCharacterInfoPanel="{ToggleCharacterInfoPanel}"
Public Const cmidTogglePositiveEffectsPanel="{TogglePositiveEffectsPanel}"
Public Const cmidToggleNegativeEffectsPanel="{ToggleNegativeEffectsPanel}"
Public Const cmidToggleLinkStatusPanel   =   "{ToggleLinkStatusPanel}"
Public Const cmidToggleUrgentAssistancePanel="{ToggleUrgentAssistancePanel}"
Public Const cmidToggleVitaePanel        =        "{ToggleVitaePanel}"
Public Const cmidToggleSocialPanel       =       "{ToggleSocialPanel}"
Public Const cmidToggleSpellManagementPanel="{ToggleSpellManagementPanel}"
Public Const cmidToggleSkillManagementPanel="{ToggleSkillManagementPanel}"
Public Const cmidToggleMapPanel          =          "{ToggleMapPanel}"
Public Const cmidToggleHousePanel        =        "{ToggleHousePanel}"
Public Const cmidToggleGameplayOptionsPanel="{ToggleGameplayOptionsPanel}"
Public Const cmidToggleCharacterOptionsPanel="{ToggleCharacterOptionsPanel}"
Public Const cmidToggleConfigOptionsPanel="{ToggleConfigOptionsPanel}"
Public Const cmidToggleRadarPanel        =        "{ToggleRadarPanel}"
Public Const cmidToggleKeyboardPanel     =     "{ToggleKeyboardPanel}"
Public Const cmidCaptureScreenshot       =       "{CaptureScreenshot}"
Public Const cmidToggleHelp              =              "{ToggleHelp}"
Public Const cmidTogglePluginManager     =     "{TogglePluginManager}"
Public Const cmidToggleAllegiancePanel   =   "{ToggleAllegiancePanel}"
Public Const cmidToggleFellowshipPanel   =   "{ToggleFellowshipPanel}"
Public Const cmidToggleSpellbookPanel    =    "{ToggleSpellbookPanel}"
Public Const cmidToggleSpellComponentsPanel="{ToggleSpellComponentsPanel}"
Public Const cmidToggleAttributesPanel   =   "{ToggleAttributesPanel}"
Public Const cmidToggleSkillsPanel       =       "{ToggleSkillsPanel}"
Public Const cmidToggleWorldPanel        =        "{ToggleWorldPanel}"
Public Const cmidToggleOptionsPanel      =      "{ToggleOptionsPanel}"
Public Const cmidToggleInventoryPanel    =    "{ToggleInventoryPanel}"
Public Const cmidLOGOUT                  =                  "{LOGOUT}"
Public Const cmidSelectQuickSlot_6       =       "{SelectQuickSlot_6}"
Public Const cmidSelectQuickSlot_7       =       "{SelectQuickSlot_7}"
Public Const cmidSelectQuickSlot_8       =       "{SelectQuickSlot_8}"
Public Const cmidSelectQuickSlot_9       =       "{SelectQuickSlot_9}"
Public Const cmidSelectQuickSlot_1       =       "{SelectQuickSlot_1}"
Public Const cmidSelectQuickSlot_2       =       "{SelectQuickSlot_2}"
Public Const cmidSelectQuickSlot_3       =       "{SelectQuickSlot_3}"
Public Const cmidSelectQuickSlot_4       =       "{SelectQuickSlot_4}"
Public Const cmidSelectQuickSlot_5       =       "{SelectQuickSlot_5}"
Public Const cmidCreateShortcut          =          "{CreateShortcut}"
Public Const cmidUseQuickSlot_1          =          "{UseQuickSlot_1}"
Public Const cmidUseQuickSlot_2          =          "{UseQuickSlot_2}"
Public Const cmidUseQuickSlot_3          =          "{UseQuickSlot_3}"
Public Const cmidUseQuickSlot_4          =          "{UseQuickSlot_4}"
Public Const cmidUseQuickSlot_5          =          "{UseQuickSlot_5}"
Public Const cmidUseQuickSlot_6          =          "{UseQuickSlot_6}"
Public Const cmidUseQuickSlot_7          =          "{UseQuickSlot_7}"
Public Const cmidUseQuickSlot_8          =          "{UseQuickSlot_8}"
Public Const cmidUseQuickSlot_9          =          "{UseQuickSlot_9}"
Public Const cmidToggleChatEntry         =         "{ToggleChatEntry}"
Public Const cmidSTART_COMMAND           =           "{START_COMMAND}"
Public Const cmidMonarchReply            =            "{MonarchReply}"
Public Const cmidPatronReply             =             "{PatronReply}"
Public Const cmidReply                   =                   "{Reply}"
Public Const cmidEnterChatMode           =           "{EnterChatMode}"
Public Const cmidCombatToggleCombat      =      "{CombatToggleCombat}"
Public Const cmidCombatDecreaseAttackPower="{CombatDecreaseAttackPower}"
Public Const cmidCombatIncreaseAttackPower="{CombatIncreaseAttackPower}"
Public Const cmidCombatLowAttack         =         "{CombatLowAttack}"
Public Const cmidCombatMediumAttack      =      "{CombatMediumAttack}"
Public Const cmidCombatHighAttack        =        "{CombatHighAttack}"
Public Const cmidCombatDecreaseMissileAccuracy="{CombatDecreaseMissileAccuracy}"
Public Const cmidCombatIncreaseMissileAccuracy="{CombatIncreaseMissileAccuracy}"
Public Const cmidCombatAimLow            =            "{CombatAimLow}"
Public Const cmidCombatAimMedium         =         "{CombatAimMedium}"
Public Const cmidCombatAimHigh           =           "{CombatAimHigh}"
Public Const cmidUseSpellSlot_10         =         "{UseSpellSlot_10}"
Public Const cmidUseSpellSlot_1          =          "{UseSpellSlot_1}"
Public Const cmidUseSpellSlot_2          =          "{UseSpellSlot_2}"
Public Const cmidUseSpellSlot_3          =          "{UseSpellSlot_3}"
Public Const cmidUseSpellSlot_4          =          "{UseSpellSlot_4}"
Public Const cmidUseSpellSlot_11         =         "{UseSpellSlot_11}"
Public Const cmidUseSpellSlot_12         =         "{UseSpellSlot_12}"
Public Const cmidCombatPrevSpellTab      =      "{CombatPrevSpellTab}"
Public Const cmidCombatNextSpellTab      =      "{CombatNextSpellTab}"
Public Const cmidCombatPrevSpell         =         "{CombatPrevSpell}"
Public Const cmidCombatCastCurrentSpell  =  "{CombatCastCurrentSpell}"
Public Const cmidCombatNextSpell         =         "{CombatNextSpell}"
Public Const cmidCombatFirstSpellTab     =     "{CombatFirstSpellTab}"
Public Const cmidCombatLastSpellTab      =      "{CombatLastSpellTab}"
Public Const cmidCombatFirstSpell        =        "{CombatFirstSpell}"
Public Const cmidCombatLastSpell         =         "{CombatLastSpell}"
Public Const cmidUseSpellSlot_5          =          "{UseSpellSlot_5}"
Public Const cmidUseSpellSlot_6          =          "{UseSpellSlot_6}"
Public Const cmidUseSpellSlot_7          =          "{UseSpellSlot_7}"
Public Const cmidUseSpellSlot_8          =          "{UseSpellSlot_8}"
Public Const cmidUseSpellSlot_9          =          "{UseSpellSlot_9}"
Public Const cmidAFKState                =                "{AFKState}"
Public Const cmidAtEaseState             =             "{AtEaseState}"
Public Const cmidATOYOT                  =                  "{ATOYOT}"
Public Const cmidAkimbo                  =                  "{Akimbo}"
Public Const cmidAkimboState             =             "{AkimboState}"
Public Const cmidBeckon                  =                  "{Beckon}"
Public Const cmidBeSeeingYou             =             "{BeSeeingYou}"
Public Const cmidBlowKiss                =                "{BlowKiss}"
Public Const cmidBowDeep                 =                 "{BowDeep}"
Public Const cmidBowDeepState            =            "{BowDeepState}"
Public Const cmidCheer                   =                   "{Cheer}"
Public Const cmidClapHands               =               "{ClapHands}"
Public Const cmidClapHandsState          =          "{ClapHandsState}"
Public Const cmidCrossArmsState          =          "{CrossArmsState}"
Public Const cmidCringe                  =                  "{Cringe}"
Public Const cmidCry                     =                     "{Cry}"
Public Const cmidCurtseyState            =            "{CurtseyState}"
Public Const cmidDrudgeDance             =             "{DrudgeDance}"
Public Const cmidDrudgeDanceState        =        "{DrudgeDanceState}"
Public Const cmidHaveASeat               =               "{HaveASeat}"
Public Const cmidHaveASeatState          =          "{HaveASeatState}"
Public Const cmidHeartyLaugh             =             "{HeartyLaugh}"
Public Const cmidHelper                  =                  "{Helper}"
Public Const cmidKneel                   =                   "{Kneel}"
Public Const cmidKneelState              =              "{KneelState}"
Public Const cmidKnock                   =                   "{Knock}"
Public Const cmidLaugh                   =                   "{Laugh}"
Public Const cmidLeanState               =               "{LeanState}"
Public Const cmidMeditateState           =           "{MeditateState}"
Public Const cmidMimeDrink               =               "{MimeDrink}"
Public Const cmidMimeEat                 =                 "{MimeEat}"
Public Const cmidMock                    =                    "{Mock}"
Public Const cmidNod                     =                     "{Nod}"
Public Const cmidNudgeLeft               =               "{NudgeLeft}"
Public Const cmidNudgeRight              =              "{NudgeRight}"
Public Const cmidPlead                   =                   "{Plead}"
Public Const cmidPleadState              =              "{PleadState}"
Public Const cmidPoint                   =                   "{Point}"
Public Const cmidPointState              =              "{PointState}"
Public Const cmidPointDown               =               "{PointDown}"
Public Const cmidPointDownState          =          "{PointDownState}"
Public Const cmidPointLeft               =               "{PointLeft}"
Public Const cmidPointLeftState          =          "{PointLeftState}"
Public Const cmidPointRight              =              "{PointRight}"
Public Const cmidPointRightState         =         "{PointRightState}"
Public Const cmidPossumState             =             "{PossumState}"
Public Const cmidPray                    =                    "{Pray}"
Public Const cmidPrayState               =               "{PrayState}"
Public Const cmidReadState               =               "{ReadState}"
Public Const cmidSalute                  =                  "{Salute}"
Public Const cmidSaluteState             =             "{SaluteState}"
Public Const cmidScanHorizon             =             "{ScanHorizon}"
Public Const cmidScratchHead             =             "{ScratchHead}"
Public Const cmidScratchHeadState        =        "{ScratchHeadState}"
Public Const cmidShakeHead               =               "{ShakeHead}"
Public Const cmidShakeFist               =               "{ShakeFist}"
Public Const cmidShakeFistState          =          "{ShakeFistState}"
Public Const cmidShiver                  =                  "{Shiver}"
Public Const cmidShiverState             =             "{ShiverState}"
Public Const cmidShoo                    =                    "{Shoo}"
Public Const cmidShrug                   =                   "{Shrug}"
Public Const cmidSitState                =                "{SitState}"
Public Const cmidSitBackState            =            "{SitBackState}"
Public Const cmidSitCrossleggedState     =     "{SitCrossleggedState}"
Public Const cmidSlouch                  =                  "{Slouch}"
Public Const cmidSlouchState             =             "{SlouchState}"
Public Const cmidSmackHead               =               "{SmackHead}"
Public Const cmidSnowAngelState          =          "{SnowAngelState}"
Public Const cmidSpit                    =                    "{Spit}"
Public Const cmidSurrender               =               "{Surrender}"
Public Const cmidSurrenderState          =          "{SurrenderState}"
Public Const cmidTalktotheHandState      =      "{TalktotheHandState}"
Public Const cmidTapFoot                 =                 "{TapFoot}"
Public Const cmidTapFootState            =            "{TapFootState}"
Public Const cmidTeapot                  =                  "{Teapot}"
Public Const cmidThinkerState            =            "{ThinkerState}"
Public Const cmidWarmHands               =               "{WarmHands}"
Public Const cmidWave                    =                    "{Wave}"
Public Const cmidWaveState               =               "{WaveState}"
Public Const cmidWaveHigh                =                "{WaveHigh}"
Public Const cmidWaveLow                 =                 "{WaveLow}"
Public Const cmidWoah                    =                    "{Woah}"
Public Const cmidWoahState               =               "{WoahState}"
Public Const cmidWinded                  =                  "{Winded}"
Public Const cmidWindedState             =             "{WindedState}"
Public Const cmidYawnStretch             =             "{YawnStretch}"
Public Const cmidYMCA                    =                    "{YMCA}"
Public Const cmidCameraMoveToward        =        "{CameraMoveToward}"
Public Const cmidCameraMoveAway          =          "{CameraMoveAway}"
Public Const cmidCameraActivateAlternateMode="{CameraActivateAlternateMode}"
Public Const cmidCameraInstantMouseLook  =  "{CameraInstantMouseLook}"
Public Const cmidCameraRotateLeft        =        "{CameraRotateLeft}"
Public Const cmidCameraRotateRight       =       "{CameraRotateRight}"
Public Const cmidCameraRotateUp          =          "{CameraRotateUp}"
Public Const cmidCameraRotateDown        =        "{CameraRotateDown}"
Public Const cmidCameraViewDefault       =       "{CameraViewDefault}"
Public Const cmidCameraViewFirstPerson   =   "{CameraViewFirstPerson}"
Public Const cmidCameraViewLookDown      =      "{CameraViewLookDown}"
Public Const cmidCameraViewMapMode       =       "{CameraViewMapMode}"
Public Const cmidAltEnter                =                "{AltEnter}"
Public Const cmidAltTab                  =                  "{AltTab}"
Public Const cmidAltF4                   =                   "{AltF4}"
Public Const cmidCtrlShiftEsc            =            "{CtrlShiftEsc}"
Public Const cmidPointerX                =                "{PointerX}"
Public Const cmidPointerY                =                "{PointerY}"
Public Const cmidSelectLeft              =              "{SelectLeft}"
Public Const cmidSelectRight             =             "{SelectRight}"
Public Const cmidSelectMid               =               "{SelectMid}"
Public Const cmidSelectDblLeft           =           "{SelectDblLeft}"
Public Const cmidSelectDblRight          =          "{SelectDblRight}"
Public Const cmidSelectDblMid            =            "{SelectDblMid}"
Public Const cmidScrollUp                =                "{ScrollUp}"
Public Const cmidScrollDown              =              "{ScrollDown}"
Public Const cmidCursorCharLeft          =          "{CursorCharLeft}"
Public Const cmidCursorCharRight         =         "{CursorCharRight}"
Public Const cmidCursorPreviousLine      =      "{CursorPreviousLine}"
Public Const cmidCursorNextLine          =          "{CursorNextLine}"
Public Const cmidCursorPreviousPage      =      "{CursorPreviousPage}"
Public Const cmidCursorNextPage          =          "{CursorNextPage}"
Public Const cmidCursorWordLeft          =          "{CursorWordLeft}"
Public Const cmidCursorWordRight         =         "{CursorWordRight}"
Public Const cmidCursorStartOfLine       =       "{CursorStartOfLine}"
Public Const cmidCursorStartOfDocument   =   "{CursorStartOfDocument}"
Public Const cmidCursorEndOfLine         =         "{CursorEndOfLine}"
Public Const cmidCursorEndOfDocument     =     "{CursorEndOfDocument}"
Public Const cmidEscapeKey               =               "{EscapeKey}"
Public Const cmidAcceptInput             =             "{AcceptInput}"
Public Const cmidDeleteKey               =               "{DeleteKey}"
Public Const cmidBackspaceKey            =            "{BackspaceKey}"
Public Const cmidCopyText                =                "{CopyText}"
Public Const cmidCutText                 =                 "{CutText}"
Public Const cmidPasteText               =               "{PasteText}"
Public Const cmidPlayerOption_ViewCombatTarget="{PlayerOption_ViewCombatTarget}"
Public Const cmidPlayerOption_AdvancedCombatUI="{PlayerOption_AdvancedCombatUI}"
Public Const cmidPlayerOption_AcceptLootPermits="{PlayerOption_AcceptLootPermits}"
Public Const cmidPlayerOption_DisplayAllegianceLogonNotifications="{PlayerOption_DisplayAllegianceLogonNotifications}"
Public Const cmidPlayerOption_UseChargeAttack="{PlayerOption_UseChargeAttack}"
Public Const cmidPlayerOption_UseCraftSuccessDialog="{PlayerOption_UseCraftSuccessDialog}"
Public Const cmidPlayerOption_HearAllegianceChat="{PlayerOption_HearAllegianceChat}"
Public Const cmidPlayerOption_DisplayDateOfBirth="{PlayerOption_DisplayDateOfBirth}"
Public Const cmidPlayerOption_DisplayAge = "{PlayerOption_DisplayAge}"
Public Const cmidPlayerOption_DisplayChessRank="{PlayerOption_DisplayChessRank}"
Public Const cmidPlayerOption_DisplayFishingSkill="{PlayerOption_DisplayFishingSkill}"
Public Const cmidPlayerOption_DisplayNumberDeaths="{PlayerOption_DisplayNumberDeaths}"
Public Const cmidPlayerOption_DisplayTimeStamps="{PlayerOption_DisplayTimeStamps}"
Public Const cmidPlayerOption_SalvageMultiple="{PlayerOption_SalvageMultiple}"
Public Const cmidPlayerOption_AutoRepeatAttack="{PlayerOption_AutoRepeatAttack}"
Public Const cmidPlayerOption_IgnoreAllegianceRequests="{PlayerOption_IgnoreAllegianceRequests}"
Public Const cmidPlayerOption_IgnoreFellowshipRequests="{PlayerOption_IgnoreFellowshipRequests}"
Public Const cmidPlayerOption_IgnoreTradeRequests="{PlayerOption_IgnoreTradeRequests}"
Public Const cmidPlayerOption_PersistentAtDay="{PlayerOption_PersistentAtDay}"
Public Const cmidPlayerOption_AllowGive  =  "{PlayerOption_AllowGive}"
Public Const cmidPlayerOption_ShowTooltips="{PlayerOption_ShowTooltips}"
Public Const cmidPlayerOption_UseDeception="{PlayerOption_UseDeception}"
Public Const cmidPlayerOption_ToggleRun  =  "{PlayerOption_ToggleRun}"
Public Const cmidPlayerOption_StayInChatMode="{PlayerOption_StayInChatMode}"
Public Const cmidPlayerOption_AutoTarget = "{PlayerOption_AutoTarget}"
Public Const cmidPlayerOption_VividTargetingIndicator="{PlayerOption_VividTargetingIndicator}"
Public Const cmidPlayerOption_FellowshipShareXP="{PlayerOption_FellowshipShareXP}"
Public Const cmidPlayerOption_FellowshipShareLoot="{PlayerOption_FellowshipShareLoot}"
Public Const cmidPlayerOption_FellowshipAutoAcceptRequests="{PlayerOption_FellowshipAutoAcceptRequests}"
Public Const cmidPlayerOption_StretchUI  =  "{PlayerOption_StretchUI}"
Public Const cmidPlayerOption_CoordinatesOnRadar="{PlayerOption_CoordinatesOnRadar}"
Public Const cmidPlayerOption_SpellDuration="{PlayerOption_SpellDuration}"
Public Const cmidPlayerOption_DisableHouseRestrictionEffects="{PlayerOption_DisableHouseRestrictionEffects}"
Public Const cmidPlayerOption_DragItemOnPlayerOpensSecureTrade="{PlayerOption_DragItemOnPlayerOpensSecureTrade}"
Public Const cmidCameraCloser            =            "{CameraCloser}"
Public Const cmidCameraFarther           =           "{CameraFarther}"
Public Const cmidCameraLeftRotate        =        "{CameraLeftRotate}"
Public Const cmidCameraLower             =             "{CameraLower}"
Public Const cmidCameraRaise             =             "{CameraRaise}"
Public Const cmidCameraRightRotate       =       "{CameraRightRotate}"
Public Const cmidFirstPersonView         =         "{FirstPersonView}"
Public Const cmidFloorView               =               "{FloorView}"
Public Const cmidMapView                 =                 "{MapView}"
Public Const cmidResetView               =               "{ResetView}"
Public Const cmidShiftView               =               "{ShiftView}"
Public Const cmidAutoRun                 =                 "{AutoRun}"
Public Const cmidHoldRun                 =                 "{HoldRun}"
Public Const cmidHoldSidestep            =            "{HoldSidestep}"
Public Const cmidJump                    =                    "{Jump}"
Public Const cmidSideStepLeft            =            "{SideStepLeft}"
Public Const cmidSideStepRight           =           "{SideStepRight}"
Public Const cmidTurnLeft                =                "{TurnLeft}"
Public Const cmidTurnRight               =               "{TurnRight}"
Public Const cmidWalkBackwards           =           "{WalkBackwards}"
Public Const cmidWalkForward             =             "{WalkForward}"
Public Const cmidHighAttack              =              "{HighAttack}"
Public Const cmidLowAttack               =               "{LowAttack}"
Public Const cmidMediumAttack            =            "{MediumAttack}"
Public Const cmidToggleCombat            =            "{ToggleCombat}"
Public Const cmidDecreasePowerSetting    =    "{DecreasePowerSetting}"
Public Const cmidIncreasePowerSetting    =    "{IncreasePowerSetting}"
Public Const cmidClosestCompassItem      =      "{ClosestCompassItem}"
Public Const cmidClosestItem             =             "{ClosestItem}"
Public Const cmidClosestMonster          =          "{ClosestMonster}"
Public Const cmidClosestPlayer           =           "{ClosestPlayer}"
Public Const cmidLastAttacker            =            "{LastAttacker}"
Public Const cmidNextCompassItem         =         "{NextCompassItem}"
Public Const cmidNextFellow              =              "{NextFellow}"
Public Const cmidNextItem                =                "{NextItem}"
Public Const cmidNextMonster             =             "{NextMonster}"
Public Const cmidNextPlayer              =              "{NextPlayer}"
Public Const cmidPreviousCompassItem     =     "{PreviousCompassItem}"
Public Const cmidPreviousFellow          =          "{PreviousFellow}"
Public Const cmidPreviousItem            =            "{PreviousItem}"
Public Const cmidPreviousMonster         =         "{PreviousMonster}"
Public Const cmidPreviousPlayer          =          "{PreviousPlayer}"
Public Const cmidPreviousSelection       =       "{PreviousSelection}"
Public Const cmidDropSelected            =            "{DropSelected}"
Public Const cmidExamineSelected         =         "{ExamineSelected}"
Public Const cmidGiveSelected            =            "{GiveSelected}"
Public Const cmidAutosortSelected        =        "{AutosortSelected}"
Public Const cmidSelectSelf              =              "{SelectSelf}"
Public Const cmidSplitSelected           =           "{SplitSelected}"
Public Const cmidUseSelected             =             "{UseSelected}"
Public Const cmidAllegiancePanel         =         "{AllegiancePanel}"
Public Const cmidAttributesPanel         =         "{AttributesPanel}"
Public Const cmidCharacterInformationPanel="{CharacterInformationPanel}"
Public Const cmidCharacterOptionsPanel   =   "{CharacterOptionsPanel}"
Public Const cmidFellowshipPanel         =         "{FellowshipPanel}"
Public Const cmidHarmfulSpellsPanel      =      "{HarmfulSpellsPanel}"
Public Const cmidHelpfulSpellsPanel      =      "{HelpfulSpellsPanel}"
Public Const cmidHousePanel              =              "{HousePanel}"
Public Const cmidInventoryPanel          =          "{InventoryPanel}"
Public Const cmidLinkStatusPanel         =         "{LinkStatusPanel}"
Public Const cmidMapPanel                =                "{MapPanel}"
Public Const cmidOptionsPanel            =            "{OptionsPanel}"
Public Const cmidSkillsPanel             =             "{SkillsPanel}"
Public Const cmidSoundAndGraphicsPanel   =   "{SoundAndGraphicsPanel}"
Public Const cmidSpellComponentsPanel    =    "{SpellComponentsPanel}"
Public Const cmidSpellbookPanel          =          "{SpellbookPanel}"
Public Const cmidTradePanel              =              "{TradePanel}"
Public Const cmidVitaePanel              =              "{VitaePanel}"
Public Const cmidAcceptCorpseLooting     =     "{AcceptCorpseLooting}"
Public Const cmidAdvancedCombatInterface = "{AdvancedCombatInterface}"
Public Const cmidAttemptToDeceivePlayers = "{AttemptToDeceivePlayers}"
Public Const cmidAutoCreateShortcuts     =     "{AutoCreateShortcuts}"
Public Const cmidAutoRepeatAttacks       =       "{AutoRepeatAttacks}"
Public Const cmidAutoTarget              =              "{AutoTarget}"
Public Const cmidAutoTrackCombatTargets  =  "{AutoTrackCombatTargets}"
Public Const cmidDisableHouseEffect      =      "{DisableHouseEffect}"
Public Const cmidDisableWeather          =          "{DisableWeather}"
Public Const cmidDisplayTooltips         =         "{DisplayTooltips}"
Public Const cmidIgnoreAllegianceRequests="{IgnoreAllegianceRequests}"
Public Const cmidIgnoreFellowshipRequests="{IgnoreFellowshipRequests}"
Public Const cmidIgnoreTradeRequests     =     "{IgnoreTradeRequests}"
Public Const cmidInvertMouseLook         =         "{InvertMouseLook}"
Public Const cmidLetPlayersGiveYouItems  =  "{LetPlayersGiveYouItems}"
Public Const cmidMuteOnLosingFocus       =       "{MuteOnLosingFocus}"
Public Const cmidRightClickToMouseLook   =   "{RightClickToMouseLook}"
Public Const cmidRunAsDefaultMovement    =    "{RunAsDefaultMovement}"
Public Const cmidShareFellowshipLoot     =     "{ShareFellowshipLoot}"
Public Const cmidShareFellowshipXP       =       "{ShareFellowshipXP}"
Public Const cmidShowRadarCoordinates    =    "{ShowRadarCoordinates}"
Public Const cmidShowSpellDurations      =      "{ShowSpellDurations}"
Public Const cmidStayInChatModeAfterSend = "{StayInChatModeAfterSend}"
Public Const cmidStretchUI               =               "{StretchUI}"
Public Const cmidVividTargetIndicator    =    "{VividTargetIndicator}"
Public Const cmidCaptureScreenshotToFile = "{CaptureScreenshotToFile}"

' dmty: Damage type masks
' dmty values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to indicate more than one type of damage.
Public Const dmtySlashing                =                     &H0001&
Public Const dmtyPiercing                =                     &H0002&
Public Const dmtyBludgeoning             =                     &H0004&
Public Const dmtyCold                    =                     &H0008&
Public Const dmtyFire                    =                     &H0010&
Public Const dmtyAcid                    =                     &H0020&
Public Const dmtyElectrical              =                     &H0040&

' eqm: Equipment type masks
' eqm values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to specify multiple equipment slots.
Public Const eqmNil                      =                           0    ' No slot.
Public Const eqmHead                     =                 &H00000001&
Public Const eqmChestUnder               =                 &H00000002&
Public Const eqmAbdomenUnder             =                 &H00000004&
Public Const eqmUpperArmsUnder           =                 &H00000008&
Public Const eqmLowerArmsUnder           =                 &H00000010&
Public Const eqmHands                    =                 &H00000020&
Public Const eqmUpperLegsUnder           =                 &H00000040&
Public Const eqmLowerLegsUnder           =                 &H00000080&
Public Const eqmFeet                     =                 &H00000100&
Public Const eqmChestOuter               =                 &H00000200&
Public Const eqmAbdomenOuter             =                 &H00000400&
Public Const eqmUpperArmsOuter           =                 &H00000800&
Public Const eqmLowerArmsOuter           =                 &H00001000&
Public Const eqmUpperLegsOuter           =                 &H00002000&
Public Const eqmLowerLegsOuter           =                 &H00004000&
Public Const eqmNecklace                 =                 &H00008000&
Public Const eqmRBracelet                =                 &H00010000&
Public Const eqmLBracelet                =                 &H00020000&
Public Const eqmBracelets                =                 &H00030000&    ' Both bracelet slots.
Public Const eqmRRing                    =                 &H00040000&
Public Const eqmLRing                    =                 &H00080000&
Public Const eqmRings                    =                 &H000C0000&    ' Both ring slots.
Public Const eqmWeapon                   =                 &H00100000&    ' Melee weapon slot.
Public Const eqmShield                   =                 &H00200000&
Public Const eqmRangedWeapon             =                 &H00400000&    ' Bow or crossbow slot.
Public Const eqmAmmo                     =                 &H00800000&
Public Const eqmFocusWeapon              =                 &H01000000&    ' Wand, staff, or orb slot.
Public Const eqmWeapons                  =                 &H01500000&    ' All weapons of any kind.
Public Const eqmAll                      =                 &H01FFFFFF&    ' All equipment slots.

' evid: Event IDs
Public Const evidNil                     =                           0    ' No event was received.
Public Const evidOnVitals                =                           1
Public Const evidOnLocChangeSelf         =                           2
Public Const evidOnLocChangeOther        =                           3
Public Const evidOnStatTotalBurden       =                           4
Public Const evidOnStatTotalExp          =                           5
Public Const evidOnStatUnspentExp        =                           6
Public Const evidOnObjectCreatePlayer    =                           7
Public Const evidOnStatSkillExp          =                           8
Public Const evidOnAdjustStack           =                           9
Public Const evidOnChatLocal             =                          10
Public Const evidOnChatSpell             =                          11
Public Const evidOnDeathSelf             =                          12
Public Const evidOnMyPlayerKill          =                          13
Public Const evidOnDeathOther            =                          14
Public Const evidOnEnd3D                 =                          15
Public Const evidOnStart3D               =                          16
Public Const evidOnStartPortalSelf       =                          17
Public Const evidOnEndPortalSelf         =                          18
Public Const evidOnEndPortalOther        =                          19
Public Const evidOnCombatMode            =                          20
Public Const evidOnAnimationSelf         =                          21
Public Const evidOnTargetHealth          =                          22
Public Const evidOnAddToInventory        =                          23
Public Const evidOnRemoveFromInventory   =                          24
Public Const evidOnAssessCreature        =                          25
Public Const evidOnAssessItem            =                          26
Public Const evidOnSpellFailSelf         =                          27
Public Const evidOnMeleeEvadeSelf        =                          28
Public Const evidOnMeleeEvadeOther       =                          29
Public Const evidOnMeleeDamageSelf       =                          30
Public Const evidOnMeleeDamageOther      =                          31
Public Const evidOnLogon                 =                          32
Public Const evidOnTellServer            =                          33
Public Const evidOnTell                  =                          34
Public Const evidOnTellMelee             =                          35
Public Const evidOnTellMisc              =                          36
Public Const evidOnTellFellowship        =                          37
Public Const evidOnTellPatron            =                          38
Public Const evidOnTellVassal            =                          39
Public Const evidOnTellFollower          =                          40
Public Const evidOnDeathMessage          =                          41
Public Const evidOnSpellCastSelf         =                          42
Public Const evidOnTradeStart            =                          43
Public Const evidOnTradeEnd              =                          44
Public Const evidOnTradeAdd              =                          45
Public Const evidOnTradeReset            =                          46
Public Const evidOnSetFlagSelf           =                          47
Public Const evidOnSetFlagOther          =                          48
Public Const evidOnApproachVendor        =                          49
Public Const evidOnChatBroadcast         =                          50
Public Const evidOnTradeAccept           =                          51
Public Const evidOnObjectCreate          =                          52
Public Const evidOnChatServer            =                          53
Public Const evidOnSpellExpire           =                          54
Public Const evidOnSpellExpireSilent     =                          55
Public Const evidOnStatTotalPyreals      =                          56
Public Const evidOnStatLevel             =                          57
Public Const evidOnChatEmoteStandard     =                          58
Public Const evidOnChatEmoteCustom       =                          59
Public Const evidOnOpenContainer         =                          60
Public Const evidOnPortalStormWarning    =                          61
Public Const evidOnPortalStormed         =                          62
Public Const evidOnCommand               =                          63
Public Const evidOnControlEvent          =                          64
Public Const evidOnItemManaBar           =                          65
Public Const evidOnActionComplete        =                          66
Public Const evidOnObjectDestroy         =                          67
Public Const evidOnLocChangeCreature     =                          68
Public Const evidOnMoveItem              =                          69
Public Const evidOnSpellAdd              =                          70
Public Const evidOnFellowCreate          =                          71
Public Const evidOnFellowDisband         =                          72
Public Const evidOnFellowDismiss         =                          73
Public Const evidOnFellowQuit            =                          74
Public Const evidOnFellowRecruit         =                          75
Public Const evidOnMeleeLastAttacker     =                          76
Public Const evidOnLeaveVendor           =                          77
Public Const evidOnTimer                 =                          78
Public Const evidOnControlProperty       =                          79
Public Const evidOnPluginMsg             =                          80
Public Const evidOnTellAllegiance        =                          81
Public Const evidOnTellCovassal          =                          82
Public Const evidOnHotkey                =                          83
Public Const evidOnFellowInvite          =                          84
Public Const evidOnNavStop               =                          85
Public Const evidOnFellowUpdate          =                          86
Public Const evidOnDisconnect            =                          87
Public Const evidOnCraftConfirm          =                          88
Public Const evidOnChatBoxMessage        =                          89
Public Const evidOnTipMessage            =                          90
Public Const evidOnOpenContainerPanel    =                          91
Public Const evidOnCloseContainer        =                          92
Public Const evidOnChatBoxClick          =                          93
Public Const evidMax                     =                          94

' ioc: Icon outline color masks
' ioc values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript).
Public Const iocNil                      =                           0
Public Const iocEnchanted                =                 &H00000001&    ' Has spells.
Public Const iocUnknown2                 =                 &H00000002&    ' (No known meaning.)
Public Const iocHealing                  =                 &H00000004&    ' Restores health.
Public Const iocMana                     =                 &H00000008&    ' Restores mana.
Public Const iocHearty                   =                 &H00000010&    ' Restores stamina.
Public Const iocFire                     =                 &H00000020&
Public Const iocLightning                =                 &H00000040&
Public Const iocFrost                    =                 &H00000080&
Public Const iocAcid                     =                 &H00000100&

' kmo: Key motions
Public Const kmoDown                     =                       &H01&    ' Press the key.
Public Const kmoUp                       =                       &H02&    ' Release the key.
Public Const kmoClick                    =                       &H03&    ' Press and release.

' material: Material codes
Public Const materialCeramic             =                           1
Public Const materialPorcelain           =                           2
Public Const materialCloth               =                           3
Public Const materialLinen               =                           4
Public Const materialSatin               =                           5
Public Const materialSilk                =                           6
Public Const materialVelvet              =                           7
Public Const materialWool                =                           8
Public Const materialGem                 =                           9
Public Const materialAgate               =                          10
Public Const materialAmber               =                          11
Public Const materialAmethyst            =                          12
Public Const materialAquamarine          =                          13
Public Const materialAzurite             =                          14
Public Const materialBlackGarnet         =                          15
Public Const materialBlackOpal           =                          16
Public Const materialBloodstone          =                          17
Public Const materialCarnelian           =                          18
Public Const materialCitrine             =                          19
Public Const materialDiamond             =                          20
Public Const materialEmerald             =                          21
Public Const materialFireOpal            =                          22
Public Const materialGreenGarnet         =                          23
Public Const materialGreenJade           =                          24
Public Const materialHematite            =                          25
Public Const materialImperialTopaz       =                          26
Public Const materialJet                 =                          27
Public Const materialLapisLazuli         =                          28
Public Const materialLavenderJade        =                          29
Public Const materialMalachite           =                          30
Public Const materialMoonstone           =                          31
Public Const materialOnyx                =                          32
Public Const materialOpal                =                          33
Public Const materialPeridot             =                          34
Public Const materialRedGarnet           =                          35
Public Const materialRedJade             =                          36
Public Const materialRoseQuartz          =                          37
Public Const materialRuby                =                          38
Public Const materialSapphire            =                          39
Public Const materialSmokeyQuartz        =                          40
Public Const materialSunstone            =                          41
Public Const materialTigerEye            =                          42
Public Const materialTourmaline          =                          43
Public Const materialTurquoise           =                          44
Public Const materialWhiteJade           =                          45
Public Const materialWhiteQuartz         =                          46
Public Const materialWhiteSapphire       =                          47
Public Const materialYellowGarnet        =                          48
Public Const materialYellowTopaz         =                          49
Public Const materialZircon              =                          50
Public Const materialIvory               =                          51
Public Const materialLeather             =                          52
Public Const materialDilloHide           =                          53
Public Const materialGromnieHide         =                          54
Public Const materialReedsharkHide       =                          55
Public Const materialMetal               =                          56
Public Const materialBrass               =                          57
Public Const materialBronze              =                          58
Public Const materialCopper              =                          59
Public Const materialGold                =                          60
Public Const materialIron                =                          61
Public Const materialPyreal              =                          62
Public Const materialSilver              =                          63
Public Const materialSteel               =                          64
Public Const materialStone               =                          65
Public Const materialAlabaster           =                          66
Public Const materialGranite             =                          67
Public Const materialMarble              =                          68
Public Const materialObsidian            =                          69
Public Const materialSandstone           =                          70
Public Const materialSerpentine          =                          71
Public Const materialWood                =                          72
Public Const materialEbony               =                          73
Public Const materialMahogany            =                          74
Public Const materialOak                 =                          75
Public Const materialPine                =                          76
Public Const materialTeak                =                          77

' mcm: Merchant category masks
Public Const mcmNil                      =                 &H00000000&    ' No category; not saleable.
Public Const mcmWeaponsMelee             =                 &H00000001&
Public Const mcmArmor                    =                 &H00000002&
Public Const mcmClothing                 =                 &H00000004&
Public Const mcmJewelry                  =                 &H00000008&
Public Const mcmCreature                 =                 &H00000010&
Public Const mcmFood                     =                 &H00000020&
Public Const mcmPyreal                   =                 &H00000040&
Public Const mcmMisc                     =                 &H00000080&
Public Const mcmWeaponsMissile           =                 &H00000100&
Public Const mcmContainers               =                 &H00000200&
Public Const mcmMiscFletching            =                 &H00000400&
Public Const mcmGems                     =                 &H00000800&
Public Const mcmSpellComponents          =                 &H00001000&
Public Const mcmBooksPaper               =                 &H00002000&
Public Const mcmKeysTools                =                 &H00004000&
Public Const mcmMagicItems               =                 &H00008000&
Public Const mcmPortal                   =                 &H00010000&
Public Const mcmLockable                 =                 &H00020000&    ' Chests have this is addition to mcmContainers.
Public Const mcmTradeNotes               =                 &H00040000&
Public Const mcmManaStones               =                 &H00080000&
Public Const mcmServices                 =                 &H00100000&
Public Const mcmPlants                   =                 &H00200000&
Public Const mcmCookingItems1            =                 &H00400000&
Public Const mcmAlchemicalItems1         =                 &H00800000&
Public Const mcmFletchingItems1          =                 &H01000000&
Public Const mcmCookingItems2            =                 &H02000000&
Public Const mcmAlchemicalItems2         =                 &H04000000&
Public Const mcmFletchingItems2          =                 &H08000000&
Public Const mcmLifestone                =                 &H10000000&
Public Const mcmTinkeringTools           =                 &H20000000&    ' The Ust is in this category.
Public Const mcmSalvageBag               =                 &H40000000&
Public Const mcmChessboard               =                 &H80000000&    ' There may be other items in this category as well.

' Miscellaneous constants
Public Const oidNil                      =                           0    ' Signifies no object.
Public Const ipackMain                   =                           0    ' Index of the main pack.
Public Const iitemNil                    =                          -1    ' Signifies the pack itself.
Public Const clrNil                      =                 &H7FFFFFFF&    ' VBScript has a problem with FFFFFFFF (turns it into 0000FFFF).
Public Const spellidNil                  =                           0    ' Signifies no spell.

' ibutton: Mouse button IDs
Public Const ibuttonLeft                 =                           0    ' Left button.
Public Const ibuttonRight                =                           1    ' Right button.
Public Const ibuttonMiddle               =                           2    ' Middle button.

' nopt: Navigation option masks
Public Const noptWalk                    =                     &H0001&
Public Const noptStopOnArrival           =                     &H0002&
Public Const noptZigzag                  =                     &H0004&

' nsc: Navigation stop codes for OnNavStop
Public Const nscArrived                  =                           0    ' You arrived at the target location.
Public Const nscTimeout                  =                           1    ' You did not arrive before the timeout expired.
Public Const nscCanceled                 =                           2    ' skapi.CancelNav was called.

' ocm: Object category masks
' ocm values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to specify multiple categories.
Public Const ocmNil                      =                     &H0000&    ' Include no objects.
Public Const ocmPlayer                   =                     &H0001&
Public Const ocmMonster                  =                     &H0002&
Public Const ocmPlayerCorpse             =                     &H0004&
Public Const ocmMonsterCorpse            =                     &H0008&
Public Const ocmLifestone                =                     &H0010&
Public Const ocmPortal                   =                     &H0020&
Public Const ocmMerchant                 =                     &H0040&
Public Const ocmEquipment                =                     &H0080&
Public Const ocmPK                       =                     &H0100&
Public Const ocmNonPK                    =                     &H0200&
Public Const ocmNPC                      =                     &H0400&
Public Const ocmHook                     =                     &H0800&
Public Const ocmAll                      =                          -1    ' Include all nearby objects of any kind.

' olc: Object location categories
' olc values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to specify multiple categories.
Public Const olcNil                      =                     &H0000&    ' Include no objects.
Public Const olcInventory                =                     &H0001&    ' Include objects in your inventory.
Public Const olcContained                =                     &H0002&    ' Include objects contained in another object (e.g. a chest or corpse).
Public Const olcEquipped                 =                     &H0004&    ' Include objects being worn or wielded (by you or someone else).
Public Const olcOnGround                 =                     &H0008&    ' Include objects out on their own in the game world.
Public Const olcAll                      =                          -1    ' Include all object regardless of location.

' oty: Object type masks
Public Const otyContainer                =                 &H00000001&
Public Const otyInscribable              =                 &H00000002&
Public Const otyNoPickup                 =                 &H00000004&
Public Const otyPlayer                   =                 &H00000008&
Public Const otySelectable               =                 &H00000010&
Public Const otyPK                       =                 &H00000020&
Public Const otyReadable                 =                 &H00000100&
Public Const otyMerchant                 =                 &H00000200&
Public Const otyDoor                     =                 &H00001000&
Public Const otyCorpse                   =                 &H00002000&
Public Const otyLifestone                =                 &H00004000&
Public Const otyFood                     =                 &H00008000&
Public Const otyHealingKit               =                 &H00010000&
Public Const otyLockpick                 =                 &H00020000&
Public Const otyPortal                   =                 &H00040000&
Public Const otyFoci                     =                 &H00800000&    ' The new magic foci, I think.
Public Const otyPKL                      =                 &H02000000&

' opm: Output modes
' opm values may be combined using the bitwise OR operator ("|" in JScript, "Or" in VBScript) to send output to multiple destinations.
Public Const opmDebugLog                 =                     &H0001&    ' Sends the output to the debug terminal (using the Win32 OutputDebugString API).
Public Const opmConsole                  =                     &H0002&    ' Sends the output to the SkunkWorks console window.
Public Const opmChatWnd                  =                     &H0010&    ' Sends the output to the primary in-game chat window.
Public Const opmChatWnd1                 =                     &H0020&    ' Sends the output to floating chat window #1.
Public Const opmChatWnd2                 =                     &H0040&    ' Sends the output to floating chat window #2.
Public Const opmChatWnd3                 =                     &H0080&    ' Sends the output to floating chat window #3.
Public Const opmChatWnd4                 =                     &H0100&    ' Sends the output to floating chat window #4.
Public Const opmChatWndAll               =                     &H01F0&    ' Sends the output to all in-game chat windows.

' plig: Player in game codes
Public Const pligNotRunning              =                           0    ' Game is not running.
Public Const pligAtLogin                 =                           1    ' Game is at login screen.
Public Const pligInPortal                =                           2    ' Player is in portal space.
Public Const pligInWorld                 =                           3    ' Player is in the game world.

' psw: Portal storm warning codes
Public Const pswStormEnded               =                           0    ' The portal storm is over.
Public Const pswMildStorm                =                           1    ' Mild portal storm.
Public Const pswHeavyStorm               =                           2    ' Heavy portal storm.

' skid: Skill IDs
Public Const skidNil                     =                           0    ' No skill.
Public Const skidAxe                     =                           1
Public Const skidBow                     =                           2
Public Const skidCrossbow                =                           3
Public Const skidDagger                  =                           4
Public Const skidMace                    =                           5
Public Const skidMeleeDefense            =                           6
Public Const skidMissileDefense          =                           7
Public Const skidSpear                   =                           9
Public Const skidStaff                   =                          10
Public Const skidSword                   =                          11
Public Const skidThrownWeapons           =                          12
Public Const skidUnarmedCombat           =                          13
Public Const skidArcaneLore              =                          14
Public Const skidMagicDefense            =                          15
Public Const skidManaConversion          =                          16
Public Const skidItemTinkering           =                          18
Public Const skidAssessPerson            =                          19
Public Const skidDeception               =                          20
Public Const skidHealing                 =                          21
Public Const skidJump                    =                          22
Public Const skidLockpick                =                          23
Public Const skidRun                     =                          24
Public Const skidAssessCreature          =                          27
Public Const skidWeaponTinkering         =                          28
Public Const skidArmorTinkering          =                          29
Public Const skidMagicItemTinkering      =                          30
Public Const skidCreatureEnchantment     =                          31
Public Const skidItemEnchantment         =                          32
Public Const skidLifeMagic               =                          33
Public Const skidWarMagic                =                          34
Public Const skidLeadership              =                          35
Public Const skidLoyalty                 =                          36
Public Const skidFletching               =                          37
Public Const skidAlchemy                 =                          38
Public Const skidCooking                 =                          39
Public Const skidSalvaging               =                          40
Public Const skidMax                     =                          41

' skts: Skill training status codes
Public Const sktsUnusable                =                           0
Public Const sktsUntrained               =                           1
Public Const sktsTrained                 =                           2
Public Const sktsSpecialized             =                           3

' Special key names
' {alt}
' {back}                                                                  ' Backspace
' {break}                                                                 ' Ctrl+Break
' {capslock}
' {control}
' {del}
' {down}                                                                  ' Down arrow
' {end}
' {esc}
' {f1}
' {f2}
' {f3}
' {f4}
' {f5}
' {f6}
' {f7}
' {f8}
' {f9}
' {f10}
' {f11}
' {f12}
' {home}
' {ins}
' {keypad 0}
' {keypad 1}
' {keypad 2}
' {keypad 3}
' {keypad 4}
' {keypad 5}
' {keypad 6}
' {keypad 7}
' {keypad 8}
' {keypad 9}
' {keypad *}
' {keypad +}
' {keypad -}
' {keypad /}
' {keypad .}
' {keypad enter}                                                          ' Keypad Enter
' {left}                                                                  ' Left arrow
' {numlock}
' {pause}
' {pgdn}
' {pgup}
' {prtsc}                                                                 ' Print Screen
' {return}                                                                ' Enter (the regular one, above RShift)
' {right}                                                                 ' Right arrow
' {scroll}                                                                ' Scroll Lock
' {shift}
' {space}                                                                 ' Spacebar
' {tab}
' {up}                                                                    ' Up arrow

' species: Species codes
Public Const speciesInvalid              =                           0
Public Const speciesOlthoi               =                           1
Public Const speciesBanderling           =                           2
Public Const speciesDrudge               =                           3
Public Const speciesMosswart             =                           4
Public Const speciesLugian               =                           5
Public Const speciesTumerok              =                           6
Public Const speciesMite                 =                           7
Public Const speciesTusker               =                           8
Public Const speciesPhyntosWasp          =                           9
Public Const speciesRat                  =                          10
Public Const speciesAuroch               =                          11
Public Const speciesCow                  =                          12
Public Const speciesGolem                =                          13
Public Const speciesUndead               =                          14
Public Const speciesGromnie              =                          15
Public Const speciesReedshark            =                          16
Public Const speciesArmoredillo          =                          17
Public Const speciesFae                  =                          18
Public Const speciesVirindi              =                          19
Public Const speciesWisp                 =                          20
Public Const speciesKnathtead            =                          21
Public Const speciesShadow               =                          22
Public Const speciesMattekar             =                          23
Public Const speciesMumiyah              =                          24
Public Const speciesRabbit               =                          25
Public Const speciesSclavus              =                          26
Public Const speciesShallowsShark        =                          27
Public Const speciesMonouga              =                          28
Public Const speciesZefir                =                          29
Public Const speciesSkeleton             =                          30
Public Const speciesHuman                =                          31
Public Const speciesShreth               =                          32
Public Const speciesChittick             =                          33
Public Const speciesMoarsman             =                          34
Public Const speciesOlthoiLarvae         =                          35
Public Const speciesSlithis              =                          36
Public Const speciesDeru                 =                          37
Public Const speciesFireElemental        =                          38
Public Const speciesSnowman              =                          39
Public Const speciesUnknown              =                          40
Public Const speciesBunny                =                          41
Public Const speciesLightningElemental   =                          42
Public Const speciesRockslide            =                          43
Public Const speciesGrievver             =                          44
Public Const speciesNiffis               =                          45
Public Const speciesUrsuin               =                          46
Public Const speciesCrystal              =                          47
Public Const speciesHollowMinion         =                          48
Public Const speciesScarecrow            =                          49
Public Const speciesIdol                 =                          50
Public Const speciesEmpyrean             =                          51
Public Const speciesHopeslayer           =                          52
Public Const speciesDoll                 =                          53
Public Const speciesMarionette           =                          54
Public Const speciesCarenzi              =                          55
Public Const speciesSiraluun             =                          56
Public Const speciesAunTumerok           =                          57
Public Const speciesHeaTumerok           =                          58
Public Const speciesSimulacrum           =                          59
Public Const speciesAcidElemental        =                          60
Public Const speciesFrostElemental       =                          61
Public Const speciesElemental            =                          62
Public Const speciesStatue               =                          63
Public Const speciesWall                 =                          64
Public Const speciesAlteredHuman         =                          65
Public Const speciesDevice               =                          66
Public Const speciesHarbinger            =                          67
Public Const speciesDarkSarcophagus      =                          68
Public Const speciesChicken              =                          69
Public Const speciesGotrokLugian         =                          70
Public Const speciesMargul               =                          71
Public Const speciesBleachedRabbit       =                          72
Public Const speciesNastyRabbit          =                          73
Public Const speciesGrimacingRabbit      =                          74
Public Const speciesBurun                =                          75
Public Const speciesTarget               =                          76
Public Const speciesGhost                =                          77
Public Const speciesFiun                 =                          78
Public Const speciesEater                =                          79
Public Const speciesPenguin              =                          80
Public Const speciesRuschk               =                          81
Public Const speciesThrungus             =                          82
Public Const speciesViamontianKnight     =                          83
Public Const speciesRemoran              =                          84
Public Const speciesSwarm                =                          85
Public Const speciesMoar                 =                          86
Public Const speciesEnchantedArms        =                          87
Public Const speciesSleech               =                          88
Public Const speciesMukkir               =                          89

' vital: Vital stat codes
Public Const vitalNil                    =                           0
Public Const vitalHealthMax              =                           1
Public Const vitalHealthCur              =                           2    ' Cannot be used with skapi.SkinfoFromVital.
Public Const vitalStaminaMax             =                           3
Public Const vitalStaminaCur             =                           4    ' Cannot be used with skapi.SkinfoFromVital.
Public Const vitalManaMax                =                           5
Public Const vitalManaCur                =                           6    ' Cannot be used with skapi.SkinfoFromVital.
Public Const vitalMax                    =                           7

' wem: WaitEvent modes
Public Const wemNormal                   =                           0    ' Returns when timeout elapsed or the queue is empty.
Public Const wemSingle                   =                           1    ' Waits for a single event of any type.
Public Const wemSpecific                 =                           2    ' Waits for a specific event type.
Public Const wemFullTimeout              =                           3    ' Always waits the full timeout interval.


