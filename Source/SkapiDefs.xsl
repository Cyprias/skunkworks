<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  version="1.0">


<xsl:template match="schema">
	<html id="TOCSkapiDefs">
	
	<head>
	<title>SkunkWorks API Constants</title>
	<link rel="stylesheet" href="SkunkWorks.css"/>
	</head>
	
	<body class="Normal" lang="EN-US">
	
	<h1><a name="_Top"/>SkunkWorks API constants</h1>
	
	<p class="Normal">This section lists the various bitmasks and constants used 
	by the <a href="Skapi.html">SkunkWorks API</a>.  Definitions for all of these 
	may also be found in SkapiDefs.js and SkapiDefs.vbs.</p>
	
	<xsl:apply-templates select="group">
		<xsl:sort select="@szTitle"/>
	</xsl:apply-templates>
	
	</body>
	
	</html>
</xsl:template>

<xsl:template match="group">
	<xsl:variable name="szLabel"><xsl:call-template name="SzLabelFromGroup">
		<xsl:with-param name="group" select="."/>
	</xsl:call-template></xsl:variable>
	<h2>
		<xsl:if test="$szLabel != ''">
			<a><xsl:attribute name="name">_<xsl:value-of select="$szLabel"/></xsl:attribute></a>
		</xsl:if>
		<span>
		<xsl:if test="@ver">
			<xsl:attribute name="class">Highlight<xsl:value-of select="@ver"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="@szHung != ''">
			<xsl:value-of select="@szHung"/><xsl:text>: </xsl:text>
		</xsl:if>
		<xsl:value-of select="@szTitle"/>
		</span>
	</h2>
	<xsl:apply-templates select="comment"/>
	
	<table class="Constant">
		<xsl:choose>
		<xsl:when test="@fStrings">
			<col style="width: 2.25in"/>
			<col style="width: 2.0in"/>
			<col/>
		</xsl:when>
		<xsl:otherwise>
			<col style="width: 2.0in"/>
			<col style="width: 1.0in"/>
			<col/>
		</xsl:otherwise>
		</xsl:choose>
		
		<tr>
			<th><p class="Tablecontents">Name</p></th>
			
			<xsl:choose>
			<xsl:when test="@fStrings">
				<th><p class="Tablecontents">Value</p></th>
			</xsl:when>
			<xsl:when test="constant[@szValue != '']">
				<th><p class="Tablecontents" 
				  style="text-align: right;">Value</p></th>
			</xsl:when>
			<xsl:otherwise>
				<th/>
			</xsl:otherwise>
			</xsl:choose>
			
			<xsl:choose>
			<xsl:when test="constant[. != '']">
				<th><p class="Tablecontents">Description</p></th>
			</xsl:when>
			<xsl:otherwise>
				<th/>
			</xsl:otherwise>
			</xsl:choose>
		</tr>
		
		<xsl:apply-templates select="constant">
			<xsl:with-param name="fStrings" select="@fStrings"/>
		</xsl:apply-templates>
	</table>
</xsl:template>

<xsl:template match="comment">
	<p class="Normal"><xsl:copy-of select="child::node()"/></p>
</xsl:template>

<xsl:template match="constant">
  <xsl:param name="fStrings"/>
	<tr>
		<xsl:choose>
		<xsl:when test="@ver">
			<td><span><xsl:attribute name="class">Highlight<xsl:value-of select="@ver"/></xsl:attribute><xsl:value-of select="@szName"/></span></td>
		</xsl:when>
		<xsl:otherwise>
			<td><xsl:value-of select="@szName"/></td>
		</xsl:otherwise>
		</xsl:choose>
		
		<xsl:choose>
		<xsl:when test="$fStrings">
			<td><xsl:value-of select="@szValue"/></td>
		</xsl:when>
		<xsl:otherwise>
			<td class="Value"><xsl:value-of select="@szValue"/></td>
		</xsl:otherwise>
		</xsl:choose>
		
		<td class="Last" style="text-align: justify;"><xsl:value-of select="."/></td>
		
	</tr>
</xsl:template>


<xsl:template name="SzLabelFromGroup">
  <xsl:param name="group"/>
	<xsl:choose>
	<xsl:when test="$group/@szLabel"><xsl:value-of select="$group/@szLabel"/></xsl:when>
	<xsl:when test="$group/@szHung"><xsl:value-of select="$group/@szHung"/></xsl:when>
	<xsl:otherwise></xsl:otherwise>
	</xsl:choose>
</xsl:template>


</xsl:stylesheet>
