<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  version="1.0">


<xsl:template match="schema">
#
// -------------------------------------------------------------------
// !!!!!!!!!!!!!!!!!!!!!!   R E A D   T H I S   !!!!!!!!!!!!!!!!!!!!!!
// -------------------------------------------------------------------
// !!!  This file is generated automatically by SkapiDefsCs.xsl.   !!!
// !!!    Any edits you make to it by hand WILL be overwritten.    !!!
// !!!         Make your changes to SkapiDefs.xml instead.         !!!
// -------------------------------------------------------------------
#
namespace SkapiDefs
{
<xsl:apply-templates select="group">
	<xsl:sort select="@szTitle"/>
</xsl:apply-templates>
}
#	
#
</xsl:template>

<xsl:template match="group">
	#
	// <xsl:if test="@szHung != ''"><xsl:value-of select="@szHung"/>: </xsl:if><xsl:value-of select="@szTitle"/><xsl:text>
	</xsl:text>
	class <xsl:value-of select="@szHung"/><xsl:if test="not(@szHung)"><xsl:value-of select="@szLabel"/></xsl:if>
	{
	<xsl:apply-templates select="comment"/>
	<xsl:apply-templates select="constant"/>
	};
	#
</xsl:template>

<xsl:template match="comment">
	// <xsl:value-of select="."/><xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="constant">
	<xsl:choose>
	<xsl:when test="@szValue != ''">
		
		<xsl:variable name="ty"><xsl:choose>
		<xsl:when test="starts-with(@szValue, '&quot;')">string</xsl:when>
		<xsl:otherwise>int</xsl:otherwise>
		</xsl:choose></xsl:variable>
		
		<xsl:variable name="szLinePrefix">public const <xsl:value-of select="$ty"/><xsl:text> </xsl:text><xsl:value-of select="@szName"/></xsl:variable>
		
		<xsl:value-of select="$szLinePrefix"/><xsl:call-template 
		  name="SzSpaces"><xsl:with-param name="cch" select="48 - string-length($szLinePrefix)"
		  /></xsl:call-template>=<xsl:call-template 
		  name="SzSpaces"><xsl:with-param name="cch" select="28 - string-length(@szValue)"
		  /></xsl:call-template><xsl:value-of select="@szValue"/>;<xsl:if test=". != ''">	// <xsl:value-of select="."/></xsl:if><xsl:text>
		</xsl:text>
		
	</xsl:when>
	<xsl:otherwise>
		
		<xsl:variable name="szLinePrefix">// <xsl:value-of select="@szName"/></xsl:variable>
		
		<xsl:value-of select="$szLinePrefix"/><xsl:if test=". != ''"><xsl:call-template 
		  name="SzSpaces"><xsl:with-param name="cch" select="79 - string-length($szLinePrefix)"
		  /></xsl:call-template>// <xsl:value-of select="."/></xsl:if><xsl:text>
		</xsl:text>
		
	</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template name="SzSpaces"><xsl:param name="cch"/><xsl:value-of 
    select="substring('                                                                                     ', 
    1, $cch)"/></xsl:template>


</xsl:stylesheet>
