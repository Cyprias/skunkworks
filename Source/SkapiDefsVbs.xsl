<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:msxsl="urn:schemas-microsoft-com:xslt"
  version="1.0">


<xsl:template match="schema">
Option Explicit
#
' -------------------------------------------------------------------
' !!!!!!!!!!!!!!!!!!!!!!   R E A D   T H I S   !!!!!!!!!!!!!!!!!!!!!!
' -------------------------------------------------------------------
' !!!  This file is generated automatically by SkapiDefsVbs.xsl.  !!!
' !!!    Any edits you make to it by hand WILL be overwritten.    !!!
' !!!         Make your changes to SkapiDefs.xml instead.         !!!
' -------------------------------------------------------------------
#
<xsl:apply-templates select="group">
	<xsl:sort select="@szTitle"/>
</xsl:apply-templates>
#	
#
</xsl:template>

<xsl:template match="group">
	#
	' <xsl:if test="@szHung != ''"><xsl:value-of select="@szHung"/>: </xsl:if><xsl:value-of select="@szTitle"/><xsl:text>
	</xsl:text>
	<xsl:apply-templates select="comment"/>
	<xsl:apply-templates select="constant"/>
	#
</xsl:template>

<xsl:template match="comment">
	' <xsl:value-of select="."/><xsl:text>
	</xsl:text>
</xsl:template>

<xsl:template match="constant">
	<xsl:choose>
	<xsl:when test="@szValue != ''">
		
		<xsl:variable name="szLinePrefix">Public Const <xsl:value-of select="@szName"/></xsl:variable>
		<xsl:variable name="szValue"><xsl:call-template 
		  name="SzVBConst"><xsl:with-param name="sz" select="@szValue"
		  /></xsl:call-template></xsl:variable>
		
		<xsl:value-of select="$szLinePrefix"/><xsl:call-template 
		  name="SzSpaces"><xsl:with-param name="cch" select="41 - string-length($szLinePrefix)"
		  /></xsl:call-template>=<xsl:call-template 
		  name="SzSpaces"><xsl:with-param name="cch" select="28 - string-length($szValue)"
		  /></xsl:call-template><xsl:value-of select="$szValue"/><xsl:if test=". != ''">    ' <xsl:value-of select="."/></xsl:if><xsl:text>
		</xsl:text>
		
	</xsl:when>
	<xsl:otherwise>
		
		<xsl:variable name="szLinePrefix">' <xsl:value-of select="@szName"/></xsl:variable>
		
		<xsl:value-of select="$szLinePrefix"/><xsl:if test=". != ''"><xsl:call-template 
		  name="SzSpaces"><xsl:with-param name="cch" select="74 - string-length($szLinePrefix)"
		  /></xsl:call-template>' <xsl:value-of select="."/></xsl:if><xsl:text>
		</xsl:text>
		
	</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template name="SzVBConst">
  <xsl:param name="sz"/>
	<xsl:choose>
	<xsl:when test="starts-with($sz, '0x')">&amp;H<xsl:value-of select="substring-after($sz, '0x')"/>&amp;</xsl:when>
	<xsl:otherwise><xsl:value-of select="$sz"/></xsl:otherwise>
	</xsl:choose>
</xsl:template>

<xsl:template name="SzSpaces"><xsl:param name="cch"/><xsl:value-of 
    select="substring('                                                                                     ', 
    1, $cch)"/></xsl:template>


</xsl:stylesheet>
