VERSION 5.00
Begin VB.Form formSkapiTest 
   Caption         =   "SkapiTest"
   ClientHeight    =   4710
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5565
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "SkapiTest.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   4710
   ScaleWidth      =   5565
   Begin VB.Frame frameMsgFile 
      Caption         =   " Message file "
      Height          =   855
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   5295
      Begin VB.CommandButton cmdStart 
         Caption         =   "Start"
         Height          =   315
         Left            =   4440
         TabIndex        =   4
         Top             =   360
         Width           =   735
      End
      Begin SkapiTest.MrulistCtl mrulistMsgs 
         Height          =   315
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   4215
         _ExtentX        =   7435
         _ExtentY        =   556
         szApp           =   "SkunkWorks"
         szFilter        =   "Capture files|*.acmsg"
      End
   End
   Begin VB.TextBox textCommands 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1215
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   3360
      Width           =   5295
   End
   Begin VB.Timer timerPoll 
      Interval        =   50
      Left            =   5160
      Top             =   840
   End
   Begin VB.TextBox textOutput 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2175
      Left            =   120
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   1080
      Width           =   5295
   End
End
Attribute VB_Name = "formSkapiTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
' Rudimentary SkunkWorks emulator for debugging SkunkWorks's message decoding
' logic without having to fire up the full AC/Decal/SkunkWorks ensemble.


'--------------
' Private data
'--------------

Private WithEvents swsrv As SWSrvCls
Attribute swsrv.VB_VarHelpID = -1
Private fRetry As Boolean, fQuit As Boolean


'----------
' Controls
'----------

Private Sub Form_Load()
    
    Set swoptions = New SWOptionsCls
    
'   Restore previous window size/position.
    Call RestoreFormPosition(Me, "", True, swoptions.szAppName)
'   Force a Resize event in the case when the size doesn't actually change
'   so that controls get laid out properly the first time.
    Call Form_Resize
    
    Set swsrv = New SWSrvCls
    Call swsrv.Attach
    
End Sub

Private Sub Form_Resize()
    
    If ScaleWidth > 0 Then
    '   Lay out controls to conform to new window size.
        Dim dxMargin As Long, dyMargin As Long
        dxMargin = textOutput.Left
        dyMargin = dxMargin
        
        Call frameMsgFile.Move(dxMargin, dyMargin, ScaleWidth - dxMargin * 2)
        
        cmdStart.Left = frameMsgFile.Width - dxMargin - cmdStart.Width
        Call mrulistMsgs.Move(dxMargin, mrulistMsgs.Top, _
          cmdStart.Left - dxMargin * 2)
        
        Dim yOutput As Long, yLimCommands As Long
        yOutput = frameMsgFile.Top + frameMsgFile.Height + dyMargin
        yLimCommands = ScaleHeight - dyMargin
        
        Dim dyAvail As Long, dyOutput As Long, dyCommands As Long
        dyAvail = yLimCommands - yOutput - dyMargin
        dyOutput = dyAvail * 2 / 3
        dyCommands = dyAvail * 1 / 3
        
        Call textOutput.Move(dxMargin, yOutput, _
          ScaleWidth - dxMargin * 2, dyOutput)
        
        Call textCommands.Move(dxMargin, yLimCommands - dyCommands, _
          ScaleWidth - dxMargin * 2, dyCommands)
        
    '   Save window size/position to registry.
        Call SaveFormPosition(Me, "", swoptions.szAppName)
    End If
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    
'   Save window size/position to registry.
    Call SaveFormPosition(Me, "", swoptions.szAppName)
    fQuit = True
    
End Sub


' Spool captured events from a file to SkunkWorks.
Private Sub cmdStart_Click()
    On Error GoTo LError
    
    cmdStart.Enabled = False
    
    textOutput.Text = ""
    textCommands.Text = ""
    
    Call swsrv.LaunchConsole
    
'   Open the capture file specified by the dropdown mrulist.
    Dim szFile As String
    szFile = mrulistMsgs.szFile
    Dim fn As Long
    fn = FreeFile
    Open szFile For Binary Access Read As fn
    
    Dim signature As Long, version As Long
    Get fn, , signature
    Get fn, , version
    If signature <> signatureCapture Then
    '   Old format without header or timestamps.
        Seek fn, 1
        version = 0
    End If
    
    Dim rgbMsg() As Byte
    Dim lfaError As Long: lfaError = -1
    
'   Read messages until EOF.
    Do Until EOF(fn)
        Dim tick As Long, cb As Long
        If version >= 1 Then
            Get fn, , tick
        Else
            tick = GetTickCount()
        End If
        Get fn, , cb
        If cb > 0 Then
        '   Read message data from file and queue it for Skapi.dll.
            ReDim rgbMsg(cb - 1)
            Get fn, , rgbMsg
            
            Do
                fRetry = False
                Call swsrv.QueueEventRgb(rgbMsg, cb, tick)
                If Not fRetry Then Exit Do
                Call Sleep(100)
                DoEvents
                If fQuit Then Exit Sub
            Loop
        End If
    Loop
    
    Close fn
    
    If lfaError = -1 Then
        Call AppendLine(textOutput, "End of file.")
        Call Beep
    Else
        Call MsgBox("Bad file format at offset " + SzHex(lfaError) + ".", _
          vbExclamation + vbOKOnly)
    End If
    
    GoTo LExit
    
LError:
    Call MsgBox(Err.Source + ":" + vbCrLf + Err.Description, vbExclamation + vbOKOnly)
    
LExit:
    cmdStart.Enabled = True
    
End Sub


Private Sub timerPoll_Timer()
    
'   Poll the SkunkWorks command queue.
    Call swsrv.Poll
    
End Sub


'--------------
' SWSrv events
'--------------

Private Sub swsrv_AddChatText(ByVal szText As String, ByVal lColor As Long, ByVal lTarget As Long)
    
    Call LogCmd("AddChatText", SzQuote(szText) + ", " + SzHex(lColor, 8) + ", " + SzHex(lTarget, 8))
    
End Sub

Private Sub swsrv_AddChatTextRaw(ByVal szText As String, ByVal lColor As Long, ByVal lTarget As Long)
    
    Call LogCmd("AddChatTextRaw", SzQuote(szText) + ", " + SzHex(lColor, 8) + ", " + SzHex(lTarget, 8))
    
End Sub

Private Sub swsrv_AddStatusText(ByVal szText As String)
    
    Call LogCmd("AddStatusText", SzQuote(szText))
    
End Sub

Private Sub swsrv_ApplyItem(ByVal UseThis As Long, ByVal OnThis As Long)
    
    Call LogCmd("ApplyItem", SzHex(UseThis, 8) + ", " + SzHex(OnThis, 8))
    
End Sub

Private Sub swsrv_CastSpell(ByVal lSpellID As Long, ByVal lObjectID As Long)
    
    Call LogCmd("CastSpell", SzHex(lSpellID, 8) + ", " + SzHex(lObjectID, 8))
    
End Sub

Private Sub swsrv_DropItem(ByVal lObjectID As Long)
    
    Call LogCmd("DropItem", SzHex(lObjectID, 8))
    
End Sub

Private Sub swsrv_FaceHeading(ByVal fHeading As Single, ByVal bUnknown As Boolean)
    
    Call LogCmd("FaceHeading", CStr(fHeading) + ", " + CStr(bUnknown))
    
End Sub

Private Sub swsrv_GiveItem(ByVal lObject As Long, ByVal lDestination As Long)
    
    Call LogCmd("GiveItem", SzHex(lObject, 8) + ", " + SzHex(lDestination, 8))
    
End Sub

Private Sub swsrv_IDQueueAdd(ByVal lObjectID As Long)
    
    Call LogCmd("IDQueueAdd", SzHex(lObjectID, 8))
    
End Sub

Private Sub swsrv_InvokeChatParser(ByVal szText As String)
    
    Call LogCmd("InvokeChatParser", SzQuote(szText))
    
End Sub

Private Sub swsrv_Logout()
    
    Call LogCmd("Logout")
    
End Sub

Private Sub swsrv_MoveItem(ByVal lObjectID As Long, ByVal lPackID As Long, ByVal lSlot As Long, ByVal bStack As Boolean)
    
    Call LogCmd("MoveItem", SzHex(lObjectID, 8) + ", " + SzHex(lPackID, 8) + ", " + SzHex(lSlot, 8) + ", " + CStr(bStack))
    
End Sub

Private Sub swsrv_MoveItemEx(ByVal lObjectID As Long, ByVal lDestinationID As Long)
    
    Call LogCmd("MoveItemEx", SzHex(lObjectID, 8) + ", " + SzHex(lDestinationID, 8))
    
End Sub

Private Sub swsrv_MoveItemExRaw(ByVal lObject As Long, ByVal lDestination As Long, ByVal lMoveFlags As Long)
    
    Call LogCmd("MoveItemExRaw", SzHex(lObject, 8) + ", " + SzHex(lDestination, 8) + ", " + SzHex(lMoveFlags, 8))
    
End Sub

Private Sub swsrv_RaiseAttribute(ByVal AttribID As Long, ByVal lExperience As Long)
    
    Call LogCmd("RaiseAttribute", CStr(AttribID) + ", " + CStr(lExperience))
    
End Sub

Private Sub swsrv_RaiseSkill(ByVal SkillID As Long, ByVal lExperience As Long)
    
    Call LogCmd("RaiseSkill", CStr(SkillID) + ", " + CStr(lExperience))
    
End Sub

Private Sub swsrv_RaiseVital(ByVal VitalID As Long, ByVal lExperience As Long)
    
    Call LogCmd("RaiseVital", CStr(VitalID) + ", " + CStr(lExperience))
    
End Sub

Private Sub swsrv_RequestID(ByVal lObjectID As Long)
    
    Call LogCmd("RequestID", SzHex(lObjectID, 8))
    
End Sub

Private Sub swsrv_SalvagePanelAdd(ByVal lObjectID As Long)
    
    Call LogCmd("SalvagePanelAdd", SzHex(lObjectID, 8))
    
End Sub

Private Sub swsrv_SalvagePanelSalvage()
    
    Call LogCmd("SalvagePanelSalvage")
    
End Sub

Private Sub swsrv_SelectItem(ByVal lObjectID As Long)
    
    Call LogCmd("SelectItem", SzHex(lObjectID, 8))
    
End Sub

Private Sub swsrv_SetAutorun(ByVal bOnOff As Boolean)
    
    Call LogCmd("SetAutorun", CStr(bOnOff))
    
End Sub

Private Sub swsrv_SetCombatMode(ByVal pVal As Long)
    
    Call LogCmd("SetCombatMode", SzHex(pVal, 8))
    
End Sub

Private Sub swsrv_SetCurrentSelection(ByVal pVal As Long)
    
    Call LogCmd("SetCurrentSelection", SzHex(pVal, 8))
    
End Sub

Private Sub swsrv_SetCursorPosition(ByVal lX As Long, ByVal lY As Long)
    
    Call LogCmd("SetCursorPosition", SzHex(lX, 8) + ", " + SzHex(lY, 8))
    
End Sub

Private Sub swsrv_SetIdleTime(ByVal dIdleTimeout As Double)
    
    Call LogCmd("SetIdleTime", CStr(dIdleTimeout))
    
End Sub

Private Sub swsrv_SetPreviousSelection(ByVal pVal As Long)
    
    Call LogCmd("SetPreviousSelection", SzHex(pVal, 8))
    
End Sub

Private Sub swsrv_SetSelectedStackCount(ByVal pVal As Long)
    
    Call LogCmd("SetSelectedStackCount", SzHex(pVal, 8))
    
End Sub

Private Sub swsrv_SpellTabAdd(ByVal lTab As Long, ByVal lIndex As Long, ByVal lSpellID As Long)
    
    Call LogCmd("SpellTabAdd", SzHex(lTab, 8) + ", " + SzHex(lIndex, 8) + ", " + SzHex(lSpellID, 8))
    
End Sub

Private Sub swsrv_SpellTabDelete(ByVal lTab As Long, ByVal lSpellID As Long)
    
    Call LogCmd("SpellTabDelete", SzHex(lTab, 8) + ", " + SzHex(lSpellID, 8))
    
End Sub

Private Sub swsrv_TradeAccept()
    
    Call LogCmd("TradeAccept")
    
End Sub

Private Sub swsrv_TradeAdd(ByVal ItemID As Long)
    
    Call LogCmd("TradeAdd", SzHex(ItemID, 8))
    
End Sub

Private Sub swsrv_TradeEnd()
    
    Call LogCmd("TradeEnd")
    
End Sub

Private Sub swsrv_TradeDecline()
    
    Call LogCmd("TradeDecline")
    
End Sub

Private Sub swsrv_TradeReset()
    
    Call LogCmd("TradeReset")
    
End Sub

Private Sub swsrv_UseItem(ByVal lObjectID As Long, ByVal lUseState As Long)
    
    Call LogCmd("UseItem", SzHex(lObjectID, 8) + ", " + SzHex(lUseState, 8))
    
End Sub

Private Sub swsrv_UseItemRaw(ByVal lObjectID As Long, ByVal lUseState As Long, ByVal lUseMethod As Long)
    
    Call LogCmd("UseItemRaw", SzHex(lObjectID, 8) + ", " + SzHex(lUseState, 8) + ", " + SzHex(lUseMethod, 8))
    
End Sub

Private Sub swsrv_VendorBuyAll()
    
    Call LogCmd("VendorBuyAll")
    
End Sub

Private Sub swsrv_VendorBuyListAdd(ByVal lID As Long, ByVal lAmount As Long)
    
    Call LogCmd("VendorBuyListAdd", SzHex(lID, 8) + ", " + SzHex(lAmount, 8))
    
End Sub

Private Sub swsrv_VendorBuyListClear()
    
    Call LogCmd("VendorBuyListClear")
    
End Sub

Private Sub swsrv_VendorSellAll()
    
    Call LogCmd("VendorSellAll")
    
End Sub

Private Sub swsrv_VendorSellListAdd(ByVal lID As Long)
    
    Call LogCmd("VendorSellListAdd", SzHex(lID, 8))
    
End Sub

Private Sub swsrv_VendorSellListClear()
    
    Call LogCmd("VendorSellListClear")
    
End Sub


Private Sub swsrv_ButtonEvent(ByVal ibutton As Long, ByVal kmo As Long)
    
    Call LogCmd("ButtonEvent", CStr(ibutton) + ", " + CStr(kmo))
    
End Sub

Private Sub swsrv_CallPanelFunction(ByVal szPanel As String, ByVal szFunction As String, v1 As Variant, v2 As Variant, v3 As Variant, v4 As Variant, v5 As Variant)
    
    Call LogCmd("CallPanelFunction", SzQuote(szPanel) + ", " + SzQuote(szFunction))
    
End Sub

Private Sub swsrv_CancelNav()
    
    Call LogCmd("CancelNav")
    
End Sub

Private Sub swsrv_DisplaySz(ByVal sz As String, ByVal cmc As Long, ByVal iwnd As Long)
    
    If sz = vbLf Then
        Call AppendSz(textOutput, vbCrLf)
    Else
        Call AppendSz(textOutput, sz)
    End If
    
End Sub

Private Sub swsrv_EnableHotkey(ByVal sz As String, ByVal vk As Long, ByVal fEnable As Boolean)
    
    Call LogCmd("EnableHotkey", SzQuote(sz) + ", " + SzHex(vk, 4) + ", " + CStr(fEnable))
    
End Sub

Private Sub swsrv_GetControlProperty(ByVal szPanel As String, ByVal szControl As String, _
  ByVal szProperty As String)
    
    Call LogCmd("GetControlProperty", _
      SzQuote(szPanel) + ", " + SzQuote(szControl) + ", " + SzQuote(szProperty))
    
End Sub

Private Sub swsrv_GoToLatLng(ByVal lat As Single, ByVal lng As Single, ByVal distArrive As Single, _
  ByVal cmsecTimeout As Long, ByVal nopt As Long, ByVal fRunAsDefault As Boolean)
    
    Call LogCmd("GoToLatLng", FormatNumber(lat, 3) + ", " + FormatNumber(lng, 3) + ", " + _
      FormatNumber(distArrive, 3) + ", " + CStr(cmsecTimeout) + ", " + _
      SzHex(nopt, 4) + ", " + CStr(fRunAsDefault))
    
End Sub

Private Sub swsrv_MouseMove(ByVal xp As Long, ByVal yp As Long)
    
    Call LogCmd("MouseMove", CStr(xp) + ", " + CStr(yp))
    
End Sub

Private Sub swsrv_ReloadOptions()
    
    Call LogCmd("ReloadOptions")
    
End Sub

Private Sub swsrv_RemoveControls(ByVal szPanel As String)
    
    Call LogCmd("RemoveControls", SzQuote(szPanel))
    
End Sub

Private Sub swsrv_ScriptMsg(ByVal szMsg As String)
    
    Call LogCmd("ScriptMsg", SzQuote(szMsg))
    
End Sub

Private Sub swsrv_SetControlProperty(ByVal szPanel As String, _
  ByVal szControl As String, ByVal szProperty As String, ByVal vValue As Variant)
    
    Call LogCmd("SetControlProperty", _
      SzQuote(szPanel) + ", " + SzQuote(szControl) + ", " + _
      SzQuote(szProperty) + ", " + CStr(vValue))
    
End Sub

Private Sub swsrv_ShowControls(ByVal szViewSchema As String, ByVal fActivate As Boolean, _
  ByVal szSkapiDir As String)
    
    Call LogCmd("ShowControls", _
      SzQuote(szViewSchema) + ", " + CStr(fActivate) + ", " + SzQuote(szSkapiDir))
    
End Sub

Private Sub swsrv_TurnToHead(ByVal head As Single)
    
    Call LogCmd("TurnToHead", FormatNumber(head, 1))
    
End Sub

Private Sub swsrv_VkEvent(ByVal vk As Long, ByVal kmo As Long)
    
    Call LogCmd("VkEvent", SzHex(vk, 4) + ", " + CStr(kmo))
    
End Sub


Private Sub swsrv_Disconnect()
    
    Call LogCmd("Disconnect")
    
End Sub

Private Sub swsrv_QueueFull(ByVal fConnected As Boolean)
    
    fRetry = fConnected
    
End Sub


'----------
' Privates
'----------

Private Sub LogCmd(ByVal szCmd As String, Optional ByVal szArgs As String = "")
    
    Call AppendLine(textCommands, szCmd + "(" + szArgs + ")")
    
End Sub

' Append a line of text to the given TextBox control.
Private Sub AppendLine(tb As TextBox, ByVal szLine As String)
    Call AppendSz(tb, szLine + vbCrLf)
End Sub

Private Sub AppendSz(tb As TextBox, ByVal sz As String)
    
    Dim szNew As String
    szNew = tb.Text + sz
    tb.Text = szNew
    tb.SelStart = Len(tb.Text)
    tb.SelLength = 0
    
End Sub




























