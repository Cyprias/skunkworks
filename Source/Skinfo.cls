VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SkinfoCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit
' Skill/Attribute information
' This class is exposed to the via skapi.SkinfoFromSkid.


'--------------
' Private data
'--------------

' Skill formula (e.g. (Coordination + Focus)/3):
Private Type SkfmType
    attr1 As Long
    attr2 As Long
    divisor As Long
End Type

Private szNameMbr As String
Private szDescMbr As String
Private lvlDefaultMbr As Long
Private dlvlMbr As Long
Private dlvlFreeMbr As Long
Private dlvlSpellsMbr As Long
Private expInvestedMbr As Double
Private sktsMbr As Long
Private skfmMbr As SkfmType
Private exptab As ExptabCls


'------------
' Properties
'------------

Public Property Get szName() As String
    szName = szNameMbr
End Property

Public Property Get szDesc() As String
    szDesc = szDescMbr
End Property

' Return the number of levels gained in this skill or attribute since
' character creation.
Public Property Get dlvl() As Long
    dlvl = dlvlMbr
End Property

' Return your default skill level in this skill or attribute.  This is the
' raw skill level given by the standard skill formula for this skill (e.g.
' (Focus + Self)/4 or whatever), and is the skill level you would have if
' you had never practiced or improved this skill.
Public Property Get lvlDefault() As Long
    
    If lvlDefaultMbr >= 0 Then
        lvlDefault = lvlDefaultMbr
    ElseIf sktsMbr > sktsUnusable Then
        lvlDefault = Int( _
          (self.AlvlUnbuffedFromAttr(skfmMbr.attr1) + _
          self.AlvlUnbuffedFromAttr(skfmMbr.attr2)) / _
          skfmMbr.divisor + 0.5)
    Else
        lvlDefault = 0
    End If
    
End Property

' Return your unbuffed skill level in this skill or attribute.
Public Property Get lvlUnbuffed() As Long
    lvlUnbuffed = lvlDefault + dlvlFreeMbr + dlvlMbr
End Property

' Return your actual current skill level in this skill or attribute,
' including buffs, debuffs, and penalties currently in effect.
Public Property Get lvlCur() As Long
    
    If lvlDefaultMbr >= 0 Then
        lvlCur = lvlDefaultMbr + dlvlFreeMbr + dlvlMbr + dlvlSpellsMbr
    ElseIf sktsMbr > sktsUnusable Then
        lvlCur = Int( _
          (self.AlvlCurFromAttr(skfmMbr.attr1) + _
          self.AlvlCurFromAttr(skfmMbr.attr2)) / _
          skfmMbr.divisor + 0.5)
        lvlCur = Round((lvlCur + dlvlFreeMbr + dlvlMbr) * self.fractVitae + 0.01) + dlvlSpellsMbr
    Else
        lvlCur = dlvlSpellsMbr
    End If
    
End Property

' Return the total amount of experience invested in this skill.
Public Property Get expInvested() As Double
    expInvested = expInvestedMbr
End Property

' Return the training status of this skill, or sktsUntrained if it's an attribute.
Public Property Get skts() As Long
    skts = sktsMbr
End Property


' Return the first underlying attribute of this skill or vital.
Public Property Get attr1() As Long
    attr1 = skfmMbr.attr1
End Property

' Return the second underlying attribute of this skill or vital, or attrNil if 
' it's a single-attribute skill.
Public Property Get attr2() As Long
    attr2 = skfmMbr.attr2
End Property

' Return the skill formula divisor for this skill or vital.
Public Property Get divisor() As Long
    divisor = skfmMbr.divisor
End Property


'---------
' Methods
'---------

' Return the amount of additional experience needed to reach the given skill level.
Public Function DexpToLvl(Optional ByVal lvlTarget As Long = -1) As Double
    
    Dim dlvlTarget As Long
    If lvlTarget <> -1 Then
        dlvlTarget = lvlTarget - lvlDefault - dlvlFreeMbr
    Else
        dlvlTarget = dlvlMbr + 1
    End If
    
    If exptab Is Nothing Then
        DexpToLvl = -1
    ElseIf dlvlTarget <= dlvlMbr Then
        DexpToLvl = 0
    Else
        Dim expTarget As Double
        expTarget = exptab.rgexp(dlvlTarget)
        If expTarget >= 0 Then
            DexpToLvl = expTarget - expInvestedMbr
        Else
            DexpToLvl = -1
        End If
    End If
    
End Function

' Return the total amount of experience needed to reach the given skill level.
Public Function ExpFromLvl(ByVal lvl As Long) As Double

    If exptab Is Nothing Then
        ExpFromLvl = -1
    Else
        Dim expTarget As Double
        expTarget = exptab.rgexp(lvl - lvlDefault - dlvlFreeMbr)
        If expTarget >= 0 Then
            ExpFromLvl = expTarget
        Else
            ExpFromLvl = -1
        End If
    End If

End Function

Public Function LvlFromExp(ByVal exp As Double) As Long

    If exptab Is Nothing Then
        LvlFromExp = -1
    Else
        LvlFromExp = lvlDefault + dlvlFreeMbr + exptab.LvlFromExp(exp)
    End If

End Function


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------

Friend Sub Init(ByVal szName As String, ByVal szDesc As String, _
  ByVal attr1 As Long, ByVal attr2 As Long, ByVal divisor As Long)
    
    szNameMbr = szName
    szDescMbr = szDesc
    
    skfmMbr.attr1 = attr1
    skfmMbr.attr2 = attr2
    skfmMbr.divisor = divisor
    
End Sub

Friend Sub SetAtinfo(ByVal avd As AVDataCls)
    
    lvlDefaultMbr = avd.lvlOrig
    dlvlMbr = avd.dlvl
    dlvlFreeMbr = 0
    expInvestedMbr = avd.expInvested
    sktsMbr = sktsUntrained
    Set exptab = portaldat.exptabAttr
    
End Sub

Friend Sub SetVtinfo(ByVal avd As AVDataCls)
    
    lvlDefaultMbr = -1
    dlvlMbr = avd.dlvl
    dlvlFreeMbr = 0
    expInvestedMbr = avd.expInvested
    sktsMbr = sktsUntrained
    Set exptab = portaldat.exptabVital
    
End Sub

Friend Sub SetSkinfo(ByVal sd As SkillDataCls)
    
    lvlDefaultMbr = -1
    dlvlMbr = sd.dlvl
    dlvlFreeMbr = sd.dlvlFree
    expInvestedMbr = sd.expInvested
    sktsMbr = sd.skts
    If sd.skts = sktsTrained Then
        Set exptab = portaldat.exptabTrain
    ElseIf sd.skts = sktsSpecialized Then
        Set exptab = portaldat.exptabSpec
    Else
        Set exptab = Nothing
    End If
    
End Sub

Friend Sub SetSkts(ByVal skts As Long)
    
    sktsMbr = skts
    
End Sub

Friend Sub SetDlvlSpells(ByVal dlvl As Long)
    dlvlSpellsMbr = dlvl
End Sub


'----------
' Privates
'----------









