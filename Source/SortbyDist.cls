VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SortbyDistCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


Public sortbyNext As Object
Public maplocFrom As MaplocCls


Private dictAcoDist As Dictionary


Public Function CompareAcos(ByVal aco1 As AcoCls, ByVal aco2 As AcoCls) As Long
    
    Dim dist1 As Single, dist2 As Single
    dist1 = DistFromAco(aco1)
    dist2 = DistFromAco(aco2)
    If dist1 < dist2 Then
        CompareAcos = -1
    ElseIf dist1 > dist2 Then
        CompareAcos = 1
    ElseIf sortbyNext Is Nothing Then
        CompareAcos = 0
    Else
        CompareAcos = sortbyNext.CompareAcos(aco1, aco2)
    End If
    
End Function


Private Function DistFromAco(ByVal aco As AcoCls) As Single
    
    If dictAcoDist.Exists(aco) Then
        DistFromAco = dictAcoDist(aco)
    Else
        If aco.maploc Is Nothing Then
            DistFromAco = 0
        Else
            DistFromAco = maplocFrom.Dist3DToMaploc(aco.maploc)
        End If
        dictAcoDist(aco) = DistFromAco
    End If
    
End Function



Private Sub Class_Initialize()
    
    Set dictAcoDist = New Dictionary
    Set maplocFrom = skapi.maplocCur
    
End Sub

Private Sub Class_Terminate()
    
    Set dictAcoDist = Nothing
    
End Sub
