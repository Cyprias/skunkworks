VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SortbyValueCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


Public sortbyNext As Object


Public Function CompareAcos(ByVal aco1 As AcoCls, ByVal aco2 As AcoCls) As Long
    
    CompareAcos = aco1.cpyValue - aco2.cpyValue
    If CompareAcos = 0 And Not (sortbyNext Is Nothing) Then
        CompareAcos = sortbyNext.CompareAcos(aco1, aco2)
    End If
    
End Function




