VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SpellCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


'--------------
' Private data
'--------------

Private spellidMbr As Long
Private szNameMbr As String
Private szDescMbr As String
Private skidMbr As Long
Private familyMbr As Long
Private diffMbr As Long
Private manaMbr As Long
Private rangeMinMbr As Single
Private rangePerLvlMbr As Single
Private csecDurationMbr As Long

Private layerMbr As Long
Private flagsMbr As Long
Private avsAffectedMbr As Long
Private dlvlMbr As Single
Private acoCasterMbr As AcoCls
Private timerMbr As TimerCls


'------------
' Properties
'------------

Public Property Get spellid() As Long
    spellid = spellidMbr
End Property

Public Property Get szName() As String
    szName = szNameMbr
End Property

Public Property Get szDesc() As String
    szDesc = szDescMbr
End Property

Public Property Get skid() As Long
    skid = skidMbr
End Property

Public Property Get family() As Long
    family = familyMbr
End Property

Public Property Get diff() As Long
    diff = diffMbr
End Property

Public Property Get mana() As Long
    mana = manaMbr
End Property

Public Property Get range() As Single
    
'   Calculate spell range from skill level.
    range = (rangeMinMbr + rangePerLvlMbr * self.SkinfoFromSkid(skidMbr).dlvl)
    
'   Range tops out at 75 meters.
    If range > 75 Then range = 75
    
'   Convert to map units.
    range = range / 240
    
End Property

Public Property Get csecDuration() As Long
    csecDuration = csecDurationMbr
End Property


Public Property Get layer() As Long
    layer = layerMbr
End Property

Public Property Get flags() As Long
    flags = flagsMbr
End Property

Public Property Get avsAffected() As Long
    avsAffected = avsAffectedMbr
End Property

Public Property Get dlvl() As Single
    dlvl = dlvlMbr
End Property

Public Property Get acoCaster() As AcoCls
    Set acoCaster = acoCasterMbr
End Property

Public Property Get csecElapsed() As Long
    If timerMbr Is Nothing Then
        csecElapsed = -1
    Else
        csecElapsed = timerMbr.cmsec / 1000
    End If
End Property

Public Property Get csecRemain() As Long
    If timerMbr Is Nothing Then
        csecRemain = -1
    ElseIf csecDurationMbr > 0 Then
        csecRemain = csecDurationMbr - csecElapsed
        If csecRemain < 0 Then csecRemain = 0
    Else
        csecRemain = csecDurationMbr
    End If
End Property


'---------
' Methods
'---------


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------

Friend Sub FromEnchantment(ByVal spellid As Long, ByVal layer As Long, _
  ByVal family As Long, ByVal diff As Long, ByVal flags As Long, _
  ByVal avsAffected As Long, ByVal dlvl As Single, ByVal acoCaster As AcoCls, _
  ByVal csecDuration As Double, ByVal csecElapsed As Double)
    
    Dim spellT As SpellCls
    Set spellT = portaldat.SpellFromSpellid(spellid)
    
    spellidMbr = spellid
    szNameMbr = spellT.szName
    szDescMbr = spellT.szDesc
    skidMbr = spellT.skid
    familyMbr = family
    diffMbr = diff
    manaMbr = spellT.mana
    rangeMinMbr = spellT.rangeMin
    rangePerLvlMbr = spellT.rangePerLvl
    csecDurationMbr = csecDuration
    
    layerMbr = layer
    flagsMbr = flags
    avsAffectedMbr = avsAffected
    dlvlMbr = dlvl
    Set acoCasterMbr = acoCaster
    Set timerMbr = New TimerCls
    
'   csecElapsed can get arbitrarily large if the user hasn't taken off their armor
'   in a long time.  After three weeks or so of toon time this causes an overflow
'   here, so check for that case and pretend the elapsed time is zero.
    If csecElapsed < 1000000 Then
        timerMbr.cmsec = csecElapsed * 1000
    End If
    
End Sub

Friend Sub FromSpelldat(ByVal spellid As Long, _
  ByVal szName As String, ByVal szDesc As String, ByVal skid As Long, _
  ByVal family As Long, ByVal diff As Long, ByVal mana As Long, _
  ByVal rangeMin As Single, ByVal rangePerLvl As Single, ByVal csecDuration As Long)
    
    spellidMbr = spellid
    szNameMbr = szName
    szDescMbr = szDesc
    skidMbr = skid
    familyMbr = family
    diffMbr = diff
    manaMbr = mana
    rangeMinMbr = rangeMin
    rangePerLvlMbr = rangePerLvl
    csecDurationMbr = csecDuration
    
    layerMbr = 0
    flagsMbr = 0
    avsAffectedMbr = 0
    dlvlMbr = 0
    Set acoCasterMbr = Nothing
    Set timerMbr = Nothing
    
End Sub

Friend Function rangeMin() As Single
    rangeMin = rangeMinMbr
End Function

Friend Function rangePerLvl() As Single
    rangePerLvl = rangePerLvlMbr
End Function


'----------
' Privates
'----------












