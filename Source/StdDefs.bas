Attribute VB_Name = "StdDefs"
Option Explicit
' Miscellaneous definitions.


Public Const pi As Double = 3.14159


Public Const iNil As Long = -1


Public Const tNil As Date = 0
Public Const dtDay As Date = 1
Public Const dtHour As Date = dtDay / 24
Public Const dtHalfHour As Date = dtHour / 2
Public Const dtMinute As Date = dtHour / 60
Public Const dtSecond As Date = dtMinute / 60
Public Const dtMsec As Date = dtSecond / 1000


Public Enum PermType
    permYes
    permNo
    permAsk
End Enum


Public Function DwFromWords(ByVal wHigh As Long, ByVal wLow As Long) As Long
    
    wHigh = wHigh And &HFFFF&
    wLow = wLow And &HFFFF&
    
'   Avoid overflow when shifting negative numbers.
    If (wHigh And &H8000&) = 0 Then
        DwFromWords = wHigh * &H10000 Or wLow
    Else
        DwFromWords = (wHigh And &H7FFF&) * &H10000 Or wLow Or &H80000000
    End If
    
End Function

Public Function FFromV(ByVal v As Variant) As Boolean
    
    If IsNull(v) Then
        FFromV = False
    Else
        FFromV = CBool(v)
    End If
    
End Function
