Attribute VB_Name = "Strings"
Option Explicit
' String-handling functions.


Public Function CchMaxOfSz(sz As String) As Long
' Return max number of chars (not counting terminating null)
' that can be stored in sz.
    
    CchMaxOfSz = Len(sz) - 1
    
End Function

Public Function ClineInSz(ByVal sz As String) As Long
    
    Dim cline As Long
    cline = 0
    
    Dim ich As Long, cch As Long
    ich = 0
    cch = Len(sz)
    Do While ich < cch
        cline = cline + 1
        ich = IchInStr(ich, sz, vbLf)
        If ich = iNil Then
            ich = cch
        Else
            ich = ich + 1
        End If
    Loop
    
    ClineInSz = cline
End Function

Public Function FEqSz(ByVal sz1 As String, ByVal sz2 As String) As Boolean
    
    FEqSz = (StrComp(sz1, sz2, vbBinaryCompare) = 0)
    
End Function

Public Function FEqSzI(ByVal sz1 As String, ByVal sz2 As String) As Boolean
    
    FEqSzI = (StrComp(sz1, sz2, vbTextCompare) = 0)
    
End Function

Public Function FHexSz(ByVal sz As String) As Boolean
    
    Dim ich As Long
    For ich = 0 To Len(sz) - 1
        Dim szCh As String
        szCh = SzMid(sz, ich, 1)
        If IchInStr(0, "0123456789ABCDEF", UCase(szCh)) = iNil Then
            FHexSz = False
            Exit Function
        End If
    Next ich
    
    FHexSz = True
End Function

Public Function FNumericSz(ByVal sz As String) As Boolean
    
    Dim ich As Long
    For ich = 0 To Len(sz) - 1
        Dim szCh As String
        szCh = SzMid(sz, ich, 1)
        If Not (szCh >= "0" And szCh <= "9") Then
            FNumericSz = False
            Exit Function
        End If
    Next ich
    
    FNumericSz = True
End Function

Public Function FPrefixMatchSzI(ByVal sz As String, ByVal szPrefix As String) As Boolean
    
    Dim cchPrefix As Long
    cchPrefix = Len(szPrefix)
    If cchPrefix <= Len(sz) Then
        FPrefixMatchSzI = FEqSzI(Left(sz, cchPrefix), szPrefix)
    Else
        FPrefixMatchSzI = False
    End If
    
End Function

Public Function FSuffixMatchSzI(ByVal sz As String, ByVal szSuffix As String) As Boolean
    
    Dim cchSuffix As Long
    cchSuffix = Len(szSuffix)
    If cchSuffix <= Len(sz) Then
        FSuffixMatchSzI = FEqSzI(Right(sz, cchSuffix), szSuffix)
    Else
        FSuffixMatchSzI = False
    End If
    
End Function

Public Function IchInStr(ByVal ichFirst As Long, ByVal sz1 As String, ByVal sz2 As String) As Long
'   Zero-origin version of InStr

    IchInStr = InStr(1 + ichFirst, sz1, sz2) - 1
    
End Function

Public Function IchInStrRev(ByVal sz1 As String, ByVal sz2 As String, _
  Optional ByVal ichLim As Long = iNil) As Long
'   Zero-origin version of InStrRev
    
    If ichLim = iNil Then ichLim = Len(sz1)
    
    If ichLim > 0 Then
        IchInStrRev = InStrRev(sz1, sz2, ichLim) - 1
    Else
        IchInStrRev = iNil
    End If
    
End Function

Public Function LExtractField(ByVal szLine As String, ByVal ifield As Long, _
  Optional szSep As String = ",") As Long
    
    LExtractField = LFromSzEmpty(SzExtractField(szLine, ifield, szSep))
    
End Function

Public Function LFromSzEmpty(ByVal sz As String) As Long
    
    If sz <> "" Then
        LFromSzEmpty = CLng(sz)
    Else
        LFromSzEmpty = 0
    End If
    
End Function

Public Function LFromSzHex(ByVal sz As String) As Long
    
    LFromSzHex = CLng("&H" + sz)
    
End Function

Public Sub SplitSz(ByRef szHead As String, ByRef szTail As String, _
  ByVal sz As String, ByVal szSep As String)
    
    Dim ich As Long
    ich = IchInStr(0, sz, szSep)
    If ich >= 0 Then
        szHead = Left(sz, ich)
        szTail = Right(sz, Len(sz) - (ich + Len(szSep)))
        If szSep = " " Then szTail = SzTrimWhiteSpace(szTail)
    Else
        szHead = sz
        szTail = ""
    End If
    
End Sub

Public Sub SplitSzRev(ByRef szHead As String, ByRef szTail As String, _
  ByVal sz As String, ByVal szSep As String)
    
    Dim ich As Long
    ich = IchInStrRev(sz, szSep)
    If ich >= 0 Then
        szHead = Left(sz, ich)
        szTail = Right(sz, Len(sz) - (ich + Len(szSep)))
        If szSep = " " Then szHead = SzTrimWhiteSpace(szHead)
    Else
        szHead = ""
        szTail = sz
    End If
    
End Sub

Public Function SzFixHex(ByVal sz As String) As String
    
    If FPrefixMatchSzI(sz, "0x") Then
        SzFixHex = "&H" + Right(sz, Len(sz) - 2)
    Else
        SzFixHex = sz
    End If
    
End Function

Public Function SzGetAppVersion() As String
    SzGetAppVersion = CStr(App.Major) + "." + CStr(App.Minor) + "." + CStr(App.Revision)
End Function

' Extract the (zero-origin) ifield'th field from a line of tab- or comma-separated fields.
' szSep specifies the field separator.
Public Function SzExtractField(ByVal szLine As String, ByVal ifield As Long, _
  Optional szSep As String = ",") As String
    
    Dim ifieldT As Long, ichFirst As Long, ichLim As Long
    
    ifieldT = 0
    ichFirst = 0
    Do
        ichLim = IchInStr(ichFirst, szLine, szSep)
        If ichLim = -1 Then
            If ifieldT < ifield Then
                SzExtractField = ""
                Exit Function
            End If
            ichLim = Len(szLine)
        End If
        If ifieldT = ifield Then Exit Do
        ifieldT = ifieldT + 1
        ichFirst = ichLim + 1
    Loop
    
    SzExtractField = SzTrimWhiteSpace(SzMid(szLine, ichFirst, ichLim - ichFirst))
    
End Function

Public Function SzFixCase(ByVal sz As String) As String
    
    If sz = "" Then
        SzFixCase = ""
        Exit Function
    End If
    
    Dim szUCase As String, szLCase As String
    szUCase = UCase(sz)
    szLCase = LCase(sz)
    
    If szUCase = sz Or szLCase = sz Then
    '   String is all uppercase or all lowercase.
    '   Change first char to uppercase, rest to lowercase.
        SzFixCase = Left(szUCase, 1) + Right(szLCase, Len(sz) - 1)
    Else
    '   String is mixed case.
    '   Change first char to uppercase and leave the rest alone.
        SzFixCase = Left(szUCase, 1) + Right(sz, Len(sz) - 1)
    End If
    
End Function

Public Function SzFromVNull(v As Variant) As String
    
    If IsNull(v) Then
        SzFromVNull = ""
    Else
        SzFromVNull = CStr(v)
    End If
    
End Function

Public Function SzHex(ByVal l As Long, _
  Optional ByVal cdigit As Long = 0) As String
    
    Dim sz As String
    sz = Hex(l)
    If Len(sz) < cdigit Then sz = String(cdigit - Len(sz), "0") & sz
    SzHex = sz
    
End Function

Public Function SzMid(ByVal sz As String, ByVal ich As Long, _
  Optional ByVal cch As Long = 1) As String
'   Zero-origin version of Mid
    
    SzMid = Mid(sz, ich + 1, cch)
    
End Function

Public Function SzNThings(ByVal n As Long, _
  ByVal szSingular As String, ByVal szPlural As String) As String
    
    SzNThings = CStr(n) + " " + IIf(n = 1, szSingular, szPlural)
    
End Function

Public Function SzPadLeft(ByVal sz As String, ByVal cch As Long) As String
    
    Dim cchSz As Long
    cchSz = Len(sz)
    If cchSz < cch Then
        SzPadLeft = Space(cch - cchSz) + sz
    Else
        SzPadLeft = sz
    End If
    
End Function

Public Function SzPadRight(ByVal sz As String, ByVal cch As Long) As String
    
    Dim cchSz As Long
    cchSz = Len(sz)
    If cchSz < cch Then
        SzPadRight = sz + Space(cch - cchSz)
    Else
        SzPadRight = sz
    End If
    
End Function

Public Function SzQuote(ByVal sz As String) As String
    
    SzQuote = """" + sz + """"

End Function

Public Function SzQuoteIfNonEmpty(ByVal sz As String) As String
    
    If Len(sz) > 0 Then
        SzQuoteIfNonEmpty = SzQuote(sz)
    Else
        SzQuoteIfNonEmpty = sz
    End If

End Function

Public Function SzStripBrackets(ByVal sz As String, ByVal szLeft As String, ByVal szRight As String) As String
    
    If FPrefixMatchSzI(sz, szLeft) Then
        sz = Right(sz, Len(sz) - Len(szLeft))
    End If
    
    If FSuffixMatchSzI(sz, szRight) Then
        sz = Left(sz, Len(sz) - Len(szRight))
    End If
    
    SzStripBrackets = SzTrimWhiteSpace(sz)
    
End Function

Public Function SzStripQuotes(ByVal sz As String) As String
    
    If Len(sz) >= 2 Then
        If Left(sz, 1) = """" Then
            sz = Right(sz, Len(sz) - 1)
            If Right(sz, 1) = """" Then
                sz = Left(sz, Len(sz) - 1)
            End If
        ElseIf Left(sz, 1) = "'" Then
            sz = Right(sz, Len(sz) - 1)
            If Right(sz, 1) = "'" Then
                sz = Left(sz, Len(sz) - 1)
            End If
        End If
    End If
    
    SzStripQuotes = sz
End Function

Public Function SzTrimNulls(ByVal sz As String) As String
    
    Dim ich As Long
    ich = IchInStr(0, sz, vbNullChar)
    If ich <> iNil Then
        SzTrimNulls = Left(sz, ich)
    Else
        SzTrimNulls = sz
    End If
    
End Function

Public Function SzTrimWhiteSpace(ByVal sz As String) As String
    
    Dim ichFirst As Long, ichLim As Long
    For ichFirst = 0 To Len(sz) - 1
        If SzMid(sz, ichFirst, 1) <> " " And _
           SzMid(sz, ichFirst, 1) <> vbTab And _
           SzMid(sz, ichFirst, 1) <> vbCr And _
           SzMid(sz, ichFirst, 1) <> vbLf Then
            Exit For
        End If
    Next ichFirst
    
    For ichLim = Len(sz) To ichFirst + 1 Step -1
        If SzMid(sz, ichLim - 1, 1) <> " " And _
           SzMid(sz, ichLim - 1, 1) <> vbTab And _
           SzMid(sz, ichLim - 1, 1) <> vbCr And _
           SzMid(sz, ichLim - 1, 1) <> vbLf Then
            Exit For
        End If
    Next ichLim
    
    SzTrimWhiteSpace = SzMid(sz, ichFirst, ichLim - ichFirst)
End Function


Public Function SzFromRgbAsciz(rgbAsciz() As Byte, _
  Optional ByVal ib As Long = 0) As String
    
    Dim sz As String
    sz = ""
    
    Do While ib < UBound(rgbAsciz) And rgbAsciz(ib) <> 0
        sz = sz + Chr(rgbAsciz(ib))
        ib = ib + 1
    Loop
    
    SzFromRgbAsciz = sz
End Function

Public Sub RgbAscizFromSz(rgbAsciz() As Byte, ByVal ib As Long, _
  ByVal sz As String)
    
    Dim ich As Long
    For ich = 0 To Len(sz) - 1
        rgbAsciz(ib + ich) = Asc(SzMid(sz, ich, 1))
    Next ich
    
    rgbAsciz(ib + Len(sz)) = 0
    
End Sub


Public Function SzFromLNil(ByVal l As Long) As String
    
    If l <> iNil Then
        SzFromLNil = CStr(l)
    Else
        SzFromLNil = ""
    End If
    
End Function

Public Function LFromSzNil(ByVal sz As String) As Long
    
    If sz <> "" Then
        LFromSzNil = CLng(sz)
    Else
        LFromSzNil = iNil
    End If
    
End Function























