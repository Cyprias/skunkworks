VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form formSwoptions 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Console Options"
   ClientHeight    =   2790
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6150
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Swoptions.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2790
   ScaleWidth      =   6150
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame frameLoginScript 
      Caption         =   " Login Script "
      Height          =   1815
      Left            =   240
      TabIndex        =   2
      Top             =   240
      Width           =   5655
      Begin VB.CommandButton cmdBrowse 
         Caption         =   "..."
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5160
         TabIndex        =   6
         Top             =   1305
         Width           =   330
      End
      Begin VB.TextBox textLoginScript 
         Height          =   285
         Left            =   360
         TabIndex        =   5
         Top             =   1320
         Width           =   4815
      End
      Begin VB.OptionButton rgoptLoginScript 
         Caption         =   "Run this script on login:"
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   4
         Top             =   1080
         Width           =   5415
      End
      Begin VB.OptionButton rgoptLoginScript 
         Caption         =   "Do not run a script on login"
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   840
         Width           =   5415
      End
      Begin VB.Label labelLoginScript 
         Caption         =   "When you log a character into the game, SkunkWorks can automatically run a script for you if there isn't one running already."
         Height          =   495
         Left            =   240
         TabIndex        =   7
         Top             =   360
         Width           =   5175
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4836
      TabIndex        =   1
      Top             =   2280
      Width           =   1095
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   3600
      TabIndex        =   0
      Top             =   2280
      Width           =   1095
   End
   Begin MSComDlg.CommonDialog comdlg 
      Left            =   5760
      Top             =   0
      _ExtentX        =   688
      _ExtentY        =   688
      _Version        =   393216
   End
End
Attribute VB_Name = "formSwoptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


Private Sub Form_Load()
    
    textLoginScript.Text = swoptions.szLoginScript
    If swoptions.szLoginScript <> "" Then
        rgoptLoginScript(1).Value = True
    Else
        rgoptLoginScript(0).Value = True
    End If
    
End Sub

Private Sub rgoptLoginScript_Click(Index As Integer)
    
    If rgoptLoginScript(1).Value Then
        textLoginScript.Enabled = True
        textLoginScript.BackColor = vbWindowBackground
        If Visible Then Call SelectFilename
    Else
        textLoginScript.Enabled = False
        textLoginScript.BackColor = vbButtonFace
    End If
    
End Sub

Private Sub cmdBrowse_Click()
    
    Dim szFile As String
    comdlg.DialogTitle = "Choose Login Script"
    szFile = textLoginScript.Text
    If Not FGetSzFileOpen(szFile, comdlg, SzFilterFromSzLang("", True, False), "") Then
        Exit Sub
    End If
    
    textLoginScript.Text = szFile
    rgoptLoginScript(1).Value = True
    Call SelectFilename
    
End Sub

Private Sub cmdOK_Click()
    
    If rgoptLoginScript(1).Value Then
        swoptions.szLoginScript = textLoginScript.Text
    Else
        swoptions.szLoginScript = ""
    End If
    
    Call Unload(Me)
    
End Sub

Private Sub cmdCancel_Click()
    
    Call Unload(Me)
    
End Sub


Private Sub SelectFilename()
    
    textLoginScript.SelStart = Len(textLoginScript.Text)
    textLoginScript.SelLength = 0
    Call textLoginScript.SetFocus
    
End Sub

