#ifndef _SWXSCRIPTCP_H_
#define _SWXSCRIPTCP_H_

template <class T>
class CProxy_ISwxSiteEvents : public IConnectionPointImpl<T, &DIID__ISwxSiteEvents, CComDynamicUnkArray>
{
	//Warning this class may be recreated by the wizard.
public:
	VOID Fire_ScriptError(BSTR strFile, LONG iline, BSTR strSrc, BSTR strDesc, LONG errcode)
	{
		T* pT = static_cast<T*>(this);
		int nConnectionIndex;
		CComVariant* pvars = new CComVariant[5];
		int nConnections = m_vec.GetSize();
		
		for (nConnectionIndex = 0; nConnectionIndex < nConnections; nConnectionIndex++)
		{
			pT->Lock();
			CComPtr<IUnknown> sp = m_vec.GetAt(nConnectionIndex);
			pT->Unlock();
			IDispatch* pDispatch = reinterpret_cast<IDispatch*>(sp.p);
			if (pDispatch != NULL)
			{
				pvars[4] = strFile;
				pvars[3] = iline;
				pvars[2] = strSrc;
				pvars[1] = strDesc;
				pvars[0] = errcode;
				DISPPARAMS disp = { pvars, NULL, 5, 0 };
				pDispatch->Invoke(0x1, IID_NULL, LOCALE_USER_DEFAULT, DISPATCH_METHOD, &disp, NULL, NULL, NULL);
			}
		}
		delete[] pvars;
	
	}
};
#endif