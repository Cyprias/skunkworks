
SwxScriptps.dll: dlldata.obj SwxScript_p.obj SwxScript_i.obj
	link /dll /out:SwxScriptps.dll /def:SwxScriptps.def /entry:DllMain dlldata.obj SwxScript_p.obj SwxScript_i.obj \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		$<

clean:
	@del SwxScriptps.dll
	@del SwxScriptps.lib
	@del SwxScriptps.exp
	@del dlldata.obj
	@del SwxScript_p.obj
	@del SwxScript_i.obj
