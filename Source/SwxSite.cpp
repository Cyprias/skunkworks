// SwxSite.cpp : Implementation of CSwxSite
#include "stdafx.h"
#include "SwxScript.h"
#include "SwxSite.h"


//	Script Debugger support:
CComPtr< IProcessDebugManager >	g_pdm = NULL;
CComPtr< IDebugApplication >	g_pDebugApp = NULL;
CComPtr< IDebugDocumentHelper >	g_pDebugDocHelper = NULL;
DWORD							g_dwAppCookie = 0;
DWORD							g_crefPdm = 0;


void __cdecl ThreadProcSwxSite(void* lpParam);


/////////////////////////////////////////////////////////////////////////////
// CSwxSite

STDMETHODIMP CSwxSite::InterfaceSupportsErrorInfo(REFIID riid)
	{
	static const IID* arr[] = 
		{
		&IID_ISwxSite
		};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
		{
		if (IsEqualGUID(*arr[i], riid))
			return S_OK;
		}
	return S_FALSE;
	}

STDMETHODIMP CSwxSite::Begin(/*[in]*/ BSTR strLang, /*[in]*/ BSTR strScriptName, 
  /*[in]*/ BOOL fDebug, /*[in]*/ BSTR strEventInterrupt)
	{
	USES_CONVERSION;
	if (m_fTrace) TraceEnter("CSwxSite::Begin(\"%s\", \"%s\", %u, \"%s\")", 
		OLE2T(strLang), OLE2T(strScriptName), fDebug, OLE2T(strEventInterrupt));
	if (m_fTrace) TraceLine("g_crefPdm = %u", g_crefPdm);
	
	HRESULT hres;
	TCHAR szError[1000];
	
	try
		{
	//	Script Debugger support:
		if (fDebug)
			{
			if (g_pdm == NULL)
				{
			//	Create a ProcessDebugManager.
				if (m_fTrace) TraceLine("CoCreateInstance(CLSID_ProcessDebugManager)");
				hres = CoCreateInstance(CLSID_ProcessDebugManager, NULL, 
				  CLSCTX_INPROC_SERVER | CLSCTX_LOCAL_SERVER, IID_IProcessDebugManager,
				  (void**)&g_pdm);
				if (SUCCEEDED(hres))
					{
				//	Create a DebugApplication.
					if (m_fTrace) TraceLine("g_pdm->CreateApplication");
					hres = g_pdm->CreateApplication(&g_pDebugApp);
					_ASSERTE(SUCCEEDED(hres));
					
					if (m_fTrace) TraceLine("g_pDebugApp->SetName");
					hres = g_pDebugApp->SetName(L"SkunkWorks");
					_ASSERTE(SUCCEEDED(hres));
					
				//	Register the DebugApp with the debugger.
					if (m_fTrace) TraceLine("g_pdm->AddApplication");
					hres = g_pdm->AddApplication(g_pDebugApp, &g_dwAppCookie);
					_ASSERTE(SUCCEEDED(hres));
					
					if (m_fTrace) TraceLine("g_pdm->CreateDebugDocumentHelper");
					hres = g_pdm->CreateDebugDocumentHelper(NULL, &g_pDebugDocHelper);
					_ASSERTE(SUCCEEDED(hres));
					
				//	Create a root DebugDocHelper.
					if (m_fTrace) TraceLine("g_pDebugDocHelper->Init");
					hres = g_pDebugDocHelper->Init(g_pDebugApp, strScriptName, strScriptName, TEXT_DOC_ATTR_READONLY);
					_ASSERTE(SUCCEEDED(hres));
					
					if (m_fTrace) TraceLine("g_pDebugDocHelper->Attach");
					hres = g_pDebugDocHelper->Attach(NULL);
					_ASSERTE(SUCCEEDED(hres));
					
					g_crefPdm = 1;
					}
				}
			else
				g_crefPdm++;
			}
		if (m_fTrace) TraceLine("g_crefPdm = %u", g_crefPdm);
		
	//	Get the CLSID for the specified scripting language.
		if (m_fTrace) TraceLine("CLSIDFromProgID");
		CLSID clsidScript;
		hres = ::CLSIDFromProgID(strLang, &clsidScript);
		if (!SUCCEEDED(hres))
			{
			::_sntprintf(szError, 1000, _T("Unknown scripting language '%s'."), OLE2T(strLang));
			if (m_fTrace) TraceExit("CSwxSite::Begin (error)");
			return Error(szError);
			}
		
	//	Create the script engine.
		if (m_fTrace) TraceLine("CoCreateInstance");
		hres = ::CoCreateInstance(clsidScript, NULL, CLSCTX_INPROC_SERVER, 
		  IID_IActiveScript, reinterpret_cast< void ** >(&m_pScript));
		_ASSERTE(SUCCEEDED(hres));
		
	//	Hook it up to our host site.
		if (m_fTrace) TraceLine("SetScriptSite");
		hres = m_pScript->SetScriptSite(this);
		_ASSERTE(SUCCEEDED(hres));
		
	//	Get its parser interface for parsing script text.
		if (m_fTrace) TraceLine("m_pScript->QueryInterface");
		hres = m_pScript->QueryInterface(&m_pParse);
		_ASSERTE(SUCCEEDED(hres));
		
		if (m_fTrace) TraceLine("m_pParse->InitNew");
		hres = m_pParse->InitNew();
		_ASSERTE(SUCCEEDED(hres));
		
		if (_tcsclen(OLE2T(strEventInterrupt)) > 0)
			{
		//	Set up IPC events for the asynchronous Stop feature.
			m_heventStop = CreateEvent(NULL, true, false, OLE2T(strEventInterrupt));
			m_heventStopped = CreateEvent(NULL, false, false, NULL);
			m_hthreadStop = _beginthread(ThreadProcSwxSite, 0, (void*)this);
			ResetEvent(m_heventStop);
			}
		
		m_cGetTypeinfo = 0;
		}
	catch (TCHAR* pszError)
		{
		if (m_fTrace) TraceExit("CSwxSite::Begin (error)");
		return Error(pszError);
		}
	catch (...)
		{
		if (m_fTrace) TraceExit("CSwxSite::Begin (error)");
		return Error(_T("Unknown error."));
		}
	
	if (m_fTrace) TraceLine("g_crefPdm = %u", g_crefPdm);
	if (m_fTrace) TraceExit("CSwxSite::Begin");
	return S_OK;
	}

STDMETHODIMP CSwxSite::AddObject(/*[in]*/ BSTR strName, /*[in]*/ IDispatch *pobj, 
								 /*[in]*/ BOOL fGlobalMembers)
	{
	USES_CONVERSION;
	if (m_fTrace) TraceEnter("CSwxSite::AddObject(\"%s\")", OLE2T(strName));
	
	HRESULT hres;
	
	try
		{
		CNobj* pnobj = new CNobj(strName, pobj);
		pnobj->m_pnobjNext = m_pnobjHead;
		m_pnobjHead = pnobj;
		
		if (m_fTrace) TraceLine("AddNamedItem");
		hres = m_pScript->AddNamedItem(strName, 
		  SCRIPTITEM_ISVISIBLE | 
		  (fGlobalMembers ? SCRIPTITEM_GLOBALMEMBERS : 0));
		_ASSERTE(SUCCEEDED(hres));
		}
	catch (TCHAR* pszError)
		{
		if (m_fTrace) TraceExit("CSwxSite::AddObject (error)");
		return Error(pszError);
		}
	catch (...)
		{
		if (m_fTrace) TraceExit("CSwxSite::AddObject (error)");
		return Error(_T("Unknown error."));
		}
	
	if (m_fTrace) TraceExit("CSwxSite::AddObject");
	return S_OK;
	}

STDMETHODIMP CSwxSite::AddCode(/*[in]*/ BSTR strCode, /*[in]*/ BSTR strFilename, /*[in]*/ long ilineFirst)
	{
	USES_CONVERSION;
	if (m_fTrace) TraceEnter("CSwxSite::AddCode(\"%s\")", OLE2T(strFilename));
	
	HRESULT hres;
	
	try
		{
		if (cfile >= cfileMax)
			{
			if (m_fTrace) TraceExit("CSwxSite::AddCode (error)");
			return Error(_T("Too many script files."));
			}
		
	//	Script Debugger support:
		CComPtr< IDebugDocumentHelper >	ddh = NULL;
		DWORD dwContext = cfile;
		if (g_pdm != NULL)
			{
		//	Create a DebugDocHelper for this script file.
			if (m_fTrace) TraceLine("g_pdm->CreateDebugDocumentHelper");
			hres = g_pdm->CreateDebugDocumentHelper(NULL, &ddh);
			_ASSERTE(SUCCEEDED(hres));
			
			if (m_fTrace) TraceLine("ddh->Init");
			hres = ddh->Init(g_pDebugApp, strFilename, strFilename, TEXT_DOC_ATTR_READONLY);
			_ASSERTE(SUCCEEDED(hres));
			
		//	Hook it up to the root DDH.
			if (m_fTrace) TraceLine("ddh->Attach");
			hres = ddh->Attach(g_pDebugDocHelper);
			_ASSERTE(SUCCEEDED(hres));
			
		//	Feed the script text to the debugger.
			if (m_fTrace) TraceLine("ddh->AddDBCSText");
			hres = ddh->AddDBCSText(OLE2T(strCode));
			_ASSERTE(SUCCEEDED(hres));
			
		//	Define a block and get its debug context ID.
			if (m_fTrace) TraceLine("ddh->DefineScriptBlock");
			hres = ddh->DefineScriptBlock(0, strlen(OLE2T(strCode)), m_pScript, 
			  FALSE, &dwContext);
			_ASSERTE(SUCCEEDED(hres));
			if (m_fTrace) TraceLine("dwContext = 0x%08X", dwContext);
			}
		
		rgddh[cfile] = ddh;
		rgdwContext[cfile] = dwContext;
		rgstrFile[cfile] = _bstr_t(strFilename).copy();
		rgilineFirst[cfile] = ilineFirst;
		cfile++;
		
	//	Feed the script text to the script engine.
		if (m_fTrace) TraceLine("ParseScriptText");
		EXCEPINFO ei;
		hres = m_pParse->ParseScriptText(strCode, NULL, NULL, NULL, dwContext, 0, 
		  SCRIPTITEM_ISVISIBLE | (g_pdm != NULL ? SCRIPTTEXT_HOSTMANAGESSOURCE : 0), 
		  NULL, &ei);
		}
	catch (TCHAR* pszError)
		{
		if (m_fTrace) TraceExit("CSwxSite::AddCode (error)");
		return Error(pszError);
		}
	catch (...)
		{
		if (m_fTrace) TraceExit("CSwxSite::AddCode (error)");
		return Error(_T("Unknown error."));
		}
	
	if (m_fTrace) TraceExit("CSwxSite::AddCode");
	return S_OK;
	}

STDMETHODIMP CSwxSite::OnScriptError(IActiveScriptError *pase)
	{
	if (m_fTrace) TraceEnter("CSwxSite::OnScriptError");
	
	USES_CONVERSION;
	
	EXCEPINFO ei;
	DWORD dwContext;
	DWORD ifile;
	ULONG iline;
	LONG ich;
//	CComBSTR strLine;
	
	pase->GetExceptionInfo(&ei);
	pase->GetSourcePosition(&dwContext, &iline, &ich);
	ifile = IfileFromDwContext(dwContext);
//	pase->GetSourceLineText(&strLine);
	
	if (m_fTrace)
		{
		TCHAR szError[1000];
		::_sntprintf(szError, 1000, _T("%s: %s at %s line %u."), 
		  OLE2T(ei.bstrSource), OLE2T(ei.bstrDescription), 
		  OLE2T(rgstrFile[ifile]), rgilineFirst[ifile] + iline);
		TraceLine(szError);
		}
	
	if (m_fTrace) TraceLine("Fire_ScriptError");
	Fire_ScriptError(rgstrFile[ifile], rgilineFirst[ifile] + iline, 
	  ei.bstrSource, ei.bstrDescription, ei.wCode ? ei.wCode : ei.scode);
	
	if (m_fTrace) TraceExit("CSwxSite::OnScriptError");
	return S_OK;
	}

STDMETHODIMP CSwxSite::Run(/*[in]*/ BOOL fBreakOnEntry)
	{
	if (m_fTrace) TraceEnter("CSwxSite::Run(%u)", fBreakOnEntry);
	
	HRESULT hres;
	
	try
		{
		if (fBreakOnEntry && g_pDebugApp != NULL)
			{
			if (m_fTrace) TraceLine("g_pDebugApp->CauseBreak");
			hres = g_pDebugApp->CauseBreak();
			_ASSERTE(SUCCEEDED(hres));
			}
		
		if (m_fTrace) TraceLine("SetScriptState(STARTED)");
		hres = m_pScript->SetScriptState(SCRIPTSTATE_STARTED);
		if (m_fTrace) TraceHresult("m_pScript->SetScriptState", hres);
		_ASSERTE(SUCCEEDED(hres));
		
		if (m_fTrace) TraceLine("SetScriptState(CONNECTED)");
		hres = m_pScript->SetScriptState(SCRIPTSTATE_CONNECTED);
		if (m_fTrace) TraceHresult("m_pScript->SetScriptState", hres);
		_ASSERTE(SUCCEEDED(hres));
		}
	catch (TCHAR* pszError)
		{
		if (m_fTrace) TraceExit("CSwxSite::Run (error)");
		return Error(pszError);
		}
	catch (...)
		{
		if (m_fTrace) TraceExit("CSwxSite::Run (error)");
		return Error(_T("Unknown error."));
		}
	
	if (m_fTrace) TraceExit("CSwxSite::Run");
	return S_OK;
	}

STDMETHODIMP CSwxSite::End()
	{
	if (m_fTrace) TraceEnter("CSwxSite::End");
	if (m_fTrace) TraceLine("g_crefPdm = %u", g_crefPdm);
	
	try
		{
		Close();
		
	//	Script Debugger support:
		if (g_pdm != NULL)
			{
			g_crefPdm--;
			if (g_crefPdm == 0)
				{
				if (g_pDebugDocHelper != NULL)
					{
					if (m_fTrace) TraceLine("g_pDebugDocHelper->Detach");
					g_pDebugDocHelper->Detach();
					g_pDebugDocHelper = NULL;
					}
				if (g_pDebugApp != NULL)
					{
					g_pDebugApp->Close();
					if (m_fTrace) TraceLine("g_pdm->RemoveApplication");
					g_pdm->RemoveApplication(g_dwAppCookie);
					g_pDebugApp = NULL;
					g_dwAppCookie = 0;
					}
				g_pdm = NULL;
				}
			}
		}
	catch (TCHAR* pszError)
		{
		if (m_fTrace) TraceExit("CSwxSite::End (error)");
		return Error(pszError);
		}
	catch (...)
		{
		if (m_fTrace) TraceExit("CSwxSite::End (error)");
		return Error(_T("Unknown error."));
		}
	
	if (m_fTrace) TraceLine("g_crefPdm = %u", g_crefPdm);
	if (m_fTrace) TraceExit("CSwxSite::End");
	return S_OK;
	}


STDMETHODIMP CSwxSite::Stop()
	{
	if (m_fTrace) TraceEnter("CSwxSite::Stop");
	
	try
		{
		if (m_fTrace) TraceLine("SetEvent(m_heventStop)");
		SetEvent(m_heventStop);
		if (m_fTrace) TraceLine("WaitForSingleObject(m_heventStopped)");
		WaitForSingleObject(m_heventStopped, 1000);
		}
	catch (TCHAR* pszError)
		{
		if (m_fTrace) TraceExit("CSwxSite::Stop (error)");
		return Error(pszError);
		}
	catch (...)
		{
		if (m_fTrace) TraceExit("CSwxSite::Stop (error)");
		return Error(_T("Unknown error."));
		}
	
	if (m_fTrace) TraceExit("CSwxSite::Stop");
	return S_OK;
	}

STDMETHODIMP CSwxSite::get_objScript(IDispatch **pVal)
	{
	if (m_fTrace) TraceEnter("CSwxSite::get_objScript");

	HRESULT hres;
	
	try
		{
		hres = m_pScript->GetScriptDispatch(NULL, pVal);
		_ASSERTE(SUCCEEDED(hres));
		}
	catch (TCHAR* pszError)
		{
		if (m_fTrace) TraceExit("CSwxSite::get_objScript (error)");
		return Error(pszError);
		}
	catch (...)
		{
		if (m_fTrace) TraceExit("CSwxSite::get_objScript (error)");
		return Error(_T("Unknown error."));
		}
	
	if (m_fTrace) TraceExit("CSwxSite::get_objScript");
	return S_OK;
	}

STDMETHODIMP CSwxSite::FFunctionDefined(/*[in]*/ BSTR strFunc, /*[out, retval]*/ BOOL *pfDefined)
	{
	USES_CONVERSION;
	if (m_fTrace) TraceEnter("CSwxSite::FFunctionDefined(\"%s\")", OLE2T(strFunc));
	
	HRESULT hres;
	
	try
		{
		CComPtr< IDispatch > pdisp;
		hres = m_pScript->GetScriptDispatch(NULL, &pdisp);
		_ASSERTE(SUCCEEDED(hres));
		
		OLECHAR* rgpsz[1];
		DISPID rgdispid[1];
		rgpsz[0] = strFunc;
		rgdispid[0] = DISPID_UNKNOWN;
		hres = pdisp->GetIDsOfNames(IID_NULL, rgpsz, 1, 
		  LOCALE_SYSTEM_DEFAULT, rgdispid);
		
		*pfDefined = (rgdispid[0] != DISPID_UNKNOWN);
		}
	catch (TCHAR* pszError)
		{
		if (m_fTrace) TraceExit("CSwxSite::FFunctionDefined (error)");
		return Error(pszError);
		}
	catch (...)
		{
		if (m_fTrace) TraceExit("CSwxSite::FFunctionDefined (error)");
		return Error(_T("Unknown error."));
		}
	
	if (m_fTrace) TraceExit("CSwxSite::FFunctionDefined: %s", 
		(*pfDefined ? "true" : "false"));
	return S_OK;
	}

STDMETHODIMP CSwxSite::get_hwndHost(long *pVal)
	{
	if (m_fTrace) TraceEnter("CSwxSite::get_hwndHost");
	
	*pVal = reinterpret_cast<long>(m_hwndHost);
	
	if (m_fTrace) TraceExit("CSwxSite::get_hwndHost");
	return S_OK;
	}

STDMETHODIMP CSwxSite::put_hwndHost(long newVal)
	{
	if (m_fTrace) TraceEnter("CSwxSite::put_hwndHost");
	
	m_hwndHost = reinterpret_cast<HWND>(newVal);
	
	if (m_fTrace) TraceExit("CSwxSite::put_hwndHost");
	return S_OK;
	}

STDMETHODIMP CSwxSite::get_fTrace(BOOL *pVal)
	{
	if (m_fTrace) TraceEnter("CSwxSite::get_fTrace");
	
	*pVal = m_fTrace;
	
	if (m_fTrace) TraceExit("CSwxSite::get_fTrace");
	return S_OK;
	}

STDMETHODIMP CSwxSite::put_fTrace(BOOL newVal)
	{
	if (m_fTrace) TraceEnter("CSwxSite::put_fTrace");
	
	m_fTrace = newVal;
	
	if (m_fTrace) TraceExit("CSwxSite::put_fTrace");
	return S_OK;
	}

STDMETHODIMP CSwxSite::get_fInterrupted(BOOL *pVal)
	{
	if (m_fTrace) TraceEnter("CSwxSite::get_fInterrupted");
	
	*pVal = m_fInterrupted;
	
	if (m_fTrace) TraceExit("CSwxSite::get_fInterrupted: %s", 
		(m_fInterrupted ? "true" : "false"));
	return S_OK;
	}


////////////////////////////////////
// IActiveScriptSiteDebug Methods //
////////////////////////////////////

// Used by the language engine to delegate IDebugCodeContext::GetSourceContext. 
STDMETHODIMP CSwxSite::GetDocumentContextFromPosition(
	/*[in]*/ DWORD dwContext,				// As provided to ParseScriptText
	/*[in]*/ ULONG uCharacterOffset,		// character offset relative 
											// to start of script block or scriptlet 
	/*[in]*/ ULONG uNumChars,				// Number of characters in context 
	/*[out]*/ IDebugDocumentContext** ppsc)	// Returns the document context corresponding 
											// to this character-position range. 
	{
	if (m_fTrace) TraceEnter("CSwxSite::GetDocumentContextFromPosition(0x%08X, %u, %u)", 
		dwContext, uCharacterOffset, uNumChars);
	
	HRESULT hres;
	
	try
		{
		DWORD ifile = IfileFromDwContext(dwContext);
		CComPtr< IDebugDocumentHelper >	ddh = rgddh[ifile];
		
		if (ddh == NULL)
			{
			if (m_fTrace) TraceExit("CSwxSite::GetDocumentContextFromPosition");
			return E_NOTIMPL;
			}
		
		ULONG ulStartPos = 0;
		if (m_fTrace) TraceLine("ddh->GetScriptBlockInfo");
		hres = ddh->GetScriptBlockInfo(dwContext, NULL, &ulStartPos, NULL);
		_ASSERTE(SUCCEEDED(hres));
		
		if (m_fTrace) TraceLine("ddh->CreateDebugDocumentContext");
		hres = ddh->CreateDebugDocumentContext(ulStartPos + uCharacterOffset, 
			uNumChars, ppsc);
		_ASSERTE(SUCCEEDED(hres));
		}
	catch (TCHAR* pszError)
		{
		if (m_fTrace) TraceExit("CSwxSite::GetDocumentContextFromPosition (error)");
		return Error(pszError);
		}
	catch (...)
		{
		if (m_fTrace) TraceExit("CSwxSite::GetDocumentContextFromPosition (error)");
		return Error(_T("Unknown error."));
		}
	
	if (m_fTrace) TraceExit("CSwxSite::GetDocumentContextFromPosition");
	return S_OK;
	}

// Returns the debug application object associated with this script site. Provides 
// a means for a smart host to define what application object each script belongs to. 
// Script engines should attempt to call this method to get their containing application 
// and resort to IProcessDebugManager::GetDefaultApplication if this fails. 
STDMETHODIMP CSwxSite::GetApplication(/*[out]*/ IDebugApplication** ppda)
	{
	if (m_fTrace) TraceEnter("CSwxSite::GetApplication");
	
	try
		{
		if (g_pDebugApp == NULL)
			{
			if (m_fTrace) TraceExit("CSwxSite::GetApplication");
			return E_NOTIMPL;
			}
		
		if (m_fTrace) TraceLine("g_pDebugApp.CopyTo");
		g_pDebugApp.CopyTo(ppda);
		}
	catch (TCHAR* pszError)
		{
		if (m_fTrace) TraceExit("CSwxSite::GetApplication (error)");
		return Error(pszError);
		}
	catch (...)
		{
		if (m_fTrace) TraceExit("CSwxSite::GetApplication (error)");
		return Error(_T("Unknown error."));
		}
	
	if (m_fTrace) TraceExit("CSwxSite::GetApplication");
	return S_OK;
	}

// Gets the application node under which script documents should be added 
// can return NULL if script documents should be top-level. 
STDMETHODIMP CSwxSite::GetRootApplicationNode(/*[out]*/ IDebugApplicationNode** ppdanRoot)
	{
	if (m_fTrace) TraceEnter("CSwxSite::GetRootApplicationNode");
	
	HRESULT hres;
	
	try
		{
		if (g_pDebugDocHelper == NULL)
			{
			if (m_fTrace) TraceExit("CSwxSite::GetRootApplicationNode");
			return E_NOTIMPL;
			}
		
		if (m_fTrace) TraceLine("g_pDebugDocHelper->GetDebugApplicationNode");
		hres = g_pDebugDocHelper->GetDebugApplicationNode(ppdanRoot);
		_ASSERTE(SUCCEEDED(hres));
		}
	catch (TCHAR* pszError)
		{
		if (m_fTrace) TraceExit("CSwxSite::GetRootApplicationNode (error)");
		return Error(pszError);
		}
	catch (...)
		{
		if (m_fTrace) TraceExit("CSwxSite::GetRootApplicationNode (error)");
		return Error(_T("Unknown error."));
		}
	
	if (m_fTrace) TraceExit("CSwxSite::GetRootApplicationNode");
	return S_OK;
	}

// Allows a smart host to control the handling of runtime errors 
STDMETHODIMP CSwxSite::OnScriptErrorDebug( 
	/*[in]*/ IActiveScriptErrorDebug *pErrorDebug,	// the runtime error that occurred
	/*[out]*/ BOOL*pfEnterDebugger,					// whether to pass the error to the 
													// debugger to do JIT debugging 
	/*[out]*/ BOOL *pfCallOnScriptErrorWhenContinuing)	// whether to call IActiveScriptSite::OnScriptError() 
													// when the user decides to continue without debugging
	{
	if (m_fTrace) TraceEnter("CSwxSite::OnScriptErrorDebug(0x%08X, 0x%08X)", 
		pfEnterDebugger, pfCallOnScriptErrorWhenContinuing);
	
	try
		{
		if (pfEnterDebugger)
			*pfEnterDebugger = TRUE;
		if (pfCallOnScriptErrorWhenContinuing)
			*pfCallOnScriptErrorWhenContinuing = TRUE;
		}
	catch (TCHAR* pszError)
		{
		if (m_fTrace) TraceExit("CSwxSite::OnScriptErrorDebug (error)");
		return Error(pszError);
		}
	catch (...)
		{
		if (m_fTrace) TraceExit("CSwxSite::OnScriptErrorDebug (error)");
		return Error(_T("Unknown error."));
		}
	
	if (m_fTrace) TraceExit("CSwxSite::OnScriptErrorDebug");
	return S_OK;
	}

DWORD CScriptSite::IfileFromDwContext(DWORD dwContext)
	{
	if (m_fTrace) TraceEnter("CScriptSite::IfileFromDwContext(0x%08X)", 
		dwContext);
	
	DWORD ifile;
	for (ifile = 0; ifile < cfile; ifile++)
		{
		if (rgdwContext[ifile] == dwContext)
			break;
		}
	if (ifile == cfile)
		ifile = 0;
	
	if (m_fTrace) TraceExit("CScriptSite::IfileFromDwContext: %d", ifile);
	return ifile;
	}


//////////////
// Privates //
//////////////

void __cdecl ThreadProcSwxSite(void* lpParam)
	{
	CSwxSite* pThis = (CSwxSite*)lpParam;
	
	WaitForSingleObject(pThis->m_heventStop, INFINITE);
	if (pThis->m_fTrace) pThis->TraceLine("ThreadProcSwxSite: Stop signaled.");
	
	if (pThis->m_pScript != NULL)
		{
		EXCEPINFO ei;
		ZeroMemory(&ei, sizeof(ei));
		
		if (pThis->m_fTrace) pThis->TraceLine("ThreadProcSwxSite: InterruptScriptThread");
		HRESULT hres = pThis->m_pScript->InterruptScriptThread(SCRIPTTHREADID_ALL, &ei, 0);
		_ASSERTE(SUCCEEDED(hres));
		
		pThis->m_fInterrupted = true;
		}
	
	SetEvent(pThis->m_heventStopped);
	
	_endthread();
	}


