// SwxSite.h : Declaration of the CSwxSite

#ifndef __SWXSITE_H_
#define __SWXSITE_H_

#include "resource.h"       // main symbols
#include "CScriptSite.h"
#include "SwxScriptCP.h"

/////////////////////////////////////////////////////////////////////////////
// CSwxSite
class ATL_NO_VTABLE CSwxSite : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CSwxSite, &CLSID_SwxSite>,
	public ISupportErrorInfo,
	public IConnectionPointContainerImpl<CSwxSite>,
	public IDispatchImpl<ISwxSite, &IID_ISwxSite, &LIBID_SWXSCRIPTLib>,
	public CScriptSite,
	public CProxy_ISwxSiteEvents< CSwxSite >
{
public:
	CSwxSite()
	{
	}
	
DECLARE_REGISTRY_RESOURCEID(IDR_SWXSITE)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CSwxSite)
	COM_INTERFACE_ENTRY(ISwxSite)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
	COM_INTERFACE_ENTRY(IActiveScriptSite)
	COM_INTERFACE_ENTRY(IActiveScriptSiteWindow)
	COM_INTERFACE_ENTRY(IActiveScriptSiteDebug)
	COM_INTERFACE_ENTRY_IMPL(IConnectionPointContainer)
END_COM_MAP()
BEGIN_CONNECTION_POINT_MAP(CSwxSite)
CONNECTION_POINT_ENTRY(DIID__ISwxSiteEvents)
END_CONNECTION_POINT_MAP()
	
	
// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);
	
// ISwxSite
public:
	STDMETHOD(FFunctionDefined)(/*[in]*/ BSTR strFunc, /*[out, retval]*/ BOOL *pfDefined);
	STDMETHOD(get_fInterrupted)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(get_fTrace)(/*[out, retval]*/ BOOL *pVal);
	STDMETHOD(put_fTrace)(/*[in]*/ BOOL newVal);
	STDMETHOD(get_hwndHost)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_hwndHost)(/*[in]*/ long newVal);
	STDMETHOD(get_objScript)(/*[out, retval]*/ IDispatch* *pVal);
	STDMETHOD(Begin)(/*[in]*/ BSTR strLang, /*[in]*/ BSTR strScriptName, /*[in]*/ BOOL fDebug, /*[in]*/ BSTR strEventInterrupt);
	STDMETHOD(AddObject)(/*[in]*/ BSTR strName, /*[in]*/ IDispatch *pobj, /*[in]*/ BOOL fGlobalMembers);
	STDMETHOD(AddCode)(/*[in]*/ BSTR strCode, /*[in]*/ BSTR strFilename, /*[in]*/ long ilineFirst);
	STDMETHOD(Run)(/*[in]*/ BOOL fBreakOnEntry);
	STDMETHOD(Stop)();
	STDMETHOD(End)();
	
// IActiveScriptSite
	STDMETHOD(OnScriptError)(IActiveScriptError *pase);
	
// IActiveScriptSiteDebug
	STDMETHOD(GetDocumentContextFromPosition)( 
	  /*[in]*/ DWORD dwSourceContext, /*[in]*/ ULONG uCharacterOffset, 
	  /*[in]*/ ULONG uNumChars, /*[out]*/ IDebugDocumentContext** ppsc);
	
	STDMETHOD(GetApplication)(/*[out]*/ IDebugApplication** ppda);
	
	STDMETHOD(GetRootApplicationNode)(/*[out]*/IDebugApplicationNode** ppdanRoot);
	
	STDMETHOD(OnScriptErrorDebug)(/*[in]*/ IActiveScriptErrorDebug* pErrorDebug, 
	  /*[out]*/ BOOL* pfEnterDebugger, /*[out]*/ BOOL* pfCallOnScriptErrorWhenContinuing);
};

#endif //__SWXSITE_H_
