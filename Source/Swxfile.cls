VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SwxfileCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


'--------------
' Private data
'--------------

Private szFileMbr As String
Private xmldoc As DOMDocument40
Private fDirtyMbr As Boolean

Private szFileSave As String
Private szXmlSave As String
Private fDirtySave As Boolean

Private Const fRenameDPTDft As Boolean = True
Private Const fRegisterStdHandlersDft As Boolean = True
Private Const fActivateACDft As Boolean = True

Private selEnum As IXMLDOMSelection


'------------
' Properties
'------------

Public Property Get szFile() As String
    szFile = szFileMbr
End Property

Public Property Get szLeafFile() As String
    szLeafFile = SzLeaf(szFileMbr)
End Property

Public Property Get szDir() As String
    szDir = SzDirFromPath(szFileMbr)
End Property


Public Property Get fRenameDPT() As Boolean
    fRenameDPT = FGetAttr(xmldoc.documentElement, "RenameDPT", fRenameDPTDft)
End Property

Public Property Let fRenameDPT(ByVal fNew As Boolean)
    If fNew <> fRenameDPT Then
        Call xmldoc.documentElement.setAttribute("RenameDPT", CStr(fNew))
        fDirtyMbr = True
    End If
End Property


Public Property Get fRegisterStdHandlers() As Boolean
    fRegisterStdHandlers = FGetAttr(xmldoc.documentElement, "RegisterStdHandlers", fRegisterStdHandlersDft)
End Property

Public Property Let fRegisterStdHandlers(ByVal fNew As Boolean)
    If fNew <> fRegisterStdHandlers Then
        Call xmldoc.documentElement.setAttribute("RegisterStdHandlers", CStr(fNew))
        fDirtyMbr = True
    End If
End Property


Public Property Get fActivateAC() As Boolean
    fActivateAC = FGetAttr(xmldoc.documentElement, "ActivateAC", fActivateACDft)
End Property

Public Property Let fActivateAC(ByVal fNew As Boolean)
    If fNew <> fActivateAC Then
        Call xmldoc.documentElement.setAttribute("ActivateAC", CStr(fNew))
        fDirtyMbr = True
    End If
End Property


Public Property Get szArgs() As String
    szArgs = SzFromVNull(xmldoc.documentElement.getAttribute("Arguments"))
End Property

Public Property Let szArgs(ByVal szNew As String)
    If szNew <> szArgs Then
        Call xmldoc.documentElement.setAttribute("Arguments", szNew)
        fDirtyMbr = True
    End If
End Property


Public Property Get szLang() As String
    
    szLang = ""
    
'   Take the language of the last module as the default for this project.
    Call StartEnum
    Dim szLangT As String
    Do While FNextScript(szLangT, "", "")
        If szLangT <> "" Then
            szLang = szLangT
        End If
    Loop
    
End Property


Public Property Get fDirty() As Boolean
    fDirty = fDirtyMbr
End Property


'---------
' Methods
'---------

Public Sub Load(ByVal szFile As String)
    
    Dim szFileAbs As String
    szFileAbs = fso.GetAbsolutePathName(szFile)
    If Not fso.FileExists(szFileAbs) Then
        Call SkError("File not found: " + szFileAbs)
    End If
    
    If Not xmldoc.Load(szFileAbs) Then
        Call RaiseParseError
    End If
    
    szFileMbr = szFileAbs
    fDirtyMbr = False
    Set selEnum = Nothing
    
End Sub

Public Sub Save(Optional ByVal szFileDst As String = "")
    
    If szFileDst = "" Then szFileDst = szFileMbr
    
'    Dim szPath As String
'    szPath = SzPathBackslash(SzDirFromPath(szFileDst))
    
'   CD to the (old) .swx file's home directory so that GetAbsolutePath works properly.
    Call SetCurrentDirectory(SzDirFromPath(szFileMbr))
    
'   Beautify the generated XML text by inserting text nodes in the right places.
    Dim nodeChild As IXMLDOMNode, nodeNext As IXMLDOMNode
    Set nodeChild = xmldoc.documentElement.firstChild
    Do Until nodeChild Is Nothing
        Set nodeNext = nodeChild.nextSibling
        Select Case nodeChild.nodeType
        Case NODE_TEXT
            Dim szText As String
            szText = SzTrimWhiteSpace(nodeChild.Text)
            If szText = "" Then
            '   Remove empty text nodes.
                Call xmldoc.documentElement.removeChild(nodeChild)
            Else
            '   Preserve non-empty text nodes, adjusting indentation.
                nodeChild.Text = vbCrLf + vbTab + szText
            End If
        Case NODE_ELEMENT
        '   Insert indentation.
            Call xmldoc.documentElement.insertBefore( _
              xmldoc.createTextNode(vbCrLf + vbTab), nodeChild)
        '   Normalize script filename to be relative to project file path.
            Dim elemScript As IXMLDOMElement, szSrc As String
            Set elemScript = nodeChild
            szSrc = SzSrcFromElem(elemScript)
            Call SetElemSzSrc(elemScript, szSrc, szFileDst)
'            szSrc = SzFromVNull(elemScript.getAttribute("src"))
'            If szSrc <> "" Then
'                szSrc = fso.GetAbsolutePathName(szSrc)
'                If FPrefixMatchSzI(szSrc, szPath) Then
'                    szSrc = Right(szSrc, Len(szSrc) - Len(szPath))
'                    Call elemScript.setAttribute("src", szSrc)
'                End If
'            End If
        End Select
        Set nodeChild = nodeNext
    Loop
    Call xmldoc.documentElement.appendChild(xmldoc.createTextNode(vbCrLf))
    
'   Save it to the specified file.
    Call xmldoc.Save(szFileDst)
    
    szFileMbr = szFileDst
    fDirtyMbr = False
    
End Sub


Public Sub SetUndo()
    
    szFileSave = szFileMbr
    szXmlSave = xmldoc.xml
    fDirtySave = fDirtyMbr
    
End Sub

Public Sub Undo()
    
    szFileMbr = szFileSave
    Call xmldoc.loadXML(szXmlSave)
    fDirtyMbr = fDirtySave
    Set selEnum = Nothing
    
End Sub


Public Sub StartEnum()
    
    Set selEnum = xmldoc.documentElement.selectNodes("script")
    
'   CD to the .swx file's home directory so relative script file names
'   resolve properly.
    Call SetCurrentDirectory(SzDirFromPath(szFileMbr))
    
End Sub

Public Function FNextScript(ByRef szLang As String, _
  ByRef szSrc As String, ByRef szInline As String) As Boolean
    
    Dim elemScript As IXMLDOMElement
    Set elemScript = selEnum.nextNode
    If elemScript Is Nothing Then
        Set selEnum = Nothing
        FNextScript = False
        Exit Function
    End If
    
    szLang = SzFromVNull(elemScript.getAttribute("language"))
    szInline = elemScript.Text
    
    szSrc = SzSrcFromElem(elemScript)
'    szSrc = SzFromVNull(elemScript.getAttribute("src"))
'    If szSrc <> "" Then szSrc = fso.GetAbsolutePathName(szSrc)
    
    If szLang = "" And szSrc <> "" Then
        szLang = SzLangFromSzFile(szSrc)
    End If
    
    FNextScript = True
End Function


Public Sub InsertScript(ByVal iscript As Long, ByVal szFile As String)
    
    Dim elemNew As IXMLDOMElement
    Set elemNew = xmldoc.createElement("script")
    Call SetElemSzSrc(elemNew, szFile)
'    Call elemNew.setAttribute("src", szFile)
    
    Dim sel As IXMLDOMSelection
    Set sel = xmldoc.documentElement.selectNodes("script")
    If iscript >= 0 And iscript < sel.Length Then
        Call xmldoc.documentElement.insertBefore(elemNew, sel.Item(iscript))
    Else
        Call xmldoc.documentElement.appendChild(elemNew)
    End If
    
    fDirtyMbr = True
    
End Sub

Public Sub RemoveScript(ByVal iscript As Long)
    
    Dim sel As IXMLDOMSelection
    Set sel = xmldoc.documentElement.selectNodes("script")
    Call xmldoc.documentElement.removeChild(sel.Item(iscript))
    
    fDirtyMbr = True
    
End Sub

Public Sub MoveScript(ByVal iscriptDst As Long, ByVal iscriptSrc As Long)
    
    Dim sel As IXMLDOMSelection
    Set sel = xmldoc.documentElement.selectNodes("script")
    
    Dim elemMove As IXMLDOMElement, elemDst As IXMLDOMElement
    Set elemMove = sel.Item(iscriptSrc)
    
    If iscriptDst >= iscriptSrc Then iscriptDst = iscriptDst + 1
    If iscriptDst < sel.Length Then
        Set elemDst = sel.Item(iscriptDst)
    Else
        Set elemDst = Nothing
    End If
    Call xmldoc.documentElement.insertBefore(elemMove, elemDst)
    
    fDirtyMbr = True
    
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()

    Set xmldoc = New DOMDocument40
    xmldoc.preserveWhiteSpace = True
    xmldoc.async = False
    Call xmldoc.setProperty("SelectionLanguage", "XPath")
    Set xmldoc.documentElement = xmldoc.createElement("project")
    
    fRegisterStdHandlers = False
    fRenameDPT = False
    
    szFileMbr = ""
    fDirtyMbr = False
    
End Sub


Private Function FGetAttr(ByVal elem As IXMLDOMElement, ByVal szName As String, _
  Optional ByVal fDefault As Boolean = False) As Boolean
    
    Dim szAttr As String
    szAttr = SzFromVNull(elem.getAttribute(szName))
    If szAttr = "" Then
        FGetAttr = fDefault
    ElseIf FEqSzI(szAttr, "True") Or FEqSzI(szAttr, "Yes") Then
        FGetAttr = True
    Else
        FGetAttr = False
    End If
    
End Function

Private Sub RaiseParseError()
    
    Dim szSource As String, szError As String
    szSource = SzLeaf(xmldoc.parseError.url)
    If xmldoc.parseError.Line <> 0 Then
        szSource = szSource + " line " + CStr(xmldoc.parseError.Line)
    End If
    szError = SzTrimWhiteSpace(xmldoc.parseError.reason)
    Call Err.Raise(xmldoc.parseError.errorCode, szSource, szError)
    
End Sub


Private Function SzSrcFromElem(ByVal elemScript As IXMLDOMElement) As String
    
    Dim szSrc As String
    szSrc = SzFromVNull(elemScript.getAttribute("src"))
    If szSrc <> "" Then
        If FGetAttr(elemScript, "lib") Then
            szSrc = fso.BuildPath(swoptions.szLibDir, szSrc)
        Else
            szSrc = fso.GetAbsolutePathName(szSrc)
        End If
    End If
    
    SzSrcFromElem = szSrc
End Function

Private Sub SetElemSzSrc(ByVal elemScript As IXMLDOMElement, ByVal szSrc As String, _
  Optional ByVal szFileDst As String = "")
    
    If szFileDst = "" Then szFileDst = szFileMbr
    
    Dim szPathLib As String, szPathProject As String
    szPathLib = SzPathBackslash(swoptions.szLibDir)
    If szFileDst <> "" Then
        szPathProject = SzPathBackslash(SzDirFromPath(szFileDst))
    Else
        szPathProject = ""
    End If
    
    If FPrefixMatchSzI(szSrc, szPathLib) Then
        szSrc = Right(szSrc, Len(szSrc) - Len(szPathLib))
        Call elemScript.setAttribute("lib", "True")
    ElseIf szPathProject <> "" And _
      FPrefixMatchSzI(szSrc, szPathProject) Then
        szSrc = Right(szSrc, Len(szSrc) - Len(szPathProject))
        Call elemScript.removeAttribute("lib")
    Else
        Call elemScript.removeAttribute("lib")
    End If
    
    Call elemScript.setAttribute("src", szSrc)
    
End Sub

























