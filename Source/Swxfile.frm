VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form formSwxfile 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Project Options"
   ClientHeight    =   6030
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6150
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Swxfile.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6030
   ScaleWidth      =   6150
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin MSComDlg.CommonDialog comdlg 
      Left            =   5760
      Top             =   0
      _ExtentX        =   688
      _ExtentY        =   688
      _Version        =   393216
   End
   Begin VB.CheckBox checkFullPath 
      Caption         =   "Show complete file paths"
      Height          =   252
      Left            =   240
      TabIndex        =   9
      Top             =   5100
      Width           =   2172
   End
   Begin VB.CommandButton cmdMoveDown 
      Caption         =   "Move Down"
      Height          =   375
      Left            =   4920
      TabIndex        =   8
      Top             =   4680
      Width           =   972
   End
   Begin VB.CommandButton cmdMoveUp 
      Caption         =   "Move Up"
      Height          =   375
      Left            =   4920
      TabIndex        =   7
      Top             =   4200
      Width           =   972
   End
   Begin VB.CommandButton cmdRemove 
      Caption         =   "Remove"
      Height          =   375
      Left            =   4920
      TabIndex        =   6
      Top             =   3720
      Width           =   972
   End
   Begin VB.CommandButton cmdAdd 
      Caption         =   "Add..."
      Height          =   375
      Left            =   4920
      TabIndex        =   5
      Top             =   3240
      Width           =   972
   End
   Begin VB.CommandButton cmdSaveAs 
      Caption         =   "Save As..."
      Height          =   375
      Left            =   240
      TabIndex        =   10
      Top             =   5520
      Width           =   1095
   End
   Begin VB.ListBox listScripts 
      Height          =   1815
      IntegralHeight  =   0   'False
      ItemData        =   "Swxfile.frx":030A
      Left            =   240
      List            =   "Swxfile.frx":030C
      TabIndex        =   4
      Top             =   3240
      Width           =   4572
   End
   Begin VB.CheckBox checkRenameDPT 
      Caption         =   "Rename references to ac.Debug, ac.Print, and obj.Type"
      Height          =   252
      Left            =   240
      TabIndex        =   2
      Top             =   2040
      Width           =   5652
   End
   Begin VB.CheckBox checkRegisterStdHandlers 
      Caption         =   "Automatically register ACMsg_* handlers"
      Height          =   252
      Left            =   240
      TabIndex        =   3
      Top             =   2400
      Width           =   5652
   End
   Begin VB.CheckBox checkActivateAC 
      Caption         =   "Activate AC when script starts"
      Height          =   252
      Left            =   240
      TabIndex        =   1
      Top             =   1680
      Width           =   5652
   End
   Begin VB.TextBox textArgs 
      Height          =   288
      Left            =   240
      TabIndex        =   0
      Top             =   1200
      Width           =   5652
   End
   Begin VB.TextBox textFilename 
      BackColor       =   &H8000000F&
      Height          =   288
      Left            =   240
      Locked          =   -1  'True
      TabIndex        =   14
      TabStop         =   0   'False
      Top             =   480
      Width           =   5652
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   3600
      TabIndex        =   11
      Top             =   5520
      Width           =   1095
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4836
      TabIndex        =   12
      Top             =   5520
      Width           =   1095
   End
   Begin VB.Label labelLang 
      Alignment       =   1  'Right Justify
      Height          =   252
      Left            =   2520
      TabIndex        =   17
      Top             =   5040
      Width           =   2292
   End
   Begin VB.Label labelScripts 
      AutoSize        =   -1  'True
      Caption         =   "Script files will be loaded in the order shown here.  To change the order, use the Move Up and Move Down buttons."
      Height          =   390
      Left            =   240
      TabIndex        =   16
      Top             =   2760
      Width           =   4575
      WordWrap        =   -1  'True
   End
   Begin VB.Label labelArgs 
      Caption         =   "Default arguments:"
      Height          =   252
      Left            =   240
      TabIndex        =   15
      Top             =   960
      Width           =   5652
   End
   Begin VB.Label labelFilename 
      Caption         =   "Project filename:"
      Height          =   252
      Left            =   240
      TabIndex        =   13
      Top             =   240
      Width           =   5652
   End
End
Attribute VB_Name = "formSwxfile"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


'--------------
' Private data
'--------------

Private swxfile As SwxfileCls
Private fResult As Boolean


'---------
' Methods
'---------

Public Function FEditSwxfile(ByVal swxfileA As SwxfileCls, ByVal formOwner As Form) As Boolean
    
    Dim mptrPrev As Long
    mptrPrev = Screen.MousePointer
    Screen.MousePointer = vbDefault
    
    Set swxfile = swxfileA
    Call swxfile.SetUndo
    
    If swxfile.szFile <> "" Then
        textFilename.Text = swxfile.szFile
    Else
        textFilename.Text = "New Project"
    End If
    
    textArgs.Text = swxfile.szArgs
    checkActivateAC.Value = IIf(swxfile.fActivateAC, vbChecked, vbUnchecked)
    checkRenameDPT.Value = IIf(swxfile.fRenameDPT, vbChecked, vbUnchecked)
    checkRegisterStdHandlers.Value = IIf(swxfile.fRegisterStdHandlers, vbChecked, vbUnchecked)
    
    Call FillScriptList
    
    fResult = False
    Call Show(vbModal, formOwner)
    
    Call Unload(Me)
    
    Screen.MousePointer = mptrPrev
    
    FEditSwxfile = fResult
End Function


'----------
' Controls
'----------

Private Sub Form_Load()
    
    labelScripts.Top = listScripts.Top - 30 - labelScripts.Height
    
End Sub


Private Sub textArgs_Change()
    
    swxfile.szArgs = textArgs.Text
    
End Sub

Private Sub checkActivateAC_Click()
    
    swxfile.fActivateAC = (checkActivateAC.Value = vbChecked)
    
End Sub

Private Sub checkRenameDPT_Click()
    
    swxfile.fRenameDPT = (checkRenameDPT.Value = vbChecked)
    
End Sub

Private Sub checkRegisterStdHandlers_Click()
    
    swxfile.fRegisterStdHandlers = (checkRegisterStdHandlers.Value = vbChecked)
    
End Sub


Private Sub listScripts_DblClick()
    
    Dim iscript As Long
    iscript = listScripts.ListIndex
    If iscript >= 0 And iscript < listScripts.ListCount Then
        Dim iscriptT As Long, szLang As String, szScript As String
        iscriptT = 0
        Call swxfile.StartEnum
        Do While swxfile.FNextScript(szLang, szScript, "")
            If iscriptT = iscript Then
                Call ShellExecute(hwnd, "Edit", szScript, _
                  0, 0, SW_SHOWNORMAL)
                Exit Sub
            End If
            iscriptT = iscriptT + 1
        Loop
    End If
    
    Call Beep
    
End Sub


Private Sub cmdAdd_Click()
    
    Dim szFilter As String
    szFilter = SzFilterFromSzLang(swxfile.szLang, False, True)
    
    Dim szFile As String
    comdlg.DialogTitle = "Add Script File"
    If FGetSzFileOpen(szFile, comdlg, szFilter, "", _
      SzDirFromPath(swxfile.szFile)) Then
        Dim iscript As Long
        iscript = listScripts.ListCount
        Call swxfile.InsertScript(iscript, szFile)
        Call FillScriptList
        listScripts.ListIndex = iscript
    End If
    
End Sub

Private Sub cmdRemove_Click()
    
    Dim iscript As Long
    iscript = listScripts.ListIndex
    If iscript >= 0 And iscript < listScripts.ListCount Then
        Call swxfile.RemoveScript(iscript)
        Call FillScriptList
        listScripts.ListIndex = iNil
    Else
        Call Beep
    End If
    
End Sub

Private Sub cmdMoveUp_Click()
    
    Dim iscript As Long
    iscript = listScripts.ListIndex
    If iscript > 0 And iscript < listScripts.ListCount Then
        Dim szItem As String
        szItem = listScripts.List(iscript)
        Call swxfile.MoveScript(iscript - 1, iscript)
        Call listScripts.RemoveItem(iscript)
        Call listScripts.AddItem(szItem, iscript - 1)
        listScripts.ListIndex = iscript - 1
    Else
        Call Beep
    End If
    
End Sub

Private Sub cmdMoveDown_Click()
    
    Dim iscript As Long
    iscript = listScripts.ListIndex
    If iscript >= 0 And iscript + 1 < listScripts.ListCount Then
        Dim szItem As String
        szItem = listScripts.List(iscript)
        Call swxfile.MoveScript(iscript + 1, iscript)
        Call listScripts.RemoveItem(iscript)
        Call listScripts.AddItem(szItem, iscript + 1)
        listScripts.ListIndex = iscript + 1
    Else
        Call Beep
    End If
    
End Sub


Private Sub checkFullPath_Click()
    
    Dim iscript As Long
    iscript = listScripts.ListIndex
    Call FillScriptList
    listScripts.ListIndex = iscript
    
End Sub


Private Sub cmdSaveAs_Click()
    
    Call FSaveAsFile
    
End Sub

Private Sub cmdOK_Click()
    
    If swxfile.szFile = "" Then
        If Not FSaveAsFile() Then Exit Sub
    ElseIf swxfile.fDirty Then
        Call swxfile.Save
    End If
    
    fResult = True
    Call Hide
    
End Sub

Private Sub cmdCancel_Click()
    
    Call swxfile.Undo
    
    fResult = False
    Call Hide
    
End Sub



'----------
' Privates
'----------

Private Sub FillScriptList()
    
    Call listScripts.Clear
    labelLang.Caption = ""
    
    Dim szLang As String, szScript As String
    Call swxfile.StartEnum
    Do While swxfile.FNextScript(szLang, szScript, "")
        If szScript <> "" Then
            If checkFullPath.Value <> vbChecked Then
                szScript = SzLeaf(szScript)
            End If
            Call listScripts.AddItem(szScript)
        Else
            Call listScripts.AddItem("Inline " + szLang + " code")
        End If
        
        If szLang <> "" And IchInStr(0, labelLang.Caption, szLang) = iNil Then
            If labelLang.Caption = "" Then
                labelLang.Caption = "Language: " + szLang
            Else
                labelLang.Caption = labelLang.Caption + ", " + szLang
            End If
        End If
    Loop
    
    If labelLang.Caption = "" And listScripts.ListCount > 0 Then
        labelLang.Caption = "Language: Unknown"
    End If
    
End Sub

Private Function FSaveAsFile() As Boolean
    
    Dim szFile As String
    szFile = swxfile.szFile
    comdlg.DialogTitle = "Save Project"
    If FGetSzFileSave(szFile, comdlg, "SkunkWorks projects|*.swx", "") Then
        Call swxfile.Save(szFile)
        textFilename.Text = swxfile.szFile
        FSaveAsFile = True
    Else
        FSaveAsFile = False
    End If
    
End Function






























