VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "SwxsiteCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit


Public WithEvents site As swxsite
Attribute site.VB_VarHelpID = -1


Private Sub site_ScriptError(ByVal strFile As String, ByVal iline As Long, _
  ByVal strSrc As String, ByVal strDesc As String, ByVal errcode As Long)
    
    If strDesc <> "" And strDesc <> "Script was interrupted." Then
        Call skapi.OutputLine(">> Script error in " + strFile + _
          " line " + CStr(iline) + ": " + strDesc, _
          opmDebugLog + opmConsole + opmChatWnd)
        formSWConsole.fError = True
    End If
    
End Sub




