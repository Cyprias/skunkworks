Attribute VB_Name = "SzFile"
Option Explicit
' Filename utility functions.


'-------------
' Public data
'-------------

Public fso As New FileSystemObject


'-------------
' Public APIs
'-------------

Public Function FEqSzFile(ByVal sz1 As String, ByVal sz2 As String) As Boolean
    
    FEqSzFile = FEqSzI(sz1, sz2)
    
End Function

Public Function FFileExists(szFile As String) As Boolean
    
    FFileExists = False
    On Error GoTo LExit
    
    FFileExists = (Dir(szFile) <> "")
    
LExit:
End Function

Public Function FRelativePathSz(ByVal szPath As String) As Boolean
    
    FRelativePathSz = Not (left(szPath, 2) = "\\" Or SzMid(szPath, 1, 1) = ":")
    
End Function

Public Sub PathHead(ByRef szHead As String, ByRef szTail As String, _
  ByVal szPath As String)
    
    Call SplitSz(szHead, szTail, szPath, "\")
    
End Sub

Public Function SzChangeExt(ByVal szFile As String, ByVal szExtNew As String) As String
    
    SzChangeExt = SzRemoveExt(szFile) + "." + szExtNew
    
End Function

Public Function SzDriveFromPath(ByVal szPath As String)
    SzDriveFromPath = left(szPath, 2)
End Function

Public Function SzDirFromPath(ByVal szPath As String) As String
    
    SzDirFromPath = fso.GetParentFolderName(szPath)
    
End Function

Public Function SzExtension(ByVal szFile As String) As String

    Dim ichFirst As Long, ichLim As Long
    ichLim = Len(szFile)
    ichFirst = ichLim
    Do While ichFirst > 0
        If SzMid(szFile, ichFirst - 1, 1) = "." Then GoTo Break
        ichFirst = ichFirst - 1
    Loop
Break:
    
    SzExtension = SzMid(szFile, ichFirst, ichLim - ichFirst)
End Function

Public Function SzFileFromCmdLine() As String
    
    SzFileFromCmdLine = SzStripQuotes(SzTrimWhiteSpace(Command))
    
End Function

Public Function SzFileInAppDir(ByVal SzLeaf As String) As String
    
    SzFileInAppDir = fso.BuildPath(App.Path, SzLeaf)
    
End Function

Public Function SzFileInTempDir(ByVal SzLeaf As String) As String
    
    SzFileInTempDir = fso.BuildPath(SzTempDir(), SzLeaf)
    
End Function

Public Function SzLeaf(ByVal szFile As String) As String
    
    SzLeaf = fso.GetFileName(szFile)
    
End Function

Public Function SzLeafNoExt(ByVal szFile As String) As String

    SzLeafNoExt = fso.GetBaseName(szFile)
    
End Function

Public Function SzPathBackslash(ByVal szPath As String) As String
    
    If right(szPath, 1) = "\" Or Len(szPath) = 0 Then
        SzPathBackslash = szPath
    Else
        SzPathBackslash = szPath + "\"
    End If
    
End Function

Public Function SzPathNoBackslash(ByVal szPath As String) As String
    
    If right(szPath, 1) = "\" Then
        SzPathNoBackslash = left(szPath, Len(szPath) - 1)
    Else
        SzPathNoBackslash = szPath
    End If
    
End Function

Public Function SzPathFixCase(ByVal szPath As String) As String
    
    Dim szHead As String, szTail As String, sz As String
    szTail = szPath
    sz = ""
    
    Do While szTail <> ""
        Call PathHead(szHead, szTail, szTail)
        sz = sz + SzFixCase(szHead)
        If szTail <> "" Then sz = sz + "\"
    Loop
    
    SzPathFixCase = sz
End Function

Public Function SzQuoteFilename(ByVal szFile As String) As String
    
    If Len(szFile) > 0 Then
        If left(szFile, 1) <> """" Then
            szFile = SzQuote(szFile)
        End If
    End If
    
    SzQuoteFilename = szFile
End Function

Public Function SzRemoveExt(ByVal szFile As String) As String
    
    Dim ichDot As Long
    ichDot = Len(szFile)
    Do While ichDot > 0
        ichDot = ichDot - 1
        If SzMid(szFile, ichDot, 1) = "." Then GoTo Break
    Loop
    ichDot = Len(szFile)
Break:
    
    SzRemoveExt = SzMid(szFile, 0, ichDot)
End Function

Public Function SzTempDir() As String
    
    SzTempDir = fso.GetSpecialFolder(TemporaryFolder).Path
    
End Function

Public Function SzUrlFromSzFile(ByVal szFile As String) As String
    
    Dim szURL As String
    szURL = "file:"
    
    If Not FPrefixMatchSzI(szFile, "\\") Then
        szURL = szURL + "///"
    End If
    
    Do While szFile <> ""
        Dim szT As String
        Call SplitSz(szT, szFile, szFile, "\")
        szURL = szURL + szT
        If szFile <> "" Then szURL = szURL + "/"
    Loop
    
    SzUrlFromSzFile = szURL
    
End Function


' Read the entire contents of the named file and return it as a string.
Public Function SzReadFile(ByVal szFile As String) As String
    On Error GoTo LError
    
    Dim ts As TextStream
    Set ts = fso.OpenTextFile(szFile, ForReading)
    SzReadFile = ts.ReadAll
    Call ts.Close
    
    GoTo LExit
    
LError:
    Call Err.Raise(Err.Number, SzLeaf(szFile), Err.Description)
    
LExit:
End Function




