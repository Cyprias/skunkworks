Attribute VB_Name = "ThreeSpace"
Option Explicit


'--------------------
' Public definitions
'--------------------

Public Enum AxisType
    axisX
    axisY
    axisZ
    axisMax
End Enum

Public Const axisLng = axisY
Public Const axisLat = axisX

Public Const axisYaw = axisY
Public Const axisPitch = axisX
Public Const axisRoll = axisZ

Public Type Pt3Type
    x As Double
    y As Double
    z As Double
End Type

Public Type ViewType
    pt3 As Pt3Type
    mat(2, 2) As Double
    matInv(2, 2) As Double
End Type


'-------------
' Public APIs
'-------------

Public Sub InitView(view As ViewType)
    
    view.pt3.x = 0
    view.pt3.y = 0
    view.pt3.z = 0
    Call MatIdentity(view.mat)
    Call MatIdentity(view.matInv)
    
End Sub

Public Sub TranslateView(view As ViewType, pt3 As Pt3Type)
    
    view.pt3 = pt3
    
End Sub

Public Sub RotateView(view As ViewType, _
  ByVal axis As AxisType, ByVal angle As Double)
    
    Call RotateViewEx(view, axis, Sin(angle), Cos(angle))
    
End Sub

Public Sub RotateViewEx(view As ViewType, _
  ByVal axis As AxisType, ByVal sinA As Double, ByVal cosA As Double)
    
    Dim mat(2, 2) As Double, matInv(2, 2) As Double
    Dim matT(2, 2) As Double
    Call MatIdentity(mat)
    Call MatIdentity(matInv)
    
    Select Case axis
        
    Case axisX
    '   Positive pitch rotates z axis toward y.
    '   Rotate world in opposite direction.
        mat(1, 1) = cosA
        mat(1, 2) = sinA
        mat(2, 1) = -sinA
        mat(2, 2) = cosA
    
        matInv(1, 1) = cosA
        matInv(1, 2) = -sinA
        matInv(2, 1) = sinA
        matInv(2, 2) = cosA
        
    Case axisY
    '   Positive yaw rotates x axis toward z.
    '   Rotate world in opposite direction.
        mat(0, 0) = cosA
        mat(0, 2) = -sinA
        mat(2, 0) = sinA
        mat(2, 2) = cosA
    
        matInv(0, 0) = cosA
        matInv(0, 2) = sinA
        matInv(2, 0) = -sinA
        matInv(2, 2) = cosA
        
    Case axisZ
    '   Positive roll rotates x axis toward y.
    '   Rotate world in opposite direction.
        mat(0, 0) = cosA
        mat(0, 1) = -sinA
        mat(1, 0) = sinA
        mat(1, 1) = cosA
    
        matInv(0, 0) = cosA
        matInv(0, 1) = sinA
        matInv(1, 0) = -sinA
        matInv(1, 1) = cosA
        
    End Select
    
    Call MatMul(matT, view.mat, mat)
    Call MatCopy(view.mat, matT)
    
    Call MatMul(matT, matInv, view.matInv)
    Call MatCopy(view.matInv, matT)
    
End Sub

Public Sub TransformPoint(pt3Dst As Pt3Type, _
  pt3 As Pt3Type, view As ViewType)
    
    Dim pt3T As Pt3Type
    
'   Translate xyz to viewer-centered coordinates.
    pt3T.x = pt3.x - view.pt3.x
    pt3T.y = pt3.y - view.pt3.y
    pt3T.z = pt3.z - view.pt3.z
    
'   Apply rotations.
    Call Pt3MulMat(pt3Dst, pt3T, view.mat)
    
End Sub

Public Sub UntransformPoint(pt3Dst As Pt3Type, _
  pt3 As Pt3Type, view As ViewType)
    
    Dim pt3T As Pt3Type
    
'   Apply inverse rotations.
    Call Pt3MulMat(pt3T, pt3, view.matInv)
    
'   Translate back to absolute coordinates.
    pt3Dst.x = view.pt3.x + pt3T.x
    pt3Dst.y = view.pt3.y + pt3T.y
    pt3Dst.z = view.pt3.z + pt3T.z
    
End Sub

Public Sub ViewLongLat(view As ViewType, lng As Double, lat As Double)
    
'   Unrotated, I'm looking at lng = pi/2, lat = 0.  To look at the given
'   coords, rotate my head so that lng is where pi/2 used to be, then raise my
'   sightline by lat.
    Call RotateView(view, axisLng, lng - pi / 2)
    Call RotateView(view, axisLat, lat)
    
End Sub

Public Sub ViewHeadElev(view As ViewType, head As Double, elev As Double)
    
'   Head is measured in a clockwise sense from dead ahead.  Negate it to turn
'   it into a delta lng by which to rotate.  Elev and lat are interchangeable.
    Call RotateView(view, axisY, -head)
    Call RotateView(view, axisX, elev)
    
End Sub

Public Sub ViewPoint(view As ViewType, pt3 As Pt3Type)
    
    Dim pt3T As Pt3Type
    
'   Find polar coords of point in current frame.
    Call TransformPoint(pt3T, pt3, view)
    
    Dim radXZSqrd As Double, radXZ As Double, radXYZ As Double
    radXZSqrd = pt3T.x * pt3T.x + pt3T.z * pt3T.z
    radXZ = Sqr(radXZSqrd)
    radXYZ = Sqr(radXZSqrd + pt3T.y * pt3T.y)
    
    Dim sinHead As Double, cosHead As Double
    sinHead = -pt3T.x / radXZ
    cosHead = pt3T.z / radXZ
    
    Dim sinElev As Double, cosElev As Double
    sinElev = pt3T.y / radXYZ
    cosElev = radXZ / radXYZ
    
'   Rotate those coords into view.
    Call RotateViewEx(view, axisY, sinHead, cosHead)
    Call RotateViewEx(view, axisX, sinElev, cosElev)
    
End Sub


'----------
' Privates
'----------

Private Sub MatIdentity(mat() As Double)
    
    mat(0, 0) = 1
    mat(0, 1) = 0
    mat(0, 2) = 0
    
    mat(1, 0) = 0
    mat(1, 1) = 1
    mat(1, 2) = 0
    
    mat(2, 0) = 0
    mat(2, 1) = 0
    mat(2, 2) = 1
    
End Sub

Private Sub MatCopy(matDst() As Double, matSrc() As Double)
    
    matDst(0, 0) = matSrc(0, 0)
    matDst(0, 1) = matSrc(0, 1)
    matDst(0, 2) = matSrc(0, 2)
    
    matDst(1, 0) = matSrc(1, 0)
    matDst(1, 1) = matSrc(1, 1)
    matDst(1, 2) = matSrc(1, 2)
    
    matDst(2, 0) = matSrc(2, 0)
    matDst(2, 1) = matSrc(2, 1)
    matDst(2, 2) = matSrc(2, 2)
    
End Sub

Private Sub MatMul(matDst() As Double, mat1() As Double, mat2() As Double)
'   CAUTION: matDst must NOT refer to the same matrix as mat1 or mat2.
    
    matDst(0, 0) = mat1(0, 0) * mat2(0, 0) + mat1(0, 1) * mat2(1, 0) + mat1(0, 2) * mat2(2, 0)
    matDst(0, 1) = mat1(0, 0) * mat2(0, 1) + mat1(0, 1) * mat2(1, 1) + mat1(0, 2) * mat2(2, 1)
    matDst(0, 2) = mat1(0, 0) * mat2(0, 2) + mat1(0, 1) * mat2(1, 2) + mat1(0, 2) * mat2(2, 2)
    
    matDst(1, 0) = mat1(1, 0) * mat2(0, 0) + mat1(1, 1) * mat2(1, 0) + mat1(1, 2) * mat2(2, 0)
    matDst(1, 1) = mat1(1, 0) * mat2(0, 1) + mat1(1, 1) * mat2(1, 1) + mat1(1, 2) * mat2(2, 1)
    matDst(1, 2) = mat1(1, 0) * mat2(0, 2) + mat1(1, 1) * mat2(1, 2) + mat1(1, 2) * mat2(2, 2)
    
    matDst(2, 0) = mat1(2, 0) * mat2(0, 0) + mat1(2, 1) * mat2(1, 0) + mat1(2, 2) * mat2(2, 0)
    matDst(2, 1) = mat1(2, 0) * mat2(0, 1) + mat1(2, 1) * mat2(1, 1) + mat1(2, 2) * mat2(2, 1)
    matDst(2, 2) = mat1(2, 0) * mat2(0, 2) + mat1(2, 1) * mat2(1, 2) + mat1(2, 2) * mat2(2, 2)
    
End Sub

Private Sub Pt3MulMat(pt3Dst As Pt3Type, pt3 As Pt3Type, mat() As Double)
'   CAUTION: PvecDst must NOT point to the same vec as pvec.
    
    pt3Dst.x = pt3.x * mat(0, 0) + pt3.y * mat(1, 0) + pt3.z * mat(2, 0)
    pt3Dst.y = pt3.x * mat(0, 1) + pt3.y * mat(1, 1) + pt3.z * mat(2, 1)
    pt3Dst.z = pt3.x * mat(0, 2) + pt3.y * mat(1, 2) + pt3.z * mat(2, 2)
    
End Sub

































