VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TimerCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


'--------------
' Private data
'--------------

Private cmsecStartMbr As Long
Private timerNextMbr As TimerCls
Private fInListMbr As Boolean


'-------------------
' Public properties
'-------------------

Public tag As Variant


Public Property Get cmsec() As Long
    
    cmsec = LSubNoOflow(GetTickCount(), cmsecStartMbr)
    
End Property

Public Property Let cmsec(ByVal cmsecNew As Long)
    
'   If I'm in the list of scheduled events, remove me.
    If fInListMbr Then Call RemoveFromList
    
    cmsecStartMbr = LSubNoOflow(GetTickCount(), cmsecNew)
    
'   If my zero-point is in the future, schedule me to fire an event then.
    If cmsecNew < 0 Then Call AddToList
    
End Property


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------

Friend Function cmsecStart() As Long
    
    cmsecStart = cmsecStartMbr
    
End Function

Friend Function timerNext() As TimerCls
    
    Set timerNext = timerNextMbr
    
End Function

Friend Sub SetTimerNext(ByVal timer As TimerCls)
    
    Set timerNextMbr = timer
    
End Sub


Friend Sub AddToList()
    
'   Insert me into the list in order of ascending cmsecStart,
'   so that the one due to expire soonest comes first.
    Dim timerT As TimerCls, timerPrev As TimerCls
    Set timerPrev = Nothing
    Set timerT = timerFirst
    Do Until timerT Is Nothing
        If LSubNoOflow(cmsecStartMbr, timerT.cmsecStart) < 0 Then Exit Do
        Set timerPrev = timerT
        Set timerT = timerT.timerNext
    Loop
    
    If timerPrev Is Nothing Then
        Set timerNextMbr = timerFirst
        Set timerFirst = Me
    Else
        Set timerNextMbr = timerPrev.timerNext
        Call timerPrev.SetTimerNext(Me)
    End If
    fInListMbr = True
    
End Sub

Friend Sub RemoveFromList()
    
    Dim timerT As TimerCls, timerPrev As TimerCls
    Set timerPrev = Nothing
    Set timerT = timerFirst
    Do Until timerT Is Nothing
        If timerT Is Me Then
            If timerPrev Is Nothing Then
                Set timerFirst = timerNextMbr
            Else
                Call timerPrev.SetTimerNext(timerNextMbr)
            End If
            Set timerNextMbr = Nothing
            fInListMbr = False
            Exit Do
        End If
        Set timerPrev = timerT
        Set timerT = timerT.timerNext
    Loop
    
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    
    cmsec = 0
    Set timerNextMbr = Nothing
    fInListMbr = False
    
End Sub














