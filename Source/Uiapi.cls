VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UiapiCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
' API functions for scriptable UI.


'--------------
' Private data
'--------------

Private szPanelMbr As String
Private szDirMbr As String
Private csviewMbr As CsviewCls


'---------
' Methods
'---------

Public Sub FireControlEvent(ByVal szControl As String)
    Call TraceEnter("Uiapi.FireControlEvent", SzQuote(szControl))
    
    Call swcontrols.SendControlMsg(szPanelMbr, szControl)
    
    Call TraceExit("Uiapi.FireControlEvent")
End Sub

Public Sub ShowControls(ByVal szViewSchema As String, _
  Optional ByVal fActivate As Boolean = True)
    Call TraceEnter("Uiapi.ShowControls")
    
    Call swcontrols.ShowControls(szViewSchema, fActivate, szDirMbr)
    
    Call TraceExit("Uiapi.ShowControls")
End Sub

Public Sub RemoveControls()
    Call TraceEnter("Uiapi.RemoveControls")
    
'   Can't actually shut down the view while script is on the call stack.
'   Just mark it to be closed when the current event handler or script function returns.
    Call csviewMbr.MarkForClose
    
    Call TraceExit("Uiapi.RemoveControls")
End Sub

Public Function CallPanelFunction(ByVal szPanel As String, ByVal szFunction As String, _
  Optional ByRef v1 As Variant = Empty, Optional ByRef v2 As Variant = Empty, Optional ByRef v3 As Variant = Empty, _
  Optional ByRef v4 As Variant = Empty, Optional ByRef v5 As Variant = Empty)
    Call TraceEnter("Uiapi.CallPanelFunction")
    
    CallPanelFunction = swcontrols.CallPanelFunction(szPanel, szFunction, v1, v2, v3, v4, v5)
    
    Call TraceExit("Uiapi.CallPanelFunction")
End Function

Public Sub OutputDebugLine(ByVal szLine As String)
    
    Call DebugLine(szLine)
    
End Sub


'------------------
' Friend functions
'------------------

Friend Sub Init(ByVal szPanel As String, ByVal szDir As String, ByVal csview As CsviewCls)
    Call TraceEnter("Uiapi.Init", SzQuote(szPanel))
    
    szPanelMbr = szPanel
    szDirMbr = szDir
    Set csviewMbr = csview
    
    Call TraceExit("Uiapi.Init")
End Sub


'----------
' Privates
'----------

Private Sub Class_Initialize()
    Call TraceLine("Uiapi.Initialize")
End Sub

Private Sub Class_Terminate()
    Call TraceLine("Uiapi.Terminate")
End Sub





