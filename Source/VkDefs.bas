Attribute VB_Name = "VKDefs"
Option Explicit


'
' Virtual Keys, Standard Set
'
Public Const VK_LBUTTON = &H1
Public Const VK_RBUTTON = &H2
Public Const VK_CANCEL = &H3
Public Const VK_MBUTTON = &H4            ' NOT contiguous with L & RBUTTON

Public Const VK_XBUTTON1 = &H5           ' NOT contiguous with L & RBUTTON
Public Const VK_XBUTTON2 = &H6           ' NOT contiguous with L & RBUTTON

'
' &H07 : unassigned
'

Public Const VK_BACK = &H8
Public Const VK_TAB = &H9

'
' &H0A - &H0B : reserved
'

Public Const VK_CLEAR = &HC
Public Const VK_RETURN = &HD

Public Const VK_SHIFT = &H10
Public Const VK_CONTROL = &H11
Public Const VK_MENU = &H12
Public Const VK_PAUSE = &H13
Public Const VK_CAPITAL = &H14

Public Const VK_KANA = &H15
Public Const VK_HANGEUL = &H15         ' old name - should be here for compatibility
Public Const VK_HANGUL = &H15
Public Const VK_JUNJA = &H17
Public Const VK_FINAL = &H18
Public Const VK_HANJA = &H19
Public Const VK_KANJI = &H19

Public Const VK_ESCAPE = &H1B

Public Const VK_CONVERT = &H1C
Public Const VK_NONCONVERT = &H1D
Public Const VK_ACCEPT = &H1E
Public Const VK_MODECHANGE = &H1F

Public Const VK_SPACE = &H20
Public Const VK_PRIOR = &H21
Public Const VK_NEXT = &H22
Public Const VK_END = &H23
Public Const VK_HOME = &H24
Public Const VK_LEFT = &H25
Public Const VK_UP = &H26
Public Const VK_RIGHT = &H27
Public Const VK_DOWN = &H28
Public Const VK_SELECT = &H29
Public Const VK_PRINT = &H2A
Public Const VK_EXECUTE = &H2B
Public Const VK_SNAPSHOT = &H2C
Public Const VK_INSERT = &H2D
Public Const VK_DELETE = &H2E
Public Const VK_HELP = &H2F

'
' VK_0 - VK_9 are the same as ASCII '0' - '9' (= &H30 - = &H39)
Public Const VK_0 = &H30
Public Const VK_1 = &H31
Public Const VK_2 = &H32
Public Const VK_3 = &H33
Public Const VK_4 = &H34
Public Const VK_5 = &H35
Public Const VK_6 = &H36
Public Const VK_7 = &H37
Public Const VK_8 = &H38
Public Const VK_9 = &H39

' &H40 : unassigned

' VK_A - VK_Z are the same as ASCII 'A' - 'Z' (= &H41 - = &H5A)
Public Const VK_A = &H41
Public Const VK_B = &H42
Public Const VK_C = &H43
Public Const VK_D = &H44
Public Const VK_E = &H45
Public Const VK_F = &H46
Public Const VK_G = &H47
Public Const VK_H = &H48
Public Const VK_I = &H49
Public Const VK_J = &H4A
Public Const VK_K = &H4B
Public Const VK_L = &H4C
Public Const VK_M = &H4D
Public Const VK_N = &H4E
Public Const VK_O = &H4F
Public Const VK_P = &H50
Public Const VK_Q = &H51
Public Const VK_R = &H52
Public Const VK_S = &H53
Public Const VK_T = &H54
Public Const VK_U = &H55
Public Const VK_V = &H56
Public Const VK_W = &H57
Public Const VK_X = &H58
Public Const VK_Y = &H59
Public Const VK_Z = &H5A

Public Const VK_LWIN = &H5B
Public Const VK_RWIN = &H5C
Public Const VK_APPS = &H5D

'
' &H5E : reserved
'

Public Const VK_SLEEP = &H5F

Public Const VK_NUMPAD0 = &H60
Public Const VK_NUMPAD1 = &H61
Public Const VK_NUMPAD2 = &H62
Public Const VK_NUMPAD3 = &H63
Public Const VK_NUMPAD4 = &H64
Public Const VK_NUMPAD5 = &H65
Public Const VK_NUMPAD6 = &H66
Public Const VK_NUMPAD7 = &H67
Public Const VK_NUMPAD8 = &H68
Public Const VK_NUMPAD9 = &H69
Public Const VK_MULTIPLY = &H6A
Public Const VK_ADD = &H6B
Public Const VK_SEPARATOR = &H6C
Public Const VK_SUBTRACT = &H6D
Public Const VK_DECIMAL = &H6E
Public Const VK_DIVIDE = &H6F
Public Const VK_F1 = &H70
Public Const VK_F2 = &H71
Public Const VK_F3 = &H72
Public Const VK_F4 = &H73
Public Const VK_F5 = &H74
Public Const VK_F6 = &H75
Public Const VK_F7 = &H76
Public Const VK_F8 = &H77
Public Const VK_F9 = &H78
Public Const VK_F10 = &H79
Public Const VK_F11 = &H7A
Public Const VK_F12 = &H7B
Public Const VK_F13 = &H7C
Public Const VK_F14 = &H7D
Public Const VK_F15 = &H7E
Public Const VK_F16 = &H7F
Public Const VK_F17 = &H80
Public Const VK_F18 = &H81
Public Const VK_F19 = &H82
Public Const VK_F20 = &H83
Public Const VK_F21 = &H84
Public Const VK_F22 = &H85
Public Const VK_F23 = &H86
Public Const VK_F24 = &H87

'
' &H88 - &H8F : unassigned
'

Public Const VK_NUMLOCK = &H90
Public Const VK_SCROLL = &H91

'
' NEC PC-9800 kbd definitions
'
Public Const VK_OEM_NEC_EQUAL = &H92    ' '=' key on numpad

'
' Fujitsu/OASYS kbd definitions
'
Public Const VK_OEM_FJ_JISHO = &H92     ' 'Dictionary' key
Public Const VK_OEM_FJ_MASSHOU = &H93   ' 'Unregister word' key
Public Const VK_OEM_FJ_TOUROKU = &H94   ' 'Register word' key
Public Const VK_OEM_FJ_LOYA = &H95      ' 'Left OYAYUBI' key
Public Const VK_OEM_FJ_ROYA = &H96      ' 'Right OYAYUBI' key

'
' &H97 - &H9F : unassigned
'

'
' VK_L* & VK_R* - left and right Alt, Ctrl and Shift virtual keys.
' Used only as parameters to GetAsyncKeyState() and GetKeyState().
' No other API or message will distinguish left and right keys in this way.
'
Public Const VK_LSHIFT = &HA0
Public Const VK_RSHIFT = &HA1
Public Const VK_LCONTROL = &HA2
Public Const VK_RCONTROL = &HA3
Public Const VK_LMENU = &HA4
Public Const VK_RMENU = &HA5

Public Const VK_BROWSER_BACK = &HA6
Public Const VK_BROWSER_FORWARD = &HA7
Public Const VK_BROWSER_REFRESH = &HA8
Public Const VK_BROWSER_STOP = &HA9
Public Const VK_BROWSER_SEARCH = &HAA
Public Const VK_BROWSER_FAVORITES = &HAB
Public Const VK_BROWSER_HOME = &HAC

Public Const VK_VOLUME_MUTE = &HAD
Public Const VK_VOLUME_DOWN = &HAE
Public Const VK_VOLUME_UP = &HAF
Public Const VK_MEDIA_NEXT_TRACK = &HB0
Public Const VK_MEDIA_PREV_TRACK = &HB1
Public Const VK_MEDIA_STOP = &HB2
Public Const VK_MEDIA_PLAY_PAUSE = &HB3
Public Const VK_LAUNCH_MAIL = &HB4
Public Const VK_LAUNCH_MEDIA_SELECT = &HB5
Public Const VK_LAUNCH_APP1 = &HB6
Public Const VK_LAUNCH_APP2 = &HB7

'
' &HB8 - &HB9 : reserved
'

Public Const VK_OEM_1 = &HBA            ' ';:' for US
Public Const VK_OEM_PLUS = &HBB         ' '+' any country
Public Const VK_OEM_COMMA = &HBC        ' ',' any country
Public Const VK_OEM_MINUS = &HBD        ' '-' any country
Public Const VK_OEM_PERIOD = &HBE       ' '.' any country
Public Const VK_OEM_2 = &HBF            ' '/?' for US
Public Const VK_OEM_3 = &HC0            ' '`~' for US

'
' &HC1 - &HD7 : reserved
'

'
' &HD8 - &HDA : unassigned
'

Public Const VK_OEM_4 = &HDB           '  '[{' for US
Public Const VK_OEM_5 = &HDC           '  '\|' for US
Public Const VK_OEM_6 = &HDD           '  ']}' for US
Public Const VK_OEM_7 = &HDE           '  ''"' for US
Public Const VK_OEM_8 = &HDF

'
' &HE0 : reserved
'

'
' Various extended or enhanced keyboards
'
Public Const VK_OEM_AX = &HE1          '  'AX' key on Japanese AX kbd
Public Const VK_OEM_102 = &HE2         '  "<>" or "\|" on RT 102-key kbd.
Public Const VK_ICO_HELP = &HE3        '  Help key on ICO
Public Const VK_ICO_00 = &HE4          '  00 key on ICO

Public Const VK_PROCESSKEY = &HE5

Public Const VK_ICO_CLEAR = &HE6


Public Const VK_PACKET = &HE7

'
' &HE8 : unassigned
'

'
' Nokia/Ericsson definitions
'
Public Const VK_OEM_RESET = &HE9
Public Const VK_OEM_JUMP = &HEA
Public Const VK_OEM_PA1 = &HEB
Public Const VK_OEM_PA2 = &HEC
Public Const VK_OEM_PA3 = &HED
Public Const VK_OEM_WSCTRL = &HEE
Public Const VK_OEM_CUSEL = &HEF
Public Const VK_OEM_ATTN = &HF0
Public Const VK_OEM_FINISH = &HF1
Public Const VK_OEM_COPY = &HF2
Public Const VK_OEM_AUTO = &HF3
Public Const VK_OEM_ENLW = &HF4
Public Const VK_OEM_BACKTAB = &HF5

Public Const VK_ATTN = &HF6
Public Const VK_CRSEL = &HF7
Public Const VK_EXSEL = &HF8
Public Const VK_EREOF = &HF9
Public Const VK_PLAY = &HFA
Public Const VK_ZOOM = &HFB
Public Const VK_NONAME = &HFC
Public Const VK_PA1 = &HFD
Public Const VK_OEM_CLEAR = &HFE

'
' &HFF : reserved
'

Public Const maskVkShift = &H100
Public Const maskVkCtrl = &H200
Public Const maskVkAlt = &H400


Public Const VK_SEMICOLON = VK_OEM_1
Public Const VK_PLUS = VK_OEM_PLUS
Public Const VK_COMMA = VK_OEM_COMMA
Public Const VK_MINUS = VK_OEM_MINUS
Public Const VK_PERIOD = VK_OEM_PERIOD
Public Const VK_SLASH = VK_OEM_2
Public Const VK_GRAVE = VK_OEM_3
Public Const VK_LBRACKET = VK_OEM_4
Public Const VK_BACKSLASH = VK_OEM_5
Public Const VK_RBRACKET = VK_OEM_6
Public Const VK_APOSTROPHE = VK_OEM_7













