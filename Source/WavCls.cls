VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "WavCls"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


'--------------
' Private data
'--------------

Private szFileMbr As String


'-------------------
' Public properties
'-------------------

Public Property Get szFile() As String
    
    szFile = szFileMbr
    
End Property

Public Property Let szFile(ByVal szNew As String)
    
    szFileMbr = szNew
    
End Property


'----------------
' Public methods
'----------------

Public Function Play() As Long
    
    Play = PlaySound(szFileMbr, 0, SND_FILENAME Or SND_SYNC)
    
End Function


'---------------------------------------------------
' Friend functions
' For internal use only; not visible to the script.
'---------------------------------------------------

Friend Sub Create(ByVal szFile As String)
    
    szFileMbr = szFile
    
End Sub


'----------
' Privates
'----------






