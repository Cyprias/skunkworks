Attribute VB_Name = "Win32APIs"
Option Explicit
' Win32 API definitions and declarations.


Public Const INVALID_HANDLE_VALUE As Long = -1
Public Const hInvalid As Long = INVALID_HANDLE_VALUE
Public Const hNil As Long = 0
Public Const hwndNil As Long = hNil
Public Const HWND_BROADCAST = &HFFFF&

Declare Function SendMessage Lib "user32" Alias "SendMessageA" _
  (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wparam As Long, ByVal lparam As Long) As Long
Declare Function PostMessage Lib "user32" Alias "PostMessageA" _
  (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wparam As Long, ByVal lparam As Long) As Long
Declare Function SendMessageCallback Lib "user32" Alias "SendMessageCallbackA" _
  (ByVal hwnd As Long, ByVal msg As Long, ByVal wparam As Long, ByVal lparam As Long, _
   ByVal lpResultCallBack As Long, ByRef dwData As Any) As Long

Declare Function SendMessageTimeout Lib "user32" Alias "SendMessageTimeoutA" _
  (ByVal hwnd As Long, ByVal msg As Long, ByVal wparam As Long, ByVal lparam As Long, _
   ByVal fuFlags As Long, ByVal uTimeout As Long, lpdwResult As Long) As Long
Declare Function SendMessageTimeoutSz Lib "user32" Alias "SendMessageTimeoutA" _
  (ByVal hwnd As Long, ByVal msg As Long, ByVal wparam As Long, ByVal szLParam As String, _
   ByVal fuFlags As Long, ByVal uTimeout As Long, lpdwResult As Long) As Long

Public Const SMTO_NORMAL = &H0

Declare Function SendMessageSz Lib "user32" Alias "SendMessageA" _
  (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wparam As Long, ByVal szLParam As String) As Long
Declare Function PostMessageSz Lib "user32" Alias "PostMessageA" _
  (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wparam As Long, ByVal szLParam As String) As Long

Public Const WM_ACTIVATE = &H6
Public Const WM_ACTIVATEAPP = &H1C
Public Const WM_CHAR = &H102
Public Const WM_COMMAND = &H111
Public Const WM_GETTEXT = &HD
Public Const WM_GETTEXTLENGTH = &HE
Public Const WM_INITMENU = &H116
Public Const WM_KEYDOWN = &H100
Public Const WM_KEYUP = &H101
Public Const WM_KILLFOCUS = &H8
Public Const WM_LBUTTONDBLCLK = &H203
Public Const WM_LBUTTONDOWN = &H201
Public Const WM_LBUTTONUP = &H202
Public Const WM_MBUTTONDBLCLK = &H209
Public Const WM_MBUTTONDOWN = &H207
Public Const WM_MBUTTONUP = &H208
Public Const WM_MOUSEHOVER = &H2A1
Public Const WM_MOUSEMOVE = &H200
Public Const WM_MOUSELEAVE = &H2A3
Public Const WM_NCACTIVATE = &H86
Public Const WM_NCHITTEST = &H84
Public Const WM_RBUTTONDBLCLK = &H206
Public Const WM_RBUTTONDOWN = &H204
Public Const WM_RBUTTONUP = &H205
Public Const WM_PAINT = &HF
Public Const WM_QUIT = &H12
Public Const WM_SETCURSOR = &H20
Public Const WM_SETFOCUS = &H7
Public Const WM_SETTEXT = &HC
Public Const WM_SETTINGCHANGE = &H1A
Public Const WM_TIMER = &H113
Public Const WM_USER = &H400

Public Const MK_LBUTTON = &H1&
Public Const MK_MBUTTON = &H10&
Public Const MK_RBUTTON = &H2&

Public Const SC_CLOSE = &HF060
Public Const SC_MAXIMIZE = &HF030
Public Const SC_MINIMIZE = &HF020
Public Const SC_RESTORE = &HF120&

Public Const BM_CLICK = &HF5
Public Const BM_GETCHECK = &HF0
Public Const BST_CHECKED = &H1

Public Const LB_ADDSTRING = &H180
Public Const LB_FINDSTRINGEXACT = &H1A2
Public Const LB_GETCOUNT = &H18B
Public Const LB_GETCURSEL = &H188
Public Const LB_GETTEXT = &H189
Public Const LB_GETTEXTLEN = &H18A
Public Const LB_INSERTSTRING = &H181
Public Const LB_SELECTSTRING = &H18C
Public Const LB_SETCURSEL = &H186
Public Const LB_ERR = (-1)
Public Const LBN_SELCHANGE = 1

Public Const CB_ADDSTRING = &H143
Public Const CB_FINDSTRINGEXACT = &H158
Public Const CB_SETCURSEL = &H14E
Public Const CB_ERR = (-1)
Public Const CBN_SELCHANGE = 1
Public Const CBN_SELENDOK = 9

Public Const EM_SETMODIFY = &HB9

Public Const BN_CLICKED = 0


Public Const SND_APPLICATION = &H80         '  look for application specific association
Public Const SND_ALIAS = &H10000
Public Const SND_FILENAME = &H20000
Public Const SND_ASYNC = &H1
Public Const SND_NODEFAULT = &H2
Public Const SND_MEMORY = &H4
Public Const SND_SYNC = &H0         '  play synchronously (default)

Declare Function PlaySound Lib "winmm.dll" Alias "PlaySoundA" _
  (ByVal lpszName As String, ByVal hmodule As Long, ByVal dwFlags As Long) As Long
Declare Function PlaySoundRgb Lib "winmm.dll" Alias "PlaySoundA" _
  (rgb As Any, ByVal hmodule As Long, ByVal dwFlags As Long) As Long

Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Declare Function GetTickCount Lib "kernel32" () As Long


Public Declare Function LoadIcon Lib "user32" Alias "LoadIconA" _
  (ByVal hInstance As Long, ByVal lpIconName As String) As Long
Public Declare Function LoadStdIcon Lib "user32" Alias "LoadIconA" _
  (ByVal hInstance As Long, ByVal idIcon As Long) As Long

Public Const IDI_APPLICATION = 32512&
Public Const IDI_ERROR = 32513&
Public Const IDI_QUESTION = 32514&
Public Const IDI_EXCLAMATION = 32515&
Public Const IDI_INFORMATION = 32516&

Public Declare Function DrawIcon Lib "user32" _
  (ByVal hdc As Long, ByVal X As Long, ByVal Y As Long, _
   ByVal hicon As Long) As Long


Public Declare Function FindWindow Lib "user32" Alias "FindWindowA" _
  (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Public Declare Function FindWindowEx Lib "user32" Alias "FindWindowExA" _
  (ByVal hWnd1 As Long, ByVal hWnd2 As Long, ByVal lpClassName As String, ByVal lpWindowName As String) As Long

Public Declare Function EnumWindows Lib "user32" _
  (ByVal lpEnumFunc As Long, ByVal lparam As Long) As Long

Public Declare Function EnumChildWindows Lib "user32" _
  (ByVal hWndParent As Long, ByVal lpEnumFunc As Long, ByVal lparam As Long) As Long

Public Declare Function EnumThreadWindows Lib "user32" _
  (ByVal dwThreadId As Long, ByVal lpfn As Long, ByVal lparam As Long) As Long

Declare Function IsWindow Lib "user32" (ByVal hwnd As Long) As Long
Declare Function IsIconic Lib "user32" (ByVal hwnd As Long) As Long
Declare Function IsZoomed Lib "user32" (ByVal hwnd As Long) As Long

Declare Function CallWindowProc Lib "user32" Alias "CallWindowProcA" _
  (ByVal lpPrevWndFunc As Long, _
   ByVal hwnd As Long, ByVal msg As Long, _
   ByVal wparam As Long, ByVal lparam As Long) As Long

Public Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" _
  (ByVal hwnd As Long, ByVal nIndex As Long) As Long
Declare Function SetWindowLong Lib "user32" Alias "SetWindowLongA" _
  (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long

Public Const GWL_EXSTYLE = (-20)
Public Const GWL_HINSTANCE = (-6)
Public Const GWL_HWNDPARENT = (-8)
Public Const GWL_ID = (-12)
Public Const GWL_STYLE = (-16)
Public Const GWL_USERDATA = (-21)
Public Const GWL_WNDPROC = (-4)

Public Declare Function GetClassName Lib "user32" Alias "GetClassNameA" _
  (ByVal hwnd As Long, ByVal lpClassName As String, ByVal nMaxCount As Long) As Long

Public Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" _
  (ByVal hwnd As Long, ByVal lpString As String, ByVal cch As Long) As Long
Public Declare Function GetWindowTextLength Lib "user32" Alias "GetWindowTextLengthA" _
  (ByVal hwnd As Long) As Long

Public Const WS_DISABLED = &H8000000
Public Const WS_POPUP = &H80000000
Public Const WS_VISIBLE = &H10000000


Public Declare Function GetWindowThreadProcessId Lib "user32" _
  (ByVal hwnd As Long, lpdwProcessId As Long) As Long

Public Declare Function GetCurrentThreadId Lib "kernel32" () As Long

Public Declare Function GetCurrentProcessId Lib "kernel32" () As Long

Public Type STARTUPINFO
    cb As Long
    lpReserved As String
    lpDesktop As String
    lpTitle As String
    dwX As Long
    dwY As Long
    dwXSize As Long
    dwYSize As Long
    dwXCountChars As Long
    dwYCountChars As Long
    dwFillAttribute As Long
    dwFlags As Long
    wShowWindow As Integer
    cbReserved2 As Integer
    lpReserved2 As Long
    hStdInput As Long
    hStdOutput As Long
    hStdError As Long
End Type
Public Const STARTF_USESHOWWINDOW = &H1

Public Type PROCESS_INFORMATION
    hprocess As Long
    hThread As Long
    dwProcessId As Long
    dwThreadId As Long
End Type

Public Declare Function CreateProcess Lib "kernel32" Alias "CreateProcessA" _
  (ByVal lpApplicationName As String, ByVal lpCommandLine As String, _
  ByVal lpProcessAttributes As Long, ByVal lpThreadAttributes As Long, _
  ByVal bInheritHandles As Long, ByVal dwCreationFlags As Long, ByVal lpEnvironment As Long, _
  ByVal lpCurrentDriectory As String, lpStartupInfo As STARTUPINFO, _
  lpProcessInformation As PROCESS_INFORMATION) As Long


Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" _
  (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Long, _
   ByVal lpFileName As String) As Long
Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" _
  (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, _
   ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" _
  (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Long


Declare Function GetSystemMetrics Lib "user32" (ByVal nIndex As Long) As Long

Public Const SM_CMETRICS = 44
Public Const SM_CMOUSEBUTTONS = 43
Public Const SM_CXBORDER = 5
Public Const SM_CXCURSOR = 13
Public Const SM_CXDLGFRAME = 7
Public Const SM_CXDOUBLECLK = 36
Public Const SM_CXFIXEDFRAME = SM_CXDLGFRAME
Public Const SM_CXFRAME = 32
Public Const SM_CXHSCROLL = 21
Public Const SM_CXHTHUMB = 10
Public Const SM_CXICON = 11
Public Const SM_CXICONSPACING = 38
Public Const SM_CXMAXIMIZED = 61
Public Const SM_CXMIN = 28
Public Const SM_CXMINTRACK = 34
Public Const SM_CXSCREEN = 0
Public Const SM_CXSIZE = 30
Public Const SM_CXSIZEFRAME = SM_CXFRAME
Public Const SM_CXVSCROLL = 2
Public Const SM_CYBORDER = 6
Public Const SM_CYCAPTION = 4
Public Const SM_CYCURSOR = 14
Public Const SM_CYDLGFRAME = 8
Public Const SM_CYDOUBLECLK = 37
Public Const SM_CYFIXEDFRAME = SM_CYDLGFRAME
Public Const SM_CYFRAME = 33
Public Const SM_CYHSCROLL = 3
Public Const SM_CYFULLSCREEN = 17
Public Const SM_CYICON = 12
Public Const SM_CYICONSPACING = 39
Public Const SM_CYKANJIWINDOW = 18
Public Const SM_CYMAXIMIZED = 62
Public Const SM_CYMENU = 15
Public Const SM_CYMIN = 29
Public Const SM_CYMINTRACK = 35
Public Const SM_CYSCREEN = 1
Public Const SM_CYSIZEFRAME = SM_CYFRAME
Public Const SM_CYVTHUMB = 9

Declare Function CopyFile Lib "kernel32" Alias "CopyFileA" _
  (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, _
   ByVal bFailIfExists As Long) As Long
Declare Function CreateDirectory Lib "kernel32" Alias "CreateDirectoryA" _
  (ByVal lpPathName As String, lpSecurityAttributes As Any) As Long
Declare Function DeleteFile Lib "kernel32" Alias "DeleteFileA" _
  (ByVal lpFileName As String) As Long
  
Declare Function MoveFile Lib "kernel32" Alias "MoveFileA" _
  (ByVal lpExistingFileName As String, ByVal lpNewFileName As String) As Long
Declare Function MoveFileEx Lib "kernel32" Alias "MoveFileExA" _
  (ByVal lpExistingFileName As String, ByVal lpNewFileName As String, ByVal dwFlags As Long) As Long
Public Const MOVEFILE_REPLACE_EXISTING = &H1
Public Const MOVEFILE_COPY_ALLOWED = &H2
Public Const MOVEFILE_DELAY_UNTIL_REBOOT = &H4

Public Declare Function SetCurrentDirectory Lib "kernel32" Alias "SetCurrentDirectoryA" _
  (ByVal lpPathName As String) As Long


' Struct used by Shell_NotifyIcon (from ShellAPI.h):
Public Type NOTIFYICONDATA
    cbSize As Long
    hwnd As Long
    uId As Long
    uFlags As Long
    uCallBackMessage As Long
    hicon As Long
    szTip As String * 64
End Type

' Flag values for NOTIFYICONDATA.uFlags (from ShellAPI.h):
Public Const NIF_MESSAGE = &H1
Public Const NIF_ICON = &H2
Public Const NIF_TIP = &H4

' Shell_NotifyIcon message constants (from ShellAPI.h):
Public Const NIM_ADD = &H0
Public Const NIM_MODIFY = &H1
Public Const NIM_DELETE = &H2

Declare Function Shell_NotifyIcon Lib "Shell32" Alias "Shell_NotifyIconA" _
   (ByVal dwMessage As Long, pnid As NOTIFYICONDATA) As Boolean


Declare Function FindFirstChangeNotification Lib "kernel32" Alias "FindFirstChangeNotificationA" _
  (ByVal lpPathName As String, ByVal bWatchSubtree As Long, ByVal dwNotifyFilter As Long) As Long
Declare Function FindNextChangeNotification Lib "kernel32" _
  (ByVal hChangeHandle As Long) As Long
Declare Function FindCloseChangeNotification Lib "kernel32" _
  (ByVal hChangeHandle As Long) As Long

Public Const FILE_NOTIFY_CHANGE_ATTRIBUTES = &H4
Public Const FILE_NOTIFY_CHANGE_DIR_NAME = &H2
Public Const FILE_NOTIFY_CHANGE_FILE_NAME = &H1
Public Const FILE_NOTIFY_CHANGE_LAST_WRITE = &H10
Public Const FILE_NOTIFY_CHANGE_SECURITY = &H100
Public Const FILE_NOTIFY_CHANGE_SIZE = &H8


Declare Function WaitForSingleObject Lib "kernel32" _
  (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long

Public Const INFINITE = &HFFFFFFFF       '  Infinite timeout
Public Const WAIT_OBJECT_0 As Long = &H0&
Public Const WAIT_TIMEOUT As Long = &H102&
Public Const WAIT_ABANDONED As Long = &H80&


Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" _
  (ByVal lpBuffer As String, nSize As Long) As Long

Declare Function GetComputerName Lib "kernel32" Alias "GetComputerNameA" _
  (ByVal lpBuffer As String, nSize As Long) As Long


Public Type POINTAPI
    X As Long
    Y As Long
End Type

Public Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

Public Declare Function GetWindowRect Lib "user32" _
  (ByVal hwnd As Long, lpRect As RECT) As Long
Public Declare Function GetClientRect Lib "user32" _
  (ByVal hwnd As Long, lpRect As RECT) As Long

Public Declare Function MoveWindow Lib "user32" _
  (ByVal hwnd As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal bRepaint As Long) As Long

Declare Function SetWindowPos Lib "user32" _
  (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, _
   ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, _
   ByVal wFlags As Long) As Long


Public Type WINDOWPLACEMENT
        Length As Long
        flags As Long
        showCmd As Long
        ptMinPosition As POINTAPI
        ptMaxPosition As POINTAPI
        rcNormalPosition As RECT
End Type
Public Declare Function GetWindowPlacement Lib "user32" _
  (ByVal hwnd As Long, lpwndpl As WINDOWPLACEMENT) As Long
Public Declare Function SetWindowPlacement Lib "user32" _
  (ByVal hwnd As Long, lpwndpl As WINDOWPLACEMENT) As Long


Public Const HWND_TOP = 0
Public Const HWND_BOTTOM = 1
Public Const HWND_TOPMOST = -1
Public Const HWND_NOTOPMOST = -2

Public Const SWP_NOSIZE = &H1
Public Const SWP_NOMOVE = &H2
Public Const SWP_NOZORDER = &H4
Public Const SWP_NOREDRAW = &H8
Public Const SWP_NOACTIVATE = &H10
Public Const SWP_FRAMECHANGED = &H20        '  The frame changed: send WM_NCCALCSIZE
Public Const SWP_DRAWFRAME = SWP_FRAMECHANGED
Public Const SWP_SHOWWINDOW = &H40
Public Const SWP_HIDEWINDOW = &H80
Public Const SWP_NOCOPYBITS = &H100
Public Const SWP_NOOWNERZORDER = &H200      '  Don't do owner Z ordering
Public Const SWP_NOREPOSITION = SWP_NOOWNERZORDER

Declare Function ShowWindow Lib "user32" _
  (ByVal hwnd As Long, ByVal nCmdShow As Long) As Long
Public Const SW_MAXIMIZE = 3
Public Const SW_MINIMIZE = 6
Public Const SW_RESTORE = 9
Public Const SW_SHOW = 5
Public Const SW_SHOWDEFAULT = 10
Public Const SW_SHOWMAXIMIZED = 3
Public Const SW_SHOWMINIMIZED = 2
Public Const SW_SHOWMINNOACTIVE = 7
Public Const SW_SHOWNA = 8
Public Const SW_SHOWNOACTIVATE = 4
Public Const SW_SHOWNORMAL = 1

Public Declare Function GetForegroundWindow Lib "user32" () As Long
Public Declare Function SetForegroundWindow Lib "user32" _
  (ByVal hwnd As Long) As Long


Declare Function SetTimer Lib "user32" _
  (ByVal hwnd As Long, ByVal id As Long, ByVal cmsec As Long, ByVal lpTimerFunc As Long) As Long
Declare Function KillTimer Lib "user32" _
  (ByVal hwnd As Long, ByVal id As Long) As Long

Public Const idNil = 0


Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" _
  (ByVal hwnd As Long, ByVal lpOperation As String, ByVal lpFile As String, _
   ByVal lpParameters As String, ByVal lpDirectory As String, _
   ByVal nShowCmd As Long) As Long


Declare Function GetLastError Lib "kernel32" () As Long
Public Const ERROR_SUCCESS = 0&
Public Const ERROR_CANCELLED = 1223&
Public Const ERROR_ALREADY_EXISTS = 183&


Public Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" _
  (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long

Declare Function GetTempFileName Lib "kernel32" Alias "GetTempFileNameA" _
  (ByVal lpszPath As String, ByVal lpPrefixString As String, _
   ByVal wUnique As Long, ByVal lpTempFileName As String) As Long

Public Const MAX_PATH = 260


Declare Function GetExitCodeThread Lib "kernel32" _
  (ByVal hThread As Long, lpExitCode As Long) As Long

  
Declare Function GetFileAttributes Lib "kernel32" Alias "GetFileAttributesA" _
  (ByVal lpFileName As String) As Long
Public Const FILE_ATTRIBUTE_ARCHIVE = &H20
Public Const FILE_ATTRIBUTE_COMPRESSED = &H800
Public Const FILE_ATTRIBUTE_DIRECTORY = &H10
Public Const FILE_ATTRIBUTE_HIDDEN = &H2
Public Const FILE_ATTRIBUTE_NORMAL = &H80
Public Const FILE_ATTRIBUTE_READONLY = &H1
Public Const FILE_ATTRIBUTE_SYSTEM = &H4
Public Const FILE_ATTRIBUTE_TEMPORARY = &H100

Public Type FILETIME
        dwLowDateTime As Long
        dwHighDateTime As Long
End Type

Public Type WIN32_FIND_DATA
        dwFileAttributes As Long
        ftCreationTime As FILETIME
        ftLastAccessTime As FILETIME
        ftLastWriteTime As FILETIME
        nFileSizeHigh As Long
        nFileSizeLow As Long
        dwReserved0 As Long
        dwReserved1 As Long
        cFileName As String * MAX_PATH
        cAlternate As String * 14
End Type

Declare Function FindFirstFile Lib "kernel32" Alias "FindFirstFileA" _
  (ByVal lpFileName As String, lpFindFileData As WIN32_FIND_DATA) As Long
Declare Function FindNextFile Lib "kernel32" Alias "FindNextFileA" _
  (ByVal hFindFile As Long, lpFindFileData As WIN32_FIND_DATA) As Long
Declare Function FindClose Lib "kernel32" _
  (ByVal hFindFile As Long) As Long


Public Declare Function GetFileTime Lib "kernel32" _
  (ByVal hFile As Long, lpCreationTime As FILETIME, lpLastAccessTime As FILETIME, _
   lpLastWriteTime As FILETIME) As Long
Public Declare Sub GetSystemTimeAsFileTime Lib "kernel32" _
  (lpFileTime As FILETIME)
Public Declare Function CompareFileTime Lib "kernel32" _
  (lpFileTime1 As FILETIME, lpFileTime2 As FILETIME) As Long


Declare Function GetDiskFreeSpace Lib "kernel32" Alias "GetDiskFreeSpaceA" _
  (ByVal lpRootPathName As String, lpSectorsPerCluster As Long, _
  lpBytesPerSector As Long, lpNumberOfFreeClusters As Long, _
  lpTotalNumberOfClusters As Long) As Long

Public Type LargeintType
    dwLow As Long
    dwHigh As Long
End Type
Declare Function GetDiskFreeSpaceEx Lib "kernel32" Alias "GetDiskFreeSpaceExA" _
  (ByVal lpDirectoryName As String, _
  ByRef lpFreeBytesAvailableToCaller As LargeintType, _
  ByRef lpTotalNumberOfBytes As LargeintType, _
  ByRef lpTotalNumberOfFreeBytes As LargeintType) As Long


Declare Function Polygon Lib "gdi32" _
  (ByVal hdc As Long, lpPoint As POINTAPI, ByVal nCount As Long) As Long

Declare Function CreatePen Lib "gdi32" _
  (ByVal nPenStyle As Long, ByVal nWidth As Long, ByVal crColor As Long) As Long
Public Const PS_SOLID = 0

Declare Function CreateSolidBrush Lib "gdi32" _
  (ByVal crColor As Long) As Long

Declare Function SelectObject Lib "gdi32" _
  (ByVal hdc As Long, ByVal hObject As Long) As Long


Public Const IDOK = 1
Public Const IDCANCEL = 2
Public Const IDYES = 6
Public Const IDNO = 7


Declare Function GetMenu Lib "user32" _
  (ByVal hwnd As Long) As Long
Declare Function GetSubMenu Lib "user32" _
  (ByVal hmenu As Long, ByVal nPos As Long) As Long
Declare Function GetMenuItemCount Lib "user32" _
  (ByVal hmenu As Long) As Long
Declare Function GetMenuItemID Lib "user32" _
  (ByVal hmenu As Long, ByVal nPos As Long) As Long


Declare Function GetParent Lib "user32" _
  (ByVal hwnd As Long) As Long
Declare Function GetWindow Lib "user32" _
  (ByVal hwnd As Long, ByVal wCmd As Long) As Long
Public Const GW_HWNDFIRST = 0
Public Const GW_HWNDLAST = 1
Public Const GW_HWNDNEXT = 2
Public Const GW_HWNDPREV = 3
Public Const GW_OWNER = 4
Public Const GW_CHILD = 5


Public Declare Sub ZeroMemory Lib "kernel32" Alias "RtlZeroMemory" _
  (dest As Any, ByVal cb As Long)
Public Declare Sub FillMemory Lib "kernel32" Alias "RtlFillMemory" _
  (dest As Any, ByVal cb As Long, ByVal bFill As Long)
Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" _
  (Destination As Any, Source As Any, ByVal cb As Long)
Declare Sub CopyMemFromPv Lib "kernel32" Alias "RtlMoveMemory" _
  (Destination As Any, ByVal pvSource As Long, ByVal cb As Long)
Declare Sub CopyMemFromSz Lib "kernel32" Alias "RtlMoveMemory" _
  (Destination As Any, ByVal szSource As String, ByVal cb As Long)
Declare Sub CopyMemToPv Lib "kernel32" Alias "RtlMoveMemory" _
  (ByVal pvDest As Long, Source As Any, ByVal cb As Long)
Declare Sub CopyMemToSz Lib "kernel32" Alias "RtlMoveMemory" _
  (ByVal szDst As String, Source As Any, ByVal cb As Long)
Declare Sub CopySzFromPv Lib "kernel32" Alias "RtlMoveMemory" _
  (ByVal szDst As String, ByVal pvSource As Long, ByVal cb As Long)
Declare Sub CopyPv Lib "kernel32" Alias "RtlMoveMemory" _
  (ByVal pvDst As Long, ByVal pvSource As Long, ByVal cb As Long)


Public Const PROCESS_TERMINATE = &H1
Public Const PROCESS_CREATE_THREAD = &H2
Public Const PROCESS_SET_SESSIONID = &H4
Public Const PROCESS_VM_OPERATION = &H8
Public Const PROCESS_VM_READ = &H10
Public Const PROCESS_VM_WRITE = &H20
Public Const PROCESS_DUP_HANDLE = &H40
Public Const PROCESS_CREATE_PROCESS = &H80
Public Const PROCESS_SET_QUOTA = &H100
Public Const PROCESS_SET_INFORMATION = &H200
Public Const PROCESS_QUERY_INFORMATION = &H400

Public Declare Function OpenProcess Lib "kernel32" _
  (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Public Declare Function CloseHandle Lib "kernel32" _
  (ByVal hObject As Long) As Long
Public Declare Function ReadProcessMemory Lib "kernel32" _
  (ByVal hprocess As Long, ByVal lpBaseAddress As Long, lpBuffer As Any, ByVal nSize As Long, lpNumberOfBytesWritten As Long) As Long
Public Declare Function WriteProcessMemory Lib "kernel32" _
  (ByVal hprocess As Long, ByVal lpBaseAddress As Long, lpBuffer As Any, ByVal nSize As Long, lpNumberOfBytesWritten As Long) As Long


Public Declare Function VirtualQueryEx Lib "kernel32" _
  (ByVal hprocess As Long, ByVal lpAddress As Long, _
   lpBuffer As MEMORY_BASIC_INFORMATION, ByVal dwLength As Long) As Long

Public Type MEMORY_BASIC_INFORMATION
     BaseAddress As Long
     AllocationBase As Long
     AllocationProtect As Long
     RegionSize As Long
     State As Long
     Protect As Long
     lType As Long
End Type

Public Const MEM_COMMIT = &H1000
Public Const MEM_RESERVE = &H2000
Public Const MEM_FREE = &H10000


Public Declare Function EnumProcessModules Lib "psapi" _
  (ByVal hprocess As Long, ByRef rghmodule As Long, _
   ByVal cb As Long, ByRef cbNeeded As Long) As Long

Public Declare Function GetModuleFileNameEx Lib "psapi" Alias "GetModuleFileNameExA" _
  (ByVal hprocess As Long, ByVal hmodule As Long, _
   ByVal lpFileName As String, ByVal nSize As Long) As Long

Public Type ModuleInfoType
    lpBaseOfDll As Long
    SizeOfImage As Long
    EntryPoint As Long
End Type

Public Declare Function GetModuleInformation Lib "psapi" _
  (ByVal hprocess As Long, ByVal hmodule As Long, _
   moduleinfo As ModuleInfoType, ByVal cb As Long) As Long


Public Type TrackMouseEventType
    cbSize As Long
    dwFlags As Long
    hwndTrack As Long
    dwHoverTime As Long
End Type
Public Const TME_HOVER = &H1
Public Const TME_LEAVE = &H2
Public Const TME_QUERY = &H40000000
Public Const TME_CANCEL = &H80000000

Public Declare Function TrackMouseEvent Lib "user32" _
  (tme As TrackMouseEventType) As Long


Public Declare Function GetCursorPos Lib "user32" _
  (lpPoint As POINTAPI) As Long
Public Declare Function SetCursorPos Lib "user32" _
  (ByVal X As Long, ByVal Y As Long) As Long

Public Declare Function ClientToScreen Lib "user32" _
  (ByVal hwnd As Long, lpPoint As POINTAPI) As Long
Public Declare Function ScreenToClient Lib "user32" _
  (ByVal hwnd As Long, lpPoint As POINTAPI) As Long


Public Declare Function GetFullPathName Lib "kernel32" Alias "GetFullPathNameA" _
  (ByVal lpFileName As String, ByVal nBufferLength As Long, ByVal lpBuffer As String, lpFilePart As Long) As Long



Public Declare Sub OutputDebugString Lib "kernel32" Alias "OutputDebugStringA" _
  (ByVal lpOutputString As String)


Public Declare Function GetDC Lib "user32" _
  (ByVal hwnd As Long) As Long
Public Declare Function ReleaseDC Lib "user32" _
  (ByVal hwnd As Long, ByVal hdc As Long) As Long

Public Declare Function GetPixel Lib "gdi32" _
  (ByVal hdc As Long, ByVal X As Long, ByVal Y As Long) As Long


Public Type OSVERSIONINFO
        dwOSVersionInfoSize As Long
        dwMajorVersion As Long
        dwMinorVersion As Long
        dwBuildNumber As Long
        dwPlatformId As Long
        szCSDVersion As String * 128      '  Maintenance string for PSS usage
End Type
Public Const VER_PLATFORM_WIN32_NT = 2
Public Const VER_PLATFORM_WIN32_WINDOWS = 1
Public Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" _
  (lpVersionInformation As OSVERSIONINFO) As Long


Public Type SECURITY_ATTRIBUTES
        nLength As Long
        lpSecurityDescriptor As Long
        bInheritHandle As Long
End Type


Public Declare Function CreateNamedPipe Lib "kernel32" Alias "CreateNamedPipeA" _
  (ByVal lpName As String, ByVal dwOpenMode As Long, ByVal dwPipeMode As Long, _
   ByVal nMaxInstances As Long, ByVal nOutBufferSize As Long, ByVal nInBufferSize As Long, _
   ByVal nDefaultTimeOut As Long, ByVal lpSecurityAttributes As Long) As Long
Public Const PIPE_ACCESS_DUPLEX = &H3
Public Const PIPE_ACCESS_INBOUND = &H1
Public Const PIPE_ACCESS_OUTBOUND = &H2
Public Const PIPE_TYPE_BYTE = &H0
Public Const PIPE_TYPE_MESSAGE = &H4
Public Const PIPE_READMODE_BYTE = &H0
Public Const PIPE_READMODE_MESSAGE = &H2
Public Const PIPE_NOWAIT = &H1
Public Const PIPE_WAIT = &H0

Public Declare Function PeekNamedPipe Lib "kernel32" _
  (ByVal hNamedPipe As Long, lpBuffer As Any, ByVal nBufferSize As Long, _
   lpBytesRead As Long, lpTotalBytesAvail As Long, lpBytesLeftThisMessage As Long) As Long

Public Declare Function ConnectNamedPipe Lib "kernel32" _
  (ByVal hNamedPipe As Long, ByVal lpOverlapped As Long) As Long

Public Declare Function DisconnectNamedPipe Lib "kernel32" _
  (ByVal hNamedPipe As Long) As Long

Public Declare Function SetNamedPipeHandleState Lib "kernel32" _
  (ByVal hNamedPipe As Long, lpMode As Long, lpMaxCollectionCount As Long, _
   lpCollectDataTimeout As Long) As Long

Public Const ERROR_PIPE_BUSY = 231&
Public Const ERROR_PIPE_CONNECTED = 535&
Public Const ERROR_PIPE_LISTENING = 536&
Public Const ERROR_PIPE_NOT_CONNECTED = 233&
Public Const ERROR_NO_DATA = 232&


Public Const SYNCHRONIZE = &H100000
Public Const STANDARD_RIGHTS_REQUIRED = &HF0000


Public Declare Function CreateEvent Lib "kernel32" Alias "CreateEventA" _
  (ByVal lpEventAttributes As Long, ByVal bManualReset As Long, _
   ByVal bInitialState As Long, ByVal lpName As String) As Long
Public Declare Function OpenEvent Lib "kernel32" Alias "OpenEventA" _
  (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, _
   ByVal lpName As String) As Long
Public Const EVENT_MODIFY_STATE = &H2&
Public Const EVENT_ALL_ACCESS = (STANDARD_RIGHTS_REQUIRED Or SYNCHRONIZE Or _
  &H3&)
Public Declare Function SetEvent Lib "kernel32" _
  (ByVal hevent As Long) As Long
Public Declare Function ResetEvent Lib "kernel32" _
  (ByVal hevent As Long) As Long

Public Declare Function CreateMutex Lib "kernel32" Alias "CreateMutexA" _
  (ByVal lpMutexAttributes As Long, ByVal bInitialOwner As Long, _
   ByVal lpName As String) As Long
Public Declare Function OpenMutex Lib "kernel32" Alias "OpenMutexA" _
  (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, _
   ByVal lpName As String) As Long
Public Const MUTEX_QUERY_STATE = &H1&
Public Const MUTEX_ALL_ACCESS = (STANDARD_RIGHTS_REQUIRED Or SYNCHRONIZE Or _
  MUTEX_QUERY_STATE)
Public Declare Function ReleaseMutex Lib "kernel32" _
  (ByVal hmutex As Long) As Long

Public Declare Function CreateFile Lib "kernel32" Alias "CreateFileA" _
  (ByVal lpFileName As String, ByVal dwDesiredAccess As Long, _
   ByVal dwShareMode As Long, ByVal lpSecurityAttributes As Long, _
   ByVal dwCreationDisposition As Long, ByVal dwFlagsAndAttributes As Long, _
   ByVal hTemplateFile As Long) As Long
Public Const GENERIC_READ = &H80000000
Public Const GENERIC_WRITE = &H40000000
Public Const FILE_SHARE_READ = &H1
Public Const FILE_SHARE_WRITE = &H2
Public Const FILE_SHARE_DELETE = &H4
Public Const CREATE_NEW = 1
Public Const CREATE_ALWAYS = 2
Public Const OPEN_EXISTING = 3
Public Const OPEN_ALWAYS = 4
Public Const TRUNCATE_EXISTING = 5
Public Const FILE_FLAG_RANDOM_ACCESS = &H10000000
Public Const FILE_FLAG_DELETE_ON_CLOSE = &H4000000

Public Type OVERLAPPED
        Internal As Long
        InternalHigh As Long
        offset As Long
        OffsetHigh As Long
        hevent As Long
End Type

Public Declare Function ReadFile Lib "kernel32" _
  (ByVal hFile As Long, lpBuffer As Any, ByVal cb As Long, _
   lpNumberOfBytesRead As Long, ByVal lpOverlapped As Long) As Long

Public Declare Function ReadFileSz Lib "kernel32" Alias "ReadFile" _
  (ByVal hFile As Long, ByVal sz As String, ByVal cb As Long, _
   lpNumberOfBytesRead As Long, ByVal lpOverlapped As Long) As Long

Public Declare Function ReadFilePb Lib "kernel32" Alias "ReadFile" _
  (ByVal hFile As Long, ByVal lpBuffer As Long, ByVal cb As Long, _
   lpNumberOfBytesRead As Long, ByVal lpOverlapped As Long) As Long

Public Declare Function WriteFile Lib "kernel32" _
  (ByVal hFile As Long, lpBuffer As Any, ByVal cb As Long, _
  lpNumberOfBytesWritten As Long, ByVal lpOverlapped As Long) As Long

Public Declare Function WriteFileSz Lib "kernel32" Alias "WriteFile" _
  (ByVal hFile As Long, ByVal sz As String, ByVal cch As Long, _
  lpNumberOfBytesWritten As Long, ByVal lpOverlapped As Long) As Long

Public Declare Function WriteFilePb Lib "kernel32" Alias "WriteFile" _
  (ByVal hFile As Long, ByVal lpBuffer As Long, ByVal cb As Long, _
  lpNumberOfBytesWritten As Long, ByVal lpOverlapped As Long) As Long

Public Declare Function SetFilePointer Lib "kernel32" _
  (ByVal hFile As Long, ByVal lDistanceToMove As Long, lpDistanceToMoveHigh As Long, _
   ByVal dwMoveMethod As Long) As Long
Public Const FILE_BEGIN = 0
Public Const FILE_CURRENT = 1
Public Const FILE_END = 2

Public Declare Function SetEndOfFile Lib "kernel32" (ByVal hFile As Long) As Long

Public Declare Function CreateFileMapping Lib "kernel32" Alias "CreateFileMappingA" _
  (ByVal hFile As Long, ByVal lpFileMappingAttributes As Long, _
   ByVal flProtect As Long, ByVal dwMaximumSizeHigh As Long, _
   ByVal dwMaximumSizeLow As Long, ByVal lpName As String) As Long

Public Declare Function OpenFileMapping Lib "kernel32" Alias "OpenFileMappingA" _
  (ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, _
   ByVal lpName As String) As Long

Public Declare Function MapViewOfFile Lib "kernel32" _
  (ByVal hFileMappingObject As Long, ByVal dwDesiredAccess As Long, _
   ByVal dwFileOffsetHigh As Long, ByVal dwFileOffsetLow As Long, _
   ByVal dwNumberOfBytesToMap As Long) As Long

Public Declare Function UnmapViewOfFile Lib "kernel32" _
  (ByVal lpBaseAddress As Long) As Long

Public Const PAGE_READONLY = &H2&
Public Const PAGE_READWRITE = &H4&
Public Const PAGE_WRITECOPY = &H8&

Public Const SECTION_QUERY = &H1
Public Const SECTION_MAP_WRITE = &H2
Public Const SECTION_MAP_READ = &H4
Public Const SECTION_MAP_EXECUTE = &H8
Public Const SECTION_EXTEND_SIZE = &H10
Public Const SECTION_ALL_ACCESS = STANDARD_RIGHTS_REQUIRED Or _
  SECTION_QUERY Or SECTION_MAP_WRITE Or SECTION_MAP_READ Or _
  SECTION_MAP_EXECUTE Or SECTION_EXTEND_SIZE

Public Const FILE_MAP_ALL_ACCESS = SECTION_ALL_ACCESS
Public Const FILE_MAP_COPY = SECTION_QUERY
Public Const FILE_MAP_READ = SECTION_MAP_READ
Public Const FILE_MAP_WRITE = SECTION_MAP_WRITE


Public Type InputType
    dwType As Long
    rgdw(5) As Long
End Type

Public Const INPUT_MOUSE = 0
Public Const INPUT_KEYBOARD = 1
Public Const INPUT_HARDWARE = 2

Public Type MouseInputType
    dwType As Long
    dx As Long
    dy As Long
    mousedata As Long
    dwFlags As Long
    time As Long
    dwExtraInfo As Long
End Type

Public Const MOUSEEVENTF_ABSOLUTE = &H8000 '  absolute move
Public Const MOUSEEVENTF_LEFTDOWN = &H2 '  left button down
Public Const MOUSEEVENTF_LEFTUP = &H4 '  left button up
Public Const MOUSEEVENTF_MIDDLEDOWN = &H20 '  middle button down
Public Const MOUSEEVENTF_MIDDLEUP = &H40 '  middle button up
Public Const MOUSEEVENTF_MOVE = &H1 '  mouse move
Public Const MOUSEEVENTF_RIGHTDOWN = &H8 '  right button down
Public Const MOUSEEVENTF_RIGHTUP = &H10 '  right button up

Public Type KeybdInputType
    dwType As Long
    wVk As Integer
    wScan As Integer
    dwFlags As Long
    time As Long
    dwExtraInfo As Long
End Type

Public Const KEYEVENTF_EXTENDEDKEY = &H1
Public Const KEYEVENTF_KEYUP = &H2

Public Declare Function SendInput Lib "user32" _
  (ByVal cinput As Long, rginput() As InputType, ByVal cbInput As Long) As Long

Public Declare Function VkKeyScan Lib "user32" Alias "VkKeyScanA" _
  (ByVal cchar As Byte) As Integer

Public Declare Function MapVirtualKey Lib "user32" Alias "MapVirtualKeyA" _
  (ByVal wCode As Long, ByVal wMapType As Long) As Long

Public Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Long) As Integer

Public Declare Function GetKeyboardState Lib "user32" (ByRef lpKeyState As Byte) As Long

Public Declare Function ToAscii Lib "user32" _
  (ByVal uVirtKey As Long, ByVal uScanCode As Long, ByRef lpKeyState As Byte, _
   ByRef lpChar As Integer, ByVal uFlags As Long) As Long


Public Declare Function GetCurrentProcess Lib "kernel32" () As Long
Public Declare Function GetCurrentThread Lib "kernel32" () As Long
Public Declare Function GetPriorityClass Lib "kernel32" (ByVal hprocess As Long) As Long
Public Declare Function GetThreadPriority Lib "kernel32" (ByVal hThread As Long) As Long
Public Declare Function SetPriorityClass Lib "kernel32" _
  (ByVal hprocess As Long, ByVal dwPriorityClass As Long) As Long
Public Declare Function SetThreadPriority Lib "kernel32" _
  (ByVal hThread As Long, ByVal nPriority As Long) As Long

Public Const NORMAL_PRIORITY_CLASS = &H20
Public Const IDLE_PRIORITY_CLASS = &H40
Public Const HIGH_PRIORITY_CLASS = &H80
Public Const REALTIME_PRIORITY_CLASS = &H100

Public Const THREAD_PRIORITY_IDLE = -15
Public Const THREAD_PRIORITY_LOWEST = -2
Public Const THREAD_PRIORITY_BELOW_NORMAL = -1
Public Const THREAD_PRIORITY_NORMAL = 0
Public Const THREAD_PRIORITY_ABOVE_NORMAL = 1
Public Const THREAD_PRIORITY_HIGHEST = 2
Public Const THREAD_PRIORITY_TIME_CRITICAL = 15


Public Declare Function GetFileVersionInfo Lib "version.dll" Alias "GetFileVersionInfoA" _
  (ByVal lptstrFilename As String, ByVal dwHandle As Long, ByVal dwLen As Long, _
  lpData As Any) As Long
Public Declare Function GetFileVersionInfoSize Lib "version.dll" Alias "GetFileVersionInfoSizeA" _
  (ByVal lptstrFilename As String, lpdwHandle As Long) As Long
Public Declare Function VerQueryValue Lib "version.dll" Alias "VerQueryValueA" _
  (pBlock As Any, ByVal lpSubBlock As String, lplpBuffer As Long, _
  puLen As Long) As Long

Public Type VS_FIXEDFILEINFO
    dwSignature As Long
    dwStrucVersion As Long         '  e.g. 0x00000042 = "0.42"
    dwFileVersionMS As Long        '  e.g. 0x00030075 = "3.75"
    dwFileVersionLS As Long        '  e.g. 0x00000031 = "0.31"
    dwProductVersionMS As Long     '  e.g. 0x00030010 = "3.10"
    dwProductVersionLS As Long     '  e.g. 0x00000031 = "0.31"
    dwFileFlagsMask As Long        '  = 0x3F for version "0.42"
    dwFileFlags As Long            '  e.g. VFF_DEBUG Or VFF_PRERELEASE
    dwFileOS As Long               '  e.g. VOS_DOS_WINDOWS16
    dwFileType As Long             '  e.g. VFT_DRIVER
    dwFileSubtype As Long          '  e.g. VFT2_DRV_KEYBOARD
    dwFileDateMS As Long           '  e.g. 0
    dwFileDateLS As Long           '  e.g. 0
End Type


Public Declare Function IsBadReadPtr Lib "kernel32" _
  (ByVal lp As Long, ByVal ucb As Long) As Long


Public Declare Function LoadLibrary Lib "kernel32" Alias "LoadLibraryA" _
  (ByVal lpLibFileName As String) As Long

Public Declare Function FreeLibrary Lib "kernel32" (ByVal hLibModule As Long) As Long


Public Type SYSTEMTIME
    wYear As Integer
    wMonth As Integer
    wDayOfWeek As Integer
    wDay As Integer
    wHour As Integer
    wMinute As Integer
    wSecond As Integer
    wMilliseconds As Integer
End Type

Public Type TIME_ZONE_INFORMATION
    Bias As Long
    StandardName(32) As Integer
    StandardDate As SYSTEMTIME
    StandardBias As Long
    DaylightName(32) As Integer
    DaylightDate As SYSTEMTIME
    DaylightBias As Long
End Type

Public Declare Function GetTimeZoneInformation Lib "kernel32" _
  (lpTimeZoneInformation As TIME_ZONE_INFORMATION) As Long


Private szClassFind As String, szCaptionFind As String, hwndIgnoreFind As Long
Private hwndFound As Long


Public Function SzUserName() As String
    
    Dim szUser As String
    szUser = String(50, vbNullChar)
    Call GetUserName(szUser, CchMaxOfSz(szUser))
    szUser = SzTrimNulls(szUser)
    
    SzUserName = szUser
End Function

Public Function SzComputerName() As String
    
    Dim szComputer As String
    szComputer = String(50, vbNullChar)
    Call GetComputerName(szComputer, CchMaxOfSz(szComputer))
    szComputer = SzTrimNulls(szComputer)
    
    SzComputerName = szComputer
End Function

Public Function SzFullPath(ByVal szFile As String) As String
    
    Dim szFull As String, pszLeaf As Long
    szFull = String(300, vbNullChar)
    Call GetFullPathName(szFile, CchMaxOfSz(szFull), szFull, pszLeaf)
    szFull = SzTrimNulls(szFull)
    
    SzFullPath = szFull
End Function

Public Function SzClassFromHwnd(ByVal hwnd As Long) As String
    
    Dim cch As Long, szClass As String
    cch = 100
    szClass = String(cch + 1, vbNullChar)
    Call GetClassName(hwnd, szClass, cch + 1)
    szClass = SzTrimNulls(szClass)
    
    SzClassFromHwnd = szClass
End Function

Public Function SzCaptionFromHwnd(ByVal hwnd As Long) As String
    
    Dim cch As Long, szCaption As String
    cch = GetWindowTextLength(hwnd)
    szCaption = String(cch + 1, vbNullChar)
    Call GetWindowText(hwnd, szCaption, cch + 1)
    szCaption = SzTrimNulls(szCaption)
    
    SzCaptionFromHwnd = szCaption
End Function

Public Function SzExeVerFromFile(ByVal szExe As String) As String
    
    SzExeVerFromFile = ""
    
    Dim cb As Long, dwHandle As Long
    cb = GetFileVersionInfoSize(szExe, dwHandle)
    If cb > 0 Then
        Dim rgbInfo() As Byte: ReDim rgbInfo(cb - 1)
        If GetFileVersionInfo(szExe, dwHandle, cb, rgbInfo(0)) <> 0 Then
            Dim pvffi As Long
            If VerQueryValue(rgbInfo(0), "\", pvffi, cb) <> 0 Then
                Dim rgw(3) As Integer
                Call CopyMemFromPv(rgw(0), pvffi + 8, 8)
                SzExeVerFromFile = _
                  CStr(rgw(1)) + "." + _
                  CStr(rgw(0)) + "." + _
                  CStr(rgw(3)) + "." + _
                  CStr(rgw(2))
            End If
        End If
    End If
    
End Function


Public Function HwndFind(ByVal szClass As String, ByVal szCaption As String, _
  ByVal hwndIgnore As Long) As Long
    
    szClassFind = szClass
    szCaptionFind = szCaption
    hwndIgnoreFind = hwndIgnore
    hwndFound = hwndNil
    Call EnumWindows(AddressOf EWHwndFind, 0)
    HwndFind = hwndFound
    
End Function

Private Function EWHwndFind(ByVal hwnd As Long, ByVal lparam As Long) As Long
    
    If hwnd <> hwndIgnoreFind Then
        If SzClassFromHwnd(hwnd) Like szClassFind And _
          SzCaptionFromHwnd(hwnd) Like szCaptionFind Then
            hwndFound = hwnd
            EWHwndFind = False
            Exit Function
        End If
    End If
    
    EWHwndFind = 1 ' True in Win32
    
End Function


Public Function HwndChildFromId(ByVal hwnd As Long, ByVal id As Long) As Long
    
    hwndFound = hwndNil
    Call EnumChildWindows(hwnd, AddressOf ECWHwndChildFromId, id)
    HwndChildFromId = hwndFound
    
End Function

Private Function ECWHwndChildFromId(ByVal hwnd As Long, ByVal id As Long) As Long
    
    If GetWindowLong(hwnd, GWL_ID) = id Then
        hwndFound = hwnd
        ECWHwndChildFromId = False
        Exit Function
    End If
    
    ECWHwndChildFromId = 1 ' True in Win32
    
End Function


Public Function SzGetIni(ByVal szIniFile As String, ByVal szSection As String, _
  ByVal szKey As String, Optional ByVal szDefault As String = "") As String
    
    Dim sz As String
    sz = String(1000, vbNullChar)
    Call GetPrivateProfileString(szSection, szKey, szDefault, _
      sz, CchMaxOfSz(sz), szIniFile)
    sz = SzTrimNulls(sz)
    
    SzGetIni = sz
End Function


